<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.54
     from suif1.texi on 28 April 1999 -->

<TITLE>The SUIF Version 1 Library - If Nodes</TITLE>
<link href="suif1_16.html" rel=Next>
<link href="suif1_14.html" rel=Previous>
<link href="suif1_toc.html" rel=ToC>

</HEAD>
<BODY>
<p>Go to the <A HREF="suif1_1.html">first</A>, <A HREF="suif1_14.html">previous</A>, <A HREF="suif1_16.html">next</A>, <A HREF="suif1_113.html">last</A> section, <A HREF="suif1_toc.html">table of contents</A>.
<P><HR><P>


<H3><A NAME="SEC15" HREF="suif1_toc.html#TOC15">If Nodes</A></H3>
<P>
<A NAME="IDX53"></A>
<A NAME="IDX54"></A>

</P>
<P>
<A NAME="IDX55"></A>
<A NAME="IDX56"></A>
<A NAME="IDX57"></A>
<A NAME="IDX58"></A>
<A NAME="IDX59"></A>
<A NAME="IDX60"></A>
<A NAME="IDX61"></A>
<A NAME="IDX62"></A>
<A NAME="IDX63"></A>
Nodes from the <CODE>tree_if</CODE> class represent "if-then-else"
structures.  A <CODE>tree_if</CODE> contains three tree node lists: the
<CODE>header</CODE>, the <CODE>then_part</CODE>, and the <CODE>else_part</CODE>.  The
<CODE>header</CODE> contains code to evaluate the "if" condition and branch
to the <CODE>jumpto</CODE> label, implicitly located at the beginning of the
<CODE>else_part</CODE>, if the condition is false.  Otherwise it falls through
to the <CODE>then_part</CODE>.  The <CODE>then_part</CODE> implicitly ends with a
jump around the <CODE>else_part</CODE>.  The <CODE>tree_if</CODE> class provides
methods to access all of these parts.

</P>
<P>
The <CODE>jumpto</CODE> label must be defined in the scope containing the
<CODE>tree_if</CODE> node, but the label position is implicit rather than
being marked by a label instruction.  When the <CODE>tree_if</CODE> is
expanded to low-SUIF form, a label instruction for <CODE>jumpto</CODE> is
inserted at the beginning of the <CODE>else_part</CODE>.  The <CODE>jumpto</CODE>
label also has the restriction that it may only be used in the
<CODE>header</CODE> list of its <CODE>tree_if</CODE>; it is illegal to use this
label at all in the <CODE>then_part</CODE> or <CODE>else_part</CODE>, or at all
outside the <CODE>tree_if</CODE>.

</P>
<P>
Although the tree node lists within a <CODE>tree_if</CODE> can hold
arbitrary ASTs, certain conventions must be followed.

</P>
<P>
The <CODE>header</CODE> list is only allowed limited control flow.  It may
contain <CODE>tree_instr</CODE> nodes and other <CODE>tree_if</CODE> nodes
arbitrarily nested, but it is not allowed any <CODE>tree_block</CODE>,
<CODE>tree_for</CODE>, or <CODE>tree_loop</CODE> nodes, even in nested
<CODE>tree_if</CODE> nodes.  Label instructions may be used within the
<CODE>header</CODE>, but jumps or branches from outside the <CODE>header</CODE>
into it are not allowed; such a label can only be the target of
branches or jumps from elsewhere in the <CODE>header</CODE>.  Similarly, all
of the branch and jump instructions are allowed within the
<CODE>header</CODE>, but all the target labels must be within the
<CODE>header</CODE> list with one important exception: branch and jump
targets may include the <CODE>jumpto</CODE> label of the original
<CODE>tree_if</CODE> node.

</P>
<P>
The idea is that the <CODE>header</CODE> part contains the computation to
figure out whether the <CODE>then_part</CODE> or <CODE>else_part</CODE> is
executed.  Often this will be a single conditional branch instruction
to the <CODE>jumpto</CODE> label.  But we want to allow more complex test
computations than a single SUIF expression.  In particular, we want to
be able to capture all the computation in a C test expression, which
may contain short-circuit evaluation of <CODE>&#38;&#38;</CODE> and <CODE>||</CODE>
operators and partial evaluation of the <CODE>?:</CODE> operator.  The front
end puts the computation of the test expression of a C <CODE>if</CODE>
statement in the <CODE>header</CODE> of the <CODE>tree_if</CODE> it creates.  With
complex C expressions, it is often useful to use the structured
control flow of nested <CODE>tree_if</CODE> nodes within the <CODE>header</CODE>
and that's why they are allowed.  It is legal to have an empty
<CODE>header</CODE>, but any <CODE>tree_if</CODE> with an empty <CODE>header</CODE>, or
even with a <CODE>header</CODE> that contains no branch or jump to the
<CODE>jumpto</CODE> label is degenerate--the <CODE>then_part</CODE> will always be
executed instead of the <CODE>else_part</CODE>.

</P>
<P>
The <CODE>then_part</CODE> and <CODE>else_part</CODE> have fewer restrictions.
After the front end, in standard SUIF, neither is allowed to have
potential jumps or branches in or out, or between the <CODE>then_part</CODE>
and <CODE>else_part</CODE>.  Branches or jumps anywhere within the
<CODE>then_part</CODE> must be to labels elsewhere in the <CODE>then_part</CODE>
and labels in the <CODE>then_part</CODE> may only be used as targets by
branches or jumps within the <CODE>then_part</CODE>; the same holds
separately for the <CODE>else_part</CODE>.  Note that the symbol table
containing a label may be outside the <CODE>tree_if</CODE>; it is the label
instruction we are concerned with here.  If the label instruction is
within the <CODE>else_part</CODE>, it may only be used as a target by
instructions in other sections of the <CODE>else_part</CODE>.  Any
<CODE>tree_node</CODE>s can be used within the <CODE>then_part</CODE> and
<CODE>else_part</CODE> with arbitrary nesting, so long as the restriction on
jumps into or out of the top level <CODE>else_part</CODE> or
<CODE>then_part</CODE> lists is honored.

</P>
<P>
Either or both of the <CODE>then_part</CODE> and <CODE>else_part</CODE> may be
empty lists; in fact the <CODE>else_part</CODE> is often empty, as in C
there is often an <CODE>if</CODE> statements without an <CODE>else</CODE>.  There
is little point in having a <CODE>tree_if</CODE> if both part these parts
are to be empty, but this may occur if all the code is optimized away
or moved elsewhere.

</P>
<P>
Here's an example.  The following C code could be translated into the
SUIF code shown in a simplified form below.  (The real C front-end
generates a more complicated tree that involves nested <CODE>tree_if</CODE>
structures in the header.)

</P>

<PRE>
if ((x &#62; y) &#38;&#38; (x &#62; 0)) {
    y = x;
} else {
    y = 0;
}
</PRE>


<PRE>
IF (Jumpto=L:__L1)
IF HEADER
    bfalse e1, L:__L1
      e1: sl y, x
    bfalse e1, L:__L1
      e1: sl e2, x
        e2: ldc 0
IF THEN
    cpy y = x
IF ELSE
    ldc y = 0
IF END
</PRE>

<P><HR><P>
<p>Go to the <A HREF="suif1_1.html">first</A>, <A HREF="suif1_14.html">previous</A>, <A HREF="suif1_16.html">next</A>, <A HREF="suif1_113.html">last</A> section, <A HREF="suif1_toc.html">table of contents</A>.
</BODY>
</HTML>
