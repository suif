<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.54
     from suif1.texi on 28 April 1999 -->

<TITLE>The SUIF Version 1 Library - Sub-Variables</TITLE>
<link href="suif1_58.html" rel=Next>
<link href="suif1_56.html" rel=Previous>
<link href="suif1_toc.html" rel=ToC>

</HEAD>
<BODY>
<p>Go to the <A HREF="suif1_1.html">first</A>, <A HREF="suif1_56.html">previous</A>, <A HREF="suif1_58.html">next</A>, <A HREF="suif1_113.html">last</A> section, <A HREF="suif1_toc.html">table of contents</A>.
<P><HR><P>


<H3><A NAME="SEC57" HREF="suif1_toc.html#TOC57">Sub-Variables</A></H3>
<P>
<A NAME="IDX392"></A>

</P>
<P>
The data objects represented by variable symbols are allowed to
overlap, but only in a carefully controlled way.  The mechanism that
allows this is the existence of sub-variables.  One variable symbol is
allowed to be a sub-variable of another, occupying a portion of the
parent variable symbol specified by an offset and the types of the two
variables.  This allows arbitrary hierarchies of variable symbols and
the relations among them are known at all times.

</P>
<P>
For any hierarchy, there will always be one root ancestor in terms of
which everything else can be specified.  If neither of two variables
is a sub-variable of anything else, the two variables are guaranteed
to be completely independent objects.

</P>
<P>
Use of sub-variables is optional.  Anything that can be represented
with the sub-variable can be represented in terms of its parent by
loading from the parent's address plus the offset and with the type of
the child variable.

</P>
<P>
Sub-variable names are independent of their parent variable symbols.
Whether or not they have the <CODE>userdef</CODE> flag set is also independent
of the parent.  There is nothing inconsistent in having a parent which
is user-defined and a sub-variable which is not (if some stage of the
compiler decides a particular piece of a larger user-defined object has
some special property and wants a handle on it), and neither is there
anything inconsistent in a sub-variable that is user-defined in a parent
which is not (user-defined variables that are equivalenced cause the
system to create a new aggregate object).

</P>
<P>
Among the properties general to the <CODE>sym_node</CODE> class that may be
changed by the user, the only one for which sub-variables have any
special restriction is the symbol table in which they reside.
A sub-variable should be in the same symbol table as its parent.  They
may be temporarily in different symbol tables while being moved
around, so the library doesn't enforce the restriction that they
always be in the same symbol table at all times.  The library does
enforce the restrictions that they must be in the same table when a
symbol is first linked in as a sub-variable by the
<CODE>var_sym::add_child</CODE> method and when the owning symbol table of
the parent or child variable is written out.

</P>
<P>
<A NAME="IDX393"></A>
Among properties specific to variable symbols that the user has
control over, some may be set independently in sub-variables, and some
are kept consistent between sub-variables and their parents.  For the
properties that are kept consistent, the value of the property in the
sub-variable is slaved to that in the parent.  In that case, either
setting or getting the property from the sub-variable has exactly the
same effect as getting or setting it from the parent variable.  If and
when the sub-variable is removed from being a sub-variable by the
<CODE>var_sym::remove_child</CODE> method, the current values of those
properties in the sub-variable are set to the values of the parent at
the time of separation and can be changed independently from then on.

</P>
<P>
<A NAME="IDX394"></A>
<A NAME="IDX395"></A>
Sub-variables are attached to parent variables by the <CODE>add_child</CODE>
method.  The user tells the parent which variable symbol is to be the
sub-variable and at what offset it begins and the library makes it a
sub-variable.  If it is ever necessary to remove a sub-variable, the
<CODE>remove_child</CODE> method may be used.  It undoes what
<CODE>add_child</CODE> did making the sub-variable into a fully independent
variable.  The argument of <CODE>remove_child</CODE> must already be a
sub-variable of the given variable.  This method should be called
before a sub-variable is deleted, or else there will be dangling
pointers from the parent.

</P>
<P>
<A NAME="IDX396"></A>
<A NAME="IDX397"></A>
Two methods, <CODE>parent_var</CODE> and <CODE>offset</CODE>, provide a way to see
whether a variable is a sub-variable, and if so what variable it is a
sub-variable of and where it is in that variable.  If a variable is
not a sub-variable, <CODE>parent_var</CODE> will return <CODE>NULL</CODE>.  If it
is a sub-variable, this method will return the variable symbol for the
variable of which it is a sub-variable.  If it is a sub-variable, the
<CODE>offset</CODE> method will return the offset in bits of the beginning
of the sub-variable from the beginning of the parent variable.  If
called on a variable that is not a sub-variable, <CODE>offset</CODE> will
return zero.

</P>
<P>
<A NAME="IDX398"></A>
<A NAME="IDX399"></A>
There are also methods provided to access all the variables that are
sub-variables of a given variable.  The <CODE>num_children</CODE> method
will return the number of sub-variables for a given variable symbol.
Given a non-negative number less than the number of children, the
<CODE>child_var</CODE> method will return one of the sub-variables.  The
ordering of the sub-variables is the order in which they were added,
starting with zero for the oldest remaining sub-variable.  For
example, <CODE>child_var(2)</CODE> will return the sub-variable that was the
third added of those that are still sub-variables.  Hence calling this
method for each integer from zero through the number of children minus
one will give all the sub-variables.  It is an error to call
<CODE>child_var</CODE> with an argument greater than or equal to the number
of sub-variables or less than zero.

</P>
<P>
<A NAME="IDX400"></A>
<A NAME="IDX401"></A>
<A NAME="IDX402"></A>
Given a variable that may or may not be a sub-variable, the
<CODE>root_ancestor</CODE> will return the unique ancestor of a sub-variable
that is not itself a sub-variable, or the variable itself if it is not
a sub-variable.  The <CODE>root_offset</CODE> method will return the offset
of the variable within that root ancestor, which will be the sum of
all the offsets along the path up.  The <CODE>overlaps</CODE> method is
provided to determine whether any two variables overlap to any extent.
If the result is <CODE>TRUE</CODE>, then they definitely overlap, at least
in part; otherwise, the result is <CODE>FALSE</CODE>, and the variables do
not overlap at all.

</P>
<P>
<A NAME="IDX403"></A>
It is often useful to know whether a given portion of a variable
treated as a given type has a corresponding sub-variable.  The
<CODE>find_child</CODE> method will answer that question.  If such a
sub-variable exists, it will return a pointer to it, otherwise it will
return NULL.

</P>
<P>
<A NAME="IDX404"></A>
As an alternative to attaching an existing variable symbol as a
sub-variable to another variable, the <CODE>build_child</CODE> method is
provided.  Given a location and type within a variable and a name, it
creates an sub-variable, installs it in the proper symbol table if the
parent variable was in a symbol table, and adds it as a sub-variable,
all at once.

</P>

<P><HR><P>
<p>Go to the <A HREF="suif1_1.html">first</A>, <A HREF="suif1_56.html">previous</A>, <A HREF="suif1_58.html">next</A>, <A HREF="suif1_113.html">last</A> section, <A HREF="suif1_toc.html">table of contents</A>.
</BODY>
</HTML>
