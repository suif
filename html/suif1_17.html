<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.54
     from suif1.texi on 28 April 1999 -->

<TITLE>The SUIF Version 1 Library - For Nodes</TITLE>
<link href="suif1_18.html" rel=Next>
<link href="suif1_16.html" rel=Previous>
<link href="suif1_toc.html" rel=ToC>

</HEAD>
<BODY>
<p>Go to the <A HREF="suif1_1.html">first</A>, <A HREF="suif1_16.html">previous</A>, <A HREF="suif1_18.html">next</A>, <A HREF="suif1_113.html">last</A> section, <A HREF="suif1_toc.html">table of contents</A>.
<P><HR><P>


<H3><A NAME="SEC17" HREF="suif1_toc.html#TOC17">For Nodes</A></H3>
<P>
<A NAME="IDX77"></A>
<A NAME="IDX78"></A>

</P>
<P>
<A NAME="IDX79"></A>
Many of our compiler passes are designed to work with Fortran <CODE>DO</CODE>
loops because they are relatively easy to analyze.  A <CODE>DO</CODE> loop has
a single index variable that is incremented or decremented on every
iteration and varies from an initial to a final value.  SUIF uses
<CODE>tree_for</CODE> nodes to represent well-structured <CODE>DO</CODE> loops.  The
exact conditions that a loop must meet to be represented as a
<CODE>tree_for</CODE> in SUIF are described below.  The expander's cleanup
pass, which is run immediately after the front-end, converts
<CODE>tree_for</CODE> nodes that violate any of these conditions into
<CODE>tree_loop</CODE> nodes (see section <A HREF="suif1_16.html#SEC16">Loop Nodes</A>).  Even though they are
primarily intended for Fortran code, <CODE>tree_for</CODE> nodes may also be
used for C loops that meet the same conditions.

</P>
<P>
<A NAME="IDX80"></A>
<A NAME="IDX81"></A>
The index variable of a <CODE>tree_for</CODE> can be accessed using the
<CODE>index</CODE> method and set using the <CODE>set_index</CODE> method.  The
index variable must be a scalar variable that is defined in the scope of
the <CODE>tree_for</CODE> node.  It may not be modified anywhere within the
loop body.  This applies across procedures as well.  If the loop body
contains a call to another procedure that modifies the index variable,
then the loop cannot be represented by a <CODE>tree_for</CODE> node.
Moreover, if you are using Fortran form, the index variable may not be a
call-by-reference parameter.
See section <A HREF="suif1_89.html#SEC89">Features for Compiling Fortran</A>.

</P>
<P>
<A NAME="IDX82"></A>
<A NAME="IDX83"></A>
<A NAME="IDX84"></A>
<A NAME="IDX85"></A>
<A NAME="IDX86"></A>
<A NAME="IDX87"></A>
The range of values for the index variable is specified by three
<CODE>operand</CODE> fields.  See section <A HREF="suif1_30.html#SEC30">Operands</A>.  The lower and upper bounds and
the step value can be accessed by the <CODE>lb_op</CODE>, <CODE>ub_op</CODE>, and
<CODE>step_op</CODE> methods.  The <CODE>set_lb_op</CODE>, <CODE>set_ub_op</CODE>, and
<CODE>set_step_op</CODE> methods are provided to change them.  These operands
are expressions that are evaluated once at the beginning of the loop.
The index variable is initially assigned the lower bound value and then
incremented by the step value on every iteration until it reaches the
upper bound value; the code to do this is automatically created when the
<CODE>tree_for</CODE> is expanded to low-SUIF code.

</P>
<P>
<A NAME="IDX88"></A>
<A NAME="IDX89"></A>
<A NAME="IDX90"></A>
<A NAME="IDX91"></A>
<A NAME="IDX92"></A>
<A NAME="IDX93"></A>
Most users will always use <CODE>tree_for</CODE> nodes in conjunction with
expression trees.  Flat lists of instructions are typically used only in
the library and with back-end passes where the <CODE>tree_for</CODE> nodes
have been dismantled.  It is possible to use <CODE>tree_for</CODE> nodes
without expression trees, but the bounds and step values cannot be
treated as operands.  In fact, even with expression trees those operands
are actually stored on tree node lists.  If necessary, these lists can
be accessed directly using the <CODE>lb_list</CODE>, <CODE>ub_list</CODE>, and
<CODE>step_list</CODE> methods.  Each list is required to contain a single
expression with a dummy copy instruction at the root.  The destination
of the dummy copy must be a null operand.  Methods are provided in the
tree node list class to extract the operands from the tree node lists
(see section <A HREF="suif1_20.html#SEC20">Tree Node Lists</A>).

</P>
<P>
<A NAME="IDX94"></A>
<A NAME="IDX95"></A>
<A NAME="IDX96"></A>
The <CODE>tree_for</CODE> must also specify the comparison operator used to
determine when the index variable has reached the upper bound value.
The possible comparison operators are members of the
<CODE>tree_for_test</CODE> enumerated type.  The <CODE>test</CODE> and
<CODE>set_test</CODE> methods are used to access and modify the comparison
operator for a <CODE>tree_for</CODE> node.  The <CODE>tree_for_test</CODE>
enumeration includes quite a few comparison operators, but some of them
are only used by the front-end.  Both signed and unsigned versions are
available for most of the comparison operators, as indicated by the
letters "<CODE>S</CODE>" and "<CODE>U</CODE>" in their names.

</P>
<DL COMPACT>

<DT><CODE>FOR_SLT</CODE>
<DD>
<DT><CODE>FOR_ULT</CODE>
<DD>
Less than.  Repeat as long as the index is strictly less than the upper
bound.

<DT><CODE>FOR_SLTE</CODE>
<DD>
<DT><CODE>FOR_ULTE</CODE>
<DD>
Less than or equal.  Repeat as long as the index is less than or equal
to the upper bound.

<DT><CODE>FOR_SGT</CODE>
<DD>
<DT><CODE>FOR_UGT</CODE>
<DD>
Greater than.  Repeat as long as the index is strictly greater than the
upper bound.  Sometimes <CODE>DO</CODE> loops go backwards, using a negative
step value.  For those loops, the comparison operator must also be
reversed.

<DT><CODE>FOR_SGTE</CODE>
<DD>
<DT><CODE>FOR_UGTE</CODE>
<DD>
Greater than or equal.  Repeat as long as the index is greater than or
equal to the upper bound.  Again, this may be used when the step value
is negative.

<DT><CODE>FOR_SGELE</CODE>
<DD>
<DT><CODE>FOR_UGELE</CODE>
<DD>
These comparisons are only used by the front-end.  In FORTRAN, it may
not be possible to determine the direction of a loop at compile time.
For example, if the step value is not a constant, it could be either
positive or negative.  These comparison operators indicate that the loop
test may be either "greater than or equal" or "less than or equal",
depending on the sign of the step value.  The expander's cleanup pass
converts any <CODE>tree_for</CODE> nodes with these tests to two
<CODE>tree_for</CODE> nodes and a <CODE>tree_if</CODE> node to decide between them.
Thus, these comparison operators should never be encountered in most
SUIF code.

<DT><CODE>FOR_EQ</CODE>
<DD>
<DT><CODE>FOR_NEQ</CODE>
<DD>
Equal and not equal.  These comparisons are only used by the front-end.
The expander's cleanup pass dismantles <CODE>tree_for</CODE> nodes that use
these comparisons.
</DL>

<P>
<A NAME="IDX97"></A>
<A NAME="IDX98"></A>
The body of a <CODE>tree_for</CODE> loop is stored in a tree node list.  The
methods to get and set the loop body are <CODE>body</CODE> and
<CODE>set_body</CODE>, respectively.  The <CODE>body</CODE> list contains only the
instructions corresponding to the body of the loop in the source
program.  The instructions to compare the index variable with the upper
bound, increment it, and branch back to the beginning of the body are
not included as part of the body; they are created when the
<CODE>tree_for</CODE> is expanded to low-SUIF code.

</P>
<P>
<A NAME="IDX99"></A>
<A NAME="IDX100"></A>
<A NAME="IDX101"></A>
Besides the loop body, a <CODE>tree_for</CODE> node has an additional tree
node list called the <CODE>landing_pad</CODE>.  The code in the landing pad
is executed if and only if the loop body is executed at least one
time, but the <CODE>landing_pad</CODE> is executed only once, unlike the
body which is usually executed many times.  The <CODE>landing_pad</CODE> is
executed immediately before the first time through the loop body.  The
landing pad thus provides a place to move loop-invariant code.

</P>
<P>
<A NAME="IDX102"></A>
<A NAME="IDX103"></A>
<A NAME="IDX104"></A>
<A NAME="IDX105"></A>
Two labels are associated with a <CODE>tree_for</CODE>: <CODE>contlab</CODE> and
<CODE>brklab</CODE>.  A "continue" statement in the loop body requires a
jump over the rest of the body to the code that increments the index and
continues with the next iteration.  This can be implemented with a jump
to the <CODE>contlab</CODE> label, which is implicitly located at the end of
the <CODE>body</CODE> list.  Similarly, a "break" statement in the loop is
translated to a jump to the <CODE>brklab</CODE> label which is located
immediately after the loop.  These two labels must be defined in the
scope of the <CODE>tree_for</CODE> node, but the label instructions that mark
their locations are not inserted into the tree node lists until the
<CODE>tree_for</CODE> node is expanded into low-SUIF form.

</P>
<P>
In summary, the semantics of a <CODE>tree_for</CODE> node are as follows.  The
lower bound, upper bound, and step operands are evaluated once at the
beginning of the loop <A NAME="DOCF2" HREF="suif1_foot.html#FOOT2">(2)</A>.  The lower bound is compared to the upper bound.  If
the test fails, the loop does not execute.  Otherwise, the lower bound
is assigned to the index variable, and any code in the landing pad list
is executed.  After that, the body is executed repeatedly and the index
variable is incremented by the step value on every iteration, until the
index variable fails the test with the upper bound value.

</P>
<P>
As an example, the following C code could be translated into the SUIF
code shown in a simplified form below:

</P>

<PRE>
for (i = 100; i &#62;= 0; i--) {
    A[i] = i; 
}
</PRE>


<PRE>
FOR (Index=i Test=SGTE Cont=L:__L1 Brk=L:__L2)
FOR LB
    ldc 100
FOR UB
    ldc 0
FOR STEP
    ldc -1
FOR LANDING PAD
FOR BODY
    str e1 = i
      e1: array e2+0, size(32), index(i), bound(e3)
        e2: ldc &#60;A,0&#62;
          e3: ldc 101
FOR END
</PRE>

<P><HR><P>
<p>Go to the <A HREF="suif1_1.html">first</A>, <A HREF="suif1_16.html">previous</A>, <A HREF="suif1_18.html">next</A>, <A HREF="suif1_113.html">last</A> section, <A HREF="suif1_toc.html">table of contents</A>.
</BODY>
</HTML>
