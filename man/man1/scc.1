.TH SCC 1 "Feb 14 1994" "Stanford University" "SUIF Compiler"
.SH NAME
scc \- SUIF compiler driver program
.SH SYNOPSIS
.B scc
[
.I options
] files .\^.\^.
.SH DESCRIPTION
The
.I scc
program is the driver for the SUIF ANSI C and FORTRAN 77 compiler.
It behaves much like the system's
.I cc
compiler.  SUIF is a multi-pass compiler.  The following chart shows
the order in which some of the basic passes are executed and the
filename extensions that
.I scc
uses for the output of each pass:
.TP \w'abunchofspaces'u+1n
.I .c
Fortran-to-C translator
.TP
.I .i
C preprocessor
.TP
.I .snt
C front end
.TP
.I .spd
SUIF cleanup pass
.TP
.I .spx
Expander
.TP
.I .sfo
Back-end optimizer
.TP
.I .s
Code generator (currently only for MIPS systems)
.TP
.I .o
Assembler
.TP
(a.out)
Linker
.PP
The extension of each filename argument determines where in the
compilation process the compiler will start for that file.
For example, files whose names end with
.I .c
are assumed to be C source programs, whereas arguments whose names
end with
.I .s
are assumed to be assembler files.  Files with
extensions
.I .f
and
.I .F
are both treated as Fortran files.
.SH OPTIONS
Options with identical meanings to those in
.I cc
are
.BR \-S ,
.BR \-c ,
.BR \-I ,
.BR \-D ,
.BR \-U ,
.BR \-o ,
.BR \-L ,
.BR \-G ,
.BR \-l ,
.BR \-w ,
.B \-E
and
.BR \-M .
The following options are specific to
.IR scc :
.TP
.BI \-B dir
Look for the compiler binaries in directory
.IR dir .
.TP
.PD 0
.B \-show
.TP
.B \-v
.TP
.PD
.B \-V
As each pass is executed, print the pass name followed by the command
line to execute it.
.TP
.B \-k
Don't execute the passes.  Useful in conjunction with
.B \-v
to see what passes
.I scc
executes with which flags.  For example, to see what passes are run
with the
.B \-O
flag, run ``scc \-v \-k \-O foo.c''.
.TP
.B \-T
Print timing information for each pass as it is executed.
.TP
.B \-g
Keep symbolic information for debugging.  This affects only
.I s2c
or other back ends.  For
.IR s2c ,
it means putting the original source line numbers in the output
C file.
.TP
.B \-s2c
Use the SUIF-to-C pass in place of the SUIF back-end.  This is
the default for systems for which there is no SUIF code generator
(at present, everything except MIPS).
.TP
.BI \-cc " compiler"
Use the specified C compiler to generate code after running the
SUIF-to-C pass.  This option is only meaningful in combination with
the
.B \-s2c
option.
.TP
.PD 0
.B \-f2c
.TP
.PD
.B \-sf2c
Informs relevant passes that source files came from Fortran.
This is the default for filenames with
.I .f
or
.I .F
extensions.
.TP
.BI \-. suffix
Stop the compilation process after files with the given
.I suffix
have been generated (see chart above).  For example, the option
``\-.sfo'' stops the compilation process after optimization, placing
output in files with a suffix ``.sfo''.  (The
.B \-.s
and
.B \-.o
options are equivalent to
.B \-S
and
.B \-c
respectively.)
.TP
.B \-keep
Save all files generated for each phase, with the appropriate
extensions, in the current directory.  If this flag is repeated,
also save temporary files used within phases when a phase executes
more than one pass, as does the optimization phase.
.TP
.B \-O
Invoke the SUIF optimizer and register allocator.  If your program
does not speed up by at least 50%, Steve Tjiang will donate a pizza
to the charity of his choice. Really.
.TP
.B \-O2
Invoke the SUIF optimizer.  Run an extra pass of code motion.
.TP
.B \-Oivard
Find induction variables and perform other optimizations in the front-end.
.TP
.B \-Oifs
Moves IFs in and then out again.
.TP
.B \-Oregs
Do register allocation.
.TP
.BI \-t dir
Use directory
.I dir
for temporaries.  If no
.B \-t
option is specified, temporary files are written to the /tmp directory
by default.  See also the
.B TMPDIR
environment variable.
.TP
.B \-reassoc
Reassociate complex array address calculations such that it is
easier to code motion parts out of the inner loop.  The default
is to do so only when scalar optimization is being performed.
.TP
.B \-noreassoc
Don't bother to reassociate array address calculations.
.TP
.BI \-Target " machine"
Override the default target architecture to perform a
cross-compilation with target
.I machine.
This option only affects passes that are provided with SUIF; correct
cross-system use of assemblers, linkers, back-end C compilers, and
files include by the C pre-processor is left up to the user.  To be
safe, this option should only be used when compiling up to the point
where the code is converted out of SUIF into assembly or back-end C
code.  Likewise, system include-file compatability is up to the user,
so Fortran should be fine but typical C code won't go through cpp
correctly without additional measures because the included system
header files will not be those of the target machine.
.TP
.BI \-option " passname flags"
Call compilation pass
.I passname
with the
.I flags
options.  For example, use ``\-option SKWEEL \-W'' to have
.I skweel
print out statistics.  You need to know the name of the pass
to use this flag. Some common pass names are:
.BR SF2C ,
.BR CPP ,
.BR SNOOT ,
.BR FIXFORTRAN ,
.BR MGEN ,
and
.BR LD .
Phases that might run more than once have more obscure names,
since each pass must be uniquely named.  If you use the verbose option
(\-v), you will see the pass name of each pass as it is executed,
followed by the command to run that pass.  See the table in
src/scc/commands.def for a complete list.
.TP
.BI \-yes " passname"
Force a particular pass to run, overriding other options.
For example, ``scc f.c \-yes SR'' runs the strength reduction pass,
even though
.B \-O
wasn't specified.  The string
.I passname
must be a valid pass name; see the
.B \-option
flag for details about pass names.
.TP
.BI \-no " passname"
This is the opposite of the
.B \-yes
flag.  It prevents a pass from running, even if specified by
other options.  For example, ``scc \-O f.c \-no REG' runs the
optimizer but skips the register allocation phase.  As with the
.B \-yes
flag,
.I passname
must be a valid pass name; see the
.B \-option
flag for details about pass names.
.TP
.B \-automatic
Put Fortran locals on the stack by calling
.I sf2c
with the
.B \-a
flag.  This is the default.
.TP
.B \-static
Keep Fortran locals in memory and initialize them to zero.
.TP
.BI \-N anything
Any flag beginning with upper case ``N'' is passed through to
.I sf2c
unchanged.
.I sf2c
uses this to specify implementation limits for its own internal
processing.  See the
.I sf2c
documentation.
.TP
.B \-checkwarn
This runs
.I checksuif1
on the output of every pass that produces a SUIF1 file.  The
.I checksuif1
program is given the
.B \-warn
flag so it will print a warning about anything illegal that it finds.
.TP
.B \-checkfail
This is identical to
.B \-checkwarn
except that it gives the
.B -fail
switch to
.I checksuif1
so it will print an error message and abort the whole compile if it
finds a problem.
.TP
.B \-Wall
This turns on all warnings again, in case they were turned off by
.BR \-w .
.SH ENVIRONMENT VARIABLES
.TP
.B SUIFHOME
The top directory in the SUIF file hierarchy.
.TP
.B MACHINE
The type of machine on which the system is running.  The convention
used is <architecture>-<vendor>-<OS>, for example, mips-sgi-irix5,
sparc-sun-sunos4, or mips-dec-ultrix.
.TP
.B SUIFPATH
List of directories to search for SUIF binaries.
The directory names are separated by colons.  If this variable is
not specified,
.I scc
defaults to look in the $\fBSUIFHOME\fR/$\fBMACHINE\fR/bin directory.
.TP
.B TMPDIR
Directory where temporary files are stored.  The default value is /tmp.
This can also be overridden with the
.B \-t
option.
.SH HISTORY
The
.I scc
program was originally written by Michael Wolf, who also
developed the basis for the table-driven approach that is currently
used by
.IR scc .
Bob Wilson rewrote most of the original code to make the pass
table more flexible and to support interprocedural passes.
Since then Chris Wilson has fixed several bugs and made a lot of
updates to the options and passes.
