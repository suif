.TH SNOOT 1 "April 19 1994" "Stanford University" "SUIF Compiler"
.SH NAME
snoot \- translate pre-processed C to SUIF
.SH SYNOPSIS
.B snoot
[
.I options
]
.I infile
.I outfile
.SH DESCRIPTION
The
.I snoot
program is the core of the SUIF C front end.  It translates from
pre-processed ANSI-C to SUIF.  The SUIF code that
.I snoot
produces is non-standard, however; it must be passed through
.I porky
with the
.I -defaults
flag before it becomes standard SUIF that other passes can handle.
.PP
The entire C front-end for SUIF consists of three passes.  First,
.I cpp,
the ANSI-C pre-processor.  This is not included with SUIF.  Any
standard ANSI-C pre-processor may be used; the gnu C pre-processor
that is included with gcc is one option.  The output of
.I cpp
is then fed into
.I snoot.
The output from
.I snoot
is the first SUIF file.  Then the last stage is
.I porky
with the
.I -defaults
option.  Together, these three passes turn ANSI-C into standard SUIF.
.SH OPTIONS
.TP
.B \-W
Turn on more warning messages.  By default, only important warnings
are displayed.  If used a second time, all possible warnings are
issued.
.TP
.B \-w
Suppress all warning messages.  This overrides the
.B \-W
option.
.TP
.B \-keep-comments
Save all comments from the C code as "C comment" annotations on SUIF
``mrk'' instructions.  By default, comments are ignored.
.sp
Note that for this to have any effect, the C comments must have been
preserved by the C pre-processor; by default
.I cpp
throws them away before
.I snoot
even sees the code.  Use the
.B \-C
option for
.I cpp
to save comments in the pre-processing stage.
.TP
.B \-ignore-comments
Do not save C comments.  This is the default.  If both
.B \-keep-comments
and
.B \-ignore-comments
are specified, the one that occurs latest on the command line takes
effect.
.TP
.B \-x
Write cross-reference of symbol uses as "source references"
annotations on the SUIF symbols.  The location of each reference in
the source file is recorded.
.TP
.B \-P
Print function prototypes and global variable declarations to standard
error.  This output can be appended to the top of a source file so
that the next time it's compiled more type checking can be done.
.TP
.BI \-s density
Set the density of labels used as a cutoff for using SUIF mbr
instructions instead of individual comparisons for switch statements.
The
.I density
is interpreted as a floating point value.  The default is 0.5, meaning
that at least half the values in a range have to have case labels
before an mbr instruction is used.
.TP
.BI \-e n
Set the maximum number of error messages
.I snoot
will issue before giving up.  The default is 20.  Ordinarily,
.I snoot
will issue an error message and continue to look for more errors.
Even if the rest of the program can successfully be translated,
.I snoot
will still return an error code.  Once the limit is reached,
.I snoot
assumes that it is so hopelessly confused by earlier errors that
there's no point in reporting any more.
.TP
.B \-mark-execution-points
Put "sequence point" annotations in the SUIF code corresponding to
sequence points in the source code, when possible.  Temporary values
will be spilled into new variables if necessary.  The one situation
where sequence points are not properly inserted is for C ``for()''
statements that are translated into SUIF TREE_FOR nodes.  There is no
place on a SUIF TREE_FOR to put the sequence point that comes between
evaluation of the incrementing expression and the test expression.
.sp
Sequence points are defined in the ANSI C standard as points in the
execution where some things are guaranteed to have already been
executed and others not yet executed.  The standard defines exactly
where they occur.  The option of marking these in the SUIF code is
provided in case later passes want to use them to print traces of
variable values or something of that sort.
.TP
.B \-ignore-execution-points
Do not mark sequence points in the SUIF code.  This is the default.
If both
.B \-mark-execution-points
and
.B \-ignore-execution-points
are specified, the one that occurs latest on the command line takes
effect.
.TP
.B \-null-check
Insert code to check at runtime for a NULL pointer every time memory
is explicitly read or written in the C code (i.e. for every use of the
``[]'', ``->'', or unary ``*'' operators).  If a NULL pointer is
found, an error message is issued giving the source line number and
file and the program aborts.
.TP
.B \-no-null-check
Do not insert code to check at runtime for NULL pointers.  This is the
default.  If both
.B \-null-check
and
.B \-no-null-check
are specified, the one that occurs latest on the command line takes
effect.
.TP
.BI \-T target-name
Use the system-specific target parameters specified by
.I target-name
in compilation.  This is used in cross-compilation or to generate code
for a different back-end C compiler.  The parameters include such
things as the byte order of the machine, the sizes of the various
types, and whether the type ``char'' is signed or unsigned.  The
default is to use the parameters from the C compiler used to build
snoot (typically gcc).
.sp
To see a list of targets
.I snoot
understands, type
.I snoot
.I \-T\-.
Target information is stored in the
.I snoot
source file
.I config.h.
See the
.I snoot
source file
.I find_params.c
for information on generating target information for new systems.
.TP
.B \-keep-typedef-info
Create dummy variables to represent typedef names.  The dummy
variables are given type ``int'' and have "typedef name" annotations
attached.  The data on the annotation contains an immed list with one
item, the type of the typedef.
.TP
.BI "\-builtin " builtin-name
Consider
.I builtin-name
to be a built-in identifier.  That means that
.I builtin-name
can be used as a variable or followed by arguments in parentheses and
each such expression will be translated to a SUIF generic instruction
with
.I builtin-name
as its name.  Within the parentheses can be a comma-separated argument
list, where each argument can be a type, a string literal, or an
expression.  These generic instructions will be passed through most
SUIF passes and
.I s2c
will translate them back into a form similar to their form in the
original source code.  This can be used to implement asm() directives,
or built-in identifiers used for vararg macros in headers for some
systems.  Note that an alternative to specifying built-in identifiers
on the command-line with this switch is to specify them in the
configuration file "config.h".
.TP
.B \-force-enum-is-int
Force all enum types to use type ``int'' as their equivalent type.
Ordinarily, enum types are mapped to the smallest integer type that
can contain all their enumeration constants.  This option overrides
that for compatibility, since some programs assume that the compiler
maps enums only to ``int'' under all circumstances.
.SH HISTORY
The first ANSI-C front-end for SUIF was
.I snout,
written by Robert French.  It was based on
.I lcc,
a portable C front-end with a code-generation interface (see ``A Code
Generation Interface for ANSI C'' by Fraser and Hanson).  Since the
back-end interface provided by
.I lcc
is too low-level for SUIF, some substantial changes were made in
various levels of
.I lcc
code and data-structures to preserve more information when generating
SUIF code.
.PP
When the SUIF system was overhauled, Todd Smith rewrote the
SUIF-specific parts of
.I snout
to create
.I snoot
for new SUIF.  When more changes were made to SUIF that required more
information to be preserved that lcc was losing early-on, Chris Wilson
rewrote
.I snoot.
Various internal transformations and datastructures used by lcc were
removed.  Now the front-end of lcc is used to generate SUIF code
directly.
