#!/bin/csh

set envvars=(SUIFHOME MACHINE SUIFPATH COMPILER_NAME \
	 MANPATH LD_LIBRARY_PATH PATH TCL_INCLUDE_FLAGS \
	 CC CXX  \
	 DEFAULT_SYSTEM_SPECIFIC_CXXFLAGS \
	 DEFAULT_SYSTEM_SPECIFIC_CFLAGS \
	 CC_OVERRIDE \
	 CXX_OVERRIDE \
	 SYSTEM_SPECIFIC_CFLAGS \
	 SYSTEM_SPECIFIC_CXXFLAGS \
	 INCLDIR SRCDIR MANDIR INFODIR HTMLDIR DOCDIR SCRIPTDIR TCLDIR \
	 MACHDIR BINDIR LIBDIR SODIR \
	 AUTOINITDIR VERDATADIR \
	 PUREHOME \
	 PURE \
	 PURE_FLAGS \
	 PURE_COLLECTOR \
	 VISUAL_TCL)

set flag='-csh'
set limits_off=0

while ($#argv)
    set flag1=$argv[1]
    if ("$flag1" == '-limits') then
	set limits_off=1
	shift
	continue
    endif
    if ("$flag1" == '-suifhome') then
	shift
        if ($#argv < 0) then
	   echo "-suifhome must have an argument"
	   exit 1
        endif
	setenv $SUIFHOME $argv[1]
	shift
	continue
    endif
    if (("$flag1" == '-csh') || ("$flag1" == '-sh') || ("$flag1" == '-perl')) then
	set flag=$flag1
	shift
	continue
    endif
    break
end

if (($#argv > 0) || \
    ((x"$flag" != 'x-csh') && (x"$flag" != 'x-sh') && \
	(x"$flag" != 'x-perl'))) then
    echo "Usage: $0 [-limits] [-suifhome home] { -sh | -csh | -perl }"
    echo "       Defaults to csh"
    exit 1
else if (! $?SUIFHOME) then
    echo "Need SUIFHOME to be set or use the -suifhome option"
    exit 1
endif

set gccver=`gcc -v |& fgrep 'Reading specs' |& head -1`
if (! $?MACHINE) then
    setenv MACHINE `echo $gccver | sed -e s'/^.*gcc-lib\///' | \
			sed -e s'/\/.*$//'`
endif

if (! $?SUIFPATH) then
    setenv SUIFPATH `echo $gccver | sed -e 's/Reading specs from //' | \
			set -e 's/\/specs//'`
endif

if (! $?COMPILER_NAME) then
    echo $MACHINE | grep '^sparc-sun-solaris[.0-9]*$' >& /dev/null
    if ((! $status) && ($COMPILER_NAME == 'sun_procompiler')) then
        setenv MACHINE "${MACHINE}-sunpro"
    else if (($MACHINE == 'mips-sgi-irix5.3) && \
	     ($COMPILER_NAME =~ gcc*)) then
        setenv MACHINE "${MACHINE}-gcc"
    endif
else if ($MACHINE == "mips-sgi-irix5.3") then
    setenv COMPILER_NAME 'sgi_c++'
else if ($MACHINE =~ i386-*-solaris2.4) then
    setenv COMPILER_NAME 'sun_procompiler'
else if ($MACHINE == 'i586-linux') then
    setenv COMPILER_NAME 'egcs'
    setenv CC_OVERRIDE 'gcc'
    setenv CXX_OVERRIDE 'g++'
else
    setenv COMPILER_NAME 'gcc'
    setenv CC_OVERRIDE 'gcc'
    setenv CXX_OVERRIDE 'g++'
endif

if ($?MANPATH) then
    echo $MANPATH | fgrep "$SUIFHOME/man" >& /dev/null
    if ($status) then
	setenv MANPATH "$SUIFHOME/man:$MANPATH"
    endif
else
    set oldmanpath=`man -w`
    if ($status) then
	# have to guess
	$oldmanpath="/krb5/man:/usr/share/catman:/usr/share/man:/usr/catman:/usr/man:/opt/SUNWspro/man:/usr/local/man"
    endif
    setenv MANPATH "$SUIFHOME/man:$oldmanpath"
endif

if ($?LD_LIBRARY_PATH) then
    setenv LD_LIBRARY_PATH "$SUIFHOME/$MACHINE/solib:$LD_LIBRARY_PATH"
else
    setenv LD_LIBRARY_PATH "$SUIFHOME/$MACHINE/solib"
endif

# put SUIFHOME dirs in path
echo $PATH | fgrep "${SUIFHOME}/${MACHINE}/bin" >& /dev/null
if ($status) then
    setenv PATH "${SUIFHOME}/${MACHINE}/bin:${PATH}"
endif
echo $SUIFPATH | fgrep "${SUIFHOME}/${MACHINE}/bin" >& /dev/null
if ($status) then
    setenv SUIFPATH "${SUIFHOME}/${MACHINE}/bin:${SUIFPATH}"
endif

# For the vbrowser, we need to include <tcl.h>, which isn't in
# /usr/include on all our machines.  The following handles that issue.

if ($MACHINE == 'mips-sgi-irix5.3') then
    setenv TCL_INCLUDE_FLAGS '-I/usr/local/include'
else if ($MACHINE == 'sparc-sun-solaris2.3') then
    setenv TCL_INCLUDE_FLAGS '-I/usr/openwin/include -DTK4_1 -L/usr/openwin/lib'
else if ($MACHINE =~ i386-*-solaris2.4) then
    setenv TCL_INCLUDE_FLAGS '-I/usr/local/include -I/usr/openwin/include -L/usr/local/lib -L/usr/openwin/lib'
else if ($MACHINE =~ sparc-sun-solaris*) then
    setenv TCL_INCLUDE_FLAGS '-I/usr/local/include -I/usr/local/ultra/include -I/usr/openwin/include -L/usr/local/lib -L/usr/local/ultra/lib -L/usr/openwin/lib'
else if ($MACHINE == 'alpha-dec-osf3.2') then
    setenv TCL_INCLUDE_FLAGS '-I/usr/local/ultra/include -L/usr/local/ultra/lib'
else 
    echo $MACHINE | grep linux >& /dev/null
    if (! $status) then
      setenv TCL_INCLUDE_FLAGS '-L/usr/X11R6/lib'
    endif
endif

# shared directories
if (! $?SRCDIR) setenv SRCDIR "$SUIFHOME/src"
if (! $?INFODIR) setenv INFODIR "$SUIFHOME/info"
if (! $?DOCDIR) setenv DOCDIR "$SUIFHOME/docs"

if (! $?HTMLDIR) setenv HTMLDIR "$SUIFHOME/html"
if (! $?MANDIR) setenv MANDIR  "$SUIFHOME/man"
if (! $?SCRIPTDIR) setenv SCRIPTDIR "$SUIFHOME/scripts"
if (! $?TCLDIR) setenv TCLDIR "$SUIFHOME/tcl"

# machine-specific directories
if (! $?MACHDIR) setenv MACHDIR "$SUIFHOME/$MACHINE"
if (! $?INCLDIR) setenv INCLDIR "$MACHDIR/include"
if (! $?BINDIR) setenv BINDIR "$MACHDIR/bin"
if (! $?LIBDIR) setenv LIBDIR "$MACHDIR/lib"
if (! $?SODIR) setenv SODIR "$MACHDIR/solib"
if (! $?AUTOINITDIR) setenv AUTOINITDIR "$MACHDIR/auto_init_libs"
if (! $?VERDATADIR) setenv VERDATADIR "$MACHDIR/verdata"

# different name is used by vbrowser
setenv VISUAL_TCL $TCLDIR

# purify stuff
if (! $?PUREHOME) setenv PUREHOME '/usr/local/pure'

setenv PURE "$PUREHOME/purify"
if ($?PURE_COLLECTOR) then
   setenv PURE_FLAGS "-collector=$PURE_COLLECTOR"
endif

#
while ($#envvars)
    set var=${envvars[1]}
    shift envvars
    set val=`printenv $var`
    if (! $status) then
	if (x$flag == 'x-csh') then
	    echo "setenv $var '$val'"
	else if (x$flag == 'x-sh') then
	    echo "export $var; $var='$val';"
	else if (x$flag == 'x-perl') then
	    echo '$'"ENV{$var}='"$val"';"
        endif
    endif
end
if ($limits_off) then
    echo "unlimit datasize;\n";
endif
