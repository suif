#define _MODULE_ "libipmath.a"


#include <stdio.h>
#include <suif1.h>
#include <suifmath.h>
#include <useful.h>
#include <builder.h>
#include <ist.h>
#include "ipmath.h"


named_symcoeff_ineq * remap(named_symcoeff_ineq syst, in_cal * inc, boolean up)
{
    named_symcoeff_ineq ret;


    for(int im=0; im<syst.m(); im++) {
	named_symcoeff_ineq curr(syst.ineq(im));
	instruction * ins = curr.create_expression();
	operand op;
	if(!up)
	    op = IST->up_from_call(operand(ins), inc);
	else
	    op = IST->down_through_call(operand(ins), inc);
	named_symcoeff_ineq * map = named_symcoeff_ineq::convert_exp(op);
	if (op.is_instr()) delete op.instr();
	delete ins;
	
	if(map == NULL) return NULL;
	assert(map->m() == 1);
	ret &= *map;
	delete map;
    }
    ret.cleanup();
    return new named_symcoeff_ineq(ret);
}

    
	
named_lin_ineq * remap(const named_lin_ineq & syst, in_cal * inc, boolean up)
{
    named_symcoeff_ineq ns(syst);
    named_symcoeff_ineq * ret = remap(ns, inc, up);
    if(ret == NULL) return NULL;
    if(ret->p() > 1) {
	delete ret;
	return NULL;
    }

    lin_ineq * pln = ret->get_p(0);
    named_lin_ineq * lr = new named_lin_ineq(ret->cols(), *pln);
    delete pln;
    delete ret;
    return lr;
}
