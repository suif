#define _MODULE_ "libipmath.a"
#pragma implementation "codegen.h"

#include <stdio.h>
#include <suif1.h>
#include <suifmath.h>
#include <useful.h>
#include <builder.h>
#include <ist.h>
#include "ipmath.h"

const char * code_gen_copy_array_region_annote::k_annote;
const char * code_gen_initialize_annote::k_annote;
const char * code_gen_finalize_annote::k_annote;

/******************************************************
 ******************************************************
 ******************************************************/
void * code_gen_copy_array_region_annote_from(const char *name, immed_list *il, suif_object *)
{
    code_gen_copy_array_region_annote * ret = new code_gen_copy_array_region_annote;
    
    assert(name == code_gen_copy_array_region_annote::k_annote);
    assert(il);
    ret->init(il);
    return (void *)ret;
}


immed_list * code_gen_copy_array_region_annote_to(const char *name, void *data)
{
    assert(name == code_gen_copy_array_region_annote::k_annote);
    assert(data);
    code_gen_copy_array_region_annote * cgcara = (code_gen_copy_array_region_annote *) data;

    immed_list * im = cgcara->create();
    return im;
}

void code_gen_copy_array_region_annote_free(void *data)
{
    assert(data);
    code_gen_copy_array_region_annote * cgcara = (code_gen_copy_array_region_annote *) data;
    delete cgcara;
}

    
void code_gen_copy_array_region_annote_print(FILE * fp, const char * /* name */,
                                             void *data)
{
    assert(data);
    code_gen_copy_array_region_annote * cgcara = (code_gen_copy_array_region_annote *) data;
    cgcara->print(fp);
}


code_gen_copy_array_region_annote::code_gen_copy_array_region_annote()
{
    varf = NULL;
    vart = NULL;
}
    

void code_gen_copy_array_region_annote::init(immed_list *il)
{
    assert(il);
    assert(il->count() >= 3);
    immed im_varf((*il)[0]);
    immed im_vart((*il)[1]);
    varf = (var_sym *)(im_varf.symbol());
    vart = (var_sym *)(im_vart.symbol());
    region.init(*il, 2);
}

immed_list * code_gen_copy_array_region_annote::create()
{
    immed_list * im = new immed_list;
    im->append(immed(vart));
    im->append(immed(varf));
    immed_list * rg = region.cvt_immed_list();
    im->append(rg);
    return im;
}

void code_gen_copy_array_region_annote::print(FILE *fp)
{
    fprintf(fp, "from:%s to %s\n", (varf)?varf->name():"<null>", (vart)?vart->name():"<null>");
    region.print(fp);
}

tree_node_list * code_gen_copy_array_region_annote::generate_code(base_symtab * bs)
{
    return do_generate_code(region, NULL, varf, vart, bs);
}

/******************************************************
 ******************************************************
 ******************************************************/
void * code_gen_initialize_annote_from(const char *name, immed_list *il, suif_object *)
{
    code_gen_initialize_annote * ret = new code_gen_initialize_annote;
    
    assert(name == code_gen_initialize_annote::k_annote);
    assert(il);
    ret->init(il);
    return (void *)ret;
}


immed_list * code_gen_initialize_annote_to(const char *name, void *data)
{
    assert(name == code_gen_initialize_annote::k_annote);
    assert(data);
    code_gen_initialize_annote * cgia = (code_gen_initialize_annote *) data;

    immed_list * im = cgia->create();
    return im;
}

void code_gen_initialize_annote_free(void *data)
{
    assert(data);
    code_gen_initialize_annote * cgia = (code_gen_initialize_annote *) data;
    delete cgia;
}

    
void code_gen_initialize_annote_print(FILE * fp, const char * /* name */, void *data)
{
    assert(data);
    code_gen_initialize_annote * cgia = (code_gen_initialize_annote *) data;
    cgia->print(fp);
}


code_gen_initialize_annote::code_gen_initialize_annote()
{
    array = NULL;
    varf = NULL;
    vart = NULL;
    nregion = 0;
    ndim = 0;
    int i;
    for(i=0; i<MAXNREGION; i++) region[i] = NULL;
    for(i=0; i<8; i++) adim[i] = NULL;
}

code_gen_initialize_annote::code_gen_initialize_annote(var_sym * ar)
{
    array = ar;
    varf = NULL;
    vart = NULL;
    nregion = 0;
    ndim = 0;
    int i;
    for(i=0; i<MAXNREGION; i++) region[i] = NULL;
    for(i=0; i<8; i++) adim[i] = NULL;
}


code_gen_initialize_annote::~code_gen_initialize_annote()
{
    for(int i=0; i<nregion; i++) delete region[i];
}


void code_gen_initialize_annote::init(immed_list *il)
{
    assert(il);
    assert(il->count() >= 5);

    immed im_ar((*il)[0]);
    array = (var_sym *)(im_ar.symbol());

    immed im_nr((*il)[1]);
    assert(im_nr.is_integer());
    nregion = im_nr.integer();

    immed im_nr2((*il)[2]);
    assert(im_nr2.is_integer());
    ndim = im_nr2.integer();

    immed im_nr3((*il)[3]);
    vart = (var_sym *)(im_nr3.symbol());

    immed im_nr4((*il)[4]);
    varf = (var_sym *)(im_nr4.symbol());
    
    int curr = 5;
    int i;
    for(i=0; i<ndim; i++) {
	immed im_nr3((*il)[curr++]);
	assert(im_nr3.is_symbol());
	adim[i] = (var_sym *)im_nr3.symbol();
    }
    
    for(i=0; i<nregion; i++) {
	region[i] = new named_lin_ineq;
	curr = region[i]->init(*il, curr);
    }
}

immed_list * code_gen_initialize_annote::create()
{
    immed_list * im = new immed_list;
    im->append(immed(array));
    im->append(immed(nregion));
    im->append(immed(ndim));
    im->append(immed(vart));
    im->append(immed(varf));
    int i;
    for(i=0; i<ndim; i++) {
	im->append(adim[i]);
    }
    for(i=0; i<nregion; i++) {
	immed_list * rg = region[i]->cvt_immed_list();
	im->append(rg);
    }
    return im;
}

void code_gen_initialize_annote::print(FILE *fp)
{
    fprintf(fp, "%s: from:%s to %s   ", array->name(), (varf)?varf->name():"<null>", (vart)?vart->name():"<null>");
    int i;
    for(i=0; i<ndim; i++)
	fprintf(fp, "%s ", adim[i]->name());
    fprintf(fp, "\n");
    for(i=0; i<nregion; i++) {
	region[i]->print(fp);
    }
}

void code_gen_initialize_annote::add_region(named_lin_ineq & r)
{
    if(nregion == 0) set_dimnames();
    assert(nregion+1<MAXNREGION);
    region[nregion] = new named_lin_ineq(r);
    map_dimnames(region[nregion]);
    nregion++;
}

extern code_gen_initialize_annote * IN;

tree_node_list * code_gen_initialize_annote::generate_code(base_symtab * bs)
{
    rename_dimnames();
    tree_node_list * tnl;
    IN = this;
    tnl = do_generate_code(region, nregion, array, varf, vart, bs);
    IN = NULL;
    return tnl;
}


array_type * get_a_type(type_node * t);


void code_gen_initialize_annote::set_dimnames()
{
    array_type * at = get_a_type(array->type());
    assert(at);
    
    ndim = numdim(at);
    for(int i=0; i<ndim; i++) {
	char nm[64];
	sprintf(nm, "_init_%sd%d",array->name(), i);
	block *garbage = &block::new_sym(cst_int, nm);
	block newvs(garbage);
	adim[i] = (var_sym *)newvs.get_sym();
	delete garbage;
    }
}


void code_gen_initialize_annote::map_dimnames(named_lin_ineq * c)
{
    for(int i=0; i<ndim; i++) {
	var_sym * vsi = get_dim(array, i);
	var_sym * repl = adim[i];
	assert(vsi);
	assert(repl);
	int pos = c->find(vsi);
	if (pos>0)
	    c->names()[pos].set_name(immed(repl));
    }
}



void code_gen_initialize_annote::rename_dimnames()
{
    var_sym * olddim[8];

    for(int i=0; i<ndim; i++) {
	block newvs(block::new_sym(cst_int, adim[i]->name()));
	olddim[i] = adim[i];
	adim[i] = (var_sym *)newvs.get_sym();
    }

    for(int r=0; r<nregion; r++) {
	named_lin_ineq * c = region[r];
	int pos;
	for(int i=0; i<ndim; i++)
	if((pos = c->find(immed(olddim[i]))) > 0) 
	    c->names()[pos].set_name(immed(adim[i]));
    }
}

		
	    


/******************************************************
 ******************************************************
 ******************************************************/
void * code_gen_finalize_annote_from(const char *name, immed_list *il, suif_object *)
{
    code_gen_finalize_annote * ret = new code_gen_finalize_annote;
    
    assert(name == code_gen_finalize_annote::k_annote);
    assert(il);
    ret->init(il);
    return (void *)ret;
}


immed_list * code_gen_finalize_annote_to(const char *name, void *data)
{
    assert(name == code_gen_finalize_annote::k_annote);
    assert(data);
    code_gen_finalize_annote * cgfa = (code_gen_finalize_annote *) data;

    immed_list * im = cgfa->create();
    return im;
}

void code_gen_finalize_annote_free(void *data)
{
    assert(data);
    code_gen_finalize_annote * cgfa = (code_gen_finalize_annote *) data;
    delete cgfa;
}

    
void code_gen_finalize_annote_print(FILE * fp, const char * /* name */, void *data)
{
    assert(data);
    code_gen_finalize_annote * cgfa = (code_gen_finalize_annote *) data;
    cgfa->print(fp);
}


code_gen_finalize_annote::code_gen_finalize_annote()
{
    array = NULL;
    nosave_var1 = NULL;
    nosave_var2 = NULL;
    nosave_var3 = NULL;
}

code_gen_finalize_annote::code_gen_finalize_annote(var_sym * ar)
{
    array = ar;
    nosave_var1 = NULL;
    nosave_var2 = NULL;
    nosave_var3 = NULL;
}
    

void code_gen_finalize_annote::init(immed_list *il)
{
    assert(il);

    assert((il->count() == 4) || (il->count() == 1));

    immed im_ar((*il)[0]);
    array = (var_sym *)im_ar.symbol();

    if (il->count() == 1)
        return;

    immed im_ns1((*il)[1]);
    nosave_var1 = (var_sym *)im_ns1.symbol();

    immed im_ns2((*il)[2]);
    nosave_var2 = (var_sym *)im_ns2.symbol();

    immed im_ns3((*il)[3]);
    nosave_var3 = (var_sym *)im_ns3.symbol();
}

immed_list * code_gen_finalize_annote::create()
{
    immed_list * im = new immed_list;
    im->append(immed(array));
    im->append(immed(nosave_var1));
    im->append(immed(nosave_var2));
    im->append(immed(nosave_var3));
    return im;
}

void code_gen_finalize_annote::print(FILE *fp)
{
    fprintf(fp, "finalize %s (%s %s %s)", 
	    array->name(),
	    (nosave_var1)?nosave_var1->name():"<null>",
	    (nosave_var2)?nosave_var2->name():"<null>",
	    (nosave_var3)?nosave_var3->name():"<null>");

}


