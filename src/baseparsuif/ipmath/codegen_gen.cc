#define _MODULE_ "libipmath.a"

#include <stdio.h>
#include <suif1.h>
#include <suifmath.h>
#include <builder.h>
#include <useful.h>
#include <ist.h>
#include "ipmath.h"

code_gen_initialize_annote * IN = 0;
    
array_type * get_a_type(type_node * t)
{
    if(t->unqual()->is_array())
	return (array_type *)t->unqual();
    if(t->unqual()->is_ptr())
	return get_a_type(((ptr_type *)t->unqual())->ref_type());
    return NULL;
}

static array_type * get_a_type(var_sym * a)
{
    return get_a_type(a->type());
}


boolean is_in_dim(var_sym * dimv, var_sym * a)
{
    array_type * at = get_a_type(a);
    if(at == NULL) return FALSE;
    
    for(int i=0; i< IN->ndim; i++)
	if(IN->adim[i] == dimv) return TRUE;
    return FALSE;
}

void arrange_dims(named_lin_ineq & c, var_sym * a)
{
    array_type * at = get_a_type(a);
    assert(at);
    int n = IN->ndim;
    int pos = c.n()-1;
    for(int i=n-1; i>=0; i--) {
	var_sym * dim = IN->adim[i];
	int x = c.find(dim);
	assert(x>0);
	if(x != pos)
	    c.swap_col(x, pos);
	pos--;
    }
}



tree_node_list * do_generate_code(named_lin_ineq * c[], int nc, var_sym * ar,  var_sym * varf, var_sym * vart, base_symtab * bs)
{
    if(nc == 0)
	return do_generate_code(varf, vart, bs);
    if(nc == 1)
	return do_generate_code(*c[0], ar, varf, vart, bs);
    else if(nc>1) {
	block bk;
	for(int i=0; i<nc; i++) {
	    block curr(do_generate_code(*c[i], ar, varf, vart, bs));
	    if(i==0)
		bk.set(curr);
	    else
		bk.set(block::statement(bk, curr));
	}
	return bk.make_tree_node_list((block_symtab *)bs);
    } else assert(0);
    return NULL;
}
	    



tree_node_list * do_generate_code(named_lin_ineq c, var_sym * ar, var_sym * varf, var_sym * vart, base_symtab * bs)
{
    assert_msg(varf, ("No from"));
    assert_msg(vart, ("No to"));

    array_type * tpf = get_a_type(varf);
    array_type * tpt = get_a_type(vart);
    if(tpf && tpt)
	assert(numdim(tpf) == numdim(tpt));

    array_type * tpar;
    if(ar)
	if((tpar = get_a_type(ar)) != NULL) {
	    if(tpt)
		assert(numdim(tpar) == numdim(tpt));
	    else if(tpf)
		assert(numdim(tpar) == numdim(tpt));
	}
    
    var_sym * dimv;
    int i;
    for(i=1; i<c.n(); i++) 
	if(c.names()[i].kind() == nte_dim) {
	    dimv = c.names()[i].var();
	    break;
	}

    assert(dimv);
    var_sym * ad = NULL;
    if(ar) {
	if(is_in_dim(dimv, ar))
	    ad = ar;
	else {
	    if(is_in_dim(dimv, varf)) ad = varf;
	    if(is_in_dim(dimv, vart)) ad = vart;
	}
    } else {
	if(is_in_dim(dimv, varf)) ad = varf;
	if(is_in_dim(dimv, vart)) ad = vart;
    }
    
    assert(ad);
    arrange_dims(c, ad);

    c.project();
    block * ind[5];
    assert(IN->ndim <= 5);
    for(i=0; i<5; i++) 
	ind[i] = builder::B_NOOP;
    
    for(i=0; i<IN->ndim; i++) {
	ind[i] = new block(IN->adim[i]);
    }
    

    block baset(vart);
    block basef(varf);
    if(!tpt)
	baset.set(baset.dref());
    if(!tpf)
	basef.set(basef.dref());
    
    
    block acct(block::ARRAY(baset, *ind[0], *ind[1], *ind[2], *ind[3], *ind[4]));
    block accf(block::ARRAY(basef, *ind[0], *ind[1], *ind[2], *ind[3], *ind[4]));

    block code(acct = accf);
    
    constraint mask(c.n());
    for(i=c.n()-1; i>=1; i--) {
	if(c.names()[i].kind() == nte_dim) {
	    var_sym * vs = c.names()[i].var();
	    mask = 0;
	    mask[i] = 1;
	    named_lin_ineq lb(c.names(), c.ineqs().filter_thru(mask,  1));
	    named_lin_ineq ub(c.names(), c.ineqs().filter_thru(mask, -1));

	    boolean single_iter = FALSE;
	    if((lb.m()==1)&&(ub.m()==1)) {
		lin_ineq luadd(lb.ineqs() + ub.ineqs());
		if(luadd[0][0] == 0) {    // luadd has 0 constant
		    luadd.del_zeros();
		    if(luadd.m() == 0) single_iter = TRUE; // luadd is zero
		}
	    }
	    
	    if(single_iter) {
		instruction * ieq = ub.create_expression(immed(vs), TRUE);
		code.set(block::statement(block(block(vs)=block(ieq)), code));
	    } else {
		instruction * ilb = lb.create_expression(immed(vs), FALSE);
		instruction * iub = ub.create_expression(immed(vs), TRUE);
		code.set(block::FOR(block(vs), block(ilb), block(iub), code));
	    }
	}
    }
    tree_node_list * tnl = code.make_tree_node_list((block_symtab *)bs);
    tnl->print(stdout);
    return tnl;
}

/* Create initialization code for scalar variables */
tree_node_list * do_generate_code(var_sym * varf, var_sym * vart, base_symtab * bs)
{
    assert_msg(varf, ("No from"));
    assert_msg(vart, ("No to"));

    block accf(varf);
    block acct(vart);

    /* dereference source variable if it is a pointer and dest is not */
    if (varf->type()->is_ptr() && !varf->type()->is_ptr())
        accf.set(accf.dref());

    block code(vart = varf);
    tree_node_list * tnl = code.make_tree_node_list((block_symtab *)bs);
    tnl->print(stdout);
    return tnl;
}
