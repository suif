#pragma interface

#define MAXNREGION     32
/**************************************************************************
 ***                                                                    ***
 *** 
 ***                                                                    ***
 **************************************************************************/
struct code_gen_copy_array_region_annote {
    static const char * k_annote;
    named_lin_ineq region;
    var_sym * varf;
    var_sym * vart;

    code_gen_copy_array_region_annote();
    

    void set_from(var_sym * v)                     { varf = v; }
    void set_to(var_sym * v)                       { vart = v; }
    void set_region(named_lin_ineq & c)            { region = c; }
    tree_node_list * generate_code(base_symtab *);

    void init(immed_list * il);
    immed_list * create();    
    void print(FILE *fp = stdout);
};
 
void * code_gen_copy_array_region_annote_from(const char *name, immed_list *il, suif_object *);
immed_list * code_gen_copy_array_region_annote_to(const char *name, void *data);
void code_gen_copy_array_region_annote_free(void *data);
void code_gen_copy_array_region_annote_print(FILE * fp, const char * nm, void *data);




/**************************************************************************
 ***                                                                    ***
 *** 
 ***                                                                    ***
 **************************************************************************/
struct code_gen_initialize_annote {
    static const char * k_annote;
    var_sym * array;
    int nregion;
    int ndim;
    var_sym * adim[8];
    named_lin_ineq * region[MAXNREGION];


    

    var_sym * vart;
    var_sym * varf;


    code_gen_initialize_annote();
    ~code_gen_initialize_annote();
    code_gen_initialize_annote(var_sym * ar);

    void set_from(var_sym * v)                     { varf = v; }
    void set_to(var_sym * v)                       { vart = v; }
    void add_region(named_lin_ineq & r);
    tree_node_list * generate_code(base_symtab *);

    void init(immed_list * il);
    immed_list * create();    
    void print(FILE *fp = stdout);
    void set_dimnames();
    void map_dimnames(named_lin_ineq *);
    void rename_dimnames();
};


void * code_gen_initialize_annote_from(const char *name, immed_list *il, suif_object *);
immed_list * code_gen_initialize_annote_to(const char *name, void *data);
void code_gen_initialize_annote_free(void *data);
void code_gen_initialize_annote_print(FILE * fp, const char * nm, void *data);




/**************************************************************************
 ***                                                                    ***
 *** 
 ***                                                                    ***
 **************************************************************************/
struct code_gen_finalize_annote {
    static const char * k_annote;
    var_sym * array;

    var_sym * nosave_var1;
    var_sym * nosave_var2;
    var_sym * nosave_var3;

    code_gen_finalize_annote();
    code_gen_finalize_annote(var_sym * ar);

    void init(immed_list * il);
    immed_list * create();
    void print(FILE *fp = stdout);
};


void * code_gen_finalize_annote_from(const char *name, immed_list *il, suif_object *);
immed_list * code_gen_finalize_annote_to(const char *name, void *data);
void code_gen_finalize_annote_free(void *data);
void code_gen_finalize_annote_print(FILE * fp, const char * nm, void *data);


tree_node_list * do_generate_code(named_lin_ineq * c[], int nc, var_sym * ar, var_sym * from, var_sym * to, base_symtab *bs);
tree_node_list * do_generate_code(named_lin_ineq c, var_sym * ar, var_sym * from, var_sym * to,base_symtab *);
tree_node_list * do_generate_code(var_sym * from, var_sym * to, base_symtab *);






