#define _MODULE_ "libipmath.a"
#pragma implementation "reshape.h"

#include <stdio.h>
#include <suif1.h>
#include <suifmath.h>
#include <useful.h>
#include <builder.h>
#include <ist.h>
#include "ipmath.h"


boolean debug_ipmath_reshape = FALSE;

void vars_up_from_call(named_lin_ineq * nli, in_cal * ic);
void vars_down_from_call(named_lin_ineq * nli, in_cal * ic);
void vars_map(named_lin_ineq * nli, in_cal * ic, boolean direction);


var_sym  * array_remap_name(in_cal  * inc, boolean up, var_sym * vs)
{
    in_ldc *new_ldc = new in_ldc(vs->type()->ptr_to(), operand(), immed(vs));
    operand op;
    if(!up)
	op = IST->up_from_call(operand(new_ldc), inc);
    else
	op = IST->down_through_call(operand(new_ldc), inc);

    delete new_ldc;

    sym_node *new_sym = operand_address_root_symbol(op);

    if (op.is_expr())
        delete op.instr();

    if (new_sym == NULL) return NULL;

    assert(new_sym->is_var());
    return (var_sym *)new_sym;
}


sym_addr * array_remap_name(in_cal * /* inc */, boolean /* up */, sym_addr * /* sa */, array_type * /* at */)
{
// BUGBUG
    assert(0);
    return 0;
}


static int find_param_pos(proc_symtab * pst, var_sym * param)
{
    for(int i=0; i < pst->params()->count(); i++) {
	sym_node * sn = (*pst->params())[i];
	if(sn == param) return i;
    }
    return  -1;
}



named_lin_ineq * array_remap(const named_lin_ineq & ctxt, in_cal * inc, boolean up, var_sym * callee, boolean * exact)
{
    assert(inc);
    assert(callee);

//    printf("Remapping.... %s  insym=%s\n", up?"up":"down", insym->name());
    
    
    var_sym * caller = array_remap_name(inc, FALSE, callee);
    if(caller == callee) return new named_lin_ineq(ctxt);   // global variable

    var_sym * formal = callee;

    if(formal->is_param()) {                               // parameter
	assert(callee->parent()->is_proc());
	proc_symtab * procsym = (proc_symtab *)callee->parent();
	proc_sym * ps  = ((tree_proc *)procsym->block())->proc();
	sym_node * ps2 = operand_address_root_symbol(inc->addr_op());
	assert(ps == ps2);
	
	int pos = find_param_pos(procsym, formal);
	assert(pos >= 0);

	operand arg(inc->argument(pos));
	assert(arg.type()->is_ptr());
	while (arg.is_instr() &&
	       ((arg.instr()->opcode() == io_cpy) ||
		(arg.instr()->opcode() == io_cvt))) {
	    in_rrr *the_rrr = (in_rrr *)(arg.instr());
	    arg = the_rrr->src_op();
	    assert(arg.type()->is_ptr());
	}
	named_lin_ineq * ret;
	if(arg.is_symbol()) {
	    assert(arg.symbol()->is_var());
	    if(!up) {
		ret = array_up_from_call((var_sym *)arg.symbol(), formal, ctxt, exact);
		vars_up_from_call(ret, inc);
	    } else {
		ret = array_down_from_call((var_sym *)arg.symbol(), formal, ctxt, exact);
		vars_down_from_call(ret, inc);
	    }
	    return ret;
	} else if(arg.is_instr()) {
	    if(arg.instr()->opcode() == io_array) {
		in_array * ina = (in_array *)arg.instr();
		if(!up) {
		    ret = array_up_from_call(ina, formal, ctxt, exact);
		    vars_up_from_call(ret, inc);
		} else {
		    ret = array_down_from_call(ina, formal, ctxt, exact);
		    vars_down_from_call(ret, inc);
		}
		return ret;
	    } else if(arg.instr()->opcode() == io_ldc) {
		in_ldc * ldc = (in_ldc *)arg.instr();
		if(!up) {
		    ret = array_up_from_call((var_sym *)ldc->value().symbol(), formal, ctxt, exact);
		    vars_up_from_call(ret, inc);
		} else  {
		    ret = array_down_from_call((var_sym *)ldc->value().symbol(), formal, ctxt, exact);
		    vars_down_from_call(ret, inc);
		}
		return ret;
	    } else
		assert(0);
	} else
	    assert(0);
    } else
	return NULL;                                       // local variable
    return NULL;
}


void vars_up_from_call(named_lin_ineq * nli, in_cal * ic)
{
    vars_map(nli, ic, FALSE);
}

void vars_down_from_call(named_lin_ineq * nli, in_cal * ic)
{
    vars_map(nli, ic, TRUE);
}

void vars_map(named_lin_ineq * nli, in_cal * ic, boolean direction)
{
    if(nli == NULL) return;
    for(int i=1; i<nli->n(); i++) {
	name_table_entry_kind k = nli->names()[i].kind();
	if ((k != nte_summary) && (k != nte_aux)) {
	    var_sym * vin = nli->names()[i].var();
	    var_sym * vout = scalar_remap_name(ic, direction, vin);
	    if(vout && (vout != vin))
		nli->names()[i].init(vout);
	}
    }
}

var_sym *
scalar_remap_name (in_cal *inc, boolean direction, var_sym *v) {
   operand op;
    if(!direction)
	op = IST->up_from_call(operand(v), inc);
    else
	op = IST->down_through_call(operand(v), inc);

    sym_node *new_sym;
    if (op.is_symbol()) 
        new_sym = op.symbol();
    else
        new_sym = operand_address_root_symbol(op);

    if (new_sym == NULL) return NULL;

    assert(new_sym->is_var());
    return (var_sym *)new_sym;
} 

boolean is_placeholder(var_sym *v) {
  if (IST->is_ist_placeholder(v)) return TRUE;
  return FALSE;
}









