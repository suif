#define _MODULE_ "libipmath.a"

#include <stdio.h>
#include <suif1.h>
#include <suifmath.h>
#include <useful.h>
#include <builder.h>
#include <ist.h>
#include "ipmath.h"

array_bound (*better_bound_f)(array_bound) = NULL;

/*************************************************************************
 * initialize                                                            *
 *************************************************************************/
bound::bound()
{
    const_bound = TRUE;
    val.cnst = 0;
}

bound::bound(const bound & b)
{
  if(b.const_bound)
    val.cnst = b.val.cnst;
  else
    val.var = b.val.var;
  const_bound = b.const_bound;
}


/*************************************************************************
 * set the bound given by the operand op.                                *
 *************************************************************************/
void bound::set(const operand & op, int & too_messy)
{
    const_bound = TRUE;
    if(op.is_symbol()) {
	val.var = (var_sym *)op.symbol();
	const_bound = FALSE;
    } else {
	if(op.is_instr()) {
	    // interpret the instruction
	    named_symcoeff_ineq * ineq = named_symcoeff_ineq::convert_exp(op);
	    if(ineq) {
		ineq->cleanup();
		if((ineq->m() == 1)&&(ineq->p() == 1)) {
		    if(ineq->n() == 1) {  // just a constant
			const_bound = TRUE;
			val.cnst = (*ineq)[0][0][0];
		    } else if((ineq->n() == 2)&&
			((*ineq)[0][0][0] == 0)&&
			((*ineq)[0][0][1] == 1)) { // just a variable
			const_bound = FALSE;
			val.var = ineq->cols()[1].var();
		    } else
			too_messy = TRUE;
		} else
		    too_messy = TRUE;
		delete ineq;
	    } else
		too_messy = TRUE;
	} else
	    too_messy = TRUE;
    }
}



/*************************************************************************
 * set the bound given by the array_bound                                *
 *************************************************************************/
void bound::set(const array_bound & ab, int & too_messy)
{
    array_bound x = (better_bound_f)?((*better_bound_f)(ab)):ab;
    
    const_bound = TRUE;
    if(x.is_constant())
	val.cnst = x.constant();
    else if(x.is_variable()) {
	const_bound = FALSE;
	val.var = x.variable();
    } else
	too_messy = TRUE;
}


/*************************************************************************
 * Create expression of the bound                                        *
 *************************************************************************/
named_symcoeff_ineq bound::get()
{
    named_symcoeff_ineq ret;
    if(const_bound) {
	ret.init(1,1,1);
	ret[0][0][0] = val.cnst;
    } else {
	ret.init(1,1,2);
	immed im(val.var);
	name_table_entry nte(nte_symconst, im); 
	name_table nt;
	nt.insert(nte,1);
	ret.cols().init(nt);
	ret[0][0][1] = 1;
    }
    return ret;
}


/*************************************************************************
 * Is two bounds the same?                                               *
 *************************************************************************/
boolean bound::operator==(const bound & a)
{
    if(const_bound != a.const_bound) return FALSE;
    if(const_bound)
	return (val.cnst == a.val.cnst);
    else
	return (val.var == a.val.var);
}


/*************************************************************************
 * print the darn thing.                                                 *
 *************************************************************************/
void bound::print(FILE * fp)
{
    if(const_bound)
      fprintf(fp, "%d", val.cnst);
    else
      fprintf(fp, "%s", val.var->name());
}


/* ###############################################
   #####   dim_info                          #####
   ############################################### */


dim_info::dim_info(dim_info & di):lb(di.lb),ub(di.ub),av(di.av)
{
  sig = di.sig;
}

/*************************************************************************
 * Return number of elements if known, otherwise :) :)                   *
 *************************************************************************/
int dim_info::num_elem()
{
    if(lb.const_bound && ub.const_bound)
	return ub.val.cnst - lb.val.cnst + 1;
    return -1;
}


/* ###############################################
   #####   array_dim_info                    #####
   ############################################### */

/*************************************************************************
 * initialize array info using an array instruction                      *
 *************************************************************************/
array_dim_info::array_dim_info(in_array * ia)
{
    assert(ia);
    init(get_sym_of_array(ia));
    add(ia);
}


/*************************************************************************
 * 
 *************************************************************************/
array_dim_info::array_dim_info(var_sym * vs)
{
    assert(vs);
    init(vs);
}

array_dim_info::array_dim_info(var_sym * vs, array_type * at)
{
    assert(vs);
    init(vs, at);
}

array_dim_info::array_dim_info(array_dim_info & adi)
{
  var = adi.var;
  ndim = adi.ndim;
  elemsize = adi.elemsize;
  too_messy = adi.too_messy;
  no_outer_dim = adi.no_outer_dim;
  
  array_dim_info_iter iter(&adi);
  while(!iter.is_empty()) {
    dim_info * di = iter.step();
    dim_info * ndi = new dim_info(*di);
    append(ndi);
  }
}
    


/*************************************************************************
 * Add the access function to the information                            *
 *************************************************************************/
void array_dim_info::add(in_array * ia)
{
    too_messy = FALSE;
    no_outer_dim = FALSE;
    assert(ia);
    assert(var == get_sym_of_array(ia));
    
    assert(var);

    // BIGBUGBUG: When a complex number is used as complex
    // the 3rd dimension is not in the access
    // COMPLEX A(100)
    // REAL(A(10)) becomes A(0, 10) but
    // A(10)       is still A(10)
    // thus access has 1 less dimension than the array, pad the first.

    int st = 0;
    if(((unsigned)num_dim()) == ia->dims() + 1) {
	named_symcoeff_ineq nsi;
	nsi.init(1, 1, 1);
	nsi[0][0][0] = 0;
	(*this)[0]->av = nsi;
	st = 1;
    } else
	assert(((unsigned)num_dim()) == ia->dims());

    for(int i=st; i < ndim; i++) {
	dim_info * di = (*this)[i];
	named_symcoeff_ineq * ineq = named_symcoeff_ineq::convert_exp(ia->index(ndim-i-1));
        if(ineq) {
           di->av.init(ineq);
	   delete ineq;
	} else
           too_messy = TRUE;
    }
}


/*************************************************************************
 * Initialize using the type information of the array variable           *
 *************************************************************************/
void array_dim_info::init(var_sym * vs, array_type * at)
{
    assert(vs);
    too_messy = FALSE;
    no_outer_dim = FALSE;
    if(at == NULL) {
	//BUGBUG: So this will work with fortran compiled in not-fortran form
	if(vs->type()->unqual()->is_ptr()) {
	    assert(((ptr_type *)vs->type()->unqual())->ref_type()->is_array());
	    at = (array_type *)((ptr_type *)vs->type()->unqual())->ref_type();
	} else {
	    assert(vs->type()->unqual()->is_array());
	    at = (array_type *)vs->type()->unqual();
	}
    }

    type_node * etn = get_element_type(at);
    assert(etn);
    elemsize = etn->size();

    ndim = numdim(at);
    var = vs;
    for(int i=0; i<ndim; i++) {
        assert(at);
        assert(at->is_array());
	dim_info * di = new dim_info;
        di->lb.set(at->lower_bound(), too_messy);
	di->ub.set(at->upper_bound(), too_messy);
	// outer dimension can be too messy, it is ok with us.
	if((i==0)&&(too_messy)) {
	    too_messy = FALSE;
	    di->lb.set(1);
            di->ub.set(-1);
/*	    di->ub.set(10000); */
	    no_outer_dim = TRUE;
	}
	di->sig = get_dim(var, i);
	assert(di->sig);
	push(di);
        at = (array_type *)at->elem_type()->unqual();
    }
}


/*************************************************************************
 * delete                                                                *
 *************************************************************************/
array_dim_info::~array_dim_info()
{
    array_dim_info_iter iter(this);
    while(!iter.is_empty()) {
	dim_info * di = iter.step();
	delete di;
    }
}


/*************************************************************************
 * 
 *************************************************************************/
void dim_info::print(FILE * fp)
{
    fprintf(fp, "%s:", sig->name());
    fprintf(fp, "<");
    lb.print(fp);
    fprintf(fp, ":");
    ub.print(fp);
    fprintf(fp, ">");

    if(av.m() == 1) {
        fprintf(fp, "(");
        av.print_exp(pet_expr, fp);
        fprintf(fp, ")");
    }
}


/*************************************************************************
 * 
 *************************************************************************/
void array_dim_info::print(FILE * fp)
{
    if(too_messy) {
	fprintf(fp, "%s[<too_messy>]", var->name());
	return;
    }
    
    fprintf(fp, "%s[", var->name());
    int cnt = count();
    for(int i=0; i<cnt; i++) {
	dim_info * di = (*this)[count()-1-i];
	if(no_outer_dim && (i==cnt-1)) 
	  fprintf(fp,"*");
        di->print(fp);
	if(i != count()-1) fprintf(fp, ", ");
    }
    fprintf(fp, "](%d) ", elemsize);
}


void array_dim_info::remove_inner_dim()
{
  assert(ndim>0);
  ndim--;
  dim_info * di = pop();
  int ne = di->num_elem();
  /* I believe this is unnecessary -- MWH 12/97
  if (ne > 0)
      elemsize *= ne;
  else
      too_messy = TRUE;
  */
}

void array_dim_info::remove_outer_dim()
{
  assert(ndim>0);
  ndim--;

  dim_info_e *last_e = this->tail();
  remove(last_e);
}

	    
	    
/*************************************************************************
 * Find the bounding-box of the array space                              *
 *************************************************************************/
named_lin_ineq * array_dim_info::bounding_box(boolean get_access)
{
    named_symcoeff_ineq ret;
    array_dim_info_iter iter(this);
    int cdim = 0;
    int cnt = count();
    while(!iter.is_empty()) {
	dim_info * di = iter.step();
	named_symcoeff_ineq curr;
	
	if(get_access) {
	    if(di->av.m() != 1) return NULL;
	    curr = di->av;
	} else {
	    curr.init(1,1,2);
	    immed im(di->sig);
	    name_table_entry nte(nte_dim, im);
	    name_table nt;
	    nt.insert(nte, 1);
	    curr.cols().init(nt);
	    curr[0][0][1] = 1;
	}
	    
	named_symcoeff_ineq l = sub(curr, di->lb.get());
        named_symcoeff_ineq u, tst;

        if (!(di->ub.const_bound && di->ub.val.cnst == -1)) {	
  	    u = sub(di->ub.get(), curr);
	    tst = sub(u, minus(l));
	}
        else {
            tst.init(l);
	}
         
	tst.cleanup();
        if (debug_ipmath_arrayinfo) {
           
	    fprintf(stderr,"Manipulating bb\ntst: "); tst.print_exp(stderr);
	    fprintf(stderr, "\nl: "); l.print_exp(stderr);
	    fprintf(stderr,"\nu: "); u.print_exp(stderr);
	    fprintf(stderr,"\n");
	}
	if(tst.m()==1)
	    if(!((tst.p()==1)&&(tst.n()==1)&&(tst[0][0][0] == 0))) {
		ret &= l;
		if(!((cdim==cnt-1)&&no_outer_dim)) 
		  ret &= u;
	    }
	cdim++;
    }
    
    if (debug_ipmath_arrayinfo) {
      fprintf(stderr, "Bounding Box\n");
      ret.print_exp(stderr);
    }

    named_lin_ineq *retnli = ret.nli();
    if (retnli) retnli->simplify();
    return retnli;
}


/*************************************************************************
 * Find the bounding-box of the array space                              *
 *************************************************************************/
named_lin_ineq * array_dim_info::inner_dim_bounding_box(int sz)
{
    named_symcoeff_ineq ret;
    array_dim_info_iter iter(this);
    boolean inner =TRUE;

    named_symcoeff_ineq range;
    range.init(1,1,1);
    range[0][0][0] = -(sz-1);
    int cdim = 0;
    int cnt = count();
    while(!iter.is_empty()) {
	dim_info * di = iter.step();
	named_symcoeff_ineq curr;

	named_symcoeff_ineq lb;
	named_symcoeff_ineq ub;

	curr.init(1,1,2);
	immed im(di->sig);
	name_table_entry nte(nte_dim, im);
	name_table nt;
	nt.insert(nte, 1);
	curr.cols().init(nt);
	curr[0][0][1] = 1;
	if(di->av.m() == 1) {
	    lb = sub(curr, di->av);
	    ub = sub(di->av, curr);
	} else {
	    lb = sub(curr, di->lb.get());
	    ub = sub(di->lb.get(), curr);
	} 

	if(inner)
	    ub = sub(ub, range);
	
	ret &= lb;
	if(!((cdim==cnt-1)&&no_outer_dim)) 
	  ret &= ub;
	inner = FALSE;
	cdim++;
    }
    
    if (debug_ipmath_arrayinfo) {
      fprintf(stderr, "Inner Dim Bounding Box (%d)\n", sz);
      ret.print_exp(stderr);
    }
    
    named_lin_ineq *retnli = ret.nli();
    if (retnli) retnli->simplify();
    return retnli;
}


/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * array_dim_info::conservative_assumption()
{
    return bounding_box();
}

/*************************************************************************
 * BUGBUG: Needed this hack to since g++ went crazy with add()           *
 *************************************************************************/
static void my_mul(named_symcoeff_ineq & a, int x)
{
    for(int p=0; p<a.p(); p++)
	for(int m=0; m<a.m(); m++)
	    for(int n=0; n<a.n(); n++)
		a[p][m][n] *= x;
}

/*************************************************************************
 * BUGBUG: compiler error when add() was used, so I defined this for now 
 *************************************************************************/
static named_symcoeff_ineq my_add(named_symcoeff_ineq & a, named_symcoeff_ineq & b)
{
    named_symcoeff_ineq A(a);
    named_symcoeff_ineq B(b);

    A || B;
    assert(A.m() == B.m());

    for(int ip =0; ip < A.p(); ip++)
	A[ip] = A[ip] + B[ip];

    return A;
}


/*************************************************************************
 * Linearize the array 
 *************************************************************************/
named_lin_ineq * array_dim_info::linearize_expr(boolean sub_array)
{
    int stride = 1;
    named_symcoeff_ineq linear;
    linear.init(1,1,1);

    array_dim_info_iter iter(this);

    while(!iter.is_empty()) {
	dim_info * di = iter.step();
	
	named_symcoeff_ineq curr;
	curr.init(1,1,2);
	immed im(di->sig);
	name_table_entry nte(nte_dim, im);
	name_table ntc;
	ntc.insert(nte, 1);
        curr.cols().init(ntc);
	curr[0][0][1] = 1;


	if(di->av.m() == 1){
	    named_symcoeff_ineq cav(di->av);
	    curr = sub(curr, cav);
fprintf(stderr,"newcurr = \n"); curr.print(stderr);

	} else
	    curr = sub(curr, di->lb.get());


	if(stride == -1) return NULL;

        // Introduce equality relation if formal is subarray of actual.
        // For simplicity, only if actual is a single dimension.   
        // I would prefer to do it for more dimensions, but don't want to break the code.-- MWH-1/98
        if (sub_array && num_dim() == 1 && di->av.m() == 1) {
            linear.init(1,2,1);
            named_symcoeff_ineq tcurr(curr);
            curr &= minus(tcurr);
        } 

	my_mul(curr, stride);
	linear = my_add(curr, linear);
        fprintf(stderr,"linear = \n"); linear.print(stderr);

	if(di->lb.const_bound && di->ub.const_bound) {
	    int ne = di->num_elem();
	    if (ne > 0)
		stride = stride*ne;
	    else
		stride = -1;
	} else	 
	    stride = -1;

    }

    named_lin_ineq *retnli = linear.nli();
    if (retnli) retnli->simplify();
    return retnli;
}

/*
void temp_test(named_lin_ineq ineq)
{
    lin_ineq L(ineq.ineqs());

    lin_ineq del_list(1, ineq.n());
    del_list[0] = 0;

    // set which are symbolic constants
    for(int a=1; a<L.n(); a++)
        if(ineq.names()[a].kind() == nte_symconst)
            del_list[0][a] = -1;
        else
            del_list[0][a] = 1;
    
    poly_iterator Poly(L);
    Poly.set_sort_order(del_list[0].data_array());
    L = Poly.get_iterator(0);
    L.sort();
    ineq.ineqs().init(L);

    if (debug_ipmath_arrayinfo) {
        printf("Full project\n");
        ineq.print();
    }
}
*/



/*************************************************************************
 * 
 *************************************************************************/
void array_dim_info::project_dim_away(named_lin_ineq & ineq, array_dim_info & away)
{
    
    ineq.simplify();
    name_table nt;
    array_dim_info_iter iter1(&away);
    while(!iter1.is_empty()) {
	dim_info * di = iter1.step();
	if(ineq.names().find(di->sig) > 0) {
	  immed im(di->sig);
	  name_table_entry nte(nte_dim, im);
	  nt.insert(nte, 1);
	}
    }

    if(nt.n() != 0) 
	ineq.project_away(nt);
}




static boolean is_same_val(named_symcoeff_ineq & nsi, bound & b)
{
    if(b.const_bound) {
	if((nsi.p() != 1)||(nsi.m() != 1)) return FALSE;
	return (nsi[0][0][0] == b.val.cnst);
    } else {
	return FALSE;
    }
}


boolean is_same_dim(dim_info & di1, dim_info & di2, boolean chk_ub)
{
  if(chk_ub)
    if(di1.ub != di2.ub) 
      return FALSE;
  if((di1.av.m() == 1)&&(di2.av.m() == 1)) {
    // both are accesses, use them as offsets
    di1.av || di2.av;
    if(!di1.av.is_same(di2.av)) return FALSE;
  } else if(di1.av.m() == 1) {
    // one is an access, compare the access against the lower bound
    if(is_same_val(di1.av, di2.lb) == FALSE) return FALSE;
  } else if(di2.av.m() == 1) {
    if(is_same_val(di2.av, di1.lb) == FALSE) return FALSE;
  } else {
    // compare both lower bounds
    if(di1.lb != di2.lb) return FALSE;
  }
  return TRUE;
}


/*************************************************************************
 * 
 *************************************************************************/
boolean is_same_dim(array_dim_info & adi1, array_dim_info & adi2, boolean chk_last_dim)
{
    if(adi1.num_dim() != adi2.num_dim()) return FALSE;
    if(adi1.elem_size() != adi2.elem_size()) return FALSE;

     for(int i=0; i< adi1.count(); i++) {
       dim_info * di1 = (adi1)[i];
       dim_info * di2 = (adi2)[i];
	// check the upper bound
       boolean chk_ub = (i != adi1.count()-1)||(chk_last_dim);
       if(is_same_dim(*di1, *di2, chk_ub) == FALSE)
	 return FALSE;
     }
     return TRUE;
}



/*************************************************************************
 * 
 *************************************************************************/
boolean is_an_access(array_dim_info & adi)
{
    array_dim_info_iter iter(&adi);
    while(!iter.is_empty()) {
	dim_info * di = iter.step();
	if(di->av.m() > 0) return TRUE;
    }
    return FALSE;
}


/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * reshape(array_dim_info & ac, array_dim_info & fo)
{
    if(ac.too_messy) return NULL;
    if(fo.too_messy) return NULL;

    if (debug_ipmath_arrayinfo) {
      fprintf(stderr, "Dim 1: "); ac.print(stderr);
      fprintf(stderr, "\nDim 2: "); fo.print(stderr); fprintf(stderr, "\n");
    }    

    // check if formal is empty???  This is the case if all dimensions of
    // formal match inner dimensions of actual, 
    // and actual has more dimensions. -- MWH - 12/97
    named_lin_ineq *lin1, *lin2;
    if (fo.num_dim() == 0) {
      lin1 = ac.linearize_expr(TRUE);
      lin2 = new named_lin_ineq();
    }
    else {
      lin1 = ac.linearize_expr();
      lin2 = fo.linearize_expr();
    }
    if (debug_ipmath_arrayinfo) {
      fprintf(stderr,"lin1 = \n");
      lin1 ? lin1->print(stderr) : (void) fprintf(stderr,"<NULL>\n");
      fprintf(stderr,"lin2 = \n");
      lin2 ? lin2->print(stderr) : (void) fprintf(stderr,"<NULL>\n");
    }

    named_lin_ineq * bb1  = ac.bounding_box();
    named_lin_ineq * bb2  = fo.bounding_box();
    named_lin_ineq * bbacc1 = ac.bounding_box(TRUE);
    named_lin_ineq * bbacc2  = fo.bounding_box(TRUE);
    
    if((lin1 == NULL)||(lin2 == NULL)||(bb1 == NULL)||(bb1 == NULL)) {
	if(lin1) delete lin1;
	if(lin2) delete lin2;
	if(bb1) delete bb1;
	if(bb2) delete bb2;
	if (bbacc1) delete bbacc1;
	if (bbacc2) delete bbacc2;
	return NULL;
    }
    
    named_symcoeff_ineq lsc1(lin1); delete lin1;
    named_symcoeff_ineq lsc2(lin2); delete lin2;
    named_symcoeff_ineq bsc1(bb1); delete bb1;
    named_symcoeff_ineq bsc2(bb2); delete bb2;
    named_symcoeff_ineq ret;
    if(ac.elem_size() != fo.elem_size()) {
	int g = gcd(ac.elem_size(), fo.elem_size());
	lsc1 = mul(lsc1, ac.elem_size()/g);
	lsc2 = mul(lsc2, fo.elem_size()/g);
    }
    if (lsc2.m() != 0) {  // don't do this if formal has been eliminated  -- MWH 12/97
        ret = sub(lsc1, lsc2);
        ret &= sub(lsc2, lsc1);
        ret &= bsc1;
    }
    else ret &= lsc1;

    ret &= bsc2;
    if(bbacc1) {
	named_symcoeff_ineq x1(bbacc1);
	ret &= x1;
	delete bbacc1; bbacc1 = 0;
    }
    if(bbacc2) {
	named_symcoeff_ineq x2(bbacc2);
	ret &= x2;
	delete bbacc2; bbacc2 = 0;
    }
    
    ret.cleanup();

    named_lin_ineq *retnli = ret.nli();
    if (retnli) retnli->simplify();
    if (debug_ipmath_arrayinfo) {
      fprintf(stderr,"reshaped:\n"); retnli->print(stderr);
    }
    return retnli;
}


/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * array_map(array_dim_info & actual, array_dim_info & formal, const named_lin_ineq & ctxt)
{
  named_lin_ineq equiv;
  array_dim_info new_actual(actual);
  array_dim_info new_formal(formal);
  // Find the equivalent inner dims and remove them from the reshape
  if(new_actual.elem_size() == new_formal.elem_size()) {
    if (new_formal.num_dim() > new_actual.num_dim()) {
       /* first, delete formal's irrelevant outer dim.  Would          */
       /* like to do more than just last one, but iter goes backwards. */

       /* check on last dim */
       dim_info_e *dif_e = new_formal.tail();
       dim_info *dif = dif_e->contents;

       var_sym *dimvar = dif->sig;
       /* look for dimvar in ctxt */
       int pos = ctxt.find(dimvar);
       if (pos != -1) {
          constraint filter(ctxt.const_ineqs().n());
          filter = 0;
          filter[pos] = 1;
          lin_ineq leq = ctxt.const_ineqs().filter_thru(filter,-1);
          leq *= -1;
          named_lin_ineq *dim_bound 
	      = new named_lin_ineq(ctxt.const_names(),leq);
          dim_bound->simplify();
          dim_bound->print();
          if (dim_bound->ineqs().n() == 2 && dim_bound->ineqs().m() ==1) { /* constant value */
              if (dim_bound->ineqs()[0][0] == -1) {
                  new_formal.remove_outer_dim();
                  ctxt.print();
                  /* ctxt.del_col(pos); */ /* or del_col? */
	      }
	   }
        }
        else fprintf(stderr, "ipmath ERROR: Formal's dim var not part of ctxt.\n");
    }

    boolean both_one_dim = (new_actual.num_dim() == 1 && new_formal.num_dim() == 1) ? TRUE : FALSE;

    int mindim = MIN(new_actual.num_dim(), new_formal.num_dim());
    boolean match = TRUE;
    for(int i=0; (i<mindim)&&match; i++) {
      dim_info * adi = new_actual[0];
      dim_info * fdi = new_formal[0];

      // don't delete dims of both if actual has access vector -- MWH 6/98
      if (both_one_dim && adi->av.m() > 0) both_one_dim = FALSE;
      if(is_same_dim(*adi, *fdi) || both_one_dim) {
	name_table_entry ntea(nte_dim, immed(adi->sig));
	name_table_entry ntef(nte_dim, immed(fdi->sig));
	name_table nt;
	nt.insert(ntea, 1);
	nt.insert(ntef, 1);
	lin_ineq leq = Compose(2, 3,
			       0,  1, -1,
			       0, -1,  1);
	named_lin_ineq curr(nt, leq);
	equiv &= curr;
	
	new_actual.remove_inner_dim();
	new_formal.remove_inner_dim();
      } else 
	match = FALSE;
    }
  }
  
  if (debug_ipmath_arrayinfo) {
    fprintf(stderr, "Actual: "); actual.print(stderr);
    fprintf(stderr, "\nFormal: "); formal.print(stderr);
    fprintf(stderr, "\nNew actual: "); new_actual.print(stderr);
    fprintf(stderr, "\nNew formal: "); new_formal.print(stderr);
    fprintf(stderr, "\nequivalance: \n"); equiv.print(stderr);
  }
  
  named_lin_ineq * re = reshape(new_actual, new_formal);
  if(re == NULL) return NULL;

  if (debug_ipmath_arrayinfo) {
    fprintf(stderr, "\nReshaped: \n"); re->print(stderr);
    fprintf(stderr, "\nContext: \n"); ctxt.print(stderr);
  }

  *re &= ctxt;
  if(equiv.m())
    *re &= equiv;

  if (debug_ipmath_arrayinfo) {
    if(re) { fprintf(stderr, "\nResults: \n"); re->print(stderr); fprintf(stderr, "\n"); }
  }
  return re;
}


/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * rename_dimvars(array_dim_info & to, array_dim_info & from, const named_lin_ineq * ctxt)
{
    assert(is_same_dim(to, from, FALSE));
    named_lin_ineq * ret = new named_lin_ineq(ctxt);
    array_dim_info_iter iter_f(&from);
    array_dim_info_iter iter_t(&to);
    while(!iter_f.is_empty()) {
	dim_info * dif = iter_f.step();
	dim_info * dit = iter_t.step();
	int pos = ret->find(dif->sig);
	if(pos > 0) {
	    ret->names()[pos].init(dit->sig);
	    ret->names()[pos].mark_dim();
	}
    }
    return ret;
}

	
/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * array_map_from_call(array_dim_info & adfrom, array_dim_info & adto, const named_lin_ineq & ctxt, boolean * exact)
{
    if(exact) *exact=TRUE;
    
    if(is_same_dim(adfrom, adto, FALSE))
	return rename_dimvars(adfrom, adto, &ctxt);
    
    named_lin_ineq * map  = array_map(adfrom, adto,  ctxt);
    if(map == NULL) {
	fprintf(stderr, "ipmath: Array context cannot remap, making a conservative assumption\n");
	if(exact) *exact=FALSE;
	return adfrom.conservative_assumption();
    }
    
    if (debug_ipmath_arrayinfo) {
      fprintf(stderr, "array_up_from_call before project:\n"); map->print(stderr);
    }
    if(~map->ineqs() == TRUE) {
	fprintf(stderr, "ipmath: Array context is inconsistant, mapped to empty\n");
	delete map;
	if(exact) *exact=FALSE;
	return adfrom.conservative_assumption();
    }
    adfrom.project_dim_away(*map, adto);

    if (debug_ipmath_arrayinfo) {
      fprintf(stderr, "array_up_from_call output:\n"); map->print(stderr);
    }
    return map;
}

 
/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * array_up_from_call(in_array * actual, var_sym * formal, const named_lin_ineq & ctxt, boolean * exact)
{
//    printf("array_up_from_call input:\n"); ctxt.print();

    array_dim_info ac(actual);
    array_dim_info fo(formal);
    if(ac.too_messy) { 
      if (debug_ipmath_arrayinfo)
	fprintf(stderr, "Actual is too messy\n"); 
      if (exact) *exact = FALSE;
      return ac.conservative_assumption(); 
    }
    if(fo.too_messy) { 
      if (debug_ipmath_arrayinfo)
	fprintf(stderr, "Formal is too messy\n"); 
      if (exact) *exact = FALSE;
      return ac.conservative_assumption(); 
    }

    return array_map_from_call(ac, fo, ctxt, exact);
}


/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * array_up_from_call(var_sym * actual, var_sym * formal, const named_lin_ineq & ctxt, boolean * exact)
{
    array_dim_info ac(actual);
    array_dim_info fo(formal);
    if(ac.too_messy) { 
      if (debug_ipmath_arrayinfo)
	fprintf(stderr, "Actual is too messy\n"); 
      if (exact) *exact = FALSE;
      return ac.conservative_assumption(); 
    }
    if(fo.too_messy) { 
      if (debug_ipmath_arrayinfo)
	fprintf(stderr, "Formal is too messy\n"); 
      if (exact) *exact = FALSE;
      return ac.conservative_assumption(); 
    }
    return array_map_from_call(ac, fo, ctxt, exact);
}


/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * array_down_from_call(in_array * actual, var_sym * formal, const named_lin_ineq & ctxt, boolean * exact)
{
//    printf("array_up_from_call input:\n"); ctxt.print();

    array_dim_info ac(actual);
    array_dim_info fo(formal);
    if(ac.too_messy) { 
      if (debug_ipmath_arrayinfo)
	fprintf(stderr, "Actual is too messy\n"); 
      if (exact) *exact = FALSE;
      return ac.conservative_assumption(); 
    }
    if(fo.too_messy) { 
      if (debug_ipmath_arrayinfo)
	fprintf(stderr, "Formal is too messy\n"); 
      if (exact) *exact = FALSE;
      return ac.conservative_assumption(); 
    }
    return array_map_from_call(fo, ac, ctxt, exact);
}


/*************************************************************************
 * 
 *************************************************************************/
named_lin_ineq * array_down_from_call(var_sym * actual, var_sym * formal, const named_lin_ineq & ctxt, boolean * exact)
{
    array_dim_info ac(actual);
    array_dim_info fo(formal);
    if(ac.too_messy) { 
      if (debug_ipmath_arrayinfo)
	fprintf(stderr, "Actual is too messy\n"); 
      if (exact) *exact = FALSE;
      return ac.conservative_assumption(); 
    }
    if(fo.too_messy) { 
      if (debug_ipmath_arrayinfo)
	fprintf(stderr, "Formal is too messy\n"); 
      if (exact) *exact = FALSE;
      return ac.conservative_assumption(); 
    }
    return array_map_from_call(fo, ac, ctxt, exact);
}


named_lin_ineq * array_symbols_call(in_cal * call, boolean up, named_lin_ineq & ctxt)
{
    named_lin_ineq ret(ctxt);
    proc_sym * callee = proc_for_call(call);
    assert(callee);
    for(int i=1; i<ctxt.n(); i++) {
	var_sym * vs = ctxt.names()[i].var();
	assert(vs);
	if(vs->is_param()) {
	    operand mapop;
	    if(up)
		mapop = IST->up_from_call(operand(vs), call);
	    else
		mapop = IST->down_through_call(operand(vs), call);
	    if(mapop.is_symbol()) {
		int pos = ret.find(vs);
		ret.names()[pos].set_name(immed(mapop.symbol()));
	    } else {
		//BUGBUG: Not implemented, need to do what map class do in ipa
		fprintf(stderr,"\n*****\n");
		fprintf(stderr,"Encountered BUGBUG in ipmath/diminfo.cc\n");
		fprintf(stderr,"in procedure array_symbols_call()\n");
		fprintf(stderr,"unimplemented case with mapop.instr:\n");
	        mapop.instr()->print(stderr);
		fprintf(stderr,"\n*****\n");
		return NULL;
//		assert(0);
	    }
	}
    }
    return new named_lin_ineq(ret);
 }
