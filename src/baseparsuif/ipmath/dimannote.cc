#define _MODULE_ "libipmath.a"
#pragma implementation "arrayinfo.h"

#include <stdio.h>
#include <suif1.h>
#include <suifmath.h>
#include <useful.h>
#include <builder.h>
#include <ist.h>
#include "ipmath.h"


boolean debug_ipmath_arrayinfo = FALSE;

/* ################################################
   ####    array_dim_annote                 ####
   ################################################ */

void * array_dim_annote_from(const char *name, immed_list *il, suif_object *)
{
    array_dim_annote * ret = new array_dim_annote;
    
    assert(name == array_dim_annote::k_annote);
    assert(il);
    ret->init(il);
    return (void *)ret;
}

void array_dim_annote::init(immed_list *il)
{
    assert(il);
    assert(il->count() >= 2);
    immed im_cnt((*il)[0]);
    assert(im_cnt.is_integer());
    ndim = im_cnt.integer();
    for(int i=0; i<ndim; i++) {
	immed im_vs((*il)[i+1]);
	assert(im_vs.is_symbol());
	dimvars[i] = (var_sym *)im_vs.symbol();
    }
}



immed_list * array_dim_annote_to(const char *name, void *data)
{
    assert(name == array_dim_annote::k_annote);
    assert(data);
    array_dim_annote * ada = (array_dim_annote *) data;

    immed_list * im = ada->create();
    return im;
}

immed_list * array_dim_annote::create()
{
    immed_list * im = new immed_list;
    im->append(immed(ndim));
    for(int i=0; i<ndim; i++) 
	im->append(immed(dimvars[i]));
    
    return im;
}


void array_dim_annote_free(void *data)
{
    assert(data);
    array_dim_annote * ada = (array_dim_annote *) data;
    delete ada;
}


void array_dim_annote_print(FILE * fp, const char * /* name */, void * data)
{
    assert(data);
    array_dim_annote * ada = (array_dim_annote *) data;
    ada->print(fp);
}

void array_dim_annote::print(FILE *fp)
{
    fprintf(fp, "(");
    for(int i=0; i<ndim; i++)
	fprintf(fp, "%s ", dimvars[i]->name());
    fprintf(fp, ")");
}




array_dim_annote::array_dim_annote(var_sym * vs, base_symtab * bst)
{
    array_type * at;
    if(vs->type()->unqual()->is_array())
	at = (array_type *)vs->type()->unqual();
    else if(vs->type()->unqual()->is_ptr()) {
	if(((ptr_type *)vs->type()->unqual())->ref_type()->is_array())
	    at = (array_type *)((ptr_type *)vs->type()->unqual())->ref_type();
    }
    assert(at);

    ndim = numdim(at);
    for(int i = 0; i<ndim; i++) {
	char nm[128];
	//sprintf(nm, "_dim%d_%s", i, vs->name());
	sprintf(nm, "_D%d%s", i, vs->name());
	dimvars[i] = bst->new_unique_var(type_void, nm);
    }
}



var_sym * get_dim(var_sym * vs, int i)
{
    assert(i>=0);
    array_dim_annote * adi = (array_dim_annote *)vs->peek_annote(array_dim_annote::k_annote);
    if(adi == NULL) {
	fprintf(stderr, "No array dim annotations, perhaps preip is not run?\n");
	return NULL;
    }
    
    if(i >= adi->ndim) return NULL;
    return (*adi)[i];
}


 
void add_dimsym(var_sym * vs, base_symtab * bst)
{
    if(vs->type()->unqual()->is_array())
	if(vs->peek_annote(array_dim_annote::k_annote) == NULL) {
	    array_type * at = (array_type *)vs->type()->unqual();
	    assert(at->is_array());
	    array_dim_annote * ann = new array_dim_annote(vs, bst);
	    vs->set_annote(array_dim_annote::k_annote, ann);
	}

    if(vs->type()->unqual()->is_ptr()) {
	type_node * tn = ((ptr_type *)vs->type()->unqual())->ref_type();
	if(tn->is_array()) { // for Call-by-ref not in fortran mode
	    array_dim_annote * ann = new array_dim_annote(vs, bst);
	    vs->set_annote(array_dim_annote::k_annote, ann);
	} else if((tn->is_base())&&(tn->is_same(type_char)))
	    if(vs->peek_annote(array_dim_annote::k_annote) == NULL) {
		array_dim_annote * ann = new array_dim_annote();
		char nm[128];
		sprintf(nm, "_DS%s", vs->name());
		ann->ndim = 1;
		ann->dimvars[0] = bst->new_unique_var(type_void, nm);
		vs->set_annote(array_dim_annote::k_annote, ann);
	    }
    }

    for(unsigned i = 0; i < vs->num_children(); i++)
	add_dimsym(vs->child_var(i), bst);
}


void del_dimsym(var_sym * vs, base_symtab * bst)
{
    array_dim_annote * ada = (array_dim_annote *)vs->get_annote(array_dim_annote::k_annote);
    if(ada) {
	for(int i=0; i<ada->ndim; i++) {
	    var_sym * xvs = ada->dimvars[i];
	    base_symtab * xbst = xvs->parent();
	    xbst->remove_sym(xvs);
	}
    }
    
    for(unsigned i = 0; i < vs->num_children(); i++)
	del_dimsym(vs->child_var(i), bst);
}

