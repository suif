#pragma interface

extern ist *IST;
extern boolean debug_ipmath_arrayinfo;


#define MAXDIM 16
/**************************************************************************
 ***                                                                    ***
 *** A structured annotation that will keep a unique variable per each  ***
 *** dimension of an array.                                             ***
 ***                                                                    ***
 **************************************************************************/
struct array_dim_annote {
    // for annotation handling
    static const char * k_annote;

    var_sym * dimvars[MAXDIM];
    int ndim;

    array_dim_annote()                                 { ndim = 0;  }
	
    array_dim_annote(var_sym * vs, base_symtab * bst);
    var_sym * operator[](int i)                        { return dimvars[i]; }
    void print(FILE *fp = stdout);
    
    void init(immed_list * il);
    immed_list * create();
};

void * array_dim_annote_from(const char *name, immed_list *il, suif_object *);
immed_list * array_dim_annote_to(const char *name, void *data);
void array_dim_annote_free(void *data);
void array_dim_annote_print(FILE * fp, const char * nm, void *data);


// A simple interface, give the array var_sym and the dimenstion number
// this will return the unique var_sym for that array dimension.
var_sym * get_dim(var_sym * vs, int i);


void add_dimsym(var_sym * vs, base_symtab * bst);
void del_dimsym(var_sym * vs, base_symtab * bst);
