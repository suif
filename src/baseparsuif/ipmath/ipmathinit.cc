#define _MODULE_ "libipmath.a"

#include <stdio.h>
#include <suif1.h>
#include <suifmath.h>
#include <useful.h>
#include <builder.h>
#include <ist.h>
#include "ipmath.h"

const char * array_dim_annote::k_annote = 0;
ist *IST = 0;

void init_ipmath(int & /* argc */, char * /* argv */ [])
{
    STRUCT_ANNOTE(array_dim_annote::k_annote,
	"array_dim",
	TRUE,
	array_dim_annote_from,
	array_dim_annote_to,
	array_dim_annote_free,
	array_dim_annote_print);
    STRUCT_ANNOTE(code_gen_copy_array_region_annote::k_annote,
	"code_gen_copy_array_region",
	TRUE,
	code_gen_copy_array_region_annote_from,
	code_gen_copy_array_region_annote_to,
	code_gen_copy_array_region_annote_free,
	code_gen_copy_array_region_annote_print);
    STRUCT_ANNOTE(code_gen_initialize_annote::k_annote,
	"code_gen_initialize",
	TRUE,
	code_gen_initialize_annote_from,
	code_gen_initialize_annote_to,
	code_gen_initialize_annote_free,
	code_gen_initialize_annote_print);
    STRUCT_ANNOTE(code_gen_finalize_annote::k_annote,
	"code_gen_finalize",
	TRUE,
	code_gen_finalize_annote_from,
	code_gen_finalize_annote_to,
	code_gen_finalize_annote_free,
	code_gen_finalize_annote_print);
    IST = new ist();
}

void exit_ipmath(void)
{
    if (IST)
        delete IST;
    IST = 0;
}

