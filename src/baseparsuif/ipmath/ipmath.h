/* file "suifmath.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Top-Level Include File for the SUIF Interprocedural Math and other support Library */

#ifndef IPMATH_H
#define IPMATH_H

#ifdef IPMATHLIB
#define IPMATHINCFILE(F) #F
#else
#define IPMATHINCFILE(F) <ipmath/F>
#endif

#include IPMATHINCFILE(reshape.h)
#include IPMATHINCFILE(arrayinfo.h)
#include IPMATHINCFILE(codegen.h)

#endif

