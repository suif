#pragma interface

extern boolean debug_ipmath_reshape;

named_lin_ineq * remap(const named_lin_ineq &, in_cal *, boolean);
named_symcoeff_ineq * remap(named_symcoeff_ineq syst, in_cal * inc, boolean up);


named_lin_ineq * array2nli(in_array *);
in_array * nli2array(named_lin_ineq &, var_sym *, base_symtab * bst=NULL);
in_array * nli2array(named_lin_ineq &, sym_addr *, array_type *, base_symtab * bst=NULL);


named_lin_ineq * array_remap(const named_lin_ineq &, in_cal *, boolean, var_sym *, boolean * exact =NULL);
named_lin_ineq * array_remap(const named_lin_ineq &, in_cal *, boolean, sym_addr *, array_type *, boolean * exact=NULL);
var_sym  * array_remap_name(in_cal *, boolean, var_sym *);
sym_addr * array_remap_name(in_cal *, boolean, sym_addr *, array_type *);


named_lin_ineq * array_symbols_call(in_cal * call, boolean up, named_lin_ineq & ctxt);

var_sym * scalar_remap_name(in_cal *, boolean, var_sym *);
boolean is_placeholder(var_sym *v);


extern ist *IST;
void init_ipmath(int &argc, char * argv[]);

/**************************************************************************
 **************************************************************************
 **************************************************************************/


/**************************************************************************
 ***                                                                    ***
 *** Array bounds, just like class array_bound in the suif library      ***
 ***                                                                    ***
 **************************************************************************/
struct bound {
    boolean const_bound;
    union {
	int cnst;
	var_sym * var;
    } val;
    bound();
    bound(const bound &);
    void set(const operand & op, int & too_messy);
    void set(const array_bound & ab, int & too_messy);
    void set(int i)        { const_bound = TRUE; val.cnst = i; }
    boolean operator==(const bound &);
    boolean operator!=(const bound & a)               { return !(*this==a);}
    named_symcoeff_ineq get();
    void print(FILE * fp=stdout);
};


/**************************************************************************
 ***                                                                    ***
 *** Array dimension information                                        ***
 ***                                                                    ***
 **************************************************************************/
struct dim_info {
    bound lb;                                // lower bound
    bound ub;                                // upper bound
    named_symcoeff_ineq av;                  // access (if one exist)
    var_sym * sig;                           // The unique var of the dim

    dim_info()                                      { sig = NULL; }
    dim_info(dim_info &);

    int num_elem();			     // returns -1 if nonconst;

    void print(FILE *fp=stdout);
};

    
DECLARE_DLIST_CLASSES(array_dim_info_base, dim_info_e, array_dim_info_iter, dim_info *);

/**************************************************************************
 ***                                                                    ***
 *** Array information                                                  ***
 ***                                                                    ***
 *** All the necessary information of an array access (or from array    ***
 *** type)                                                              ***
 ***   The dimensions are stored in inner to outer                      ***
 **************************************************************************/

class array_dim_info:public array_dim_info_base {
    var_sym * var;
    int ndim;
    int elemsize;
    boolean no_outer_dim;
public:
    boolean too_messy;
  
    array_dim_info(array_dim_info &);
    array_dim_info(in_array *);
    array_dim_info(var_sym *);
    array_dim_info(var_sym *, array_type *);
    ~array_dim_info();

    int num_dim()                                     { return ndim; }
    int elem_size()                                   { return elemsize; }
    var_sym * array()                                 { return var; }
    void remove_inner_dim();
    void remove_outer_dim();
    named_lin_ineq * bounding_box(boolean get_access=FALSE);
    named_lin_ineq * inner_dim_bounding_box(int sz);
    named_lin_ineq * linearize_expr(boolean sub_array=FALSE);
    named_lin_ineq * conservative_assumption();
    void project_dim_away(named_lin_ineq &, array_dim_info &);

    void print(FILE * fp=stdout);
private:
    void add(in_array *);
    void init(var_sym * vs, array_type * at =NULL);
};

boolean is_same_dim(array_dim_info & adi1, array_dim_info & adi2, boolean chk_last_dim=TRUE);
boolean is_same_dim(dim_info & di1, dim_info & di2, boolean chk_ub=TRUE);

named_lin_ineq * array_up_from_call(in_array * actual, var_sym * formal, const named_lin_ineq & ctxt, boolean * exact=NULL);
named_lin_ineq * array_up_from_call(var_sym * actual, var_sym * formal, const named_lin_ineq & ctxt, boolean * exact=NULL);
named_lin_ineq * array_down_from_call(in_array * actual, var_sym * formal, const named_lin_ineq & ctxt, boolean * exact=NULL);
named_lin_ineq * array_down_from_call(var_sym * actual, var_sym * formal, const named_lin_ineq & ctxt, boolean * exact=NULL);


extern array_bound (*better_bound_f)(array_bound);
