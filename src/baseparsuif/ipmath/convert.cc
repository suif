#define _MODULE_ "libipmath.a"

#include <stdio.h>
#include <suif1.h>
#include <suifmath.h>
#include <useful.h>
#include <builder.h>
#include <ist.h>
#include "ipmath.h"

named_lin_ineq * array2nli(in_array * ia)
{
    named_lin_ineq *res = new named_lin_ineq;
    array_type * at = find_array_type(ia);
    var_sym * as = get_sym_of_array(ia);

    (void) at; // avoid unused var warnings but keep for debugging
    for(int i=0; ((unsigned)i) < ia->dims(); i++) {
	var_sym * dimv = get_dim(as, i);

	operand expr(ia->index(i));
	named_symcoeff_ineq * nsi = named_symcoeff_ineq::convert_exp(expr);
	named_lin_ineq * ind = nsi->nli();
	if(ind == NULL) return NULL;

	assert(ind->ineqs().m() == 1);
	name_table_entry nte(dimv);
	nte.mark_dim();
	ind->add_col(nte, 1);
	ind->ineqs()[0][1] = -1;

	*res || *ind;
	*res &= *ind;
	ind->ineqs()[0] *= -1;
	*res &= *ind;
	delete ind;
    }

    res->simplify();
    return res;
}



in_array * nli2array(named_lin_ineq & nli, var_sym * as, base_symtab * bst)
{
    // get the array type
    array_type * at = find_array_type(as->type());
    if(at == NULL) return NULL;


    int nd = numdim(at);
    
    block * indlist[6];
    int i;
    for(i=0; i<6; i++) indlist[i] = builder::B_NOOP;
    
    for(i=0; i<nd; i++) {
	var_sym * dimv = get_dim(as, i);
	assert(dimv);
	instruction * ins = nli.create_expression(immed(dimv), FALSE);
	assert(ins);
	indlist[i] = new block(ins);
    }

    block arr(block::ARRAY(block(as),
			   *indlist[0], *indlist[1],
			   *indlist[2], *indlist[3],
			   *indlist[4], *indlist[5]));

    instruction * retins = arr.make_instruction((block_symtab *)bst);

    // BUILDER Specific code, get the array instruction
    assert(retins->opcode() == io_lod);
    in_rrr * lod = (in_rrr *)retins;
    assert(lod->src1_op().is_instr());
    assert(lod->src1_op().instr()->opcode() == io_array);
    return (in_array *)lod->src1_op().instr();
}
	
    




