/*  Declarations for Reading Pointer Analysis Annotations */

#ifndef POINTERS_H
#define POINTERS_H

class w_obj;


/*
 *  Since pointers may references symbols outside the scope where they
 *  are visible, we need some way to identify non-visible symbols.
 *  The w_non_visible class contains information that identifies such
 *  symbols, and it provides a method to find the corresponding symbols
 *  if they are currently in memory.
 */

class w_non_visible {
    friend void wilbyr_obj_from(w_obj *b, immed_list_iter *iter);
private:
    const char *nm;
    unsigned sid;
    int fn;
    unsigned prid;
    w_non_visible()                     { nm = NULL; sid=0; fn=0; prid=0; }
public:
    w_non_visible(sym_node *s);
    const char *name()			{ return nm; }
    unsigned sym_id()			{ return sid; }
    int file_num()			{ return fn; }
    unsigned proc_sym_id()		{ return prid; }

    boolean operator==(const w_non_visible &v);
    boolean operator!=(const w_non_visible &v) { return !operator==(v); }
    sym_node *symbol();
};



/*
 *  A pointer value may reference four kinds of storage: symbols, heap,
 *  extended parameters, and non-visible symbols.  The following classes
 *  represent these things.
 */

enum w_obj_kind {
    W_SYM_OBJ,				/* symbols */
    W_HEAP_OBJ,				/* heap blocks */
    W_PARAM,				/* extended parameters */
    W_NON_VIS				/* non-visible symbols */
};

class w_obj {
    friend void wilbyr_obj_from(w_obj *b, immed_list_iter *iter);
private:
    w_obj_kind knd;
    union {
	int ndx;			/* for W_HEAP_OBJ and W_PARAM */
	sym_node *sn;			/* for W_SYM_OBJ */
	w_non_visible *nv;		/* for W_NON_VIS */
    } u;
public:
    w_obj()				{ }
    w_obj(sym_node *s)			{ knd = W_SYM_OBJ; u.sn = s; }
    w_obj(const w_obj &v);
    ~w_obj();

    w_obj_kind kind()			{ return knd; }
    boolean is_sym_obj()		{ return (kind() == W_SYM_OBJ); }
    boolean is_heap_obj()		{ return (kind() == W_HEAP_OBJ); }
    boolean is_param()			{ return (kind() == W_PARAM); }
    boolean is_non_visible()		{ return (kind() == W_NON_VIS); }

    int index()				{ return u.ndx; }
    sym_node *symbol()			{ return u.sn; }
    w_non_visible *non_visible()	{ return u.nv; }
    void operator=(const w_obj &v);
    boolean operator==(const w_obj &v);
    boolean operator!=(const w_obj &v)	{ return !operator==(v); }
};


/*
 *  A pointer value combines a base address with an offset and a stride.
 *  The w_pval class represent these pointer values.
 */

class w_pval {
    friend void wilbyr_pval_from(w_pval *p, immed_list_iter *iter);
private:
    w_obj bs;				/* base object */
    int off;				/* offset */
    int str;				/* stride */
public:
    w_pval()				{ }
    w_pval(w_obj b, int f, int s)	{ bs = b; off = f; str = s; }

    w_obj *base()			{ return &bs; }
    int offset()			{ return off; }
    int stride()			{ return str; }

    boolean operator==(const w_pval &v);
    boolean operator!=(const w_pval &v)	{ return !operator==(v); }
    boolean aliased(w_pval *p, int *f=NULL, int *s=NULL);
};

DECLARE_LIST_CLASSES(w_pval_list_base, w_pval_list_e, w_pval_list_iter,
		     w_pval);

class w_pval_list : public w_pval_list_base {
private:
    boolean aliased_helper(w_pval *v1, int *off, int *str);
public:
    boolean aliased(w_pval_list *p, int *f=NULL, int *s=NULL);
    boolean aliased(sym_node *sym, base_symtab *scope, int clone_num,
		    int *f=NULL, int *s=NULL);
};



/*
 *  The following declarations describe the format of the annotations
 *  produced by the pointer analysis.  Most of them include a "clone
 *  number" to specify the context where the information applies.
 */


/*
 *  The "bind" annotations are attached to the procedure symbol tables
 *  to indicate the partial transfer function points to mappings
 *  These can be strong or weak and will also have a
 *  currently superfluous control flow graph node number that they are
 *  associated with.  Of course if the CFG is written out, this would
 *  change.
 */

class w_bind {
    friend void *wilbyr_bind_from(const char*, immed_list *il, suif_object*);
private:
    int cln;				// Clone number
    unsigned nd;			// control flow graph node (NA)
    boolean str;			// strong(true)  weak(false)
    w_pval pv;				// the pointing pval
    w_pval_list pvl;			// the pointed to pval list
public:
    int clone_num()			{ return cln; }
    unsigned cfg_node()			{ return nd; }
    boolean is_strong()			{ return str; }
    w_pval *pval()			{ return &pv; }
    w_pval_list *points_to()		{ return &pvl; }
};

/*
 *  The "global_alias" annotations are attached to symbol tables (typically
 *  the procedure symbol tables, except for static variables declared in
 *  nested blocks) to indicate that a global or static variable is aliased
 *  with certain extended parameters.  There may be multiple aliases for a
 *  variable and the aliases may include offsets and/or strides.
 */

class w_global_alias {
    friend void *wilbyr_global_alias_from(const char*, immed_list *il, suif_object*);
private:
    int cln;
    sym_node *sn;
    w_pval_list als;
public:
    int clone_num()			{ return cln; }
    sym_node *symbol()			{ return sn; }
    w_pval_list *aliases()		{ return &als; }
};


/*
 *  The "load_from" and "store_to" annotations on memory references record
 *  the addresses that may potentially be referenced.  The "array_base"
 *  annotations on array instructions identify the potential values of
 *  the base pointer.
 */

class w_points_to {
    friend void *wilbyr_points_to_from(const char *, immed_list *il, suif_object *);
private:
    int cln;
    w_pval_list pt;
public:
    int clone_num()			{ return cln; }
    w_pval_list *points_to()		{ return &pt; }
};


/*
 *  At each call site, "call_tgt" annotations on the call instruction
 *  specify the target procedures and target clone numbers.  The target
 *  procedure is usually obvious except for calls through pointers, but
 *  the target clone number is still needed to determine which version
 *  of points-to annotations to use in the target procedure.  Note:
 *  Since the symbol for the target procedure may not be visible, it
 *  may be identified by a non-visible object instead of a symbol.
 */

class w_call_tgt {
    friend void *wilbyr_call_tgt_from(const char *, immed_list *il, suif_object *);
private:
    int cln;
    w_obj tpr;
    int tcln;
public:
    int clone_num()			{ return cln; }
    w_obj *target()			{ return &tpr; }
    int target_clone_num()		{ return tcln; }
};


/*
 *  To use the pointer information interprocedurally, one needs to know
 *  how to map the extended parameters across procedure calls.  The
 *  "param_map" annotations provide this information.  They are attached
 *  to call instructions, immediately following a "call_tgt" annotation
 *  (when there are multiple targets the order of the annotations
 *  identifies which target the mapping is for).  There is a separate
 *  annotation for each extended parameter that is referenced in the
 *  target procedure; unreferenced parameters may not be listed.  The
 *  annotations list the actual values from the calling procedure for
 *  each extended parameter.
 */

class w_param_map {
    friend void *wilbyr_param_map_from(const char *, immed_list *il, suif_object *);
private:
    int ndx;
    w_pval_list acts;
public:
    int index()				{ return ndx; }
    w_pval_list *actuals()		{ return &acts; }
};


/*
 *  Heap objects are simply represented by integer identifiers.  If
 *  more descriptive information is needed, it can be read from the
 *  "heapobj" annotations on procedure symbol tables.  The basic
 *  scheme for naming heap objects is to use the context where they
 *  are allocated.  Thus each procedure keeps track of heap objects
 *  that are returned to it from its callees.  A heap object is
 *  identified by the call site from which it was returned, the callee
 *  procedure and clone number, and the identifier for the corresponding
 *  heap object in the callee.  Since this basic scheme leads to an
 *  explosion in the number of heap objects, the pointer analysis
 *  combines sets of them and uses one token heap object to represent
 *  the entire set.  The other heap objects in the set are marked with
 *  the index of the token heap object that replaces them.  Note: The
 *  heap objects are numbered beginning with one instead of zero.
 */

class w_heapobj {
    friend void *wilbyr_heapobj_from(const char *, immed_list *il, suif_object *);
private:
    int repl;
    unsigned caln;
    w_obj tpr;
    int tcln;
    int tn;
public:
    int replacement()			{ return repl; }
    unsigned call_site()		{ return caln; }
    w_obj *callee()			{ return &tpr; }
    int callee_clone_num()		{ return tcln; }
    int callee_index()			{ return tn; }
};

class w_heap_table {
    friend void *wilbyr_heapobj_from(const char *, immed_list *il, suif_object *);
private:
    int cln;
    int nh;
    w_heapobj *tbl;
public:
    ~w_heap_table();
    int clone_num()			{ return cln; }
    int num_heap_objs()			{ return nh; }
    w_heapobj *heap_obj(int n);
};


/*  Initialization function */
extern void wilbyr_init_annotes();

/*  Macros for accessing annotation data */
#define WILBYR_BIND(an)		(w_bind *)(an)->data()
#define WILBYR_CALL_TGT(an)	(w_call_tgt *)(an)->data()
#define WILBYR_GLOBAL_ALIAS(an)	(w_global_alias *)(an)->data()
#define WILBYR_HEAPOBJ(an)	(w_heap_table *)(an)->data()
#define WILBYR_LOAD_FROM(an)	(w_points_to *)(an)->data()
#define WILBYR_STORE_TO(an)	(w_points_to *)(an)->data()
#define WILBYR_ARRAY_BASE(an)	(w_points_to *)(an)->data()
#define WILBYR_PARAM_MAP(an)	(w_param_map *)(an)->data()

/*  Annotation names */
extern const char *k_wilbyr_bind;
extern const char *k_wilbyr_call_tgt;
extern const char *k_wilbyr_global_alias;
extern const char *k_wilbyr_heapobj;
extern const char *k_wilbyr_load_from;
extern const char *k_wilbyr_store_to;
extern const char *k_wilbyr_array_base;
extern const char *k_wilbyr_param_map;

/*  Helper functions */
extern void wilbyr_obj_from(w_obj *b, immed_list_iter *iter);
extern void wilbyr_obj_to(immed_list *result, w_obj *b);
extern void wilbyr_obj_print(FILE *fp, w_obj *b);
extern void wilbyr_pval_from(w_pval *p, immed_list_iter *iter);
extern void wilbyr_pval_to(immed_list *result, w_pval *p);
extern void wilbyr_pval_print(FILE *fp, w_pval *p);
extern void wilbyr_pval_list_from(w_pval_list *pl, immed_list_iter *iter);
extern void wilbyr_pval_list_to(immed_list *result, w_pval_list *pl);
extern void wilbyr_pval_list_print(FILE *fp, w_pval_list *pl);

/*  Functions for the annotation manager */
extern void *wilbyr_bind_from(const char *, immed_list *il, suif_object *);
extern void *wilbyr_call_tgt_from(const char *, immed_list *il, suif_object *);
extern void *wilbyr_global_alias_from(const char *, immed_list *il, suif_object *);
extern void *wilbyr_heapobj_from(const char *, immed_list *il, suif_object *);
extern void *wilbyr_points_to_from(const char *, immed_list *il, suif_object *);
extern void *wilbyr_param_map_from(const char *, immed_list *il, suif_object *);

extern immed_list *wilbyr_bind_to(const char *, void *data);
extern immed_list *wilbyr_call_tgt_to(const char *, void *data);
extern immed_list *wilbyr_global_alias_to(const char *, void *data);
extern immed_list *wilbyr_heapobj_to(const char *, void *data);
extern immed_list *wilbyr_points_to_to(const char *, void *data);
extern immed_list *wilbyr_param_map_to(const char *, void *data);

extern void wilbyr_bind_free(void *data);
extern void wilbyr_call_tgt_free(void *data);
extern void wilbyr_global_alias_free(void *data);
extern void wilbyr_heapobj_free(void *data);
extern void wilbyr_points_to_free(void *data);
extern void wilbyr_param_map_free(void *data);

extern void wilbyr_bind_print(FILE *fp, const char *name, void *data);
extern void wilbyr_call_tgt_print(FILE *fp, const char *name, void *data);
extern void wilbyr_global_alias_print(FILE *fp, const char *name, void *data);
extern void wilbyr_heapobj_print(FILE *fp, const char *name, void *data);
extern void wilbyr_points_to_print(FILE *fp, const char *name, void *data);
extern void wilbyr_param_map_print(FILE *fp, const char *name, void *data);

#endif /* POINTERS_H */
