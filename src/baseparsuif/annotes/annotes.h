/*  Top-level Structured Annotation Library Include File */

#ifndef ANNOTES_H
#define ANNOTES_H

#include <suif1.h>
#ifdef ANNOTESLIB
#include "pointers.h"
#else
#include <annotes/pointers.h>
#endif

/*----------------------------------------------------------------------*
    Beginning of Initialization Routines
 *----------------------------------------------------------------------*/

/*
 *  Initialize this library.  This must be called before anything else
 *  in this library is used, but usually it's called automatically by
 *  start_suif() if necessary, so the user needn't think about
 *  it.
 */
extern void init_annotes(int &argc, char *argv[]);

/*
 *  Cleanup after the last use of this library.  This is called before
 *  the program exits to do cleanup such as de-allocating
 *  datastructures.
 */
extern void exit_annotes(void);

/*----------------------------------------------------------------------*
    End of Initialization Routines
 *----------------------------------------------------------------------*/

#endif /* ANNOTES_H */
