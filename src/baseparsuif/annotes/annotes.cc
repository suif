/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 * This is the main program for the SUIF library of miscellaneous
 * registered annotation routines.
 */

#define _MODULE_ "libannotes.a"

#pragma implementation "annotes.h"

#define RCS_BASE_FILE annotes_cc

#include "annotes.h"

RCS_BASE(
    "$Id: annotes.cc,v 1.1.1.1 1998/07/07 05:09:38 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

	The SUIF ``annotes'' library provides a place for annotations
	required for advanced C processing - e.g. wilbyr annotation that
	describe aliasing patterns.  

 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/


extern void init_annotes(int & /* argc */, char * /* argv */ [])
  {
      wilbyr_init_annotes();
  }

extern void exit_annotes(void)
  {
    return;
  }
