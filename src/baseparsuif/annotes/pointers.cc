/*  Functions for Reading Pointer Analysis Annotations */

#include <suif1.h>
#include "pointers.h"

static w_pval_list *sym_node_aliases(sym_node *s, base_symtab *scope,
				     int clone_num);
static void meet(int *off, int *str, int f, int s);
static int int_gcd(int m, int n);

const char *k_wilbyr_bind;
const char *k_wilbyr_call_tgt;
const char *k_wilbyr_global_alias;
const char *k_wilbyr_heapobj;
const char *k_wilbyr_load_from;
const char *k_wilbyr_store_to;
const char *k_wilbyr_array_base;
const char *k_wilbyr_param_map;


void
wilbyr_init_annotes ()
{
    if (k_wilbyr_bind != NULL) return;
    STRUCT_ANNOTE(k_wilbyr_bind, "wilbyr_bind", TRUE,
		  wilbyr_bind_from,
		  wilbyr_bind_to,
		  wilbyr_bind_free,
		  wilbyr_bind_print);
    STRUCT_ANNOTE(k_wilbyr_call_tgt, "wilbyr_call_tgt", TRUE,
		  wilbyr_call_tgt_from,
		  wilbyr_call_tgt_to,
		  wilbyr_call_tgt_free,
		  wilbyr_call_tgt_print);
    STRUCT_ANNOTE(k_wilbyr_global_alias, "wilbyr_global_alias", TRUE,
		  wilbyr_global_alias_from,
		  wilbyr_global_alias_to,
		  wilbyr_global_alias_free,
		  wilbyr_global_alias_print);
    STRUCT_ANNOTE(k_wilbyr_heapobj, "wilbyr_heapobj", TRUE,
		  wilbyr_heapobj_from,
		  wilbyr_heapobj_to,
		  wilbyr_heapobj_free,
		  wilbyr_heapobj_print);
    STRUCT_ANNOTE(k_wilbyr_load_from, "wilbyr_load_from", TRUE,
		  wilbyr_points_to_from,
		  wilbyr_points_to_to,
		  wilbyr_points_to_free,
		  wilbyr_points_to_print);
    STRUCT_ANNOTE(k_wilbyr_store_to, "wilbyr_store_to", TRUE,
		  wilbyr_points_to_from,
		  wilbyr_points_to_to,
		  wilbyr_points_to_free,
		  wilbyr_points_to_print);
    STRUCT_ANNOTE(k_wilbyr_array_base, "wilbyr_array_base", TRUE,
		  wilbyr_points_to_from,
		  wilbyr_points_to_to,
		  wilbyr_points_to_free,
		  wilbyr_points_to_print);
    STRUCT_ANNOTE(k_wilbyr_param_map, "wilbyr_param_map", TRUE,
		  wilbyr_param_map_from,
		  wilbyr_param_map_to,
		  wilbyr_param_map_free,
		  wilbyr_param_map_print);
}


void
wilbyr_obj_from (w_obj *b, immed_list_iter *iter)
{
    immed im = iter->step();

    switch (im.kind()) {
	case im_int: {
	    int imint = im.integer();
	    if (imint < 0) {
		b->knd = W_HEAP_OBJ;
		b->u.ndx = -imint;
	    } else {
		b->knd = W_PARAM;
		b->u.ndx = imint;
	    }
	    break;
	}
	case im_symbol: {
	    b->knd = W_SYM_OBJ;
	    b->u.sn = im.symbol();
	    break;
	}
	case im_string: {
	    b->knd = W_NON_VIS;
	    w_non_visible *nv = new w_non_visible;
	    nv->nm = im.string();
	    nv->sid = iter->step().unsigned_int();
	    nv->fn = iter->step().integer();
	    nv->prid = iter->step().unsigned_int();
	    b->u.nv = nv;
	    break;
	}
	default: {
	    assert(FALSE);
	}
    }
}


void
wilbyr_obj_to (immed_list *result, w_obj *b)
{
    switch (b->kind()) {
	case W_HEAP_OBJ: {
	    result->append(immed(-b->index()));
	    break;
	}
	case W_PARAM: {
	    result->append(immed(b->index()));
	    break;
	}
	case W_SYM_OBJ: {
	    result->append(immed(b->symbol()));
	    break;
	}
	case W_NON_VIS: {
	    w_non_visible *nv = b->non_visible();
	    result->append(immed(nv->name()));
	    result->append(immed(nv->sym_id()));
	    result->append(immed(nv->file_num()));
	    result->append(immed(nv->proc_sym_id()));
	    break;
	}
    }
}


void
wilbyr_obj_print (FILE *fp, w_obj *b)
{
    switch (b->kind()) {
	case W_HEAP_OBJ: {
	    fprintf(fp, "$H%d", b->index());
	    break;
	}
	case W_PARAM: {
	    fprintf(fp, "$P%d", b->index());
	    break;
	}
	case W_SYM_OBJ: {
	    b->symbol()->print(fp);
	    break;
	}
	case W_NON_VIS: {
	    sym_node *sn = b->non_visible()->symbol();
	    if (sn) {
		sn->print(fp);
	    } else {
		fprintf(fp, "[%s]", b->non_visible()->name());
	    }
	    break;
	}
    }
}


void
wilbyr_pval_from (w_pval *p, immed_list_iter *iter)
{
    wilbyr_obj_from(p->base(), iter);
    p->off = iter->step().integer();
    p->str = iter->step().integer();
}


void
wilbyr_pval_to (immed_list *result, w_pval *p)
{
    wilbyr_obj_to(result, p->base());
    result->append(immed(p->offset()));
    result->append(immed(p->stride()));
}


void
wilbyr_pval_print (FILE *fp, w_pval *p)
{
    fputc('<', fp);
    wilbyr_obj_print(fp, p->base());
    fprintf(fp, ", %d", p->offset());
    fprintf(fp, ", %d", p->stride());
    fputc('>', fp);
}


void
wilbyr_pval_list_from (w_pval_list *pl, immed_list_iter *iter)
{
    while (!iter->is_empty()) {
	w_pval v;
	wilbyr_pval_from(&v, iter);
	pl->append(v);
    }
}


void
wilbyr_pval_list_to (immed_list *result, w_pval_list *pl)
{
    w_pval_list_iter iter(pl);
    while (!iter.is_empty()) {
	w_pval v = iter.step();
	wilbyr_pval_to(result, &v);
    }
}


void
wilbyr_pval_list_print (FILE *fp, w_pval_list *pl)
{
    if (pl->is_empty()) {
	fputs("None", fp);
    } else {
	w_pval_list_iter iter(pl);
	while (TRUE) {
	    w_pval v = iter.step();
	    wilbyr_pval_print(fp, &v);
	    if (iter.is_empty()) break;
	    fputs(", ", fp);
	}
    }
}


void *
wilbyr_bind_from (const char *, immed_list *il, suif_object *)
{
    w_bind *result = new w_bind;
    immed_list_iter iter(il);
    result->cln = iter.step().integer();
    result->nd = iter.step().unsigned_int();
    result->str = (iter.step().unsigned_int() != 0);
    wilbyr_pval_from(&result->pv, &iter);
    wilbyr_pval_list_from(&result->pvl, &iter);
    return (void *)result;
}


immed_list *
wilbyr_bind_to (const char *, void *data)
{
/*
    immed_list *ims = new immed_list;
		annote *an = new annote(k_wilbyr_bind, (void *)ims);
		ims->append(immed(x->clone_num()));
		ims->append(immed(num));
		ims->append(immed(b->is_strong()));
		append_pval(ims, p, proc->block()->scope());
		append_pval_list(ims, pvl, proc->block()->scope());
*/
    w_bind *bnd = (w_bind *)data;
    immed_list *result = new immed_list;
    result->append(immed(bnd->clone_num()));
    result->append(immed(bnd->cfg_node()));
    result->append(immed(bnd->is_strong()));
    wilbyr_pval_to(result, bnd->pval());
    wilbyr_pval_list_to(result, bnd->points_to());
    return result;
}


void
wilbyr_bind_free (void *data)
{
    w_bind *ctgt = (w_bind *)data;
    delete ctgt;
}


void
wilbyr_bind_print (FILE *fp, const char *name, void *data)
{
    w_bind *bnd = (w_bind *)data;
    fprintf(fp, "[\"%s\": (%d) ",
	    name, bnd->clone_num());
    wilbyr_pval_print(fp, bnd->pval());
    if (bnd->is_strong()) {
	fputs(" => ", fp);
    } else {
	fputs(" -> ", fp);
    }
    wilbyr_pval_list_print(fp, bnd->points_to());
    fprintf(fp, " @inst %d ]", 
	    bnd->cfg_node());
}


void *
wilbyr_call_tgt_from (const char *, immed_list *il, suif_object *)
{
    w_call_tgt *result = new w_call_tgt;
    immed_list_iter iter(il);
    result->cln = iter.step().integer();
    wilbyr_obj_from(&result->tpr, &iter);
    result->tcln = iter.step().integer();
    return (void *)result;
}


immed_list *
wilbyr_call_tgt_to (const char *, void *data)
{
    w_call_tgt *ctgt = (w_call_tgt *)data;
    immed_list *result = new immed_list;
    result->append(immed(ctgt->clone_num()));
    wilbyr_obj_to(result, ctgt->target());
    result->append(immed(ctgt->target_clone_num()));
    return result;
}


void
wilbyr_call_tgt_free (void *data)
{
    w_call_tgt *ctgt = (w_call_tgt *)data;
    delete ctgt;
}


void
wilbyr_call_tgt_print (FILE *fp, const char *name, void *data)
{
    w_call_tgt *ctgt = (w_call_tgt *)data;
    fprintf(fp, "[\"%s\": (%d) target=", name, ctgt->clone_num());
    wilbyr_obj_print(fp, ctgt->target());
    fprintf(fp, "(%d)]", ctgt->target_clone_num());
}


void *
wilbyr_global_alias_from (const char *, immed_list *il, suif_object *)
{
    w_global_alias *result = new w_global_alias;
    immed_list_iter iter(il);
    result->cln = iter.step().integer();
    result->sn = iter.step().symbol();
    wilbyr_pval_list_from(&result->als, &iter);
    return (void *)result;
}


immed_list *
wilbyr_global_alias_to (const char *, void *data)
{
    w_global_alias *globa = (w_global_alias *)data;
    immed_list *result = new immed_list;
    result->append(immed(globa->clone_num()));
    result->append(immed(globa->symbol()));
    wilbyr_pval_list_to(result, globa->aliases());
    return result;
}


void
wilbyr_global_alias_free (void *data)
{
    w_global_alias *globa = (w_global_alias *)data;
    delete globa;
}


void
wilbyr_global_alias_print (FILE *fp, const char *name, void *data)
{
    w_global_alias *globa = (w_global_alias *)data;
    fprintf(fp, "[\"%s\": (%d) ", name, globa->clone_num());
    globa->symbol()->print(fp);
    fputs(" aliases = ", fp);
    wilbyr_pval_list_print(fp, globa->aliases());
    fputc(']', fp);
}


void *
wilbyr_heapobj_from (const char *, immed_list *il, suif_object *)
{
    w_heap_table *result = new w_heap_table;
    immed_list_iter iter(il);
    result->cln = iter.step().integer();
    int nh = iter.step().integer();
    result->nh = nh;
    result->tbl = new w_heapobj[nh];
    for (int n = 0; n < nh; n++) {
	result->tbl[n].repl = -iter.step().integer();
	result->tbl[n].caln = iter.step().unsigned_int();
	wilbyr_obj_from(&result->tbl[n].tpr, &iter);
	result->tbl[n].tcln = iter.step().integer();
	result->tbl[n].tn = -iter.step().integer();
    }
    return (void *)result;
}


immed_list *
wilbyr_heapobj_to (const char *, void *data)
{
    w_heap_table *hptbl = (w_heap_table *)data;
    immed_list *result = new immed_list;
    result->append(immed(hptbl->clone_num()));
    result->append(immed(hptbl->num_heap_objs()));
    for (int n = 1; n <= hptbl->num_heap_objs(); n++) {
	w_heapobj *hp = hptbl->heap_obj(n);
	result->append(immed(-hp->replacement()));
	result->append(immed(hp->call_site()));
	wilbyr_obj_to(result, hp->callee());
	result->append(immed(hp->callee_clone_num()));
	result->append(immed(-hp->callee_index()));
    }
    return result;
}


void
wilbyr_heapobj_free (void *data)
{
    w_heap_table *hptbl = (w_heap_table *)data;
    delete hptbl;
}


void
wilbyr_heapobj_print (FILE *fp, const char *name, void *data)
{
    w_heapobj *hp;
    int repl;
    w_heap_table *hptbl = (w_heap_table *)data;
    fprintf(fp, "[\"%s\": (%d)",
	    name, hptbl->clone_num());
    for (int i = 1; i <= hptbl->num_heap_objs(); i++) {
	hp = hptbl->heap_obj(i);
	repl = (hp->replacement() == 0) ? i : hp->replacement();
	fprintf(fp, " | $H%d=$H%d => (%d)$H%d @call %d",
		i,
		repl,
		hp->callee_clone_num(),
		hp->callee_index(),
		hp->call_site()
	);
    }
    fprintf(fp, " ]\n");
}


void *
wilbyr_points_to_from (const char *, immed_list *il, suif_object *)
{
    w_points_to *result = new w_points_to;
    immed_list_iter iter(il);
    result->cln = iter.step().integer();
    wilbyr_pval_list_from(&result->pt, &iter);
    return (void *)result;
}


immed_list *
wilbyr_points_to_to (const char *, void *data)
{
    w_points_to *pt = (w_points_to *)data;
    immed_list *result = new immed_list;
    result->append(immed(pt->clone_num()));
    wilbyr_pval_list_to(result, pt->points_to());
    return result;
}


void
wilbyr_points_to_free (void *data)
{
    w_points_to *pt = (w_points_to *)data;
    delete pt;
}


void
wilbyr_points_to_print (FILE *fp, const char *name, void *data)
{
    w_points_to *pt = (w_points_to *)data;
    fprintf(fp, "[\"%s\": (%d) ", name, pt->clone_num());
    wilbyr_pval_list_print(fp, pt->points_to());
    fputc(']', fp);
}


void *
wilbyr_param_map_from (const char *, immed_list *il, suif_object *)
{
    w_param_map *result = new w_param_map;
    immed_list_iter iter(il);
    result->ndx = iter.step().integer();
    wilbyr_pval_list_from(&result->acts, &iter);
    return (void *)result;
}


immed_list *
wilbyr_param_map_to (const char *, void *data)
{
    w_param_map *pm = (w_param_map *)data;
    immed_list *result = new immed_list;
    result->append(immed(pm->index()));
    wilbyr_pval_list_to(result, pm->actuals());
    return result;
}


void
wilbyr_param_map_free (void *data)
{
    w_param_map *pm = (w_param_map *)data;
    delete pm;
}


void
wilbyr_param_map_print (FILE *fp, const char *name, void *data)
{
    w_param_map *pm = (w_param_map *)data;
    fprintf(fp, "[\"%s\": $P%d actuals = ", name, pm->index());
    wilbyr_pval_list_print(fp, pm->actuals());
    fputc(']', fp);
}

w_non_visible::w_non_visible(sym_node *s) 
{
  int file_num = 0;
  int proc_sym_id = 0;

  /*  put the symbol name first (to identify it as a non-visible
      symbol; also helps to make the output intelligible) */
  nm = s->name();
  sid = s->sym_id();
  if (s->parent()->is_block()) {
    proc_sym *pr = ((block_symtab *)s->parent())->block()->proc();
    proc_sym_id = pr->sym_id();
    s = pr;
  }
  if (s->parent()->is_file()) {
    file_num = ((file_symtab *)s->parent())->fse()->file_id();
  }

  fn = file_num;
  prid = proc_sym_id;
}

boolean
w_non_visible::operator== (const w_non_visible &v)
{
    return ((nm == v.nm) &&
	    (sid == v.sid) &&
	    (fn == v.fn) &&
	    (prid == v.prid));
}

// Need a default equals operator
void w_obj::operator= (const w_obj &obj)
{
  this->knd = obj.knd;
  switch (this->knd) {
  case W_HEAP_OBJ:
  case W_PARAM: {
    this->u.ndx = obj.u.ndx;
    break;
  }
  case W_SYM_OBJ: {
    this->u.sn = obj.u.sn;
    break;
  }
  case W_NON_VIS: {
    this->u.nv = new w_non_visible(*obj.u.nv);
    break;
  }
  }
}  

// Need a default copy constructor
w_obj::w_obj (const w_obj &obj)
{
  this->knd = obj.knd;
  switch (this->knd) {
  case W_HEAP_OBJ:
  case W_PARAM: {
    this->u.ndx = obj.u.ndx;
    break;
  }
  case W_SYM_OBJ: {
    this->u.sn = obj.u.sn;
    break;
  }
  case W_NON_VIS: {
    this->u.nv = new w_non_visible(*obj.u.nv);
    break;
  }
  }
}  
  

w_obj::~w_obj() 
{
    switch (knd) {
    case W_NON_VIS:
	free(non_visible());
	return;
    default:
	return;
    }
}

boolean
w_obj::operator== (const w_obj &v)
{
    switch (knd) {
	case W_HEAP_OBJ:
	case W_PARAM: {
	    if (v.knd != knd) return FALSE;
	    return (u.ndx == v.u.ndx);
	}
	case W_SYM_OBJ: {
	    if (v.knd == W_NON_VIS) {
		return (u.sn == v.u.nv->symbol());
	    }
	    if (v.knd != W_SYM_OBJ) return FALSE;
	    return (u.sn == v.u.sn);
	}
	case W_NON_VIS:	{
	    if (v.knd == W_SYM_OBJ) {
		return (u.nv->symbol() == v.u.sn);
	    }
	    if (v.knd != W_NON_VIS) return FALSE;
	    return (*u.nv == *v.u.nv);
	}
    }
    return TRUE;
}


boolean
w_pval::operator== (const w_pval &v)
{
    return ((bs == v.bs) && (off == v.off) && (str == v.str));
}


sym_node *
w_non_visible::symbol ()
{
    base_symtab *scope = NULL;

    if (file_num() == 0) {
	scope = fileset->globals();
    } else {
	/*  find the file */
	file_set_entry_list_iter file_iter(fileset->file_list());
	while (!file_iter.is_empty()) {
	    file_set_entry *fse = file_iter.step();
	    if (fse->file_id() == file_num()) {
		scope = fse->symtab();
		break;
	    }
	}
	if (!scope) return NULL;
    }

    if (proc_sym_id() != 0) {
	proc_sym *pr = (proc_sym *)scope->lookup_sym_id(proc_sym_id());
	if (!pr) return NULL;
	assert(pr->is_proc());
	if (pr->block() == NULL) return NULL;
	scope = pr->block()->symtab();
    }

    return scope->lookup_sym_id(sym_id());
}


w_heap_table::~w_heap_table ()
{
    delete[] tbl;
}


w_heapobj *
w_heap_table::heap_obj (int n)
{
    assert_msg((n > 0) && (n <= num_heap_objs()),
	       ("invalid heap index %d", n));
    return &tbl[n - 1];
}


boolean
w_pval::aliased (w_pval *p, int *f, int *s)
{
    /*  compare the base objects */
    if (*p->base() != *base()) return FALSE;

    /*  compute the relative offset */
    if (f && s) {
	int str = int_gcd(p->stride(), stride());
	int off = p->offset() - offset();
	if (str != 0) {
	    off = off % str;
	    if (off < 0) off += str;
	}
	*f = off;
	*s = str;
    }

    return TRUE;
}


boolean
w_pval_list::aliased (w_pval_list *p, int *f, int *s)
{
    boolean result = FALSE;
    int off = 0;
    int str = 0;

    w_pval_list_iter iter1(p);
    while (!iter1.is_empty()) {
	w_pval v1 = iter1.step();
	if (aliased_helper(&v1, &off, &str)) {
	    if (f && s) {
		if (result) {
		    meet(f, s, off, str);
		} else {
		    *f = off;
		    *s = str;
		}
	    }
	    result = TRUE;
	}
    }

    return result;
}


w_pval_list *
sym_node_aliases (sym_node *s, base_symtab *scope, int clone_num)
{
    /*  only static variables are aliased */
    if (!s->is_var() || !((var_sym *)s)->is_static()) return NULL;

    /*  check if this global is aliased with extended parameters */
    while (scope->is_block()) {
	/*  search the symtab for "global_alias" annotations */
	if (scope->are_annotations()) {
	    annote_list_iter iter(scope->annotes());
	    while (!iter.is_empty()) {
		annote *an = iter.step();
		if (an->name() != k_wilbyr_global_alias) continue;
		w_global_alias *globa = WILBYR_GLOBAL_ALIAS(an);
		if ((globa->symbol() == s) &&
		    (globa->clone_num() == clone_num)) {
		    return globa->aliases();
		}
	    }
	}
	scope = scope->parent();
    }
    return NULL;
}


boolean
w_pval_list::aliased (sym_node *sym, base_symtab *scope, int clone_num,
		      int *f, int *s)
{
    w_pval_list *sym_aliases = sym_node_aliases(sym, scope, clone_num);
    if (sym_aliases) return aliased(sym_aliases, f, s);

    w_pval v1 = w_pval(w_obj(sym), 0, 0);
    int off = 0;
    int str = 0;

    boolean result = aliased_helper(&v1, &off, &str);
    if (f) *f = off;
    if (s) *s = str;
    return result;
}


boolean
w_pval_list::aliased_helper (w_pval *v1, int *off, int *str)
{
    int ftmp, stmp;
    boolean result = FALSE;

    w_pval_list_iter iter2(this);
    while (!iter2.is_empty()) {
	w_pval v2 = iter2.step();
	if (v2.aliased(v1, &ftmp, &stmp)) {
	    if (result) {
		meet(off, str, ftmp, stmp);
	    } else {
		*off = ftmp;
		*str = stmp;
	    }
	    result = TRUE;
	}
    }
    return result;
}


void
meet (int *off, int *str, int f, int s)
{
    int diff = *off - f;
    if (diff < 0) diff = -diff;
    *str = int_gcd(int_gcd(*str, s), diff);
    *off = ((*str == 0) ? f : f % *str);
}


/*
 *  Greatest common denominator.  This function either uses Euclid's
 *  algorithm to find the real gcd of the two values or it cheats and
 *  computes a simplified version where the gcd is either one of the
 *  inputs or unity.  The simplified version may not be much faster
 *  but it may also speed things up by reducing the number of location
 *  sets.
 */

int
int_gcd (int m, int n)
{
    if (m == 0) return n;
    if (n == 0) return m;

#ifdef FAKE_GCD
    if (m % n == 0) return n;
    if (n % m == 0) return m;
    return 1;
#else
    /* Euclid's algorithm from Knuth, vol. 1 */
    int r;
    while (TRUE) {
	r = m % n;
	if (r == 0) break;
	m = n;
	n = r;
    }
    return n;
#endif /* FAKE_GCD */
}

