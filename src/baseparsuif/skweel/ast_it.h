/* file "ast_it.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  General functions for parallelization and locality analysis  */

#ifndef AST_IT_H
#define AST_IT_H

#pragma interface

RCS_HEADER(ast_it_h, "$Id: ast_it.h,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")


// Associated with each for loop
//
struct it_for_annote {
    sym_node_list *modified;
    sym_node_list *privatizable;
    sym_node_list *reduction_vars;
    boolean cannot_transform;
};


// interesting statistics about this compilation

class it_stats {
public:
    int num;                             // loop nests
    int tiled,permute_not_tiled;         // nests with these applied
    int reversed,skewed;                 // enablers for permutation, tiling
    int dep_none,dep_scalar,dep_fp,dep_seq,dep_other;  // dependence types
    int dep_dv;                          // loops with only distance vectors
    int dep_dvx;                         // nearly distance vectors
    int bound_rect,bound_tri,bound_conv,bound_other;   // loop bound types
    int ref_simple,ref_i_plus_j,ref_i_i,ref_other;     // array reference types
    int depth[MAXDEPTH+1];               // a histogram of loop nest depths
    int depth_cannot[MAXDEPTH+1];        // untransformable nests of each depth
    int depth_goto[MAXDEPTH+1];          // depths of nests w/ gotos
    int depth_bounds[MAXDEPTH+1];        // depths of nests w/ bad loop bounds
    it_stats();
    void operator += (it_stats &x);      // add in some new statistics
    void print(FILE *f);
};

void it_init_proc(proc_sym *ps);

#endif /* AST_IT_H */
