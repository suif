/* file "matrix.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Generic matrix class for unimodular and tiling transformations.  
    Use in conjunction with VM_HEADER. */

#pragma implementation "matrix.h"

#define RCS_BASE_FILE matrix_cc

#include <suif.h>
#include <suifmath.h>
#include "vm_header.h"
#include "matrix.h"
#include "vector.h"

RCS_BASE("$Id: matrix.cc,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

#ifndef VM_E
	Need to define VM_E!!!!!!!!!!!!!!!!!!
#endif
#ifndef VM_E_PRINT
	Need to define VM_E_PRINT!!!!!!!!!!!!!!!!!!
#endif
#ifndef VM_E_IS_ZERO
	Need to define VM_E_IS_ZERO!!!!!!!!!!!!!!!!!!
#endif
#ifndef VM_E_ABS
	Need to define VM_E_ABS!!!!!!!!!!!!!!!!!!
#endif

tmatrix::~tmatrix()
{
	for(int i=0; i<c; i++)
		delete[] m[i];
	delete inter;
}

tmatrix::tmatrix(int sz1,int sz2):r(sz1),c(sz2),inter(0)
{
	assert(c<=MAXCOLS);
	for(int i=0; i<c; i++)
		m[i] = new VM_E[r];
}

tmatrix::tmatrix(tmatrix &a):r(a.r),c(a.c)
{
	assert(c<=MAXCOLS);
	int i;
	for(i=0; i<c; i++)
		m[i] = new VM_E[r];

	for(int j=0; j<c; j++)
		for(i=0; i<r; i++)
			this->elt(i,j) = a.elt(i,j);

	if(a.inter) {
		inter = new int[r];
		for(int i=0; i<r; i++)
			inter[i] = a.inter[i];
		for(i=0; i<c; i++)
			pivotcols[i] = a.pivotcols[i];
	}
	else
		inter = 0;
}

void tmatrix::operator =(tmatrix &a)
{
	if(this == &a) return;		// assignment to self

	assert(r == a.r && c == a.c);

	for(int j=0; j<c; j++)
		for(int i=0; i<r; i++)
			this->elt(i,j) = a.elt(i,j);

	if(a.inter) {
		if(inter == 0) inter = new int[r];
		for(int i=0; i<r; i++)
			inter[i] = a.inter[i];
		for(i=0; i<c; i++)
			pivotcols[i] = a.pivotcols[i];
	} else if(inter) {
		    delete[] inter;
		inter = 0;
	}
}

void tmatrix::print(FILE *f) FCONST
{
	for(int i=0; i<r; i++) {
		for(int j=0; j<c; j++) {
			if(j!=0) fprintf(f,"   ");
			VM_E_PRINT(f,this->elt(i,j));
		}
		fprintf(f,"\n");
	}
	if(inter) {
		fprintf(f,"    inter:[");
		for(int j=0; j<r; j++)
			fprintf(f,"%s%d",j==0?"":",",inter[j]);
		fprintf(f,"]\n");
		fprintf(f,"    pivotcols:[");
		for(j=0; j<c; j++) {
                    if(pivotcols[j] == FALSE) fprintf(f,"n");
                    else if(pivotcols[j] == TRUE) fprintf(f,"y");
                    else assert(0);
		}
                fprintf(f,"]\n");
	}
}

void tmatrix::transpose()
{
	assert(inter == 0);

	tmatrix tmp(c,r);
	for(int i=0; i<r; i++)
		for(int j=0; j<c; j++)
			tmp.elt(j,i) = this->elt(i,j);
	for(i=0; i<c; i++) {
		delete[] m[i];
		m[i] = 0;
	}
	c = tmp.c;
	r = tmp.r;
	for(i=0; i<c; i++) {
		m[i] = tmp.m[i];
		tmp.m[i] = 0;
	}
}

tmatrix tmatrix::operator *(tmatrix &a) FCONST
{
	assert(!inter && !a.inter);
	assert(c == a.r);
	tmatrix x(r,a.c);
	for(int i=0; i<r; i++)
		for(int j=0; j<a.c; j++) {
			VM_E t = 0;
			for(int k=0; k<c; k++)
				t += elt(i,k) * a.elt(k,j);
			x.elt(i,j) = t;
		}
	return x;
}

int tmatrix::factor_col(VM_E *w,int new_pivot_row)	// what to exchange with
{
	Linverse(w);
	if(new_pivot_row == r) return r;
	assert(new_pivot_row < r);
	int j = new_pivot_row;
	int p = j;
#ifdef VM_E_EXACT
	VM_E max = w[j];
	if(VM_E_IS_ZERO(max)) {
		for(int i=j+1; i<r; i++) {
			max = VM_E_ABS(w[i]);
			if(VM_E_IS_ZERO(max)) continue;
			else {
				p = i;
				break;
			}
		}
	}
#else
	VM_E max = VM_E_ABS(w[j]);
	for(int i=j+1; i<r; i++) {
		VM_E tmp = VM_E_ABS(w[i]);
		if(tmp > max) {
			max = tmp;
			p = i;
		}
	}
#endif
	assert(j <=p && p < r);
	if(p != j) {
		VM_E tmp = w[j];
		w[j] = w[p];
		w[p] = tmp;
	}
	if(!VM_E_IS_ZERO(w[j])) {
		for(int i=c+1; i<r; i++) {
			w[i] /= w[j];
		}
	}
	return p;
}

int tmatrix::factor_col_and_insert(VM_E *w,int insert_only_nonzero_piv)
{
	VM_E *newc = new VM_E[r];
	m[c] = newc;
	for(int i=0; i<r; i++)
		newc[i] = w[i];
	int curpivrow = 0;
	for(i=0; i<c; i++)
		curpivrow += pivotcols[i];
	if(curpivrow == r && insert_only_nonzero_piv)
		return 0;
	assert(curpivrow <= r);
	int rrr = factor_col(newc,curpivrow);
	assert((curpivrow <= rrr && rrr < r) || (rrr == r && curpivrow == rrr));

	if(insert_only_nonzero_piv && VM_E_IS_ZERO(newc[curpivrow])) {
		delete[] newc;
		m[c] = 0;
		return 0;
	}
	if(curpivrow < r) {
		inter[curpivrow] = rrr;
		if(rrr != curpivrow) {
			// newc is swapped, now swap prev guys
			for(int i=0; i<c; i++) {
				VM_E tmp = elt(rrr,i);
				elt(rrr,i) = elt(curpivrow,i);
				elt(curpivrow,i) = tmp;
			}
		}
		if(VM_E_IS_ZERO(newc[curpivrow]))
			pivotcols[c] = FALSE;
		else
			pivotcols[c] = TRUE;
	} else
		pivotcols[c] = FALSE;
	assert(c < MAXCOLS);
	c++;
	return 1;
}

void tmatrix::factor()
{
	assert(inter == 0);
	inter = new int[r];
	for(int rr=0; rr<r; rr++)
		inter[rr] = rr;

	int cold = c;
	c = 0;
	for(int j=0; j<cold; j++) {
		VM_E *hold = m[j];
		m[j] = 0;
		factor_col_and_insert(hold,0);
		delete[] hold;
	}
}

void tmatrix::unfactor()
{
	assert(inter);
	tmatrix l = L();
	tmatrix u = U();

	int *p = inter+r-1;
	for(int i=r-1; i>=0; i--,p--) {
		if(*p != i) {
			assert(*p > i);
			for(int j = 0; j<r; j++) {
				VM_E tmp = l.elt(i,j);
				l.elt(i,j) = l.elt(*p,j);
				l.elt(*p,j) = tmp;
			}
		}
	}
	*this = l*u;
	assert(!inter);
}

tmatrix tmatrix::L()
{
	assert(inter);

	tmatrix rval(r,r);

	for(int j=0; j<r; j++) {
		for(int i=0; i<r; i++)
			if(i<j) rval.elt(i,j) = VM_E(0);
			else if(i==j) rval.elt(i,j) = VM_E(1);
			else if(j<c) rval.elt(i,j) = this->elt(i,j);
			else rval.elt(i,j) = VM_E(0);
		}
	return rval;
}

tmatrix tmatrix::U()
{
	assert(inter);

	tmatrix rval(r,c);

	for(int j=0; j<c; j++) {
		for(int i=0; i<r; i++)
			if(i<=j) rval.elt(i,j) = elt(i,j);
			else rval.elt(i,j) = VM_E(0);
		}
	return rval;
}

void tmatrix::Linverse(VM_E *b)
{
	assert(inter);

	// do exchanges first
	int *p = inter;
	for(int i=0; i<r; i++,p++) {
		if(*p != i) {
			assert(*p > i);
			VM_E tmp = b[i];
			b[i] = b[*p];
			b[*p] = tmp;
		}
	}
	// now perform operations on column as dictated by L.
	for(int j=0; j<c; j++) {
		for(int i=j+1; i<r; i++) {
			b[i] -= b[j] * elt(i,j);
		}
	}
}

int tmatrix::last_nonzero_Urow() FCONST
{
	assert(inter);
	int rval = -1;
	for(int i=0; i<c; i++)
		rval += pivotcols[i];
	return rval;
}

int tmatrix::UbacksolveColumn(VM_E *w,int fvar1,VM_E *ans)
{
	// all free variables are assumed to be zero except the one
	// specified by fvar1.
	assert(inter);

	// first examine all zero rows
	int rr = last_nonzero_Urow();

	for(int ii = rr+1; ii<r; ii++) {
		if(!VM_E_IS_ZERO(w[ii]))
			return 0;
	}

	int i=rr;
	for(int j=c-1; j>=0; j--) {
		if(!pivotcols[j]) {
			if(j==fvar1) ans[j] = VM_E(1);
			else ans[j] = VM_E(0);
		}
		else {
			VM_E tmp = w[i];
			for(int jj = j+1; jj<c; jj++)
				tmp -= elt(i,jj) * ans[jj];
			ans[j] = tmp/elt(i,j);
			i--;
		}
	}
	return 1;
}

tvector_space tmatrix::kernel()
{
    tvector_space vs(c);
    assert(inter);
    VM_E *zeros = new VM_E[r];
    for(int i=0; i<r; i++) zeros[i] = VM_E(0);
    
    VM_E *ans = new VM_E[MAXCOLS];
    for(int f=0; f<c; f++) {
        // the kernel has a dimension for each non-pivot column
        if(!pivotcols[f]) {
            int ok = UbacksolveColumn(zeros,f,ans);
            assert(ok);
            ok = vs.insert(ans);
            assert(ok);
        }
    }
    delete[] zeros;
    delete[] ans;
    return vs;
}

tvector tmatrix::particular_solution(VM_E *w,int *ok)
{
	VM_E *ans = new VM_E[MAXCOLS];
	VM_E *hold = new VM_E[r];
	for(int i=0; i<r; i++) hold[i] = w[i];
	Linverse(hold);
	*ok = UbacksolveColumn(hold,-1,ans);
	delete[] hold;
	return tvector(ans,c);
}

void tmatrix::appendcol(VM_E *w)
{
	assert(inter == 0);
	assert(c < MAXCOLS);
	m[c] = new VM_E[r];
	for(int i=0; i<r; i++)
		m[c][i] = w[i];
	c++;
}

tmatrix tmatrix::inverse()
{
	assert(r == c);
	if(!inter) factor();
	for(int i=0; i<r; i++)
		assert(pivotcols[i]);
	tmatrix ans(r,0);
	VM_E *w = new VM_E[MAXCOLS];
	VM_E *ww = new VM_E[MAXCOLS];
	for(i=0; i<r; i++) {
		for(int j=0; j<r; j++) w[j] = VM_E(i==j);
		Linverse(w);
		int ok = UbacksolveColumn(w,-1,ww);
		assert(ok);
		ans.appendcol(ww);
	}
	delete[] w;
	delete[] ww;
	return ans;
}
