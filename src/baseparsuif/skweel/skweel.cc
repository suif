/* file "skweel.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Main program */

#pragma implementation "skweel.h"

#define RCS_BASE_FILE skweel_cc

#include "skweel.h"
#include "ast_it.h"
#include "params.h"

#include <annotes.h>
RCS_BASE("$Id: skweel.cc,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

//
// Global flags
//
boolean register_block = FALSE;
boolean is_fortran = FALSE;
int cache_size = 65536;
double fill_factor = 1.0;
int line_size = 16;
int tile_size = 0;
int parallelism_level = 1;
boolean normalize_only = FALSE;
boolean no_scalar_privatization = FALSE;
boolean do_not_tile = FALSE;
boolean coalesce = FALSE;
boolean innermost_for_registers = FALSE;
double small_loop_cutoff = 0.0;
int small_bound_cutoff = 0;
boolean print_parallelism_info = FALSE;
boolean no_doall_regions = FALSE;
boolean no_doacross = FALSE;
boolean no_locality = FALSE;

boolean gather_statistics = FALSE;
int exact = TRUE;
int quit_after = 100000;
boolean print_summary = FALSE;

int tdebug[256];
dependency_test *deptest;
char *progname;
char *fname=0;

//
// Annotations:
//
const char *k_it_stats;
const char *k_it_for;
const char *k_begin_fully_permutable;
const char *k_end_fully_permutable;
const char *k_begin_parallel_region;
const char *k_end_parallel_region;
const char *k_privatized;
const char *k_reduced;
const char *k_reduced_gen;
const char *k_small_bound;
const char *k_locality;
const char *k_wilbyr_run;
const char *k_doacross;
const char *k_doall_region;
const char *k_tile_loops;
const char *k_tileable_loops;
const char *k_contains_call;

// From oynk/moo
const char *k_privatizable;
const char *k_need_finalization;
const char *k_iv_live;

// From reduction
const char *k_reduction;
const char *k_reduction_gen;

// From front-end
const char *k_C_pragma;
const char *k_chaotic;


void it_initialize()
{
  k_param_usage = lexicon->enter("param_usage")->sp;
  k_it_for = lexicon->enter("it_for")->sp;
  k_it_stats = lexicon->enter("it_stats")->sp;
  k_privatizable = lexicon->enter("privatizable")->sp;
  k_need_finalization = lexicon->enter("need finalization")->sp;
  k_iv_live = lexicon->enter("iv live")->sp;
  k_pure_function = lexicon->enter("pure function")->sp;
  k_C_pragma = lexicon->enter("C pragma")->sp;
  k_chaotic = lexicon->enter("chaotic")->sp;
  k_wilbyr_run = lexicon->enter("wilbyr_run")->sp;

  ANNOTE(k_begin_fully_permutable, "begin_fully_permutable", TRUE);
  ANNOTE(k_end_fully_permutable, "end_fully_permutable", TRUE);
  ANNOTE(k_begin_parallel_region, "begin_parallel_region", TRUE);
  ANNOTE(k_end_parallel_region, "end_parallel_region", TRUE);
  ANNOTE(k_privatized, "privatized", TRUE);
  ANNOTE(k_reduction, "reduction", TRUE);
  ANNOTE(k_reduction_gen, "reduction_gen", TRUE);
  ANNOTE(k_reduced, "reduced", TRUE);
  ANNOTE(k_reduced_gen, "reduced_gen", TRUE);
  ANNOTE(k_small_bound, "small_bound", TRUE);
  ANNOTE(k_locality, "locality", TRUE);
  ANNOTE(k_doacross, "doacross", TRUE);
  ANNOTE(k_doall_region, "doall_region", TRUE);
  ANNOTE(k_tile_loops, "tile_loops", TRUE);
  ANNOTE(k_tileable_loops, "tileable_loops", TRUE);
  ANNOTE(k_contains_call, "contains_call", TRUE);
}


static int atoikm(char *s)
{
    char *terminator;
    int v = (int) strtol(s,&terminator,10);
    switch(*terminator) {
    case 'k':
    case 'K':
        v *= 1024;
        break;
    case 'm':
    case 'M':
        v *= (1024 * 1024);
	break;
    }
    return v;
}

static it_stats total_stats;
int main(int argc,char **argv)
{
    progname = argv[0];
    deptest = new dependency_test;

    int dump_flag = 0;
    char *debug_arg = NULL;
    char *cache_size_arg = NULL;
    char *line_size_arg = NULL;

    start_suif(argc, argv);


    static cmd_line_option option_table[] =
      {
	{CLO_INT,    "-A",              "0",       &dump_flag},
        {CLO_NOARG,  "-D",              NULL,      &print_parallelism_info},
        {CLO_NOARG,  "-N",              NULL,      &normalize_only},
	{CLO_INT,    "-P",              "1",       &parallelism_level},
	{CLO_INT,    "-Q",              "100000",  &quit_after},
	{CLO_NOARG,  "-S",              NULL,      &no_scalar_privatization},
	{CLO_NOARG,  "-T",              NULL,      &do_not_tile},
	{CLO_NOARG,  "-V",              NULL,      &print_parallelism_info},
	{CLO_NOARG,  "-W",              NULL,      &gather_statistics},
	{CLO_NOARG,  "-2",              NULL,      &coalesce},
	{CLO_NOARG,  "-b",              NULL,      &register_block},
        {CLO_STRING, "-c",              "65536",   &cache_size_arg},
        {CLO_STRING, "-d",              "",        &debug_arg},
	{CLO_DOUBLE, "-f",              "1.0",     &fill_factor},
	{CLO_NOARG,  "-i",              NULL,      &innermost_for_registers},
        {CLO_STRING, "-l",              "16",      &line_size_arg},
	{CLO_NOARG,  "-p",              NULL,      &print_summary},
	{CLO_INT,    "-t",              "0",       &tile_size},
	{CLO_INT,    "-y",              "0",       &small_bound_cutoff},
	{CLO_DOUBLE, "-z",              "0.0",     &small_loop_cutoff},
	{CLO_NOARG,  "-no-regions",     "",        &no_doall_regions},
	{CLO_NOARG,  "-no-doacross",    "",       &no_doacross},
	{CLO_NOARG,  "-no-locality",    "",        &no_locality}
      };

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    if (argc != 3) {
        fprintf(stderr,"usage: %s [options] src dest\n",progname);
        exit(1);
    }

    char *input_file = argv[1];
    char *output_file = argv[2];

    // Handle debug flags
    //
    for(char *s = debug_arg; *s; s++)  tdebug[*s-'\0']++;

    // Cache size flag
    if (cache_size_arg) {
	cache_size = atoikm(cache_size_arg);
	assert(cache_size);
    }

    // Line size flag
    if (line_size_arg) {
	line_size = atoikm(line_size_arg); 
	assert(line_size);
    }

    if (exact)  deptest->do_exact();

    fileset->add_file(input_file, output_file);

    fileset->reset_iter();
    file_set_entry *fse;
    it_initialize();
    wilbyr_init_annotes();

    while ((fse = fileset->next_file())) {
        if (print_parallelism_info) {
          printf("Processing fileset %s: in = %s, out = %s\n",  
	       fse->name(), argv[1], argv[2]);
        }

        fse->reset_proc_iter();
        proc_sym *ps;

        while ((ps = fse->next_proc())) {
          if (ps->is_readable() && !ps->is_written()) {
              is_fortran = ps->src_lang() == src_fortran;

	      if (is_fortran)
		  ps->read_proc(TRUE, TRUE); 
	      else
		  ps->read_proc(TRUE, FALSE); 

	      it_init_proc(ps);  // stuff ast_it_proc used to do

        
              if (print_parallelism_info) {
		printf("transforming %s()\n", ps->name());
		fflush(stdout);
	      }
        
	      if (dump_flag & 1) {
		printf("------ %s before transformation -------\n",ps->name());
		ps->block()->print(stdout);
	      }
        
	      if(quit_after-- > 0) {
		if(quit_after == 0) {
		  printf("THIS ONE %s() IS THE LAST ONE\n", ps->name());
		  fflush(stdout);
		}

		find_regions_and_transform(ps->block());
		fill_in_access(ps->block());
	      }
        
	      if (dump_flag & 2) {
		printf("------ %s after transformation -------\n", ps->name());
		fill_in_access(ps->block());
		fileset->globals()->print(stdout);
		fse->symtab()->print(stdout);
		ps->block()->print(stdout);
	      }

	      if(print_summary) {
		printf("SUMMARY:\n");
		quick_summary(ps->block()->body(), 1);
	      }
        
              it_stats *stats = 
		  (it_stats *) ps->block()->get_annote(k_it_stats);
	      assert(stats);
	      total_stats += *stats;

	      if (gather_statistics) {
		stats->print(stdout);
	      }
	      delete stats;

              ps->write_proc(fse);
	      ps->flush_proc();

	  } // if 
        } // while file
    } // while fileset

    delete fileset;
    fileset = NULL;

    if(gather_statistics) {
        printf("*********** FINAL STATISTICS ************\n");
        total_stats.print(stdout);
    }
    return 0;
}
