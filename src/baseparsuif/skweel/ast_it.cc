/* file "ast_it.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  General functions for parallelization and locality analysis  */

#pragma implementation "ast_it.h"

#define RCS_BASE_FILE ast_it_cc

#include <useful.h>
#include "skweel.h"
#include "ast_it.h"
#include "params.h"

RCS_BASE("$Id: ast_it.cc,v 1.2 1999/08/25 03:27:08 brm Exp $")

/* Local functions */

static void remove_reductions_tree(tree_node *tn, void *x);
static void remove_reductions_gen_tree(tree_node *tn, void *x);
static boolean one_element_dim(type_node *curr_type);

boolean has_return_instr(tree_node_list *tnl)
{
    tree_node_list_iter ai(tnl);
    while (!ai.is_empty()) {
	tree_node *n = ai.step();
	switch (n->kind()) {
	    case TREE_INSTR: {
		tree_instr *ti = (tree_instr *) n;
		if (ti->instr()->opcode() == io_ret) return TRUE;
		break;
	    }

	    case TREE_IF: {
		tree_if *ti = (tree_if *) n;
		if (has_return_instr(ti->then_part())) return TRUE;
		if (has_return_instr(ti->else_part())) return TRUE;
		break;
	    }

	    case TREE_BLOCK: {
		tree_block *tb = (tree_block *) n;
		if (has_return_instr(tb->body())) return TRUE;
		break;
            }

	    case TREE_LOOP: {
		tree_loop *tl = (tree_loop *) n;
		if (has_return_instr(tl->body())) return TRUE;
		break;
	    }

	    case TREE_FOR: {
		tree_for *tf = (tree_for *) n;
		if (has_return_instr(tf->body())) return TRUE;
		if (has_return_instr(tf->landing_pad())) return TRUE;
		break;
            }
	}
    }

    return FALSE;
}


boolean has_loop_inside(tree_node_list *tnl)
{
    tree_node_list_iter ai(tnl);
    while (!ai.is_empty()) {

	tree_node *n = ai.step();

	switch (n->kind()) {

	    case TREE_LOOP:
	    case TREE_FOR:
		return TRUE;

	    case TREE_IF: {
		tree_if *ni = (tree_if *)n;
		if (has_loop_inside(ni->header()) ||
		    has_loop_inside(ni->then_part()) ||
		    has_loop_inside(ni->else_part())) {
		    return TRUE;
		}
		break;
	    }

            case TREE_BLOCK:
		if (has_loop_inside(((tree_block *)n)->body()))
		    return TRUE;
		break;

	    case TREE_INSTR:
		break;
	}
    }
    return FALSE;
}


// further in ones are bad, except in for bodies.
static tree_for *find_one_for_on_list(tree_node_list *l, int *numfound)
{
    assert(l);
    *numfound = 0;
    tree_for *retval = 0;
    
    tree_node_list_iter ai(l);
    while (!ai.is_empty()) {
        tree_node *n = ai.step();
        
        switch(n->kind()) {
        case TREE_INSTR:
            break;
        case TREE_IF: {
            int cnt = 0;
            find_one_for_on_list(((tree_if *)n)->header(), &cnt);
            if (cnt > 0) {*numfound = 2; return 0;}
            find_one_for_on_list(((tree_if *)n)->then_part(), &cnt);
            if(cnt > 0) {*numfound = 2; return 0;}
            find_one_for_on_list(((tree_if *)n)->else_part(), &cnt);
            if(cnt > 0) {*numfound = 2; return 0;}
            break;
          }
        case TREE_LOOP:
            *numfound = 2;
            return 0;
        case TREE_FOR:
            if(*numfound) {
                *numfound = 2;
                return 0;
            } else {
                *numfound = 1;
                retval = (tree_for *) n;
            }
            break;
        case TREE_BLOCK: {
	    int cnt;
	    tree_for *ret;
            ret = find_one_for_on_list(((tree_block *)n)->body(), &cnt);

	    *numfound += cnt;
	    if (*numfound > 1) return 0; 
	    else if (cnt == 1) retval = ret;
	    break;
        }
      }
    }
    return retval;
}

tree_for *only_inner_for(tree_for *tf)
{
    int cnt = 0;
    tree_for *rval = find_one_for_on_list(tf->body(), &cnt);
    assert((rval != 0) == (cnt == 1));
    return rval;
}


boolean check_call(instruction *ins)
{
    if (ins->opcode() == io_cal) {
        in_cal *ic = (in_cal *) ins;

	if (!ic->peek_annote(k_reduction_gen)) {
	  proc_sym *sn = proc_for_call(ic);
	  if (!sn) return TRUE;

	  if (!sn->peek_annote(k_pure_function)) {
	    if (tdebug['t']>1 || print_parallelism_info) {
	      printf("Found call to %s\n", sn->name());
	    }

	    int s = simple_ipa_call_info((in_cal *)ins);
	    if (s) return TRUE;
	  }
	}
    }

    // Treat generic instructions as call to unknown functions
    //
    else if (ins->opcode() == io_gen) return TRUE;  

    return FALSE;
}

static void findcall_instr(instruction *i, void *x)
{
    boolean *res = (boolean *) x;

    if (check_call(i)) *res = TRUE;
}


static boolean findcall_src(instruction *, operand *r, void *x)
{
    if (r->is_instr()) {
	boolean *res = (boolean *) x;

	if (check_call(r->instr())) *res = TRUE;
	else r->instr()->src_map(findcall_src, x);
    }

    return FALSE;
}


static boolean findcall_op(operand op)
{
    boolean result = FALSE;
    findcall_src(NULL, &op, &result);
    return result;
}



static boolean findcall(tree_node_list *l, boolean recursive)
{
    tree_node_list_iter ai(l);
    while(!ai.is_empty()) {
        tree_node *n = ai.step();
        switch(n->kind()) {
        case TREE_LOOP:
            if(recursive) {
                tree_loop *nl = (tree_loop *)n;
                if(findcall(nl->body(),1) || findcall(nl->test(),1)) {
                    return TRUE;
		}
            }
            break;
        case TREE_FOR: {
            tree_for *nf = (tree_for *)n;
            boolean reslb = findcall_op(nf->lb_op());
            boolean resub = findcall_op(nf->ub_op());
            boolean resstep = findcall_op(nf->step_op());

            if(reslb || resub || resstep) {
		return TRUE;
            }

            if(recursive && findcall(nf->body(), 1)) {
		return TRUE;
	    }
            break;
          }

        case TREE_IF: {
            tree_if *ni = (tree_if *) n;
            if(findcall(ni->header(),recursive) ||
               findcall(ni->then_part(),recursive) ||
               findcall(ni->else_part(),recursive)) {
                return TRUE;
	    }
            break;
          }

        case TREE_INSTR: {
            tree_instr *ti = (tree_instr *) n;
	    
            boolean ret = FALSE;
            ti->instr_map(findcall_instr, &ret);
            if (ret) {
		return TRUE;
            }
            break;
          }

        case TREE_BLOCK:{
	    tree_block *tb = (tree_block *) n;
            if (recursive && findcall(tb->body(), TRUE)) {
		return TRUE;
	    }
	    break;
          }
        }
    }

    return FALSE;
}

boolean is_ncall_in_body(tree_for *tf, boolean recursive)
{
    boolean res = findcall(tf->body(), recursive);

    if (res) {
	tf->append_annote(k_contains_call);
	if (tdebug['t'] > 1) {
	    printf("Cannot transform loop `%s' because of function call\n", 
		   tf->index()->name());
	}
    }

    return res;
}


// Mark the var privatizable in all enclosing loops
//
static void mark_privatizable(var_sym *vs, tree_node_list *tnl)
{
    tree_node *curr_node = tnl->parent();

    while (curr_node) {
	if (curr_node->is_for() && 
	    curr_node->scope()->is_ancestor(vs->parent())) {

	    annote *an1 = curr_node->annotes()->peek_annote(k_privatized);
	    annote *an2 = curr_node->annotes()->peek_annote(k_privatizable);
	    
	    if (!an1) 
	      {
		an1 = new annote(k_privatized, new immed_list());
		curr_node->annotes()->append(an1);
	      }
	    if (!an2)
	      {
		an2 = new annote(k_privatizable, new immed_list());
		curr_node->annotes()->append(an2);
	      }
	    
	    an1->immeds()->append(immed(vs));
	    an2->immeds()->append(immed(vs));
	    break;
	}

        if (curr_node->parent()) curr_node = curr_node->parent()->parent();
	else break;
    }
}

void move_out_upper_bound(tree_for *tf)
{
    operand cur_ub = tf->ub_op();
    cur_ub.remove();

    var_sym *vs = tf->scope()->new_unique_var(tf->index()->type(), "_ub");
    tree_node *tn = create_assignment(vs, cur_ub);

    if (tn->is_instr())
      {
	tree_instr *ti = (tree_instr *) tn;
	ti->instr()->append_annote(k_fred);
      }
    else tn->append_annote(k_fred);
    
    tf->parent()->insert_before(tn, tf->list_e());
    tf->set_ub_op(operand(vs));

    // Upper bound is privatizable to all enclosing loops:
    mark_privatizable(vs, tf->parent());
}

void move_out_lower_bound(tree_for *tf)
{
    operand cur_lb = tf->lb_op();
    cur_lb.remove();

    var_sym *vs = tf->scope()->new_unique_var(tf->index()->type(), "_lb");
    tree_node *tn = create_assignment(vs, cur_lb);

    if (tn->is_instr())
      {
	tree_instr *ti = (tree_instr *) tn;
	ti->instr()->append_annote(k_fred);
      }
    else tn->append_annote(k_fred);

    tf->parent()->insert_before(tn, tf->list_e());
    tf->set_lb_op(operand(vs));

    // Lower bound is privatizable to all enclosing loops:
    mark_privatizable(vs, tf->parent());
}


// ----------------------------------------------------------

it_stats::it_stats()
{
    int i;
    num = 0;
    tiled = permute_not_tiled = 0;
    reversed = skewed = 0;
    dep_scalar = dep_none = dep_fp = dep_seq = dep_other = 0;
    dep_dv = dep_dvx = 0;
    bound_rect = bound_tri = bound_conv = bound_other = 0;
    ref_simple = ref_i_plus_j = ref_i_i = ref_other = 0;
    for(i=0; i<=MAXDEPTH; i++) {
        depth[i] = 0;
        depth_cannot[i] = 0;
        depth_goto[i] = 0;
        depth_bounds[i] = 0;
    }
}

void it_stats::print(FILE *f) {
    int i;

    fprintf(f,"****** nests %d ******\n",num);
    if(num) {
        fprintf(f,"tiled %d,permute not tiled %d (skewed %d,reversed %d)\n",tiled,permute_not_tiled,skewed,reversed);
        fprintf(f,"DEPS: none %d,scalar %d,fp %d,sequential %d,other %d\n",
                dep_none,dep_scalar,dep_fp,dep_seq,dep_other);
        fprintf(f,"DV DEPS: none %d,scalar %d,dv %d,almost dv %d\n",
                dep_none,dep_scalar,dep_dv,dep_dvx);
        fprintf(f,"BOUNDS: rect %d,tri %d,convex %d,other %d\n",
                bound_rect,bound_tri,bound_conv,bound_other);
        fprintf(f,"REFS: simple only %d,[i+j] %d,[i,i] %d,[ugly] %d\n",
                ref_simple,ref_i_plus_j,ref_i_i,ref_other);
        fprintf(f,"DEPTHS:");
        for(i=0; i<MAXDEPTH; i++) {
            if(depth[i])
                fprintf(f,":: depth %d has %d",i,depth[i]);
        }
        fprintf(f,"\n");
        fprintf(f,"DEPTHS CANNOT TRANSFORM:");
        for(i=0; i<MAXDEPTH; i++) {
            if(depth_cannot[i])
                fprintf(f,":: depth %d has %d",i,depth_cannot[i]);
        }
        fprintf(f,"\n");
        fprintf(f,"DEPTHS GOTOS:");
        for(i=0; i<MAXDEPTH; i++) {
            if(depth_goto[i])
                fprintf(f,":: depth %d has %d",i,depth_goto[i]);
        }
        fprintf(f,"\n");
        fprintf(f,"DEPTHS BOUNDS:");
        for(i=0; i<MAXDEPTH; i++) {
            if(depth_bounds[i])
                fprintf(f,":: depth %d has %d",i,depth_bounds[i]);
        }
        fprintf(f,"\n");
    }
    fflush(f);
}

void it_stats::operator += (it_stats &x) {
    int i;
    num += x.num;
    tiled += x.tiled;
    permute_not_tiled += x.permute_not_tiled;
    reversed += x.reversed;
    skewed += x.skewed;
    dep_none += x.dep_none;
    dep_fp += x.dep_fp;
    dep_scalar += x.dep_scalar;
    dep_seq += x.dep_seq;
    dep_other += x.dep_other;
    dep_dv += x.dep_dv;
    dep_dvx += x.dep_dvx;
    bound_rect += x.bound_rect;
    bound_tri += x.bound_tri;
    bound_conv += x.bound_conv;
    bound_other += x.bound_other;
    ref_simple += x.ref_simple;
    ref_i_plus_j += x.ref_i_plus_j;
    ref_i_i += x.ref_i_i;
    ref_other += x.ref_other;
    for(i=0; i<MAXDEPTH; i++) {
        depth[i] += x.depth[i];
        depth_cannot[i] += x.depth_cannot[i];
        depth_goto[i] += x.depth_goto[i];
        depth_bounds[i] += x.depth_bounds[i];
    }
}


static void sce_info(tree_node_list *l)
{
    tree_node_list_iter ai(l);
    while(!ai.is_empty()) {
        tree_node *n = ai.step();
        switch(n->kind()) {
        case TREE_INSTR: 
            // note that only the direct FOR loop is given a "modified"
            // pr, so if it's not privatizable, then someone further down
            // must cry.
            {
                operand dest = ((tree_instr *)n)->instr()->dst_op();
                if (dest.is_symbol()) {
                    tree_node_list *bdy = l;
                    for (tree_node *a = l->parent(); a && a->parent(); bdy = a->parent(), a=bdy->parent()) {
                        if (a->kind() == TREE_FOR) {
                            it_for_annote *an = (it_for_annote *) a->peek_annote(k_it_for);
                            assert(an);
                            an->modified->append(dest.symbol());
                            break;
                        }
                    }
                }
            }
            break;
        case TREE_FOR: {
            // note that only the direct FOR loop is given a "modified"
            // pr, so if it's not privatizable, then someone further down
            // must cry.

            // note which scalars are modified, privatizable.
            // no finalization in this implementation.

            tree_for *f = (tree_for *) n;

            tree_node_list *bdy = l;
            for (tree_node *node = l->parent(); node && node->parent(); 
		 bdy = node->parent(), node=bdy->parent()) {

                if (node->kind() == TREE_FOR) {
		    it_for_annote *curr_an = 
			(it_for_annote *) node->peek_annote(k_it_for);
		    assert(curr_an);
		    curr_an->modified->append(f->index());
		    break;
		}
            }

            int i;
	    it_for_annote *ifa = new it_for_annote();
	    ifa->privatizable = new sym_node_list();
	    ifa->modified = new sym_node_list();
	    ifa->reduction_vars = new sym_node_list();
	    ifa->cannot_transform = FALSE;
	    f->append_annote(k_it_for, ifa);

            // these must go here, modified is defined early enough
            sce_info(f->body());

	    // Read annotations off of for loop:
            //
	    annote_list_iter ali(f->annotes());
	    while (!ali.is_empty()) {
		annote *a = ali.step();
		
		if (((a->name()==k_privatizable) || (a->name()==k_privatized))
		    && !no_scalar_privatization) {
		    for(i=0; i < a->immeds()->count(); i++) {
			sym_node *sn = (*(a->immeds()))[i].symbol();

			if (sn->is_var()) {
			    ifa->privatizable->append(sn);
			}
		    }
		} // privatizable

		else if (a->name() == k_reduction) {
		    sym_node *sn = (*(a->immeds()))[1].symbol();
		    assert(sn->is_var());
		    var_sym *reduction_var = (var_sym *) sn;
		    boolean has_one_elem = 
			one_element_dim(reduction_var->type());

		    if ((!known_bounds(reduction_var->type())) || 
			(has_one_elem &&
			 reduction_var->type()->unqual()->is_array())) {
		    // This means we can't figure out the size & can't 
		    // allocate storage for this variable, so we give up on
		    // the reduction.  The latter case (array of size 1)
		    // means that we potentially have a Fortran "pointer"
                    // and we can't allocate the memory.
                    //
		        f->body()->map(remove_reductions_tree, reduction_var, 
				       TRUE, TRUE);
		        f->annotes()->remove(f->annotes()->lookup(a));
		        delete a;
                    }
                
		    else if (reduction_var->is_scalar()) {
		        ifa->reduction_vars->append(sn);
		    }
	        } // reduction

		else if (a->name() == k_reduction_gen) {
		    immed_list *il = a->immeds();
		    immed_list_iter ili(il);
		    immed im = ili.step();  // proc
		    im = ili.step();  // ic
		    while (!ili.is_empty()) {
			im = ili.step();
			sym_node *sn = im.symbol();
			assert(sn->is_var());
			var_sym *reduction_var = (var_sym *) sn;
			boolean has_one_elem = 
			    one_element_dim(reduction_var->type());

			if ((!known_bounds(reduction_var->type())) || 
			    (has_one_elem &&
			     reduction_var->type()->unqual()->is_array())) {
			// This means we can't figure out the size & can't 
			// allocate storage for this variable, so we give up on
			// the reduction.  The latter case (array of size 1)
			// means that we potentially have a Fortran "pointer"
			// and we can't allocate the memory.
			//
			    f->body()->map(remove_reductions_gen_tree,
					   reduction_var, TRUE, TRUE);
			    f->annotes()->remove(f->annotes()->lookup(a));
			    delete a;
			}
                
			else if (reduction_var->is_scalar()) {
			    ifa->reduction_vars->append(sn);
			}
		    }
	        } // reduction_gen
		    
                else if (a->name() == k_need_finalization) {
                    for(i=0; i<a->immeds()->count(); i++) {
		        immed im = (*(a->immeds()))[i];
  		        assert(im.is_symbol());
		        sym_node *sn = im.symbol();
		        sym_node_list_e *e = ifa->privatizable->lookup(sn);
		        if (e) ifa->privatizable->remove(e);
                    }
	        }

	        else if (a->name() == k_iv_live) {
                    ifa->cannot_transform++;
                    printf("%s(): ",f->proc()->name());
                    printf("(%s): index live, so cannot transform\n",
                        f->index()->name());
                } // iv_live
            }

            // no indices outside are privatizable here, because
            // too complicated.
            // 
            for(tree_node *aa = f; aa && aa->parent(); 
                                                 aa = aa->parent()->parent()) {
                if(aa->kind() == TREE_FOR) {
                    sym_node *ind = ((tree_for *)aa)->index();
		    sym_node_list_e *e = ifa->privatizable->lookup(ind);
                    if (e) ifa->privatizable->remove(e);
                }
            }

            // If modified but not privatizable, too bad.
            //
            sym_node_list_iter ri(ifa->modified);
            tree_for *fouter = 
               (tree_for *) next_further_out((tree_node *) f, TREE_FOR);

            while(!ri.is_empty()) {
                sym_node *r = ri.step();
                if(!ifa->privatizable->lookup(r) && 
		   !ifa->reduction_vars->lookup(r)) {

                    ifa->cannot_transform = TRUE;
                    if(fouter) {
			it_for_annote *ifa_outer = 
			    (it_for_annote *) fouter->peek_annote(k_it_for);
                        ifa_outer->modified->append(r);
		    }
                    if (ifa->cannot_transform && tdebug['s']) {
                        printf("%s(): ", f->proc()->name());
                        printf("%s: caused scalar privatization problems\n", 
                               r->name());
                    }
                }
            }

	    immed_list *priv_immeds = new immed_list();
            ri.reset(ifa->privatizable);
            while (!ri.is_empty()) {
                sym_node *sn = ri.step();
  	        if (f->scope()->is_ancestor(sn->parent())) 
                    priv_immeds->append(immed(sn));
            }

            if (!priv_immeds->is_empty()) 
                f->append_annote(k_privatized, priv_immeds);

            break;
	}

        case TREE_IF:
            sce_info(((tree_if *)n)->header());
            sce_info(((tree_if *)n)->then_part());
            sce_info(((tree_if *)n)->else_part());
            break;
        case TREE_LOOP:
            sce_info(((tree_loop *)n)->body());
            sce_info(((tree_loop *)n)->test());
            break;
        case TREE_BLOCK: {
	    // Add all scalars declared in this blocks scope to privatizable
            // lists of all enclosing loops:
	    tree_block *blk = (tree_block *) n;
	    block_symtab *blk_tab = blk->symtab();
            sym_node_list_iter snli(blk_tab->symbols());
	    while (!snli.is_empty()) { 
                sym_node *sn = snli.step();
                if (sn->is_var() && ((var_sym *)sn)->is_scalar()) {

                    for(tree_node *aa = blk; aa && aa->parent(); 
                                                 aa = aa->parent()->parent()) {

                        if(aa->kind() == TREE_FOR) {
			    it_for_annote *curr_an = 
			        (it_for_annote *) aa->peek_annote(k_it_for);
			    assert(curr_an);
			    curr_an->privatizable->append(sn);
			    break;
                        }
		    }
		}
            }
	  
            sce_info(((tree_block *)n)->body());
            break;
	}
          
      }
    }
}

#define SPACE(d) {int i;for(i=0; i<d; i++) {putchar(' ');putchar(' ');}}

void quick_summary(tree_node_list *l,int depth)
{
    tree_node_list_iter ai(l);
    while(!ai.is_empty()) {
        tree_node *n = ai.step();
        switch(n->kind()) {
        case TREE_INSTR:
            break;
        case TREE_IF:
            SPACE(depth)
            printf("IF-THEN\n");
            quick_summary(((tree_if *)n)->then_part(),depth+1);
            SPACE(depth)
            printf("ELSE\n");
            quick_summary(((tree_if *)n)->else_part(),depth+1);
            break;
        case TREE_FOR: {
            tree_for *f = (tree_for *)n;
	    it_for_annote *ifa = (it_for_annote *) f->peek_annote(k_it_for);
	    if (ifa) {
		SPACE(depth)
  	        printf("FOR ");
		printf(" (%s): can%s transform",
		       f->index()->name(),ifa->cannot_transform?"not":"");

		annote *doall = f->annotes()->peek_annote(k_doall);
		annote *tiled = f->annotes()->peek_annote(k_tiled);
		annote *tiled_c = f->annotes()->peek_annote(k_tiledc);
		if (doall) doall->print(stdout);
		if(tiled) tiled->print(stdout);
		if(tiled_c) tiled_c->print(stdout);
		putchar('\n');

		if(ifa->modified->count() > 0) {
		    SPACE(depth+1)
  		    printf("modified: ");
		    sym_node_list_iter snli(ifa->modified);
		    while (!snli.is_empty()) {
			sym_node *sn = snli.step();
			printf(" %s%s", sn->name(),
			       (snli.is_empty() ? "" : ","));
		    }
		    printf("\n");
		}
		if(ifa->privatizable->count() > 0) {
		    SPACE(depth+1)
		    printf("privatizable: ");
		    sym_node_list_iter snli(ifa->privatizable);
		    while (!snli.is_empty()) {
			sym_node *sn = snli.step();
			printf(" %s%s", sn->name(), 
			       (snli.is_empty() ? "" : ","));
		    }
		    printf("\n");
		}
		if(ifa->reduction_vars->count() > 0) {
		    SPACE(depth+1)
                    printf("reduction_vars: ");
		    sym_node_list_iter snli(ifa->reduction_vars);
		    while (!snli.is_empty()) {
			sym_node *sn = snli.step();
			printf(" %s%s", sn->name(), 
			       (snli.is_empty() ? "" : ","));
		    }
		    printf("\n");
		}
	    }
            quick_summary(f->body(),depth+1);
            break;
          }
        case TREE_LOOP:
            SPACE(depth)
            printf("LOOP\n");
            quick_summary(((tree_loop*)n)->body(),depth+1);
            break;
        case TREE_BLOCK:
            SPACE(depth)
            printf("BLOCK\n");
            quick_summary(((tree_block*)n)->body(),depth+1);
            break;
        }

    }
}

#undef SPACE

// ----------------------------------------------------------


void it_init_proc(proc_sym *ps)
{
    tree_proc *tp = ps->block();
    it_stats *stats = new it_stats();
    tp->append_annote(k_it_stats, stats);

    fill_in_access(tp);

    sce_info(tp->body());                  // Adds it_for annotes
    annotate_param_usage(tp);              // Adds param_info annote
                                           //   and any alias information
    if(tdebug['A']) quick_summary(tp->body(),0);
}


static void remove_reductions_tree(tree_node *tn, void *x)
{
    annote *an;
    if (tn->kind() == TREE_INSTR) {
        tree_instr *ti = (tree_instr *) tn;

        if ((an = ti->instr()->annotes()->peek_annote(k_reduction))) {
	    sym_node *sn = (*(an->immeds()))[1].symbol();
	    assert(sn->is_var());
	    var_sym *annote_var = (var_sym *) sn;
	    var_sym *reduction_var = (var_sym *) x;

	    if (annote_var == reduction_var) {
		an = ti->instr()->annotes()->get_annote(k_reduction);
		delete an;
	    }
        }
    }
}


static void remove_reductions_gen_tree(tree_node *tn, void * /* x */)
{
    annote *an;
    if (tn->kind() == TREE_INSTR) {
        tree_instr *ti = (tree_instr *) tn;

        if ((an = ti->instr()->annotes()->peek_annote(k_reduction_gen))) {
//	    sym_node *sn = (*(an->immeds()))[1].symbol();
//	    assert(sn->is_var());
//	    var_sym *annote_var = (var_sym *) sn;
//	    var_sym *reduction_var = (var_sym *) x;

//	    if (annote_var == reduction_var) {
		an = ti->instr()->annotes()->get_annote(k_reduction_gen);
		delete an;
//	    }
        }
    }
}


//
// Returns TRUE if it can be determined that the first dimension has
// a single element.  This means that it's a potential "Fortran pointer"
// and that the code is likely to overstep the array bounds.  
//
static boolean one_element_dim(type_node *curr_type) 
{
    type_node *true_type = curr_type->unqual();

    if (true_type->peek_annote(k_call_by_ref)) {
	assert(true_type->is_ptr());
	true_type = ((ptr_type *) true_type)->ref_type()->unqual();
    }

    if (!true_type->is_array()) return FALSE;

    // Find the size of the last dimension
    // 
    int too_messy;
    if (array_num_elem((array_type *)true_type, &too_messy) == 1) return TRUE;
    
    return FALSE;
}

