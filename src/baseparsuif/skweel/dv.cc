/* file "dv.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Distance vectors */

#pragma implementation "dv.h"

#define RCS_BASE_FILE dv_cc

#include "skweel.h"
#include "dv.h"

RCS_BASE("$Id: dv.cc,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

void DVlist_e::print(FILE *f)
{
    if(len == -1) {
        fprintf(f,"<uninitialized>");
    } else if(len >= 0 && len <= MAXDEPTH) {
        for(int i=0; i<len; i++) {
            fprintf(f,i==0?"(":" ");
            d[i].print(f);
        }
        putc(')',f);
    } else {
        fprintf(f,"<illegal vector: program error>");
    }
}

int operator==(DVlist_e &d1,DVlist_e &d2)
{
    assert(d1.len >= 0 && d2.len >= 0);
    if(d1.len != d2.len) return 0;
    for(int i=0; i<d1.len; i++)
        if(d1.d[i] != d2.d[i])
            return 0;
    return 1;
}

int operator!=(DVlist_e &d1,DVlist_e &d2)
{
    return !(d1==d2);
}

void DVlist_e::reverse_all(int from)
{
    for(int i=from; i<len; i++)
        d[i].negate();
}

int DVlist_e::is_zero()
{
    assert(len > 0);
    for(int i=0; i<len; i++) {
        if(d[i].dir() != d_eq) {
            return 0;
        } else {
            assert(d[i].is_const());assert(d[i].dist() == 0);
        }
    }
    return 1;
}

int DVlist_e::is_positive()
{
    assert(len > 0);
    for(int i=0; i<len; i++) {
        switch(d[i].dir()) {
        case d_lt:
            return 1;
        case d_gt:
        case d_ge:
        case d_star:
        case d_lg:
            return 0;
        case d_eq:
        case d_le:
            break;
        default:
            assert(0);
        }
    }
    return 0;	/* something like (=,<=) */
}

DVlist_e::DVlist_e(DVlist_e *e)
{
    len = e->len;
    for(int i=0; i<len; i++)
        d[i] = e->d[i];
}

DVlist::DVlist()
{
    star_level = 0;
    count = 0;
}

DVlist::DVlist(DVlist *l)
{
    count = l->count;
    star_level = l->star_level;
    for(int i=0; i<count; i++)
        dv[i] = new DVlist_e(l->dv[i]);
}

DVlist::~DVlist()
{
    for(int i=0; i<count; i++)
        delete dv[i];
}

void DVlist::print(FILE *f)
{
    fprintf(f,"{");
    for(int c = 0; c < count; c++)
        dv[c]->print(f);
    fprintf(f,"}\n");
}

/*
 * This routine expands a single distance vector into a set of
 * lexicographically positive vectors that encompass exactly the
 * same dependences.  For example, (*,*) is expanded into
 * (<,*) and (0,<).  Each of the new lexicographically positive
 * dependences is inserted in the list.
 * If positive_only is not set, then the negative portions of the
 * dependences are reversed and then inserted also.  In any case,
 * all dependences are lexicographically positive, of course.
 */
    
boolean DVlist::ins(DVlist_e *v,int positive_only)
{
    DVlist_e *dv_copy=0,*dv_copy2=0;
    boolean added_vector = FALSE;

    if(tdebug['D']>1) {
        printf("considering: ");
        v->print(stdout);
        printf("\n");
    }
    
    assert(v->len > 0);
    for(int i=0; i<v->len; i++) {
        switch(v->d[i].dir()) {
        case d_lt:
            final_ins(v);
            return TRUE;
        case d_gt:
            if(positive_only) {
                delete v;
		return FALSE;
            } else {
                v->reverse_all(i);
                final_ins(v);
		return TRUE;
            }
        case d_eq:
            assert(v->d[i].dist() == 0);
            break;
        case d_le:
            dv_copy = new DVlist_e(v);
            v->d[i].set_direction(d_eq);
            dv_copy->d[i].set_direction(d_lt);
            if (ins(v)) added_vector = TRUE;
            if (ins(dv_copy)) added_vector = TRUE;
            return added_vector;
        case d_ge:
            dv_copy = new DVlist_e(v);
            v->d[i].set_direction(d_eq);
            dv_copy->d[i].set_direction(d_gt);
            if (ins(v)) added_vector = TRUE;
            if (ins(dv_copy)) added_vector = TRUE;
            return added_vector;
        case d_lg:
            dv_copy = new DVlist_e(v);
            v->d[i].set_direction(d_lt);
            dv_copy->d[i].set_direction(d_gt);
            if (ins(v)) added_vector = TRUE;
            if (ins(dv_copy)) added_vector = TRUE;
            return added_vector;
        case d_star:
            dv_copy = new DVlist_e(v);
            dv_copy2 = new DVlist_e(v);
            v->d[i].set_direction(d_lt);
            dv_copy->d[i].set_direction(d_gt);
            dv_copy2->d[i].set_direction(d_eq);
            if (ins(v)) added_vector = TRUE;
            if (ins(dv_copy)) added_vector = TRUE;
            if (ins(dv_copy2)) added_vector = TRUE;
            return added_vector;
        default:
            assert(0);
        }
    }
    assert(v->is_zero());
    delete v;
    return added_vector;
}

void DVlist::final_ins(DVlist_e *v)
{
    /*
     * just insert, but optimize by removing duplicates: if the
     * identical vector is on the list, then discard.  More fancy
     * redundancy detection can be included if necessary; I doubt it.
     */
    
    if(tdebug['D']) {
        printf("inserting: ");
        v->print(stdout);
    }
    
    for(int i=0; i < count; i++) {
        if(*dv[i] == *v) {
            if(tdebug['D']) printf("not inserted\n");
            delete v;
            return;
        } 
    }
    assert(count-1 < MAXVECS);
    dv[count++] = v;
    if(tdebug['D']) {
        printf("new list: ");
        print(stdout);
    }
}

boolean DVlist::insert(distance_vector *v,int depth)
{
    if(v->indep())
        return FALSE;
    distance_vector_iter dvi(v);
    int len = 0;
    DVlist_e *newd = new DVlist_e;
    int stl = -1,still=1;
    while(!dvi.is_empty()) {
        distance d = dvi.step()->d;
        newd->d[len++] = d;
        if(d.dir() == d_eq) assert(d.dist() == 0);
        if(newd->d[len-1].dir() == d_eq) assert(newd->d[len-1].dist() == 0);
        if(still) {
            if(stl == -1 && !d.is_const()) stl++;
            else if(stl >= 0 && d.is_star()) stl++;
            else still = 0;
        }
    }
    if(tdebug['D'] && stl>0) {
        printf("In DVlist::insert(), a positive star level %d: ",stl);
        v->print(stdout);
        printf("\n");
    }
    if(star_level < stl) star_level = stl;
    assert(len == depth);
    newd->len = len;
    return ins(newd);
}
