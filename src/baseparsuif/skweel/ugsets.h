/* file "ugsets.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Uniformly-generated references */

#ifndef UGSETS_H
#define UGSETS_H

#include "skweel.h"

#pragma interface

RCS_HEADER(ugsets_h, "$Id: ugsets.h,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

#define MAXCOLS 16

class refs;

// ugset: a single set of uniformly generated references [GaGJ'88,PLDI'91],
//        references with differ only by a constant in each index expression.
//        e.g. a[i,j], a[i+1,j-2], ...
// set_of_ugset: many ugsets
//        each reference in the inner loop falls into exactly one ugset.

/* one indexing function */

struct ugset_e: public glist_e {
    array_info *ai;             // the reference, e.g. [i,j-1]
    int reads;                  // number of times this ref is read and
    int writes;                 // written in inner loop
    ugset_e(array_info *v):glist_e(),ai(v),reads(0),writes(0) {}
    ~ugset_e() {}
    ugset_e *next_elem() {return (ugset_e *) glist_e::next_e;}
    void print(FILE *f);
};

/*
 * the uniformly generated set: a list of indexing functions, plus
 * information on the base array, where there is locality, etc
 */

class ugset: public glist {
    int aicompat(array_info *v);
public:
    sym_addr base;              // the array, e.g. a[] or parameter:pr#6
    array_info *cannon_ai;       // the non-constant part of the indexing fctn
    int element_size;            // in bytes
    vector_space *self_temporal; // space of self temporal reuse for this UGS
    vector_space *self_spatial;  // space of self spatial reuse for this UGS
    vector_space *locality;      // union of self and group reuse in this UGS
    vector_space *tinner;
    vector_space *sinner;        // for current localized space ("innermost
                                 // loops"), intersection of that and
                                 // self_temporal/self_spatial
    int tdim,sdim;               // dimensionality of tinner, sinner
    matrix *A;		         // the coefficients for this reference
    matrix *Af;	                 // factored coefficients
    matrix *AS;	                 // spatial coefficients for this reference
    matrix *ASf;	         // factored coefficients

    ugset(sym_addr p, array_info *v,int esz,int d,tree_for **f,ugset_e **x);
    ~ugset();
    int nest_depth();             // how deep is the nesting of this reference
    int index_count() {return cannon_ai->count();} // how many indices?
    int compatable(sym_addr p,array_info *v,int esz)
	// belongs in this ugset?
        {return (p == base && esz == element_size) ? aicompat(v) : 0;}
    ugset_e *enter(array_info *v); // enter indexing expressions in ugset
    int distinct_bytes()           // bytes in cache due to one iteration
        {return glist::count() * element_size;}
                                   // lcost: memrefs/iter, given "inner loops"
    double lcost(vector_space *vs_inner_loops,double blksz,int iter);
    void print(FILE *f);
    int count() {return glist::count();}
};

/*
 * Iterator to examine all indexing functions for a uniformly generated set
 */

struct ugset_iter: public glist_iter {
    ugset_iter(ugset *v):glist_iter((glist *)v) {}
    ~ugset_iter() {}
    ugset_e *step() {return (ugset_e *) glist_iter::step();}
    int is_empty() {return glist_iter::is_empty();}
};

/*
 * Sets of uniformly generated sets.  Also, information that goes along
 * with that object, such as the associated loops and loop nest depth.
 */

struct set_of_ugset_e: public glist_e {
    ugset set;
    set_of_ugset_e(sym_addr p, array_info *v,int esz,
                   int depth,tree_for **f,ugset_e **x):
                   glist_e(),set(p,v,esz,depth,f,x) {}
    ~set_of_ugset_e() {}
    void print(FILE *f);
};

struct set_of_ugset: public glist {
    int depth;                          // total nest depth
    tree_for *flist[MAXDEPTH];        // loops in nest, outer to inner
    set_of_ugset(tree_for **f,int d);
    ~set_of_ugset();
    ugset_e *enter(sym_addr,array_info *,int esz);
    void append(refs *);                // insert refs into UGSs.
                                        // lcost: memrefs/iter for all UGSs
    double lcost(vector_space *inner_loops,double blksz,int iter);
    void print(FILE *f);
    int distinct_bytes();                // sum of distinct_bytes for each UGS
};

struct set_of_ugset_iter: public glist_iter {
    set_of_ugset_iter(set_of_ugset *v):glist_iter(v) {}
    ~set_of_ugset_iter() {}
    set_of_ugset_e *step() {return (set_of_ugset_e *) glist_iter::step();}
    int is_empty() {return glist_iter::is_empty();}
};

/*
 * construct one of these to find the cost (memrefs/iter) of the given
 * ugsets (one for registers, one for memory -- see void * problem)
 * with the given inner loop.
 */

struct cost_and_trip_size {
    double cost;              /* in memrefs/iter */
    int blocksize;            /* for now, just one for each dimension */
    cost_and_trip_size(set_of_ugset *s1,set_of_ugset *s2,vector_space *inner);
    ~cost_and_trip_size() {}
};

#endif

