/* file "matrix.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Generic matrix class for unimodular and tiling transformations.  
   Use in conjunction with VM_HEADER. */

#ifndef MATRIX_H
#define MATRIX_H

#pragma interface

RCS_HEADER(matrix_h, "$Id: matrix.h,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

class tvector_space;
class tvector;

#define MAXCOLS 16

class tmatrix {
	//static const MAXCOLS=16;
	boolean pivotcols[MAXCOLS]; // only if inter!=0
	VM_E *m[MAXCOLS];
	int r,c;
	int *inter;		// is zero iff not factored
public:
	tmatrix(int sz1,int sz2);
	tmatrix(tmatrix &);
	~tmatrix();
	VM_E &elt(int i,int j) FCONST
		{assert(i>=0 && i<r && j>=0 && j<c);
		 return m[j][i]; }
	int rows() FCONST {return r;}
	int cols() FCONST {return c;}
	int is_factored() FCONST {return inter != 0;}
	int is_pivot(int i) FCONST {return pivotcols[i];}
	void operator =(tmatrix &);
	void print(FILE *f=stdout) FCONST;
	void transpose();
	tmatrix operator *(tmatrix &) FCONST;
	int factor_col(VM_E *,int);
	int factor_col_and_insert(VM_E *,int);
	void factor();
	void unfactor();
	tmatrix L();
	tmatrix U();
	void Linverse(VM_E *);
	void appendcol(VM_E *);
	int last_nonzero_Urow() FCONST;
	int UbacksolveColumn(VM_E *w,int fvar1,VM_E *ans); //ret 0: inconsistent
	tvector_space kernel();
	tvector particular_solution(VM_E *w,int *ok);
	tmatrix inverse();
};

#endif MATRIX_H
