/* file "vector.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Vectors, vector spaces, etc. Used for unimodular and tiling 
    transformations.  Use in conjunction with VM_HEADER. */

#pragma implementation "vector.h"

#define RCS_BASE_FILE vector_cc

#include <suif.h>
#include <suifmath.h>
#include "vm_header.h"
#include "vector.h"
#include "matrix.h"

RCS_BASE("$Id: vector.cc,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

#ifndef VM_E
	Need to define VM_E!!!!!!!!!!!!!!!!!!
#endif
#ifndef VM_E_PRINT
	Need to define VM_E_PRINT!!!!!!!!!!!!!!!!!!
#endif
#ifndef VM_E_IS_ZERO
	Need to define VM_E_IS_ZERO!!!!!!!!!!!!!!!!!!
#endif
#ifndef VM_E_ABS
	Need to define VM_E_ABS!!!!!!!!!!!!!!!!!!
#endif

void tvector::copy(VM_E *vv)
{
	v = new VM_E[dim];
	for(int i=0; i<dim; i++)
		v[i] = vv[i];
}

tvector::tvector(int d):dim(d)
{
	v = new VM_E[dim];
	for(int i=0; i<dim; i++)
		v[i] = VM_E(0);
}

void tvector::print(FILE *f)
{
	fprintf(f,"[");
	for(int i=0; i<dim; i++) {
		if(i!=0) fprintf(f," ");
		VM_E_PRINT(f,v[i]);
	}
	fprintf(f,"]");
}

void tvector::operator +=(tvector &vv)
{
	for(int i=0; i<dim; i++)
		v[i] += vv.v[i];
}

int tvector::operator ==(tvector &vv) FCONST
{
	assert(dim == vv.dim);
	for(int i=0; i<dim; i++)
		if(v[i] != vv.v[i])
			return 0;
	return 1;
}

int tvector::proportional(tvector &vv) FCONST
{
	assert(dim == vv.dim);
        VM_E cur_r = VM_E(0);
	for(int i=0; i<dim; i++) {
            int z1 = VM_E_IS_ZERO(v[i]), z2 = VM_E_IS_ZERO(vv.v[i]);
            if(z1 || z2 ) {
                if(!z1 || !z2) return 0;
            } else {
                VM_E r = v[i]/vv.v[i];
                if(VM_E_IS_ZERO(cur_r)) cur_r = r;
                else if(cur_r != r) return 0;
            }
        }
	return 1;
}

void tvector::operator -=(tvector &vv)
{
	for(int i=0; i<dim; i++)
		v[i] -= vv.v[i];
}

int tvector::is_zero() FCONST
{
	for(int i=0; i<dim; i++)
		if(!VM_E_IS_ZERO(v[i]))
			return 0;
	return 1;
}

VM_E tvector::dot(VM_E *w) FCONST
{
	VM_E ans = VM_E(0);
	for(int i=0; i<dim; i++)
		ans += vect()[i] * w[i];
	return ans;
}

void tvector::reduce_magnitude()
{
#if VM_TYPE_DOUBLE
    return;
#else
#if VM_TYPE_FRACT
    // strategy: reduce numerator and denominator maximally.
    int n = 0, d = 0;
    for(int i=0; i<dim; i++) {
        int num = v[i].num();
        if(num < 0) num = -num;
        n = gcd(n,num);
        d = gcd(d,v[i].denom());
    }
    assert(d != 0);
    assert(n != 0 || d == 1);
    if(d > 1 || n > 1)
        for(i=0; i<dim; i++) {
	    fract cur = v[i];
            v[i] = fract(cur.num() / n, cur.denom() / d);
        }
#else
    It's impossible!  It's just impossible!
#endif
#endif
}

tvector operator *(VM_E scalar,tvector &v)
{
	VM_E *x = new VM_E[v.dim];
	for(int i=0; i<v.dim; i++)
		x[i] = scalar * v.v[i];
	tvector ret(x,v.dim);
	delete[] x;
	return ret;
}

tvector operator *(tmatrix &m,tvector &v)
{
	VM_E *x = new VM_E[m.rows()];
	assert(m.cols() == v.dim);
	for(int i=0; i<m.rows(); i++) {
		VM_E tmp = VM_E(0);
		for(int j=0; j<m.cols(); j++) tmp += m.elt(i,j) * v.vect()[j];
		x[i] = tmp;
	}
	tvector ret(x,m.rows());
	delete[] x;
	return ret;
}

void tvector_list::operator =(tvector_list &vl)
{
	assert(dim == vl.dim);
	while(!is_empty())
		delete pop();
	tvector_iter vi(&vl);
	while(!vi.is_empty()) {
		tvector *vx = vi.step();
		tvector *vnew = new tvector(vx->v,dim);
		append(vnew);
	}
}


tvector_list::tvector_list(tvector_list &vl)
{
        dim = vl.dim;
	tvector_iter vi(&vl);
	while(!vi.is_empty()) {
		tvector *vx = vi.step();
		tvector *vnew = new tvector(vx->v,dim);
		append(vnew);
	}
}

tvector_list::~tvector_list()
{
    tvector_iter vi(this);
    while (!vi.is_empty()) {
	delete vi.step();
    }
}

void tvector_list::print(FILE *f)
{
	int count=0;
	tvector_iter vi(this);
	fprintf(f,"    {");
	while(!vi.is_empty()) {
		if(count++) fprintf(f,",");
		vi.step()->print(f);
	}
	fprintf(f,"}\n");
}

void tvector_list::reduce_magnitude()
{
    tvector_iter vi(this);
    while(!vi.is_empty()) {
        vi.step()->reduce_magnitude();
    }
}

tvector_space::~tvector_space()
{
    if (bv) delete bv;
    if (lu) delete lu;
}

void tvector_space::make_bv_aux()
{
	assert(lu && !bv);
	lu->unfactor();
	bv = new tvector_list(lu->rows());
	VM_E *col = new VM_E[lu->rows()];
	for(int j=0; j<lu->cols(); j++) {
		for(int i=0; i<lu->rows(); i++)
			col[i] = lu->elt(i,j);
		bv->append(new tvector(col,lu->rows()));
	}
	delete[] col;
	delete lu;
	lu = 0;
}

void tvector_space::make_lu_aux()
{
	assert(bv && !lu);
	int cnt = bv->count();
	assert(cnt <= bv->dimensions());

	lu = new tmatrix(bv->dimensions(),cnt);

	tvector_iter vi(bv);
	for(int j=0; j<cnt; j++) {
		assert(!vi.is_empty());
		VM_E *v = vi.step()->vect();
		for(int i=0; i<bv->dimensions(); i++)
			lu->elt(i,j) = v[i];
	}
	lu->factor();
	delete bv;
	bv = 0;
}

tvector_space::tvector_space(tvector_space &vs)
{
	if(vs.bv) {
		assert(!vs.lu);
		lu = 0;
		bv = new tvector_list(vs.bv->dimensions());
		*bv = *vs.bv;
	} else {
		assert(vs.lu);
		bv = 0;
		lu = new tmatrix(vs.lu->rows(),vs.lu->cols());
		*lu = *vs.lu;
	}
}

void tvector_space::operator =(tvector_space &vs)
{
	delete lu;
	delete bv;
	if(vs.bv) {
		assert(!vs.lu);
		lu = 0;
		bv = new tvector_list(vs.bv->dimensions());
		*bv = *vs.bv;
	} else {
		assert(vs.lu);
		bv = 0;
		lu = new tmatrix(vs.lu->rows(),vs.lu->cols());
		*lu = *vs.lu;
	}
}

void tvector_space::print(FILE *f)
{
	make_bv();
	bv->print(f);
}

int tvector_space::in(VM_E *w)
{
	make_lu();

	// check for null
	int i;
	for(i=0; i<lu->rows(); i++)
		if(!VM_E_IS_ZERO(w[i]))
			break;

	if (i == lu->rows()) return TRUE;

	VM_E *hold = new VM_E[lu->rows()];
	for(i=0; i<lu->rows(); i++)
		hold[i] = w[i];

	int curpivrow = 0;
	for(i=0; i<lu->cols(); i++)
		curpivrow += lu->is_pivot(i);

	if (curpivrow == lu->rows()) {
	    delete[] hold;
	    return TRUE; // spans full space
        }

	lu->factor_col(hold,curpivrow);
	if(VM_E_IS_ZERO(hold[curpivrow])) {
		delete[] hold;
		return TRUE;
	} else {
		delete[] hold;
		return FALSE;
	}
}

int tvector_space::insert(VM_E *w)
{
	make_lu();
	int rv = lu->factor_col_and_insert(w,1);
	return rv;
}

void tvector_space::reduce_magnitude()
{
	make_bv();
        bv->reduce_magnitude();
}

int tvector_space::operator +=(tvector_space &v)
{
	int rval = 0;
	v.make_bv();
	tvector_iter vi(v.bv);
	while(!vi.is_empty()) {
		tvector *w = vi.step();
		rval += insert(w->vect());
	}
	return rval;
}

tvector tvector::proj(VM_E *w) FCONST
{
	// strang p107: p = (at b/at a) a, where a is w, b is this.
	// returns proj of this onto line spanned by w.

	tvector ww(w,dimensionality());
	VM_E f = this->dot(w)/ww.dot(w);
	return f * ww;
}

tmatrix tvector_space::proj_matrix()
{
	// strang p116: A(AtA)-1At
	int was_lu = lu !=0;
	make_bv();
	tmatrix m(bv->dimensions(),0);
	tvector_iter vi(bv);
	while(!vi.is_empty())
		m.appendcol(vi.step()->vect());
	tmatrix mt = m;
	mt.transpose();
	tmatrix mtm = mt*m;
	tmatrix mtm_inv = mtm.inverse();
	tmatrix rv = m * mtm_inv * mt;
	if(was_lu) make_lu();
	return rv;
}

void tvector_space::operator -=(tvector_space &vs)
{
	make_bv();
	vs.make_bv();

        // quick removal of common directions, efficiency
        tvector_space vs2(vs.bv->dimensions());
#if 0
        vs2.make_bv();
        tvector_iter vsi(vs.bv);
        while(!vsi.is_empty()) {
            tvector *v = vsi.step();
            tvector_iter vi(bv);
            int removed = 0;
            while(!vi.is_empty()) {
                tvector *vv = vi.step();
                if(v->proportional(*vv)) {removed++; bv->remove(vv); break;}
            }
            if(!removed) vs2.bv->append(new tvector(*v));
        }
#else
        vs2 = vs;
#endif

	assert(bv->dimensions() == vs2.bv->dimensions());
	tvector_list hold(bv->dimensions());
	tvector_iter vi(bv);
	while(!vi.is_empty()) {
		hold.append(new tvector(vi.step()->vect(),bv->dimensions()));
	}
	tmatrix pm = vs2.proj_matrix();
	tvector_space ret(bv->dimensions());
	while(!hold.is_empty()) {
		tvector *v = hold.pop();
		*v -= pm * (*v);
                v->reduce_magnitude();
		if(!v->is_zero()) {
			ret.insert(v->vect());
		}
		delete v;
	}
	*this = ret;
}

void tvector_space::operator *=(tvector_space &vs)
{
	// strang, pp93-95
	make_lu();	int count1 = lu->cols();
	vs.make_bv();	int count2 = vs.bv->count();
	tvector_iter vi(vs.bv);
	while(!vi.is_empty()) {
		lu->factor_col_and_insert(vi.step()->vect(),0);
	}
	tvector_space vs2 = lu->kernel();
	vs2.make_bv();
	tvector_space vs3(lu->rows());
	VM_E *x = new VM_E[lu->rows()];
	while(!vs2.bv->is_empty()) {
		int i;
		tvector *v = vs2.bv->pop();
		assert(v);
		for(i=0; i<lu->rows(); i++)
			x[i] = 0;
		tvector vv = tvector(x,lu->rows());
		tvector_iter vi(vs.bv);
		for(i=0; i<count2; i++) {
			assert(!vi.is_empty());
			VM_E *vct = v->vect();
			VM_E f = ((VM_E *)vct)[count1+i];//cast for compiler bug
			tvector vx = f * *vi.step();
			vv += vx;
		}
		assert(vi.is_empty());
		assert(!vs3.in(vv.vect()));
		vs3.insert(vv.vect());
	}
	delete[] x;
	*this = vs3;
}

tvector_space tvector_space::operator +(tvector_space &v)
{
	tvector_space r = *this;
	r += v;
	return r;
}

tvector_space tvector_space::operator -(tvector_space &v)
{
	tvector_space r = *this;
	r -= v;
	return r;
}

tvector_space tvector_space::operator *(tvector_space &v)
{
	tvector_space r = *this;
	r *= v;
	return r;
}

void tvector_space::beautify()
{
	make_lu();
	int rws = lu->rows();
	VM_E *x = new VM_E[rws];
	tvector_space hold(rws);
	for(int j=0; j<rws; j++) {
		for(int i=0; i<rws; i++)
			x[i] = VM_E(i==j);
		if(in(x))
			hold.insert(x);
	}
	*this -= hold;
	hold.make_bv();
	while(!hold.bv->is_empty()) {
		tvector *v = hold.bv->pop();
		insert(v->vect());
		delete v;
	}
	delete[] x;
}

int tvector_space::perpendicular(VM_E *w)
{
	make_bv();
	tvector_iter i(bv);
	while(!i.is_empty()) {
		tvector *v = i.step();
		if(!VM_E_IS_ZERO(v->dot(w)))
			return 0;
	}
	return 1;
}

tmatrix tvector_space::lu_matrix()
{
	make_lu();
	return *lu;
}
