/* file "skweel.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Main header file */

/*
 * Options:
 *
 *      'A' -- dump before (once or three times),
 *             after transformation (twice or three times)
 *      'D' -- print out info about parallelization (same as -V)
 *      'N' -- normalize loops only -- don't transform to improve code
 *      'P' -- look for parallelism
 *      'Q' -- Number of functions to optimize before quitting
 *      'S' -- don't do scalar privatization
 *      'T' -- do everything except tile.
 *      'V' -- print out info about parallelization (same as -D)
 *      'W' -- statistics about output
 *      '2' -- when tiling, generate 2n-1 loops rather than 2n
 *
 *      'b' -- register block (NOT IMPLEMENTED)
 *      'c' -- cache size, bytes
 *      'd' -- debugging (see type below)
 *      'f' -- fill_factor (% of cache to fill, default = 0.6)
 *      'i' -- after cache improvement, choose innermost loop in tile
 *             for registers
 *      'l' -- linesize
 *      't' -- tile size (if none specified, then skweel will calculate size)
 *      'p' -- print for loops
 *      'y' -- small bound cutoff
 *      'z' -- small loop cutoff
 *
 * The debugging types in use right now are
 *
 *	'A' -- ast_it.c (parameter info, quick summary)
 *	'B' -- blocksize and cost iteration
 *      'C' -- cost in order.c
 *	'D' -- dv.c (insert function)
 *      'G' -- enter your own transformation
 *	'I' -- loop interchange process
 *	'N' -- nest.c
 *      'O' -- order.c
 *	'R' -- reflists.c (whether everything touched by a call, that's all)
 *	'S' -- skewing
 *      'T' -- transformation matrix information (order.c)
 *	'U' -- ugsets.c (cost info)
 *	'V' -- extra ugsets::print info (vector spaces)
 *      'v' -- find cost vector space info
 *      'i' -- choosing the best innermost loop (for registers)
 *      'n' -- normalization
 *      's' -- scalar privatization
 *	't' -- outline of transform process (two levels)
 *	'x' -- reserved for testing
 */

#ifndef SKWEEL_H
#define SKWEEL_H

#pragma interface

#include <suif1.h>
#include <dependence.h>
#include <transform.h>

RCS_HEADER(skweel_h, "$Id: skweel.h,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

#define MAXVECS 200
#define BYTESZ 8

class tree_proc;
class nest_info;

extern void find_regions_and_transform(tree_proc *); // transform a procedure
extern void find_and_do_transform(nest_info *);      // transform a loop nest
extern void quick_summary(tree_node_list *,int);     // print instruction list

extern tree_for *next_perfect_inner(tree_for *tf);
extern boolean has_loop_inside(tree_node_list *tnl);



extern boolean register_block;         // perform register blocking
extern boolean is_fortran;             // compiling a fortran procedure
extern int cache_size;		       // in bytes
extern double fill_factor;             // fraction of cache to use [ASPLOS'91]
extern int line_size;		       // in bytes
extern int tile_size;                  // size of tiles
extern boolean parallelism_level;      // effort to put into parallelization
extern boolean normalize_only;         // don't perform other transformations
extern boolean no_scalar_privatization;// turn off scalar privatization
extern boolean do_not_tile;            // do everything but the final tiling
extern boolean coalesce;               // generate 2N-1 loop nests when tiling
extern int innermost_for_registers;    // attempt to interchange to make
                                       // the innermost loop utilize registers
                                       // effectively (bonus for cache opt)
extern double small_loop_cutoff;       // don't parallelize loops w/ less work 
extern int small_bound_cutoff;         // don't parallelize loops w/ less iters
extern boolean print_parallelism_info; // print info about parallelization
extern boolean no_doall_regions;       // don't try to create doall_regions
extern boolean no_doacross;            // don't mark doacross loops
extern boolean no_locality;            // don't do any locality opts

extern int tdebug[];                   // what to debug?
extern dependency_test *deptest;       // dependence testing style
extern char *progname;                 // the name of this program
extern char *fname;                    // the current function being optimized


// Defined in skweel.cc:
//
extern const char *k_it_stats;
extern const char *k_it_for;
extern const char *k_begin_fully_permutable;
extern const char *k_end_fully_permutable;
extern const char *k_begin_parallel_region;
extern const char *k_end_parallel_region;
extern const char *k_privatized;
extern const char *k_reduction;
extern const char *k_reduction_gen;
extern const char *k_reduced;
extern const char *k_reduced_gen;
extern const char *k_privatizable;
extern const char *k_need_finalization;
extern const char *k_iv_live;
extern const char *k_small_bound;
extern const char *k_C_pragma;        
extern const char *k_chaotic;
extern const char *k_locality;
extern const char *k_alias_no_offset_list;
extern const char *k_alias;
extern const char *k_alias_no_offset;
extern const char *k_wilbyr_run;
extern const char *k_doacross;
extern const char *k_doall_region;
extern const char *k_tile_loops;
extern const char *k_tileable_loops;
extern const char *k_contains_call;

extern const char *k_pure_function;  // Defined in useful.cc


inline void swap(int &i,int &j)
{int t=i; i=j; j=t;}
inline void swap(var_sym* &i,var_sym* &j)
{var_sym* t=i; i=j; j=t;}
inline void swap(access_vector* &i,access_vector* &j)
{access_vector* t=i; i=j; j=t;}
inline void swap(tree_node_list* &i,tree_node_list* &j)
{tree_node_list* t=i; i=j; j=t;}
inline void swap(tree_instr* &i,tree_instr* &j)
{tree_instr* t=i; i=j; j=t;}

#endif /* SKWEEL_H */
