/* file "ugsets.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Uniformly-generated references */

#pragma implementation "ugsets.h"

#define RCS_BASE_FILE ugsets_cc

#include <math.h>
#include <useful.h>
#include <stdlib.h>
#include "skweel.h"
#include "reflists.h"
#include "ugsets.h"

RCS_BASE("$Id: ugsets.cc,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

//--------------------------------------------------------------------
// Implementation of ugset member functions
//--------------------------------------------------------------------

void ugset_e::print(FILE *f)
{
	ai->print(f);
	printf("[r=%d,w=%d]",reads,writes);
}

set_of_ugset::set_of_ugset(tree_for **f,int d)
{
    int i;

    depth = d;
    for(i=0; i<d; i++)
        flist[i] = f[i];
}

set_of_ugset::~set_of_ugset()
{
    while(!glist::is_empty())
        delete (set_of_ugset_e *)glist::pop();
}

ugset_e *set_of_ugset::enter(sym_addr p,array_info *v,int esz)
{
    // if any component is too messy, we don't worry about
    // its locality
    
    if(v == 0)
        return 0;
    
    int all_constants = TRUE;

    array_info_iter ii(v);
    while(!ii.is_empty()) {
	access_vector *av = ii.step();
        if(av->too_messy)
	    return 0;
	else if (!av->is_const())
	    all_constants = FALSE;
    }
    if (all_constants) return 0; // access is all constants, don't worry
                                 // about locality
    set_of_ugset_iter i(this);
    while(!i.is_empty()) {
        set_of_ugset_e *e = i.step();
        if(e->set.compatable(p,v,esz)) {
            return e->set.enter(v);
        }
    }

    ugset_e *x;
    glist::push(new set_of_ugset_e(p,v,esz,depth,flist,&x));
    return x;
}

void set_of_ugset::append(refs *r)
{
    refs_iter i(r);
    while(!i.is_empty()) {
        refs_e *e = i.step();
        if(e->iarr && (e->memref->opcode() != io_cal)) {
	    sym_node *sn = e->base.symbol();
	    assert(sn->is_var());
	    type_node *tn = get_element_type(((var_sym *) sn)->type());

	    dep_instr_annote *dia = 
		(dep_instr_annote *) e->iarr->peek_annote(k_dep_instr_annote);
	    assert(dia);
	    
            ugset_e *ue = 
		enter(e->base, dia->ai, tn->unqual()->size()/BYTESZ);

            if(ue) {
                ue->reads += e->reads;
                ue->writes += e->writes;
            }
        }
    }
}

void set_of_ugset::print(FILE *f)
{
    set_of_ugset_iter i(this);
    while(!i.is_empty()) {
        i.step()->print(f);
    }
}

int set_of_ugset::distinct_bytes()
{
    set_of_ugset_iter i(this);
    int sum = 0;
    while(!i.is_empty()) {
        sum += i.step()->set.distinct_bytes();
    }
    return sum;
}

double set_of_ugset::lcost(vector_space *inner,double blocksize,int iter)
{
    set_of_ugset_iter i(this);
    double cost = 0.0;
    while(!i.is_empty())
        cost += i.step()->set.lcost(inner,blocksize,iter);
    return cost;
}

void set_of_ugset_e::print(FILE *f)
{
	set.print(f);
}

ugset::ugset(sym_addr p,array_info *v,int esz,int depth,tree_for **f,ugset_e **x)
  :base(p),cannon_ai(v),element_size(esz),tinner(0),sinner(0)
{
    int rowno, j;
    assert(v);
    *x = enter(v);
    int rows = v->count();
    
    matrix m_t(rows,depth);
    matrix m_s(rows-1,depth);
    array_info_iter aii(v);
    for(rowno = 0; !aii.is_empty(); rowno++) {
        access_vector *av = aii.step();
        for(j=0; j<depth; j++) {
            fract t = fract(av->val(f[j]));
/*
#ifdef VM_TYPE_FRACT
*/
            // This is to avoid overflows of fractions when analyzing
            // linearized arrays ... sigh.
            // This is a harmless hack that changes the constant in the
            // array.  Of course, this doesn't change the code, just
            // the locality analysis, which will not be materially harmed
            // by this.  Still, print out an alert so that we know there is
            // a problem, and we can attack it when and if necessary.
            fract a = t.abs();
            if(a > 5) {
                if (tdebug['U']) {
                    printf("<<alert: basically harmless linearized array?: ");
                    t.print(stdout);
                }

                int neg = t < 0;
                t = (a-5)/1000 + 5;
                t = t.num()/t.denom();
                if(neg) t = -t;
            
                if (tdebug['U']) {
                    printf(" changed to ");
                    t.print(stdout);
                    printf(">>\n");
                }
            }
/*
#endif
*/
            m_t.elt(rowno,j) = t;
            if(rowno < rows-1) m_s.elt(rowno,j) = t;
	}
    }
    assert(rowno == rows);
    
    A = new matrix(rows,depth);
    *A = m_t;
    m_t.factor();
    Af = new matrix(rows,depth);
    *Af = m_t;

    self_temporal = new vector_space(depth);
    *self_temporal = m_t.kernel();
    self_temporal->reduce_magnitude();
    self_temporal->beautify();

    AS = new matrix(rows-1,depth);
    *AS = m_s;
    m_s.factor();
    ASf = new matrix(rows-1,depth);
    *ASf = m_s;

    self_spatial = new vector_space(depth);
    *self_spatial = m_s.kernel();
    self_spatial->reduce_magnitude();
    self_spatial->beautify();

    // all locality vector space: initially just the spatial locality
    locality = new vector_space(depth);
    *locality = *self_spatial;
}

ugset::~ugset()
{
    while(!glist::is_empty())
        delete (ugset_e *)glist::pop();
    if(self_temporal) delete self_temporal;
    if(self_spatial) delete self_spatial;
    if(locality) delete locality;
    if(tinner) delete tinner;
    if(sinner) delete sinner;
    if(A) delete A;
    if(Af) delete Af;
    if(AS) delete AS;
    if(ASf) delete ASf;
}

int ugset::aicompat(array_info *v)
{
    // differ by at most a constant in all dimensions

    assert(v && cannon_ai && cannon_ai->count() == v->count());
    
    array_info_iter i1(v);
    array_info_iter i2(cannon_ai);
    
    while(!i1.is_empty() && !i2.is_empty()) {
        access_vector *a1 = i1.step();
        access_vector *a2 = i2.step();
        if(a1->too_messy || a2->too_messy)
            return 0;
        av_compare_info c(a1,a2);
        if(!(c.same_indregs() && c.same_conregs() &&
             c.same_memregs() && c.same_paths()))
            return 0;
    }
    return 1;
}

ugset_e *ugset::enter(array_info *v)
{
    ugset_iter i(this);
    while(!i.is_empty()) {
        ugset_e *e = i.step();
        array_info_iter ai1(e->ai);
        array_info_iter ai2(v);
        assert(e->ai->count() == v->count());
        int same = 1;
        while(!ai1.is_empty()) {
            av_compare_info c(ai1.step(),ai2.step());
            assert(c.same_const() == c.identical());
            if(!c.same_const()) {
                same = 0;
                break;
            }
        }
        if(same) {
            return e;
        }
    }

    // this is a new element.  It contributes some group spatial locality
    // with other elements in the group.  We'll union that in with the
    // locality vector space.

    i.reset(this);
    if(!i.is_empty()) {
        fract_vector w(A->m());
        while(!i.is_empty()) {
            ugset_e *e = i.step();
            array_info_iter ai1(e->ai);
            array_info_iter ai2(v);
            int j = 0;
            while(!ai1.is_empty()) {
                // note we compute w[rows-1], even though it doesn't contribute
                w[j++] = ai1.step()->con - ai2.step()->con;
            }
            int ok = 0;
            fract_vector xp = ASf->particular_solution(w,&ok);
            if(ok) {
                // the general soln is has already been included in
                // locality's initialization (self_spatial)
                locality->insert(xp);
            }
        }
    }

    ugset_e *e = new ugset_e(v);
    glist::push(e);
    return e;
}

int ugset::nest_depth()
{
    return A->n();
}

void ugset::print(FILE *f)
{
    if(tdebug['V']) {
        printf("[nest depth %d,",nest_depth());
        printf("index count %d]",index_count());
    }
    
    base.symbol()->print(f);
    fprintf(f, "+%d ", base.offset());
    
    int first = 1;
    ugset_iter i(this);
    while(!i.is_empty()) {
        if(first) first = 0;
        else fprintf(f,"&");
        i.step()->print(f);
    }
    fprintf(f,"\n");
    if(tdebug['V']) {
        if(self_temporal) {
            fprintf(f,"self temporal: ");
            self_temporal->print(f);
        }
        if(self_spatial) {
            fprintf(f,"self spatial: ");
            self_spatial->print(f);
        }
        if(locality) {
            fprintf(f,"all locality: ");
            locality->print(f);
        }
    }
}


//--------------------------------------------------------------------

/*
 * The cost_and_trip_size() routine assumes 2N blocking, where the goal
 * is to fill up the cache (modulo fill_factor) with the inner N loops.
 * As is discussed in my thesis, we now know that a better approach is
 * to use 2N-1 blocking where applicable, where the goal is to fill up
 * a smaller portion of the cache; this latter method is more accurate.
 * Since the precise block size is typically not that important (and
 * other factors, such as cache mapping characteristics, make more
 * difference anyway [ASPLOS'91]), there has been no need to fix this code.
 * Any fixing of this code should also include calculating gamma as
 * per [PLDI'91], which is simpler.
 */

cost_and_trip_size::cost_and_trip_size(set_of_ugset *s1,set_of_ugset *s2,vector_space *inner)
{
    int i;
    assert((s1||s2) && inner);
    int n = inner->dimensionality();
    double n_inverse = 1.0/(double)n;
    
    // startup is the number of bytes in the first iteration.
    // startup + cost*iters = cache_size is the relation we capture,
    // where iters is B^n.
    // Note that cost*iters is a loop constant.  We iterate on this
    // using an initial estimates of cost, converging to cost and iters.
    
    int startup = (s1?s1->distinct_bytes():0) + (s2?s2->distinct_bytes():0);
    
    double x = cache_size*fill_factor - startup;
    if(x < 10.0) { error_line(0, NULL, "cache too small??"); x = 10.0; }
    cost = startup ? double(startup) : 1.0;   // as a place to start
    double b = pow(x/cost,n_inverse);
    cost = (s1?s1->lcost(inner,b,0):0) + (s2?s2->lcost(inner,b,0):0);
    
    if(cost == 0.0) {
        // there is no cost, apparently
        assert(startup == 0);
        blocksize = 100000;
    } else {
        int iters = 12;
        for(i=1; i<=iters; i++) {
            double oldcost=cost,oldblksz=b;
            b = pow(x/cost,n_inverse);
            cost = (s1?s1->lcost(inner,b,i):0) + (s2?s2->lcost(inner,b,i):0);
            if(tdebug['B']) {
                printf("\niter: %d, cost: %g, blocksize %g",i,cost,b);
                fflush(stdout);
            }
            if(i > 2) {
                double costdiff = fabs(oldcost-cost), blkdiff =fabs(oldblksz-b);
                double costrat = fabs(costdiff/cost), blkrat = fabs(blkdiff/b);
                if((costdiff<0.01 || costrat<0.01) &&
                   (blkdiff<0.1 || blkrat<0.02)) {
                       break;
                   }
                if(fabs(cost) < 1.0e-10) {
                    cost = 0.0;
                    blocksize = 1000000;
                    if(tdebug['B']) {
                        printf("\nfinal cost: %g, blocksize %g\n",cost,b);
                        fflush(stdout);
                    }
                    break;
                }
            }
        }
        if(i==iters)
            error_line(0,NULL,"cost fails to converge? [c=%g,b=%g]\n",cost,b);
        
        // blocksize must be integral, round off.
        blocksize = int(b+0.5);
        b = (double) blocksize;
        cost = (s1?s1->lcost(inner,b,i):0) + (s2?s2->lcost(inner,b,i):0);
        cost += startup/pow(b,double(n));
    }

    if(tdebug['B']) {
        printf("\nfinal cost: %g, blocksize %d\n",cost,blocksize);
        fflush(stdout);
    }
}

/*
 * All the code from here down is for evaluating ugset::lcost()
 */

static double B;

static double ovlp(int i, fract_vector *v1, fract_vector *v2=0, double q=0.0)
{
        int ii;
	double r = 1.0;
	for(ii = 0; ii < i; ii++) {
		double vv1 = v1 ? v1->elt(ii).real() : 0.0;
		double vv2 = v2 ? v2->elt(ii).real() : 0.0;
		double b = B - fabs(vv1 + q * vv2);
		if(b <= 0.0) return 0.0;
		r *= b/B;
	}
	return r;
}

// Convert (1/3,1/5,1/6) -> (10,6,5)
//
static fract make_whole(fract_vector &f) 
{
        int i, fst;
	int top=1,bottom=1;
	int vdim = f.n();

	for(;;) {
		int multiply_by = 1;
		for(i=0; i < vdim; i++) {
			int d = f[i].denom();
			assert(d > 0);
			if(d > multiply_by) multiply_by = d;
		}
		if(multiply_by == 1) {
			for(i=0; i < vdim; i++)
				assert(f[i].denom() == 1);
			break;
		}
		top *= multiply_by;
		fract m(multiply_by,1);
		for(i=0; i < vdim; i++)
			f[i] *= m;
	}

	// now all the elements have denom 1, so we reduce

	for(fst = 0; fst < vdim; fst++)
		if(f[fst].num())
			break;
	assert(fst != vdim);
	int mn = f[fst].num();
	for(fst++; fst < vdim; fst++) {
		if(f[fst].num() < mn) mn = f[fst].num();
	}

	int dvby = 2;
	while(dvby <= mn) {
		for(i=0; i<vdim; i++)
			if(f[i].num() % dvby)
				break;
		if(i == vdim) {
			bottom *= dvby;
			mn /= dvby;
			for(i=fst; i<vdim; i++)
				f[i] /= fract(dvby,1);
		} else {
			dvby++;
		}
	}

	fract r(top,bottom);
	return r;
}

static double get_ovlp(ugset_e *,ugset_e *,ugset *,fract_vector *,vector_space *,vector_space *,vector_space *);

inline int max(int a,int b) { return a < b ? b : a; }

double ugset::lcost(vector_space *vs_inner,double BB,int iter)
{
/*
#ifndef VM_TYPE_FRACT
	assert(0); //necessary if we want the functionality in make_whole
#endif
*/
        int i;
	B = BB;

        if(iter == 0) {
            // first time through, so initialize these
            if(tinner) delete tinner;
            if(sinner) delete sinner;
            tinner = new vector_space(*vs_inner * *self_temporal);
            sinner = new vector_space(*vs_inner * *self_spatial);
            tdim = tinner->dimensionality();
            sdim = sinner->dimensionality();
        }

	if(tdebug['U']) {
            if(tdebug['U']>1 || iter == 0) {
		printf("---> COST for ugset (iter %d): ",iter);
                this->print(stdout);
                printf(">> inner loop: ");
                vs_inner->print(stdout);
                if(iter == 0) {
                    printf("tinner [dim %d]: ",tdim);
                    tinner->print(stdout);
                    printf("sinner [dim %d]: ",sdim);
                    sinner->print(stdout);
                    printf("tdim = %d, sdim = %d\n",tdim,sdim);
                }
		fflush(stdout);
            }
	}

	// 1/alpha is the approximate cost for one memory reference with the
	// given self temporal locality.  Is it really this simple??
        // double alpha =  (tdim == 0) ? 1.0 :
        //	(tdim == 1) ? B   :
	//	(tdim == 2) ? B*B :
	//	pow(B,double(tdim));
        // No!, because if vectors in wierd direction (e.g. (1,1/100)) then
        // can't take advantage.  Since tinner has been beautified and
        // had its magnitude reversed, a good check of how much reuse is
        // there is just the magnitude of the numerator and denominator

        double alpha = 1.0;

        fract_vector_list *vl = tinner->basis();

        fract_vector_list_iter vi(vl);
        while(!vi.is_empty()) {
            fract_vector *v = vi.step();
            int mag = 0;
            for(i=0; i<v->n(); i++) {
                mag = max(mag,abs((*v)[i].num()));
                mag = max(mag,(*v)[i].denom());
            }
            assert(mag > 0);
            double a = B + 1 - mag;
            if(a > 1) alpha *= a;
        }

	// now the self spatial:  beta is effective line size from self reuse.
	// A beta of 1 means that the line is well used; a large beta means
	// that the line is not being used efficiently.  Beta is simply the
	// stride, adjusted because long lines may bring in more than
	// you'd like.  (We use expected valued rather than bounds.)

        assert(element_size > 0);

	int l = (line_size > element_size) ? line_size/element_size : 1;
	double stride = (double) l;
	double fs = 1.0 - 1/B;		// fraction with spatial locality

	fract_vector *xs = NULL;
	fract factor = fract(1);

	if(tdim < sdim) {
		assert(sdim == tdim + 1);
                vector_space dif = *sinner - *tinner;
		fract_vector_list *vl = dif.basis();
		assert(vl->count() == 1);
		fract_vector *v = vl->pop();
		int vdim = nest_depth();
		assert(vdim == v->n());
		xs = new fract_vector(vdim);
		for(i=0; i<vdim; i++)
			xs->elt(i) = v->elt(i);
		delete v;

		factor *= make_whole(*xs);

		fract_vector vv = (*A) * (*xs);
		int d = vv.n();
		for(i=0; i<d-1; i++)
			assert(vv[i].num() == 0);
		double q = vv[i].real();
		q = fabs(q);
		if(q < stride) stride = q;
		fs *= ovlp(vdim, xs);
	}

	double beta = fs * stride + (1.0 - fs) * l;
	
	// gamma is the factor we multiply for all group locality.
	// We do not care if there is more than one read/write in the
	// loop, since the others cache hit.  Also, each ref costs
	// one, whether a read or write.

	double gamma = 0.0;
	ugset_iter i1(this);
	while(!i1.is_empty()) {
		ugset_e *e = i1.step();
		double e_ovlp = 0.0;
		for(ugset_e *ee = e->next_elem(); ee; ee = ee->next_elem()) {
			double o = get_ovlp(e,ee,this,xs,self_temporal,tinner,vs_inner);
			if(e_ovlp < o) e_ovlp = o;
		}
		gamma += (1.0 - e_ovlp);
		if(tdebug['U']>1) {
			e->print(stdout);
			printf(" group overlap %g\n",e_ovlp);
			fflush(stdout);
		}
	}

	double cost = element_size * beta * gamma / alpha;

	if(tdebug['U']>1 || (tdebug['U'] && iter == 0)) {
		print(stdout);
		printf("[blksz: %g, stride: %g] ",B,stride);
		printf("alpha: %g, beta: %g, gamma: %g ",alpha,beta,gamma);
		printf("cost: %g\n",cost);
	}

	if (xs) delete xs;
	return cost;
}

static double line_ovlp(double L,double q)
{
	double qq = fabs(q);
	if(qq < L) return (L-qq)/L;
	else return 0;
}

static double get_ovlp(ugset_e *e1,ugset_e *e2,ugset *u,fract_vector *xs,
			vector_space *t,vector_space *ti,vector_space *inner)
{
        int d, ii, i, qpp, q, r, c;
	// first find the constant difference betw e1 and e2 and place in w
	array_info_iter ai1(e1->ai),ai2(e2->ai);
	fract_vector w(MAXCOLS);
	int all_zero_except_last=1;
	fract last = fract(0);
	for(d=0; !ai1.is_empty() && !ai2.is_empty(); d++) {
		w[d] = fract(ai1.step()->con - ai2.step()->con);
		if (last != fract(0)) all_zero_except_last = 0;
		last = w[d];
	}
	assert(ai1.is_empty() && ai2.is_empty());
	assert(d == u->index_count());
	assert(d > 0 && d <= MAXCOLS);
	int depth = u->nest_depth();

	// now find a solution to Ax=w
	int ok;
	fract_vector xp = u->Af->particular_solution(w,&ok);
	fract_vector *xt = NULL;
	if(ok) {
		vector_space Itp(*t);
		int ok = Itp.insert(xp);
		assert(ok);
		Itp *= *inner;
		Itp -= *ti;
		if(Itp.dimensionality()) {
			assert(Itp.dimensionality() == 1);
			fract_vector *xpp = Itp.basis()->pop();
			assert(xpp->n() == depth);

			// xpp points in the desired direction.  So we just
			// project xp onto xpp, since all other components
			// contribute zero anyway.

			// Hack to make sure projection doesn't overflow
                        //
			boolean ovflow = FALSE;
			for (i = 0; i < depth; i++) {
			    int nn = xp[i].num();
			    int dd = xp[i].denom();
			    if (nn > 100 || nn < -100 || 
				dd > 100 || dd < -100) {
				ovflow = TRUE;
				break;
			    }

			    nn = xpp->elt(i).num();
			    dd = xpp->elt(i).denom();
			    if (nn > 100 || nn < -100 || 
				dd > 100 || dd < -100) {
				ovflow = TRUE;
				break;
			    }
			}

			if (!ovflow) {
			    fract_vector tmp = xp.projection(*xpp);
			    xt = new fract_vector(depth);
			    for(ii=0; ii<depth; ii++)
				xt->elt(ii) = tmp[ii];
			}
			delete xpp;
		}
	}
	// with long cache lines, it is possible that reuse will occur even
	// if the inner loop not in a stride one direction, ie if xs is zero.
	// Thus we need to fix this.
	if(tdebug['U'] > 1) {
		if(xt) {
			xt->print(stdout);
			printf(" is xt\n");
		} else printf("<xt is null>\n");
		if (xs) {
			xs->print(stdout);
			printf(" is xs\n");
		} else printf("<xs is null>\n");
		fflush(stdout);
	}

	// neither, either or both of xs and xt may be missing.
	// Now we get sloppy.  There is reuse for xt+q*xs, where
	// q is not zero -> line locality.

	int L = line_size/u->element_size;
	if(L==0) L=1;
	double Ld = double(L);
	double o = 0.0;

	if(all_zero_except_last) {
		// xs is our solution.
		if(xs) {
			double q = last.real();
			fract_vector x = *u->A * (*xs);
			for(i=0; i<d-1; i++)
				assert(x[i] == fract(0));

			double qp = x[i].real();
			assert(x[i] != fract(0));

			int a = int(fabs(q/qp));
			int b = int(Ld/fabs(qp));
			for(qpp = a-b; qpp<=a+b; qpp++) {
				double oo = line_ovlp(Ld,q-double(qpp)*qp);
				oo *= ovlp(depth,0,xs,double(qpp));
				if(oo > o) o = oo;
			}
		} else {
			// really the same as the != 0 part, but xs is nil
			// so obviously the best overlap is 100%.
			o = line_ovlp(Ld, last.real());
		}
	} else if (xt && xs) {
		// overlap is (L-|q|)/L * ovlp(xt+q*xs).
		// maximize over q? We'll just try integral values of -L<q<L

		// note that adding in values from Kt*I might help the overlap
		// without changing q, so we miss out on some stuff here.
		// But this is pretty good.

		for(q = -L + 1; q < L; q++) {
			double qd = double(q);
			double oo = line_ovlp(Ld,qd) * ovlp(depth,xt,xs,qd);
			if(oo > o) o = oo;
		}
	} else if (xt && !xs) {
		// exact hit ... q=0.  However, xs does not exist so no other
		// q's are possible.  line overlap is 100%.
		o = ovlp(depth,xt);
	} else if(!xt && xs) {
		// xt+q*xs cannot exist
		;
	} else {
		// Here there is at most one possible q for which
		// Ax = w+(0,0,..,0,q) has a solution.  (obviously not q=0,
		// since that was handles in the case xt && !xs)
		// We do this by solving ignoring the last dimension.

		// e.g. for i do a(i,j) = a(i+1,j+1).  solution is (1,0) with
		// a q (distance from direct hit, line usage) of one.

		int rr = u->A->m();
		int cc = u->A->n();
		matrix AA(rr-1,cc);
		for(r = 0; r < rr-1; r++)
			for(c = 0; c< cc; c++)
				AA.elt(r,c) = u->A->elt(r,c);
		AA.factor();
		fract_vector v = AA.particular_solution(w,&ok);
		if(ok) {
			assert(v.n() == cc);
			fract_vector xx = *u->A * v;
			assert(xx.n() == rr);
			for(i=0; i<rr-1; i++)
				assert(xx[i] == w[i]);
			fract qq = xx[i] - w[i];
			double q = qq.real();
			if(fabs(q) <= Ld) return 0.0;
			o = line_ovlp(Ld,q) * ovlp(depth, &v);
		}
	}

	delete xt;
	return o;
}




