/* file "region.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Finding transformable regions of code */

#define RCS_BASE_FILE region_cc

#include "skweel.h"
#include "ast_it.h"
#include "nest.h"
#include "dv.h"
#include <string.h>

RCS_BASE("$Id: region.cc,v 1.2 1999/08/25 03:27:09 brm Exp $")


// From ast_it.cc

extern boolean is_ncall_in_body(tree_for *tf, boolean recursive);
extern tree_for *only_inner_for(tree_for *tf);
extern void move_out_upper_bound(tree_for *tf);
extern void move_out_lower_bound(tree_for *tf);
extern boolean has_loop_inside(tree_node_list *tnl);
extern boolean has_return_instr(tree_node_list *tnl);

extern boolean op_is_unity(tree_node_list *tnl);  // from transform/lptrans.cc 

static void handle_pragmas(tree_for *tf);

/*
 * If the bounds are not in standard form, then uglybound returns
 * a non-zero, and the nest cannot be transformed.  As a debugging
 * aid (since this shouldn't happen), uglybound() returns a character
 * string indicating what the problem is.
 *
 * The bounds are in non-standard form if there is a load/store/call
 * in them (there should be only pseudoregister operations) 
 */

struct ugly_params {
    char *str;
    boolean ugly;
};

static boolean uglybound_src(instruction *, operand *r, void *x)
{
    if (!r->is_instr()) return FALSE;
    ugly_params *params = (ugly_params *) x;

    switch(r->instr()->opcode()) {
        case io_lod:
        case io_str:
        case io_cal: 
	case io_gen: {
	    params->ugly = TRUE;
            sprintf(params->str,"%s",if_ops_name(r->instr()->opcode()));
        }
        default: break;
    }

    if (!params->ugly) r->instr()->src_map(uglybound_src, x);

    return FALSE;
}


static boolean uglybound(operand bound, char *rval)
{
    if (!bound.is_instr()) return FALSE;
    
    switch(bound.instr()->opcode()) {
        case io_lod:
        case io_str:
        case io_cal:
        case io_gen:
            sprintf(rval, "%s", if_ops_name(bound.instr()->opcode()));
            return TRUE;
        default: break;
    }

    ugly_params params;
    params.str = rval;
    params.ugly = FALSE;

    bound.instr()->src_map(uglybound_src, &params);

    return (params.ugly);
}


/*
 * For data gathering purposes, examine the index expressions and
 * see what they look like:
 *   ipj: a[i+j]    (multiple index variables within one index expression)
 *   ici: a[i,i]    (an index variable in multiple index expressions of one
 *                   reference)
 *   other:         (anything that's not an access vector, e.g. a[i**2])
 */

struct index_info_params {
    int d;
    tree_for **f;
    int *ipj;
    int *ici;
    int *other;
};

void get_info(in_array *ia, index_info_params *x)
{
    dep_instr_annote *dia = 
	(dep_instr_annote *) ia->peek_annote(k_dep_instr_annote);
    assert(dia);

    array_info_iter aii(dia->ai);
    int seen[MAXDEPTH];
    int i;
    for(i = 0; i < x->d; i++)   seen[i] = 0;

    while(!aii.is_empty()) {
        access_vector *av = aii.step();
	if(av->too_messy) (*(x->other))++;
	else {
	    int num_here = 0;
	    for(int i = 0; i < x->d; i++) {
	        if(av->val(x->f[i])) {
		    num_here++;
		    seen[i]++;
		}
	    }
	    if(num_here > 1) (*(x->ipj))++;
    	}
    }

    for(i=0; i< x->d; i++) {
        if(seen[i]>1) (*(x->ici))++;
    }
}


static boolean index_info_src(instruction *, operand *r, void *x)
{
    if (r->is_instr()) {
        instruction *ins = r->instr();
	if (ins->opcode() == io_array) get_info((in_array *) ins, 
					      (index_info_params *) x);
        ins->src_map(index_info_src, x);
    }

    return FALSE;
}


static void index_info_tree(tree_node *tn, void *x)
{
    if (tn->kind() == TREE_INSTR) {
        instruction *ins = ((tree_instr *) tn)->instr();

	if (ins->opcode() == io_array) get_info((in_array *) ins, 
					      (index_info_params *) x);
	ins->src_map(index_info_src, x);
    }
}



static void index_info(int d,tree_for **f,tree_node_list *l,int *ipj,int *ici,int *other)
{
    index_info_params *x = new index_info_params();
    x->d = d;
    x->f = f;
    x->ipj = ipj;
    x->ici = ici;
    x->other = other;

    l->map(index_info_tree, x);
}


static void gather_index_info_for_stats(int d,tree_for **f,it_stats *s)
{
    if (tdebug['x']) {
	printf("gather_index_info_for\n");
	f[0]->print(stdout);
	fflush(stdout);
    }

    int i_plus_j=0,i_comma_i=0,other=0;
    index_info(d,f,f[0]->body(),&i_plus_j,&i_comma_i,&other);
    if(i_plus_j || i_comma_i || other) {
        if(i_plus_j) s->ref_i_plus_j++;
        if(i_comma_i) s->ref_i_i++;
        if(other) s->ref_other++;
    } else {
        s->ref_simple++;
    }
}

/*
 * For data gathering purposes, examine the bounds expressions.
 * Rectangular:
 *     do i = 1,n
 *       do j = 1,n
 *
 * Triangular:
 *      do i = 1,n
 *        do j = 1,i
 *
 * Minmax_tri:  (convex)
 *      do i = 1,n
 *        do j = max(3,i),min(n-4,n-i)
 *
 * Other: anything else
 */

enum boundstuff {av_rect,av_tri,minmax_rect,minmax_tri,boundstuff_other};

static boundstuff get_bound_info_for_stats(int i,tree_for **f,array_info *b)
{
    int count = 0;
    int triangular = 0;
    array_info_iter aii(b);
    while(!aii.is_empty()) {
        count++;
        access_vector *av = aii.step();
        if(av->too_messy || av->mod_this)
            return boundstuff_other;
        for(int j=0; j<i; j++) {
            if(av->val(f[j]))
                triangular++;
        }
    }
    if(count == 0) return av_rect;
    else if(count == 1) return triangular ? av_tri : av_rect;
    else return triangular ? minmax_tri : minmax_rect;
}

static void gather_bound_info_for_stats(int d,tree_for **f,it_stats *s)
{
    int minmax = 0,triangular = 0;
    for(int i=0; i<d; i++) {
        int unitstep = op_is_unity(f[i]->step_list());
        dep_for_annote *dfa = (dep_for_annote *) 
	    f[i]->peek_annote(k_dep_for_annote);
        assert(dfa);
       
        boundstuff lb = get_bound_info_for_stats(i,f,dfa->lb);
        boundstuff ub = get_bound_info_for_stats(i,f,dfa->ub);
        if(!unitstep || lb == boundstuff_other || ub == boundstuff_other) {
            s->bound_other++;
            return;
        }
        if(lb==minmax_tri || lb==av_tri || ub==minmax_tri || ub==av_tri)
            triangular++;
        if(lb==minmax_tri || ub == minmax_tri)
            minmax++;
    }
    if(minmax)
        s->bound_conv++;
    else if(triangular)
        s->bound_tri++;
    else
        s->bound_rect++;
}

/*
 * For data gathering purposes, examine the bounds expressions.
 *
 * dep_scalar: calls, etc., have made this a sequential nest
 * dep_none: no dependences at all (every loop in nest parallel)
 * dep_dv: all dependences are distance vectors
 * dep_dvx: all dependences would be distance vectors with trivial
 *          work (e.g. (+,0) => (1,0))
 * dep_fp: a fully permutable nest (all dependence components >= 0)
 * dep_seq: this is a sequential nest purely because of array dependences
 * dep_other: none of the above (funky dependences, interesting dependences)
 */

static void gather_deps_info_for_stats(nest_info *ni,it_stats *s)
{
    if(ni->deps == 0) {
        s->dep_scalar++;
        return;
    }

    if(ni->deps->count == 0) {
        s->dep_none++;
        return;
    }

    // Are they all distance vectors?
    int status=0;
    int i;
    for(i=0; i<ni->deps->count; i++) {
        int all_zeros = 1;
        int ndirs = 0;
        for(int j=0; j<ni->deps->dv[i]->len; j++) {
            distance &dist = ni->deps->dv[i]->d[j];
            if(!dist.is_const()) {
                ndirs++;
            } else if(dist.dist() != 0) {
                all_zeros = 0;
            }
        }
        if(ndirs > 1 || (ndirs == 1 && !all_zeros)) {
            status = 2;
            break;
        } else if(ndirs == 1)
            status = 1;
    }
    if(status == 0) s->dep_dv++;
    else if(status == 1) s->dep_dvx++;

    for(i=0; i<ni->deps->count; i++) {
        for(int j=0; j<ni->deps->dv[i]->len; j++) {
            distance &dist = ni->deps->dv[i]->d[j];
            if(dist.dir() & d_gt)
                goto next_try;
        }
    }
    s->dep_fp++;
    return;

  next_try:

    int satisfied[MAXVECS];
    boolean all_satisfied = FALSE;
    for(i=0; i<ni->deps->count; i++)
        satisfied[i] = 0;

    int j;
    for(j=0; (!all_satisfied) && (j < ni->depth); j++) {
        // see if any other loop could go next
        for(int jj=j+1; jj<ni->depth; jj++) {
            int none_neg=1;
            for(int i=0; i<ni->deps->count; i++) {
                if(!satisfied[i] &&
                   ni->deps->dv[i]->len > jj &&
                   ni->deps->dv[i]->d[jj].dir()&d_gt) {
                    none_neg = 0;
                    break;
                }
            }
            if(none_neg) {
                s->dep_other++;
                return;
            }
        }
        all_satisfied = TRUE;
        for(i=0; i<ni->deps->count; i++) {
            if(!satisfied[i]) {
                if(ni->deps->dv[i]->d[j].dir() == d_lt)
                    satisfied[i]++;
                else if(ni->deps->dv[i]->len-1 == j)
                    satisfied[i]++;
                else
                    all_satisfied = FALSE;
            }
        }
    }
    assert(all_satisfied);
    if(j == ni->depth) s->dep_seq++;
    else s->dep_other++;
}

/*
 * Find singly nested regions (TPDS'91) to transform: regions of the form
 *    do i
 *      S1
 *      do j
 *        S2
 *        ...
 *
 * Check that the region meets the requirements to be transformed
 * (normalizable bounds, non-catostrophic dependences).  If all
 * is well, call find_and_do_transform() and improve the nest!
 */ 

static void find_nests(tree_node_list *, tree_proc *);

static void find_nests_for(tree_for *nn,tree_proc *p)
{
    if (print_parallelism_info) {
	printf("\n-----------\n");
	printf("Found loop `%s' at line #%d\n", nn->index()->name(),
	       source_line_num(nn));
    }

    if(is_ncall_in_body(nn,1)) {
	handle_pragmas(nn);
        find_nests(nn->body(), p);
        return;
    }

    tree_for *forlist[MAXDEPTH];
    int d=0;

    for( ; nn; nn = only_inner_for(nn)) {
        it_stats *s = (it_stats *)p->peek_annote(k_it_stats);
        assert(s);

	it_for_annote *an = (it_for_annote *) nn->peek_annote(k_it_for);
	assert(an);

	if (print_parallelism_info) {
	    printf("\nExamining loop `%s'\n", nn->index()->name());

	    printf("Modified scalars: ");
	    sym_node_list_iter snli(an->modified);
	    while (!snli.is_empty()) {
		sym_node *sn = snli.step();
		printf(" %s%s", sn->name(), (snli.is_empty() ? "" : ","));

            }
	    printf("\nPrivatizable scalars: ");
	    snli.reset(an->privatizable);
	    while (!snli.is_empty()) {
		sym_node *sn = snli.step();
		printf(" %s%s", sn->name(), (snli.is_empty() ? "" : ","));
            }
	    printf("\nPotential reduction vars: ");
	    snli.reset(an->reduction_vars);
	    while (!snli.is_empty()) {
		sym_node *sn = snli.step();
		printf(" %s%s", sn->name(), (snli.is_empty() ? "" : ","));
            }
	    printf("\n");
	}

        if(an->cannot_transform) {
            s->depth_cannot[d]++;
            break;
        }

	// Only need to check for return instruction inside loop.  Don't
        // need to check for branches/labels since porky would have already
        // dismantled any for loops with branches to/from outside the loop
        //
        if(has_return_instr(nn->body())) {
            s->depth_goto[d]++;
	    if (tdebug['t'] || print_parallelism_info) {
                printf("Found return instruction inside loop\n");
            }
            break;
        }

        fill_in_access(p);
	boolean normalize_ok;
        fill_in_access(nn, NORMALIZE_CODE, &normalize_ok);

	if (tdebug['x']) {
	    printf("finding nest at loop\n");
	    nn->print(stdout);
	    fflush(stdout);
	}

        boolean bexpr_ok = bounds_ok(nn);
	boolean ok = bexpr_ok && normalize_ok;

        bexpr *lb = (bexpr *) nn->peek_annote(k_lbexpr);
        bexpr *ub = (bexpr *) nn->peek_annote(k_ubexpr);

        if(!ok && d==0) {
            move_out_lower_bound(nn);
            move_out_upper_bound(nn);

	    if (tdebug['x']) {
		printf("finding nest at loop\n");
		nn->print(stdout);
		fflush(stdout);
	    }

	    fill_in_access(nn, NORMALIZE_CODE, &normalize_ok);
	    bexpr_ok = bounds_ok(nn);

            if (!bexpr_ok) {  // Try one more time to move out bounds
                move_out_lower_bound(nn);
                move_out_upper_bound(nn);
	        bexpr_ok = bounds_ok(nn);
            }

	    ok = bexpr_ok && normalize_ok;
	    lb = (bexpr *) nn->peek_annote(k_lbexpr);
	    ub = (bexpr *) nn->peek_annote(k_ubexpr);

	    if (ok && (!lb->bx->av_max_only() || !lb->bx->av_min_only())) {
                if(lb) {
		    lb = (bexpr *) nn->get_annote(k_lbexpr);
		    delete lb;
		}
                if(ub) {
		    ub = (bexpr *) nn->get_annote(k_ubexpr);
		    delete ub;
		}
		ok = FALSE;
            } 
        } //if

        if (ok) {
            char *where, how[80];
	    boolean ugly = FALSE;
            if ((ugly=uglybound(nn->lb_op(),how))) where="lb";
            else if ((ugly=uglybound(nn->ub_op(),how))) where="ub";
            else if ((ugly=uglybound(nn->step_op(),how))) where="step";

            if(ugly) {
                printf("loop%s: %s modified (%s), can't transform\n",
                       nn->index()->name(),where,how);
                ok = 0;
            }
            if(ok && (lb->divisor != 1 || ub->divisor != 1)) {
                printf("loop %s: bound has nonzero divisor, can't transform\n",
                       nn->index()->name());
                if(lb) {
		    lb = (bexpr *) nn->get_annote(k_lbexpr);
		    delete lb;
		}
                if(ub) {
		    ub = (bexpr *) nn->get_annote(k_ubexpr);
		    delete ub;
		}
                ok = 0;
            }
        }
        if (ok) {
            assert(d < MAXDEPTH);
            forlist[d] = nn;
            d++;
        }
        else {
            if (tdebug['t'] || print_parallelism_info) {
		printf("\nLoop bounds in loop `%s' too ugly; "  
                       "giving up on loop nest of depth %d\n", 
                        nn->index()->name(), d+1);
            }
            s->depth_bounds[d]++;
            break;
        }
    } // for

    forlist[d] = 0;
              
    if(d == 0) {
	handle_pragmas(nn);
        find_nests(nn->body(), p);
        return;
    }

    if(!normalize_only) {
        /* recalculate access vectors for our nest */
        fill_in_access(p);
        it_stats *s = (it_stats *)p->peek_annote(k_it_stats);
        assert(s);

        nest_info *ni = new nest_info(d,forlist);

        // gather statistics
        if (d > 1 && !has_loop_inside(forlist[d-1]->body())) {
            if (tdebug['t']) {
                printf("Found interesting loop nest, depth %d %s\n", d,
                    ni->deps ? "" : "<sequential>");
                ni->print(stdout);
            }
            s->num++;
            s->depth[d]++;
            gather_bound_info_for_stats(d,forlist,s);
            gather_index_info_for_stats(d,forlist,s);
            gather_deps_info_for_stats(ni,s);
        }
        if(ni->deps)  find_and_do_transform(ni);

	for (int i = 0; i < d; i++) {
	    handle_pragmas(forlist[i]);
	}

        delete ni;
    }

    find_nests(forlist[d-1]->body(), p);
}


static void find_nests(tree_node_list *l, tree_proc *p)
{
// Don't use map because don't want to call bodies of for recursively

    assert(l);
    tree_node_list_iter ani(l);
    while(!ani.is_empty()) {
        tree_node *n = ani.step();
        switch(n->kind()) {
          case TREE_INSTR:
            break;
          case TREE_FOR:
            find_nests_for((tree_for *)n,p);
            break;
          case TREE_LOOP:
            find_nests(((tree_loop *)n)->body(), p);
            find_nests(((tree_loop *)n)->test(), p);
            break;
          case TREE_IF:
            find_nests(((tree_if *)n)->header(), p);
            find_nests(((tree_if *)n)->then_part(), p);
            find_nests(((tree_if *)n)->else_part(), p);
            break;
	  case TREE_BLOCK:
            find_nests(((tree_block *)n)->body(), p);
	    break;
         }
    }
}


void find_regions_and_transform(tree_proc *p)
{
    find_nests(p->body(), p);
}

//
// Move any doall annotations (from pragmas) from before the loop onto
// the loop itself.  Also turn chaotic annotations into doall annotations
// on the loop, but honor reductions by turning any reduction annotations 
// into reduced annotations.
//
static void handle_pragmas(tree_for *tf)
{
    boolean use_reductions = FALSE;

    if (tf->peek_annote(k_doall)) return;  // Already has annotation,
                                           // nothing to do.

    tree_node_list_e *cur_node_e = tf->list_e()->prev();
    tree_node_list_e *prev_node_e;

    while (cur_node_e && cur_node_e->contents->is_instr()) {
	prev_node_e = cur_node_e->prev();
	instruction *ins = ((tree_instr *) cur_node_e->contents)->instr();

	annote_list_iter ali(ins->annotes());
	while (!ali.is_empty()) {
	    annote *an = ali.step();

	    if (an->name() == k_doall) {
		tf->append_annote(k_doall, new immed_list());
		while (tf->get_annote(k_doacross)) ;
		break;
	    }

	    else if (an->name() == k_chaotic) {
		tf->append_annote(k_doall, new immed_list());
		while (tf->get_annote(k_doacross)) ;
		use_reductions = TRUE;
		break;
	    }

	    else if (an->name() == k_C_pragma) {
		immed im = an->immeds()->head()->contents;
		if (im.is_string()  && 
		    (!strcmp(im.string(), k_doall) ||
		     !strcmp(im.string(), k_chaotic))) {

		    if (!strcmp(im.string(), k_chaotic)) use_reductions = TRUE;
		    tf->append_annote(k_doall, new immed_list());
		    while (tf->get_annote(k_doacross)) ;
		    break;
		}
	    }
	}

	cur_node_e = prev_node_e;
    }

    // Convert "reduction" annotations to "reduced" annotations.
    //
    if (use_reductions) {
	annote_list_iter ali(tf->annotes());
	while (!ali.is_empty()) {
	    annote *an = ali.step();

	    if (an->name() == k_reduction) {
		sym_node *sn = (*(an->immeds()))[1].symbol();
		assert(sn->is_var());
	        immed_list *il = new immed_list();
		il->copy(an->immeds());
		tf->append_annote(k_reduced, il);
	    }
        }
    }
}
