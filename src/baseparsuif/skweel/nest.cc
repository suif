/* file "nest.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Loop nest information */

#pragma implementation "nest.h"

#define RCS_BASE_FILE nest_cc

#include "skweel.h"
#include "reflists.h"
#include "nest.h"
#include "params.h"
#include "ugsets.h"
#include "dv.h"

#include <annotes.h>

RCS_BASE("$Id: nest.cc,v 1.2 1999/08/25 03:27:08 brm Exp $")


enum alias_kind {
    INDEPENDENT,
    SAME_BASE,
    ALIASED_WITH_OFFSET,
    ALIASED_NO_OFFSET
};


reduction_info::reduction_info(var_sym *var)
{ 
    reduction_var = var; 
    reduction_deps = new DVlist; 
}

reduction_info::~reduction_info()
{ 
    if (reduction_deps) delete reduction_deps;
}

reduction_list::~reduction_list()
{
    reduction_list_iter rli(this);
    while (!rli.is_empty()) delete rli.step();
}

reduction_info *reduction_list::lookup(var_sym *var)
{
    reduction_list_iter rli(this);
    while (!rli.is_empty()) {
	reduction_info *ri = rli.step();
	if (ri->reduction_var == var) return ri;
    }

    return NULL;
}


reduction_info *reduction_list::get_info(var_sym *var)
{
    reduction_info *ri = lookup(var);
    if (ri) return ri;

    ri = new reduction_info(var);
    this->append(ri);

    return ri;
}


void nest_info::print(FILE *f)
{
    int i;

    fprintf(f,"NEST INFO: depth = %d, outerdepth = %d,  ",depth,outerdepth);
    
    fprintf(f,"indices ");
    for(i=0; i<depth; i++) {
        fprintf(f,"%s%c",forlist[i]->index()->name(),i==depth-1?'\n':',');
    }
    fprintf(f,"perfect from ");
    for(i=0; i<outerpcount; i++) {
        tree_for *ox = forlist[outerperfect[i]];
        tree_for *ix = forlist[outerperfect[i+1]-1];
        fprintf(f,"%s to %s%c",ox->index()->name(),ix->index()->name(),
                               i==outerpcount-1?'\n':',');
    }
    fprintf(f,"\nfp nests ");
    for (i = 0; i < fpcount; i++) {
        tree_for *tf1 = forlist[fp[i]];
        tree_for *tf2 = forlist[fp[i+1]-1];
        fprintf(f,"%s to %s%c",tf1->index()->name(),tf2->index()->name(),
                               i==fpcount-1?'\n':',');
    }
    fprintf(f, "\n");
	
    if(deps) {printf("DEPS: "); deps->print(f);}
    if(rlocs || plocs) {
        assert(rlocs && plocs);
        printf("INNER LOOP UGSETS:\n");
        rlocs->print(stdout);
        plocs->print(stdout);
    }
    fprintf(f,"END OF PERFECT NEST INFO\n");
}

/*
 * al typically would be the loops that r1 and r2 have in common.
 * Actually, it turns out to be more precise to do that, yes, but
 * then if it turns out if r2 is nested within the same loops as
 * r1, then use r2's nesting instead of r1's (according to Dror).
 */

static int set_al(nest_info *ni,refs_e *r1,refs_e *r2,tn_list *al)
{
    tree_for *outer = ni->forlist[0];

    // the head of the tn_list is the innermost.

    tree_for *r1f = (tree_for *) 
	next_further_out((tree_node *) r1->memref->parent(), TREE_FOR);
    tree_for *r2f = (tree_for *) 
	next_further_out((tree_node *) r2->memref->parent(), TREE_FOR);

    // find innermost common loop: that's n
    tree_for *n = r1f;
    for( ; n != outer; n = (tree_for *) next_further_out((tree_node *) n, TREE_FOR)) {
        tree_for *nx = r2f;
        while(nx != outer && nx != n)
            nx = (tree_for *) (next_further_out((tree_node *) nx, TREE_FOR));
        if(nx == n)
            break;
    }

    /* goofy part, necessary for current dependence analyzer */
    tree_for *n2 = n;
    if(r1f == n) n2 = r2f;
    else if(r2f == n) n2 = r1f;

    /* and now putting these on an tn_list */
    int rval = 0;
    assert(al == 0 || al->is_empty());
    int counting = 0;
    for( ; n2; n2 = (tree_for *) (next_further_out((tree_node *) n2, TREE_FOR))) {
        if(al) al->append(n2);
        if(n == n2) counting=1;
        if(counting) rval++;
    }
    assert((al == 0 || !al->is_empty()) && rval > 0);
    return rval;
}


static boolean insert_one_dvect(nest_info *ni,DVlist *curr_deps,distance_vector *d,int ndepth,tn_list *al,int dlength)
{
    // We only care about the perfectly nested components.  So any outside
    // the outer -> independence if can't be zero.  Any inside the inner
    // are truncated.  Just keep the middle ones ("depth" components).

    int i;
    int outer_seen = 0;
    tree_for *outer = ni->forlist[0];

    if(tdebug['N']) {
        printf("attempting to insert ");
        d->print(stdout);
    }

    tn_list_iter ai(al);
    while(!ai.is_empty()) {
        tree_node *n = ai.step();
        if(outer_seen == 0) {
            if(n == outer) outer_seen = 1;
            continue;
        }
        distance_vector_e *de = d->pop();
        switch(de->d.dir()) {
        case d_ge: case d_le: case d_eq: case d_star:
            delete de;
            break;		// ok, could be =
        default:
            delete de;
            if(tdebug['N']) printf(" lex pos or neg outside region\n");
            return FALSE;		// definitely independent
        }
    }

    if(ndepth > ni->depth) ndepth = ni->depth;
    if(ndepth > dlength) ndepth = dlength;

    while(d->count() > ndepth) {
        distance_vector_iter x(d);
        for(i=0; i<ndepth; i++)
            x.step();
        distance_vector_e *de = x.step();
        d->remove(de);
        delete de;
    }

    if(tdebug['N']) {
        printf(" consider truncated dependence vector ");
        d->print(stdout);
        printf("\n");
    }

    return (curr_deps->insert(d,ndepth));
}

static boolean insert_dvect(nest_info *ni, DVlist *curr_deps, refs_e *r1,
			    refs_e *r2, alias_kind alias = SAME_BASE)
{
    if (alias == INDEPENDENT) return FALSE;

    if(r1->iarr == 0 || r2->iarr == 0) {
        if(tdebug['N']>1) printf("dependence with bad array instruction\n");
        curr_deps->stars(MAXDEPTH);
        return FALSE;
    }

    // modify tn_list to be correct for this pair.  Just modify
    // al, which will hopefully be "pretty close".
    //
    tn_list *al = new tn_list;
    int common_nest_depth = set_al(ni,r1,r2,al) - ni->outerdepth;

    if (alias == ALIASED_WITH_OFFSET) {
	curr_deps->stars(common_nest_depth);
	delete al;
	return FALSE;
    }
    if (!(r1->instr_type->is_same(r2->instr_type))) {
        if(tdebug['N']>1) printf("array has accesses of different types\n");
        curr_deps->stars(common_nest_depth);
        delete al;
        return FALSE;
    }

    dep_instr_annote *r1_dia = 
       (dep_instr_annote *) r1->iarr->peek_annote(k_dep_instr_annote);
    assert(r1_dia);

    dep_instr_annote *r2_dia = 
       (dep_instr_annote *) r2->iarr->peek_annote(k_dep_instr_annote);
    assert(r2_dia);

    if (!(r1_dia->ai->count() == r2_dia->ai->count())) {
        if(tdebug['N']>1) 
            printf("array has access vectors of different lengths\n");
        curr_deps->stars(common_nest_depth);
        delete al;
        return FALSE;
    }

    if(tdebug['N']) {
        printf("comparing ");
        r1_dia->ai->print(stdout);
        printf(" and ");
        r2_dia->ai->print(stdout);
        printf("\n");
    }

    // Call dependence test in dependence library. Set lexpos FALSE b/c
    // the vectors are converted to lexpos in dv.cc
    //
    deptest_result res;
    boolean is_aliased = (alias != SAME_BASE);
    dvlist *d = DependenceTest(r1->iarr, r2->iarr, FALSE, &res, is_aliased); 
    assert_msg(d,
        ("No dependence vectors returned from library (res=%d)", res));

    boolean added_vector = FALSE;
    if (!d->indep()) {
        dvlist_iter di(d);
        while(!di.is_empty()) {
            if (insert_one_dvect(ni, curr_deps, di.step()->dv,
	      al->count() - ni->outerdepth, al, common_nest_depth)) 
                  added_vector = TRUE;
        }
    } 

    else if (tdebug['N']) printf("...independent");
    if(tdebug['N']) printf("\n");
    
    delete al;
    d->clear();
    delete d;

    return added_vector;
}


// Extract all the "array_base" annotations
// 
static annote_list *
make_array_base_annote_list(refs_e *r)
{
    annote_list *al = new annote_list();

    if (r->iarr == NULL) return al;
    
    annote_list_iter ali(r->iarr->annotes());
    while (!ali.is_empty()) {
	annote *an = ali.step();
	if (an->name() == k_wilbyr_array_base) {
	    al->append(an);
	}
    }

    return al;
}


alias_kind compare_alias_lists(annote_list *al1, annote_list *al2)
{
    assert(al1 && !al1->is_empty());
    assert(al2 && !al2->is_empty());
    w_points_to *wpt1, *wpt2;
    int stride, offset;

    alias_kind alias = INDEPENDENT;

    annote_list_iter ali1(al1);
    annote_list_iter ali2(al2);
    int cur_clone_num;
    while (!ali1.is_empty() && !ali2.is_empty()) {
      wpt1 = WILBYR_ARRAY_BASE(ali1.step());
      wpt2 = WILBYR_ARRAY_BASE(ali2.step());

      cur_clone_num = MAX(wpt1->clone_num(), wpt2->clone_num());

      // Ignore clones that don't match.  Assumes array_base
      // annotations are in order.
      //
      while ((wpt1->clone_num() != cur_clone_num) && (!ali1.is_empty())) {
	wpt1 = WILBYR_ARRAY_BASE(ali1.step());
      }
      while ((wpt2->clone_num() != cur_clone_num) && (!ali2.is_empty())) {
	wpt2 = WILBYR_ARRAY_BASE(ali2.step());
      }

      assert(wpt1->clone_num() == wpt2->clone_num());
      if (wpt1->points_to()->aliased(wpt2->points_to(), &offset, &stride)) {

	  if ((stride == 0) && (offset == 0)) alias = ALIASED_NO_OFFSET;
	  else return ALIASED_WITH_OFFSET;
      }
    }

    return alias;
}

alias_kind 
compare_sym_to_list(sym_addr &base, base_symtab *scope, annote_list *al)
{
    assert(al && !al->is_empty());
    w_points_to *wpt;
    int stride, offset;

    alias_kind alias = INDEPENDENT;

    annote_list_iter ali(al);
    while (!ali.is_empty()) {
      wpt = WILBYR_ARRAY_BASE(ali.step());
      if (wpt->points_to()->aliased(base.symbol(), scope, wpt->clone_num(),
				    &offset, &stride)) {

	  if ((stride == 0) && (offset == (int)base.offset())) 
	      alias = ALIASED_NO_OFFSET;
	  else 
	      return ALIASED_WITH_OFFSET;
      }
    }

    return alias;
}

//
// Compare lists of read and write references looking for dependences
// If there is no array_base annotation, then the reference is considered
// to not be aliased.
//
static void compare_refs(nest_info *ni, refs *r,refs *w)
{
    DVlist *dv = ni->deps;
    
    if(dv->star_level >= ni->depth) return;

    refs_iter riw(w);
    while(!riw.is_empty()) {
        refs_e *rwe = riw.step();
        assert(rwe->base.symbol()->is_var());
        var_sym *pbw = (var_sym *) rwe->base.symbol();

	annote_list *aliw = make_array_base_annote_list(rwe);
	base_symtab *wscope = rwe->iarr->parent()->scope();

        refs_iter rix(r);
        while(!rix.is_empty()) {
            refs_e *rxe = rix.step();
            assert(rxe->base.symbol()->is_var());

	    alias_kind alias = INDEPENDENT;
	    if (rxe->iarr) {
	    annote_list *alix = make_array_base_annote_list(rxe);

	    if ((aliw->is_empty()) && (alix->is_empty())) {
		var_sym *pbx = (var_sym *) rxe->base.symbol();
		if (pbx->overlaps(pbw)) alias = SAME_BASE;
	    } else if (!aliw->is_empty() && !alix->is_empty()) {
		alias = compare_alias_lists(aliw, alix);
	    } else if (aliw->is_empty()) {
		alias = compare_sym_to_list(rwe->base, wscope, alix);
	    } else {
		assert(alix->is_empty());
		base_symtab *rscope = rxe->iarr->parent()->scope();
		alias = compare_sym_to_list(rxe->base, rscope, aliw);
	    }
	    }
	    
	    insert_dvect(ni, ni->deps, rwe, rxe, alias);
        }
        rix.reset(w);
        while(!rix.is_empty()) {
            refs_e *rxe = rix.step();

	    if (rwe->memref->annotes()->peek_annote(k_reduction) &&
		rxe->memref->annotes()->peek_annote(k_reduction)) continue;

            assert(rxe->base.symbol()->is_var());
	    annote_list *alix = make_array_base_annote_list(rxe);
	    alias_kind alias = INDEPENDENT;

	    if ((aliw->is_empty()) && (alix->is_empty())) {
		var_sym *pbx = (var_sym *) rxe->base.symbol();
		if (pbx->overlaps(pbw)) alias = SAME_BASE;
	    } else if (!aliw->is_empty() && !alix->is_empty()) {
		alias = compare_alias_lists(aliw, alix);
	    } else if (aliw->is_empty()) {
		alias = compare_sym_to_list(rwe->base, wscope, alix);
	    } else {
		assert(alix->is_empty());
		base_symtab *rscope = rxe->iarr->parent()->scope();
		alias = compare_sym_to_list(rxe->base, rscope, aliw);
	    }
				      
	    insert_dvect(ni, ni->deps, rwe, rxe, alias);
        }
    }
}


/*
 * Find all dependences for this list of references
 */

static boolean has_alias_info(refs &r)
{
    refs_iter ri(&r);
    while (!ri.is_empty()) {
	refs_e *ref = ri.step();
	if ((!ref->iarr) || (!ref->iarr->peek_annote(k_wilbyr_array_base)))
	    return FALSE;
    }

    return TRUE;
}

void nest_info::make_dependence_list()
{
    deps = new DVlist;
    
    if (references->everything_touched) { 
      deps->stars(MAXDEPTH); 
      return; 
    }
    
    // No writes, and all temps privatizable -> no dependences
    //
    if (references->rg_w.is_empty() && references->p_w.is_empty() && 
       references->sl_w.is_empty() && references->pl_w.is_empty() && 
       references->g_w.is_empty()) {
        return;
    }
    
    // We don't know the address for indirections through a variable without
    // alias analysis.
    //
    boolean alias_info = (has_alias_info(references->rg_w) && 
                          has_alias_info(references->rg_r));
    if (!alias_info) {
      deps->stars(MAXDEPTH);
      return;
    }
    
    // If Fortran, we know that parameters aren't aliased.  
    // If C then we need alias information to figure anything out.
    //
    if (!is_fortran) {
      alias_info = (has_alias_info(references->p_w) &&
		    has_alias_info(references->p_r));

    } else {
      //
      // Make sure that param vars aren't written in Fortran. If so, then 
      // the assumption that Fortran parameters aren't aliased is not valid.
      // This could happen if some other pass transformed the original 
      // Fortran code.
      //
      refs_iter ri(&references->p_r);
      while(!ri.is_empty() && alias_info) {
	  refs_e *ref = ri.step();
	  param_usage *u = 
	      (param_usage *) ref->base.symbol()->peek_annote(k_param_usage);
	  if (!u || u->is_written()) {
	      alias_info = FALSE;
	  }
      }
      ri.reset(&references->p_w);
      while(!ri.is_empty() && alias_info) {
	  refs_e *ref = ri.step();
	  param_usage *u = (param_usage *) 
	      ref->base.symbol()->peek_annote(k_param_usage);
	  if (!u || u->is_written()) {
	      alias_info = FALSE;
	  }
      }
    }

    if (!alias_info) {
	deps->stars(MAXDEPTH);
	return;
    }

    // At this point we must have alias information for references 
    // that are indirections through variables (rg_r,rg_w), and parameters
    // (p_r,p_w) if C or we've given up.  
    // 
    // We have the following classes of references:
    // 
    // params (p_w, p_r), indirections (rg_w, rg_r), globals (g_w, g_r),
    // statics (sl_r, sl_w) and locals (pl_r, pl_w)
    //
    // 
    // The following classes can overlap:
    //
    // indirections vs. params
    // indirections vs. globals   
    // indirections vs. statics   
    // indirections vs. locals    
    // params vs. globals  (only in C)
    // params vs. statics  (only in C)
    // params vs. locals   (only in C)
    // 
    // The following are independent:
    //
    // globals vs. statics
    // globals vs. locals
    // statics vs. locals
    //

    //
    // Compare references across classes:
    //
    compare_refs(this, &references->rg_r, &references->p_w);
    compare_refs(this, &references->p_r, &references->rg_w);

    compare_refs(this, &references->rg_r, &references->g_w);
    compare_refs(this, &references->g_r, &references->rg_w);

    compare_refs(this, &references->rg_r, &references->sl_w);
    compare_refs(this, &references->sl_r, &references->rg_w);

    compare_refs(this, &references->rg_r, &references->pl_w);
    compare_refs(this, &references->pl_r, &references->rg_w);

    if (!is_fortran) {
	compare_refs(this, &references->p_r, &references->g_w);
	compare_refs(this, &references->g_r, &references->p_w);

	compare_refs(this, &references->p_r, &references->sl_w);
	compare_refs(this, &references->sl_r, &references->p_w);

	compare_refs(this, &references->p_r, &references->pl_w);
	compare_refs(this, &references->pl_r, &references->p_w);
    }

    //
    // Compare references within each class:
    //
    compare_refs(this, &references->rg_r, &references->rg_w);
    compare_refs(this, &references->p_r, &references->p_w);
    compare_refs(this, &references->g_r, &references->g_w);
    compare_refs(this, &references->sl_r, &references->sl_w);
    compare_refs(this, &references->pl_r, &references->pl_w);
}

// Find dependences caused by variables involved in a reduction.
//
void nest_info::find_reduction_deps()
{
    references->build_reductions(forlist[0]->body());
    // Find dependences that can be ignored b/c they're involved in a 
    // reduction:
    reductions = new reduction_list();
    
    refs_iter riw(&references->reduction_w);
    while(!riw.is_empty()) {
        refs_e *rwe = riw.step();
	assert(rwe->base.symbol()->is_var());
        var_sym *pbw = (var_sym *) rwe->base.symbol();

        refs_iter rix(&references->reduction_w);
        while(!rix.is_empty()) {
            refs_e *rxe = rix.step();
	    assert(rxe->base.symbol()->is_var());
            var_sym *pbx = (var_sym *) rxe->base.symbol();
            if(pbx->overlaps(pbw)) {
		assert(pbx == pbw);  // if reduction better be same symbol

		reduction_info *ri = reductions->get_info(pbx);

                if (insert_dvect(this,ri->reduction_deps,rwe,rxe)) {
                    // If deps b/c of these references, add "reduced"
		    // annotation 
		    annote *an = 
			rwe->memref->annotes()->peek_annote(k_reduction);
		    assert(an);
		    immed_list *il = new immed_list();
		    il->copy(an->immeds());
                    rwe->memref->append_annote(k_reduced, il);
                }
            }
        }
    }
}


void nest_info::make_locality_list()
{
    if (!has_loop_inside(forlist[depth-1]->body())) {
        reflists *rf = new reflists(forlist[depth-1]);
        // rf->append_scalars(forlist,depth);
        
        rlocs = new set_of_ugset(forlist,depth);
        plocs = new set_of_ugset(forlist,depth);
        
        plocs->append(&rf->g_r);  plocs->append(&rf->g_w);
        plocs->append(&rf->sl_r); plocs->append(&rf->sl_w);
        plocs->append(&rf->pl_r); plocs->append(&rf->pl_w);
        rlocs->append(&rf->rg_r); rlocs->append(&rf->rg_w);
        rlocs->append(&rf->p_r);  rlocs->append(&rf->p_w);
        
        delete rf;
    }
}

nest_info::nest_info(int d,tree_for **f):rlocs(0),plocs(0)
{
    int i, lvp, j;

    assert(d>=1);
    depth = d;
    for(i=0; i<depth; i++)
        forlist[i] = f[i];
    outerdepth = -1;
    for(tree_node *af = forlist[0]; af; af = next_further_out(af, TREE_FOR))
        outerdepth++;
    assert(outerdepth >= 0);
    assert(depth <= MAXDEPTH);

    forlist[depth] = 0;
    outerpcount = 0;
    outerperfect[0] = 0;
    fp[0] = 0;
    fpcount = 0;
    for(i=0; i<depth-1; i++) {
        tree_for *q = next_perfect_inner(forlist[i]);
        if(q == forlist[i+1]) {
            ;
        } else if(q) {
            assert(0);
        } else {
            outerperfect[++outerpcount] = i+1;
        }
    }
    outerperfect[++outerpcount] = depth;
    if(tdebug['N'] || (tdebug['t']>1 && depth>1)) {
        printf("perfect loop nests: \n");
        for(i=0; i<outerpcount; i++)
            printf("\t%d to %d\n",outerperfect[i],outerperfect[i+1]-1);
    }


    references = new reflists(forlist[0]);
    if(tdebug['N']) references->print(stdout);

    reductions = NULL;
    make_dependence_list();  // Fills in deps

    if(deps->star_level >= depth) {
        if(tdebug['N']) printf("All stars ... can't transform\n");
        delete deps;
        deps = 0;
        return;
    }

    if(deps->star_level) {
        // just pretend the loop is not nested as deeply
        int lv = deps->star_level;
        assert(lv > 0);
        
        if(tdebug['t'])
            printf("%d outermost loops are sequential, remove.\n",lv);

        depth -= lv;
        outerdepth += lv;

        if(depth < 1) {
            delete deps;
            deps = 0;
            return;
        }

        for(i=0; i<depth; i++)
            forlist[i] = forlist[i+lv];
        forlist[i] = 0;
        for(lvp=0; outerperfect[lvp] < lv; lvp++)
            ;
        if(outerperfect[lvp] > lv) lvp--; // as opposed to equal
        assert(lvp >= 0);
        for(i=0; outerperfect[i+lvp] < depth+lv; i++) {
            outerperfect[i] = outerperfect[i+lvp]-lv;
            if(i == 0) {
                assert(outerperfect[i] <= 0);
                outerperfect[i] = 0;
            } else {
                assert(outerperfect[i] > 0);
            }
        }
        assert(outerperfect[i+lvp] == depth+lv);
        outerperfect[i] = depth;
        outerpcount -= lvp;
        assert(outerpcount == i);

        // now fix the dependences, which are too deep.

        int new_count=0;
        for(i=0; i<deps->count; i++) {
            DVlist_e *dv = deps->dv[i];
            int keep = 0;
            if(dv->len > lv) {
                keep = 1;
                for(j=0; j < lv; j++) {
                    switch(dv->d[j].dir()) {
                    case d_ge: case d_le: case d_eq: case d_star:
                        break;		// ok, could be =
                    default:
                        keep = 0;
                    }
                }
                if(keep) {
                    int alleq = 1;
                    for( ; j<dv->len; j++) {
                        if(dv->d[j].dir() != d_eq)
                            alleq = 0;
                    }
                    if(alleq) keep = 0;
                }
            }
            if(keep) {
                dv->len -= lv;
                for(j=0; j<dv->len; j++)
                    dv->d[j] = dv->d[j+lv];
                deps->dv[new_count++] = deps->dv[i];
            } else
                delete dv;
        }
        deps->count = new_count;
    }

    if(d > 1)
        make_locality_list();

    if (print_parallelism_info) {
	printf("\nLoop nest of depth %d: loops (", depth);
	for (i=0; i < depth; i++) 
	    printf(" %s ", forlist[i]->index()->name());
	printf(")\n");

	printf("Lexicographically positive dependence vectors:\n");
	deps->print(stdout);
    }
}

nest_info::~nest_info()
{
    if(deps) delete deps;
    if(references) delete references;
    if(rlocs) delete rlocs;
    if(plocs) delete plocs;
    if(reductions) delete reductions;
}



