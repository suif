/* file "dv.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Distance vectors */

#ifndef DV_H
#define DV_H

#pragma interface

RCS_HEADER(dv_h, "$Id: dv.h,v 1.2 1999/08/25 03:27:08 brm Exp $")


// A single distance vector e.g. (<,3).
// This structure is implemented as an array of distances.

class DVlist_e {
friend class DVlist;
    int is_all_stars();
public:
    int len;                      // elements in vector
    distance d[MAXDEPTH];         // distances
    int is_positive();            // lexicographically positive?
    int is_zero();                // all components zero?
    void reverse_all(int from=0); // negate a vector, starting at 'from'
    DVlist_e(int l):len(l) {assert(len < MAXDEPTH);}
    DVlist_e():len(-1) {}
    DVlist_e(DVlist_e *);
    ~DVlist_e() {}
    friend int operator==(DVlist_e&,DVlist_e&);  // two vectors identical?
    friend int operator!=(DVlist_e&,DVlist_e&);
    void print(FILE * =stdout);
};

// A list of distance vectors, actually again an array.
// Dependences are lexicographically positive.

class DVlist {
    boolean ins(DVlist_e *,int positive_only = 0);
    void final_ins(DVlist_e *);
public:
    int count;                     // vectors on list
    int star_level;                // count of leading stars (efficiency hack)
    void stars(int level) {if(level>star_level) star_level = level;}
    DVlist_e *dv[MAXVECS];         // the vectors.  
    DVlist();
    DVlist(DVlist *);
    ~DVlist();
    boolean insert(distance_vector *,int depth);
    void print(FILE * =stdout);
};

#endif /* DV_H */
