/* file "order.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Ordering the loop nests */

#define RCS_BASE_FILE order_cc

#include "skweel.h"
#include "nest.h"
#include "ugsets.h"
#include "dv.h"
#include "ast_it.h"
#include <suifmath.h>
#include <useful.h>
#include <string.h>

RCS_BASE("$Id: order.cc,v 1.2 1999/08/25 03:27:09 brm Exp $")

extern tree_for *next_perfect_inner(tree_for *tf);  // transform/lptrans.cc

static boolean is_loop_small(tree_for *tf);
static boolean are_bounds_small(tree_for *tf);
static boolean is_nested(tree_for *outer, tree_for *inner);
static boolean calc_upper_bound_on_min(instruction *ins, int *val);
static boolean calc_lower_bound_on_max(instruction *ins, int *val);

// old looptrans data structures

struct tile_info {
    boolean nontrivial_tile;     // if false, do nothing
    boolean no_tiling;           // don't do the tiling
    int regions;                 // number of tiling regions
    int coalesce[MAXDEPTH];      // coalesce the given region?
    int trip[MAXDEPTH];          // trip size for loop (not region)
    int first[MAXDEPTH+1];       // first loop in region.  Must have
                                 //     first[regions] = depth
    void print(FILE *);          // output this structure
    boolean innermost;           // if no loops inside
    tile_info():nontrivial_tile(FALSE),no_tiling(do_not_tile),regions(0),
                innermost(FALSE)  {first[0] = 0;}
    ~tile_info() {}
};

struct Tinfo {
    int depth;                 // number of loops in nest of interest
    tree_for *loops[MAXDEPTH]; // the loops
    boolean nontrivial_U;      // if 0, then don't do a unimodular transf.
    int U[MAXDEPTH][MAXDEPTH]; // the transformation itself, if nontrivial_U
    int doall[MAXDEPTH];       // just appends a "doall" annotation to the loop
    tile_info tile;            // how to tile
    void print(FILE *);        // print this structure
    Tinfo():depth(0),nontrivial_U(FALSE)  {  } 
    ~Tinfo();
};


// ----------------- transformation info local to this file ------

struct skew_info {
    boolean do_not_skew;         // i.e. assume identity skew matrix
    int sk[MAXDEPTH][MAXDEPTH];  // skew matrix: first elt is ROW, i.e. LOOP 
    void print(FILE *,int depth);
    skew_info() { do_not_skew=TRUE; }
};

struct tinfo {
    int depth;                   // loop nest depth 
    matrix *T;                   // set by calcT (linear transformation) 
    tile_info tile;              // how to tile
    skew_info skew;              // how to skew
    int permutation[MAXDEPTH];   // reordering the loops 
    int reversal[MAXDEPTH];      // 0=don't reverse, 1=reverse 
    int is_doall[MAXDEPTH];      // after transf, which loops parallel?
    void print(FILE *);
    tinfo();
    ~tinfo();
    boolean is_parallel()
        {assert(tile.regions <= depth);
         return (tile.regions < depth);}
    int is_null_transform();     // if no permute, no rev., no tile 
    void calcT();                // translate permutation, reversal and 
                                 // skew into linear transformation T 
};


Tinfo::~Tinfo()
{ }

void Tinfo::print(FILE *f)
{
    int i, j;

    fprintf(f,"Tinfo DUMP (depth=%d) (",depth);
    for(i=0; i<depth; i++)
        fprintf(f,"%s%s ",loops[i]->index()->name(),doall[i]?"(DOALL)":"");
    fprintf(f,")\n");
    if(nontrivial_U) {
        for(i=0; i<depth; i++) {
            fprintf(f,"\t");
            for(j=0; j<depth; j++)
                fprintf(f,"%d ",U[i][j]);
            fprintf(f,"\n");
        }
    } else {
        fprintf(f,"<no transformation matrix>\n");
    }
    tile.print(f);
    fprintf(f,"END Tinfo DUMP\n");
}


void tile_info::print(FILE *f)
{
    int i;
    int depth = first[regions];

    if(nontrivial_tile) {
        assert(regions>0 && regions <= MAXDEPTH);
        fprintf(f,"tiling regions %d: (first:",regions);
        for(i=0; i<=regions; i++)
            fprintf(f," %d",first[i]);
        fprintf(f,") (trips:");
        for(i=0; i<depth; i++)
            fprintf(f," %d",trip[i]);
        fprintf(f,")");
        if(no_tiling) fprintf(f,"<<don't tile>>");
        if(innermost) fprintf(f,"<<innermost>>");
        fprintf(f,"\n");
    } else {
        fprintf(f,"<no tiling>\n");
    }
}


void skew_info::print(FILE *f,int depth)
{
    int i, j;

    if(do_not_skew)
        fprintf(f,"<no skewing>\n");
    else {
        fprintf(f,"skew:");
        for(i=0; i<depth; i++) {
            for(j=0; j<=i; j++)
                fprintf(f,"\t%d",sk[i][j]);
            fprintf(f,"\n");
        }
    }
}
void tinfo::print(FILE *f)
{
    int i;

    fprintf(f,"TINFO DUMP (depth=%d)\n",depth);
    tile.print(f);
    skew.print(f,depth);
    fprintf(f,"permutation: (");
    for(i=0; i<depth; i++)
        fprintf(f,"%d ",permutation[i]);
    fprintf(f,")     reversal: (");
    for(i=0; i<MAXDEPTH; i++)
        fprintf(f,"%d ",reversal[i]);
    fprintf(f,")\n");
    if(T) {
        fprintf(f,"transformation matrix:\n");
        T->print(f);
    } else {
        fprintf(f,"<no transformation matrix>\n");
    }
    fprintf(f,"END TINFO DUMP\n");
}

tinfo::tinfo()
{
    int i;
    T = 0;
    depth = 0;

    for(i=0; i<MAXDEPTH; i++) {
        permutation[i] = -1;
        reversal[i] = -1;
    }
}

void tinfo::calcT()
{
    int i,j;

    matrix r(depth,depth);
    for(i=0; i<depth; i++) {
        for(j=0; j<depth; j++) {
            if(i != j) r.elt(i,j) = fract(0);
            else if(reversal[i]) r.elt(i,j) = fract(-1);
            else r.elt(i,j) = fract(1);
        }
    }

    if(tdebug['T']) {
        printf("reversal matrix:\n");
        r.print(stdout);
    }

    matrix p(depth,depth);
    for(i=0; i<depth; i++) {
        for(j=0; j<depth; j++) {
            if(i == permutation[j]) p.elt(i,j) = fract(1);
            else p.elt(i,j) = fract(0);
        }
    }

    if(tdebug['T']) {
        printf("permutation matrix:\n");
        p.print(stdout);
    }

    T = new matrix(depth,depth);
    if(skew.do_not_skew) {
        *T = p*r;
    } else {
        matrix s(depth,depth);
        for(i=0; i<depth; i++) {
            for(j=0; j<depth; j++) {
                if(j<i) s.elt(i,j) = fract(skew.sk[i][j]);
                else if(j==i) s.elt(i,j) = fract(1);
                else s.elt(i,j) = fract(0);
            }
        }
        if(tdebug['T']) {
            printf("skew matrix:\n");
            s.print(stdout);
        }
        *T = s*p*r;
    }

    if(tdebug['T']) {
        printf("transformation matrix:\n");
        T->print(stdout);
    }
}

tinfo::~tinfo()
{
    if(T) delete T;
}

int tinfo::is_null_transform()
{
    int i;
    for(i=0; i<depth; i++)
        if(permutation[i] != i || reversal[i])
            return 0;
    if(tile.regions != depth)
        return 0;
    return 1;
}


//-------------------- end of local transformation info ---------

// Express the resultant inner loop after transformation in terms
// of iteration space vector spaces.  This routine allocates a
// vector space that needs to be deleted.

vector_space *inner_loop_vector_space(int bits,int depth)
{
    vector_space *inner = new vector_space(depth);
    fract_vector w(depth);
    int i, j;

    for(i=0; i<depth; i++) {
        if(bits&(1<<i)) {// an innermost loop
            for(j=0; j<depth; j++)
                w[j] = (j == i);
            inner->insert(w);
        }
    }
    
    if(tdebug['v']) {
        printf("vector space inner loop <1>\n");
        inner->print(stdout);
    }
    return inner;
}

vector_space *inner_loop_vector_space(tinfo *t)
{
    int outers = t->tile.first[t->tile.regions-1];
    int i, j;
    fract_vector w(t->depth);
        
    vector_space *inner = new vector_space(t->depth);
        
    if(outers > 0) {
        matrix tmp(outers,t->depth);
        for(i=0; i<outers; i++) {
            for(j=0; j<t->depth; j++) {
                tmp.elt(i,j) = t->T->elt(j,i);
            }
        }
        if(tdebug['v']) {
            printf("outer loops:\n");
            tmp.print(stdout);
        }
        tmp.factor();
        *inner = tmp.kernel();
    } else {
        assert(outers == 0);
        for(i=0; i<t->depth; i++) {
            for(j=0; j<t->depth; j++) {
                fract tt = fract(0);
                if(i == j) tt = fract(1);
                w[j] = tt;
            }
	    
            inner->insert(w);
        }
    }
    
    if(tdebug['v']) {
        printf("vector space inner loop <2>\n");
        inner->print(stdout);
    }

    return inner;
}

// find_cost only finds locality cost: doesn't factor in parallelism

static void find_cost(nest_info *n,tinfo *t,double *cost,int *trip,vector_space *inner)
{
    int unknown_trip_size=50;

    if(tdebug['v']) {
        printf("entering find_cost()\n");
        n->print(stdout);
        t->print(stdout);
    }
    assert(t->tile.regions>0);
    assert(n->depth == t->depth);
    
    double locality_cost = 0.0;
    *trip = unknown_trip_size;

    if(n->rlocs || n->plocs) {
        // compute locality in this nest (cost) and block size.
        
        cost_and_trip_size rval(n->rlocs,n->plocs,inner);
        
        locality_cost = rval.cost;
        *trip = rval.blocksize;
    }

    *cost = locality_cost;

    if(tdebug['C'])
        printf("COST %g, TRIP %d\n",*cost,*trip);
}

static int dmin(distance &d)
{
	if(d.is_const())
		return d.dist();
	else {
		direction x = d.dir();
		if(x == d_lt) return 1;
		else if(x == d_le) return 0;
		else assert(0);
	}

        // bogus return, should never reach here
	return 0;
}

/*
 * must be cautious about "short" dependence vectors arising
 * from imperfectly nested loops.  Because of the way we order
 * the passes, skewing comes LAST.
 */

/*
 * This routine calculates a skew that makes the nest fully
 * permutable.  If the nest is already fully permutable (common
 * case) no skew (the identity skew) will be returned.  Otherwise,
 * if some skew is legal to create a fully permutable nest, it
 * will be guaranteed to find such a skew.  It might be possible
 * that this routine will skew more than necessary, which is not
 * great, but also not common or harmful enough to be worth figuring
 * out how to do better, unless data indicate otherwise.
 */

static void calcskew(DVlist *d,int depth,int *deps,tinfo *ti,int region)
{
    int i, dd, l, ii, j, jj;
    int *ddp;

    if(depth < 2) return;
    
    skew_info *si = &ti->skew;
    int firstrow = ti->tile.first[region];
    int lastrow = ti->tile.first[region+1]-1;
    int *lp = &ti->permutation[0];
    
    // step one: find amount to skew each loop by.  Each loop is skewed
    // by zero times an inner, one times itself, and some number times
    // an outer.  For those loops that are reversed, it's easiest to
    // reverse their dependences immediately.
    
    for(i=firstrow; i<=lastrow; i++) {
        for(j=0; j<=i; j++)
            si->sk[i][j] = i==j;
    }
    
    if(tdebug['S']) {
        printf("dependences of interest: ");
        for(dd=0; deps[dd] != MAXVECS; dd++)
            d->dv[deps[dd]]->print(stdout);
        printf("\nwith loops: (");
        for(l=firstrow; l <= lastrow; l++)
            printf(" %d",lp[l]);
        printf(")\n");
        fflush(stdout);
    }
    
    for(i=firstrow; i<=lastrow; i++) {
        int l = lp[i];
        int rv = ti->reversal[l];
        if(rv) {
            for(ddp = deps; *ddp != MAXVECS; ddp++)
                if(d->dv[*ddp]->len > l)
                    d->dv[*ddp]->d[l].negate();
        }
        
        // Using this method, suboptimal skews very possible.
        // In order to not do too hideous a job, we go through
        // the list of dependences first to find where we have
        // no choice.  For example, if there is a dependence
        // (1,0,<=,-3), then we must skew the last by at least
        // 3x the first.
        
        // dbad is a list of vectors that cannot be fixed so easily
        
        int factors[MAXDEPTH];
        for(ii=firstrow; ii<i; ii++)
            factors[ii] = 0;
        
        DVlist_e *dbad[MAXDEPTH],**db=dbad;
        
        for(ddp = deps; *ddp != MAXVECS; ddp++) {
            DVlist_e *de = d->dv[*ddp];

            if(de->len <= l)
                continue;
            int the_dist = dmin(de->d[l]);
            if(the_dist >= 0)
                continue;
            
            // if none of the previous components are pos, error.
            // If one is, mandatory skew.
            // If two or more are, save for later.
            
            int u = -1,ud;
            
            for(j=firstrow; j<i; j++) {
                int dtmp = dmin(de->d[lp[j]]);
                assert(dtmp >= 0);
                if(dtmp > 0) {
                    if(u == -1) {u=j; ud=dtmp;}
                    else {*db++ = de; break;}
                }
            }
            assert(u != -1);
            if(i == j) {
                the_dist = (-the_dist + ud - 1)/ud;
                if(the_dist > factors[u])
                    factors[u] = the_dist;
            }
        }
        
        // Each vector on the dbad list has two or more choices,
        // but previous skewing may have fixed it.
        
        for(DVlist_e **dbb = dbad; dbb < db; dbb++) {
            
            // Just play around with your two or more choices.
            // It is possibly ok at this point.  If not, we'd
            // like to try the one that helps the most, but a
            // pain to program.  Make it work, so just use the
            // innermost loop's.

            int dist = dmin((*dbb)->d[l]);
            assert(dist < 0);
            int u = -1,ud;
            for(j=0; j<i; j++) {
                int dtmp = dmin((*dbb)->d[lp[j]]);
                if(dtmp > 0) {
                    u = j;
                    ud = dtmp;
                    dist += factors[u] * ud;
                }
            }
            assert(u>=0);
            if(dist < 0) {
                if(tdebug['S']>1)
                    printf("dist = %d, so being bogus\n",dist);
                factors[u] += (-dist+ud-1)/ud;
            }
        }

        // fix dependence vectors
        
        for(ddp = deps; *ddp != MAXVECS; ddp++) {
            DVlist_e *de = d->dv[*ddp];

            if(de->len <= l)
                continue;
            int is_dist = de->d[l].is_const();
            int tmp = dmin(de->d[l]);
            for(ii=firstrow; ii<i; ii++) {
                if(is_dist) is_dist = de->d[lp[ii]].is_const();
                tmp += factors[lp[ii]] * dmin(de->d[lp[ii]]);
            }
            if(is_dist)		de->d[l].set_distance(tmp);
            else if(tmp == 0)	de->d[l].set_direction(d_le);
            else if(tmp > 0)	de->d[l].set_direction(d_lt);
            else			assert(0);
        }
        
        // fix skew chart.  Note the factors are only part of
        // the story, since each factor may represent a skewed loop itself
        
        for(ii=firstrow; ii<i; ii++) {
            for(jj=firstrow; jj<=ii; jj++) {
                si->sk[i][jj] += factors[ii];
            }
        }
    }
    
    if(tdebug['S'])
        printf("skewing matrix (for this subportion):\n");
    int do_skew = 0;
    for(i=firstrow; i<=lastrow; i++) {
        for(j=firstrow; j<=i; j++) {
            if(si->sk[i][j] != (i == j))
                do_skew++;
            if(tdebug['S'])
                printf("\t%d",si->sk[i][j]);
        }
        if(tdebug['S'])
            printf("\n");
    }
    si->do_not_skew = (do_skew == 0);
}

/*
 * Select outer:
 *    Find a legal loop to place next outermost.
 *    To do this, it may be necessary to perform a reversal first.
 *
 * This routine returns the loop number of a legal outermost loop,
 *    or loopnum-MAXDEPTH if the loop is to be for reversed.
 * It removes the returned loop from loop_choices.
 *
 * Dependence inputs:
 *   deps: all dependences not satisfied by outer nests not including this one.
 *   newdeps: dependences not satisfied by the outer loops of this nest.
 *   (Never updates 'deps' but does update 'newdeps'.)
 */

static int select_outer(nest_info *ni,int *loops,int *deps,int *newdeps)
{
    // check ascending order, for my piece of mind
    int i, l;

    if(deps[0] != MAXVECS) {
        for(i = 1; deps[i] != MAXVECS; i++)
            assert(deps[i-1] < deps[i]);
    }
    if(newdeps[0] != MAXVECS) {
        for(i = 1; newdeps[i] != MAXVECS; i++)
            assert(newdeps[i-1] < newdeps[i]);
    }
    if(loops[0] != MAXDEPTH) {
        for(i = 1; loops[i] != MAXDEPTH; i++)
            assert(loops[i-1] < loops[i]);
    }
    
    DVlist *d = ni->deps;
    
    for(l = 0; loops[l] != MAXDEPTH; l++) {
        int loop = loops[l];
        int all_ge = 1, all_le = 1;
        int *ndp = newdeps;

        // If the dependence is on deps and on newdeps, then 
        // it is not carried yet in the sequential execution of
        // the tile; skewing won't help and it must be all positives
        // or all negatives.  If the dependence not on newdeps,
        // then if it is a constant it can be skewed as well.

        for(i=0; deps[i] != MAXVECS; i++) {
            int dp = deps[i];

            if(d->dv[dp]->len <= loop) {       /* not a factor */
                if(*ndp == dp) ndp++;
                continue;
            }

            distance dd = d->dv[dp]->d[loop];

            if(*ndp == dp) {
                ndp++;
            } else {
                assert(*ndp > dp);
                if(dd.is_const())
                    continue;
            }

            switch (dd.dir()) {
            case d_eq: break;
            case d_le: case d_lt: all_ge = 0; break;
            case d_ge: case d_gt: all_le = 0; break;
            case d_lg: case d_star: all_le = all_ge = 0; break;
            default: assert(0);
            }
            if (!all_le && !all_ge)
                break;
        }

        if (all_le || all_ge) {
            // fix newdeps
            int reversed = !all_le;
            int *ndeps = newdeps;
            for(ndp = newdeps; *ndp != MAXVECS; ndp++) {
                if(d->dv[*ndp]->len <= loop)        /* not a factor */
                    continue;
                distance dd = d->dv[*ndp]->d[loop];
                if (dd.dir() != (reversed ? d_gt : d_lt))
                    *ndeps++ = *ndp;
            }
            *ndeps = MAXVECS;

            // fix loops
            assert(loop == loops[l]);
            for(; loops[l+1] != MAXDEPTH; l++)
                loops[l] = loops[l+1];
            loops[l] = MAXDEPTH;
            return reversed ? loop - MAXDEPTH : loop;
        }
    }
    return MAXDEPTH; 		//failure
}

/*
 * permute_reverse_skew: much excitement here.  Anyway, we first
 * just reverse and permute, and don't bother with calculating
 * the skew (prs()).  Then we calculate the skew at the very end, once we
 * have been successful.
 */

static int prs(nest_info *ni,tinfo *ti,int *deps,int firstp,int *firstp_loops,int lastp,int *lastp_loops,DVlist *dcopy)
{
    int i, p;

    if(tdebug['O']>2) {
        printf("entering prs with:\n");
        printf("vecs: ");
        for(i=0; deps[i] != MAXVECS; i++)
            printf("%d ",deps[i]);
        printf("\nfirstp = %d: ",firstp);
        for(i=0; firstp_loops[i] != MAXDEPTH; i++)
            printf("%d ",firstp_loops[i]);
        printf("\nlastp = %d: ",lastp);
        for(i=0; lastp_loops[i] != MAXDEPTH; i++)
            printf("%d ",lastp_loops[i]);
        printf("\n");
    }

    int successfully_placed = 0;

    int newdeps[MAXVECS+1];
    for(i=0; (newdeps[i] = deps[i]) != MAXVECS; i++);

    for(p = firstp; p<=lastp; p++) {
        /* consider loops from this perfect nest only. */

        int loop_choices[MAXDEPTH+1];
        if(p == firstp || p == lastp) {
            int *l = ((p == firstp) ? firstp_loops : lastp_loops);
            for(i=0; l[i] != MAXDEPTH; i++) 
                loop_choices[i] = l[i];
            loop_choices[i] = MAXDEPTH;
        } else {
            for(i=ni->outerperfect[p]; i<ni->outerperfect[p+1]; i++)
                loop_choices[i-ni->outerperfect[p]] = i;
            loop_choices[i-ni->outerperfect[p]] = MAXDEPTH;
        }

        while(loop_choices[0] != MAXDEPTH) {
            int choice = select_outer(ni,loop_choices,deps,newdeps);
            if(choice != MAXDEPTH) {
                int rv = (choice <0);
                if(rv) choice += MAXDEPTH;
                if(tdebug['O']>1)
                    printf("choice = %d %s\n",choice,rv?"reversed":"");
                ti->reversal[choice] = rv;
                ti->permutation[ti->tile.first[ti->tile.regions]+successfully_placed] = choice;
                successfully_placed++;
            } else if(successfully_placed == 0) {
                /* couldn't place any in this nest */
                return 0;
            } else {
                /* didn't place them all -- time for a recursive call */
                int r = ti->tile.regions++;
                assert(r+1 < MAXDEPTH);
                ti->tile.first[r+1] = ti->tile.first[r] + successfully_placed;
                int ok = prs(ni,ti,newdeps,p,loop_choices,lastp,lastp_loops,dcopy);
                if(ok) calcskew(dcopy,ni->depth,deps,ti,r);
                for(i=0; (deps[i] = newdeps[i]) != MAXVECS; i++);
                return ok;
            }
        }
    }

    /* we only reach here if we placed all loops there are to place */

    if(successfully_placed) {   /* otherwise, we were called with no work */
        int r = ti->tile.regions++;
        ti->tile.first[r+1] = ti->tile.first[r] + successfully_placed;
        calcskew(dcopy,ni->depth,deps,ti,r);
        for(i=0; (deps[i] = newdeps[i]) != MAXVECS; i++);
    }
    return 1;
}

static int permute_reverse_skew(nest_info *ni,tinfo *ti,int *deps,
                                int firstp,int *firstp_loops,
                                int lastp,int *lastp_loops)
{
    DVlist *dcopy = new DVlist(ni->deps);
    int i;

    if(tdebug['O']>1) {
        printf("entering permute_reverse_skew with:\n");
        printf("vecs: ");
        for(i=0; deps[i] != MAXVECS; i++)
            printf("%d ",deps[i]);
        printf("\nfirstp = %d: ",firstp);
        for(i=0; firstp_loops[i] != MAXDEPTH; i++)
            printf("%d ",firstp_loops[i]);
        printf("\nlastp = %d: ",lastp);
        for(i=0; lastp_loops[i] != MAXDEPTH; i++)
            printf("%d ",lastp_loops[i]);
        printf("\n");
    }

    ti->tile.nontrivial_tile = 1;
    ti->tile.regions = 0;
    ti->tile.first[0] = 0;
    int ok = prs(ni,ti,deps,firstp,firstp_loops,lastp,lastp_loops,dcopy);
    if(tdebug['O']>1)
        printf("permute_reverse_skew returns %s\n",ok?"ok":"not ok");

    delete dcopy;
    return ok;
}

/*
 * examine order has two parts.  First, it should find a legal ordering,
 * hopefully but not necessarily with fully permutable loops, of all
 * the zero-bits in val.  Then it should find a tile for all the 1 bits.
 * It returns whether it could do those things.  If so, how it is done
 * is in tinfo.
 *
 * The first part is accompiled via the standard permute-reverse-skew
 * transformation.  The second is done exactly the same way, but only
 * one tilable nest is allowed.
 */

static int examine_order(nest_info *ni,tinfo *ti,int pnest,int bits)
{
    int oloops[MAXDEPTH+1],*ol=oloops;  /* list of the outer loops  */
    int iloops[MAXDEPTH+1],*il=iloops;  /* list of the inner loops */
    int i, j;
    
    for(i=0; i<ni->outerperfect[pnest+1]-ni->outerperfect[pnest]; i++) {
        if((bits&(1<<i)) == 0) *ol++ = ni->outerperfect[pnest] + i;
        else *il++ = ni->outerperfect[pnest] + i;
    }
    *ol = MAXDEPTH;
    *il = MAXDEPTH;
    
    int t1depth = (ol - oloops) + ni->outerperfect[pnest];
    int t2depth = (il - iloops) + ni->depth - ni->outerperfect[pnest+1];
    
    assert(t1depth+t2depth == ni->depth);
    
    ti->depth = t1depth;
    
    int deps[MAXVECS+1];
    for(i=0; i<ni->deps->count; i++)
        deps[i] = i;
    deps[i] = MAXVECS;
    
    // order/parallelize outer
    
    int ok;
    if(oloops[0] == MAXDEPTH && pnest == 0) {
        ok = 1; /* nothing to do here */
        assert(ti->tile.regions == 0);
        assert(t1depth == 0);
    } else if(pnest == 0) {
        ok = permute_reverse_skew(ni,ti,deps,0,oloops,0,oloops);
    } else {
        int l1[MAXDEPTH+1];
        assert(ni->outerperfect[0] == 0);
        for(i=0; i<ni->outerperfect[1]; i++)
            l1[i] = i;
        l1[i] = MAXDEPTH;
        ok=permute_reverse_skew(ni,ti,deps,0,l1,pnest,oloops);
	// don't tile outer region unless perfectly nested
	if (pnest > 1) ti->tile.no_tiling = TRUE; 
    }
    if(!ok) {
        if(tdebug['O'])
            printf("failed to order outer part\n");
        return 0;
    }
    
    if(tdebug['O']>1) {
        printf("Outer transformation:\n");
        ti->print(stdout);
    }
    
    if(iloops[0] != MAXDEPTH) {
        
        // block inner
        tinfo tloc;
        tloc.depth = t2depth;
        
        if(pnest == ni->outerpcount-1) {
            ok = permute_reverse_skew(ni,&tloc,deps,pnest,iloops,pnest,iloops);
        } else {
            int l2[MAXDEPTH+1];
            int mxp = ni->outerpcount-1;
            assert(ni->outerperfect[mxp+1] == ni->depth);
            for(i=0; i<ni->depth-ni->outerperfect[mxp]; i++)
                l2[i] = i+ni->outerperfect[mxp];
            l2[i] = MAXDEPTH;
            ok = permute_reverse_skew(ni,&tloc,deps,pnest,iloops,mxp,l2);
        }
        
        int rgs = tloc.tile.regions;
        assert(rgs > 0);
        if(rgs > 1) {
            if(tdebug['O']>1)
                printf("Inner block legal, but %d (>1) regions.\n",rgs);
            return 0;
        }
        if(ok) {
            if(tdebug['O']>1) {
                printf("Inner transformation:\n");
                tloc.print(stdout);
            }
            
            // combine the transformations
            for(i=0; i<tloc.depth; i++) {
                int lpi = tloc.permutation[i];
                ti->reversal[lpi] = tloc.reversal[lpi];
                ti->permutation[ti->depth+i] = lpi;
            }
            for(i=ti->depth; i<tloc.depth+ti->depth; i++) {
                for(j=0; j<ti->depth; j++)
                    ti->skew.sk[i][j] = 0;
                for( ; j<ti->depth+tloc.depth; j++)
                    ti->skew.sk[i][j] = tloc.skew.sk[i-ti->depth][j-ti->depth];
            }
            assert(ti->tile.first[ti->tile.regions] == ti->depth);
            ti->depth += tloc.depth;
            ti->tile.first[++ti->tile.regions] = ti->depth;
        }
    }
    if(ok) {
        ti->skew.do_not_skew = 1;
        for(i=0; i<ti->depth; i++) {
            for(j=0; j<=i; j++) {
                if(ti->skew.sk[i][j] != (i==j)) {
                    ti->skew.do_not_skew = 0;
                    break;
                }
            }
        }
        ti->calcT();
    }
    return ok;
}

int y_or_n(char *message)
{
    char buf[80];
    while(1) {
        printf("%s (y/n) ",message);
        fgets(buf, 80, stdin);
        if(buf[0] == 'y' || buf[0] == 'Y')
            return 1;
        if(buf[0] == 'n' || buf[0] == 'N')
            return 0;
    }
}

int get_number_from_input()
{
    char buf[80];
    gets(buf);
    while(buf[0] == '\0' || !(buf[0]>='0' && buf[0]<='9')) {
        printf("enter number: ");
        gets(buf);
    }
    return atoi(buf);
}

/*
 * Manually enter a transformation to apply (for debugging)
 */

static int enter_transformation(nest_info *ni,tinfo *ti)
{
    char buf[80];
    int i, j;

    strcpy(buf,"Enter transformation for:");
    for(i=0; i<ni->depth; i++) {
        strcat(buf," ");
        strcat(buf,ni->forlist[i]->index()->name());
    }

    int yes = y_or_n(buf);

    if(!yes) {
        yes = y_or_n("Use default mechanism?");
        if(yes) return 0;
        printf("I don't know what you want.  Try again.\n");
        return enter_transformation(ni,ti);
    }

    yes = y_or_n("Null transformation?");
    if(yes) {
        ti->tile.nontrivial_tile = 1;
        ti->depth = ni->depth;
        ti->skew.do_not_skew = 1;
        for(i=0; i<ti->depth; i++) {
            ti->permutation[i] = i;
            ti->reversal[i] = 0;
            for(j=0; j<ti->depth; j++)
                ti->skew.sk[i][j] = i==j;
        }
        ti->calcT();
        return 1;
    }
 
    ti->depth = ni->depth;

    printf("reversal (enter 0 to not reverse, 1 to reverse):\n");
    for(i=0; i<ni->depth; i++) {
        printf("%d: ",i);
        ti->reversal[i] = get_number_from_input();
    }

    printf("permutation:\n");
    for(i=0; i<ni->depth; i++) {
        printf("%d: ",i);
        ti->permutation[i] = get_number_from_input();
    }

    printf("skewing:\n");
    ti->skew.do_not_skew = !y_or_n("do you want to skew?");
    for(i=0; i<ni->depth; i++) {
        for(j=0; j<i; j++) { 
            if(ti->skew.do_not_skew) {
                ti->skew.sk[i][j] = 0;
            } else {
                printf("%d,%d: ",i,j);
                ti->skew.sk[i][j] = get_number_from_input();
            }
        }
        ti->skew.sk[i][i] = 1;
    }

    printf("tiling: ");
    yes = y_or_n("do you want to tile?");
    if(!yes) {
        ti->tile.nontrivial_tile = 0;
    } else {
        ti->tile.nontrivial_tile = 1;
        printf("regions: ");
        ti->tile.regions = get_number_from_input();
        for(i=0; i<ti->depth; i++) {
            printf("trip[i]: "); ti->tile.trip[i] = get_number_from_input();
        }
        for(i=0; i<ti->tile.regions; i++) {
            printf("first[i]: "); ti->tile.first[i] = get_number_from_input();
        }
        for(i=0; i<ti->tile.regions; i++) {
            printf("coalesce[i]: "); ti->tile.coalesce[i] = get_number_from_input();
        }
        ti->tile.first[i] = ti->depth;
    }

    ti->calcT();

    while(1) {
        ti->print(stdout);
        if(y_or_n("Good enough? "))
            return 1;
        if(y_or_n("Use normal machanism? "))
            return 0;
        if(y_or_n("Try again? "))
            return enter_transformation(ni,ti);
        printf("I don't know what you want.  Let's do this again.\n");
    }
}

/*
 * Evaluate the advantages of having the given vector space be innermost
 * for register purposes, given the uniformly generated sets of innermost
 * array references.  [PLDI'91] gives only the slightest hints about
 * how to do this, and [CaCK'90] is not much better.  But it's clear
 * how it sould be done: temporal reuse is all that counts for registers.
 * Self-temporal allows a single register to hold a value throughout the
 * loop, resulting in no loads or stores within the loop.  In the group
 * case, e.g.
 *   DO i
 *    a(i) ... a(i+1)
 * it is clear that by playing register swap games we can save the second
 * load (i.e. also a savings of 1), but at a higher register cost.
 * Someone (not me) should figure out how to do this right --- I'll just
 * implement a hack that should work 99% of the time.
 *
 * The equivalence classes are straight out of [PLDI'91]
 */

static double reg_eval(vector_space &vs,set_of_ugset *sou)
{
    int d;

    double savings = 0.0;
    set_of_ugset_iter soui(sou);
    while(!soui.is_empty()) {
        ugset &set = soui.step()->set;
        if((vs * *set.self_temporal).dimensionality()) {
            // if ST, the savings is everything
            ugset_iter ui(&set);
            while(!ui.is_empty()) {
                ugset_e *u = ui.step();
                savings += (u->reads>0) + (u->writes>0);
            }
        } else {
            // if GT, there's some savings.
            // We need to build the equivalence classes, etc.  Here we go.
            int savtmp = 0;
            fract_vector_list *vl = vs.basis();
            assert(vl->count() == 1);
            fract_vector *inner = vl->pop();
            ugset_iter ui(&set);
            ugset_e *classes[100],**c;
            classes[0] = 0;
            int rr[100],ww[100];
            while(!ui.is_empty()) {
                ugset_e *u = ui.step();
                // is this in the same class as any of the others seen so far
                for(c=classes; *c; c++) {
                    // solve Ax=w
                    array_info_iter ai1((*c)->ai),ai2(u->ai);
                    fract_vector w(MAXCOLS);
                    for(d=0; !ai1.is_empty() && !ai2.is_empty(); d++)
                        w[d] = fract(ai1.step()->con - ai2.step()->con);
                    assert(ai1.is_empty() && ai2.is_empty());
                    int ok;
                    fract_vector xp = set.Af->particular_solution(w, &ok);
                    if(ok && xp.proportional(*inner)) {
                        for(d=0; d < xp.n(); d++) {
                            // quickly: if fractional, doesnt count
                            if(xp[d].denom() != 1) ok = 0;
                        }
                        if(ok) break;
                    }
                         
                }
                if(*c == 0) {
                    rr[c-classes] = 0;
                    ww[c-classes] = 0;
                    *c = u;
                    *(c+1) = 0;
                }
                assert(u->reads>=0 && u->writes>=0);
                savtmp += ((u->reads>0) + (u->writes>0));
                rr[c-classes] += u->reads;
                ww[c-classes] += u->writes;
            }
            for(c=classes; *c; c++) { // still have to do ONE ld/st
                savtmp -= ((rr[c-classes]>0) + (ww[c-classes]>0));
            }
            assert(savtmp >= 0);
            savings += savtmp*0.7; //harder to make good use of GT
                                   //0.7 is a hack to do the right
                                   //thing without writing another thesis.
        }
    }
    return savings;
}

/*
 * Examine innermost loops.  Which one is best to have innermost?
 * The best one is the one with the most temporal locality.
 * Actually, you need to be able to register allocate the temporal
 * locality as well, but I'll let that pass -- this is an heuristic.
 * (counts double if there is both a read and a write.)
 */

static void choose_best_innermost_loop(nest_info *ni,Tinfo *ft)
{
    tree_for *most[MAXDEPTH],**mp = most; //candidates for innermost loop
    int i, j, k;

    // the loop must be in the innermost perfect nest
    for(i = ni->depth-1; i>=0; i--) {
        tree_for *p = i>0 ? ni->forlist[i-1] : 0;
        tree_for *f = ni->forlist[i];
        *mp++ = (tree_for *)f;
        if(i > 0 && (next_perfect_inner(p) != f)) break;
    }
    *mp = 0;

    // it also must be in the innermost region
    int rgno = ft->tile.regions-1;
    int loop1 = ft->tile.first[rgno];
    int ok = 0;
    for(i=loop1; i<ni->depth; i++) {
        for(j=0; most[j]; j++)
            if(ni->forlist[i] == most[j])
                break;
        if(most[j]) {tree_for *x = most[j]; most[j]=most[ok]; most[ok++] = x;}
    }
    most[ok] = 0;

    assert(ok>0);
    if(ok == 1) {
        assert(most[0] == ni->forlist[ni->depth-1]);
        return;
    }

    // look for temporal locality.  This is tricky.  The direction of a
    // loop is space spanned by the outer loops including it minus the
    // spaced spannded by the outer loops not including it.

    tree_for *bestloop = 0;
    double bestsavings = -222.2;
    fract_vector *d[MAXDEPTH];
    vector_space all(ni->depth);

    matrix U(ni->depth,ni->depth);
    for(i=0; i<ni->depth; i++) {
        for(j=0; j<ni->depth; j++)
            U.elt(i,j) = ft->U[i][j];
    }
    matrix Uinverse = U.inverse();
    for(i=0; i<ni->depth; i++) {
        fract_vector v(ni->depth);
        for(j=0; j<ni->depth; j++)
            v[j] = Uinverse.elt(i,j);
        d[i] = new fract_vector(v);
        all.insert(*(d[i]));
    }
    all.reduce_magnitude();
    all.beautify();
    for(mp = most; *mp; mp++) {
        if(tdebug['i']) {printf("evaluating .. "); fflush(stdout);}
        vector_space vs2(ni->depth);
        for(j=0; j<ni->depth; j++) {
            if(*mp != ni->forlist[j])
                vs2.insert(*(d[j]));
        }
        vector_space vs(all - vs2);
        assert(vs.dimensionality() == 1);
        if(tdebug['i']) {vs.print(stdout); fflush(stdout);}

        double savr = ni->rlocs?reg_eval(vs,ni->rlocs):0.0;
        double savp = ni->plocs?reg_eval(vs,ni->plocs):0.0;
        double savings = savr + savp;
        if(tdebug['i']) {printf("register savings = %g\n",savings);}

        // there's also the cost of small loop bounds, which is
        // heuristically means the big bounds should be innermost,
        // but in practice, over 20 is probably fine, esp. with tiling.
        // cost = max(0,20/iters-1)

        if(!ft->nontrivial_U) { // bounds get ugly otherwise
            bexpr *lb = (bexpr *) (*mp)->peek_annote(k_lbexpr);
            bexpr *ub = (bexpr *) (*mp)->peek_annote(k_ubexpr);
            assert(lb && ub);
            int mn,mnok,mx,mxok;
            lb->bx->range(&mn,&mnok,&mx,&mxok);
            if(tdebug['i']>1) printf("lb-> %d %d %d %d\n",mn,mnok,mx,mxok);
            int umn,umnok,umx,umxok;
            ub->bx->range(&umn,&umnok,&umx,&umxok);
            if(tdebug['i']) printf("ub-> %d %d %d %d\n",umn,umnok,umx,umxok);
            if(mxok && umnok) {
                int iters = umn - mx + 1;
                if(tdebug['i']>1) printf("iterations: maybe %d\n",iters);
                if(iters < 20) savings -= (20.0/iters - 1);
            }
        }

        if(savings >= bestsavings &&
           !(savings == bestsavings && bestloop == ni->forlist[ni->depth-1])) {
            bestsavings = savings;
            bestloop = *mp;
        }
    }
    assert(bestloop);
    int besti;
    for(i=0; i<ni->depth; i++)
        if(bestloop == ni->forlist[i]) 
            {besti = i; break;}
    assert(besti < ni->depth);

    if(besti != ni->depth-1) {
        // perform permutation for register locality
        if(tdebug['i'] || tdebug['t']) {
            const char *n1 = ni->forlist[ni->depth-1]->index()->name();
            const char *n2 = ni->forlist[besti]->index()->name();
            printf("interchanged for register locality! (\"%s\"->\"%s\")\n",
                   n1,n2);
            fflush(stdout);
        }
        ft->nontrivial_U = 1;
        if(ft->nontrivial_U == 0) {
            // make U the identity unimodular transformation
            for(j=0; j<ni->depth; j++) {
                for(k=0; k < ni->depth-1; k++)
                    ft->U[k][j] = (i == j);
            }
        }
        // shift rather than swap:
        // heuristic: programmer probably had good cache loop innermost.
        // so keep it 2nd innermost anyway.  Can't hurt.
        for(j=0; j<ni->depth; j++) {
            int tmp = ft->U[besti][j];
            for(k=besti; k < ni->depth-1; k++)
                ft->U[k][j] = ft->U[k+1][j];
            ft->U[ni->depth-1][j] = tmp;
        }
    }
}

/*
 * We mark which loops are doalls, and do loop interchange within
 * perfectly nested loops to make them outermost.
 */

static void handle_doalls(DVlist *deps,Tinfo *ft)
{
    // step one: transform dependences
    int i, j, k, dd;
    distance d[MAXDEPTH];

    for(i=0; i<deps->count; i++) {
        DVlist_e *dd = deps->dv[i];
        for(j=0; j<dd->len; j++) {
            int dist = 0;
            direction dir = direction(0);
            for(k=0; k<ft->depth; k++) {
                int factor = ft->U[j][k];
                if(factor == 0)
                    continue;
                if(dd->d[k].is_const()) {
                    dist += factor * dd->d[k].dist();
                } else {
                    direction dddir = dd->d[k].dir();
                    if(factor < 0) {
                        // swap first and third bits
                        dddir = (dddir == d_lt) ? d_gt :
                                (dddir == d_gt) ? d_lt :
                                (dddir == d_le) ? d_ge :
                                (dddir == d_ge) ? d_le : dddir;
                    }
                    dir = direction(int(dddir) | int(dir));
                }
            }
            if(int(dir) == 0)
                d[j].set_distance(dist);
            else if(dist == 0 || dir == d_star)
                d[j].set_direction(dir);
            else switch(dir) {
              case d_lt:
                if(dist>0) d[j].set_direction(dir);
                else if(dist == -1) d[j].set_direction(d_le);
                else d[j].set_direction(d_star);
                break;
              case d_le:
                if(dist>0) d[j].set_direction(d_lt);
                else d[j].set_direction(d_star);
                break;
              case d_gt:
                if(dist<0) d[j].set_direction(dir);
                else if(dist == 1) d[j].set_direction(d_ge);
                else d[j].set_direction(d_star);
                break;
              case d_ge:
                if(dist<0) d[j].set_direction(d_gt);
                else d[j].set_direction(d_star);
                break;
              case d_lg:
                d[j].set_direction(d_star);
                break;
              default:
                assert(0);
            }
        }
        for(j=0; j<dd->len; j++)
            dd->d[j] = d[j];
    }

    // step two: mark doall loops

    int dsat[MAXVECS];
    for(i=0; i<deps->count; i++)
        dsat[i] = 0;

    for(i=0; i<ft->depth; i++) {
	ft->doall[i] = 1;
	for(dd=0; dd<deps->count; dd++) {
	    if(dsat[dd]) continue;
	    if(deps->dv[dd]->len <= i) {dsat[dd] = 1; continue;}
	    direction dir = deps->dv[dd]->d[i].dir();

	    if(dir == d_eq) ;
	    else if(dir == d_lt) {dsat[dd] = 1; ft->doall[i] = 0;}
	    else if(dir == d_le) {ft->doall[i] = 0;}
	    else {
		char buf[256];
		deps->dv[dd]->print(stderr);
		sprintf(buf,"(d=%d,i=%d) Transformed vector ",dd,i);
		sprintf(buf," lexicographically negative, bye bye");
		error_line(1, ft->loops[i], buf);
	    }
	}
    }

    // step three: pull out doall loops within tilable nests, except
    // the innermost tilable nest, of course

    for(i=0; i<ft->tile.regions - 1; i++) {
        int newfirst = ft->tile.first[i];
        for(j=newfirst; j<ft->tile.first[i+1]; j++) {
            if(j>newfirst&&next_perfect_inner(ft->loops[j-1])!=ft->loops[j])
                newfirst = j;
            if(ft->doall[j]) {
                if(j != newfirst) {
                    ft->doall[newfirst] = 1;
                    ft->doall[j] = 0;
                    for(k=0; k<ft->depth; k++) {
                        int t = ft->U[j][k];
                        ft->U[j][k] = ft->U[newfirst][k];
                        ft->U[newfirst][k] = t;
                    }
                }
                newfirst++;
            }
        }
    }

    // step four: fix tile info.  Don't mess up innermost tile.

    tile_info ti;
    ti.nontrivial_tile = 1;
    ti.regions = 0;
    ti.first[0] = 0;

    int irgno = -1;
    for(i=0; i<ft->depth; i++) {
        ti.trip[i] = ft->tile.trip[i];
        // is this the beginning of a new region
        if(ft->tile.first[irgno+1]==i) irgno++;
        if(ft->tile.first[irgno]==i ||
           (irgno < ft->tile.regions-1 && (ft->doall[i-1] || ft->doall[i]))) {
            ti.coalesce[ti.regions] = ft->tile.coalesce[irgno];
            ti.first[ti.regions++] = i;
        }
    }
    ti.first[ti.regions] = i;

    ft->tile = ti;

}


static void handle_regions(nest_info *ni)
{
    // Put in begin_ and end_parallel_region annotations.  If loop i
    // has all dependences d_i = 0, then that loop should be moved outer
    // to maximize outermost parallelism.  Rather than actually moving loop
    // outermost just make the parallel region the entire fully permutable 
    // nest if it can be determined that each always gets the same
    // iterations (i.e. loop bounds are rectangular).  
    // This is equivalent to moving the doall outermost, making it
    // into a parallel region, and then having uniprocessor locality
    // analysis move it back to the current position.
    // If there are no loops with deps d_i = 0 then the outermost doall 
    // is the parallel region (which pgen does automatically).
    // Note: this current implementation does not perform unimodular 
    // xforms for outermost parallelism. 
    // Note: this optimization assumes a static partitioning of loop 
    // iterations.

    boolean all_zero;
    boolean all_rectangular;
    int curr_perfect = 0;
    int i, j, k, d;

    for (i = 0; i < ni->fpcount; i++) {
	all_rectangular = TRUE;
	tree_for *first_loop = ni->forlist[ni->fp[i]];

	//
	// Which perfect nest?
	//
	int old_perfect = curr_perfect;
       	for (k = old_perfect; k < ni->outerpcount; k++) {
       	    if (ni->outerperfect[k+1] > i) break;
	    else curr_perfect = k+1;
	}

	for (j = ni->fp[i]; j < ni->fp[i+1]; j++) {
	    tree_for *curr_loop = ni->forlist[j];

	    //
	    // Check if bounds are rectangular
	    //
	    dep_for_annote *dfa = 
		(dep_for_annote *) curr_loop->peek_annote(k_dep_for_annote);
	    assert(dfa);

	    // Lower bound
	    array_info_iter aii(dfa->lb);
	    while (!aii.is_empty() && all_rectangular) {
		access_vector *av = aii.step();
		if (av->too_messy || 
		   (av->mod_this && is_nested(first_loop, av->mod_this))) {
		    all_rectangular = FALSE;
		    break;
		}

		for (k = i; (k < j) && all_rectangular; k++) {
		    if (av->val(ni->forlist[k])) {
			all_rectangular = FALSE;
			break;
		    }
		}
	    }
	    // Upper bound
	    aii.reset(dfa->ub);
	    while (!aii.is_empty() && all_rectangular) {
		access_vector *av = aii.step();
		if (av->too_messy || 
		   (av->mod_this && is_nested(first_loop, av->mod_this))) {
		    all_rectangular = FALSE;
		    break;
		}

		for (k = i; (k < j) && all_rectangular; k++) {
		    if (av->val(ni->forlist[k])) {
			all_rectangular = FALSE;
			break;
		    }
		}
	    }
	    if (!all_rectangular) break;  

	    all_zero = TRUE;
	    for(d = 0; d < ni->deps->count; d++) {
		if ((ni->deps->dv[d]->len <= j) || 
		    (!ni->deps->dv[d]->d[j].is_zero())) {

		    all_zero = FALSE;
		    break;  // go on to next loop
		}
	    }
	    
	    if (all_zero) {
		if (ni->outerperfect[curr_perfect+1] > j) {
		    first_loop->append_annote(k_doall_region,new immed_list());
		}
		return;
	    }
	}
    }
}


static void handle_doacross(nest_info *ni, Tinfo *ft)
{
    // If dependences are distances then annotate doacross loops
    //
    int i, j, d;

    for (i = 0; i < ni->fpcount; i++) {

	boolean distances_only = TRUE;

	for (j = ni->fp[i]; (j < ni->fp[i+1]) && distances_only; j++) {
	    for(d = 0; d < ni->deps->count; d++) {
		if ((ni->deps->dv[d]->len <= j) && 
		    (!ni->deps->dv[d]->d[j].is_const())) {

		    distances_only = FALSE;
		    break;  // go on to next loop
		}
	    }
	}

	if (distances_only) {
	    for (j = ni->fp[i]; j < ni->fp[i+1]; j++) {
		if (!ft->doall[j]) {
	    	    ni->forlist[j]->append_annote(k_doacross,new immed_list());
		}
	    }
	}
    }
}


//
// Determine if scalar expansion is necessary.  Since we don't handle
// scalar-expansion, don't tile the loop, for now.
//
static void figure_out_scalar_expansion(nest_info *ni, Tinfo *ft)
{
    if (ft->tile.no_tiling) return;

    int depth = ft->depth;
    int ii = depth;
    int jj = depth;
    int p, i, k;

    // ii is the outermost loop that has been transformed by unimodular transf.
    //
    if(ft->nontrivial_U) {
        for(ii = 0; ii<depth; ii++) {
            for(jj = 0; jj<depth; jj++)
                if(ft->U[ii][jj] != ii==jj)
                    goto byebye;
        }
    }
  byebye:
    tile_info *tin = &ft->tile;

    // jj is the outermost loop (+1) that has been transformed by tiling
    //
    if(tin->nontrivial_tile) {
        for(jj = 0; jj<tin->regions; jj++) {
            if(tin->first[jj] != jj)
                break;
        }
    }

    // set ii to be the outermost loop that has been transfomred.
    //
    if(jj-1<ii) ii = jj-1;

    int curr_perfect_nest = ni->outerpcount;
    for (p = 1; p < ni->outerpcount; p++) {
	if (ni->outerperfect[p] > ii) { 
	    curr_perfect_nest = p;
	    break;
        }
    }
    if (curr_perfect_nest == ni->outerpcount) return;  // perfect nested.

    // Look at all non-perfectly nested regions within ii.
    //
    for(i=ii; i < depth-1; i++) {
	it_for_annote *an =
	    (it_for_annote *) ft->loops[i]->peek_annote(k_it_for);
	assert(an);

	if (!an->privatizable->is_empty()) {
	    sym_node_list_iter snli(an->privatizable);
	    while (snli.is_empty()) {
		sym_node *sn = snli.step();
		boolean is_index_var = FALSE;
		
		for (k = ii; k < depth; k++) {
		    if (ft->loops[k]->index() == sn) {
			is_index_var = TRUE;
			break;
		    }
		}


		if (!is_index_var) {
		    ft->tile.no_tiling = TRUE;
		    return;
		}
	    }
	}
    }
}


// Create "privatized" annotations for scalars that require privatization
//
static void figure_out_scalar_privatization(nest_info *ni)
{
    // Privatize all privatizable variables within the nest:
    //
    int depth = ni->depth;
    immed_list **privatized = new immed_list*[depth];
    int i, j;

    for(i=depth-1; i>=0; i--) {
	privatized[i] = new immed_list();

	tree_for *curr_for = ni->forlist[i];
	base_symtab *curr_scope = curr_for->scope();
	curr_for->get_annote(k_privatized); 

	it_for_annote *an = (it_for_annote *) curr_for->peek_annote(k_it_for);
	assert(an);
        sym_node_list_iter ri(an->privatizable);


        while(!ri.is_empty()) {
	    sym_node *sn = ri.step();
	    assert(sn->is_var());

	    if (curr_scope->is_ancestor(sn->parent())) {
		// Check if on any list already:
		boolean add_scalar = TRUE;

		for (j = i; j < depth; j++) {
		    assert(privatized[j]);
		    if (privatized[j]->lookup(sn)) {  // immed comparison looks
			add_scalar = FALSE;           // at symbols
		        break;
		    }		      
                }
	        if (add_scalar) privatized[i]->append(immed(sn));
	    }
        }
	if (!privatized[i]->is_empty()) {
	    curr_for->append_annote(k_privatized, privatized[i]);
        }
    } // for

    delete privatized;
}

static void handle_reductions(nest_info *ni, Tinfo *ft)
{
    int i, d;

    // Check if reduction deps cause this loop to be a dependence
    //
    ni->find_reduction_deps();

    reduction_list_iter rli(ni->reductions);
    while (!rli.is_empty()) {
	reduction_info *ri = rli.step();
	DVlist *deps = ri->reduction_deps;

	int dsat[MAXVECS];
	for(i=0; i<deps->count; i++)
	    dsat[i] = 0;

	ri->no_reduction_doall = new boolean[ft->depth];
    
	for(i=0; i<ft->depth; i++) {
	    ri->no_reduction_doall[i] = TRUE;
	    for(d=0; d<deps->count; d++) {
		if(dsat[d]) continue;
		if(deps->dv[d]->len <= i) {dsat[d] = 1; continue;}
		direction dir = deps->dv[d]->d[i].dir();
		if(dir == d_eq) ;
		else if (dir == d_lt) 
		    { dsat[d] = 1; ri->no_reduction_doall[i] = FALSE; }
		else if( dir == d_le) 
		    { ri->no_reduction_doall[i] = FALSE; }
		else {
		    char buf[256];
		    deps->dv[d]->print(stderr);
		    sprintf(buf,"(d=%d,i=%d) Reduction dependence ",d,i);
		    sprintf(buf," lexicographically negative, bye bye");
		    error_line(1, ft->loops[i], buf);
		}
	    }
	}
    }

    for (i = 0; i < ft->depth; i++) {
	if (!ft->doall[i]) continue; // Not parallel anyway; reductions useless

        it_for_annote *ifa = 
	    (it_for_annote *) ft->loops[i]->peek_annote(k_it_for);

	boolean need_scalars = !ifa->reduction_vars->is_empty();

	annote_list_iter ali(ft->loops[i]->annotes());
	while (!ali.is_empty()) {
	    annote *old_an = ali.step();

	    if (old_an->name() == k_reduction) {
		sym_node *sn = (*(old_an->immeds()))[1].symbol();
		assert(sn->is_var());
		var_sym *curr_var = (var_sym *) sn;

	        if (curr_var->is_scalar()) {
		    if (need_scalars) {
	 	        immed_list *il = old_an->immeds()->clone();
			ft->loops[i]->append_annote(k_reduced, il);
		    }
                }
		else {
		    reduction_info *ri = ni->reductions->lookup(curr_var);
		    if (ri && !ri->no_reduction_doall[i]) {
			immed_list *il = old_an->immeds()->clone();
			ft->loops[i]->append_annote(k_reduced, il);
		    }
                }
	    } else if (old_an->name() == k_reduction_gen) {
		immed_list *il = old_an->immeds()->clone();
		ft->loops[i]->append_annote(k_reduced_gen, il);
	    }
        }
    }
}


/*
 * The external procedure for this file.  Take a loop nest and
 * transform it!
 *
 * This is done by simply running through the different possible
 * combinations of blocked loops (given that loop interchange of
 * non-perfectly nested loops is not possible) and evaluating each
 * unique blocked loop, first for legality, then for quality of
 * code (cost).  This search is much faster than other compilers
 * because of the realization that all blocking styles that include
 * the same set of loops are equivalent for the cache.  Further
 * search speedup is realized by noting that if a given transformation
 * does not exploit locality, it cannot be better than the identity
 * transformation, which is tried first.
 */

void find_and_do_transform(nest_info *ni)
{
    int i, j, ii, jj, d, p;

    // Two simple cases: non-innermost trying to improve locality, and ...
    if(!ni->rlocs && !ni->plocs && parallelism_level==0)
        return;
    // ... either a doall or not
    if(ni->depth == 1) {
        int is_doall = 1;
        for(d=0; d<ni->deps->count; d++) {
            if(ni->deps->dv[d]->d[0].dir() & (d_lt|d_gt)) {
                is_doall = 0; break;
            }
        }
        if(tdebug['t']) {
            printf("depth 1 loop %s(",ni->forlist[0]->index()->name());
            ni->forlist[0]->index()->print(stdout);
            printf(") is%s a DOALL\n",is_doall?"":" not");
        }
        if(!is_doall) return;

	figure_out_scalar_privatization(ni);

	Tinfo temp_info;
	temp_info.depth = 1;
	temp_info.loops[0] = ni->forlist[0];
        temp_info.doall[0] = TRUE;

        loop_transform tr(1, temp_info.loops, temp_info.doall);
        tr.annotate_doalls();
	handle_reductions(ni, &temp_info);

	if (is_loop_small(ni->forlist[0])) {
	    ni->forlist[0]->get_annote(k_doall);
	    ni->forlist[0]->get_annote(k_doacross);
	}
	if (are_bounds_small(ni->forlist[0])) {
   	    ni->forlist[0]->get_annote(k_doall);
   	    ni->forlist[0]->get_annote(k_doacross);
	    ni->forlist[0]->append_annote(k_small_bound, new immed_list());
        }

        return;
    }

    tinfo *tibest = new tinfo, *titmp = new tinfo;
    tibest->tile.innermost = titmp->tile.innermost = ni->rlocs || ni->plocs;
    double bestcost;
    int besttrip = 20;  // task granularity

    int manual_trans = tdebug['G'] && enter_transformation(ni,tibest);
    if(!manual_trans) {
        // transform ignoring locality
        int ok = examine_order(ni,tibest,ni->outerpcount-1,0);
        assert(ok);
        if(tdebug['O']>1) {
            printf("Final transformation of parallel attempt:\n");
            tibest->print(stdout);
        }
    }

    // Fully permutable annotes
    //
    ni->fpcount = tibest->tile.regions;
    ni->fp[ni->fpcount] = ni->depth;
    for(i=0; i < tibest->tile.regions; i++) {
        int outer = tibest->tile.first[i]; 
        int inner = tibest->tile.first[i+1] - 1;

	ni->fp[i] = outer;
	ni->forlist[outer]->append_annote(k_begin_fully_permutable,
					  new immed_list());
	ni->forlist[inner]->append_annote(k_end_fully_permutable,
					  new immed_list());
    }

    if(!manual_trans && ni->depth > 1 && (ni->rlocs || ni->plocs)) {

        // at this stage, we have a default, which is the transformation
        // that is best for parallelism but ignores locality.  Thus we
        // will go ahead and evaluate various transformations that have
        // better locality, i.e. alternate innermost loops.

        // but first, see if we do have some parallelism.  Since this
        // approach is best for small-scale multiprocessors, the
        // levels of parallelism we are interested in are "some" or "none".

        boolean adequate_parallelism = (parallelism_level == 0);
        if (!adequate_parallelism) {
            tree_for *f = ni->forlist[0];
            while ((f = (tree_for *)next_further_out((tree_node *)f,TREE_FOR)))
		{
                annote *an = f->annotes()->peek_annote(k_doall);
                if (!an) an = f->annotes()->peek_annote(k_tiled);
                if (an) {
		    adequate_parallelism = TRUE;
		    break;
		}
            }
        }

	if (!no_locality)  {
	    // locality for this ordering.

	    vector_space *inner = inner_loop_vector_space(tibest);
	    find_cost(ni,tibest,&bestcost,&besttrip,inner);
	    vector_space best_locality(ni->depth); 
	    vector_space total_locality(ni->depth);

	    set_of_ugset_iter iter(ni->rlocs);
	    while(!iter.is_empty()) 
	      total_locality += *iter.step()->set.locality;
	    iter.reset(ni->plocs);
	    while(!iter.is_empty()) 
	      total_locality += *iter.step()->set.locality;

	    best_locality = total_locality * *inner;
	    delete inner;
	    boolean any_locality = FALSE;

	    for(p=0; p<ni->outerpcount; p++) {
	      int nloops = ni->outerperfect[p+1] - ni->outerperfect[p];

            // we just run through the power set of potential inner loops.
            // bit 0 is set if the outermost loop of the innermost perfect
            // nest is to be placed innermost.  bit (nloops - 1) is set
            // if the innermost loop is to be innermost.  Etc.

              for(unsigned bits = (1<<nloops)-1; bits > 0; bits--) {
                if(tdebug['t'] || tdebug['O']) {
                    if(tdebug['O'])
                        printf("--------------------------------------\n");
                    printf("... evaluating [");
                    for(i=0; i<ni->outerperfect[p]; i++)
                        printf("%s ",ni->forlist[i]->index()->name());
                    for(i=0; i<nloops; i++)
                        if(!((1<<i)&bits))
                            printf("%s ",ni->forlist[ni->outerperfect[p]+i]->index()->name());
                    printf("][");
                    for(i=0; i<nloops; i++)
                        if((1<<i)&bits)
                            printf("%s ",ni->forlist[ni->outerperfect[p]+i]->index()->name());
                    for(i=ni->outerperfect[p+1]; i<ni->depth;i++)
                        printf("%s ",ni->forlist[i]->index()->name());
                    printf("] ... ");
                }

                // if there's no locality, don't bother
                vector_space *inner = inner_loop_vector_space(bits<<(ni->depth-nloops),ni->depth);
                int found_locality = 0;

                set_of_ugset_iter si(ni->rlocs);
                while(!si.is_empty() && !found_locality) {
                    vector_space loc = *si.step()->set.locality * *inner;
                    if(loc.dimensionality() != 0) found_locality = 1;
                }

                si.reset(ni->plocs);
                while(!si.is_empty() && !found_locality) {
                    vector_space loc = *si.step()->set.locality * *inner;
                    if(loc.dimensionality() != 0) found_locality = 1;
                }

                int ok = found_locality && examine_order(ni,titmp,p,bits);

                if(ok) {
                    if(tdebug['O']>1) {
                        printf("Final transformation:\n");
                        titmp->print(stdout);
                    }

		    any_locality = TRUE;
                    double costtmp;
                    int triptmp;
                    find_cost(ni,titmp,&costtmp,&triptmp,inner);

                    if(tdebug['O'] || tdebug['t'])
                        printf("blksz=%d cost=%g\n",triptmp,costtmp);
                    if(costtmp < bestcost) {
                        bestcost = costtmp;
                        besttrip = triptmp;
                        tinfo *txx = titmp;titmp = tibest,tibest = txx;

			// Find best locality
			best_locality = total_locality * *inner;
                    }
                } else {
                    if(tdebug['t'] || tdebug['O']) {
                        if(found_locality) printf(" failed to order\n");
                        else printf(" no locality: didn't explore\n");
                    }
                }
                delete inner;
	      }
	    }

	    // Don't bother to tile if there's no locality at all
	    //
	    if (!any_locality) tibest->tile.no_tiling = TRUE;

	    if (best_locality.dimensionality() > 0) {
	      best_locality.beautify();
	      immed_list *iml = best_locality.get_matrix()->cvt_immed_list();
	      ni->forlist[0]->append_annote(k_locality, iml);
	    }
	  }
    }

    // gross hack
    if(parallelism_level>1)
        besttrip >>= 1;
    for(i=0; i<ni->depth; i++)
        tibest->tile.trip[i] = besttrip;

    if(tdebug['t']) {
        printf("BEST NEST FOUND: indices [");
        int firsti = tibest->tile.first[tibest->tile.regions-1];
        for(i=0; i<firsti; i++)
            printf("%s ",ni->forlist[tibest->permutation[i]]->index()->name());
        printf("][");
        for( ; i<ni->depth; i++)
            printf("%s ",ni->forlist[tibest->permutation[i]]->index()->name());
        printf("]\n");
    }

    int tiled = (!no_locality && (tibest->tile.regions != tibest->depth) && 
                !tibest->tile.no_tiling);
    int permuted = 0;
    int reversed = 0;
    int skewed = !tibest->skew.do_not_skew;
    for(i = 0; i < tibest->depth; i++) {
        if(tibest->permutation[i] != i) permuted=1;
        if(tibest->reversal[i]) reversed=1;
    }

    it_stats *s = (it_stats *) 
	ni->forlist[0]->proc()->block()->peek_annote(k_it_stats);
    assert(s);
    s->tiled += tiled;
    s->permute_not_tiled += (permuted && !tiled);
    s->reversed += reversed;
    s->skewed += skewed;

    // Check for small bounds in tile.  If any, don't tile that region
    // 
    if (!tibest->tile.no_tiling) {
	for (i = 0; i < tibest->depth; i++) {
	    if (are_bounds_small(ni->forlist[i])) {
		tibest->tile.no_tiling = TRUE;
		break;
	    }
	}
    }

    Tinfo ft;
    ft.depth = tibest->depth;
    ft.nontrivial_U = 0;
    ft.tile = tibest->tile;
    for(ii=0; ii<ft.depth; ii++) {
        ft.loops[ii] = ni->forlist[ii];
        ft.doall[ii] = 0;
        ft.tile.coalesce[ii] = coalesce; // a region, not a loop; no biggie
        for(jj=0; jj<ft.depth; jj++) {
            assert(tibest->T->elt(ii,jj).denom() == 1);
            ft.U[ii][jj] = tibest->T->elt(ii,jj).num();
            if(ft.U[ii][jj] != (ii == jj)) ft.nontrivial_U = 1;
        }
    }

    if (innermost_for_registers)
        choose_best_innermost_loop(ni,&ft);
    if (parallelism_level > 0) {
        handle_doalls(ni->deps,&ft);
	if (!no_doall_regions)  handle_regions(ni);
	if (!no_doacross)  handle_doacross(ni,&ft);
    }

    ft.tile.no_tiling = do_not_tile || tibest->tile.no_tiling || no_locality;
    if(tdebug['t']) ft.print(stdout);

    figure_out_scalar_expansion(ni, &ft); 
    figure_out_scalar_privatization(ni);

    int depth = ft.depth;
    integer_matrix U(depth, depth);

    for(ii=0; ii<depth; ii++) {
        for(jj=0; jj<depth; jj++) {
            U[ii][jj] = ft.U[ii][jj];
        }
    }

    // If the tile_size is specified on the command-line overwrite the
    // tile size found by the analysis
    //
    if (tile_size > 0) {
	for (i = 0; i < ft.depth; i++)
	    ft.tile.trip[i] = tile_size;
    }

    loop_transform trans(ft.depth, ft.loops, ft.doall);
    boolean trans_ok = trans.unimodular_transform(U);
    assert_msg(trans_ok, 
      ("Unimodular transformation failed for loop %s at line #%d", 
       ft.loops[0]->index()->name(), source_line_num(ft.loops[0])));

    if (!ft.tile.no_tiling) {
	if (parallelism_level <= 0) {
	    trans_ok = trans.tile_transform(ft.tile.trip, ft.tile.regions, 
					    ft.tile.coalesce, ft.tile.first);
	    assert_msg(trans_ok, 
                ("Tile transformation failed for loop %s at line #%d", 
	        ft.loops[0]->index()->name(), source_line_num(ft.loops[0])));
	}
	else {
	    // Put in tiling annotation so pgen will tile the loops 
	    // 
	    // ["tilable_loops": depth trip_size..trip_size] (tree_fors only)
	    //
	    for (i = 0; i < ft.tile.regions; i++) {
		immed_list *iml = new immed_list();
		int curr_first = ft.tile.first[i];
		int curr_last = ft.tile.first[i+1] - 1;
		iml->append(immed(curr_last - curr_first + 1));
		if (ft.tile.coalesce[i]) iml->append(immed(1));
		else iml->append(ft.tile.trip[curr_first]);

		for (j = curr_first+1; j <= curr_last; j++) {
		    iml->append(ft.tile.trip[j]);
		}

		trans.get_loop(curr_first)->append_annote(k_tileable_loops,
							  iml);
	    }
	}
    }

    // These must done after transform b/c they add annotations to the loops
    // Transform may change the order of the loops.
    //
    fill_in_access(ni->forlist[0]->proc()->block());
    handle_reductions(ni, &ft);

    for(i=0; i<ni->depth; i++) {
	if (is_loop_small(ni->forlist[i])) {
	    ni->forlist[i]->get_annote(k_doall);
	    ni->forlist[i]->get_annote(k_doacross);
	}
	if (are_bounds_small(ni->forlist[i])) {
   	    ni->forlist[i]->get_annote(k_doall);
	    ni->forlist[i]->get_annote(k_doacross);
	    ni->forlist[i]->append_annote(k_small_bound, new immed_list());
        }
    }

    delete tibest;
    delete titmp;
}


static boolean is_loop_small(tree_for *tf)
{
    double time_estimate = rough_time_estimate(tf);

    if (time_estimate < small_loop_cutoff) return TRUE;
    else return FALSE;
}

static boolean are_bounds_small(tree_for *tf)
{
    operand lb = tf->lb_op();
    operand ub = tf->ub_op();
    operand step = tf->step_op();

    if (!lb.is_instr() || !ub.is_instr() || !step.is_instr()) return FALSE;

    fold_constants(lb.instr());
    fold_constants(ub.instr());
    fold_constants(step.instr());

    lb = tf->lb_op();
    ub = tf->ub_op();
    step = tf->step_op();

    int lb_int, ub_int, step_int;
    boolean lb_is_int, ub_is_int, step_is_int;
    lb_is_int = operand_is_int_const(lb, &lb_int);
    ub_is_int = operand_is_int_const(ub, &ub_int);
    step_is_int = operand_is_int_const(step, &step_int);

    if (!step_is_int || (step_int == 0)) return FALSE;

    if (lb.is_expr())
      {
	instruction *lb_ins = lb.instr();
	if ((lb_ins->opcode() == io_min) && (step_int < 0)) 
 	    lb_is_int = calc_upper_bound_on_min(lb_ins, &lb_int);

	else if ((lb_ins->opcode() == io_max) && (step_int > 0))
	    lb_is_int = calc_lower_bound_on_max(lb_ins, &lb_int);
      }

    if (ub.is_expr()) 
      {
	instruction *ub_ins = ub.instr();
	if ((ub_ins->opcode() == io_min) && (step_int > 0)) 
 	    ub_is_int = calc_upper_bound_on_min(ub_ins, &ub_int);

	else if ((ub_ins->opcode() == io_max) && (step_int < 0))
	    ub_is_int = calc_lower_bound_on_max(ub_ins, &ub_int);
      }
    
    if (lb_is_int && ub_is_int) 
      {
	if (ABS((ub_int - lb_int) / step_int) < small_bound_cutoff)
	    return TRUE;
      }

    return FALSE;
}

//
// Is inner nested within outer?  If they're the same return TRUE
//
static boolean is_nested(tree_for *outer, tree_for *inner)
{
    tree_node *curr_node = inner;
    while (curr_node && curr_node->parent() && curr_node != outer) {
	curr_node = curr_node->parent()->parent();
    }

    return (curr_node == outer);
}


static boolean calc_upper_bound_on_min(instruction *ins, int *val)
{
  if (ins->opcode() != io_min) return FALSE;
  in_rrr *min_instr = (in_rrr *) ins;

  operand src1 = min_instr->src1_op();
  operand src2 = min_instr->src2_op();

  boolean src1_is_int, src2_is_int;
  int src1_int, src2_int;
  src1_is_int = operand_is_int_const(src1, &src1_int);
  src2_is_int = operand_is_int_const(src2, &src2_int);

  if (src1_is_int && src2_is_int) 
    {
      *val = ((src1_int > src2_int) ? (src1_int) : (src2_int));
      return TRUE;
    }
  else if (src1_is_int) 
    {
      *val = src1_int;
      return TRUE;
    }
  else if (src2_is_int) 
    {
      *val = src2_int;
      return TRUE;
    }

  return FALSE;
}


static boolean calc_lower_bound_on_max(instruction *ins, int *val)
{
  if (ins->opcode() != io_max) return FALSE;
  in_rrr *min_instr = (in_rrr *) ins;

  operand src1 = min_instr->src1_op();
  operand src2 = min_instr->src2_op();

  boolean src1_is_int, src2_is_int;
  int src1_int, src2_int;
  src1_is_int = operand_is_int_const(src1, &src1_int);
  src2_is_int = operand_is_int_const(src2, &src2_int);

  if (src1_is_int && src2_is_int) 
    {
      *val = ((src1_int < src2_int) ? (src1_int) : (src2_int));
      return TRUE;
    }
  else if (src1_is_int) 
    {
      *val = src1_int;
      return TRUE;
    }
  else if (src2_is_int) 
    {
      *val = src2_int;
      return TRUE;
    }

  return FALSE;

}
