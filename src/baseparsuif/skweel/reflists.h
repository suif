/* file "reflists.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  List of array references */

#ifndef REFLISTS_H
#define REFLISTS_H

#pragma interface

RCS_HEADER(reflists_h, "$Id: reflists.h,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")


// Define lists of memory locations touched -- registers dealt with elsewhere 
// Convention: reads=0 && writes=0 means it's scalar expanded.

struct refs_e: public glist_e {
    int reads;          // how many reads this stands for
    int writes;         // how many writes this stands for
    sym_addr base;      // address touched
    in_array *iarr;     // can be null, meaning touches entire object
    instruction *memref;    // an instruction that gives rise to it
    type_node *instr_type;  // resulting type of instruction
    refs_e(sym_addr v, in_array *a, instruction *n, type_node *tn):
        glist_e(), reads(0), writes(0), base(v), iarr(a), memref(n),
        instr_type(tn) { }
    void print(FILE *f);
    ~refs_e() {}
    boolean operator ==(refs_e &r);   // memrefs in same loop body,
                                      // have same base and array info
};

/* a list of memory locations touched */

class refs: public glist {
    refs_e *push(sym_addr v, in_array *iarr,instruction *memref,type_node *tn)
        {return (refs_e *)glist::push(new refs_e(v,iarr,memref,tn));}
    refs_e *push(refs_e *e) {glist::push(e); return e;}
    refs_e *on(refs_e *r,int remove=0);
 public:
    refs_e *enter(sym_addr v, in_array *iarr, instruction *memref,
                  type_node *tn);
    refs_e *enter(refs_e *);
    refs_e *on(sym_addr v, in_array *iarr, instruction *memref, type_node *tn,
               int remove)
        {refs_e *e = new refs_e(v,iarr,memref,tn);
         refs_e *ok = on(e,remove);
         delete e;
         return ok;}
    int is_empty() {return glist::is_empty();}
    refs_e *pop() {return (refs_e *)glist::pop();}
    void transfer(refs *arg);         // move items from arg to this list
    void print(FILE *f);
    refs() {}
    ~refs();
    int count() {return glist::count();}  // elements on list
};

/* the iterator for refs */

struct refs_iter: public glist_iter {
    refs_iter(refs *r):glist_iter((glist *)r) {}
    int is_empty() {return glist_iter::is_empty();}
    refs_e *step() {return (refs_e *) glist_iter::step();}
};

/*
 * The references in a loop nest.  The references are sorted
 * into reads and writes, and into the category (e.g. a global,
 * a parameter, etc).  This categorization keeps allows the
 * analysis to quickly discard certain dependence possibilities.
 * For example, in FORTRAN it is never possible for static locals
 * and pseudo locals to touch the same location, so dependence
 * analysis need not be employed between items on each list.
 */

class reflists {
    void aps(tree_for **f,int depth,tree_node_list *l);
    void build_reflists(tree_node_list *);
 public:
    tree_for *tf;               // backpointer to for loop
    boolean everything_touched; // assume the worst (e.g. a call)
    refs rg_r,rg_w;		// indirect through a reg (read & write)
    refs p_r,p_w;		// indirect through a parameter (read & write)
    refs sl_r,sl_w;		// static local (read & write)
    refs pl_r,pl_w;		// pseudo local, ie spilled (read & write)
    refs g_r,g_w;		// globals (read & write)
    refs reduction_w;           // access involved in a reduction
    reflists(tree_for *);
    ~reflists() {}
    void append_scalars(tree_for **,int depth);
    void enter_memory_ref(instruction *si, operand op, boolean is_write);
    void enter_reduction_ref(instruction *si, operand op);
    void build_reductions(tree_node_list *);
    void enter_cal(instruction *);
    void print(FILE *f);
    void insert_ai_for_scalar(tree_for **,int,sym_node*,instruction *,int);
};
#endif
