/* file "vm_header.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef VM_HEADER_H
#define VM_HEADER_H

#ifdef VM_TYPE_DOUBLE
#define VM_E double
#define VM_E_PRINT(f,x) fprintf(f,"%5.2g",x)
#define VM_E_IS_ZERO(x) ((x) < 1.e-15 && (x) > -1.e-15)
#define VM_E_ABS(x) ((x)<0?-(x):(x))
#define VM_E_DOUBLE(x) (x)
#endif

#ifdef VM_TYPE_FRACT

#define VM_E fract
#define VM_E_PRINT(f,x) ((x).print(f))
#define VM_E_IS_ZERO(x) ((x).num()==0)
#define VM_E_ABS(x) ((x).abs())
#define VM_E_DOUBLE(x) ((x).real())
#define VM_E_EXACT
#endif

#ifndef VM_E
	vm_header.h needs to define VM_E
#endif

#ifdef CC2   /* 2.0 compiler */
#define FCONST const
#else
#define FCONST
#endif

#endif VM_HEADER_H
