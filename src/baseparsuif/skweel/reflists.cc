/* file "reflists.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  List of array references */

#pragma implementation "reflists.h"

#define RCS_BASE_FILE reflists_cc

#include "skweel.h"
#include "ast_it.h"
#include "reflists.h"
#include "params.h"

RCS_BASE("$Id: reflists.cc,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

boolean refs_e::operator ==(refs_e &r)
{
    if(memref->parent()->parent() != r.memref->parent()->parent() || 
       base != r.base) return FALSE;

    if (instr_type == NULL || r.instr_type == NULL) {
	if (instr_type != r.instr_type) return FALSE;
    }
    else {
	if (!instr_type->is_same(r.instr_type)) return FALSE;
    }
    
    if (iarr == NULL || r.iarr == NULL)
        return iarr == r.iarr;

    // If instructions are in the same list, then we can compare the
    // accesses to see if they're equal:
    //
    if (iarr->parent()->parent() != r.iarr->parent()->parent()) return FALSE;

    dep_instr_annote *my_dia = 
       (dep_instr_annote *) iarr->peek_annote(k_dep_instr_annote);
    assert(my_dia);

    dep_instr_annote *r_dia = 
       (dep_instr_annote *) r.iarr->peek_annote(k_dep_instr_annote);
    assert(r_dia);

    array_info_iter aii1(my_dia->ai);
    array_info_iter aii2(r_dia->ai);
    while(!aii1.is_empty() && !aii2.is_empty()) {
        access_vector *v1 = aii1.step();
        access_vector *v2 = aii2.step();
        if(v1->too_messy || v2->too_messy)
            return 0;
        av_compare_info ci(v1,v2);
        if(!ci.identical())
            return 0;
    }
    assert(aii1.is_empty() && aii2.is_empty());
    return TRUE;
}

refs_e *refs::on(refs_e *r, boolean remove)
{
    refs_iter ri(this);
    while(!ri.is_empty()) {
        refs_e *rr = ri.step();
        if (*rr == *r) {
            if (remove) {
                glist::remove(rr);
                delete rr;
            }
            return rr;
        }
    }
    return NULL;
}

refs_e *refs::enter(sym_addr v, in_array *iarr, instruction *memref,
		    type_node *tn)
{
    refs_e *e = new refs_e(v, iarr, memref, tn);
    refs_e *r = on(e, FALSE);
    if(r == NULL) {
        push(e);
        return e;
    }
    else {
        delete e;
        return r;
    }
}

refs_e *refs::enter(refs_e *e)
{
    refs_e *r = on(e, FALSE);
    if(r) {
        delete e;
        return r;
    } else {
        return push(e);
    }
}

void refs_e::print(FILE *f)
{
    base.symbol()->print(f);
    fprintf(f, "+%d ", base.offset());
    if (instr_type) instr_type->print(f);
    if (iarr) {
	dep_instr_annote *dia = 
	    (dep_instr_annote *) iarr->peek_annote(k_dep_instr_annote);
	assert(dia);
	dia->ai->print(f);
    }
    else fprintf(f, "[no array instr]");
}

void refs::print(FILE *f)
{
    int first = TRUE;

    putc('{',f);
    refs_iter ri(this);
    while(!ri.is_empty()) {
        refs_e *re = ri.step();

	if (first) first = FALSE;
	else putc(',',f);

	re->print(f);
    }
    putc('}',f);
    putc('\n',f);
}

void reflists::print(FILE *f)
{
    fprintf(f,"reflists: everything touched? %s\n",
            everything_touched ? "yes" : "no");
    
    if(!rg_r.is_empty()) {
        fprintf(f,"regs read only:\n\t");
        rg_r.print(f);
    }
    if(!rg_w.is_empty()) {
        fprintf(f,"regs written:\n\t");
        rg_w.print(f);
    }
    if(!p_r.is_empty()) {
        fprintf(f,"params read only:\n\t");
        p_r.print(f);
    }
    if(!p_w.is_empty()) {
        fprintf(f,"params written:\n\t");
        p_w.print(f);
    }
    if(!sl_r.is_empty()) {
        fprintf(f,"static locals read only:\n\t");
        sl_r.print(f);
    }
    if(!sl_w.is_empty()) {
        fprintf(f,"static locals written:\n\t");
        sl_w.print(f);
    }
    if(!pl_r.is_empty()) {
        fprintf(f,"dynamic locals read only:\n\t");
        pl_r.print(f);
    }
    if(!pl_w.is_empty()) {
        fprintf(f,"dynamic locals written:\n\t");
        pl_w.print(f);
    }
    if(!g_r.is_empty()) {
        fprintf(f,"globals read only:\n\t");
        g_r.print(f);
    }
    if(!g_w.is_empty()) {
        fprintf(f,"globals written:\n\t");
        g_w.print(f);
    }
    if(!reduction_w.is_empty()) {
        fprintf(f,"reduction vars written:\n\t");
        reduction_w.print(f);
    }
}

void refs::transfer(refs *l)
{
    grab_from(l);
}


/*
//---------------------------------------------------------

enum pb_knd {glob,loc_stat,loc_dyn };

static pb_knd what_kind(path_base *pb,ast_proc *p)
{
    assert(p);
    assert(pb);
    
    static ast_proc *old_proc = 0;
    static path_base *static_local = 0;
    static path_base *pseudo_local = 0;
    
    if(old_proc != p) {
        static_local = get_path_base(p->name->base,"static");
        pseudo_local = get_path_base(p->name->base,"pseudo");
        old_proc = p;
    }
    while(pb && pb != Root_pb) {
        if(pb == pseudo_local) return loc_dyn;
        if(pb == static_local) return loc_stat;
        if(pb == static_base) return glob;
        if(pb == global_base) return glob;
        pb = pb->prefix();
	}
	assert(0);
	return glob;	// shut up the compiler
}

static path_base *get_path_base(instruction *i)
{
    assert(i->opcode == io_ldc);
    
    icon ic = ((in_ldc *)i)->value;
    if(ic.discriminator != ic_path)
        return 0;
    annote *a = i->get_annote(k_path);
    if(a) {
        ic = a->pop();
        assert(ic.discriminator == ic_path);
        path_base *pb = ic.p()->base;
        a->push(ic);
        i->append(a);
        return pb;
    }
    return (ic.p()->offset != 0) ? 0 : ic.p()->base;
}
//---------------------------------------------------------
*/


void reflists::enter_memory_ref(instruction *si, operand op, boolean is_write)
{
    in_array *array_instr = NULL;
    type_node *tn = NULL;
    operand curr_r = op;

    while (curr_r.is_instr() && (curr_r.instr()->opcode() == io_cvt || 
				 curr_r.instr()->opcode() == io_cpy)) {
	in_rrr *rins = (in_rrr *) curr_r.instr();
	curr_r = rins->src1_op();
    }

    if (curr_r.is_instr() && curr_r.instr()->opcode() == io_array) {
	array_instr = (in_array *) curr_r.instr();
	tn = array_instr->result_type();
        curr_r = array_instr->base_op();
    }

    refs *read=0,*write=0;
    sym_node *base = NULL;
    unsigned offset = 0;

    // Try to find symbol of the array instruction
    //
    while (curr_r.is_instr() && curr_r.instr()->opcode() == io_cvt) { 
        in_rrr *rins = (in_rrr *) curr_r.instr();
        curr_r = rins->src1_op();
    }

    if (!curr_r.is_instr()) {
        assert(curr_r.is_symbol());
        if (curr_r.symbol()->is_var() && 
	    ((var_sym *) curr_r.symbol())->is_param()) {

            read = &p_r; write = &p_w;
        } 
        else {
            read = &rg_r; write = &rg_w;
        }
        base = curr_r.symbol();
    } 
    else {
        if (curr_r.instr()->opcode() == io_ldc) {
	    immed im = ((in_ldc *) curr_r.instr())->value();
	    assert(im.is_symbol());
	    sym_node *sn = im.symbol();
	    offset = im.offset();

	    if (sn->is_global()) 
		{ read = &g_r; write = &g_w; }
	    else if (sn->is_var() && ((var_sym * )sn)->is_auto()) 
		{ read = &pl_r; write = &pl_w; }
	    else 
		{ read = &sl_r; write = &sl_w; }
	    base = sn;

	} 
	else {
	    if(tdebug['R']) {
		printf("EVERYTHING ABOUT TO BE TOUCHED FROM ");
		curr_r.instr()->print(stdout);
		// printf(" ON LIST ");
		// curr_r.instr()->parent()->parent()->print(stdout,0);
	    }
	    everything_touched++;
	    return;
	}
    }

    // Check if the array is privatizable within the nest.  If so
    // ignore the memory reference.
    //
    it_for_annote *ifa = (it_for_annote *) tf->peek_annote(k_it_for);
    if (ifa && ifa->privatizable->lookup(base)) return;

    sym_addr sa(base, offset);
    if (is_write) {
        refs_e *w = write->enter(sa, array_instr, si, tn);
        w->writes++;

        refs_e *rf = read->on(sa, array_instr, si, tn, FALSE);
        if (rf) {
            w->reads += rf->reads;
            assert(rf->writes == 0);
            refs_e *rr = read->on(sa, array_instr, si, tn, TRUE);
            assert(rf == rr);
        }
    } 
    else {
        refs_e *w = write->on(sa, array_instr, si, tn, FALSE);
        if(w)
            w->reads++;
        else {
            refs_e *rf = read->enter(sa, array_instr, si, tn);
            rf->reads++;
        }
    }
}


void reflists::enter_reduction_ref(instruction *si, operand op)
{
    in_array *array_instr = NULL;
    type_node *tn = NULL;
    operand curr_r = op;

    while (curr_r.is_instr() && (curr_r.instr()->opcode() == io_cvt || 
				 curr_r.instr()->opcode() == io_cpy)) {
	in_rrr *rins = (in_rrr *) curr_r.instr();
	curr_r = rins->src1_op();
    }

    if (curr_r.is_instr() && curr_r.instr()->opcode() == io_array) {
	array_instr = (in_array *) curr_r.instr();
	tn = array_instr->result_type();
        curr_r = array_instr->base_op();
    }

    sym_node *base = NULL;
    unsigned offset = 0;

    while (curr_r.is_instr() && curr_r.instr()->opcode() == io_cvt) { 
        in_rrr *rins = (in_rrr *) curr_r.instr();
        curr_r = rins->src1_op();
    }

    if (!curr_r.is_instr()) {
        assert(curr_r.is_symbol());
        base = curr_r.symbol();
    } 
    else if (curr_r.instr()->opcode() == io_ldc) {
	immed im = ((in_ldc *) curr_r.instr())->value();
	assert(im.is_symbol());
	sym_node *sn = im.symbol();
	offset = im.offset();
	base = sn;
    } 
    else return;

    if (print_parallelism_info || tdebug['t']>1) {
        printf("Reference involved in reduction: %s", base->name());

	if (array_instr) {
	    dep_instr_annote *dia = 
	     (dep_instr_annote *) array_instr->peek_annote(k_dep_instr_annote);
	    assert(dia);
	    dia->ai->print(stdout);
	}
	printf("\n");
    }

    sym_addr sa(base,offset);
    reduction_w.enter(sa, array_instr, si, tn);
}


/*
 * The system already checks that we don't see any calls except well-behaved
 * intrinsics.  Thus enter_cal() does nothing.  A rough outline of what
 * we need to do once we have interprocedural analysis is included but
 * if'ed out.
 */

void reflists::enter_cal(instruction *si)
{
    int op = si->opcode();
    assert(op == io_cal);

#if 0
    // Experimental code below.
    //
    in_cal *ip = (in_cal *) si->instr;
    annote *a = si->instr->get_annote(k_intrinsic);
    assert(a);
    char *ainfo = a->pop().string();
    a->push(ainfo);
    si->instr->append(a);
    int i;
    
    for(i=0; i<ip->N; i++) {
        if(ainfo[i] == 'v')        // pass by value no problem
            continue;
        
        if_reg r = ip->args[i];
        ast_it_si *na = si->args()[i];
        
        if(na && na->instr->opcode == io_array) {
            r = ((in_array *)na->instr)->base;
            na = na->left();
        }
        
        if(na == 0) {
            refs *list;
            if(((ast_it_proc *)si->on_list->procedure)->pinfo->is_param(r))
                list = ainfo[i]=='r' ? &p_r : &p_w;
            else
                list = ainfo[i]=='r' ? &rg_r : &rg_w;
            list->enter((void *)(int)r,0,si);
        } else if(na->instr->opcode == io_ldc) {
            refs *list;
            path_base *pb = get_path_base(na->instr);
            switch(what_kind(pb,si->on_list->procedure)) {
            default: assert(0);
            case glob: list = ainfo[i]=='r' ? &g_r : &g_w; break;
            case loc_stat: list = ainfo[i]=='r' ? &sl_r : &sl_w; break;
            case loc_dyn: list = ainfo[i]=='r' ? &pl_r : &pl_w; break;
            }
            list->enter((void *)pb,0,si);
        } else {
            if(tdebug['R']) 
                printf("everything touched by a call\n");
            everything_touched = 1;
            return;
        }
    }
#endif
}

/*
 * Find loads, stores and calls in the loop nest.  Put them in the
 * reflists data structure.
 */

static void build_reflists_instr(instruction *i, void *x)
{
    reflists *rl = (reflists *) x;

    if (i->opcode() == io_lod) {
	in_rrr *rd_ins = (in_rrr *) i;
	rl->enter_memory_ref(i, rd_ins->src1_op(), FALSE);
    }
    else if (i->opcode() == io_str) {
	in_rrr *wr_ins = (in_rrr *) i;
	rl->enter_memory_ref(i, wr_ins->src1_op(), TRUE);
    }
    else if (i->opcode() == io_memcpy)  {
	in_rrr *rd_wr_ins = (in_rrr *) i;
	rl->enter_memory_ref(i, rd_wr_ins->src1_op(), TRUE);
	rl->enter_memory_ref(i, rd_wr_ins->src2_op(), FALSE);
    }

    else if (i->opcode() == io_cal)  rl->enter_cal(i);
}


static void build_reductions_instr(instruction *i, void *x)
{
    reflists *rl = (reflists *) x;

    if (!i->annotes()->peek_annote(k_reduction)) return;

    if (i->opcode() == io_lod) {
	// Nuke reduction annotes on reads -- there will always be a write
        // with the same access 
        i->annotes()->get_annote(k_reduction);
    }
    else if (i->opcode() == io_str) {
	in_rrr *wr_ins = (in_rrr *) i;
	rl->enter_reduction_ref(i, wr_ins->src1_op());
    }
    else if (i->opcode() == io_memcpy)  {
	in_rrr *rd_wr_ins = (in_rrr *) i;
	rl->enter_reduction_ref(i, rd_wr_ins->src1_op());
    }
}


static void build_reflists_tree(tree_node *tn, void *x)
{
    if (tn->kind() == TREE_INSTR) {
        tree_instr *ti = (tree_instr *) tn;
	ti->instr_map(build_reflists_instr, x);
    }
}


static void build_reductions_tree(tree_node *tn, void *x)
{
    if (tn->kind() == TREE_INSTR) {
        tree_instr *ti = (tree_instr *) tn;
	ti->instr_map(build_reductions_instr, x);
    }
}


void reflists::build_reflists(tree_node_list *l)
{
    l->map(build_reflists_tree, this);
}

void reflists::build_reductions(tree_node_list *l)
{
    l->map(build_reductions_tree, this);
}

/*
 * reflists::append_scalars() looks at the innermost loop.  If
 * scalar expansion may need to occur, include expanded scalar the
 * reflist, even though expansion may not occur.  This does not harm
 * dependence information or even locality information, since reuse
 * appears to occur only when there wouldn't be expansion anyway.
 * This trick allows expansion to occur in this data structure only,
 * without changing any actual SUIF code.  Then we can use the lists
 * as always and, once we've made our transformation decisions,
 * just expand as necessary.
 */
/*
void reflists::insert_ai_for_scalar(tree_for **f,int depth,sym_node*r,instruction *si,int)
{
    if(tdebug['R']) {
        r->print(stdout);
        printf(": creating access vector? ... ");
    }

    sym_node_list_e *e=0;
    int i, j;

    for(i=depth-1; i>=0; i--) {
        it_for_annote *an = (it_for_annote *)f[i]->peek_annote(k_it_for);
	assert(an);
        sym_node_list *snl_priv = an->privatizable;
        sym_node_list *snl_red = an->reduction_vars;
        assert(snl_red && snl_priv);
        if (e = snl_priv->lookup(r))  break;
	if (e = snl_red->lookup(r)) break;
    }
    if(e == 0) {
        if(tdebug['R'])
            printf("not private\n");
        return;
    }
    if(i == depth-1) {
	if(tdebug['R'])
	    printf("completely private, there will be no need to expand\n");
        return;
    }
    array_info *a = new array_info;
    for(j=0; j<=i; j++) {
        access_vector *av = new access_vector;
        av->add(f[j],1);
        a->append(av);
    }

    if(a) {
	refs_e *z;
	z=pl_w.enter(sym_addr(e->contents,0),a,si,si->result_type());
	z->writes++;
    }

    if(tdebug['R']) {
        a->print(stdout);
        putc('\n',stdout);
    }
}

struct aps_params {
    reflists *rl;
    tree_for **f;
    int depth;
};


static boolean is_index(tree_for **f, int depth, var_sym *var)
{
    int i;

    for (i = 0; i < depth; i++)
	if (f[i]->index() == var) return TRUE;

    return FALSE;
}


static void aps_instr(instruction *i, void *x)
{
    int j;
    aps_params *p = (aps_params *) x;

    for (j = 0; j < i->num_srcs(); j++) {
	operand r = i->src_op(j);

	if (r.is_symbol()) {
	    var_sym *var = r.symbol();

	    if (var->is_scalar() && !is_index(p->f, p->depth, var))  {
		p->rl->insert_ai_for_scalar(p->f, p->depth, var, i, 0);
	    }
	}
    }
}


static void aps_tree(tree_node *tn, void *x)
{
    if (tn->kind() == TREE_INSTR) {
        tree_instr *ti = (tree_instr *) tn;
        instruction *ins = ti->instr();

        operand r = ins->dst_op();

        if (r.is_symbol()) {
           var_sym *var = r.symbol();
           if (var->is_scalar())  {
	       aps_params *p = (aps_params *) x;
               p->rl->insert_ai_for_scalar(p->f, p->depth, var, ins, 1);
           }
        }

	ti->instr_map(aps_instr, x);
    }

    else if (tn->kind() == TREE_FOR || tn->kind() == TREE_LOOP)  assert(0);
}


void reflists::aps(tree_for **f,int depth,tree_node_list *l)
{
    aps_params *p = new aps_params;
    p->rl = this;
    p->f = f;
    p->depth = depth;
    l->map(aps_tree, p); 
}

void reflists::append_scalars(tree_for **f,int depth)
{
    aps(f,depth,f[depth-1]->body());
}
*/

reflists::reflists(tree_for *f)
{
    tf = f;
    everything_touched = 0;
    build_reflists(f->body());
}

refs::~refs()
{
	while(!is_empty())
		delete pop();
}







