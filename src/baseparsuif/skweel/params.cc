/* file "params.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Information about parameters */

#pragma implementation "params.h"

#define RCS_BASE_FILE params_cc

#include "skweel.h"
#include "params.h"

RCS_BASE("$Id: params.cc,v 1.2 1999/08/25 03:27:09 brm Exp $")

const char *k_param_usage;

boolean accesses(operand r, sym_node *var)
{
    if (r.is_symbol() && r.symbol() == var) return TRUE;
    return FALSE;
}

static void find_usage_instr(instruction *i, void *x)
{
    param_usage *info = (param_usage *) x;

    boolean found_read = FALSE;

    for (int j = 0; j < (int)i->num_srcs(); j++) {
	if (accesses(i->src_op(j), info->param)) { 
	    found_read = TRUE;
	    break;
	}
    }

    // Handle special case where symbol might be in immediate:

    if (i->format() == inf_ldc) {
	in_ldc *il = (in_ldc *) i;
	if (il->value().is_symbol() && il->value().symbol() == info->param) {
	    found_read = TRUE;
	}
    }

    if (found_read)  info->set_kind(USAGE_READ_ONLY);
}


static void find_usage_tree(tree_node *tn, void *x)
{
    param_usage *info = (param_usage *) x;

    if (info->kind == USAGE_WRITE) return;  // don't bother looking

    if (tn->kind() == TREE_INSTR) {
        tree_instr *ti = (tree_instr *) tn;
        instruction *ins = ti->instr();

        if (accesses(ins->dst_op(), info->param)) info->set_kind(USAGE_WRITE);
	else ti->instr_map(find_usage_instr, x);
    }

    else if (tn->kind() == TREE_FOR) {
        tree_for *tf = (tree_for *) tn;

        if (info->param == tf->index()) {
	    info->set_kind(USAGE_WRITE);         
	}
    }
}


void annotate_param_usage(tree_proc *tp)
{
    sym_node_list *param_list = tp->proc_syms()->params();
    sym_node_list_iter it(param_list);

    while (!it.is_empty()) {
        sym_node *param = it.step();

	// Find usage of parameters
	//
	param_usage *info = new param_usage(param);
        tp->body()->map(find_usage_tree, info);
	param->append_annote(k_param_usage, info);
    }
}


param_usage::param_usage(sym_node *p)
{
    kind = USAGE_NO_RW;
    param = p;
}


void param_usage::print(FILE *f)
{
    fprintf(f, ": %s *** ",
        kind == USAGE_NO_RW ? "--" :
        kind == USAGE_READ_ONLY ? "r-" :
        kind == USAGE_WRITE ? "rw" : "<error>");
    fprintf(f,"\n");
}






