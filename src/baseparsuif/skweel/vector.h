/* file "vector.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Vectors, vector spaces, etc. Used for unimodular and tiling 
    transformations.  Use in conjunction with VM_HEADER. */

#ifndef VECTOR_H
#define VECTOR_H

#pragma interface

RCS_HEADER(vector_h, "$Id: vector.h,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

class tmatrix;

class tvector {
	friend class tvector_list;
	int dim;
	VM_E *v;
	void copy(VM_E *);
public:
	int dimensionality() FCONST {return dim;}
	tvector(VM_E *vect,int d) : dim(d) {copy(vect);}
	tvector(int d);	// sets d components all to zero
	~tvector() { delete[] v;}
	tvector(tvector &w):dim(w.dim) {copy(w.v);}
	void operator =(tvector &w)
		{assert(w.dim==dim); 
		 delete[] v; 
		 copy(w.v);}
	void operator +=(tvector &);
	void operator -=(tvector &);
	VM_E dot(VM_E *) FCONST;
        VM_E operator [](int i) FCONST {assert(i<dim);return v[i];}
	friend tvector operator *(VM_E scalar,tvector &);
	friend tvector operator *(tmatrix &,tvector &);
	tvector operator -() FCONST {return VM_E(-1) * *this;}
	tvector operator +(tvector &vv) FCONST
            {tvector w = *this; w += vv; return w;}
	tvector operator -(tvector &vv) FCONST
            {tvector w = *this; w -= vv; return w;}
	int operator ==(tvector &v) FCONST;
        int proportional(tvector &v) FCONST;
	int operator !=(tvector &v) FCONST
            {return !(*this == v);}
                // proj of this onto line spanned by w
	tvector proj(VM_E *w) FCONST;
	VM_E *vect() FCONST {return v;}
	int is_zero() FCONST;
	void print(FILE *f=stdout);
        void reduce_magnitude(); // if only direction important
};

DECLARE_LIST_CLASSES(tvector_list_base, tvector_list_e, tvector_iter,
		     tvector*);

class tvector_list: public tvector_list_base {
    // consists of a set of independent basis vectors
private:
	int dim;

public:
	tvector_list(int d) { dim = d; }
	tvector_list(tvector_list &vl);
        ~tvector_list();
	void operator =(tvector_list &);
	int dimensions() { return dim; }
	void print(FILE *f=stdout);
        void reduce_magnitude(); // if only direction important
};

struct tvector_space {
	// the vector list is a list of independent vectors, or an LU decomp
	// of the vectors in a tmatrix, or both ... whatever's available.
	tvector_list *bv;
	tmatrix *lu;
	void make_bv_aux();
	void make_lu_aux();
	void make_bv() {if(bv==0) make_bv_aux();}
	void make_lu() {if(lu==0) make_lu_aux();}
public:
	tvector_space(int dim) {bv = new tvector_list(dim); lu=0;}
	tvector_space(tvector_space &vs);
        ~tvector_space();
	void operator =(tvector_space &vs);
	tvector_list basis() {make_bv(); return *bv;}
	tmatrix lu_matrix();
	void print(FILE *f=stdout);
	int in(VM_E *w);			// ret 1 if in
	int insert(VM_E *w);		// ret 1 if inserted
	int operator +=(tvector_space &);	// ret #vectors inserted
	void operator -=(tvector_space &);
	void operator *=(tvector_space &);
	tvector_space operator +(tvector_space &);
	tvector_space operator -(tvector_space &);
	tvector_space operator *(tvector_space &);
	tmatrix proj_matrix();
	int perpendicular(VM_E *);
	void beautify();
	int dimensionality() {make_bv(); return bv->count();}
        void reduce_magnitude();
};


#endif /* VECTOR_H */
