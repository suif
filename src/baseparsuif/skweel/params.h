/* file "params.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Information about parameters */

#ifndef PARAMS_H
#define PARAMS_H

#pragma interface

RCS_HEADER(params_h, "$Id: params.h,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")

enum procedure_usage { 
    USAGE_NO_RW, 
    USAGE_READ_ONLY,
    USAGE_WRITE
};

struct param_usage {
   procedure_usage kind;
   sym_node *param;              // Back pointer to parameter

   param_usage(sym_node *p);
   void set_kind(procedure_usage k)         { if (kind < k) kind = k; }
   boolean is_written()                     { return (kind == USAGE_WRITE); }
   void print(FILE *f);
};

//
// Gathers information about parameter usage within a function.  
// Adds annotes that says how the params are used.
//
void annotate_param_usage(tree_proc *tp);

extern const char *k_param_usage;

#endif /* PARAMS_H */
