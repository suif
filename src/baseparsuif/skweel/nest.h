/* file "nest.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Loop nest information */

#ifndef NEST_H
#define NEST_H

#pragma interface

RCS_HEADER(nest_h, "$Id: nest.h,v 1.1.1.1 1998/07/07 05:09:35 brm Exp $")


class DVlist;
class set_of_ugset;
class reflists;

/*
 * The nest_info data structure contains all we need to know about
 * a loop nest we wish to transform: the loops in the nest, which
 * loops are perfectly nested, the dependences in the nest, and
 * the array references in the innermost loop.
 *
 * The constructor takes a singly nested set of loops, inserts them
 * into its forlist, and fills in the other fields from there.
 */

/*
 * depth is the loop nest depth of this loop nest, at most MAXDEPTH.
 * forlist[0] is the outermost loop.
 * forlist[depth-1] is the innermost loop.
 * outerp[m] is an index into flist, pointing to the outermost loop of
 *  the mth perfect loop nest.  The innermost of that loop nest is
 *  outerp[m+1]-1.
 */

class reduction_info {
public:
    var_sym *reduction_var;
    DVlist *reduction_deps;
    boolean *no_reduction_doall;   // deps if this weren't a reduction
    
    reduction_info(var_sym *var); 
    ~reduction_info();
};


DECLARE_LIST_CLASSES(reduction_list_base, reduction_list_e,
		     reduction_list_iter, reduction_info *);


class reduction_list: public reduction_list_base {

public:
    reduction_list()   { }
    ~reduction_list();
    reduction_info *lookup(var_sym *var);
    reduction_info *get_info(var_sym *var);  // if lookup fails, create it
};


class nest_info
{
    void make_dependence_list();
    void make_locality_list();
 public:
    int depth;                      /* nest depth itself */
    tree_for *forlist[MAXDEPTH+1];/* from outer to inner, loops in nest */
    int outerdepth;                 /* the actual nest depth of forlist[0] */
    int outerperfect[MAXDEPTH+1];   /* index into forlist of outermost */
                                    /* loops in perfect nest */
                                    /* outerperfect[outerpcount] = depth */
    int outerpcount;                /* number of perfect nests in this nest */
    int fp[MAXDEPTH+1];             /* index into forlist for fp loops */
    int fpcount;                    /* number of fp nests */
    DVlist *deps;                   /* the dependences for this nest */
    reduction_list *reductions;     /* deps removed b/c they're reductions */
    reflists *references;           /* list of references in loop nest */
    nest_info(int d,tree_for **);
    ~nest_info();
    set_of_ugset *rlocs;            /* inner loop references: register */
    set_of_ugset *plocs;            /* inner loop references: paths */
    void find_reduction_deps();
    void print(FILE *);
};

#endif
