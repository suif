/* file "flow_graph.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#pragma interface

/* This file defines the following classes */
class Flow_graph;
class Flow_graph_node;
class Flow_graph_instr;
class Flow_graph_label;
class Flow_graph_loop;
class Flow_graph_for_loop;
class Flow_graph_pre_test;
class Flow_graph_if;
class Flow_graph_block;

/* Define Data structures need for flow graph */
int label_hash(label_sym *);
boolean compare_labels(label_sym **,label_sym **);
int tree_hash(tree_node *);
boolean compare_trees(tree_node **,tree_node **);

DECLARE_ASSOC_TABLE(Tree_to_flow_graph_node, tree_node *,
		    Flow_graph_node *, tree_hash, compare_trees);

DECLARE_ASSOC_TABLE(Label_to_flow_graph_node, label_sym*,
		    Flow_graph_node*,label_hash,compare_labels);

DECLARE_X_ARRAY0(Flow_graph_instr_xarray,Flow_graph_node*,0);

/*

Flow graphs are used to compute data-flow information.  They are based
on the sharlit's CFG classes.  The CFG classes provide control-flow
analysis and lets sharlit manipulate them largely independent of the
node representation or control-flow representation.  The member
"label_map" provides a mapping between labels and flow graph
nodes.  The member patch_list holds a list of control-flow
instructions collected during Flow_graph::enter that needs
re-examination by Flow_graph::analyze.

*/
class Flow_graph
{

  Tree_to_flow_graph_node tree_map;
  Label_to_flow_graph_node label_map;
  Flow_graph_instr_xarray patch_list;
  Variable_bit_index var_index_map;
  Variable_list bit_index_map;
  Variable_list ineligible_vars;
  bit_set ineligible_var_set;

  void collect_vars(instruction *);
  void collect_vars(operand o);
  void walk_symtabs(block_symtab *);
  void assign_bit_index(var_sym *);

  void add_var(operand o,bit_set *);
  void collect_referenced_vars(instruction *i, bit_set *, bit_set *);
  void walk_for_used_var_info(tree_node *, bit_set *, bit_set *);
  void walk_for_used_var_info(tree_node_list *, bit_set *, bit_set *);

public:
  int            no_control_flow_analysis;
  CFG_1          *const forward_graph;
  CFG_1_reversed *const backward_graph;
  proc_sym       *const procedure;

  Flow_graph(proc_sym *);
  virtual ~Flow_graph() { 
    if (backward_graph) delete backward_graph;
    if (forward_graph) delete forward_graph;
  }


  int is_subgraph(CFGnode *)
  {
    assert(0);
    return 0;
  }

  CFG_0 *get_subgraph(CFGnode *)
  {
    assert(0);
    return NULL;
  }

  /* These methods are for creating flow graphs from SUIF trees */
  int enter(Flow_graph_node *u) { return forward_graph->enter((CFGnode *)u); }
  Flow_graph_node *enter(Flow_graph_node *pred, Flow_graph_node *u);
  Flow_graph_node *enter(Flow_graph_node *pred, tree_node *, block_symtab *);
  Flow_graph_node *enter(Flow_graph_node *pred, tree_node_list *,
			 block_symtab *);
  void analyze();
  inline void link(CFGnode *u, CFGnode *v)
    { if(u && v) forward_graph->link(u->unique, v->unique); }

  /*
  associating labels with nodes for use in resolving forward
  branches
  */
  void associate_label(label_sym *l,Flow_graph_node *u)
    {
      if(label_map.associate(l,u)==0)
	error_line(1,NULL,"failed to associate label");
    }

  Flow_graph_node *lookup_label(label_sym *l)
    {
      Flow_graph_node *u;
      return label_map.lookup(l,&u) ? u : 0;
    }

  void print(FILE *);

      /* Methods to access information about variables used in
	this procedures */
  inline int set_size() { return bit_index_map.hi; }
  int get_var_index(var_sym *);
  int get_var_index(operand);
  bit_set &get_ineligible_vars() { return ineligible_var_set; }
  var_sym *get_var(int i) { return bit_index_map[i]; }
};

enum Flow_graph_node_kinds
{
  NODE_SOURCE,
  NODE_SINK,
  NODE_INSTR,
  NODE_LABEL,
  NODE_LOOP,
  NODE_FOR_LOOP,
  NODE_PRE_TEST,
  NODE_IF,
  NODE_BLOCK
};
    
/*

Flow_graph_node wraps up a tree node as a CFGnode, so that they can be
manipulated by Sharlit.  Each kind of tree nodes has a corresponding
derivative of Flow_graph_node.  The derivative keep track of fake
nodes introduced to build the control-flow graph.  See comments above
each derivative for an explanation.

*/

class Flow_graph_node: public CFGnode
{
protected:
  tree_node *tnode;
  Flow_graph *cfg;

public:

  Flow_graph_node(tree_node *tn, Flow_graph *g)
  {
    tnode = tn;
    cfg = g;
  }
  virtual ~Flow_graph_node() { }; 

  virtual Flow_graph_node_kinds kind() = 0;
  virtual instruction *instr() { return NULL; }
  tree_node *tree() { return tnode; }
};

class Flow_graph_source: public Flow_graph_node
{
public:
  Flow_graph_source(Flow_graph *g):Flow_graph_node(0,g) { }
  Flow_graph_node_kinds kind() { return NODE_SOURCE; }
};

class Flow_graph_sink: public Flow_graph_node
{
public:
  Flow_graph_sink(Flow_graph *g):Flow_graph_node(0,g) { }
  Flow_graph_node_kinds kind() { return NODE_SINK; }
};

/*

Flow_graph_instr wraps up tree_instr node.  Flow_graph_label is an
empty node used to indicate specific points within flow graphs of
structures like loops, for-loops, and ifs.

*/
class Flow_graph_instr: public Flow_graph_node
{
public:
  Flow_graph_instr(tree_instr *t, Flow_graph *g):Flow_graph_node(t,g) { }
  Flow_graph_node_kinds kind() { return NODE_INSTR; }
  instruction *instr()
  {
    return tnode->is_instr() ? ((tree_instr *)tnode)->instr() : NULL;
  }
};

class Flow_graph_label: public Flow_graph_node
{
public:
  Flow_graph_label(Flow_graph *g):Flow_graph_node(0,g) { }
  Flow_graph_node_kinds kind() { return NODE_LABEL; }
};

/*

Flow_graph_loop wraps up a tree_loop node.  Each loop has two extra
nodes: a node associated with the continue label and a node associated
with the break label.  The ``continue_node'' heads the flow graph that
corresponds to the test part of the loop.

*/
class Flow_graph_loop: public Flow_graph_node
{
public:
  Flow_graph_node *continue_node;
  Flow_graph_node *end_node;

  Flow_graph_loop(tree_loop *, Flow_graph *g);

  Flow_graph_node_kinds kind() { return NODE_LOOP; }
};

/*

Flow_graph_for_loop wraps up a tree_for node. The flow graph of a
for-loop starts with the instructions that comprise the lb, ub and the
step computations, followed by a pre-test node, then the computations
in the landing pad, then the Flow_graph_for_loop node itself, then the
body, then the continue node, and finally the end_node.

*/
class Flow_graph_for_loop: public Flow_graph_node
{
public:
  block_symtab *scope;
  bit_set read_vars;
  bit_set write_vars;

  Flow_graph_pre_test *pre_test;  /* the pre-test preceding the landing pad */
  Flow_graph_node *continue_node; /* the end, where the step and test occurs */
  Flow_graph_node *end_node;

  Flow_graph_for_loop(tree_for *, Flow_graph *, block_symtab *);
  ~Flow_graph_for_loop() {};

  Flow_graph_node_kinds kind() { return NODE_FOR_LOOP; }

#ifdef HAWG4
  int in_scope(var_sym *v)
  { return NULL!=scope->lookup_symid(v->symid()); }
#else
      /* cosmetic change for hawg 5.0 */
  int in_scope(var_sym *v)
  { return NULL!=scope->lookup_sym_id(v->sym_id()); }
#endif
};

class Flow_graph_pre_test: public Flow_graph_node
{
public:
  Flow_graph_for_loop *loop;

  Flow_graph_pre_test(tree_for *, Flow_graph *, Flow_graph_for_loop *);
  Flow_graph_node_kinds kind() { return NODE_PRE_TEST; }
};

/*

Flow_graph_if wraps up an if node.  The if has only one extra node, an
end node.

*/

class Flow_graph_if: public Flow_graph_node
{
public:
  Flow_graph_node *else_start;
  Flow_graph_node *end_node;

  Flow_graph_if(tree_if *, Flow_graph *);
  Flow_graph_node_kinds kind() { return NODE_IF; }
};

/*

Flow_graph_block heads the flow graph of a tree_block.

*/

class Flow_graph_block: public Flow_graph_node
{
public:
  Flow_graph_block(tree_block *b, Flow_graph *g):Flow_graph_node(b,g) { }
  Flow_graph_node_kinds kind() { return NODE_BLOCK; }
};
