/* file "main.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
Definitions for NEW suif driver
*/

int new_suif_phases(const char *pass,
		    char *in_suif, char *out_suif);

extern const char *k_live;
extern const char *k_read_vars;
extern const char *k_write_vars;
extern const char *k_privatizable;

/* Flags */
extern int print_graph;
extern int apply_do_loops;
