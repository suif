/* file "variables.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#pragma interface

inline int var_sym_hash(var_sym *v) { return ((long)v)>>2; }
inline int compare_vars(var_sym **a,var_sym **b) { return *a == *b; }

DECLARE_ASSOC_TABLE(Variable_bit_index, var_sym*, int, var_sym_hash,
		    compare_vars);
DECLARE_X_ARRAY0(Variable_list, var_sym*, 256);

