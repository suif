/* file "cfg.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef _CFG
#define _CFG

#pragma interface

#include "x_array.h"

/*

 Conventions:
 1. Constants are all caps.
 2. Type names begin with a capital letter.
 3. Output arguments are on the right.

*/

/* Constants */
#define MAX_LOOPS 1000

/* the type of a node -- complete opaque poiner */
class Snode;
class Header_info;
class CFinfo;
typedef Snode         CFGnode;
typedef int           Node_index;
typedef signed char   Flag;

DECLARE_X_ARRAY0(Node_index_xarray,Node_index,0);
DECLARE_X_ARRAY1(CFGnode_xarray,CFGnode,32);

/*

Graph Interface Object: To give Sharlit access to your graphs, derive
Cfg to get a subclass with functions that provide to Shalrit an
abstract graph structure.  The graph interface object also contains
utility routines: finding dominators, finding strongly connected
regions.

A graph is an array of nodes pointer: This implicit each node with an
integer, an index greater than or equal to 0 and less than n_nodes.  A
graph must have a source and a sink, although not all nodes need be
xoreachable from the source, nor the sink be reachable from all nodes.

A member function index returns the index given a node pointer.  We try
to phrase all the operations in terms of indexes.

The graph interface object tells Sharlit how to access the successors
(it calls get_next) and predecessors(it calls get_prev) of a node in
the graph.  The functions return a list of integers, the indexes to the
successors and predecessors of a node.

*/
     
/*
The type of a node.  We maintain CFGnode_bb for compatibility reason
*/
enum CFGnode_kinds {
    CFGnode_unreachable=-1,
    CFGnode_normal=0,
    CFGnode_bb,
    CFGnode_header,
    CFGnode_irreducible
};

/*

For each node, this structure records information used in control-flow
analysis.  The field dfn holds the depth-first number, a number
assigned as the nodes are encountered during the initial depth-first
traversal of the graph.  The field sn holds the SNUMBER, a number that
gives the position of a node in the reverse post-order.  The field
node_count holds the number of nodes in a region, should the node be a
header of a region.  The field _kind tell us the kind of node.  The
flag is_join, when on, indicates that the node has more than one
predecessor: information that helps with reductions.

*/

class CFinfo
{
public:
  int unique;
  int dfn;
  int sn;
  CFGnode *node;
  CFinfo *highpt;
  Header_info *h_info;
  CFGnode_kinds _kind;
  unsigned char depth;
  Flag is_join, is_fork, begins_bb, needs_var;

  CFGnode_kinds kind()
  {
    return (CFGnode_kinds)_kind;
  }
  
  void print(FILE *);
};

/*

The structure Header_info contains information about each region header
in the CFG.  The field cfi points to the CFinfo structure of the header;
the index start and end delimit the nodes that form the region.  We represent
a region by storing contiguously pointers to CFinfo structure of the region\'s
node in CFG::interval_order.

*/

class Header_info
{
  public:
    CFinfo *cfi;
    int start, end;
    Node_index_xarray other_entrances;
};

/*

The class CFG_0 has two roles: to provide an abstract view of graphs to
Sharlit, and to preprocess the graph for Sharlit.  With Cfg, Sharlit can
view graphs where the nodes have an index, and where the edges are stored
in edge vectors.  Cfg also preprocesses the graph by ordering the nodes, and
by computing a _Snumber structure for each node.

The results of the preprocessing are stored in the fields: numbers,
rpo_order, and interval_order.  The field numbers is an array, indexed
by Node_index, of CFinfo, one for each node.  The field rpo_order
is an array of size n_nodes gives the reverse post-ordering of the
graph, an ordering best for iterative analysis.  The field
interval_roder, also an array of size n_nodes, gives the ordering that
is best for control-flow analysis.

To create your own flow graph class, derive from CFG_0.  In the
derived class, you must instantiate the abstract methods of CFG_0.

*/

class CFG_0
{
  /* Control-flow Analysis Routines */
  void dfs(int &,int &,Node_index);
  void set_highpt(Node_index,Node_index);
  CFinfo **compute_modified_order(CFinfo *,CFinfo **,CFinfo **);
  Header_info *find_header(CFinfo *);

public: 	 
  int debug_level;
  CFinfo *numbers;
  CFinfo **base_of_arrays;
  CFinfo **interval_order;
  CFinfo **modified_order;
  Header_info *header_info;
  Node_index source, sink;
  int n_nodes, n_headers, n_unreachables;
  int n_irred;

  CFG_0();
  virtual ~CFG_0();

  /*
  Sometimes, the interval order can`t be determined but the
  modified can be.  This is true for reversed flow graphs.
  In that case, we can apply only iteration.
  */
  virtual int apply_iteration_only() { return interval_order==NULL; }
  virtual int already_analyzed()     { return base_of_arrays!=NULL; }
  virtual void analyze();
  virtual void set(CFGnode *,CFGnode *);
  virtual void clear();

  /* abstract methods */
  virtual int number_of_nodes() = 0;
  virtual CFGnode *get_node(Node_index) = 0;
  virtual int enter(CFGnode *) = 0;

  /* abstract methods relating to edges */
  virtual CFGnode **successors(Node_index,int *) = 0;
  virtual CFGnode **predecessors(Node_index,int *) = 0;
  virtual void link(Node_index,Node_index) = 0;
  virtual int is_subgraph(CFGnode *) = 0;
  virtual CFG_0 *get_subgraph(CFGnode *) = 0;
};

/*

We need to treat reversed flow graphs differently from forward graphs when
we analyze them.  This class encapsulated this special analysis.  The
reverse control-flow information is computed directly from the result of
forward analysis.  At present, the reverse information is adequate only
for iterative data-flow analysis.

*/

class CFG_0_reversed: public CFG_0
{
  CFG_0 *forward_graph;

public:
  CFG_0_reversed(CFG_0 *);
  virtual ~CFG_0_reversed();

  virtual void analyze();
};

/*

CFG_1 extends CFG_0 with a concrete implementation of edges.  We
believe that this implementation will suit most flow graphs.
Information about edges are stored in two arrays: edge index and
edges.  Each node has a forward and a backward edge index which is
the index within edges at which the list of successors and predecessors
begin.

The three preprocessor identifers (CFG_EDGE_UNINIT, CFG_EDGE_UNIQUE,
CFG_EDGE_MULTIPLE) are used to represent edges.  Initially all nodes
have their forward and backward indices equal CFG_EDGE_UNINIT.  If in
the process of adding edges, we determine that an edge connects
uniquely two nodes u and v such that u=nodes[i] and v=nodes[i+1] then
we set their forward and backward indexes to CFG_EDGE_UNIQUE
respectively.  Otherwise the index is set to CFG_EDGE_MULTIPLE during
CFG_1::analyze() or when we determine the edge is no longer unique in
CFG_1::link().

The initial size parameter is not a strict limit on the size of
procedures.  Instead the parameter determines the initial size of
xarray structures.  The parameter is chosen to reduce repeated mallocs
and copies.

*/
#define CFG_EDGE_UNINIT   (-2)
#define CFG_EDGE_UNIQUE   (-1)
#define CFG_EDGE_MULTIPLE (0)
  
#define CFG_1_INITIAL_SIZE 512

class CFG_1_edge
{
public:
  CFGnode *from, *to;
  CFG_1_edge() { }
  CFG_1_edge(CFGnode *f,CFGnode *t) { from = f; to = t; }
};

DECLARE_X_ARRAY0(CFG_1_edge_xarray,CFG_1_edge,0);

class CFG_1: public CFG_0
{
  friend class CFG_1_reversed;

  /* Following members hold nodes and edges */
  CFGnode_xarray nodes;
  int *n_forward_edges, *n_backward_edges;
  Int_xarray forward_edge_index;
  Int_xarray backward_edge_index;
  CFGnode_xarray edges;

  /*
  Following members used temporary to compute more
  efficient edge representation held in forward_edge_index and
  backward_edge_index.
  */
  int edges_computed;
  CFG_1_edge_xarray raw_edges;

public:

  /* flags */
  int no_control_flow_analysis;

  CFG_1();
  virtual ~CFG_1();

  /* build edge information */
  virtual void analyze();

  /* instantiate abstract methods of CFG_0 */
  virtual int number_of_nodes();
  virtual CFGnode *get_node(Node_index);
  virtual int enter(CFGnode *);

  virtual CFGnode **successors(Node_index,int *);
  virtual CFGnode **predecessors(Node_index,int *);
  virtual void link(Node_index,Node_index);
  virtual int is_subgraph(CFGnode *)      { assert(0); return 0; }
  virtual CFG_0 *get_subgraph(CFGnode *)  { assert(0); return NULL; }

  /* new methods */
  virtual int number_of_edges();
};

/*

Given a CFG_1, we can construct the reversed graph represented by
the class below.  The base class is still CFG_0 which will serve
as repository of information computed by reverse "control-flow analysis."

*/
class CFG_1_reversed: public CFG_0_reversed
{
  CFG_1 *forward_graph;      /* the sister graph */

public:
  /* flags */
  int no_control_flow_analysis;

  CFG_1_reversed(CFG_1 *g):CFG_0_reversed(g)
  {
    no_control_flow_analysis = 0;
    forward_graph = g;
  }

  virtual void analyze()
  {
    if(!no_control_flow_analysis)
      CFG_0_reversed::analyze();
  }

  /* instantiate abstract methods of CFG_0 */
  virtual int number_of_nodes()
  { return forward_graph->number_of_nodes(); }

  virtual CFGnode *get_node(Node_index i)
  { return forward_graph->get_node(i); }

  virtual int enter(CFGnode *)
  { assert(0); return -1; }

  virtual CFGnode **successors(Node_index u,int *ip)
  { return forward_graph->predecessors(u,ip); }

  virtual CFGnode **predecessors(Node_index u,int *ip)
  { return forward_graph->successors(u,ip); }

  virtual void link(Node_index,Node_index)  { assert(0); }
  virtual int is_subgraph(CFGnode *)        { assert(0); return 0; }
  virtual CFG_0 *get_subgraph(CFGnode *)    { assert(0); return NULL; }

  /* new methods */
  virtual int number_of_edges()
  { return forward_graph->number_of_edges(); }
};


#endif /* _CFG */
