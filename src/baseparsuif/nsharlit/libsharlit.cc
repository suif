/* file "libsharlit.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Sharlit Graphs (Implementations) */

#pragma implementation "sharlit.h"

#include "sharlit.h"
#include "cfg.h"
#include <stdio.h>

/*

Snode_list member functions

*/
Snode *
Snode_list::pop()
{
	count--;
	return (Snode *)conslist::pop();
}

/*

The @set_append@ function puts an Snode on the list as though the
@Snode_list@ is a set.  Since sets may not have duplicate elements, we
must check before appending.

*/
Snode *
Snode_list::set_append(Snode *s)
{
	if(!contains(s))
		append(s);
	return s;
}

/*

This is an expensive operation.  It should be used for prototyping, not
for frequent use in production code.

*/
void
Snode_list::remove(Snode *s)
{
    Snode_iter si(this);
    Snode_e *c=0;
    while(!si.is_empty()){
	c=si.f_step();
	if(c->to()==s)
	  break;
    }
    assert(c);
    count--;
    glist::remove(c);
}

int
Snode_list::contains(Snode *s)
{
    Snode_iter si(this);
    while(si.is_empty()==0){
	if(si.step()==s)
	  return 1;
    }
    return 0;
}

/*

This function duplicates all the @Snode@ elements in a list
and returns a new list containing all of them.

*/
Snode_list *
Snode_list::copy()
{
	Snode_iter si(this);
	Snode_list *nw=new Snode_list;
	while(!si.is_empty())
		nw->append(si.step());
	return nw;
}

/*

Remove and free all the elements on the list.

*/
void
Snode_list::mk_empty()
{
    while(!is_empty()) pop();
}

void
Snode_list::reverse(Snode_list *l)
{
    Snode_iter si(l);
    assert(is_empty());
    while(!si.is_empty())
      push(si.step());
}

Snode *
Snode_list::operator[](int i)
{
	Snode_iter si(this);
	Snode *s=0;
	for(;i>=0 && !si.is_empty();i--)
		s=si.step();
	return s;
}

Sedge *Sedge_list::insert(Sedge *before,Sedge *n,Sedge *after)
{
  if(after==0
     || after->to->number < n->to->number
     || (after->to->number==n->to->number
	 && after->from->snumber > n->from->snumber)){
    if(before)
      before->next_e=n;
    else
      head_e=n;
    n->next_e=after;
    if(after==0)
      tail_e=n;
    return n;
  }else return insert(after,n,(Sedge *)after->next_e);
}

/*

The default node constructor.

*/
Snode::Snode(Svar *,Strans *)
{
}

/*
Special Strans objects
*/
Strans strans_top(Sk_top), strans_bottom(Sk_bottom);
Strans strans_identity(Sk_identity);
Snode snode_nowhere;

/*

Destroy an Strans object.  The destructor knows not to destory the
three special Strans object --- bottom, top, and identity --- as we
have statically allocated these special objects.

*/
Strans::~Strans()
{
    switch(kind){
      case Sk_bottom: case Sk_top: case Sk_identity:
	break;
      default:
	kind = Sk_bottom;
    }
}

void Strans::operator delete(void *ptr)
{
    if ((ptr != &strans_top) && (ptr != &strans_bottom) &&
	(ptr != &strans_identity)) {
	::operator delete(ptr);
    }
}

/*
Checks whether an edge is a cycle
*/
int
is_cycle(Snode *to, Snode *from)
{
    Seqn *q=from->Q;
    if(q==0 || q->highpt==0)
      return 0;
    if(q->highpt==to)
      return 1;
    else
      return is_cycle(to,q->highpt);
}

/*##
frag(default_transformer,"Default Transformer")
tex @{
 

Every subclass of @Strans@ must supply a transform function.  This is
enforced at run-time by providing a default transformer that causes an
assertion failure.

@}

*/

int Strans::transform(Seqn *,int)
{
    assert(0);
    return 0;
}

void Strans::flow(Snode *,Svar *)
{
    assert(0);
}


void Strans::_init(Snode *)
{
    assert(0);
}

void Strans::in_action(Snode *,Svar *)
{
}

void Strans::out_action(Snode *,Svar *)
{
}

Strans *Strans::copy()
{
    switch(kind){
      case Sk_top: case Sk_bottom: case Sk_identity:
	return this;
      default:
	assert(0);
    }
    return 0;
}

Strans *
Strans::times_ss(Strans *r)
{
    switch(kind){
      case Sk_top:
	return r==&strans_bottom ? r : this;
      case Sk_bottom:
	return this;
      case Sk_identity:
	return r->copy();
      default:
	assert(0);
    }
    return 0;
}

/*##
frag(strans_times_su,"@Strans::times_su@")

*/
Strans *
Strans::times_su(Strans *r)
{
    switch(kind){
      case Sk_top: case Sk_bottom:
	delete r;
	return this;
      case Sk_identity:
	return r;
      default:
	Strans *b=times_ss(r);
	delete r;
	return b;
    }
}
/*##
frag(strans_star_u,"@Strans::star_u@")
tex @{
This member function computes the Kleene's star of
the function represented by the @Strans@ object.
The suffix @_u@ indicates that this member will destroy
its argument which, in this case, is the pointer @this@.
@}

*/
Strans *
Strans::star_u()
{
    switch(kind){
      case Sk_top:
	return &strans_identity;
      case Sk_bottom:
	return &strans_bottom;
      default:
	delete this;
	return &strans_bottom;
    }
}


/*##
frag(strans_absorb_us,"@Strans::absorb_us@")

*/
Strans *
Strans::absorb_us(Strans *l)
{
    switch(kind){
      case Sk_top: case Sk_bottom:
	return l->kind==Sk_bottom ?  &strans_bottom : this;
      default:
	assert(0);
	return 0;
    }
}

/*##
frag(strans_absorb_uu,"@Strans::absorb_uu@")

*/
Strans *
Strans::absorb_uu(Strans *r)
{
    Strans *l;
    switch(r->kind){
      case Sk_top:
      case Sk_bottom:
	delete this;
	return r;
    }
    l=absorb_us(r);
    if(l!=this){
	delete this;
	delete r;
	return l;
    }
    delete r;
    return this;
}

/*##
frag(strans_meet_uu,"@Strans::meet_uu@")

*/
Strans *
Strans::meet_uu(Strans *r)
{
    switch(kind){
      case Sk_top:
	return r;
      case Sk_bottom:
	delete r;
	return &strans_bottom;
      default:
	delete this;
	delete r;
	return &strans_bottom;
    }
}
/*##
frag(start_end_interval,"@Strans::start_int@ and @Strans::end_int@")

*/
void
Strans::start_int(Sproblem *,Seqn *)
{}
void
Strans::end_int(Sproblem *,Seqn *)
{}

void
Sproblem::set_highpt(Seqn *header,Seqn *u)
{
    Snode **deps;
    int d, nd;
    if(u==header) return;
    if(u->highpt){
	set_highpt(header,u->highpt->Q);
	return;
    }
    if(u->snumber<header->snumber){
	header->kind=SEQN_improper;
	return;
    }
    u->highpt=header->S;
    header->rcount++;
    deps=dependents2(u->S,&nd);
    for(d=0;d<nd;d++)
      set_highpt(header,deps[d]->Q);
}
void
Sproblem::dfs1_a(int &n,int &m,Seqn *v,Seqn **so,
		Sedge_list *be, int unreachable)
{
    Snode **deps;
    int i,nd;
    m=v->number=m+1;
    if(unreachable) v->kind=SEQN_unreachable;
    deps=dependees2(v->S,&nd);
    for(i=0;i<nd;i++){
	Seqn *w=deps[i]->Q;
	if(w->number==0){
	    dfs1_a(n,m,w,so,be,unreachable);
	}else if(w->snumber==0){
	    /* frond or back-edge */
	    if(be) be->append_sorted(v,w);
	}else if(v->number<w->number){
	    ;/* reverse front */
	}else{
	    ;/* cross linke */
	}
    }
    n=v->snumber=n-1;
    if(so){
	assert(n>=0);
	so[n-1]=v;
    }
}
void
Sproblem::dfs1(int &n,int &m,Seqn *v,Seqn_conslist *dfo,Seqn_conslist *so,
		Sedge_list *be, int unreachable)
{
    Snode **deps;
    int i,nd;
    m=v->number=m+1;
    if(unreachable) v->kind=SEQN_unreachable;
    if(dfo) dfo->append(v);
    deps=dependees2(v->S,&nd);
    for(i=0;i<nd;i++){
	Seqn *w=deps[i]->Q;
	if(w->number==0){
	    dfs1(n,m,w,dfo,so,be,unreachable);
	}else if(w->snumber==0){
	    /* frond or back-edge */
	    if(be) be->append_sorted(v,w);
	}else if(v->number<w->number){
	    ;/* reverse front */
	} else {
	    ;/* cross linke */
	}
    }
    n=v->snumber=n-1;
    if(so) so->push(v);
}
/*
frag(isolve2,"@Sproblem::isolve2(Seqn_conslist *l,int n)@")
tex @{

The @changed@ flag is set to 1 initially to indicate that first
iteration; it is set to 0 for subsequent iterations.

@}

*/
int
Sproblem::isolve2(Seqn **qa,int nq,int n)
{
    int c;
    int iter=0;
    changed=1;
    iteration=0;
    do {
	c=flow_a(qa,nq);
	iter++;
	iteration++;
	changed=0;
    } while (c && --n > 0);
    return iter;
}

int
Sproblem::isolve2(Seqn_conslist *l,int n)
{
    int c;
    int iter=0;
    l->init();
    changed=1;
    iteration=0;
    do {
	c=flow(l);
	iter++;
	iteration++;
	changed=0;
    } while (c && --n > 0);
    return iter;
}

Sproblem::Sproblem(CFG_0 *g,int d)
     :graph(g),direction(d)
{
    checking=1; cnt=0; rcnt=0;
    check_limit=1;
    irred_cnt=0;
    in_bb=0;
    bb_head=0;
    debug_flags=0;
}

Sproblem::~Sproblem()
{
}

/*##

Besides calling the @transform@ function for an equation, @start_status@
and @end_state@ computes two flags: @in_bb@ and @changed@.

The flag @in_bb@ is on before calling @transform@ for those equations
within a basic block.  The flag is off just before the entry node of a
basic block.  For our purposes, a basic block consists of the header and
all those contiguous nodes following that we can find a transformer to.

The flag @changed@ is on when the previous node's transformer
indicates a change, or when we cross a join point.  If @changed@ is
off then some transfomers can safely avoid doing some work because no
change is possible.

*/

int
Sproblem::start_status(Seqn *q,int mode)
{
    if(iteration==0)
      changed=1;
    switch(q->kind){
      case SEQN_bb:
	in_bb=0;
	changed=1;
	break;
      case SEQN_normal:
	if(q->ancestor!=bb_head){
	    in_bb=0;
	    changed=1;
	}else if(!in_bb)
	  changed=1;
	break;
      default:
	in_bb=0;
	changed=1;
    }
    if(q->kind==SEQN_unreachable)
      return 0;
    return q->L->transform(q,mode);
}
int
Sproblem::end_status(Seqn *q,int c)
{
    switch(q->kind){
      case SEQN_bb:
	in_bb=1;
	bb_head=q->S;
	changed=c;
	break;
      case SEQN_normal:
	changed = in_bb ? c : 1;
	break;
      default:
	in_bb=0;
	changed=1;
    }
    if(q->solved()){
	assert(!c);
	changed=1;
    }
	return c;
}

/*##
frag(dfs,"Depth-first search of graph")
tex @{
These two routines produce a depth first traversal of the graph.
If @dfo@ is not nil then it also produce a depth first list
of the nodes.
The arguments @c1@ and @c2@ are two possibly disconnected components
of the flowgraph.
If the flag @unreachable@ is on, then we mark set all equations
to be @SEQN_unreachable@.
@}

*/
void
Sproblem::dfs(Seqn_list *ql,int nnodes,Seqn_conslist *dfo,
		Seqn_conslist *so,Sedge_list *back_edges, int unreachable)
{
	int n=nnodes+1,m=0;
	if(back_edges) assert(back_edges->is_empty());
	dfs1(n,m,ql->first(),dfo,so,back_edges,unreachable);
}

Seqn *
Sproblem::equation(Seqn *q)
{
    assert(0);
    return q;
}

/*##
frag(isolve_seqn_alternate,"@Sproblem::isolve(Seqn_list *l,int n)@")

*/
int
Sproblem::isolve(Seqn **qa,int nq,int n)
{
    int iter;
    Seqn **nqa, *q;
    Snode *s;
    int i;
    nqa=new Seqn *[nq];
    for(i=0;i<nq;i++){
	q=equation(qa[i]);
	s=q->S;
	s->Q=q;
	nqa[i]=q;
    }
    cnt=rcnt=nq;
    iter=isolve2(nqa,nq,n);
    delete nqa;
    if(check_limit)
      assert(iter<n);
    return iter;
}

int
Sproblem::isolve(Seqn_conslist *l,int n)
{
    int iter;
    Seqn_conslist el;
    Seqn_consiter qi;
    Seqn *q;
    
    for(qi.reset(l);!qi.is_empty();){
	q=qi.step();
	el.append(equation(q));
    }
    cnt=rcnt=el.count();
    iter=isolve2(&el,n);
    while(!el.is_empty())
      el.pop();
    if(check_limit)
      assert(iter<n);
    return iter;
}

int
Sproblem::isolve(Snode_list *l,int n)
{
    int iter;
    Snode_iter si(l);
    Seqn_list ol;
    Seqn_conslist so;
    
    while(!si.is_empty())
      ol.append(equation(new Seqn(si.step())));
    ol.init();
    dfs(&ol,ol.count(),0, &so, 0);	/*ol is dead after dfs */
    ol.clear();
    cnt=rcnt=so.count();
    iter=isolve2(&so,n);
    while(!so.is_empty())
      delete so.pop();
    if(check_limit)
      assert(iter<n);
    return iter;
}

/*##
frag(Sproblem_flow,"@Sproblem::flow(Seqn_conslist *l)@")

*/
int
Sproblem::flow_a(Seqn **qa,int nq)
{
    int ch=0, c;
    int i;
    print("flow start\n");
    bb_head=0;
    if(checking){
	for(i=0;i<nq;i++){
	    Seqn *e=qa[i];
	    e->L->_init(e->S);
	    ch |= end_status(e,c=start_status(e,FM_flow));
	    debug_print(e->S,e->L,0,c);
	}
    }else
      assert(0);	/* FOR NOW */
    print("flow end\n");
    return ch;
}
int
Sproblem::flow(Seqn_conslist *l)
{
	int ch=0, c;
	Seqn_consiter si(l);
	bb_head=0;
	if(checking){
		while(si.is_empty()==0){
			Seqn *e=si.step();
			e->L->_init(e->S);
			ch |= end_status(e,c=start_status(e,FM_flow));
		}
	} else {
		assert(0);	/* FOR NOW */
		while(si.is_empty()==0){
			Seqn *e=si.step();
			if(e->solved()){
				changed=1;
				continue;
			}
			e->L->_init(e->S);
			c |= e->L->transform(e,FM_flow);
			changed = c;
			ch |= c;
		}
	}
	return ch;
}

/*##
frag(dependees,"Successor nodes in the flow graph")

*/
Snode **
Sproblem::dependees2(Snode *s, int *np)
{
    if(direction<0)
      return (Snode **)graph->predecessors(s->unique,np);
    else if(direction>0)
      return (Snode **)graph->successors(s->unique,np);
    assert(0);
    return 0;
}
Snode **
Sproblem::dependents2(Snode *s,int *np)
{
    if(direction<0)
      return (Snode **)graph->successors(s->unique,np);
    else if(direction>0)
      return (Snode **)graph->predecessors(s->unique,np);
    assert(0);
    return 0;
}

/*

Default virtuals:  Derived classes should replace these routines to
print customized debugging messages.  Printq can also be used as a hook
to observe the progress of data-flow analysis.

*/
void Sproblem::printq(Seqn *,Svar *,int)
{
    assert(0);
}
void Sproblem::debug_print(Snode *,Strans *,Svar *,int)
{
}
void Sproblem::print(char *)
{}

void Sproblem::clean(Seqn **qa,int n)
{
    Seqn *q;
    int i;
    for(i=0;i<n;i++){
	q=qa[i];
	if(q->L)
	  delete q->L;
	delete_var(q->V);
    }
}

int
Sproblem::get_srcs(Svar **&srcs,Snode *n)
{
    int nd;
    static Svar *var_array[MAX_ARITY], **svp;
    Snode **preds = dependents2(n,&nd);
    Snode **preds_limit = &preds[nd];
    Seqn *q;
    for(svp=&var_array[0];preds<preds_limit;preds++){
	q=preds[0]->Q;
	if(q->kind==SEQN_unreachable)
	  continue;
	svp[0] = q->V;
	svp++;
    }
    srcs=var_array;
    return svp-var_array;
}

/*##
frag(Sproblem_new_var,"@Sproblem::new_var@")
tex @{

These functions are called by sharlit runtimes when it needs a new
variable or a copy of a variable.

@}

*/

Svar *
Sproblem::new_var()
{
    assert(0);
    return 0;
}

void
Sproblem::delete_var(Svar *v)
{
    delete v;
}

int
Sproblem::copy_var(Svar *,Svar *)
{
    assert(0);
    return 0;
}

void
Sproblem::meet(Svar *,Snode *)
{
    assert(0);
}

int Seqn::depth()
{
    register Seqn *q=this;
    register int d=0;
    while(q && q->highpt){
	q=q->highpt->Q;
	d++;
    }
    return d;
}

void Seqn::init()
{
	Snode *s;
	s=S;
	s->Q=this;
	L->_init(S);
}
/*##
frag(Seqn_push_pop,"@Seqn::pop@ and @Seqn::push@")

*/

Seqn *Seqn::pop()
{
    S->Q=covered;
    return covered;
}
void Seqn::push(Seqn *q)
{
    q->covered=this;
    S->Q=q;
}
Seqn *Seqn::top()
{
    return S->Q;
}

Seqn_list::~Seqn_list()
{
  while(!is_empty())
    delete pop();
  head_e = tail_e = NULL;
}

/*##

To determine the dependence relationship between the @Seqn@ values
in a @Seqn_list@, we have to set the @Q@ pointer to the @Seqn@ value
it is associated with.  This has to be done before solving time.
Note that if the @G@ field is zero then the equation is special and
doesn't need to be initialized.

*/
void Seqn_list::init()
{
    Seqn_iter qi(this);
    while(!qi.is_empty()){
	Seqn *q=qi.step();
	Snode *s=q->S;
	s->Q=q;
	q->L->_init(q->S);
    }
}

void Seqn_conslist::init()
{
    Seqn *q;
    Snode *s;
    Seqn_consiter qi(this);
    while(!qi.is_empty()){
	q=qi.step();
	s=q->S;
	s->Q=q;
	q->L->_init(q->S);
    }
}

/*##

evaluate and compress the header tree.  r is some node within the
header forest.  if r is zero, then compress, and return bottom.  if r
is non-zero, then compress, and return relative to r.

*/

Strans *
Sprob_tarjan::eval(Snode *s,Snode *r)
{
    Snode *a=ancestor(s);
    if(a!=0)
      path_compress(s);
    if(r){
	if(ancestor(s)==r)
	  return s->Q->iL;
	else if(s==r)
	  return &strans_identity;
	else	/* we don't know */
	  return &strans_bottom;
    }else
      return &strans_bottom;
}

Strans *
Sprob_tarjan::eval(Snode *s)
{
    Snode *a=ancestor(s);
    Seqn *q=s->Q;
    if(a!=0)
      path_compress(s);
    if(ancestor(s)==0){
	switch((SEQN_type) q->kind){
	  case SEQN_noncyclic:
	  case SEQN_header:
	  case SEQN_dominator:
	  case SEQN_bb:
	    return &strans_identity;
	  case SEQN_normal: case SEQN_unreachable:
	  case SEQN_improper:
	    return &strans_bottom;
	}
    }
    return q->iL;
}
void
Sprob_tarjan::path_compress(Snode *s)
{
    Snode *a=ancestor(s);
    if(ancestor(a)){
	path_compress(a);
	s->Q->iL = a->Q->iL->times_su(s->Q->iL);
	s->Q->ancestor=ancestor(a);
    }
}

/*##
frag(Sprob_tarjan_R,"@Sprob_tarjan::R@")
tex @{
This is an auxilliary funciton for @Sprob_tarjan::reducer@.
It performs node reductions as directed by the interval structure.
For improper or irreducible intervals @Sprob_tarjan::R@
cannot collapse any nodes into the header; the result is that
these intervals are handled iteratively.
@}

*/
void
Sprob_tarjan::R(Seqn_consiter *qi)
{
    Seqn *h,*q,*bbq;
    Snode **deps, *s;
    Strans *P,*Q;
    int nd, d;
    if(qi->is_empty())
      return;
    h=qi->peek();
    switch((SEQN_type)h->kind){
      case SEQN_header:
      case SEQN_noncyclic:
	qi->step();
	while(!qi->is_empty()){
	    q=qi->peek();
	    if(q->highpt!=h->S)
	      break;
	    if(q->kind!=SEQN_normal){
		R(qi);
		continue;
	    }
	    qi->step();
	    deps=dependents2(q->S,&nd);
	    P=Q=&strans_top;
	    for(d=0;d<nd;d++){
		s=deps[d];
		P=P->meet_uu(eval(s,h->S)->times_ss(q->iL));
	    }
	    update(q->S,P,Q);
	    link(h->S,q->S);
	}
	break;
      case SEQN_bb:
	qi->step();
	Q= &strans_top;
	bbq=0;
	/* unrolled one iteration, and then bbq==0 */
	if(qi->is_empty())
	  break;
	q=qi->peek();
	if(q->highpt!=h->S)
	  break;
	qi->step();
	deps=dependents2(q->S,&nd);
	assert(nd==1);
	P=eval(deps[0],h->S)->times_ss(q->iL);
	update(q->S,P,Q);
	link(h->S,q->S);
	bbq=q;
	while(!qi->is_empty()){
	    q=qi->peek();
	    if(q->highpt!=h->S)
	      break;
	    qi->step();
	    deps=dependents2(q->S,&nd);
	    assert(nd==1);
	    P=bbq->iL->absorb_us(q->iL);
	    if(P==bbq->iL && bbq->iL!=&strans_bottom)
	      bbq->iL=0;
	    update(q->S,P,Q);
	    link(h->S,q->S);
	    bbq=q;
	}
	break;
      case SEQN_improper:
	qi->step();
	while(!qi->is_empty()){
	    q=qi->peek();
	    if(q->highpt!=h->S)
	      break;
	    if(q->kind!=SEQN_normal){
		R(qi);
		continue;
	    }
	    qi->step();
	    update(q->S,&strans_bottom,&strans_bottom);
	    link(h->S,q->S);
	}
	break;
      case SEQN_normal: case SEQN_unreachable:
	qi->step();
	break;
      case SEQN_dominator:
	assert(0);
    }
    P=Q=&strans_top;
    deps=dependents2(h->S,&nd);
    for(d=0;d<nd;d++){
	s=deps[d];
	if(!is_cycle(h->S,s))
	  P=P->meet_uu(eval(s,h->highpt)->times_ss(h->iL));
	else
	  Q=Q->meet_uu(eval(s,h->S)->times_ss(h->iL));
    }
    update(h->S,P,Q);
    if(h->highpt)
      link(h->highpt,h->S);
    else
      keep(h->S);
}
int
Sprob_tarjan::propagate(Seqn_conslist *l)
{
	int c=0;
	int saved_iteration=iteration;
	Seqn_consiter si(l);
	iteration=0;
	while(!si.is_empty())
		c |= F(&si);
	iteration=saved_iteration;
	return c;
}
int
Sprob_tarjan::F(Seqn_consiter *qi)
{
    Seqn *q, *h;
    int c, ch=0;
    changed=1;
    switch((SEQN_type)(h=qi->peek())->kind){
      case SEQN_dominator: case SEQN_improper: case SEQN_header:
      case SEQN_noncyclic: case SEQN_bb:
	h->L->_init(h->S);
	h->L->start_int(this,h);
	if(!h->solved()){
	    c=h->L->transform(h, FM_propagate);
	    ch|=c;
	}
	qi->step();
	changed = 1;
	while(!qi->is_empty()){
	    q=qi->peek();
	    if(q->highpt==h->S){
		if(q->kind!=SEQN_normal)
		  ch |= F(qi);
		else{
		    qi->step();
		    q->L->_init(q->S);
		    c=q->L->transform(q,FM_propagate);
		    changed = q->solved() || c;
		    assert(!q->solved()||!c);
		    ch|=c;
		}
	    }else break;
	}
	h->L->_init(h->S);
	h->L->end_int(this,h);
	if(h->solved()){
	    c=h->L->transform(h,FM_propagate);
	    changed = 1;
	    assert(!c);
	    ch|=c;
	}
	break;
      case SEQN_normal:
	qi->step();
	h->L->_init(h->S);
	c=h->L->transform(h,FM_propagate);
	changed = h->solved() || c;
	assert(!h->solved()||!c);
	ch|=c;
	break;
      case SEQN_unreachable:
	/* don't do anything */
	qi->step();
		break;
	}
	return ch;
}

// Default virtual.  This function is normally generated
// by the sharlit preprocessor

int Sprob_tarjan::nonterminal(Strans *)
{
	assert(0);
	return 0;
}

/* Mark a node as kept --- The node becomes part of the reduced graph */
void Sprob_tarjan::keep(Snode *s)
{
    Seqn *q=s->Q;
    Snode **deps;
    int i,nd;
    deps=dependents2(s,&nd);
    q->rcount |= 1;
    switch(q->kind){
      case SEQN_normal:
	assert(nd==1);
	deps[0]->Q->rcount |= 1;
	break;
      default:
	for(i=0;i<nd;i++){
	    deps[i]->Q->rcount |= 1;
	    deps[i]->Q->needs_var=1;
	}
	break;
    }
}

void Sprob_tarjan::link(Snode *hd,Snode *s)
{
    Seqn *q=s->Q;
    if(q->iL!=&strans_bottom)
      q->ancestor=hd;
    else
      keep(s);
}

void Sprob_tarjan::update(Snode *s,Strans *P,Strans *Q)
{
    Seqn *q=s->Q;
    Strans *l=P->absorb_uu(Q->star_u());
    switch(l->kind){
      case Sk_top: case Sk_bottom:
	q->iL=&strans_bottom;
	break;
      default:
	q->iL=l;
    }
}

/* Generate Modified Interval Order */

void Sprob_tarjan::RO_a(int &qi,Seqn **ro,int &qj,Seqn **so,int lim)
{
    Seqn *q,*h;
    Seqn_conslist out;
    h=so[qj];
    switch((SEQN_type)h->kind){
      case SEQN_normal: case SEQN_unreachable:
	return;
      case SEQN_header: case SEQN_noncyclic: case SEQN_bb:
      case SEQN_improper:
	ro[qi++]=so[qj++];
	while(qj<lim){
	    q=so[qj];
	    if(q->highpt==h->S){
		if(q->kind==SEQN_normal)
		  ro[qi++]=so[qj++];
		else
		  RO_a(qi,ro,qj,so,lim);
		h->rcount--;
	    }else if(h->rcount)
	      out.push(so[qj++]);
	    else
	      break;
	}
	assert(h->rcount==0);
	while(!out.is_empty()){
	    so[--qj]=out.pop();
	    assert(qj>=0);
	}
	break;
      case SEQN_dominator:
	assert(0);
    } /*switch*/
}

void Sprob_tarjan::reduction_order_a(Seqn **ro,Seqn **so_list,int nq,
				Sedge_list *back_edges)
{
    Sedge_iter ei;
    Sedge *be;
    Seqn *q,*oq,*h;
    Snode **preds;
    int n_preds, n_succs, i,j;
    if(back_edges){
	for(ei.reset(back_edges);!ei.is_empty();){
	    be=ei.step();
	    if(be->to->kind==SEQN_unreachable)
	      continue;
	    be->to->kind=SEQN_header;
	    set_highpt(be->to,be->from);
	    if(be->to->kind==SEQN_improper)
	      irred_cnt++;
	}
    }
    /* iterate through all the graph components */
    for(i=0;i<nq;){
	while(i<nq && so_list[i]->kind==SEQN_unreachable)
	  i++;
	if(i>=nq)
	  break;
	if(so_list[i]->number==1){
	    /* we are making the strong assumption that all reachable nodes
	      following the initial node are exactly those dominated
		by the initial node.
		  */
	    q=so_list[i++];
	    q->kind=SEQN_noncyclic;
//	    q->needs_var=1;
	    while(i<nq && so_list[i]->kind!=SEQN_unreachable)
	      if(so_list[i]->highpt==0){
		  so_list[i++]->highpt=q->S;
		  q->rcount++;
	      }else
		i++;
	}else
	  while(i<nq && so_list[i]->kind!=SEQN_unreachable)
	    if(so_list[i]->highpt==0)
	      so_list[i++]->highpt = &snode_nowhere;
	    else
	      i++;
    }
    for(i=0,j=0;j<nq;){
	while(j<nq)
	  switch(so_list[j]->kind){
	    case SEQN_unreachable:
	    case SEQN_normal:
	      ro[i++]=so_list[j++];
	      break;
	    default:
	      goto done;
	  }
      done:
	if(i>=nq)
	  break;
	RO_a(i,ro,j,so_list,nq);
    }
    for(oq=0,h=0,j=0;j<nq;){
	q=ro[j++];
	if(q->kind==SEQN_unreachable){
	    h=0;
	    continue;
	}
	preds=dependents2(q->S,&n_preds);
	if(n_preds!=1){
	    for(i=0;i<n_preds;i++)
	      preds[i]->Q->needs_var=1;
	    h=0;
	}
	if(oq && (oq->number+1!=q->number||oq->snumber+1!=q->snumber))
	  h=0;
	if(q->kind==SEQN_normal){
	    if(h)
	      q->highpt=h->S;
	    else{
		q->kind=SEQN_bb;
		h=q;
	    }
	    dependees2(q->S,&n_succs);
	    if(n_succs>1){
		h=0;
		q->needs_var=1;
	    }
	}else
	  h=0;
	oq=q;
    }
}
/*##

The process of reduction, using combining rules provided by the
programmer, creates from the original set of data-flow equations a
smaller set of equations.  This smaller set yields solutions that can
be used to generate the solutions for the original equations by one
propagation step.

To understand the implemenation of reduction below, pay careful attention
to how the equations, @Seqn@ objects, are copied and what lists that
are on.

The input list @oq@ is scanned.  For most nodes, their original @Seqn@
objects are placed on a new list @nq@.  For complex regions---
headers, dominators, and head of nested acyclic regions---duplicates
of their @Seqn@ objects on @oq@ are placed on @nq@.

The main reduction loop scans the @nq@ lists.  Modifications to equations
are made directly to the objects on @nq@.

A final pass of the @nq@ list extracts only those equations that form
the new, smaller list @new_eqns@.

*/
int
Sprob_tarjan::reducer(Seqn_conslist *new_eqns,Seqn_conslist *oq)
{
    Seqn_consiter qi;		Seqn *q,*q2;
    Seqn_conslist nq;
    Seqn_conslist to_free_iL;
    int iterate=0;
    for(qi.reset(oq);!qi.is_empty();){
	q=qi.step();
	switch((SEQN_type)q->kind){
	  case SEQN_normal:
	  case SEQN_bb:
	  case SEQN_unreachable:
	    break;
	  case SEQN_noncyclic:
	  case SEQN_header:
	  case SEQN_dominator:
	  case SEQN_improper:
	    q=new Seqn(q);
	}
	q->iL=q->L;
	q->ancestor=0;
	q->rcount=0;
	q->init();
	nq.append(q);
    }
    if(nq.is_empty())
      return 1;
    for(qi.reset(&nq);!qi.is_empty();)
      R(&qi);
    if(!qi.is_empty())
      assert(0);
    while(!nq.is_empty()){
	q=nq.pop();
	switch((SEQN_type)q->kind){
	  case SEQN_header:
	  case SEQN_noncyclic:
	  case SEQN_improper:
	    if(q->iL!=&strans_bottom){
		assert(q->number!=1);
		q->L = eval(q->S);
	    }else switch(q->kind){
	      case SEQN_header:
	      case SEQN_improper:
		iterate=1;
	    }
	    assert(q->L->kind!=Sk_top);
	    new_eqns->append(q);
	    break;
	  case SEQN_normal:
	    if(q->rcount&1){
		q2=new Seqn(q);
		q2->init();
		assert(q2->iL);
		if(q2->iL!=&strans_bottom)
		  q2->L=eval(q2->S);
		new_eqns->append(q2);
	    } else {
		if(q->iL && nonterminal(q->iL))
		  delete q->iL;
		q->iL=0;
	    }
	    break;
	  case SEQN_bb:
	    if(q->rcount&1){
		q2=new Seqn(q);
		q2->init();
		assert(q2->iL);
		if(q2->iL!=&strans_bottom)
		  q2->L=eval(q2->S);
		new_eqns->append(q2);
	    }else if(q->iL && nonterminal(q->iL))
	      /* we can't delete it now because eval's
		// may need iL
		*/
	      to_free_iL.append(q);
	    break;
	  case SEQN_dominator:
	    assert(0);
	  case SEQN_unreachable:
	    if(q->iL && nonterminal(q->iL))
	      delete q->iL;
	    q->iL=0;
	    break;
	}
    }
    while(!to_free_iL.is_empty()){
	q=to_free_iL.pop();
	delete q->iL;
    }
    return iterate;
}

int Sprob_tarjan::isolve(Snode_list *,int)
{
    assert(0);
    return 0;
}

int Sprob_tarjan::isolve(Seqn_conslist *l,int n)
{
	int iter, iterate;
	Seqn_conslist nq, el;
	Seqn_consiter qi;
	Seqn *q;

	for(qi.reset(l);!qi.is_empty();){
		q=qi.step();
		el.append(equation(q));
	}
	el.init();
	iterate=reducer(&nq,&el);
	cnt=el.count();
	rcnt=nq.count();
	if(nq.is_empty())
		iter=isolve2(&el,n);
	else{
		iter=isolve2(&nq,iterate ? n : 1);
		for(qi.reset(&nq);!qi.is_empty();)
			qi.step()->V->solved=1;
		if(iter<n){
			el.init();
			propagate(&el);
		}
	}
	while(!nq.is_empty()){
		Seqn *q=nq.pop();
		if(q->iL) delete q->iL;
		delete q;
	}
	while(!el.is_empty())
		el.pop();
	if(check_limit)
	  assert(iter < n);
	return iter;
}

Sprob_tarjan::Sprob_tarjan(CFG_0 *g,int d)
     :Sproblem(g,d)
{
}

/*##
frag(Sprob_tarjan_DEBUG_F,"@Sprob_tarjan_DEBUG::F@")

*/
int
Sprob_tarjan_DEBUG::F(Seqn_consiter *qi)
{
    Seqn *q, *h;
    int c, ch=0;
    print("propagate start\n");
    changed=1;
    switch((SEQN_type)(h=qi->peek())->kind){
      case SEQN_dominator: case SEQN_improper: case SEQN_header:
      case SEQN_noncyclic: case SEQN_bb:
	h->L->_init(h->S);
	h->L->start_int(this,h);
	if(!h->solved()){
	    c=h->L->transform(h, FM_propagate);
	    debug_print(h->S,h->L,0,c);
	    ch|=c;
	}
	changed = 1;
	qi->step();
	while(!qi->is_empty()){
	    q=qi->peek();
	    if(q->highpt==h->S){
		if(q->kind!=SEQN_normal)
		  ch |= F(qi);
		else{
		    qi->step();
		    q->L->_init(q->S);
		    c=q->L->transform(q, FM_propagate);
		    debug_print(q->S,q->L,0,c);
		    changed = q->solved() || c;
		    assert(!q->solved()||!c);
		    ch|=c;
		}
	    }else break;
	}
	h->L->_init(h->S);
	h->L->end_int(this,h);
	if(h->solved()){
	    c=h->L->transform(h,FM_propagate);
	    debug_print(h->S,h->L,0,c);
	    changed = 1;
	    assert(!c);
	    ch|=c;
	}
	break;
      case SEQN_normal:
	qi->step();
	h->L->_init(h->S);
	c=h->L->transform(h,FM_propagate);
	debug_print(h->S,h->L,0,c);
	changed = h->solved() || c;
	assert(!h->solved()||!c);
	ch|=c;
	break;
      case SEQN_unreachable:
	qi->step();
	break;
    }
	print("propagate end\n");
	return ch;
}
/*##
frag(Sprob_tarjan_DEBUG_flow,"@Sprob_tarjan_DEBUG::flow(Seqn_conslist *l)@")
tex @{
This is a version of @flow@ used for debugging.  It calls prints functions
after evaluating the transform function of each equation.
@}

*/
int
Sprob_tarjan_DEBUG::flow(Seqn_conslist *l)
{
    int ch=0, c;
    Seqn_consiter si(l);
    print("flow start\n");
    bb_head=0;
    if(checking){
	while(si.is_empty()==0){
	    Seqn *e=si.step();
	    e->L->_init(e->S);
	    ch |= end_status(e,c=start_status(e,FM_flow));
	    debug_print(e->S,e->L,0,c);
	}
    }else{
	while(si.is_empty()==0){
	    Seqn *e=si.step();
	    if(e->solved()){
		changed=1;
		continue;
	    }
	    e->L->_init(e->S);
	    c |= e->L->transform(e,FM_flow);
	    debug_print(e->S,e->L,0,c);
	    changed = c;
	    ch |= c;
	}
    }
	print("flow end\n");
	return ch;
}
Sprob_tarjan_DEBUG::Sprob_tarjan_DEBUG(CFG_0 *g,int d)
     :Sprob_tarjan(g,d)
{
    debug_reduce=0;
}
Sproblem_DEBUG::Sproblem_DEBUG(CFG_0 *g,int d)
     :Sproblem(g,d)
{
}

/*##
frag(Sproblem_DEBUG_flow,"@Sproblem_DEBUG::flow(Seqn_conslist *l)@")

*/
int
Sproblem_DEBUG::flow(Seqn_conslist *l)
{
	int ch=0, c;
	Seqn_consiter si(l);
	print("flow start\n");
	bb_head=0;
	if(checking){
		while(si.is_empty()==0){
			Seqn *e=si.step();
			e->L->_init(e->S);
			ch |= end_status(e,c=start_status(e,FM_flow));
			debug_print(e->S,e->L,0,c);
		}
	} else {
		while(si.is_empty()==0){
			Seqn *e=si.step();
			if(e->solved()){
				changed=1;
				continue;
			}
			e->L->_init(e->S);
			c |= e->L->transform(e,FM_flow);
			debug_print(e->S,e->L,0,c);
			changed = c;
			ch |= c;
		}
	}
	print("flow end\n");
	return ch;
}

/* access routines */
Snode *Sproblem::ancestor(Snode *n)
{
    if(n->Q)
      return n->Q->ancestor;
    else
      return 0;
}
