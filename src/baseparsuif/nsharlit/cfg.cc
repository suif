/* file "cfg.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#pragma implementation "cfg.h"

#include <stdio.h>

#include "sharlit.h"
#include "cfg.h"

#define MAX_ARITY 1000
#define MAX_EXITS 100

class Back_edge {
  public:
    CFinfo *from_nums,*to_nums;
    Node_index from_node, to_node;
};

static int n_back_edges;
static Back_edge back_edges[MAX_LOOPS];
static void be_sort();
#if 0
/* The following are not currently used, but might be sometime. */
static int compare(CFinfo **,CFinfo **);
static int compare2(CFinfo **, CFinfo **);
static int compare2_aux(CFinfo *,CFinfo *);
#endif
#include <stdlib.h>
static CFinfo **test_order;
static int test_order_hi;

CFG_0::CFG_0()
{
    source=-1;
    sink=-1;
    n_nodes=0;
    base_of_arrays=0;
    interval_order=0;
    numbers=0;
    header_info = 0;
    n_unreachables=0;
    n_irred=0;
    debug_level = 0;
}

CFG_0::~CFG_0()
{
    if(base_of_arrays)
      delete base_of_arrays;
    if(numbers)
      delete numbers;
    if(header_info)
      delete[] header_info;
}

static CFinfo *initial_highpt;

void CFG_0::clear()
{
    if(base_of_arrays){
	delete base_of_arrays;
	base_of_arrays=0;
	interval_order=0;
	modified_order=0;
    }
    if(numbers)
      delete numbers;
    n_nodes=0;
    if(header_info)
      delete header_info;
    n_unreachables=0;
    n_irred=0;
}

void CFinfo::print(FILE *f)
{
  static char *kind_map[] =
    { "unrch", "norml", "bblck", "headr", "irred" };
  Header_info *hi;
  int i;

  fprintf(f,"%d",unique);

  /* the kind of the node */
  fprintf(f," %s", kind_map[_kind - CFGnode_unreachable]);

  /* the header */
  if(highpt)
    fprintf(f," %d",highpt->unique);
  else
    fprintf(f," (no header)");

  switch(_kind)
    {
    case CFGnode_header:
    case CFGnode_irreducible:
      hi = h_info;
      assert(hi->cfi==this);
      fprintf(f," [%d..%d] (",hi->start,hi->end);
      for(i=0;i<hi->other_entrances.ub();i++)
	fprintf(f," %d",hi->other_entrances[i]);
      fputc(')',f);
      break;
    default:
      break;
    }
  fputc('\n',f);
}

void CFG_0::analyze()
{
    int i,j,n,m,infinity;
    CFinfo *snp, *h;
    Back_edge *bep;
    Header_info *hi;

    /* Make sure we have not been here before */
    assert(base_of_arrays==0);

    /* if these fail then you have not initialized source/sink */
    assert(source!=-1);
    assert(sink!=-1);

    /*
    Allocate storage for orderings.  Since we know the number of nodes
      in the graph, we allocate the space for all the arrays in one chunk.
      The member base_of_arrays point to this chunk.  This chunk is split
      into two pieces, pointed to by interval_order and modified_order.
      */
    n_nodes = number_of_nodes();
    base_of_arrays = new  CFinfo*[2*n_nodes];
    interval_order = base_of_arrays;
    modified_order = base_of_arrays+n_nodes;

    /*
    Initialize the control-flow information for each node.
      */
    numbers = new CFinfo[n_nodes];
    initial_highpt = &numbers[source];
    infinity=2*n_nodes+1;
    for(i=0,snp=numbers;i<n_nodes;i++,snp++){
	snp->unique=i;
	snp->dfn = -1;
	snp->sn = infinity;
	snp->node = get_node(i);
	snp->highpt = 0;
	snp->h_info = 0;
	snp->begins_bb = snp->is_fork = snp->is_join = snp->needs_var = 0;
	snp->depth = 0;

	/* Consider all nodes unreachable until marked reachable by dfs
	*/
	snp->_kind = CFGnode_unreachable;
    }

    /*
    Do a depth-first search to find all the back-edges.
      */
    initial_highpt->highpt = 0;
    n=n_nodes;
    m= -1;
    n_back_edges=0;
    dfs(n,m,source);
    assert(n >= 0);
    n_unreachables=n;

    /*
    compute strong-ly connected components.
      Find the high points of all nodes
      At the same time, compute a vector of headers with inner
      loop nodes first.
      At the same time, compute the interval order during
      set_highpt computation.  This is an improvement over Tarjan\'s
      algorithm.
      */
    be_sort();
    initial_highpt->_kind = CFGnode_header;
    header_info = new Header_info[n_back_edges+2];
    n_headers = 0;
    if(n_back_edges)
      {
	test_order = interval_order;
	test_order_hi = 0;
	for(i=0;i<n_back_edges;i++)
	  {
	    CFinfo *hd;

	    bep=&back_edges[i];
	    
	    /*
	    Update information about the header.
	      - Mark it as a header, unless another back_edge has already
	        marked it.
	    - Allocate a Header_info structure for it.
	      */
	    hd=bep->to_nums;
	    switch(hd->_kind)
	      {
	      case CFGnode_unreachable:
	      case CFGnode_bb:
		assert(0);

	      case CFGnode_header:
	      case CFGnode_irreducible:
		/* marked already */
		break;

	      case CFGnode_normal:
		hd->_kind=CFGnode_header;
		break;
	      }
	    if(hd->h_info==0){
	      hd->h_info= &header_info[n_headers++];
	      hd->h_info->cfi = hd;
	    }

	    /* Reach-under computation. */
	    set_highpt(bep->to_node,bep->from_node);
	    if(bep->to_nums->_kind==CFGnode_irreducible)
	      n_irred++;
	  }

	/*
	Allocate header info for the initial node.
	  */
#if 0
	if(initial_highpt->h_info==0)
	  {
	    initial_highpt->h_info = &header_info[n_headers++];
	    initial_highpt->h_info->cfi = initial_highpt;
	  }
#endif
	assert(initial_highpt->h_info);
	header_info[n_headers++].cfi=0;

	test_order[test_order_hi]=initial_highpt;
	test_order_hi++;
	n = n_nodes-n_unreachables;

	if(test_order_hi!=n)
	  {
	    /*
	    the test_order_hi does not equal n then control-flow
	      analysis missed some nodes --- when that happens
		print debugging information.
		  */
	    if(debug_level)
	      for(i=0;i<test_order_hi;i++)
		{
		  fprintf(stderr,"%d: ",i);
		  test_order[i]->print(stderr);
		}
	    assert(0);
	  }
      } else
	assert(0);
    assert(n_headers <= n_back_edges+2);

    j = 0;
    i = 0;
    for(;;)
      {
	hi=&header_info[j++];
	if(hi->cfi==0)
	  break;
	hi->start=i;
	while(interval_order[i]->highpt==hi->cfi)
	  {
	    i++;
	    assert(i<n);
	  }
	hi->end = i-1;
    }
    assert(j <= n_back_edges+2);

	/* Compute nesting depth */
    numbers[source].depth=0;
    assert(header_info[n_headers-1].cfi==0);
    assert(header_info[n_headers-2].cfi==initial_highpt);
    for(i=n_headers-2;i>0;){
	snp = header_info[--i].cfi;
	snp->depth = snp->highpt->depth+1;
    }

	/* compute modified order */
    compute_modified_order(initial_highpt,interval_order,modified_order);

    /* we mark the basic blocks for compatibility reasons */
    h=0;
    n=n_nodes-n_unreachables;
    for(i=0;i<n;i++){
	snp=modified_order[i];
	switch(snp->_kind){
	  case CFGnode_unreachable: case CFGnode_bb:
	    assert(0);
	  case CFGnode_normal:
	    if(snp->begins_bb)
	      h=0;
	    if(h)
	      snp->highpt=h;
	    else{
		h=snp;
		snp->_kind=CFGnode_bb;
	    }
	    break;
	  default:
	    h=0;
	}
    }
}

/*

This function computes the node sequence which is rescanned to find
strongly connected components.  See Tarjan's testing for reducibility paper.

*/

void CFG_0::dfs(int &n,int &m,Node_index node)
{
    int i,n_successors, n_predecessors, has_non_back_edge;
    CFGnode **succp, **predp, *sp;
    Node_index succ;
    CFinfo *node_nums,*succ_nums;
    Back_edge *be;
    node_nums = &numbers[node];
    m=node_nums->dfn=m+1;
    node_nums->_kind=CFGnode_normal;
    succp = successors(node,&n_successors);
    node_nums->needs_var = node_nums->is_fork = n_successors>1;
    predp = predecessors(node,&n_predecessors);
    node_nums->begins_bb = node_nums->is_join = n_predecessors!=1;
    assert(n_successors<MAX_ARITY && n_predecessors<MAX_ARITY);
    has_non_back_edge = 0;
    for(i=0;i<n_successors;i++){
	sp=succp[i];
	succ=sp->unique;
	assert(succ>=0 && succ<n_nodes);
	succ_nums=&numbers[succ];
	if(succ_nums->dfn < 0) {
	    has_non_back_edge = 1;
	    dfs(n,m,succ);
	} else if(succ_nums->sn > n_nodes /* unstacked */){
	    /* edge (node,succ) is a back-edge (frond) */
	    be=&back_edges[n_back_edges];
	    be->from_nums = node_nums;
	    be->from_node = node;
	    be->to_nums = succ_nums;
	    be->to_node = succ;
	    n_back_edges++;
	    assert(n_back_edges<MAX_LOOPS);
	}else if(node_nums->dfn < succ_nums->dfn)
	  /* edge (node,succ) is a reverse (frond) */
	  has_non_back_edge=1;
	else
	  /* edge (node,succ) is a crosslink */
	  has_non_back_edge=1;
	node_nums->needs_var |= succ_nums->is_join;
	succ_nums->begins_bb |= node_nums->is_fork;
    }

    /*
     * If has_non_back_edge is zero then ``node'' is not connected to
     * the exit.  In that case, we need to mark ``node'' as an exit of
     * the outermost region headed by initial_highpt.  We mark ``node''
     *  by pretending that a virtual back-edge emanates from ``node''
     *
     */
    if(!has_non_back_edge){
	be=&back_edges[n_back_edges];
	be->from_nums = node_nums;
	be->from_node = node;
	be->to_nums = initial_highpt;
	be->to_node = initial_highpt->unique;
	n_back_edges++;
	assert(n_back_edges<MAX_LOOPS);
    }
    n=node_nums->sn=n-1;
}

/*

Compute high points.

*/
void
CFG_0::set_highpt(Node_index header,Node_index u)
{
  CFGnode **predp;
  int i,n_predecessors;
  register CFinfo *ui;
  assert(u>=0 && u<n_nodes);
  if(u==header)
    return;
  ui = &numbers[u];
  if(ui->_kind==CFGnode_unreachable)
    return;
  if(ui->highpt!=0){
    set_highpt(header,numbers[u].highpt-numbers);
    return;
  }

  if(ui->sn < numbers[header].sn)
    {
      /*
      Found a irreducible region.
	- Mark it as such
	  - Add ourselves onto the exit list.
	    */
      CFinfo *hi = &numbers[header];
      hi->_kind = CFGnode_irreducible;
      hi->h_info->other_entrances.extend(u);
      return;
    }

  ui->highpt = &numbers[header];
  predp = predecessors(u,&n_predecessors);
  assert(n_predecessors<MAX_ARITY);
  for(i=0;i<n_predecessors;i++)
    set_highpt(header,predp[i]->unique);

  /*
  If we are at an irreducible header, then must search backwards
    from other_entrances too.
      */
  if(ui->_kind==CFGnode_irreducible)
    {
      Node_index *ep,*limit;
      limit = ui->h_info->other_entrances.limit();
      for(ep=ui->h_info->other_entrances.first();ep<limit;ep++)
	set_highpt(header,*ep);
    }

  /* test out alternative way of building interval order */
  if(test_order){
    test_order[test_order_hi] = ui;
    test_order_hi++;
    assert(test_order_hi < n_nodes);
  }
}

void CFG_0::set(CFGnode *src,CFGnode *snk)
{
    source=src->unique;
    sink=snk->unique;
}

/* Back_edge routines */
/*

This is a simple bubble sort.  It might be too slow.  But until the
inefficiency is shown to be an issue, I won't worry about it.

*/
void
be_sort()
{
    int loop_count,change,i;
    Back_edge e1, e2;
    change=0;
    for(loop_count=0;loop_count<n_back_edges;){
	loop_count++;
	for(i=1;i<n_back_edges;i++){
	    e1=back_edges[i-1];
	    e2=back_edges[i];
	    if(e1.to_nums->dfn >= e2.to_nums->dfn)
	      continue;
//	    if(e1.to_nums->dfn>e2.to_nums->dfn)
//	      continue;
//	    if(e1.to_nums->dfn==e2.to_nums->dfn
//	       && e1.from_nums->sn < e2.from_nums->sn)
//	      continue;
	    back_edges[i-1] = e2;
	    back_edges[i] = e1;
	    change++;
	}
	if(change==0)
	  break;
    }
    for(i=1;i<n_back_edges;i++){
	e1=back_edges[i-1];
	e2=back_edges[i];
	if(e1.to_nums->dfn >= e2.to_nums->dfn)
	  continue;
//	if(e1.to_nums->dfn>e2.to_nums->dfn)
//	  continue;
//	if(e1.to_nums->dfn==e2.to_nums->dfn
//	   && e1.from_nums->sn < e2.from_nums->sn)
//	  continue;
	assert(0);
    }
}

#if 0
/* The comparison used in quicksort */
int
compare(CFinfo **x,CFinfo **y)
{
    register CFinfo *x_nums= *x;
    register CFinfo *y_nums= *y;
    register CFinfo *x_highpt=x_nums->highpt;
    register CFinfo *y_highpt=y_nums->highpt;
    if(x_highpt==0){
	    /* X is source node */
	return y_highpt==0 ? 0 : 1;
    }
    if(y_highpt==0)
      return -1;
    if(x_highpt->dfn > y_highpt->dfn)
      return -1;
    if(x_highpt==y_highpt){
	if(x_nums->sn<y_nums->sn)
	  return -1;
	else
	  return x_nums->sn > y_nums->sn;
    }else
      return 1;
}

/* comparison used to give us modified interval order */
int compare2(CFinfo **x,CFinfo **y)
{
    return compare2_aux(*x,*y);
}

int compare2_aux(CFinfo *x_nums,CFinfo *y_nums)
{
    register CFinfo *x_highpt=x_nums->highpt;
    register CFinfo *y_highpt=y_nums->highpt;
    if(x_highpt==y_highpt){
	    /* x and y in same region */
	if(x_nums->sn<y_nums->sn)
	  return -1;
	else
	  return x_nums->sn > y_nums->sn;
    }
    if(x_highpt==0)
	  /* X is source node */
      return -1;
    if(y_highpt==0)
	  /* Y is source node */
      return 1;
    if(x_nums==y_highpt)
	  /* y in x\'s region */
      return -1;
    if(y_nums==x_highpt)
	  /* x in y\' region */
      return 1;
    if(x_highpt->depth>y_highpt->depth)
      return compare2_aux(x_highpt,y_nums);
    else
      return compare2_aux(x_nums,y_highpt);
}
#endif

Header_info *CFG_0::find_header(CFinfo *h)
{
    int i;
    switch(h->_kind){
      case CFGnode_header:
      case CFGnode_irreducible:
	for(i=0;i<n_headers;i++)
	  if(header_info[i].cfi==h)
	    return &header_info[i];
	assert(0);
      default:
	assert(0);
    }
    return 0;
}

CFinfo **CFG_0::compute_modified_order(CFinfo *h,register CFinfo **io,
				     register CFinfo **mo)
{
    register Header_info *hi = find_header(h);
    register int i;
    CFinfo *ui;
    *mo = hi->cfi;
    mo++;
    for(i=hi->start;i<=hi->end;i++){
	ui=io[i];
	switch(ui->_kind){
	  case CFGnode_header:
	  case CFGnode_irreducible:
	    mo=compute_modified_order(ui,io,mo);
	    break;
	  case CFGnode_normal:
	    *mo = io[i];
	    mo++;
	    break;
	  case CFGnode_bb:
	  case CFGnode_unreachable:
	    assert(0);
	}
    }
    return mo;
}

/* CFG_0_reversed implemenation */
CFG_0_reversed::CFG_0_reversed(CFG_0 *f)
{
  forward_graph = f;
  numbers = NULL;
}

CFG_0_reversed::~CFG_0_reversed()
{
  /*
  if(base_of_arrays)
    delete base_of_arrays;
  delete[] numbers;
  assert(header_info==NULL);
  */
}

void CFG_0_reversed::analyze()
{
  int i, j, n;
  CFinfo *cfi, *cfi2;

  /* compute information from forward analysis */
  assert(forward_graph->already_analyzed());

  /* if these fail then you have not initialized source/sink */
  assert(source!=-1);
  assert(sink!=-1);

  n_nodes = forward_graph->number_of_nodes();
  base_of_arrays = modified_order = new CFinfo*[n_nodes];
  interval_order = NULL;

  numbers = new CFinfo[n_nodes];
  cfi = numbers;
  cfi2 = forward_graph->numbers;
  for(i = 0; i < n_nodes; i++)
    {
      switch(cfi2->_kind)
	{
	case CFGnode_unreachable:
	  *cfi = *cfi2;
	  break;

	case CFGnode_normal:
	case CFGnode_bb:
	case CFGnode_header:
	case CFGnode_irreducible:
	  cfi->unique = cfi2->unique;
	  cfi->node = cfi2->node;
	  cfi->highpt = NULL;
	  cfi->h_info = NULL;
	  cfi->is_join = cfi2->is_fork;
	  cfi->is_fork = cfi2->is_join;
	  cfi->needs_var = cfi2->begins_bb;
	  cfi->begins_bb = cfi2->needs_var;
	  cfi->_kind = cfi->begins_bb ? CFGnode_bb : CFGnode_normal;
	  break;
	}
      cfi++;
      cfi2++;
    }

  n_unreachables = forward_graph->n_unreachables;
  n = n_nodes - n_unreachables;
  j = n-1;
  for(i = 0; i < n; i++)
    {
      modified_order[i] = &numbers[forward_graph->modified_order[j]->unique];
      j--;
    }
  assert(j == -1);
}

/* CFG_1 implemenation */

CFG_1::CFG_1():
  nodes(CFG_1_INITIAL_SIZE),
  forward_edge_index(nodes.size),
  backward_edge_index(nodes.size),
  raw_edges(2*nodes.size)
{
  n_forward_edges = n_backward_edges = 0;
  edges_computed = 0;
  no_control_flow_analysis = 0;
}

CFG_1::~CFG_1()
{
  // The implementation of the x_array does not delete
  // the data explicitly.  remove the nodes
  int i;
  for (i=0; i< edges.ub(); i++) {
    //    delete edges[i];
  }
  for (i=0; i< nodes.ub(); i++) {
    delete (Snode *)nodes[i];
  }
  delete[] n_forward_edges;
  delete[] n_backward_edges;
}

/*

The function CFG_1::Analyze computes edges.  Two auxillary functions,
sort_from and sort_to, are used to compute the require information.

*/

static int sort_from(const void *v1, const void *v2)
{
  const CFG_1_edge *e1 = (CFG_1_edge *)v1;
  const CFG_1_edge *e2 = (CFG_1_edge *)v2;
  return (e1->from->unique - e2->from->unique);
}

static int sort_to(const void *v1, const void *v2)
{
  const CFG_1_edge *e1 = (CFG_1_edge *)v1;
  const CFG_1_edge *e2 = (CFG_1_edge *)v2;
  return (e1->to->unique - e2->to->unique);
}

void CFG_1::analyze()
{
  int i, j, k;

  /* compute forward edge list */
  qsort(raw_edges.first(), raw_edges.hi, sizeof(CFG_1_edge), sort_from);
  n_forward_edges = new int[nodes.hi];
  j = 0;
  for(i=0; i<nodes.hi; i++)
    {
      CFGnode *node = nodes[i];

      /* nodes with unique successors don't need a successor vector */
      if(forward_edge_index[i]==CFG_EDGE_UNIQUE)
	{
	  n_forward_edges[i] = 1;
	  continue;
	}
      
      forward_edge_index[i] = edges.hi;
      while(j<raw_edges.hi && raw_edges[j].from == node)
	{
	  edges.extend(raw_edges[j].to);
	  j++;
	}
      n_forward_edges[i] = j-forward_edge_index[i];
    }

  /* compute backward edge list */
  qsort(raw_edges.first(), raw_edges.hi, sizeof(CFG_1_edge), sort_to);
  n_backward_edges = new int[nodes.hi];
  k = 0;
  for(i=0; i<nodes.hi; i++)
    {
      CFGnode *node = nodes[i];

      /* nodes with unique successors don't need a successor vector */
      if(backward_edge_index[i]==CFG_EDGE_UNIQUE)
	{
	  n_backward_edges[i] = 1;
	  continue;
	}

      backward_edge_index[i] = edges.hi;
      while(k<raw_edges.hi && raw_edges[k].to == node)
	{
	  edges.extend(raw_edges[k].from);
	  j++;
	  k++;
	}
      n_backward_edges[i] = j-backward_edge_index[i];
    }

  raw_edges.clear();
  edges_computed=1;

  /* now do control-flow analysis */
  if(!no_control_flow_analysis)
    CFG_0::analyze();
}

int CFG_1::number_of_nodes()
{
  return nodes.hi;
}

CFGnode *CFG_1::get_node(Node_index i)
{
  return nodes[i];
}

/*

We assume that user does not enter a node more than once.
This is checked in CFG_0::analyze

*/
int CFG_1::enter(CFGnode *new_node)
{
  assert(!edges_computed);

  new_node->unique = nodes.hi;
  nodes.extend(new_node);
  forward_edge_index.extend(CFG_EDGE_UNINIT);
  backward_edge_index.extend(CFG_EDGE_UNINIT);
  return new_node->unique;
}

CFGnode **CFG_1::successors(Node_index i,int *n)
{
  int edge_list = forward_edge_index[i];
  assert(edges_computed);

  /*
    If edge_list is negative then the next node is the one
      immediately following nodes[i].
      */
      
  if ((*n = n_forward_edges[i]))
    {
      if(edge_list == CFG_EDGE_UNIQUE)
	return &nodes[i+1];
      else
	return &edges[edge_list];
    }
  else
    return NULL;
}

CFGnode **CFG_1::predecessors(Node_index i,int *n)
{
  int edge_list = backward_edge_index[i];
  assert(edges_computed);

  /*
    If edge_list is negative then the next node is the one
      immediately preceding nodes[i].
      */
      
  if ((*n = n_backward_edges[i]))
    {
      if(edge_list == CFG_EDGE_UNIQUE)
	return &nodes[i-1];
      else
	return &edges[edge_list];
    }
  else
    return NULL;
}

/*

Put an edge between u and v.

*/
void CFG_1::link(Node_index u, Node_index v)
{
  int *u_edge_index, *v_edge_index;

  /* Check that u and v are in range */
  assert(0<=u && u<nodes.hi);
  assert(0<=v && v<nodes.hi);

  u_edge_index = &forward_edge_index[u];
  v_edge_index = &backward_edge_index[v];

  if(u+1 == v && *u_edge_index == CFG_EDGE_UNINIT
     && *v_edge_index == CFG_EDGE_UNINIT)
    {
      /* special case */
      *u_edge_index = CFG_EDGE_UNIQUE;
      *v_edge_index = CFG_EDGE_UNIQUE;
    }
  else
    {
      /* If a unique edge connected either u or v, remove it
      and represent it using non-unique representation. */

      switch(*u_edge_index)
	{
	case CFG_EDGE_UNIQUE:
	  *u_edge_index = CFG_EDGE_MULTIPLE;
	  backward_edge_index[u+1] = CFG_EDGE_MULTIPLE;
	  link(u,u+1);
	  break;

	default:
	  *u_edge_index = CFG_EDGE_MULTIPLE;
	  break;
	}

      switch(*v_edge_index)
	{
	case CFG_EDGE_UNIQUE:
	  forward_edge_index[v-1] = CFG_EDGE_MULTIPLE;
	  *v_edge_index = CFG_EDGE_MULTIPLE;
	  link(v-1,v);
	  break;

	default:
	  *v_edge_index = CFG_EDGE_MULTIPLE;
	  break;
	}

      raw_edges.extend(CFG_1_edge(nodes[u],nodes[v]));
    }
}

int CFG_1::number_of_edges()
{
  assert((edges.hi%2)==0);
  return edges.hi/2;
}
