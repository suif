/* file "conslist.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  CONS Cell List Implementation */

#pragma implementation "conslist.h"

#include <suif1/misc.h>
#include <suif1/glist.h>
#include "conslist.h"


/*  Make a copy of another conslist.  This function relies on the behavior
    of other conslist functions to actually duplicate the list elements.
    The consiter::step() function returns just the data pointer of each
    element in the list, and the conslist::append() function automatically
    creates a new CONS cell to hold a data pointer.  */

conslist::conslist(const conslist &l)
{
    consiter li(&l);
    while (!li.is_empty()) {
	append(li.step());
    }
}



/*  Remove and delete the CONS cell at the start of the list.  Returns the
    data pointer from that cell.  */

void * conslist::pop()
{
    cons_e *e = (cons_e *)glist::pop();
    void *d = e->data;
    delete e;
    return d;
}



/*  Reverse the elements in the list.  */

void conslist::reverse(conslist *ol)
{
    consiter li(ol);
    assert(is_empty());
    while (!li.is_empty()) {
    	push(li.step());
    }
}



/*  Remove and delete all of the elements in the list.  */

void conslist::mk_empty()
{
    while(!is_empty()) {
	pop();
    }
}
