/* file "sstring.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Segmented Strings (Definitions) */

#ifndef SSTRING
#define SSTRING

#include <suif1/glist.h>

class String_seg;

/*##

A segmented string is a singly-linked list of string segments.  It is
a subclass of {\tt glist} so that it can use the generic list
routines.

*/
struct String: public glist {
	int line_no;
	const char *filename;
	String(int ln=0,const char *fn=0){ line_no=ln; filename=fn; }
	String(char *);
	void store(char c);
	void print(FILE *);
	void append(String *);
	String_seg *last() { return (String_seg*)tail_e; }
};

const int segsize=100;

struct String_seg: public glist_e {
	int length;
	char text[segsize];
	String_seg() { length=0; }
	int willfit() { return this!=0 && length<segsize; }
};

/*##
frag(String_iter,"A string iterator")

*/
struct String_iter: public glist_iter {
	int next_index;
	void reset(String *s) { next_index=0; glist_iter::reset((glist*)s); }
	String_seg *current() { return (String_seg*)cur; }
	String_iter(String *s) { reset(s); }
	String_iter() { next_index=0; }
	char step();
	int is_empty()
		{ return (current()==0 || next_index>=current()->length)
				&& glist_iter::is_empty(); }
};

/*##
frag(,"finish")

*/

#endif
