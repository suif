/* file "symbol.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#pragma interface
// This may look like C code, but it is really -*- C++ -*-

/* Symbol Table (Definitions) */

#ifndef SYMBOL
#define SYMBOL
#include <suif1/misc.h>
#include <suif1/glist.h>
#include <suif1/mtflist.h>
#include <suif1/hash.h>
#include "conslist.h"
#include "util.h"
class Symbol;
class String;
class SV_dflow;
class o_stream;
typedef String C_code;
class Scope;
class SV_rule;
class sc_code;
class sc_expr;
class SV_const;
class SV_node;
class Sym_val;

/*##
Enumeration: Symbol Kinds
*/
enum SV_tags
{
    TAG(SV_keyword),
    TAG(SV_node), TAG(SV_rule), TAG(SV_dflow),
    TAG(SV_type), TAG(SV_const), TAG(SV_cell), TAG(SV_error),
    TAG(SV_path)
  };

/*
Class: Symbol Table

A symbol table is a stack of scopes, which are grouping of symbols,
groups that corresponding to what would think of as a scope in a block
structured language.  Looking up a symbol is what one might expect:
Each scope is searched from the top of the stack downward until the
symbol is found.

*/
struct Symbol_table: public glist {
    Symbol_table();
    void push(Scope *s);
    Scope *pop();
    Symbol *enter(Symbol *);
    Symbol *lookup(const char *,int=0,int=0);
    Scope *top();
};

/*
Class: "Scopes"

This class defines the abstract interface to a scope.  It also
provides facilities that linke scopes into lists.

*/
class Scope: private glist_e {
  public:
    Scope() {}
    Scope *next_scope() { return (Scope *)glist_e::next_e; }
    virtual Symbol *enter(Symbol *);
    virtual Symbol *lookup(const char *,int=0);
    int has(Symbol *);
};

/*
Class: Hashed Scopes

A hashed scope speedups lookups by storing a scope in a hashed table.

*/
#define SMALL_TABLE_SIZE 8
#define HASH_TABLE_SIZE 1024

class Hashed_scope:public Scope
{
    hash_table table;
    Hashed_scope *prev;
  public:
    Hashed_scope(Hashed_scope * =0,int =HASH_TABLE_SIZE);
    virtual ~Hashed_scope();
    Symbol *enter(Symbol *);
    Symbol *lookup(const char *,int=0);
};

/*
 Enumeration: Code Generation context

Experimental code generation routines; not used really
*/

enum CG_context
{
    CGC_header, CGC_code, CGC_decl_head,
    CGC_decl_tail, CGC_defn_head, CGC_defn_tail
  };


class Sym_val
{
  public:
    const SV_tags kind;
    Sym_val(SV_tags k): kind(k) {}
    virtual ~Sym_val() { }
    Sym_val *is(int k) { return (this!=0 && kind==k) ? this : 0 ; }
    Sym_val *check(int k) { assert(is(k)); return this; }
    virtual void declare(o_stream *,o_stream *,const char *,sc_expr * = 0);
    virtual void code_gen(CG_context, o_stream *);
    virtual void print(o_stream *);
};

class Symbol:public hash_e
{
friend class Scope;
friend class Hashed_scope;
friend class Symbol_list;
    Scope *sc;
  public:
    const char *name;
    Sym_val *value;
    Symbol(char *,Sym_val * = 0);
    Symbol(Symbol *);
    ~Symbol();
    inline int loose()
      {
	  return this!=0 && sc==0;
      }
    Sym_val *is(int a)
      {
	  if(this!=0 && value!=0 && value->kind==a)
	    return value;
	  else
	    return 0;
      }
    Sym_val *check(int a)
      {
	  assert(this!=0 && value!=0 && value->kind==a);
	  return value;
      }
    SV_node *node_value();
  private:
    Symbol(const char *n,int s):hash_e(s)
      {
	  name=n;
	  value=0;
      }
};
extern Symbol error_sym;	/* the error Symbol */

/*##
frag(Symbol_lists,"Lists of Symbols")
tex @{
These classes define lists of {\tt symbols}s.
The original code for the generic lists are reused by providing
inline functions.  Only the {\tt pop} member function cannot
be done this way.
@}

*/
class Symbol_list: public Scope {
friend class Symbol_iter;
    conslist l;
  public:
    int is_empty() { return l.is_empty(); }
    void push(Symbol *sp) { l.push(sp); }
    Symbol *pop() { return (Symbol *)l.pop(); }
    void append(Symbol *sp) { l.append(sp); }
    void append(Symbol_list *);
    Symbol *enter(Symbol *);
    Symbol *lookup(const char *,int);
    int contains(Symbol *);
    int count() { return l.count(); }
    void operator += (Symbol_list &);
    int non_empty_intersect(Symbol_list &);
    Symbol_list();
    Symbol_list(Symbol *);
    virtual ~Symbol_list();
    void print(FILE *);
};

struct Symbol_iter: public consiter {
    void reset(Symbol_list *sl) { consiter::reset(&sl->l); }
    Symbol_iter() {}
    Symbol_iter(Symbol_list *sl) { reset(sl); }
    Symbol *step() { return (Symbol *)consiter::step(); }
};

/*##
frag(keyword_values,"A keyword value")

*/
struct SV_keyword: public Sym_val{
	const int token;
	SV_keyword(int k): Sym_val(TAG(SV_keyword)), token(k) {}
};

/*##
frag(qbinder,"@class Q_binder@")

*/
class Q_binder {
  public:
    Symbol *Q_name;
    Symbol *V_name;
    Symbol *L_name;
    Symbol *S_name;
    Q_binder(Symbol *Q,Symbol *V,Symbol *L,Symbol *S)
      {
	  Q_name=Q;
	  L_name=L;
	  V_name=V;
	  S_name=S;
      }
    int check();
    void bind_and_initialize(o_stream *,SV_dflow *,Symbol *,const char *,
			     const char * = 0);
    void bind_and_initialize2(o_stream *,SV_dflow *,Symbol *,const char *,
			     const char *);
    void bind_and_initialize3(o_stream *,SV_dflow *,Symbol *,const char *,
			     const char *);
};

/*##
frag(rule_value,"A rule value")
tex @{

Rules are used in building the reducer.

@}

*/

enum SVR_kinds {
    TAG(SVR_none),TAG(SVR_create),TAG(SVR_unit),
    TAG(SVR_absorb),TAG(SVR_star),TAG(SVR_meet),
    TAG(SVR_max)
};

class SV_rule: public Sym_val {
  public:
    Scope *symbols;
    const int rule_kind;

    SV_rule(int k=TAG(SVR_none))
      :Sym_val(TAG(SV_rule)),rule_kind(k)
      {
      }

    SV_rule *is(int t)
      {
	  return (this && rule_kind==t) ? this : 0;
      }

    SV_rule *check(int t)
      {
	  assert(this && rule_kind==t);
	  return this;
      }

    virtual void generate(SV_node *);
    virtual void gen2(o_stream *,SV_dflow *);
    virtual int operator==(SV_rule &);
    virtual void debug_print(FILE *,int = -1);
    virtual Symbol *operator[](int);
    virtual Symbol *next_sym(int);
    virtual int reducible(int);
    virtual int length();
    virtual SV_node *target();
};

class SVR_create: public SV_rule {
  public:
    Q_binder *target_names;
    Symbol *target_type;
    SV_node *target_node;
    C_code *creator;
    SVR_create(Q_binder *,Symbol *,C_code *);
    virtual void gen2(o_stream *,SV_dflow *);
    virtual int operator==(SV_rule &);
    virtual void debug_print(FILE *,int = -1);
    virtual Symbol *next_sym(int);
    virtual int reducible(int);
    virtual int length();
    virtual SV_node *target();

};
class SVR_unit: public SV_rule {
  public:
    Q_binder *target_names;
    Symbol *target_type;
    SV_node *target_node;
    Q_binder *qnames;
    Symbol *src_type;
    C_code *creator;
    SVR_unit(Q_binder *,Symbol *,Q_binder *,Symbol *,C_code *);
    virtual void gen2(o_stream *,SV_dflow *);
    virtual int operator==(SV_rule &);
    virtual void debug_print(FILE *,int = -1);
    virtual Symbol *operator[](int);
    virtual Symbol *next_sym(int);
    virtual int reducible(int);
    virtual int length();
    virtual SV_node *target();
};

class SVR_absorb: public SV_rule {
  public:
    Q_binder *target_names;
    Symbol *target_type;
    SV_node *target_node;
    Q_binder *qnames;
    Symbol *src_type;
    C_code *creator;
    SVR_absorb(Q_binder *,Symbol *,Q_binder *,Symbol *,C_code *);
    virtual void gen2(o_stream *,SV_dflow *);
    virtual int operator==(SV_rule &);
    virtual void debug_print(FILE *,int = -1);
    virtual Symbol *operator[](int);
    virtual Symbol *next_sym(int);
    virtual int reducible(int);
    virtual int length();
    virtual SV_node *target();
};

class SVR_star: public SV_rule {
  public:
    Q_binder *target_binder;
    Symbol *target_type;
    SV_node *target_node;
    C_code *creator;
    SVR_star(Q_binder *,Symbol *,C_code *);
    virtual void generate(SV_node *);
    virtual int operator==(SV_rule &);
    virtual void debug_print(FILE *, int = -1);
};

class SVR_meet: public SV_rule {
  public:
    Q_binder *lhs_binder;
    Symbol *lhs;
    SV_node *lhs_node;
    Q_binder *rhs_binder;
    Symbol *rhs;
    C_code *creator;
    SVR_meet(Q_binder *,Symbol *,Q_binder *,Symbol *,C_code *);
    virtual void generate(SV_node *);
    virtual int operator==(SV_rule &);
    virtual void debug_print(FILE *,int = -1);
};


/*##
frag(SV_rule_lists,"Lists of rules")

*/
class SV_rule_list: public conslist {
  public:
    SV_rule_list() {}
    void push(SV_rule *r) { conslist::push(r); }
    void append(SV_rule *r) { conslist::append(r); }
};
class SV_rule_iter: public consiter {
  public:
    SV_rule_iter() {}
    SV_rule_iter(SV_rule_list *rl) { reset(rl); }
    SV_rule *step() { return (SV_rule*)consiter::step(); }
};

class PP_item: public glist_e
{
public:
    SV_rule *rule;
    int dot;
    PP_item(SV_rule *r,int d=0)
      {
	  dot=d;
	  rule=r;
      }
    int operator==(PP_item &);
    int operator!=(PP_item &rhs) { return !(*this==rhs); }
    void print(FILE *);
};

class PP_iset: public glist
{
public:
    PP_iset() {}
    void enter(PP_item *i);
    int contains(PP_item *i);
    int operator==(PP_iset &);
    void print(FILE *);
};

class PP_iset_iter: public glist_iter {
  public:
    PP_iset_iter(PP_iset *s):glist_iter(s)
      {
      }
    PP_iset_iter() {}
    PP_item *step() { return (PP_item *)glist_iter::step(); }
};

class PP_state 
{
public:
    PP_iset items;
    int n_edges;
    int *out_edges;
    int processed;
    PP_state(int);
    int operator==(PP_state &);
    void print(FILE *);
    void add(SV_rule *);
    void add(SV_rule_list *);
    void next_set(Symbol_list *);
    PP_state *apply(Symbol *,int *);
};

const int PP_mach_size = 100;
class PP_mach 
{
public:
    int n_states;
    int n_nodes;
    PP_state *states[PP_mach_size];
    PP_mach(int);
    void print(FILE *);
    int add(PP_state *);
    void generate(o_stream *,SV_dflow *);
};

/*##
frag(type_structure,"Introduction to symbol table value structures")
tex @{
In order to better generate better C++ code, we try to model the
type system.
There are base types, record types, function types, and pointer types.
Currently the only base types are integers and characters.
Record types are unordered lists of members which are name-type pairs.
They may have an opaque extension, i.e. stuff in C++ that we are
unwilling to parse.
@}

*/
enum type_kind {TY_base,TY_named,TY_record,TY_function,TY_pointer,TY_enum};
struct SV_type: public Sym_val {
    const int kind;
    SV_type(int k):Sym_val(TAG(SV_type)),kind(k) {};
    int is(int k) { return this!=0 && k==kind; }
    SV_type *check(int k) { assert(this!=0 && k==kind); return this; };
    virtual SV_type *unnamed_type();
};

/*##
frag(lists_of_types,"Lists of type values")

*/

     class Type_list: public conslist {
       public:
	 Type_list() {}
	 void append(SV_type *t) { conslist::append(t); }
     };

class Type_iter:public consiter {
  public:
    Type_iter() {}
    Type_iter(Type_list *tl) { reset(tl); }
    SV_type *step() { return (SV_type *)consiter::step(); }
};

/*##
frag(type_base,"The basic types")

*/
enum base_types { BT_void, BT_int, BT_char };
struct ty_base: public SV_type {
	int type;
	ty_base(int bt):SV_type(TY_base) { type=bt; }
	void declare(o_stream *,o_stream *,const char *,sc_expr * = 0);
	void code_gen(CG_context,o_stream *);
	void print(o_stream *);
};
extern ty_base *void_type, *int_type, *char_type;

/*##
frag(type_subclasses,"The types")

*/
class ty_named:public SV_type {
public:
	Symbol *type_name;
	SV_type *type;
	ty_named(Symbol *n,SV_type *t):SV_type(TY_named)
		{ type_name=n; type=t; }
	void declare(o_stream *,o_stream *,const char *, sc_expr * = 0);
	void code_gen(CG_context,o_stream *);
	void print(o_stream *);
	SV_type *unnamed_type();
};
class ty_record;

class ty_record_list: public conslist
{
  public:
    void append(ty_record *t) { conslist::append(t); }
    ty_record *pop() { return (ty_record *)conslist::pop(); }
    ty_record *top() { return (ty_record *)conslist::top(); }
    ty_record *last() { return (ty_record *)conslist::last(); }
    ty_record_list(ty_record *t) { push(t); }
    ty_record_list() {}
    ty_record_list(const ty_record_list &l):conslist(l) {}
    ~ty_record_list();
    int contains(ty_record *t);
};

class ty_record_list_iter: public consiter {
  public:
    void reset(ty_record_list *l) { consiter::reset((conslist*)l); }
    ty_record_list_iter() {}
    ty_record_list_iter(ty_record_list *sl) { reset(sl); }
    ty_record *step() { return (ty_record *)consiter::step(); }
    ty_record *peek() { return (ty_record *)consiter::peek(); }
};	

class ty_record: public SV_type {
  public:
    Symbol *type_name;
    ty_record_list bases;
    Hashed_scope members;
    Type_list type_list;
    Symbol_list member_list;
    C_code *opaque;
    ty_record(Symbol *,ty_record *,C_code * = 0);
    ty_record(Symbol *,const ty_record_list &,C_code * = 0);
    virtual ~ty_record();
    void add_member(char *,SV_type *);
    void declare(o_stream *,o_stream *,const char *, sc_expr * = 0);
    void code_gen(CG_context,o_stream *);
};

class ty_function:public SV_type {
  public:
    ty_record *container;
    SV_type *return_type;
    Type_list formals;
    ty_function():SV_type(TY_function) { container=0; }
    ty_function(SV_type *ret,ty_record *c = 0):SV_type(TY_function){
	container=c;
	return_type=ret;
    }
    void add_formal(SV_type *t) { formals.append(t); }
    void declare(o_stream *,o_stream *,const char *, sc_expr * = 0);
    void code_gen(CG_context,o_stream *);
};

class ty_pointer:public SV_type {
  public:
    SV_type *type;
    ty_pointer(SV_type *t):SV_type(TY_pointer) {type=t;}
    void declare(o_stream *,o_stream *,const char *, sc_expr * = 0);
    void code_gen(CG_context,o_stream *);
    void print(o_stream *);
};
class ty_enum:public SV_type {
public:
	Symbol *type_name;
	Symbol_list member_list;
	int count;
	ty_enum(Symbol *n):SV_type(TY_enum) {type_name=n; count=0;}
	int add_number(Symbol *);
	int add_number(char *);
	void print(o_stream *);
	void declare(o_stream *,o_stream *,const char *, sc_expr * = 0);
	void code_gen(CG_context,o_stream *);
};

/*##
frag(cells,"Cells or Variabels (@sv_cell@)")

*/
class SV_cell: public Sym_val {
public:
	int is_arg;
	void *value;
	SV_type *type;
	SV_cell(SV_type *t,int arg=0):Sym_val(TAG(SV_cell)) { type=t; is_arg=arg; }
	virtual void declare(o_stream *,o_stream *,const char *,sc_expr * = 0);
};

/*##
frag(structured_c,"Non-opaque representations of code")
tex @{
These classes are different from @C_code@ which are opaque.
The class @sc_list2@ is used internally by the class @sc_list@.
@}

*/
struct sc_code: public glist_e {
	sc_code() {}
        virtual ~sc_code() {}
	virtual void print(o_stream *,o_stream *);
};
struct sc_list2: public glist {
	sc_list2() {}
	sc_code *push(sc_code *s) { return (sc_code *)glist::push(s); }
	sc_code *pop() { return (sc_code *)glist::pop(); }
	void append(sc_code *s) { glist::append(s); }
};
struct sc_list: public sc_code {
	Symbol_list symbols;
	sc_list2 list;
	sc_code *push(sc_code *s) { return list.push(s); }
	sc_code *pop() { return list.pop(); }
	void append(sc_code *s) { list.append(s); }
	void print(o_stream *,o_stream *);
};
struct sc_iter: public glist_iter {
	void reset(sc_list2 *l) { glist_iter::reset(l); }
	void reset(sc_list *l) { reset(&l->list); }
	sc_iter(sc_list2 *l) { reset(l); }
	sc_iter(sc_list *l) { reset(l); }
	sc_iter() {}
	sc_code *step() { return (sc_code *)glist_iter::step(); }
};
struct sc_opaque: public sc_code {
	C_code *stuff;
	sc_opaque(C_code *s) { stuff=s; }
	void print(o_stream *,o_stream *);
};

/*##
frag(expressions,"Expression nodes")

*/
struct sc_expr: public sc_code {
	virtual int priority();
	virtual SV_type *type_of();
};
struct sc_elist: public glist {
	sc_elist() {}
	sc_expr *push(sc_expr *s) { return (sc_expr *)glist::push(s); }
	sc_expr *pop() { return (sc_expr *)glist::pop(); }
	void append(sc_expr *s) { glist::append(s); }
};
struct sc_eiter: public glist_iter {
	void reset(sc_elist *l) { glist_iter::reset(l); }
	sc_eiter(sc_elist *l) { reset(l); }
	sc_eiter() {}
	sc_expr *step() { return (sc_expr *)glist_iter::step(); }
};

/*##
frag(switch_statement,"The switch statement")

*/
struct sc_case: public glist_e {
	sc_elist labels;
	sc_code *code;
	sc_case() {}
};
struct sc_cases: public glist {
	sc_case *push(sc_case *c) { return (sc_case *)glist::push(c); }
	sc_case *pop() { return (sc_case *)glist::pop(); }
	void append(sc_case *c) { glist::append(c); }
};
struct sc_citer: public glist_iter {
	void reset(sc_cases *l) { glist_iter::reset(l); }
	sc_citer(sc_cases *l) { reset(l); }
	sc_citer() {}
	sc_case *step() { return (sc_case *)glist_iter::step(); }
};
struct sc_switch: public sc_code {
	sc_expr *test;
	sc_cases cases;
	sc_switch(sc_expr *t){ test=t; }
	sc_case *add(sc_case *c) { cases.append(c); return c;}
	void print(o_stream *,o_stream *);
};

/*##
frag(constants,"Constants")

*/
class SV_const:public Sym_val {
public:
	SV_type *const type;
	SV_const(SV_type *);
	virtual void declare(o_stream *,o_stream *,const char *,sc_expr * =0);
};
class CN_function: public SV_const {
public:
	int closed;
	Symbol_list formals;
	sc_list body;
	CN_function(ty_function *t):SV_const(t) {closed=0;}
	void add_formal(char *s)
		{ assert(!closed); formals.append(new Symbol(s)); }
	void close_def();
	virtual void declare(o_stream *,o_stream *,const char *,sc_expr * = 0);
};
class cn_integer: public SV_const {
public:
	int value;
	cn_integer(int v):SV_const(int_type) { value=v; }
	virtual void declare(o_stream *,o_stream *,const char *,sc_expr * = 0);
};

/*##
frag(atomic_expression_nodes,"Atomic Expression Nodes")

*/
struct sc_const: public sc_expr {
	SV_const *constant;
	sc_const(SV_const *c) { constant=c; }
	sc_const(int i) { constant=new cn_integer(i); }
	void print(o_stream *,o_stream *);
	int priority();
	SV_type *type_of();
};
struct sc_id: public sc_expr {
	Symbol *id;
	sc_id(Symbol *i) { id=i; }
	void print(o_stream *,o_stream *);
	int priority();
	SV_type *type_of();
};

/*##
frag(select_construct,"Field selector expression (@sc_select@)")

*/
struct sc_select: public sc_expr {
	sc_expr *structure;
	Symbol *field;
	sc_select(sc_expr *,char *);
	void print(o_stream *,o_stream *);
	SV_type *type_of();
	int priority();
};

/*##

frag(dflow_value,"A {\em data flow problem} value")
     tex @{
	 This symbol value represents a data flow problem.
	   The members @PP@, @SS@, and @VV@ store information, respectively, about
	     the @P@, @S@, and @V@ parts of a data flow problem specification.

@}

*/
    
class SV_dflow: public Sym_val {
  public:
    int n_nodes;
    int new_version;
    Symbol *const name;
    ty_record *solver_type;
    ty_record *SS;/* nodes */
    ty_record *LL;/* flow functions */
    ty_record *VV;/* flow values */
    ty_enum *kinds;
    ty_pointer *solver_pointer_type;
    ty_pointer *SS_pointer_type;
    ty_pointer *VV_pointer_type;
    C_code *flow_map;
    C_code *new_var_body;
    C_code *copy_var_body;
    C_code *meet_body;
    Symbol_list extra_classes;
    Symbol_list nodes;
    Symbol *identity_function;
    int unique;
    SV_rule_list rules;
    SV_dflow(Symbol *,ty_record *);
    virtual ~SV_dflow();
    void create_solver_type(C_code *);
    void define_VV(Symbol *,C_code *);
    void define_LL(C_code *);
    void define_SS(C_code *,ty_record *);
    void define_new_var(C_code *c)
      {
	  new_var_body=c;
      }
    void define_copy_var(C_code *c)
      {
	  copy_var_body=c;
      }
    void define_meet(C_code *c)
      {
	  meet_body = c;
      }
    void define_node(Symbol *);
    void define_identity(Symbol *);
    virtual void generate();
    virtual void declare_kinds();
    virtual void declare_generated_functions();
    virtual void generate_simplifier();
    virtual void generate_flow_map();
    virtual void code_gen(CG_context, o_stream *);
    char *gensym(const char * = 0);
    int has_rules() { return !rules.is_empty(); }
};

class SV_dflow_parsing: public SV_dflow {
  public:
    SV_dflow_parsing(Symbol *,ty_record *);
    virtual void declare_generated_functions();
    virtual void generate_simplifier();
    void generate_STAR();
    void generate_MEET();
    void generate_NONTERMINAL();
};

class SV_dflow_switches: public SV_dflow_parsing {
  public:
    virtual void declare_generated_functions();
    SV_dflow_switches(Symbol *,ty_record *);
    void generate_simplifier();
    virtual void generate_flow_map();
};

/*##
A node object

This object defines the flow function, the in and out processing function
that gets associated with each node.

*/
class SV_node: public Sym_val {
  public:
    int index;
    int nonterminal;
    int is_identity;
    Symbol *sym;
    SV_dflow *df;
    C_code *transformer;
    C_code *local_def;
    C_code *flow_function;
    C_code *process_function;
    void init(Symbol *s,C_code *ldef,C_code *a,C_code *fl=0, C_code *pr=0)
      {
	  sym=s;
	  transformer=a;
	  local_def=ldef;
	  flow_function=fl;
	  process_function=pr;
	  nonterminal=0;
	  is_identity=0;
      }
    SV_node(Symbol *s,C_code *ldef,C_code *a,C_code *fl=0, C_code *pr=0)
      :Sym_val(TAG(SV_node))
      {
	  init(s,ldef,a,fl,pr);
      }
    SV_node(SV_tags k,Symbol *s,C_code *ldef,C_code *a,
	    C_code *fl=0, C_code *pr=0)
      :Sym_val(k)
      {
	  init(s,ldef,a,fl,pr);
      }
    void gen(Symbol *,SV_dflow *);
    void gen_func(Symbol *,SV_dflow *);
    void declare2(Symbol *,const char *);
    void code_gen(CG_context,o_stream *);
};

/*
A path object

This corresponds to a flow function for a path.
*/
class SV_path: public SV_node {
  public:
    Symbol_list first_set;
    SV_rule_list rules[TAG(SVR_max)];
    char *unit_table,*create_table,*absorb_table;
    SV_rule *add_rule(SV_rule *);
    SV_path(Symbol *s,C_code *ldef,C_code *a,C_code *fl=0, C_code *pr=0)
      :SV_node(TAG(SV_path),s,ldef,a,fl,pr)
	{
	    unit_table=0;
	    create_table=0;
	    absorb_table=0;
	}
};
    
/*##
frag(,"finish")

*/
#endif
