/* file "lex.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Lexical Analyser (Definitions) */
#ifndef LEX
#define LEX

#include "sstring.h"
#include "symbol.h"
class o_stream;

class i_stream
{
    friend int yylex();
    i_stream *next;
    int loose_symbols;
protected:
    int nlines;
    int skipped_nl;
    int special(o_stream *&);
    int filter(o_stream *, int =0);
    int identifier();
    int get_string();
    int get_code();
    void get_nested(String *);
    void get_quoted(String *);
    void comment(char);
public:
    const char *name;
    int yylex();
    i_stream(i_stream *nxt=0)
      { nlines=1; next=nxt; loose_symbols=0; skipped_nl=0; }
    virtual ~i_stream();
    virtual int get();
    virtual void unget(char);
    void top(o_stream *);
    void panic(char *);
    void errmsg(char *...);
    void warning(char *...);
    void loose() { loose_symbols=1; }
    void tight() { loose_symbols=0; }
    i_stream *pop();
};

class f_i_stream: public i_stream
{	// a file input stream
    FILE *fp;
public:
    f_i_stream(const char *n);
    f_i_stream(FILE *fp,char *n);
    virtual ~f_i_stream();
    int get();
    void unget(char);
};

class s_i_stream: public i_stream
{	// a string input stream
    String_iter si;
    char saved_c;
public:
    s_i_stream(String *);
    int get();
    void unget(char);
};

class c_i_stream: public i_stream
{
    const char *cp;
    char saved_c;
public:
    c_i_stream(const char*);
    int get();
    void unget(char);
};

struct o_stream
{
  const char *name;
  int indent_suppress;
  int indent_level;
  int indent_width;
  int nlines;

  virtual void put_string(String *);
  virtual void put_code(String *);
  virtual void put_string(const char *);
  virtual void nl();
  virtual void indent(int= -1);
  virtual void put_line_info(int,const char *);
  virtual void put_line_info();
  virtual void update_line_info(char);
  virtual void put(char);
  virtual void printf(char *fmt...);

  o_stream() { indent_suppress = 0; indent_level=0; indent_width=8; nlines=1; }
  virtual ~o_stream();
};

class f_o_stream: public o_stream
{
    friend void close_streams();
    int compare;
    int olength;		// length of the old file
    FILE *fp;
    f_o_stream *next;
public:
    f_o_stream(const char *);
    f_o_stream(FILE *,const char *);
    ~f_o_stream();
    void put(char);
};

class s_o_stream: public o_stream
{
public:
    String *s;
    s_o_stream(String *st) { s=st; name="internal o_stream"; }
    ~s_o_stream() { s=0; }
    void put(char);
};

class null_o_stream: public o_stream
{
public:
    null_o_stream() {};
    void put(char);
};

#endif

