/* file "sharlit_test.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef SHARLIT_TEST
#define SHARLIT_TEST

#pragma interface

class CFinfo;

/*

For every node in the CFG, Sprob_switches associates one n_Seqn object
to hold information used to solve the data-flow problem.  Let u
designate a node in the CFG.  Then fields of u\'s n_Seqn object contain
the following meanings:

 ancestor: A dominator of the node from which path simplification has
found a flow function from the entry of to the entry of u.  The node u
does not have ancestor when ancestor==0.

 keep: If on indicates that the node should become part of the reduced
CFG.

 stop_compression: If on, this flag suppresses path compression on
this node.  We turn this flag on to avoid repeatedly trying to
compress path when we know that further simplification will not yield
a non-bottom result.

 oL: The original flow function assigned to the node u by _FLOW_MAP.
That flow function represents the data-flow effect from the entry to
the exit of u.

 aL: Flow functions computed during path simplification.

 L: Flow functions for use during n_iterate.  If ancestor==0 then
n_iterate should compute the input flow value then apply L to obtain
the output flow value of u.  If ancestor!=0 then copy the output flow
value of the ancestor before applying L.

 oV: usually zero.  If non-zero, this field points to the output flow
value of the node u.

*/

class n_Seqn {
  public:
    Strans *L;
    Strans *aL;
    Strans *oL;
    Svar *oV;
    Snode *ancestor;
    unsigned int keep:1, stop_compression:1, on_reduced_list:1;
};

typedef unsigned char Sharlit_flag;
typedef signed char Sharlit_state;
/*enum CFGnode_kinds;*/

class Sprob_switches: public Sprob_tarjan_DEBUG
{
protected:

  int n_states;
  Sharlit_flag *_copiable;
  Sharlit_state *_times_able;
  Sharlit_state *_meet_able;

  void check_node(Snode *,CFinfo **, n_Seqn **);

public:
  n_Seqn *equations;
  int path_debug;
  Sprob_switches(CFG_0 *g,int d);
  virtual ~Sprob_switches();
  virtual void _init_tables();
  virtual Strans *_TIMES_xs(int,Strans *,Snode *,Strans *,Snode *);
  virtual Strans *_CREATE(int);
  virtual Strans *_UNIT(int,Strans *,Snode *);
  virtual Strans *_ABSORB(Strans *,Strans *,Snode *);
  virtual Strans *_MEET_ux(Strans *,Strans *,Snode *,int = 0);
  virtual Strans *_STAR_u(Strans *,Snode *);
  virtual int NONTERMINAL(Strans *);
  virtual Strans *_FLOW_MAP(Snode *,CFGnode_kinds k);
  virtual int solve(Seqn **qa,int nq,int n);
  virtual int simplify(Seqn **,int *,Seqn **,int);
  virtual int iterate(Seqn **l,int num_q,int n);
  virtual int propagate3(Seqn **,int);

  /* internal, but not private, routines */
  virtual int simplify_region(int,int,Seqn **);
  void update(Snode *q,Strans *P,Strans *Q);
  void path_compress(Snode *);
  int flow3(Seqn **l,int num_q,Svar *);
  void init_eqn(Seqn **,int);
  virtual void link(Snode *,Snode *);
  virtual void start_iteration(Svar *);
  virtual int get_srcs(Svar **&,Snode *);

  /* Functions for new interface */
  virtual void n_initialize_equations();
  virtual int n_propagate();
  virtual int n_one_pass(CFinfo **,int,Svar *);
  virtual int n_iterate(CFinfo **,int,int);
  virtual int n_solve(int);
  virtual void n_walk_once();
  virtual int n_simplify_region(int,int,CFinfo **);
  virtual int n_is_cycle(CFinfo *,CFinfo *);
  virtual void n_keep(CFinfo *);
  virtual void n_link(CFinfo *,CFinfo *);
  virtual void n_update(CFinfo *,Strans *P,Strans *Q);
  virtual Strans *n_eval(Snode *,Snode *);
  virtual Strans *n_eval(Snode *);
  virtual void n_path_compress(Snode *);
  virtual void n_simplify();
  virtual CFinfo **n_reduce(CFinfo **,int *,int *);
  virtual void print_graph(CFinfo **,int);
  virtual void must_have_var(Snode *);
  virtual Svar *n_get_var(Snode *);
};


// Definitions that help with implementing lattices:
// 
// The "defines" should be used as tags values in lattice elements.  The
// function Lattice_meet function indicates the action to take when
// taking the meet of two lattie elements.  It performs the
// "lattice-theoretic" part of the meet.  It tells you that the result
// should be top (Lma_top) or should be bottom (Lma_bottom); or it tells
// you that the result equals the right operand of the meet (Lma_right)
// or equals the left operand (Lma_left); or it tells you that the
// elements have to combined in a manner specific to the particular
// lattice (Lma_meet).

#define LATTICE_BOTTOM (-1)
#define LATTICE_TOP (0)
#define LATTICE_OTHER (1)

enum Lattice_meet_action
{
    Lma_top,
    Lma_bottom,
    Lma_right,
    Lma_left,
    Lma_meet
  };

inline Lattice_meet_action Lattice_meet(int left,int right)
{
    switch(left){
      case LATTICE_BOTTOM:
	return Lma_bottom;
      case LATTICE_TOP:
	return (right==LATTICE_TOP) ? Lma_top : Lma_right;
    }
    switch(right){
      case LATTICE_BOTTOM:
	return Lma_bottom;
      case LATTICE_TOP:
	return (left==LATTICE_TOP) ? Lma_top : Lma_left;
    }
    return Lma_meet;
}


#endif
