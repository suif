/* file "lex.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* lexical analyser (implementation) */

/*##
frag(,"includes")

*/
#include <suif1/misc.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h> /* for getpid(), link(), and unlink() */

#include "lex.h"
#include "parse.h"

/*##
frag(yylex,"The token reader")
tex @{
Each input stream can have a token reader or {\tt yylex} routine.
This is the base-level token reader.
@}

*/
int
i_stream::yylex()
{
    int c;
    skipped_nl=0;
    while((c=get())!=EOF){
	switch(c){
	    
	    /*##
	      frag(scan_comments,"Scan comments")
		
		*/
	  case '/':
	    switch(c=get()){
	      case '*': case '/':
		comment(c);
		break;
	      default:
		unget(c);
		return '/';
	    }
	    break;
	    
	    /*##
	      frag(white_space,"Skip over white space")
		24~
		  */
	  case ' ': case '\t': case '\f':
	    continue;
	  case '\n':
	    skipped_nl++;
	    continue;
	  case '<': case '-':
	  case '+':
	  case '*':
	  case '(': case ')': case '!': case ':': case ';':
	  case ',': case '=': case '[': case ']': case '>':
	  case '.':
	    return c;
	  case '&':
	    if((c=get())=='&')
	      return AND;	// right arrow
	    else{
		unget(c);
		return '&';
	    }
	  case '{': /* } for sam nesting */
	    unget('{'); /* } */
	    return get_code();
	  case '"': /*"*/
	    return i_stream::get_string();
	  default:
	    if(isalpha(c)){
		unget(c);
		return identifier();
	    }else if(isprint(c))
				errmsg("noise characters (%c)",c);
			else
				errmsg("noisy characters (0%o)",c);
		}
	}
	return EOF;
}

/*##
frag(comment,"Scan a comment")
tex @{
The following procedure scans for a regular C++ style comment.
It assumes that the initial ``{\tt /}'' has already been read.
Scanning for both type of comments stops either on the regular
delimiters or an end of file.  The end of file condition is
an error but no message will be printed since {\tt yylex}
or the parser should catch it.
@}

*/
void
i_stream::comment(char comment_type)
{
	int c;
	switch(comment_type){
	case '*':
		for(;;){
			c=get();
			if(c==EOF)
				break;
			if(c=='*'){
				if((c=get())=='/')
					return;
				else unget(c);
			}
		}
		break;
	case '/':
		do{
			c=get();
		}while(c!=EOF && c!='\n');
		break;
	}
}

/*##
frag(identifer,"Parse an identifer")

*/
const int id_size_lim=100;
extern Symbol_table *symtab;
int
i_stream::identifier()
{
	char id[id_size_lim+1];
	char *cp=id;
	int c;
	Symbol *sp;
	while((c=get())!=EOF && (isalpha(c)||isdigit(c)||c=='_'
					||c=='.')){
		if(cp-id>=id_size_lim)
			break;
		*cp++=c;
	}
	if(cp-id>=id_size_lim)
		panic("id too large");
	*cp=0;
	unget(c);
	if(loose_symbols){
		yylval.y_sym=new Symbol(id);
		return ID;
	}
	sp=symtab->lookup(id);
	if(TAG_IS(SV_keyword,sp))
	  return TAG_CHECK(SV_keyword,sp)->token;
	else{
		yylval.y_sym=sp==0 ? new Symbol(id) : sp;
		return ID;
	}
}

/*##
frag(get_string,"Parse a doubly-quoted string")

*/
const int string_lim = BUFSIZ;
int
i_stream::get_string()
{
	char s[string_lim+1];
	char *cp=s;
	int c;
 	while((c=get())!=EOF && c!='"'){	/*"*/
		if(cp-s>=string_lim)
			break;
		*cp++=c;
	}
	if(cp-s>=string_lim)
		panic("string too big");
	*cp=0;

	cp=new char[strlen(s)+1];
	strcpy(cp,s);
	yylval.y_cstring=cp;
	return STRING;
}

/*##
frag(get_code,"Get a code block")
tex @{
Get a block of C code from the input stream.
The code is delimited by braces and must be
properly nested with respect to braces, and quotes.
Besides that constraint, no other parts of the C
syntax is checked.
The top-level enclosing braces are not considered as part
of the C code block.
@}

*/
int
i_stream::get_code()
{
	String *s=new String(nlines,name);
	get();	// get open brace.
	get_nested(s);
	yylval.y_string=s;
	return CBLOCK;
}
void
i_stream::get_nested(String *s)
{
	char c;
	for(;;){
		c=get();
		switch(c){
		case EOF:
			errmsg("unexpected EOF (improper nesting)");
			return;
		case '}':
			return;
		case '{':
			s->store('{');
			get_nested(s);
			s->store('}');
			break;
		case '"':
			s->store(c);
			get_quoted(s);
			break;
		default:
			s->store(c);
			break;
		}
	}
}
void
i_stream::get_quoted(String *s)
{
	int c;
 	while((c=get())!=EOF && c!='"')	/*"*/ {
		s->store(c);
		if(c=='\\')
			s->store(get());
	}
	if(c!=EOF)
		s->store(c);
}

/*##
frag(get,"Default for virtual get")
tex @{
The base class cannot be used in its underived form.
@}

*/
int i_stream::get()
{
	assert(0);
	return EOF;
}
void
i_stream::unget(char)
{ assert(0); }

/*##
frag(pop,"Popping off the input stream stack")

*/
i_stream *
i_stream::pop()
{
	i_stream *new_top=next;
	next=0;
	return new_top;
}

/*##
frag(destructor,"Destructor/cleanup routine")

*/
i_stream::~i_stream()
{
	assert(next==0);
	delete (char *)name;
}

/*##
frag(file_i_stream_constructor,"Create and destroy a file input stream")
tex @{
One can make an {\tt f\_i_stream} given a file name, or a stdio
.
@}

*/
f_i_stream::f_i_stream(const char *n)
{
	name=n;
	if((fp=fopen(n,"r"))==0){
		perror(n);
		exit(1);
	}
}
f_i_stream::f_i_stream(FILE *f,char *n)
{
	name=n;
	fp=f;
}
f_i_stream::~f_i_stream()
{
	fclose(fp);
}

/*##
frag(f_i_stream_get,"Get a character from an {\tt f\_i_stream}")

*/
int
f_i_stream::get()
{
	int c=getc(fp);
	if(c=='\n')
		nlines++;
	return c;
}
void
f_i_stream::unget(char c)
{
	if(c=='\n')
		nlines--;
	ungetc(c,fp);
}

/*##
frag(s_i_stream_constructor,"Construct a segmented string input stream")

*/
s_i_stream::s_i_stream(String *t):si(t)
{
	saved_c=0;
}

/*##
frag(c_i_stream_constructor,"Create C string streams")

*/
c_i_stream::c_i_stream(const char *s)
{
	cp=s==0 ? "" : s;
	saved_c=0;
}

/*##
frag(s_i_stream_get,"Getting a character from an {\tt s\_i_stream}")

*/
int
s_i_stream::get()
{
	char c;
	if(saved_c){
		c=saved_c;
		saved_c=0;
		if(c=='\n')
			nlines++;
		return c;
	}
	if(si.is_empty())
		return EOF;
	c=si.step();
	if(c=='\n')
		nlines++;
	return c;
}
void
s_i_stream::unget(char c)
{
	if(saved_c)
		panic("double unsave");
	saved_c=c;
	if(c=='\n')
		nlines--;
}

/*##
frag(c_i_stream_get,"Getting a character from a {\tt c\_i_stream}")

*/
int
c_i_stream::get()
{
	char c;
	if(saved_c){
		c=saved_c;
		saved_c=0;
		if(c=='\n')
			nlines++;
		return c;
	}
	if((c= *cp)){
		cp++;
		if(c=='\n')
			nlines++;
		return c;
	}
	return EOF;
}
void
c_i_stream::unget(char c)
{
	if(saved_c)
		panic("double unsave");
	saved_c=c;
	if(c=='\n')
		nlines--;
}

/*##
frag(error_routines,"Error message routines")

*/
extern char *cmdnam;
void
i_stream::panic(char *msg)
{
	fprintf(stderr,"%s:(line %d of %s) %s\n",cmdnam,nlines,name,msg);
	abort();
}

void i_stream::errmsg(char *msg...)
{
  extern int n_errors;
  va_list ap;

  va_start(ap,msg);
  fprintf(stderr,"%s:(line %d of %s) ",cmdnam,nlines-skipped_nl,name);
  vfprintf(stderr, msg, ap);
  putc('\n',stderr);
  n_errors++;
}

void i_stream::warning(char *msg...)
{
  extern int n_warnings;
  va_list ap;

  va_start(ap,msg);
  fprintf(stderr,"%s:(line %d of %s) Warning: ",cmdnam,nlines-skipped_nl,name);
  vfprintf(stderr, msg, ap);
  putc('\n',stderr);
  n_warnings++;
}

/*
Create a file output stream.
*/
static f_o_stream *olist;

f_o_stream::f_o_stream(const char *n)
{
	struct stat sbuf;
	name=n;
	compare=(stat(n,&sbuf)==0);
	olength= compare ? sbuf.st_size : 0;
	if(olength==0)
		compare=0;
	if((fp=fopen(n,compare ? "r+" : "w"))==0){
		perror(n);
		exit(1);
	}
	next=olist;
	olist=this;
}

/*##
frag(f_o_stream_destructor,"Destroying a file output stream")

*/
f_o_stream::~f_o_stream()
{
	if(compare||olength!=0){
		long ndx;
		ndx=ftell(fp);
		if(ndx<olength){
			char tmpname[30];
			FILE *tfp;
			long lp;
			fprintf(stderr,"%s\n",name);
			sprintf(tmpname,"mesh%05lu",(unsigned long)getpid());
			if((tfp=fopen(tmpname,"w"))==0){
				perror(tmpname);
				exit(1);
			}
			fseek(fp,0L,0);
			for(lp=ndx;lp>0;lp--)
				putc(getc(fp),tfp);
			fclose(tfp);
			unlink(name);
			link(tmpname,name);
			unlink(tmpname);
		}
	}
	fclose(fp);
}
f_o_stream::f_o_stream(FILE *f,const char *n)
{
	name=n;
	fp=f;
	compare=0;
}

/*##
frag(f_o_stream_put,"Put a character onto a {\tt f\_o_stream}")

*/
void
f_o_stream::put(char c)
{
	update_line_info(c);
	if(fp==0)
		return;
	if(compare){
		int oc;
		oc=getc(fp);
		if(oc==c)
			return;
		fseek(fp,oc!=EOF ? -1L:0L,1); // back up a byte
		compare=0;	// just write from now on.
		fprintf(stderr,"%s\n",name);
	}
	putc(c,fp);
}

/*##
frag(s_o_stream_put,"Put a character onto a {\tt s\_o_stream}")

*/
void
s_o_stream::put(char c)
{
	update_line_info(c);
	s->store(c);
}

/*##
frag(put_string,"Put a string onto the stream")

*/
void
o_stream::put_string(const char *s)
{
	while(*s)
		put(*s++);
	if(s[-1]=='\n')
		indent();	
}
void
o_stream::put_string(String *noise)
{
	s_i_stream *str=new s_i_stream(noise);
	int c;
	while((c=str->get())!=EOF)
		put(c);
}
void
o_stream::put_code(String *noise)
{
	put_line_info(noise->line_no,noise->filename);
	put_string(noise);
	nl();
}
void
o_stream::nl()
{
	put('\n');
	indent();
}

/*##
frag(o_stream_line_control,"Line control")

*/
extern int line_control;
void
o_stream::put_line_info(int ln,const char *fn)
{
	char buf[BUFSIZ];
	if(line_control && ln && fn){
		sprintf(buf,"#line %d \"%s\"\n",ln,fn);
		assert(strlen(buf)<BUFSIZ);
		put_string(buf);
	}
}
void
o_stream::put_line_info()
{
	put_line_info(nlines,name);
}
void
o_stream::update_line_info(char c)
{
	if(c=='\n')
		nlines++;
}

/*##
frag(indent,"Creating indentation")

*/
void
o_stream::indent(int n)
{
	if(indent_suppress>0){
		indent_suppress--;
		return;
	}
	if(n<0)
		n=indent_level;
	if(n>0){
		int indentation=n*indent_width;
		while(indentation>=8){ put('\t'); indentation -= 8; }
		while(indentation>0)   put(' '), indentation--;
	}
}

/*##
frag(null_stream_put,"Put on a {\tt null\_stream}")

*/
void
null_o_stream::put(char c)
{
	/* don't do anything, except count lines*/
	update_line_info(c);
}

/*##
frag(o_stream_put,"Default put routine")
tex @{
This has to be defined or else we may pull in the {\tt o_stream}
functions in the {\tt libC.a}.
@}

*/
void
o_stream::put(char)
{ assert(0); }
o_stream::~o_stream()
{}

void o_stream::printf(char *fmt...)
{
  va_list ap;
  va_start(ap,fmt);

  for(; fmt[0]; fmt++)
    if(fmt[0]!='%')
      if(fmt[0]=='\\')
	{
	  put(fmt[1]);
	  fmt++;
	}
      else
	put(fmt[0]);
    else
      {
	switch(fmt[1])
	  {
	  case 0:
	    goto end_loop;

	  default:
	    break;

	  case '%':
	    put('%');
	    break;

	  case 's':
	    put_string(va_arg(ap,char*));
	    break;

	  case 'C':
	    put_code(va_arg(ap,String *));
	    break;

	  case '<':
	    if(strncmp(fmt+1,"<String*>",9)==0)
	      put_string(va_arg(ap,String *));
	    break;

	  }
	fmt++;
      }
 end_loop:
  return;
}

