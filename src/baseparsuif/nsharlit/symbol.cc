/* file "symbol.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Symbol Table (Implementations) */
#pragma implementation

#include <stdio.h>
#include <string.h>

#include <suif1/misc.h>
#include "lex.h"
#include "symbol.h"

/*##

This routine calculates a hash value from a string.  The hash value is
used as a {\em signature} to speed up string searches, see
@hash_table::lookup@.  A symbol is placed in the bucket whose index is
its hash value modulo the size of the hash table.

*/
static int
RawHash(const char *s)
{
	register int sum = 0;
	register int i = 0;
	while(*s!=0)
		sum += (++i)*(0377&*s++);
	return(sum);
}

Symbol_table::Symbol_table()
{
    /* nothing to do */
}
void Symbol_table::push(Scope *s)
{
    glist::push((glist_e*)s);
}
Scope *Symbol_table::pop()
{
    return (Scope *)glist::pop();
}
Symbol *Symbol_table::enter(Symbol *s)
{
    return ((Scope *)head_e)->enter(s);
}

Symbol *Symbol_table::lookup(const char *n,int sig,int shallow)
{
  Scope *t=(Scope *)head_e;
  Symbol *s=0;
  if(shallow)
    return t->lookup(n,sig);
  while(t){
    s=t->lookup(n,sig);
    if(s) break;
    t=t->next_scope();
  }
  return s;
}

Scope *Symbol_table::top()
{
#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
  return (Scope*)head_e;
#else
  void *ptr = head_e;
  return (Scope*)ptr;
#endif
}

/*##
frag(inclusion,"Inclusion test")
tex @{
This routine checks to see if the argument symbol, {\tt s} can
be found in this hash table.
@}

*/
int
Scope::has(Symbol *s)
	{ return s!=0 && s->sc==this; }

/*##
frag(error_sym,"@error_sym@")
tex @{
The global variable, @error_sym@, plays the role of a {\em bottom} value.
It can't be entered into tables; many routine return it to signify error.
@}

*/
Symbol error_sym("%%ERROR%%", new Sym_val(TAG(SV_error)));

/*

The first constructor creates a Symobl given a String while thesecond
constructor creates a copy of another Symbol with an undefined value.

*/
Symbol::Symbol(char *n,Sym_val *v)
     :hash_e(RawHash(n)), name(const_string(n))
{
	value=v; sc=0;
}
Symbol::Symbol(Symbol *s):hash_e(s->signature), name(const_string(s->name))
{
	value=0; sc=0;
}
Symbol::~Symbol()
{
	delete (char *)name;
	delete value;
}

/*##
frag(hashed_scope_constructor,"Build a new hashed scope")

*/
static
int symbol_hash_compare(Symbol *l,Symbol *r)
{
	return l->signature==r->signature && strcmp(l->name,r->name)==0;
}
Hashed_scope::Hashed_scope(Hashed_scope *prev_scope,int sz)
		:table((hash_compare)symbol_hash_compare,sz)
{
	prev=prev_scope;
}

/*##
frag(Hashed_scope_destruct,"Delete a @Hashed_scope@")

*/
Hashed_scope::~Hashed_scope()
{
}

/*##
frag(default_scope_virtuals,"Default for {\tt scope}'s virtuals")

*/
Symbol *Scope::enter(Symbol *) { assert(0); return 0; }
Symbol *Scope::lookup(const char *,int) { assert(0); return 0; }

/*##
frag(Symbol_table_access,"Enter and Lookup Symbols in a {\tt Symbol\_table}")

*/

/*##
frag(hashed_scope_enter,"Enter a Symbol into the table")
tex @{

We check for two unusual conditions before adding symbol.  The symbol
must have a cleared {\tt next} member and it must not be in some scope
already, i.e. it must be a {\em loose} symbol.  It is a fatal error if
one of these conditions is not true.

@}

*/
Symbol *
Hashed_scope::enter(Symbol *s)
{
	if(s==&error_sym)
		return s;
	table.enter((hash_e*)s);
	assert(s->sc==0);
	s->sc=this;
	return s;
}


/*##
frag(hashed_scope_lookup,"Lookup a symbol in the table")

*/
Symbol *
Hashed_scope::lookup(const char *n,int sig)
{
	if(sig==0)
		sig=RawHash(n);
	Symbol t(n,sig);
	Symbol *s=(Symbol *)table.lookup(&t);
	t.name=0;	// to stop the destructor from freeing it.
	return s;
}

Symbol_list::Symbol_list()
{
}

Symbol_list::Symbol_list(Symbol *s)
{
	enter(s);
}

/*##
frag(Symbol_list_destroy,"@Symbol_list::~Symbol_list@")

*/
Symbol_list::~Symbol_list()
{
	while(!l.is_empty())
		l.pop();
}

/*##
frag(Symbol_list_lookup,"Lookup and enter Symbols in a Symbol list")
tex @{
This functions are provided to fulfil the behavior of the
base class virtuals.  This is why {\tt enter} is not an in-line
even though it is so simple.
@}

*/
Symbol *
Symbol_list::enter(Symbol *s)
{
	assert(s->sc==0);
	s->sc=this;
	push(s);
	return s;
}
Symbol *
Symbol_list::lookup(const char *n,int sig)
{
	if(sig==0)
		sig=RawHash(n);
	Symbol_iter si(this);
	while(!si.is_empty()){
		Symbol *s=si.step();
		if ((int)s->signature==sig && strcmp(n,s->name)==0)
			return s;
	}
	return 0;
}

/*##
frag(Symbol_list_contains,"@Symbol_list::contains@")

*/
int
Symbol_list::contains(Symbol *s)
{
    Symbol_iter si(this);
    while(!si.is_empty()){
	if(si.step()==s)
	  return 1;
    }
    return 0;
}

void
Symbol_list::operator += (Symbol_list &right)
{
    Symbol_iter si(&right);
    Symbol *s;
    while(!si.is_empty()){
	s=si.step();
	if(!contains(s))
	    append(s);
    }
}

int
Symbol_list::non_empty_intersect(Symbol_list &right)
{
    Symbol_iter si(&right);
    Symbol *s;
    while(!si.is_empty()){
	s=si.step();
	if(contains(s))
	  return 1;
    }
    return 0;
}

/*##
frag(Symbol_list_append_list,"@Symbol_list::append@")

*/
void
Symbol_list::append(Symbol_list *l)
{
	Symbol_iter si(l);
	while(!si.is_empty())
		append(si.step());
}

/*##
frag(svr_unit_constructor,"@SVR_unit::SVR_unit@")

*/

SVR_create::SVR_create(Q_binder *n,Symbol *t,C_code *c):SV_rule(TAG(SVR_create))
{
    TAG_ASSERT(SV_path,t);
    target_names=n;
    target_type=t;
    target_node=(SV_node*)t->value;
    creator=c;
}

SVR_unit::SVR_unit(Q_binder *tn,
		   Symbol *t, Q_binder *qn,
		   Symbol *s, C_code *c):SV_rule(TAG(SVR_unit))
{
    TAG_ASSERT(SV_path,t);
    target_names=tn;
    target_type=t;
    target_node=(SV_node *)t->value;
    qnames=qn;
    src_type=s;
    creator=c;
}
SVR_absorb::SVR_absorb(Q_binder *tn,
		       Symbol *t, Q_binder *n,
		       Symbol *s, C_code *c):SV_rule(TAG(SVR_absorb))
{
    TAG_ASSERT(SV_path,t);
    target_names=tn;
    target_type=t;
    target_node=(SV_node *)t->value;
    qnames=n;
    src_type=s;
    creator=c;
}
SVR_star::SVR_star(Q_binder *tb,Symbol *t, C_code *c):SV_rule(TAG(SVR_star))
{
    target_node=TAG_CHECK(SV_path,t);
    target_type=t;
    target_binder=tb;
    creator=c;
}

SVR_meet::SVR_meet(Q_binder *lb,
		   Symbol *l,Q_binder *rb,Symbol *r,C_code *c):SV_rule(TAG(SVR_meet))
{
    lhs_node=TAG_CHECK(SV_path,l);
    lhs_binder=lb;
    lhs=l;
    rhs_binder=rb;
    rhs=r;
    creator=c;
}

int SV_rule::operator==(SV_rule &)
{
    assert(0);
    return 0;
}

int SVR_create::operator==(SV_rule &r)
{
    if(TAG_IS(SVR_create,&r))
      return TAG_CHECK(SVR_create,&r)->target_type==target_type;
    return 0;
}

int SVR_unit::operator==(SV_rule &r)
{
    if(TAG_IS(SVR_unit,&r)){
	SVR_unit *rhs=(SVR_unit *)&r;
	return rhs->target_type==target_type && rhs->src_type==src_type;
    }
    return 0;
}

int SVR_absorb::operator==(SV_rule &r)
{
    if(TAG_IS(SVR_absorb,&r)){
	SVR_absorb *rhs=(SVR_absorb *)&r;
	return rhs->target_type==target_type && rhs->src_type==src_type;
    }
    return 0;
}

int SVR_star::operator==(SV_rule &r)
{
    if(TAG_IS(SVR_star,&r)){
	SVR_star *rhs=(SVR_star *)&r;
	return rhs->target_type==target_type;
    }
    return 0;
}

int SVR_meet::operator==(SV_rule &r)
{
    if(TAG_IS(SVR_meet,&r)){
	SVR_meet *r2=(SVR_meet *)&r;
	return r2->lhs==lhs && r2->rhs==rhs;
    }
    return 0;
}

/*##
frag(generate_function,"@SV_rule::generate@")

*/
void
SV_rule::generate(SV_node *)
{
    assert(0);
    return;
}

SV_node *Symbol::node_value()
{
    assert(is(TAG(SV_path)) || is(TAG(SV_node)));
    return (SV_node *)value;
}

/*##
frag(add_rule,"@SV_node::add_rule@")
tex @{

As we add the rule, we check for duplicate rules and track a some
interesting information.  First, adding a rule means that the this
@SV_node@ object represents a nonterminal.  Second, the while
statement scans through the list of already added rules for a
duplicate of @new_rule@.  If so, we return @0@, an error indication.
Third, we compute the {\em first set}, the set of symbols that start a
production and is used later in parser generation.

@}

*/
SV_rule *
SV_path::add_rule(SV_rule *new_rule)
{
    Symbol *s;
    int k=new_rule->rule_kind;
    SV_rule_iter ri(&rules[k]);
    SV_rule *r;
    nonterminal = 1;
    while(!ri.is_empty()){
	r=ri.step();
	if(*r==*new_rule)
	  return 0;
    }
    rules[k].append(new_rule);
    switch(k){
      case TAG(SVR_unit):
	s=((SVR_unit *) new_rule)->src_type;
	break;
      case TAG(SVR_absorb):
	s=((SVR_absorb *)new_rule)->src_type;
	break;
      default:
	return new_rule;
    }
    if(!first_set.contains(s))
      first_set.push(s);
    return new_rule;
}

/*##
frag(sv_rule_print,"@SV_rule::debug_print@")

*/
void
SV_rule::debug_print(FILE *,int)
{ assert(0); }
void
SVR_create::debug_print(FILE *f,int ndx)
{
    assert(ndx>=-1 && ndx <=0);
    fprintf(f,"%s <- ",target_type->name);
    if(ndx==0)
      putc('.',f);
}
void
SVR_unit::debug_print(FILE *f,int ndx)
{
    assert(ndx >= -1 && ndx <= 1);
    fprintf(f,"%s <- ",target_type->name);
    if(ndx==0)
      putc('.',f);
    fprintf(f,"%s",src_type->name);
    if(ndx==1)
      putc('.',f);
}
void
SVR_absorb::debug_print(FILE *f,int ndx)
{
    assert(ndx>= -1 && ndx <= 2);
    fprintf(f,"%s <- ",target_type->name);
    if(ndx==0)
      putc('.',f);
    fprintf(f,"%s ",target_type->name);
    if(ndx==1)
      putc('.',f);
    fprintf(f,"%s",src_type->name);
    if(ndx==2)
      putc('.',f);
}
void
SVR_star::debug_print(FILE *f,int ndx)
{
    assert(ndx==-1);
    fprintf(f,"%s <- %s *",target_type->name,target_type->name);
}
void
SVR_meet::debug_print(FILE *f,int ndx)
{
    assert(ndx==-1);
    fprintf(f,"%s <- (%s + %s)",lhs->name,lhs->name,rhs->name);
}

/*##
frag(sv_rule_index,"@SV_rule::operator[]@")

*/
Symbol *
SV_rule::operator[](int)
{
    assert(0);
    return 0;
}
Symbol *
SVR_unit::operator[](int i)
{
    switch(i){
      case 0:
	return target_type;
      case 1:
	return src_type;
      case 2:
	break;
      default:
	assert(0);
    }
    return 0;
}
Symbol *
SVR_absorb::operator[](int i)
{
    switch(i){
      case 0:
      case 1:
	return target_type;
      case 2:
	return src_type;
      case 3:
	break;
      default:
	assert(0);
    }
    return 0;
}

/*##
frag(PP_state_equality,"@PP_state::operator==@")

*/

int
PP_state::operator==(PP_state &rhs)
{
    return items==rhs.items;
}

/*##
frag(PP_state_constructor,"@PP_state::PP_state@")

*/
PP_state::PP_state(int n)
{
    int i;
    processed=0;
    n_edges=n;
    out_edges=new int[n];
    for(i=0;i<n_edges;i++)
      out_edges[i]= -1;
}

/*##
frag(PP_iset_enter,"@PP_iset::enter@")

*/

void
PP_iset::enter(PP_item *i)
{
    if(contains(i))
      return;
    append(i);
}

/*##
frag(PP_iset_equality,"@PP_iset::operator==@")

*/

int
PP_iset::operator==(PP_iset &rhs)
{
    PP_iset_iter pi;
    PP_item *item;
    if(rhs.count()!=count())
      return 0;
    for(pi.reset(&rhs);!pi.is_empty();){
	item=pi.step();
	if(!contains(item))
	  return 0;
    }
    return 1;
}

/*##
frag(PP_iset_contains,"@PP_iset::contains@@")

*/

int
PP_iset::contains(PP_item *i)
{
    PP_iset_iter pi(this);
    PP_item *item;
    while(!pi.is_empty()){
	item=pi.step();
	if(*item==*i)
	  return 1;
    }
    return 0;
}

/*##
frag(PP_item_equality,"@PP_item::operator==@")

*/

int
PP_item::operator==(PP_item &rhs)
{
    return *rule==*rhs.rule && dot==rhs.dot;
}

/*##
frag(PP_item_print,"@PP_item::print@")

*/

void
PP_item::print(FILE *f)
{
    putc('[',f);
    rule->debug_print(f,dot);
    putc(']',f);
}

/*##
frag(PP_iset_print,"@PP_iset::print@")

*/
void
PP_iset::print(FILE *f)
{
    PP_iset_iter pi(this);
    fprintf(f,"{");
    while(!pi.is_empty())
      pi.step()->print(f);
    fprintf(f,"}\n");
}

/*##
frag(PP_state_print,"@PP_state::print@")

*/

void
PP_state::print(FILE *f)
{
    int i;
    items.print(f);
    for(i=0;i<n_edges;i++)
      fprintf(f,"%d:%d ",i,out_edges[i]);
    putc('\n',f);
}

/*##
frag(PP_mach_print,"@PP_mach::print@")

*/

void
PP_mach::print(FILE *f)
{
    int i;
    for(i=0;i<n_states;i++){
	fprintf(f,"%d:: ",i);
	states[i]->print(f);
    }
}

/*##
frag(PP_mach_add,"@PP_mach::add@")
tex @{

If the assertion about the machine size fails, increase PP_mach_size.

@}

*/
int
PP_mach::add(PP_state *new_state)
{
    int i;
    for(i=0;i<n_states;i++)
      if(*states[i]==*new_state){
	  delete new_state;
	  return i;
      }
    assert(n_states < PP_mach_size);/* o.w. machine is too big */
    states[n_states]=new_state;
    n_states++;
    return n_states-1;
}

/*##
frag(PP_state_add,"@PP_state::add@")

*/
void
PP_state::add(SV_rule *r)
{
    switch(r->rule_kind){
      case TAG(SVR_unit): case TAG(SVR_absorb): case TAG(SVR_create):
	items.enter(new PP_item(r));
	break;
      default:
	break;
    }
}
void
PP_state::add(SV_rule_list *rl)
{
    SV_rule_iter ri(rl);
    while(!ri.is_empty())
      add(ri.step());
}

/*##
frag(PP_state_first_set,"@PP_state::first_set@")

*/
void
PP_state::next_set(Symbol_list *sl)
{
    PP_iset_iter pi(&items);
    while(!pi.is_empty()){
	PP_item *i=pi.step();
	Symbol *s=i->rule->next_sym(i->dot);
	if(s && !sl->contains(s))
	  sl->append(s);
    }
}

/*##
frag(sv_rule_next_sym,"@SV_rule::next_sym@")

*/
Symbol *
SV_rule::next_sym(int)
{ assert(0); return 0;}
Symbol *
SVR_create::next_sym(int)
{
    return 0;
}
Symbol *
SVR_unit::next_sym(int dot)
{
    switch(dot){
      case 0:
	return src_type;
      default:
	return 0;
    }
}
Symbol *
SVR_absorb::next_sym(int dot)
{
    switch(dot){
      case 0:
	return target_type;
      case 1:
	return src_type;
      default:
	return 0;
    }
}

/*##
frag(sv_rule_reducible,"@SV_rule::reducible@")

*/
int
SV_rule::reducible(int)
{ assert(0); return 0;}
int
SVR_create::reducible(int)
{
    return 1;
}
int
SVR_unit::reducible(int dot)
{
    return dot>=1;
}
int
SVR_absorb::reducible(int dot)
{
    return dot>=2;
}

/*##
frag(sv_rule_target,"@SV_rule::target@")

*/
SV_node *
SV_rule::target()
{ assert(0); return 0;}
SV_node *
SVR_create::target()
{
    return target_node;
}
SV_node *
SVR_unit::target()
{
    return target_node;
}
SV_node *
SVR_absorb::target()
{
    return target_node;
}

/*##
frag(sv_rule_length,"@SV_rule::length@")

*/
int
SV_rule::length()
{ assert(0); return 0;}
int
SVR_create::length()
{
    return 0;
}
int
SVR_unit::length()
{
    return 1;
}
int
SVR_absorb::length()
{
    return 2;
}

/*##
frag(Symbol_list_print,"@Symbol_list::print@")

*/
void
Symbol_list::print(FILE *f)
{
    Symbol_iter si(this);
    fprintf(f,"<");
    while(!si.is_empty()){
	fprintf(f,"%s,",si.step()->name);
    }
    fprintf(f,">");
}

/*##
frag(PP_state_apply,"@PP_state::apply@")

*/
PP_state *
PP_state::apply(Symbol *ns,int *has_reductions)
{
    PP_state *new_state = new PP_state(n_edges);
    PP_iset_iter pi(&items);
    while(!pi.is_empty()){
	PP_item *i=pi.step();
	Symbol *s=i->rule->next_sym(i->dot);
	if(s){
	    if(s==ns)
	      new_state->items.enter(new PP_item(i->rule,i->dot+1));
	}else{
	    /* there is no next symbol iff this item is reducible */
	    assert(i->rule->reducible(i->dot));
	    *has_reductions = 1;
	}
    }
    if(new_state->items.is_empty()){
	delete new_state;
	return 0;
    }else
      return new_state;
}

/*##
frag(pp_mach_contructor,"@PP_mach::PP_mach@")

*/
PP_mach::PP_mach(int n)
{
    n_states=0;
    n_nodes=n;
}

extern i_stream *current_input;
extern o_stream *code_stream;
    
/* Generate the path parser automaton */
void PP_mach::generate(o_stream *cs,SV_dflow *df)
{
    char buf[500];
    int i,j;
    PP_state *s;
    PP_iset_iter pi;
    PP_item *pitem;
    cs->put_string("static Seqn fake_q((Snode *)0);\n");
    sprintf(buf,"PP_stack_e %s::_pp_reduce(Strans *_LA_L,Seqn *_LA_Q,PP_stack *_STACK_){\n",
	    df->solver_type->type_name->name);
    cs->put_string(buf);
    cs->put_string("switch(_STACK_->top->state){\n");
    for(i=0;i<n_states;i++){
	s=states[i];
	for(pi.reset(&s->items);!pi.is_empty();){
	    pitem=pi.step();
	    if(pitem->rule->reducible(pitem->dot)){
		sprintf(buf,"case %d:\n_STACK_->top -= %d;\n",
			i,pitem->rule->length());
		cs->put_string(buf);
		pitem->rule->gen2(cs,df);
	    }
	}
	/* check for reduce/reduce conflict */
       while(!pi.is_empty()){
	   pitem=pi.step();
	 if(pitem->rule->reducible(pitem->dot))
	   current_input->errmsg("reduce/reduce conflict");
       }
    }
    cs->put_string("\n}\n return PP_stack_e(0,0); }\n");
    sprintf(buf,"static int _PP_shift[%d*%d]={\n",n_states,n_nodes);
    cs->put_string(buf);
    for(i=0;i<n_states;i++){
	s=states[i];
	for(j=0;j<n_nodes;j++){
	    sprintf(buf,"%d,",s->out_edges[j]);
	    cs->put_string(buf);
	}
	cs->nl();
    }
    cs->put_string("};\n");
    sprintf(buf,"void %s::_pp_init(){\n",df->solver_type->type_name->name);
    cs->put_string(buf);
    sprintf(buf,"n_states=%d;\nn_nodes=%d;\n_pp_shift=_PP_shift;\n}\n",
	    n_states,n_nodes);
    cs->put_string(buf);
}

/*##
frag(sv_rule_gen2,"@SV_rule::gen2@")

*/
void
SV_rule::gen2(o_stream *,SV_dflow *)
{
    assert(0);
}
void
SVR_create::gen2(o_stream *cs,SV_dflow *df)
{
//    cs->put_string("{\n_STACK_->top[1].Q=&fake_q;\n");
//    cs->put_string("{\n_STACK_->top[1].Q=new Seqn((Snode *)0);\n");
    cs->put_string("{\n");
    target_names->bind_and_initialize(cs,df,target_type,"_STACK_->top[1]");
    cs->put_code(creator);
    cs->put_string("}\n_STACK_->top[1].Q=_STACK_->top[0].Q;\nreturn _STACK_->top[1];\n");
}
extern void bind_and_initialize(Symbol *t,Symbol *s,const char *e);
void
SVR_unit::gen2(o_stream *cs,SV_dflow *df)
{
    cs->put_string("{\n");
    qnames->bind_and_initialize(cs,df,src_type,"_STACK_->top[1]");
    cs->put_code(creator);
    cs->put_string("}\nreturn _STACK_->top[1];\n");
}
void
SVR_absorb::gen2(o_stream *cs,SV_dflow *df)
{
    cs->put_string("{\n");
    if(src_type){
	target_names->bind_and_initialize(cs,df,target_type,"_STACK_->top[1]");
	qnames->bind_and_initialize(cs,df,src_type,"_STACK_->top[2]");
    }else
      target_names->bind_and_initialize(cs,df,target_type,"_STACK_->top[1]");
    if(creator)
      cs->put_code(creator);
    cs->put_string("}\n_STACK_->top[1].Q=_STACK_->top[2].Q;return _STACK_->top[1];\n");
}

/*##
frag(q_binder_bind,"@Q_binder::bind_and_intialize@")

*/
void
Q_binder::bind_and_initialize(o_stream *cs,SV_dflow *df,Symbol *L_type,
			     const char *base,const char *L_src)
{
    char buf[500];
    if(this==0)
      return;
    if(base && Q_name){
	cs->put_string("Seqn *&");
	cs->put_string(Q_name->name);
	sprintf(buf,"= %s.Q;\n",base);
	cs->put_string(buf);
    }
    if(base && V_name){
	cs->put_string(df->VV->type_name->name);
	cs->put_string(" *&");
	cs->put_string(V_name->name);
	sprintf(buf,"=*(%s **)&%s.Q->V;\n",df->VV->type_name->name,base);
	cs->put_string(buf);
    }
    if(L_name){
	cs->put_string(L_type->name);
	cs->put_string(" *&");
	cs->put_string(L_name->name);
	if(!L_src)
	  sprintf(buf,"= *(%s **)&%s.iL;\n",L_type->name,base);
	else
	  sprintf(buf,"= *(%s **)&%s;\n",L_type->name,L_src);
	cs->put_string(buf);
    }
    if(base && S_name){
	cs->put_string(df->SS->type_name->name);
	cs->put_string(" *&");
	cs->put_string(S_name->name);
	sprintf(buf,"= *(%s **)&%s.Q->S;\n",df->SS->type_name->name,base);
	cs->put_string(buf);
    }
}
void
Q_binder::bind_and_initialize2(o_stream *cs,SV_dflow *df,Symbol *L_type,
			     const char *Q_src,const char *L_src)
{
    char buf[500];
    if(this==0)
      return;
    if(L_src && L_name){
	cs->put_string(L_type->name);
	cs->put_string(" *&");
	cs->put_string(L_name->name);
	sprintf(buf,"= *(%s **)&%s;\n",L_type->name,L_src);
	cs->put_string(buf);
    }
    if(Q_src && S_name){
	cs->put_string(df->SS->type_name->name);
	cs->put_string(" *&");
	cs->put_string(S_name->name);
	sprintf(buf,"= *(%s **)&%s;\n",df->SS->type_name->name,Q_src);
	cs->put_string(buf);
    }
}
void
Q_binder::bind_and_initialize3(o_stream *cs,SV_dflow *df,Symbol *L_type,
			     const char *Q_src,const char *L_src)
{
    char buf[500];
    if(this==0)
      return;
    if(L_src && L_name){
	cs->put_string(L_type->name);
	cs->put_string(" *");
	cs->put_string(L_name->name);
	sprintf(buf,"= (%s *)%s;\n",L_type->name,L_src);
	cs->put_string(buf);
    }
    if(Q_src && S_name){
	cs->put_string(df->SS->type_name->name);
	cs->put_string(" *&");
	cs->put_string(S_name->name);
	sprintf(buf,"= *(%s **)&%s;\n",df->SS->type_name->name,Q_src);
	cs->put_string(buf);
    }
}
int
ty_record_list::contains(ty_record *t)
{
    ty_record_list_iter ti(this);
    while(!ti.is_empty()){
	if(t==ti.step())
	  return 1;
    }
    return 0;
}
