/* file "conslist.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  CONS Cell List Definitions */

#ifndef CONSLIST_H
#define CONSLIST_H

#pragma interface

/*  the following files must be included before this point:
#include "useful.h"
#include "glist.h"
*/

class cons_e;
class conslist;
class consiter;


/*  class cons_e:  This simple class adds a pointer to the basic glist_e
    list element.  (This is equivalent to a CONS cell in LISP.)  */

class cons_e : public glist_e {
public:
    void *data;
    cons_e(void *d) { data = d; }
};



/*  class conslist:  This class basically provides type-correct wrappers for
    the glist functions.  The major difference is that instead of using
    pointers to cons_e elements, these functions use and return the void *
    data pointers and automatically manage the list elements.  They are
    quite straightforward.  */

class conslist : public glist {
  public:
    conslist()				{ }
    conslist(const conslist &l);
    void push(void *d)			{ glist::push(new cons_e(d)); }
    void * pop();
    void append(void *d)		{ glist::append(new cons_e(d)); }
    void reverse(conslist *ol);
    void * top()			{ return ((cons_e *)head_e)->data; }
    void * last()			{ return ((cons_e *)tail_e)->data; }
    void mk_empty();

    /* fast access functions */
    void f_push(cons_e *e)		{ glist::push(e); }
    void f_append(cons_e *e)		{ glist::append(e); }
    void * f_pop(cons_e **e)		{ *e = (cons_e *)glist::pop();
					  return (*e)->data; }
};



class consiter : public glist_iter {
public:
    consiter()				{ }
    consiter(const conslist *sl)	{ reset(sl); }
    void * step()			{ return ((cons_e *)glist_iter::step())
					  ->data; }
    void * peek()			{ return ((cons_e *)glist_iter::peek())
					  ->data; }
};

#endif /* CONSLIST_H */
