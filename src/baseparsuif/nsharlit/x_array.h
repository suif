/* file "x_array.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef X_ARRAY0
#define X_ARRAY0

#pragma interface
/*

Extendible array specific to O.  After testing, these class should be
combined with the standard xarray.h in ../general.

X_array0 - An array of void * that uses a doubling strategy.  Because
doubling may move the entire array, pointers into the array are
discouraged.  If pointers into the arrays are absolutely necessary,
you must prevent the array from moving by freezing it.  However,
freezing means the array can not be doubled in size.

*/

class X_array0 {
    int frozen;
  public:
    int size;
    int hi;
    void **data;
    X_array0(int sz);
    ~X_array0();
    void *&operator[](int);
    void operator=(int);
    void grow(int);
    void expand(int);
    int extend(void *);
    int ub();
    void grab_from(X_array0 &);
    void freeze()
      {
	  frozen=1;
      }
};

#if defined(USE_TEMPLATES)

template<class TYPE,int DEFAULT_SIZE>
class Parameterized_x_array0
{
  int frozen;

public:
  int size;
  int hi;
  TYPE *data;
  Parameterized_x_array0(int sz = -1)
  {
    frozen=0;
    size=sz;
    hi=0;
    if(sz==-1)
      sz = DEFAULT_SIZE;
    data=new TYPE[sz];
  }

  ~Parameterized_x_array0()
  {
    delete data;
  }

  TYPE &operator[](int i)
  {
    if(i<hi)
      return data[i];
    else{
      assert(0);
      return data[0];
    }
  }

  void expand(int nsz)
  {
    if(nsz<=hi)
      return;
    if(nsz>size)
      {
	int i=hi;
	TYPE *temp,*dst, *src=data;
	temp=dst=new TYPE[nsz];
	size=nsz;
	assert(!frozen);
	while(i>0)
	  {
	    *dst++=*src++;
	    i--;
	  }
	delete data;
	data=temp;
      }
  }


  void grow(int nsz)
  {
    expand(nsz);
    hi=nsz;
  }
       
  int extend(TYPE e)
  {
    if(hi==size)
      {
	int i=hi;
	TYPE *temp,*dst, *src=data;
	int nsz=size ? 2*size : 1;
	temp=dst=new TYPE[nsz];
	size=nsz;
	assert(!frozen);
	while(i>0){
	  *dst++=*src++;
	  i--;
	}
	delete data;
	data=temp;
      }
    assert(hi<size);
    data[hi]=e;
    hi++;
    return hi-1;
  }

  int ub()
  {
    return hi;
  }

  void freeze()
  {
    frozen=1;
  }

  TYPE *first()
  {
    return data;
  }

  TYPE *limit()
  {
    return &data[hi];
  }

  void clear()
  {
    hi = 0;
    size = 0;
    delete[] data;
    date = 0;
  }
};

#define DECLARE_X_ARRAY0(name,TYPE,DEFAULT_SIZE)			      \
typedef Parameterized_x_array0<TYPE,DEFAULT_SIZE> name

#else

#define DECLARE_X_ARRAY0(NAME,TYPE,DEFAULT_SIZE)			      \
class NAME								      \
{									      \
  int frozen;								      \
									      \
public:									      \
  int size;								      \
  int hi;								      \
  TYPE *data;								      \
  NAME(int sz = -1)							      \
  {									      \
    frozen=0;								      \
    hi=0;								      \
    if(sz==-1)								      \
      sz = DEFAULT_SIZE;						      \
    size=sz;								      \
    data=new TYPE[sz];							      \
  }									      \
									      \
  ~NAME()								      \
  {									      \
    delete data;							      \
  }									      \
									      \
  TYPE &operator[](int i)						      \
  {									      \
    if(i<hi)								      \
      return data[i];							      \
    else{								      \
      assert(0);							      \
      return data[0];							      \
    }									      \
  }									      \
									      \
  void expand(int nsz)							      \
  {									      \
    if(nsz<=hi)								      \
      return;								      \
    if(nsz>size)							      \
      {									      \
	int i=hi;							      \
	TYPE *temp;							      \
	TYPE *dst;							      \
	TYPE *src=data;							      \
	temp=dst=new TYPE[nsz];						      \
	size=nsz;							      \
	assert(!frozen);						      \
	while(i>0)							      \
	  {								      \
	    *dst++=*src++;						      \
	    i--;							      \
	  }								      \
	delete data;							      \
	data=temp;							      \
      }									      \
  }									      \
									      \
									      \
  void grow(int nsz)							      \
  {									      \
    expand(nsz);							      \
    hi=nsz;								      \
  }									      \
									      \
  int extend(TYPE e)							      \
  {									      \
    if(hi==size)							      \
      {									      \
	int i=hi;							      \
	TYPE *temp;							      \
	TYPE *dst;							      \
	TYPE *src=data;							      \
	int nsz=size ? 2*size : 1;					      \
	temp=dst=new TYPE[nsz];						      \
	size=nsz;							      \
	assert(!frozen);						      \
	while(i>0){							      \
	  *dst++=*src++;						      \
	  i--;								      \
	}								      \
	delete data;							      \
	data=temp;							      \
      }									      \
    assert(hi<size);							      \
    data[hi]=e;								      \
    hi++;								      \
    return hi-1;							      \
  }									      \
									      \
  int ub()								      \
  {									      \
    return hi;								      \
  }									      \
									      \
  void freeze()								      \
  {									      \
    frozen=1;								      \
  }									      \
									      \
  TYPE *first()								      \
  {									      \
    return data;							      \
  }									      \
									      \
  TYPE *limit()								      \
  {									      \
    return &data[hi];							      \
  }									      \
									      \
  void clear()								      \
  {									      \
    hi = 0;								      \
    size = 0;								      \
    delete[] data;							      \
    data = 0;								      \
  }									      \
}

#endif

// For info on USE_TEMPLATES, see nr_map.h

#if defined(USE_TEMPLATES)

template<class PTR_TYPE,int DEFAULT_SIZE>
class Parameterized_x_array1: public X_array0
{
  public:
    Parameterized_x_array1(int sz=0)
      :X_array0(sz ? sz : DEFAULT_SIZE)
      {
      }
    ~Parameterized_x_array1()
      {
      }
    inline PTR_TYPE *&operator[](int i)
      {
	  return (PTR_TYPE *&)(X_array0::operator[](i));
      }
    inline PTR_TYPE **first()
      {
	  return (PTR_TYPE **)data;
      }
    inline PTR_TYPE **limit()
      {
	  return (PTR_TYPE **)&data[hi];
      }
};

#define DECLARE_X_ARRAY1(name,ptr_type,default_size)			      \
typedef Parameterized_x_array1<ptr_type,default_size> name

#else

#define DECLARE_X_ARRAY1(name,PTR_TYPE,DEFAULT_SIZE)			      \
class name: public X_array0						      \
{									      \
  public:								      \
    inline name(int sz=0)						      \
      :X_array0(sz ? sz : DEFAULT_SIZE)					      \
      {									      \
      }									      \
    ~name()								      \
      {									      \
      }									      \
    inline PTR_TYPE *&operator[](int i)					      \
      {									      \
	  return (PTR_TYPE *&)(X_array0::operator[](i));		      \
      }									      \
    inline PTR_TYPE **first()						      \
      {									      \
	  return (PTR_TYPE **)data;					      \
      }									      \
    inline PTR_TYPE **limit()						      \
      {									      \
	  return (PTR_TYPE **)&data[hi];				      \
      }									      \
}


#endif

DECLARE_X_ARRAY0(Int_xarray,int,0);

#endif
