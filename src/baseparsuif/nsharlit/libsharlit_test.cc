/* file "libsharlit_test.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#pragma implementation "sharlit_test.h"

#include "sharlit.h"
#include "cfg.h"
#include "sharlit_test.h"


CFGnode_kinds qkind_to_cfgkind[]
= { CFGnode_normal, CFGnode_header, CFGnode_bb,
    CFGnode_header, CFGnode_header, CFGnode_irreducible,
    CFGnode_unreachable };

inline static CFGnode_kinds map_to_CFGnode_kinds(int qk)
{
  assert(SEQN_normal <= qk && qk <= SEQN_unreachable);
  return qkind_to_cfgkind[qk];
}

int
Sprob_switches::NONTERMINAL(Strans *)
{
    assert(0);
    return 0;
}

int
Sprob_switches::propagate3(Seqn **eqns,int num_eqns)
{
    int ch=0,c,i;
    Seqn *q;
    Svar *dfv=new_var();
    /*
      for(i=0;i<num_eqns;)
	c|=F3(i,dfv,eqns,num_eqns);
    */
    print("propagate3 start\n");
    start_iteration(dfv);
    for(i=0;i<num_eqns;i++){
	q=eqns[i];
	switch(q->kind){
	  default:
	    meet(dfv,q->S);
	  case SEQN_normal:
	    q->L->in_action(q->S,dfv);
	    q->L->flow(q->S,dfv);
	    if(q->V){
		c=copy_var(q->V,dfv);
		if(q->solved()&&c){
		    debug_print(q->S,q->L,dfv,c);
		    assert(0);
		}
	    }else
	      c=0;
	    q->L->out_action(q->S,dfv);
	    break;
	  case SEQN_unreachable:
	    c=0;
	    break;
	}
	debug_print(q->S,q->L,dfv,c);
	ch |= c;
    }
    delete_var(dfv);
    print("propagate3 end\n");
    return ch;
}

int
Sprob_switches::flow3(Seqn **l,int num_q,Svar *dfv)
{
    int ch=0,c,i;
    Seqn *q;
    print("flow3 start\n");
    if(checking){
	/* Evaluation Loop */
	for(i=0;i<num_q;i++){
	    q=l[i];
	    switch(q->kind){
	      case SEQN_unreachable:
		break;
	      case SEQN_normal:
		/*
		  ASSERTION --- If this node doesn't have an ancestor than
		    the previous node on the list was the predecessor.
		      */
		if(!q->ancestor)
		  ;
		else
		  copy_var(dfv,q->ancestor->Q->V);
		q->L->flow(q->S,dfv);
		c= q->V ? copy_var(q->V,dfv) : 0;
		break;
	      default:
		if(!q->ancestor)
		  meet(dfv,q->S);
		else
		  copy_var(dfv,q->ancestor->Q->V);
		q->L->flow(q->S,dfv);
		c =  copy_var(q->V,dfv);
	    }
	    ch |= c;
	    debug_print(q->S,q->L,dfv,c);
	}
    }else{
	assert(0);	/* FOR NOW */
    }
    print("flow 3 end\n");
    return ch;
}

int
Sprob_switches::iterate(Seqn **l,int num_q,int max_iter)
{
    int c;
    int iter=0;
    changed=1;
    iteration=0;
    Svar *dfv=new_var();
    do {
	start_iteration(dfv);
	c=flow3(l,num_q,dfv);
	iter++;
	iteration++;
	changed=0;
    } while (c && --max_iter > 0);
    delete_var(dfv);
    return iter;
}

/*

Given a list of equation, this procedure updates the fields, preparing
the list for solution or propgation.  After this procedure, the
following conditions are crucial:

1. For each header, copy the ``solved'' variable from the reduced set
of equations.  This is done by reference with s->Q.

2. Those variables which need variables, but don't have one are given
a fresh variable.  This step simplifies Sprob_test::F3

 CONJECTURE: There is a strong resemblence between the sequence
init_eqn and propagate, and just the call to isolve3.  A significant
difference is the call to process.

*/
void Sprob_switches::init_eqn(Seqn **eqns,int num_eqns)
{
    Seqn *q,*oq;
    Snode *s;
    Svar *v;
    int i;
    for(i=0;i<num_eqns;i++){
	q=eqns[i];
	s=q->S;
	oq=s->Q;
	s->Q=q;
	if ((v=oq->V)){
	    q->V=v;
	    v->solved=1;
	} else
	  /* fill in those places that need variables but don't have one */
	  switch(q->kind){
	    case SEQN_bb:
	    case SEQN_normal:
	      if(!q->needs_var){
		  q->V=0;
		  break;
	      }
	      q->V=new_var();
	      break;
	    case SEQN_unreachable:
	      break;
	    default:
	      /* Screw up: headers and others must already have vars */
	      assert(0);
	  }
    }
}

void Sprob_switches::link(Snode *hd,Snode *s)
{
    Seqn *q=s->Q;
    if(q->iL!=&strans_bottom){
	q->ancestor=hd;
	switch(q->kind){
	  case SEQN_bb:
	  case SEQN_normal:
	    break;
	  default:
	    keep(s);
	}
    }else
      keep(s);
}

Sprob_switches::Sprob_switches(CFG_0 *g,int d)
     :Sprob_tarjan_DEBUG(g,d)
{
    path_debug=0; /* vestigial */
    equations=0;
}
Sprob_switches::~Sprob_switches()
{
  if(equations) {
    for (unsigned i = 0; i < (unsigned)graph->n_nodes; i++) {
      if (equations[i].oV) {
	delete equations[i].oV;
      }
    }
    delete equations;
  }
}
void Sprob_switches::_init_tables()
{
    assert(0);
}

// Default Virtual Functions.

Strans *Sprob_switches::_TIMES_xs(int,Strans *,Snode *,Strans *,Snode *)
{
    assert(0);
    return 0;
}
Strans *Sprob_switches::_CREATE(int)
{
    assert(0);
    return 0;
}
Strans *Sprob_switches::_UNIT(int,Strans *,Snode *)
{
    assert(0);
    return 0;
}
Strans *Sprob_switches::_ABSORB(Strans *,Strans *,Snode *)
{
    assert(0);
    return 0;
}
Strans *Sprob_switches::_MEET_ux(Strans *,Strans *,Snode *,int)
{
    assert(0);
    return 0;
}
Strans *Sprob_switches::_STAR_u(Strans *,Snode *)
{
    assert(0);
    return 0;
}
int Sprob_switches::solve(Seqn **el,int num_q,int max_iter)
{
    int iter,needs_iteration,i,num_red_q;
    Seqn **red_q,*q;
    Svar *v;
    _init_tables();
    if(n_states>1){
	red_q=new Seqn*[num_q];
	needs_iteration=simplify(red_q,&num_red_q,el,num_q);
	cnt=num_q;
	rcnt=num_red_q ? num_red_q : num_q;
	iter=iterate(red_q,num_red_q,needs_iteration ? max_iter : 1);
	if(iter<max_iter){
	    /*
	      update the original graph with solutions from red_q.
		Also allocate variables to nodes that need them.
		  Then propagate
		    */
	    init_eqn(el,num_q);
	    propagate3(el,num_q);
	}
	for(i=0;i<num_red_q;i++){
	    q=red_q[i];
	    if(q->iL)
	      delete q->iL;
	    delete q;
	}
	delete red_q;
    } else {
	/* there can't be any reductions because the machine has no states */
	/*
	  Call flow_map and get a transformer for each equation.
	  At the same time, allocate a variable for each node required for
	  propagation of the unreduced graph.  These nodes are marked
	  with the flag needs_var.
	  */
	rcnt=cnt=num_q;
	for(i=0;i<num_q;i++){
	    q=el[i];
	    q->L=_FLOW_MAP(q->S,map_to_CFGnode_kinds(q->kind));
	    q->ancestor=0;
	    q->rcount=0;
	    q->S->Q=q;
	    switch(q->kind){
	      case SEQN_normal:
		if(!q->needs_var){
		    q->V=0;
		    break;
		}
	      default:
		q->V=new_var();
	    }
	}
	iter=iterate(el,num_q,max_iter);
	if(iter<max_iter){
	    for(i=0;i<num_q;i++){
		v=el[i]->V;
		if(v)
		  v->solved=1;
	    }
	    propagate3(el,num_q);
	}
    }
    return iter;
}

Strans *Sprob_switches::_FLOW_MAP(Snode *, CFGnode_kinds)
{
	/* abstraction function --- aborts if called */
    assert(0);
    return 0;
}

int Sprob_switches::simplify_region(int i,int n_eqns,Seqn **nqa)
{
    Seqn *h,*q,*bbq,*last_h;
    Strans *P,*Q;
    Snode *s, **deps;
    int d,nd;
    if(i==n_eqns)
      return i;
    h=nqa[i];
    switch((SEQN_type)h->kind){
      case SEQN_header:
      case SEQN_noncyclic:
	i++;
	while(i<n_eqns){
	    q=nqa[i];
	    if(q->highpt!=h->S)
	      break;
	    if(q->kind!=SEQN_normal){
		i=simplify_region(i,n_eqns,nqa);
		continue;
	    }
	    i++;
	    P=Q=&strans_top;
	    deps=dependents2(q->S,&nd);
	    for(d=0;d<nd;d++){
		s=deps[d];
		P=_MEET_ux(P,_TIMES_xs(1,eval(s,h->S),s,q->iL,q->S),q->S);
	    }
	    update(q->S,P,Q);
	    link(h->S,q->S);
	}
	break;
      case SEQN_bb:
	i++;
	/* unrolled one iteration, and then bbq==0 */
	if(i>=n_eqns)
	  break;
	q=nqa[i];
	Q= &strans_top;
	bbq=0;
	if(q->highpt!=h->S)
	  break;
	i++;
	deps=dependents2(q->S,&nd);
	assert(nd==1);
	s=deps[0];
	P=_TIMES_xs(1,eval(s,h->S),s,q->iL,q->S);
	update(q->S,P,Q);
	link(h->S,q->S);
	last_h=h;
	bbq=q;
	while(i<n_eqns){
	    q=nqa[i];
	    if(q->highpt!=h->S)
	      break;
	    i++;
	    deps=dependents2(q->S,&nd);
	    assert(nd==1);
	    assert(bbq==deps[0]->Q);
	    P=_TIMES_xs(0,bbq->iL,bbq->S,q->iL,q->S);
	    if(P==&strans_bottom){
		last_h=bbq;
	    }else{
		if(P==bbq->iL)
		  bbq->iL=0;
		update(q->S,P,Q);
	    }
	    link(last_h->S,q->S);
	    bbq=q;
	}
	break;
      case SEQN_improper:
	i++;
	while(i<n_eqns){
	    q=nqa[i];
	    if(q->highpt!=h->S)
	      break;
	    if(q->kind!=SEQN_normal){
		i=simplify_region(i,n_eqns,nqa);
		continue;
	    }
	    i++;
	    update(q->S,&strans_bottom,&strans_bottom);
	    link(h->S,q->S);
	}
	break;
      case SEQN_normal: case SEQN_unreachable:
	i++;
	break;
      case SEQN_dominator:
	assert(0);
    }
    P=Q=&strans_top;
    deps=dependents2(h->S,&nd);
    for(d=0;d<nd;d++){
	s=deps[d];
	if(!is_cycle(h->S,s))
	  P=_MEET_ux(P,_TIMES_xs(1,eval(s,h->highpt),s,h->iL,h->S),h->S);
	else
	  Q=_MEET_ux(Q,_TIMES_xs(1,eval(s,h->S),s,h->iL,h->S),h->S);
    }
    update(h->S,P,Q);
    if(h->highpt)
      link(h->highpt,h->S);
    else
      keep(h->S);
    return i;
}

int Sprob_switches::simplify(Seqn **new_q,int *num_new_q,Seqn **old_q,
			 int num_old_q)
{
    Seqn *q,*q2;
    Seqn **temp_eqns;
    int i,j;
    int iterate=0;
    Snode *s;
    
    /* a quick check */
    *num_new_q=0;
    if(num_old_q==0)
      return 1;
    temp_eqns=new Seqn*[num_old_q];
    for(i=0;i<num_old_q;i++){
	q=old_q[i];
	q->V=0;
	q->L=_FLOW_MAP(q->S,map_to_CFGnode_kinds(q->kind));
	switch((SEQN_type)q->kind){
	  case SEQN_normal:
	  case SEQN_bb:
	  case SEQN_unreachable:
	    break;
	  case SEQN_noncyclic:
	  case SEQN_header:
	  case SEQN_dominator:
	  case SEQN_improper:
	    q=new Seqn(q);
	}
	s=q->S;
	s->Q=q;
	q->iL=q->L;
	q->ancestor=0;
	q->rcount=0;
	temp_eqns[i]=q;
    }
    for(i=0;i<num_old_q;)
      i=simplify_region(i,num_old_q,temp_eqns);
    for(i=num_old_q;i>0;){
	q=temp_eqns[--i];
	switch((SEQN_type)q->kind){
	  case SEQN_header:
	  case SEQN_noncyclic:
	  case SEQN_improper:
	    if(q->ancestor){
		assert(q->iL!=&strans_bottom);
		q->ancestor->Q->rcount |= 1;
		q->rcount |= 1;
	    }else switch(q->kind){
	      case SEQN_header:
	      case SEQN_improper:
		iterate=1;
	    }
	    assert(q->L->kind!=Sk_top);
	    break;
	  case SEQN_normal:
	  case SEQN_bb:
	    if(q->rcount&1)
	      if(q->ancestor){
		  assert(q->iL!=&strans_bottom);
		  q->ancestor->Q->rcount|=1;
	      }
	    break;
	  case SEQN_dominator:
	    assert(0);
	  case SEQN_unreachable:
	    break;
	}
    }
    for(i=0,j=0;i<num_old_q;i++){
	q=temp_eqns[i];
	switch((SEQN_type)q->kind){
	  case SEQN_header:
	  case SEQN_noncyclic:
	  case SEQN_improper:
	    if(q->iL!=&strans_bottom)
	      q->L=q->iL;
	    q->V=new_var();
	    new_q[j++]=q;
	    break;
	  case SEQN_normal:
	  case SEQN_bb:
	    if(q->rcount&1){
		q2=new Seqn(q);
		if(q2->iL!=&strans_bottom)
		  q2->L=q2->iL;
		q2->V=new_var();
		q2->S->Q=q2;
		new_q[j++]=q2;
	    } else {
		if(q->iL && NONTERMINAL(q->iL))
		  delete q->iL;
		q->iL=0;
	    }
	    break;
	  case SEQN_dominator:
	    assert(0);
	  case SEQN_unreachable:
	    if(q->iL && NONTERMINAL(q->iL))
	      delete q->iL;
	    q->iL=0;
	    break;
	}
    }
    *num_new_q=j;
    delete temp_eqns;
    return iterate;
}

void Sprob_switches::update(Snode *s,Strans *P,Strans *Q)
{
    Seqn *q=s->Q;
    Strans *l0;
    Strans *l=_TIMES_xs(0,P,0,l0=_STAR_u(Q,0),0);
    delete l0;
    switch(l->kind){
      case Sk_top: case Sk_bottom:
	q->iL=&strans_bottom;
	if(l!=P)
	  delete P;
	break;
      default:
	q->iL=l;
    }
}

void Sprob_switches::path_compress(Snode *s)
{
    Strans *nff;/* new flow function */
    Seqn *q=s->Q;
    Snode *a=ancestor(s);
    Seqn *aq;
    /*
      Setting rcount bit 1 to 1 disables path compression
	because we have compress as far as we can go.
	  */
    if(q->rcount&2)
      return;
    if(ancestor(a)){
	aq=a->Q;
	path_compress(a);
	nff = _TIMES_xs(1,aq->iL,a,q->iL,s);
	/*
	  try to compress up the header tree
	    If we fail then compress where as far up as we can
	      */
	switch(nff->kind){
	  case Sk_bottom:
	    /* disable path compression */
	    q->rcount|=2;
	    return;
	  case Sk_top:
	  case Sk_identity:
	    assert(0);
	  default:
	    delete q->iL;
	    q->iL = nff;
	    q->ancestor=ancestor(a);
	}
    }
}

/*

This routine is called just before one iteration over the equation,
and just before the propagation step.  It provides a hook with which
the user can do DFA-dependent initialization at those times.

*/
void Sprob_switches::start_iteration(Svar *)
{
}

int Sprob_switches::get_srcs(Svar **&srcs,Snode *n)
{
    int nd;
    static Svar *var_array[MAX_ARITY], **svp;
    Snode **preds = dependents2(n,&nd);
    Snode **preds_limit = &preds[nd];
    Seqn *q;
    CFinfo *cfi;
    if(equations){
	for(svp=&var_array[0];preds<preds_limit;preds++){
	    cfi = &graph->numbers[preds[0]->unique];
	    if(cfi->_kind==CFGnode_unreachable)
	      continue;
	    svp[0] = equations[preds[0]->unique].oV;
	    svp++;
	}
    } else {
	for(svp=&var_array[0];preds<preds_limit;preds++){
	    q=preds[0]->Q;
	    if(q->kind==SEQN_unreachable)
	      continue;
	    svp[0] = q->V;
	    svp++;
	}
    }
    srcs=var_array;
    return svp-var_array;
}

/*

New functions that don't use Seqn.  They use n_Seqn and CFinfo instead.
This factors out the information so that each data-flow problem can have
its own list of n_Seqns

*/

void Sprob_switches::n_initialize_equations()
{
  register CFinfo *cfi;
  register n_Seqn *q;
  int i;
  assert(equations==0);
  equations=new n_Seqn[graph->n_nodes];
  cnt = rcnt = graph->n_nodes-graph->n_unreachables;
  for(i=0;i<graph->n_nodes;i++){
    cfi = &graph->numbers[i];
    q = &equations[i];

    /*
    Allocate a flow variable if needed.
      Note _FLOW_MAP can still insist on one by calling must_have_var.
      */
    switch(cfi->_kind){
    case CFGnode_normal:
      if(!cfi->needs_var){
	q->oV=0;
	break;
      }
    default:
      q->oV = new_var();
      break;
    case CFGnode_unreachable:
      q->oV = 0;
      break;
    }

    q->L = q->oL =_FLOW_MAP(cfi->node,cfi->_kind);
    q->aL = 0;
    q->ancestor = 0;
    q->keep = q->stop_compression = q->on_reduced_list = 0;
  }
}

int
Sprob_switches::n_propagate()
{
    int ch=0,c,i,n;
    register CFinfo *cfi;
    register n_Seqn *q;
    register Svar *dfv=new_var();
    register Snode *s;
    print("n_propagate start\n");
    start_iteration(dfv);
    n=graph->n_nodes-graph->n_unreachables;
    for(i=0;i<n;i++){
	cfi=graph->modified_order[i];
	s=cfi->node;
	q = &equations[cfi->unique];
	switch(cfi->_kind){
	  default:
	    meet(dfv,s);
	  case CFGnode_normal:
	    q->oL->in_action(s,dfv);
	    q->oL->flow(s,dfv);
	    if(q->oV){
		c=copy_var(q->oV,dfv);
		if(q->on_reduced_list && c){
		    debug_print(s,q->oL,dfv,c);
		    assert(0);
		}
	    }else
	      c=0;
	    q->oL->out_action(s,dfv);
	    break;
	  case CFGnode_unreachable:
	    assert(0);
	}
	debug_print(s,q->oL,dfv,c);
	ch |= c;
    }
    delete_var(dfv);
    print("n_propagate end\n");
    return ch;
}

/* Make one pass over the graph */
int Sprob_switches::n_one_pass(CFinfo **list, int length, Svar *dfv)
{
    int ch=0,c;
    register int i;
    register CFinfo *cfi;
    register n_Seqn *q;
    register Snode *s;
    register Svar *v;
    print("one_pass start\n");
    if(checking){
	for(i=0;i<length;i++){
	    cfi = list[i];
	    s=cfi->node;
	    q = &equations[cfi->unique];
	    switch(cfi->_kind){
	      case CFGnode_unreachable:
		assert(0);
		break;
	      case CFGnode_normal:
		    /*
		      ASSERTION --- If this node doesn\'t have an ancestor than
		      the previous node on the list was the predecessor.
		      */
		if(!q->ancestor)
		  ;
		else {
		    v=equations[q->ancestor->unique].oV;
		    copy_var(dfv,v);
		}
		q->L->flow(s,dfv);
		c= q->oV ? copy_var(q->oV,dfv) : 0;
		break;
	      default:
		if(!q->ancestor)
		  meet(dfv,s);
		else {
		    v=equations[q->ancestor->unique].oV;
		    copy_var(dfv,v);
		}
		q->L->flow(s,dfv);
		c =  copy_var(q->oV,dfv);
	    }
	    ch |= c;
	    debug_print(s,q->L,dfv,c);
	}
    } else {
	assert(0);	/* FOR NOW */
    }
    print("one_pass end\n");
    return ch;
}

int Sprob_switches::n_iterate(CFinfo **list,int length,int max_iter)
{
    int c;
    int iter=0;
    changed=1;
    iteration=0;
    Svar *dfv=new_var();
    do {
	start_iteration(dfv);
	c=n_one_pass(list,length,dfv);
	iter++;
	iteration++;
	changed=0;
    } while (c && --max_iter > 0);
    delete_var(dfv);
    return iter;
}

int Sprob_switches::n_solve(int max_iter)
{
    int iter, i, n, needs_iteration, reduced_size;
    register CFinfo *cfi;
    register n_Seqn *q;
    CFinfo **reduced_list, **reduced_list_base;
    _init_tables();
    if(n_states>1 && !graph->apply_iteration_only()){
	n = cnt =graph->n_nodes - graph->n_unreachables;
	equations=new n_Seqn[graph->n_nodes];
	reduced_list_base = new CFinfo *[n];
	n_simplify();
	reduced_list = n_reduce(reduced_list_base+n,&reduced_size,
				&needs_iteration);
	print_graph(reduced_list,reduced_size);
	iter = n_iterate(reduced_list,reduced_size,
			 needs_iteration ? max_iter : 1);
	rcnt = reduced_size;
	if(iter<max_iter)
	  n_propagate();
	for(i=0;i<reduced_size;i++){
	    cfi=reduced_list[i];
	    q = &equations[cfi->unique];
	    if(q->aL)
	      delete q->aL;
	}
	delete reduced_list_base;
    } else {
      n_initialize_equations();
      iter = n_iterate(graph->modified_order,cnt,max_iter);
      if(iter<max_iter)
	n_propagate();
    }
    return iter;
}

/*

Traverse the graph in the order in which the node was entered.  That
is, treat the graph as if it was one basic block. This routine visits
all nodes, including unreachable ones.

NOTE FOR FUTURE IMPROVEMENT: Merge n_initialize_equations into
for loop.

*/
void Sprob_switches::n_walk_once()
{
  register Svar *dfv;
  register CFinfo *cfi;
  register n_Seqn *q;
  register Snode *s;
  register int i;

  n_initialize_equations();
  dfv = new_var();
  start_iteration(dfv);
  for(i=0;i<graph->n_nodes;i++)
    {
      cfi = &graph->numbers[i];
      q = &equations[i];
      s=cfi->node;
      q->L->flow(s,dfv);
      if(q->oV)
	copy_var(q->oV,dfv);
      debug_print(s,q->L,dfv,1);
    }
  delete_var(dfv);
}

int Sprob_switches::n_simplify_region(int i,int lim,CFinfo **cfi_array)
{
    CFinfo *h, *cfi;
    n_Seqn *q, *bbq;
    Snode *last_h;
    Strans *P,*Q;
    Snode *s, *bbn,**deps;
    int d,nd;
    if(i==lim)
      return i;
    h=cfi_array[i];
    switch(h->_kind){
      case CFGnode_header:
	i++;
	while(i<lim){
	    cfi=cfi_array[i];
	    if(cfi->highpt!=h)
	      break;
	    if(cfi->_kind!=CFGnode_normal){
		i=n_simplify_region(i,lim,cfi_array);
		continue;
	    }
	    i++;
	    assert(0);
	    P=Q=&strans_top;
	    q=&equations[cfi->unique];
	    deps=dependents2(cfi->node,&nd);
	    for(d=0;d<nd;d++){
		s=deps[d];
		if(graph->numbers[s->unique]._kind==CFGnode_unreachable)
		  continue;
		P=_MEET_ux(P,_TIMES_xs(1,n_eval(s,h->node),s,q->aL,cfi->node),
			   cfi->node);
	    }
	    n_update(cfi,P,Q);
	    n_link(h,cfi);
	}
	break;
      case CFGnode_bb:
	i++;
	/* unrolled one iteration, and then bbq==0 */
	if(i>=lim)
	  break;
	cfi=cfi_array[i];
	Q= &strans_top;
	bbq=0;
	if(cfi->highpt!=h)
	  break;
	i++;
	q=&equations[cfi->unique];
	deps=dependents2(cfi->node,&nd);
	assert(nd==1);
	s=deps[0];
	assert(s==h->node);
	P=_TIMES_xs(1,&strans_identity,s,q->aL,cfi->node);
	n_update(cfi,P,Q);
	n_link(h,cfi);
	bbq=q;
	last_h=h->node;
	bbn=cfi->node;
	while(i<lim){
	    cfi=cfi_array[i];
	    if(cfi->highpt!=h)
	      break;
	    q= &equations[cfi->unique];
	    i++;
	    deps=dependents2(cfi->node,&nd);
	    assert(nd==1);
	    P=_TIMES_xs(0,bbq->aL,bbn,q->aL,cfi->node);
	    if(P==&strans_bottom){
		last_h=bbn;
	    } else {
		if(P==bbq->aL)
		  bbq->aL=0;
		n_update(cfi,P,Q);
	    }
	    n_link(&graph->numbers[last_h->unique],cfi);
	    bbq=q;
	    bbn=cfi->node;
	}
	break;
      case CFGnode_irreducible:
	i++;
	while(i<lim){
	    cfi=cfi_array[i];
	    if(cfi->highpt!=h)
	      break;
	    if(cfi->_kind!=CFGnode_normal){
		i=n_simplify_region(i,lim,cfi_array);
		continue;
	    }
	    i++;
	    n_update(cfi,&strans_bottom,&strans_bottom);
	    n_link(h,cfi);
	}
	break;
      case CFGnode_normal:
	i++;
	break;
      case CFGnode_unreachable:
	assert(0);
    }
    deps=dependents2(h->node,&nd);
    q=&equations[h->unique];
    switch(nd){
      default: // d>1
	P=Q=&strans_top;
	for(d=0;d<nd;d++){
	    s=deps[d];
	    cfi=&graph->numbers[s->unique];
	    if(cfi->_kind==CFGnode_unreachable)
	      continue;
	    if(!n_is_cycle(h,cfi)){
		switch(P->kind){
		  case Sk_top:
		    P=_TIMES_xs(0,&strans_identity,0,
				n_eval(s,h->highpt->node),s);
		    break;
		  case Sk_bottom:
		    goto done;
		  default:
		    P=_MEET_ux(P,n_eval(s,h->highpt->node),s,1);
		    break;
		}
	    } else {
		switch(Q->kind){
		  case Sk_top:
		    Q=_TIMES_xs(0,&strans_identity,0,
				n_eval(s,h->node),s);
		    break;
		  case Sk_bottom:
		    goto done;
		  default:
		    Q=_MEET_ux(Q,n_eval(s,h->node),s,1);
		    break;
		}
	    }
	}
      done:
	    /* assert P!=&strans_top */
	P=_TIMES_xs(0,P,0,q->aL,h->node);
	if(Q!=&strans_top)
	      /* has a back-edge */
	  Q=_TIMES_xs(0,Q,0,q->aL,h->node);
	break;
      case 1:
	s=deps[0];
	cfi=&graph->numbers[s->unique];
	assert(cfi->_kind!=CFGnode_unreachable);
	assert(!n_is_cycle(h,cfi));
	P = _TIMES_xs(1,n_eval(s,h->highpt->node),s,q->aL,h->node);
	Q = &strans_top;
	break;
      case 0:
	P=Q=&strans_top;
    }

    n_update(h,P,Q);
    if(h->highpt)
      n_link(h->highpt,h);
    else
      n_keep(h);
    return i;
}

int Sprob_switches::n_is_cycle(CFinfo *to,CFinfo *from)
{
    CFinfo *hi=from->highpt;
    if(hi==0)
      return 0;
    if(hi==to)
      return 1;
    else
      return n_is_cycle(to,hi);
}

/*

Mark a node as kept --- The node becomes part of the reduced graph

*/
void Sprob_switches::n_keep(CFinfo *cfi)
{
    n_Seqn *q = &equations[cfi->unique];
    Snode **deps;
    int i,nd;
    deps=dependents2(cfi->node,&nd);
    q->keep |= 1;
    switch(cfi->_kind){
      case CFGnode_normal:
	assert(nd==1);
	equations[deps[0]->unique].keep = 1;
	break;
      default:
	for(i=0;i<nd;i++)
	  equations[deps[i]->unique].keep = 1;
	break;
    }
}

/*

The two member functions, n_link and n_update, builds the ancestor
tree.  The function, n_eval, computes transfer functions from the
ancestor tree.  The function, n_path_compress, modifies the ancestor
tree to improve lookups.

*/

void Sprob_switches::n_link(CFinfo *hd,CFinfo *cfi)
{
    n_Seqn *q= &equations[cfi->unique];
    if(q->aL!=&strans_bottom){
	q->ancestor=hd->node;
	switch(cfi->_kind){
	  case CFGnode_bb:
	  case CFGnode_normal:
	    break;
	  case CFGnode_unreachable:
	    assert(0);
	  default:
		/*
		  9/22/92 --- I think this isn\'t necessary.  But watch
		  out for code in ndstore.dflow and also possibly in propagate
		  and iterate.
		  */
	    n_keep(cfi);
	}
    }else
      n_keep(cfi);
}

void Sprob_switches::n_update(CFinfo *cfi,Strans *P,Strans *Q)
{
    n_Seqn *q= &equations[cfi->unique];
    Strans *l0;
    Strans *l=_TIMES_xs(0,P,0,l0=_STAR_u(Q,0),0);
    delete l0;
    switch(l->kind){
      case Sk_top: case Sk_bottom:
	q->aL=&strans_bottom;
	if(l!=P)
	  delete P;
	break;
      default:
	q->aL=l;
    }
}

Strans *Sprob_switches::n_eval(Snode *s,Snode *r)
{
    n_Seqn *q= &equations[s->unique];
    if(q->ancestor!=0)
      n_path_compress(s);
    if(r){
	if(q->ancestor==r)
	  return q->aL;
	else if(s==r)
	  return &strans_identity;
	else	/* we don't know */
	  return &strans_bottom;
    }else
      return &strans_bottom;
}

Strans *Sprob_switches::n_eval(Snode *s)
{
    n_Seqn *q= &equations[s->unique];
    CFinfo *cfi = &graph->numbers[s->unique];
    if(q->ancestor!=0)
      n_path_compress(s);
    if(q->ancestor==0){
	switch(cfi->_kind){
	  case CFGnode_header:
	  case CFGnode_bb:
	    return &strans_identity;
	  case CFGnode_unreachable:
	    assert(0);
	  case CFGnode_normal:
	    return &strans_bottom;
	  default:
	    assert(0);
	}
    }
    return q->aL;
}

void Sprob_switches::n_path_compress(Snode *s)
{
    Strans *nff;/* new flow function */
    n_Seqn *q= &equations[s->unique];
    Snode *a = q->ancestor;
    n_Seqn *aq = &equations[a->unique];
    if(q->stop_compression)
      return;
    if(aq->ancestor){
	aq = &equations[a->unique];
	n_path_compress(a);
	nff = _TIMES_xs(1,aq->aL,a,q->aL,s);
	    /*
	      try to compress up the header tree
	      If we fail then compress where as far up as we can
	      */
	switch(nff->kind){
	  case Sk_bottom:
		/* disable path compression */
	    q->stop_compression=1;
	    return;
	  case Sk_top:
	  case Sk_identity:
		/*
		  Why do I disallow identity
		  */
	    assert(0);
	  default:
	    delete q->aL;
	    q->aL = nff;
	    q->ancestor = aq->ancestor;
	}
    }
}

void Sprob_switches::n_simplify()
{
    register CFinfo *cfi;
    register n_Seqn *q;
    register int i;
    int n;
    int old_size=graph->n_nodes-graph->n_unreachables;
    CFinfo **old_list = graph->modified_order;
    
    /* a quick check */
    if(old_size==0)
      return;
    n=graph->n_nodes;
    for(i=0;i<n;i++){
	cfi= &graph->numbers[i];
	q = &equations[cfi->unique];
	q->oV=0;
	q->L = q->oL = _FLOW_MAP(cfi->node,cfi->_kind);
	q->aL = q->L;
	q->ancestor=0;
	q->stop_compression = q->keep = q->on_reduced_list =0;
    }
	/*
	  Simplify all the strongly connected regions
	  */
    for(i=0;i<old_size;)
      i=n_simplify_region(i,old_size,old_list);
}

CFinfo **Sprob_switches::n_reduce(CFinfo **new_list,
				   int *reduced_size,
				   int *needs_iteration)
{
    register CFinfo *cfi;
    CFinfo **new_list_end, **old_list;
    register n_Seqn *q;
    register int i;
    int old_size;

    old_list = graph->modified_order;
    old_size = graph->n_nodes-graph->n_unreachables;
	/* a quick check */
    *reduced_size=0;
    *needs_iteration=0;
    if(old_size==0){
	*needs_iteration = 1;
	return new_list;
    }
	/*
	  Scan backwards and determine all nodes that form the
	  reduced graph.
	  */
    new_list_end = new_list;
    for(i=old_size;i>0;){
	cfi=old_list[--i];
	q= &equations[cfi->unique];
	switch(cfi->_kind){
	  case CFGnode_header:
	  case CFGnode_irreducible:
	    if(q->ancestor){
		assert(q->aL!=&strans_bottom);
		equations[q->ancestor->unique].keep = 1;
		q->keep = 1;
	    }else if(i>0)
	      *needs_iteration=1;
	    assert(q->L->kind!=Sk_top);
	    if(q->aL!=&strans_bottom)
	      q->L=q->aL;
	    q->oV=new_var();
	    --new_list;
	    *new_list = cfi;
	    q->on_reduced_list=1;
	    break;
	  case CFGnode_normal:
	  case CFGnode_bb:
	    if(q->keep)
	      if(q->ancestor){
		  assert(q->aL!=&strans_bottom);
		  equations[q->ancestor->unique].keep = 1;
	      }
	    if(q->keep){
		if(q->aL!=&strans_bottom)
		  q->L=q->aL;
		    /*
		      I think that I may not need this new_var() if I
		      modified the way that I fetch predecessor values for
		      those nodes with non-zero ancestors.  See n_iterate
		      */
		q->oV=new_var();
		--new_list;
		*new_list = cfi;
		q->on_reduced_list = 1;
	    } else {
		if(q->aL && NONTERMINAL(q->aL))
		  delete q->aL;
		q->aL=0;
	    }
	    if(q->oV==0 && cfi->needs_var)
		q->oV=new_var();
	    break;
	  case CFGnode_unreachable:
	  default:
	    assert(0);
	}
    }
    *reduced_size= new_list_end - new_list;
    return new_list;
}

static char *_kind_names[] =
{
    "unrch ", "normal", "bblock","header", "irred ",
};

static void print_ff(Strans *ff,char *suffix)
{
    if(ff)
      fprintf(stderr,"%d(%p)%s",ff->kind,ff,suffix);
    else
      fprintf(stderr,"0%s",suffix);
}

void Sprob_switches::print_graph(CFinfo **g,int size)
{
    if(debug_flags){
	int i;
	CFinfo *ui;
	n_Seqn *uq;
	for(i=0;i<size;i++){
	    ui=g[i];
	    uq = &equations[ui->unique];
	    fprintf(stderr,"%5d %p %6s %c ",ui->unique,ui->node,
		    _kind_names[ui->_kind - CFGnode_unreachable],
		    ui->needs_var ? 'v' : '-');
	    if(uq->ancestor)
	      fprintf(stderr,"%6d ",uq->ancestor->unique);
	    else
	      fprintf(stderr,"------ ");
	    print_ff(uq->L,"\t");
	    print_ff(uq->aL,"\t");
	    print_ff(uq->oL,"\n");
	}
    }
}

void Sprob_switches::check_node(Snode *s, CFinfo **cfip, n_Seqn **qp)
{
  CFinfo *cfi;
  n_Seqn *q;

  cfi = &graph->numbers[s->unique];
  q = &equations[s->unique];
  assert(cfi->node==s);
  assert(cfi->unique==s->unique);

  *cfip = cfi;
  *qp = q;
}

void Sprob_switches::must_have_var(Snode *s)
{
  CFinfo *cfi;
  n_Seqn *q;

  check_node(s,&cfi,&q);
  if(q->oV==0)
    q->oV = new_var();
}

Svar *Sprob_switches::n_get_var(Snode *s)
{
  CFinfo *cfi;
  n_Seqn *q;

  check_node(s, &cfi, &q);
  return q->oV;
}
