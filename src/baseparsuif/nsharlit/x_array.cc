/* file "x_array.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Extendible Arrays (implementation) */

#pragma implementation

#include <assert.h>
#include "x_array.h"

X_array0::X_array0(int sz)
{
    frozen=0;
    size=sz;
    hi=0;
    data=new void *[sz];
}

X_array0::~X_array0()
{
    delete data;
}

void *&X_array0::operator[](int i)
{
    if(i<hi)
      return data[i];
    else{
	assert(0);
	return data[0];
    }
}

void X_array0::operator=(int)
{
    assert(0);
}

int X_array0::extend(void *e)
{
    if(hi==size){
	int i=hi;
	void **temp,**dst, **src=data;
	int nsz=size ? 2*size : 1;
	temp=dst=new void *[nsz];
	size=nsz;
	assert(!frozen);
	while(i>0){
	    *dst++=*src++;
	    i--;
	}
	delete data;
	data=temp;
    }
    assert(hi<size);
    data[hi]=e;
    hi++;
    return hi-1;
}

void X_array0::expand(int nsz)
{
    if(nsz<=hi)
      return;
    if(nsz>size){
	int i=hi;
	void **temp,**dst, **src=data;
	temp=dst=new void *[nsz];
	size=nsz;
	assert(!frozen);//for now
	while(i>0){
	    *dst++=*src++;
	    i--;
	}
	delete data;
	data=temp;
    }
}

void X_array0::grow(int nsz)
{
    expand(nsz);
    hi=nsz;
}

int
X_array0::ub()
{
    return hi;
}

void
X_array0::grab_from(X_array0 &r)
{
    delete data;
    size=r.size;
    hi=r.hi;
    data=r.data;
    r.size=0;
    r.hi=0;
    r.data=0;
}



