/* file "sstring.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Segmented Strings (Implementations) */

#include <suif1/misc.h>
#include "sstring.h"

String::String(char *s)
{
  char c;
  while ((c = *s++))
    store(c);
}

void String::store(char c)
{
  if(!last()->willfit())
    glist::append(new String_seg);
  last()->text[last()->length++]=c;
}

/*##
frag(sstring_append,"Append a string onto ourself")
tex @{
This functions destroys its argument.
@}

*/
void
String::append(String *s)
{
	while(!s->is_empty())
		glist::append(s->pop());
}

/*##
frag(sstring_print,"Print the String")

*/
void
String::print(FILE *fp)
{
	String_iter si(this);
	while(!si.is_empty())
		putc(si.step(),fp);
}

/*##
frag(sstring_step,"Step along the String")

*/
char
String_iter::step()
{
	if(current()==0 || next_index>=current()->length){
		next_index=0;
		glist_iter::step();
		return step();
	} else return current()->text[next_index++];
}
