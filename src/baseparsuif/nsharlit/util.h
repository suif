/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#define TAG(t) TAG_ ## t
#define TAG_CHECK(t,x) ((t *)(x)->check(TAG(t)))
#define TAG_IS(t,x) ((t *)(x)->is(TAG(t)))
#define TAG_CASE(t) case TAG(t)
#define TAG_ASSERT(t,x) ((void)TAG_CHECK(t,x))
