/* file "pgen.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Find parallel regions */

#pragma implementation "pgen.h"
#define RCS_BASE_FILE pgen_cc

#include "pgen.h"
#include "par_region.h"

#include <useful.h>
#include <ist.h>
#include <transform.h>
#include <dependence.h>
#include <builder.h>
#include <ipmath.h>
#include <runtime_names.h>

RCS_BASE("$Id: pgen.cc,v 1.2 1999/08/25 03:26:40 brm Exp $")

/* Public global variables */

func_type *task_type = NULL;
proc_sym *setjmp_proc = NULL;
proc_sym *doall_proc = NULL;
proc_sym *nprocs_proc = NULL;
proc_sym *doall_level_proc = NULL;
proc_sym *global_barrier_proc = NULL;
proc_sym *sync_neighbor_proc = NULL;
proc_sym *lock_proc = NULL;
proc_sym *unlock_proc = NULL;
proc_sym *assign_ids_proc = NULL;
proc_sym *speculate_begin_proc = NULL;
proc_sym *speculate_commit_proc = NULL;
proc_sym *speculate_terminate_proc = NULL;
proc_sym *par_begin_proc = NULL;
proc_sym *par_commit_proc = NULL;
proc_sym *par_terminate_proc = NULL;
proc_sym *counter_wait_proc = NULL;
proc_sym *counter_incr_proc = NULL;
proc_sym *counter_init_proc = NULL;
proc_sym *counter_set_proc = NULL;
proc_sym *counter_set_range_proc = NULL;
proc_sym *flush_putc_proc = NULL;
proc_sym *check_work_proc = NULL;
proc_sym *reduction_lock_proc = NULL;
proc_sym *reduction_unlock_proc = NULL;
proc_sym *start_packing_proc = NULL;
var_sym *any_parallel_var = NULL;
var_sym *suif_nprocs_var = NULL;
var_sym *suif_args_var = NULL;
var_sym *suif_task_var = NULL;
var_sym *suif_mynprocs_var = NULL;
var_sym *suif_mynprocs1_var = NULL;
var_sym *suif_mynprocs2_var = NULL;
var_sym *thread_map = NULL;
var_sym *mylocks = NULL;
var_sym *jmpbuf = NULL;

/* Private global variables */

/* External function declarations */

extern char *nprocs_name(int num);                           // from task.cc
extern void process_tile_annote(tree_for *tf, annote *an);   // from task.cc
extern void process_speculate_annote(tree_for *tf, TaskProc *task);

/* Public function declarations */

void parallelize(file_set_entry *fse, proc_sym *ps);
void modify_main(proc_sym *ps);
void create_runtime_symbols(file_set_entry *fse);

/* Private function declarations */

static void search_for_regions(file_set_entry *fse, tree_node_list *l);
static void process_speculate_only_annote(tree_for *tf);
static void remove_extra_annotes_tree(tree_node *tn, void *x);
static void remove_extra_annotes(tree_for *tf);
static void extend_extra_annotes(tree_block *region_block, tree_node_list *l);
static void search_for_tiles_tree(tree_node *tn, void *);
static void convert_tiles_tree(tree_node *tn, void *);
static void create_sequential_region(tree_block *region_block);

// ONE_BLOCK_INIT - moved initializations into par_region.cc to
//           enable forward propagation for data reshape optimizations

#ifdef ONE_BLOCK_INIT
static void init_block_sizes(proc_sym *ps);
#endif

/* Function specifications */

void parallelize(file_set_entry *fse, proc_sym *ps)
{
    assert(ps);

    block bldr;
    bldr.set_proc(ps->block());   // Set builder to current procedure

    // Fill in access vectors and normalize:  Must first make sure we
    // have access vectors for the entire proc
    //
    fill_in_access(ps->block(), FALSE);   

#ifdef ONE_BLOCK_INIT 
    init_block_sizes(ps);
#endif

    search_for_regions(fse, ps->block()->body());

    // Tile the code in the parent procedure for locality
    //
    if (debug['g'-'\0']) {
	printf("Tiling parent procedure for locality\n");
    }
    fill_in_access(ps->block(), FALSE);   

    if (!no_cache_tile_flag)
        ps->block()->body()->map(search_for_tiles_tree, NULL);
}


//
// Change name of "main" to SUIF_START_NAME, set SUIF_NPROCS_NAME to number
// of processors specified at compile-time.
//
void modify_main(proc_sym *ps)
{
    ps->set_name(SUIF_START_NAME);

    assert(suif_nprocs_var);

    block nprocs_bldr(suif_nprocs_var);
    block assign_bldr(nprocs_bldr = block(nprocs_flag));

    ps->block()->body()->push(assign_bldr.make_tree_node());
}

//
// Create symbols for objects from libraries:
//
void create_runtime_symbols(file_set_entry *fse)
{
    assert(type_signed);
    global_symtab *globals = fileset->globals();

    // doall_proc:
    // Create type for tasks
    //
    type_node *temp_type = block::parse_type(globals, "void (int)");
    assert(temp_type->is_func());
    task_type = (func_type *) temp_type;

    temp_type = block::parse_type(globals, 
        "void (%% *, char *, int, int, int)", task_type);
    assert(temp_type->is_func());
    func_type *doall_type = (func_type *) temp_type;
    if (!(doall_proc = globals->lookup_proc(DOALL_NAME)))
      doall_proc = globals->new_proc(doall_type, src_c, DOALL_NAME); 

    // setjmp_proc:
    //
    //setjmp() doesn't work
    //    temp_type = block::parse_type(globals, "int (void *)");
    temp_type = block::parse_type(globals, "int (int)");
    assert(temp_type->is_func());
    func_type *int_func = (func_type *) temp_type;
    //setjmp() doesn't work
    const char *curr_name = "restart";

    if (!(setjmp_proc = globals->lookup_proc(curr_name)))
      setjmp_proc = globals->new_proc(int_func, src_c, curr_name);
    
    // nprocs_proc:
    //
    temp_type = block::parse_type(globals, "int ()");
    assert(temp_type->is_func());
    func_type *int_func0 = (func_type *) temp_type;
    curr_name = (fortran_form_flag ? NPROCS_FORTRAN_NAME : NPROCS_NAME);

    if (!(nprocs_proc = globals->lookup_proc(curr_name)))
      nprocs_proc = globals->new_proc(int_func0, src_c, curr_name);
    
    // flush_putc:
    //
    if (!(flush_putc_proc = globals->lookup_proc(FLUSH_PUTC_NAME)))
	flush_putc_proc = globals->new_proc(int_func0, src_c, FLUSH_PUTC_NAME);
  
    // global_barrier_proc:
    //
    temp_type = block::parse_type(globals, "void (int *)");
    assert(temp_type->is_func());
    func_type *void_func2 = (func_type *) temp_type;

    temp_type = block::parse_type(globals, "void (int)");
    assert(temp_type->is_func());
    func_type *void_func1 = (func_type *) temp_type;

    func_type *curr_func_type = (fortran_form_flag ? void_func2 : void_func1);
    curr_name = (fortran_form_flag ? GLOBAL_BARRIER_FORTRAN_NAME :
		                     GLOBAL_BARRIER_NAME);

    if (!(global_barrier_proc = globals->lookup_proc(curr_name)))
      global_barrier_proc = globals->new_proc(curr_func_type,src_c, curr_name);

    // sync_neighbor_proc:
    //
    curr_func_type = (fortran_form_flag ? void_func2 : void_func1);
    curr_name = (fortran_form_flag ? SYNC_NEIGHBOR_FORTRAN_NAME :
                     		   SYNC_NEIGHBOR_NAME);

    if (!(sync_neighbor_proc = globals->lookup_proc(curr_name)))
      sync_neighbor_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // doall_level_proc:
    //
    curr_name = (fortran_form_flag ? DOALL_LEVEL_FORTRAN_NAME :
		                     DOALL_LEVEL_NAME);
    if (!(doall_level_proc = globals->lookup_proc(curr_name)))
        doall_level_proc = globals->new_proc(int_func0, src_c, curr_name); 

    // lock_proc:
    //
    curr_name = (fortran_form_flag ? LOCK_FORTRAN_NAME : LOCK_NAME);
    curr_func_type = (fortran_form_flag ? void_func2 : void_func1);
    if (!(lock_proc = globals->lookup_proc(curr_name)))
      lock_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // unlock_proc:
    //
    curr_name = (fortran_form_flag ? UNLOCK_FORTRAN_NAME : UNLOCK_NAME);
    curr_func_type = (fortran_form_flag ? void_func2 : void_func1);
    if (!(unlock_proc = globals->lookup_proc(curr_name)))
      unlock_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // assign_ids_proc:
    //
    temp_type = block::parse_type(globals, "void (int *, int *, int *)");
    assert(temp_type->is_func());
    func_type *void_func3 = (func_type *) temp_type;

    if (fortran_form_flag) 
      {
	curr_func_type = void_func3;
	curr_name = ASSIGN_MYIDS_FORTRAN_NAME;
      }
    else 
      {
	temp_type = block::parse_type(globals, "void (int, int *, int *)");
	assert(temp_type->is_func());
	curr_func_type = (func_type *) temp_type;
	curr_name = ASSIGN_MYIDS_NAME;
      }
    if (!(assign_ids_proc = globals->lookup_proc(curr_name)))
      assign_ids_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // check work
    // 
    if (fortran_form_flag) 
      {
	temp_type = block::parse_type(globals, 
				      "int (int *, int *, double *, int *)");
	assert(temp_type->is_func());
	curr_func_type = (func_type *) temp_type;
	curr_name = CHECK_WORK_FORTRAN_NAME;
      }
    else 
      {
	temp_type = block::parse_type(globals, "int (int, int, double, int)");
	assert(temp_type->is_func());
	curr_func_type = (func_type *) temp_type;
	curr_name = CHECK_WORK_NAME;
      }
    if (!(check_work_proc = globals->lookup_proc(curr_name)))
	check_work_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // speculate_begin_proc:
    //
    temp_type = block::parse_type(globals, "void ()");
    assert(temp_type->is_func());
    func_type *void_func0 = (func_type *) temp_type;

    curr_func_type = void_func0;
    if (fortran_form_flag) 
      {
	curr_name = SPECULATE_BEGIN_FORTRAN_NAME;
      }
    else 
      {
	curr_name = SPECULATE_BEGIN_NAME;
      }
    if (!(speculate_begin_proc = globals->lookup_proc(curr_name)))
      speculate_begin_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // speculate_commit_proc:
    //
    curr_func_type = void_func0;
    if (fortran_form_flag) 
      {
	curr_name = SPECULATE_COMMIT_FORTRAN_NAME;
      }
    else 
      {
	curr_name = SPECULATE_COMMIT_NAME;
      }
    if (!(speculate_commit_proc = globals->lookup_proc(curr_name)))
      speculate_commit_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // speculate_terminate_proc:
    //
    curr_func_type = void_func0;
    if (fortran_form_flag) 
      {
	curr_name = SPECULATE_TERMINATE_FORTRAN_NAME;
      }
    else 
      {
	curr_name = SPECULATE_TERMINATE_NAME;
      }
    if (!(speculate_terminate_proc = globals->lookup_proc(curr_name)))
      speculate_terminate_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // par_begin_proc:
    //
    temp_type = block::parse_type(globals, "void ()");
    assert(temp_type->is_func());
    curr_func_type = void_func0;
    if (fortran_form_flag) 
      {
	curr_name = PAR_BEGIN_FORTRAN_NAME;
      }
    else 
      {
	curr_name = PAR_BEGIN_NAME;
      }
    if (!(par_begin_proc = globals->lookup_proc(curr_name)))
      par_begin_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // par_commit_proc:
    //
    curr_func_type = void_func0;
    if (fortran_form_flag) 
      {
	curr_name = PAR_COMMIT_FORTRAN_NAME;
      }
    else 
      {
	curr_name = PAR_COMMIT_NAME;
      }
    if (!(par_commit_proc = globals->lookup_proc(curr_name)))
      par_commit_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // par_terminate_proc:
    //
    curr_func_type = void_func0;
    if (fortran_form_flag) 
      {
	curr_name = PAR_TERMINATE_FORTRAN_NAME;
      }
    else 
      {
	curr_name = PAR_TERMINATE_NAME;
      }
    if (!(par_terminate_proc = globals->lookup_proc(curr_name)))
      par_terminate_proc = globals->new_proc(curr_func_type, src_c, curr_name);


    // counter_wait_proc:
    //
    func_type *void_func4;
    if (fortran_form_flag) 
      {
	curr_func_type = void_func3;  // void (int *, int *, int *)
	curr_name = COUNTER_WAIT_FORTRAN_NAME;
      }
    else 
      {
	temp_type = block::parse_type(globals, "void (int, int, int)");
	assert(temp_type->is_func());
	void_func4 = (func_type *) temp_type;
	curr_func_type = void_func4;
	curr_name = COUNTER_WAIT_NAME;
      }
    if (!(counter_wait_proc = globals->lookup_proc(curr_name)))
      counter_wait_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // counter_set_proc:
    //
    curr_name = (fortran_form_flag ? COUNTER_SET_FORTRAN_NAME : 
		                     COUNTER_SET_NAME);
    curr_func_type = (fortran_form_flag ? void_func3 : void_func4);
    if (!(counter_set_proc = globals->lookup_proc(curr_name)));
      counter_set_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // counter_set_range_proc:
    //
    curr_name = (fortran_form_flag ? COUNTER_SET_RANGE_FORTRAN_NAME : 
		                     COUNTER_SET_RANGE_NAME);
    curr_func_type = (fortran_form_flag ? void_func3 : void_func4);
    if (!(counter_set_range_proc = globals->lookup_proc(curr_name)));
      counter_set_range_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // counter_incr_proc:
    //
    if (fortran_form_flag) 
      {
	temp_type = block::parse_type(globals, "void (int *, int *)");
	assert(temp_type->is_func());
	curr_func_type = (func_type *) temp_type;
	curr_name = COUNTER_INCR_FORTRAN_NAME;
      }
    else 
      {
	temp_type = block::parse_type(globals, "void (int, int)");
	assert(temp_type->is_func());
	curr_func_type = (func_type *) temp_type;
	curr_name = COUNTER_INCR_NAME;
      }
    if (!(counter_incr_proc = globals->lookup_proc(curr_name)))
      counter_incr_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // counter_init_proc:
    //
    curr_name = (fortran_form_flag ? COUNTER_INIT_FORTRAN_NAME :
		                     COUNTER_INIT_NAME);
    curr_func_type = (fortran_form_flag ? void_func2 : void_func1);

    if (!(counter_init_proc = globals->lookup_proc(curr_name)))
      counter_init_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // reduction_lock_proc:
    //
    curr_name = (fortran_form_flag ? REDUCTION_LOCK_FORTRAN_NAME : REDUCTION_LOCK_NAME);
    curr_func_type = (fortran_form_flag ? void_func2 : void_func1);
    if (!(reduction_lock_proc = globals->lookup_proc(curr_name)))
      reduction_lock_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // reduction_unlock_proc:
    //
    curr_name = (fortran_form_flag ? REDUCTION_UNLOCK_FORTRAN_NAME : REDUCTION_UNLOCK_NAME);
    curr_func_type = (fortran_form_flag ? void_func2 : void_func1);
    if (!(reduction_unlock_proc = globals->lookup_proc(curr_name)))
      reduction_unlock_proc = globals->new_proc(curr_func_type, src_c, curr_name);

    // suif_nprocs_var:
    // Runtime library needs this var -- it is *declared* and *defined* here.
    // 
    suif_nprocs_var = globals->new_var(type_signed, SUIF_NPROCS_NAME);
    var_def *def = new var_def(suif_nprocs_var, get_alignment(type_signed));
    fse->symtab()->add_def(def);

    // suif_aligned_my_nprocs{1,2}: 
    // Declared extern here, defined in runtime
    //
    temp_type = globals->install_type(new modifier_type(TYPE_VOLATILE,
						       type_signed->ptr_to()));
    suif_mynprocs_var = globals->lookup_var(SUIF_MYNPROCS_NAME);
    suif_mynprocs1_var = globals->lookup_var(SUIF_MYNPROCS1_NAME);
    suif_mynprocs2_var = globals->lookup_var(SUIF_MYNPROCS2_NAME);

    if (!suif_mynprocs_var)
      suif_mynprocs_var = globals->new_var(temp_type, SUIF_MYNPROCS_NAME);
    if (!suif_mynprocs1_var)
      suif_mynprocs1_var = globals->new_var(temp_type, SUIF_MYNPROCS1_NAME);
    if (!suif_mynprocs2_var)
      suif_mynprocs2_var = globals->new_var(temp_type, SUIF_MYNPROCS2_NAME);

    // thread_map:
    //
    array_type *atype = new array_type(type_signed, array_bound(0), array_bound(47));
    atype = (array_type *)globals->install_type(atype);
    thread_map = globals->new_var(atype, THREAD_MAP_NAME);

    // mylocks:
    //
    array_type *atype1 = new array_type(type_signed, array_bound(0), array_bound(31));
    atype1 = (array_type *)globals->install_type(atype1);
    array_type *atype2 = new array_type(atype1, array_bound(0), array_bound(47));
    atype2 = (array_type *)globals->install_type(atype2);
    mylocks = globals->new_var(atype2, "_mylocks");
    
    //setjmp() doesn't work
    // jmpbuf:
    //
    //    ptr_type *ptype = new ptr_type(type_signed);
    //    temp_type = globals->install_type(ptype);
    //    assert(temp_type->is_ptr());
    //    jmpbuf = globals->new_var(type_ptr, "jmpbuf");

    // any_parallel_io:
    // Declared extern here, defined in I77_doall
    // 
    any_parallel_var = globals->new_var(type_signed, ANY_PARALLEL_NAME);

    // _suif_aligned_args and _suif_aligned_task_f:
    // Declared extern here, defined in runtime
    //
    temp_type = globals->install_type(new modifier_type(TYPE_VOLATILE,
							type_void->ptr_to()));
    suif_args_var = globals->new_var(temp_type, SUIF_ARGS_NAME);
    temp_type = globals->install_type(new modifier_type(TYPE_VOLATILE,
					       task_type->ptr_to()->ptr_to()));
    suif_task_var = globals->new_var(temp_type, SUIF_TASK_NAME);

    //
    temp_type = block::parse_type(globals, "void (%% *, char *)", task_type);
    assert(temp_type->is_func());
    if (!(start_packing_proc = globals->lookup_proc(START_PACKING_NAME)))
      start_packing_proc = globals->new_proc((func_type *) temp_type, src_c,
					     START_PACKING_NAME);
}


static void search_for_regions(file_set_entry *fse, tree_node_list *l)
{
    assert(l);
    tree_node_list_e *cur_node_e = l->head();
    tree_node_list_e *next_node_e;
    tree_node *cur_node;
    suif_object *cur_object;
    const char *exec_mask_str = NULL;

    while (cur_node_e) {
	cur_node = cur_node_e->contents;
        next_node_e = cur_node_e->next();

 	if (cur_node->is_instr())  { 
	    cur_object = ((tree_instr *) cur_node)->instr();
 	} 
	else {
 	    cur_object = cur_node;
 	}

	boolean is_doall = FALSE;
	boolean is_par_region = FALSE;
	boolean is_doall_region = FALSE;
	boolean is_doacross = FALSE;
	boolean is_pipeline = FALSE;
	int num_pipeline_counters = 0;
	annote *an;
	immed_list *condition_list = new immed_list;

	//
	// Look at the annotations and figure out what kind of region
	//
        if ((an = cur_object->annotes()->peek_annote(k_doall)))
	  {
	    is_doall = TRUE;
	    immed_list *iml = an->immeds();
	    
	    while (!iml->is_empty())
	      {
		immed im = iml->pop();
		if (im.is_string())
		  {
		    exec_mask_str = im.string();

		    for (const char *s = exec_mask_str; *s; s++) 
		      {
			if (mask[*s-'\0'] == 0) is_doall = FALSE;
		      }
		  }
		else if (im.is_instr())
		  {
		    condition_list->append(im);
		  }
	      }
	}
        else if (cur_object->annotes()->peek_annote(k_begin_parallel_region)) 
	    is_par_region = TRUE;

        else if (cur_object->annotes()->peek_annote(k_doall_region))
	    is_doall_region = TRUE;

	else if (cur_object->annotes()->peek_annote(k_doacross)) {
	    // Make sure we can tile by checking to see if there is a
	    // tileable annotation.
	    //
	    an = cur_object->annotes()->get_annote(k_tileable_loops);
	    if (an) {
		int depth = (*an->immeds())[0].integer();
	        if (depth > 1) {
		    immed_list *iml = new immed_list();
		    iml->append(depth);
		    int tile_size = (*an->immeds())[1].integer();
		    if (tile_size == 1)  iml->append(immed(1));
		    else  iml->append(immed(pipeline_grain_flag));

		    for (int i = 1; i < depth; i++) {
			iml->append(pipeline_grain_flag);
		    }

		    delete an;
		    an = new annote(k_tile_loops);
		    an->set_immeds(iml, cur_object);
		    cur_object->annotes()->append(an);
		    is_doacross = TRUE;
		}
	    }
        }
	// quick hack
	/*else*/ if ((an = cur_object->annotes()->peek_annote(k_pipeline))) {
	    is_pipeline = TRUE;
            num_pipeline_counters = (*an->immeds())[0].integer();
	}
	// This is the case where we generate just calls to the runtime
	// library
	if ((an = cur_node->annotes()->peek_annote(k_speculate))) {
	    //	  if (!cur_node->annotes()->peek_annote(k_doall)) {
	    if (!cur_node->annotes()->peek_annote(k_pipeline)) {
	      assert_msg(cur_node->kind() == TREE_FOR, 
			 ("Found `k_speculate' on node that's not a tree_for"));
	      process_speculate_only_annote((tree_for *)cur_node);
	    }
	    //	  }
	}


	//
	// Now process the different kinds of parallel regions
	//
	if (is_doall || is_doacross) {
	    assert_msg(cur_node->kind() == TREE_FOR, 
	      ("Found `doall' or `doacross' on node that's not a tree_for"));
	    
	    tree_for *tf = (tree_for *) cur_node;

	    // Kill off reduction on arrays
	    //
	    if (no_array_reductions_flag) 
	      {
		annote_list_iter ali(tf->annotes());
		while (!ali.is_empty() && (is_doall || is_doacross))
		  {
		    annote *an = ali.step();
		    if (an->name() != k_reduced) continue;

		    sym_node *sn = (*an->immeds())[1].symbol();
		    var_sym *reduce_var = (var_sym *) sn;
		    type_node *true_type = reduce_var->type()->unqual();

		    if (true_type->peek_annote(k_call_by_ref)) 
		      {
			assert(true_type->is_ptr());
			true_type = 
			  ((ptr_type *) true_type)->ref_type()->unqual();
		      }
		    if (true_type->is_array())
		      {
			is_doall = is_doacross = FALSE;

			if (debug['g'-'\0']) 
			  printf("Killing array reduction on %s at line #%d\n",
			    reduce_var->name(), source_line_num(cur_node));
		      }
		  }
	      }

	    if (is_doall || is_doacross) 
	      {
		// Strip nested doall,reduced annotes and 
		// Convert tileable_loops -> tile_loops annotes
		//
		boolean found_doall = TRUE;
		tf->body()->map(remove_extra_annotes_tree, &found_doall);
		if (!no_cache_tile_flag) {
		    convert_tiles_tree(tf, NULL);
		    tf->body()->map(convert_tiles_tree, NULL);
		}

		if (print_line_flag) {
		  printf("\nProc %s: parallelizing loop %s at line #%d\n", 
			 cur_node->proc()->name(), tf->index()->name(), 
			 source_line_num(cur_node));
		  
		}
	      }
	}
	if (is_par_region && print_line_flag) {
	    printf("\nProc %s: parallelizing region starting at line #%d\n", 
		   cur_node->proc()->name(), source_line_num(cur_node));
        }
	if (is_doall_region) {
	    assert_msg(cur_node->kind() == TREE_FOR, 
	    ("Found `doall_region' annotation on node that's not a tree_for"));

            // Strip any nested doall and reduced annotations and 
            // Convert tileable_loops -> tile_loops annotes
            //
	    tree_for *tf = (tree_for *) cur_node;
	    remove_extra_annotes(tf);
	    boolean found_doall = FALSE;
	    tf->body()->map(remove_extra_annotes_tree, &found_doall);
	    if (!no_cache_tile_flag) {
	        convert_tiles_tree(tf, NULL);
	        tf->body()->map(convert_tiles_tree, NULL);
	    }

	    if (print_line_flag) {
		printf("\nProc %s: parallelizing loop %s at line #%d\n", 
		    cur_node->proc()->name(), tf->index()->name(), 
		    source_line_num(cur_node));
            }
        }
	/*else*/ if (is_pipeline) {
	    assert_msg(cur_node->kind() == TREE_FOR, 
	      ("Found `pipeline' on node that's not a tree_for"));

            // Strip any nested doall and reduced annotations:
            //
	    tree_for *tf = (tree_for *) cur_node;
	    boolean found_doall = TRUE;
	    tf->body()->map(remove_extra_annotes_tree, &found_doall);

	    if (print_line_flag) {
		printf("\nProc %s: pipelining loop %s at line #%d\n", 
		    cur_node->proc()->name(), tf->index()->name(), 
		    source_line_num(cur_node));
            }
	}
 
	if (is_doall || is_doacross || is_par_region || is_doall_region || is_pipeline) {
            // Create block to put region in, and insert block into current
            // list at the parallel region's position.
            //
	    block_symtab *btab = new block_symtab("parallel_region_body"); 
	    base_symtab *enclosing_tab = cur_node->scope();
	    enclosing_tab->add_child(btab);
            tree_node_list *tl = new tree_node_list();
            tree_block *region_block = new tree_block(tl, btab);
            l->insert_before(region_block, cur_node_e);

            // Move all tree_nodes within the region into the region's block
            //
	    tree_node *cur_clone = cur_node->clone(region_block->symtab());
            l->remove(cur_node_e);

            region_block->body()->append(cur_clone);
	    delete cur_node;
	    cur_node = cur_clone;

	    if (cur_node->is_instr())  { 
		cur_object = ((tree_instr *) cur_node)->instr();
	    } 
	    else {
		cur_object = cur_node;
	    }

	    if (is_par_region) { 
		boolean found_end_region = FALSE;
                if (cur_object->annotes()->peek_annote(k_end_parallel_region))
		    found_end_region = TRUE;

                while (!found_end_region && next_node_e) {
		    cur_node_e = next_node_e;
		    cur_node = cur_node_e->contents;

		    if (cur_node->is_instr())  { 
			cur_object = ((tree_instr *) cur_node)->instr();
		    } 
		    else {
			cur_object = cur_node;
		    }

		    next_node_e = cur_node_e->next();

		    tree_node *cur_clone = 
			cur_node->clone(region_block->symtab());
                    l->remove(cur_node_e);

                    region_block->body()->append(cur_clone);
		    delete cur_node;
		    cur_node = cur_clone;

		    if (cur_node->is_instr())  { 
			cur_object = ((tree_instr *) cur_node)->instr();
		    } 
		    else {
			cur_object = cur_node;
		    }

		    if (cur_object->annotes()->peek_annote(
							k_end_parallel_region))
			found_end_region = TRUE;
                }

		assert_msg(found_end_region, 
		    ("Found `begin_parallel_region' annote with no " \
		     "`end_parallel_region'"));
		if (print_line_flag) {
		    printf("Proc %s: region ends at line #%d\n", 
	  	        cur_node->proc()->name(), source_line_num(cur_node));
		}
            }

	    // Expand region to include any HPF annotations on mrks outside
            //
	    extend_extra_annotes(region_block, l);

	    // Add conditional annotation to the region for conditional
	    // parallelism
	    if (!condition_list->is_empty()) 
	      {
		an = new annote(k_doall_conditions, NULL);
		an->set_immeds(condition_list, region_block);
		region_block->annotes()->append(an);
	      }

	    if (((region_num + 1) < num_feedback_doalls) &&
		(feedback_info[region_num+1] <= 1)) {

	      create_sequential_region(region_block);

	      if (print_line_flag) {
		printf("** suppressed **\n");
	      }
	    } else {

	      // Create parallel region
	      //
	      ParallelRegion par_region(fse, region_block->proc(), 
					region_block, exec_mask_str);

	      if (debug['g'-'\0']) {
  	        printf("  Creating a parallel region...\n");
	      }

	      par_region.construct_region(num_pipeline_counters);
	      par_region.write_task();
	    }
        }

	else {

          switch (cur_node->kind()) {

            case TREE_BLOCK: {
              search_for_regions(fse, ((tree_block *)cur_node)->body());
              break;
            }

            case TREE_LOOP: {
      	      search_for_regions(fse, ((tree_loop *)cur_node)->body());
  	      break;
            }

            case TREE_FOR: {
  	      search_for_regions(fse, ((tree_for *)cur_node)->body());
              break;
	    }

            case TREE_IF: {
	      search_for_regions(fse, ((tree_if *)cur_node)->then_part());
	      search_for_regions(fse, ((tree_if *)cur_node)->else_part());
      	      break;
    	    }

            case TREE_INSTR:
              break;
          }
        }

        cur_node_e = next_node_e;
    }
}

static void add_speculate_annote(tree_node *the_tree, 
				 int speculate_id,
				 int spec_type)
{
  annote *an = new annote(k_speculate_id);
  an->set_immeds(new immed_list(immed(speculate_id), 
				immed(spec_type)), NULL);

  if (the_tree->kind() == TREE_FOR) {
    the_tree->annotes()->append(an);
    return;
  }
    
  assert(the_tree->kind() == TREE_INSTR);
  instruction *i = ((tree_instr *)the_tree)->instr();
  assert(i->opcode() == io_cal);
  i->annotes()->append(an);
}

extern void replace_label(tree_node_list *tnl, label_sym *oldlab, label_sym *newlab);

//
//   ["speculate": ]
//
// Place the speculate_begin or par_begin above the region to be parallelized
//
static void process_speculate_only_annote(tree_for *tf)
{
    //    static int speculate_counter = 0;
    if (debug['g'-'\0']) {
	printf("    Adding speculate_begin() or par_begin() with speculate_id annotations to a loop with index %s\n",
	       tf->index()->name());
    }

    proc_sym *begin_proc, *commit_proc, *terminate_proc;
    if (tf->annotes()->peek_annote(k_doall_annote)) {
	begin_proc = par_begin_proc;
	commit_proc = par_commit_proc;
	terminate_proc = par_terminate_proc;
    } else {
	begin_proc = speculate_begin_proc;
	commit_proc = speculate_commit_proc;
	terminate_proc = speculate_terminate_proc;
    }
    
    // Add suif_speculate/par_begin()
    assert(begin_proc);
    block call_bldr(block::CALL(block(begin_proc)));

    // Annotate this call with a unique number.
    //
    // Get the unique number
    int speculate_begin_id;
    annote *loopid;
    if ((loopid = tf->annotes()->peek_annote(k_loop_unique_num)) != NULL) {
	speculate_begin_id = ( *(loopid->immeds()) )[0].integer();

	// annotation on the loop not needed, but we'll keep it
	add_speculate_annote(tf, speculate_begin_id, 0);

	tree_node  *the_call = call_bldr.make_tree_node();
	tf->parent()->insert_before(the_call, tf->list_e());
	add_speculate_annote(the_call, speculate_begin_id, 1);

	// Add suif_speculate/par_terminate()
	assert(terminate_proc);
	call_bldr.set(block::CALL(block(terminate_proc)));
	the_call = call_bldr.make_tree_node();
	tf->parent()->insert_after(the_call, tf->list_e());
	add_speculate_annote(the_call, speculate_begin_id, 3);

	// Add "commit: suif_speculate/par_commit()" at the end of tf
	assert(commit_proc);
	call_bldr.set(block::CALL(block(commit_proc)));
	the_call = call_bldr.make_tree_node(tf);
	add_speculate_annote(the_call, speculate_begin_id, 2);
	tf->body()->append(the_call);
      
	label_sym *ls = block::get_proc()->proc_syms()->new_label("L_commit");
	in_lab *inl = new in_lab(ls);
	tree_node *the_call_lab = new tree_instr(inl);
	the_call->parent()->insert_before(the_call_lab, the_call->list_e());
	// Change "in_bj contlab" to "in_bj L_commit"
	replace_label(tf->body(), tf->contlab(), ls);
    }
}


static void search_for_tiles_tree(tree_node *tn, void *)
{
    if (tn->kind() == TREE_FOR) {
	tree_for *tf = (tree_for *) tn;
	annote *an = tf->annotes()->get_annote(k_tileable_loops);

	if (an) {
	    process_tile_annote(tf, an);
	    delete an;
	}
    }
}


static void convert_tiles_tree(tree_node *tn, void *)
{
    if (tn->kind() == TREE_FOR) {
	tree_for *tf = (tree_for *) tn;
	annote *an = tf->annotes()->get_annote(k_tileable_loops);

	if (an) {
	    immed_list *iml = an->immeds();
	    an->set_data(NULL);
	    delete an;

	    an = new annote(k_tile_loops, NULL);
	    an->set_immeds(iml, tf);
	    tf->annotes()->append(an);
	}
    }
}


static void remove_extra_annotes(tree_for *tf) 
{
    annote *an = tf->annotes()->get_annote(k_doall);
    if(an) delete an;
    an = tf->annotes()->get_annote(k_pipeline);
    if(an) delete an;
    an = tf->annotes()->get_annote(k_speculate);
    if(an) delete an;
    an = tf->annotes()->get_annote(k_doacross);
    if(an) delete an;
    an = tf->annotes()->get_annote(code_gen_initialize_annote::k_annote);
    if(an) delete an;
    an = tf->annotes()->get_annote(code_gen_finalize_annote::k_annote);
    if(an) delete an;
    while ((an = tf->annotes()->get_annote(k_reduced))) delete an;
    while ((an = tf->annotes()->get_annote(k_reduced_gen))) delete an;
    while ((an = tf->annotes()->get_annote(code_gen_finalize_annote::k_annote)))
      delete an;
    while ((an=tf->annotes()->get_annote(code_gen_initialize_annote::k_annote)))
      delete an;
}


static void remove_extra_annotes_tree(tree_node *tn, void *x)
{
    boolean *found_doall = (boolean *) x;

    if (tn->kind() == TREE_FOR) {
        tree_for *tf = (tree_for *) tn;
	if (!(*found_doall) && tf->peek_annote(k_doall)) {
	    *found_doall = TRUE;
	    return;
	}
	else remove_extra_annotes(tf);
    }
}


static void extend_extra_annotes(tree_block *region_block, tree_node_list *l)
{
    tree_node_list_e *cur_node_e = region_block->list_e()->prev();
    tree_node_list_e *prev_node_e;

    while (cur_node_e && cur_node_e->contents->is_instr()) {
	prev_node_e = cur_node_e->prev();

	tree_instr *ti = (tree_instr *) cur_node_e->contents;

	if (ti->instr()->peek_annote(k_HPF) || 
	    ti->instr()->peek_annote(k_data_layout)) {
	    tree_instr *cur_clone = ti->clone(region_block->symtab());
            l->remove(cur_node_e);

            region_block->body()->push(cur_clone);
	    delete ti;
	}
	else if (ti->instr()->opcode() != io_mrk) break;

	cur_node_e = prev_node_e;
    }
}


static void create_sequential_region(tree_block *region_block)
{
  region_num++;

  proc_sym *orig_proc = region_block->proc();

  char buf[256];
  sprintf(buf, "_%s_%d_seq", orig_proc->name(), region_num);
  block_symtab *new_symtab = new block_symtab(buf);
  tree_block *new_block = new tree_block(new tree_node_list, new_symtab);

  tree_block *old_block = region_block;
  base_symtab *old_symtab = old_block->symtab();

  base_symtab *parent_symtab = old_symtab->parent();
  parent_symtab->add_child(new_symtab);
  parent_symtab->remove_child(old_symtab);
  new_symtab->add_child(old_symtab);
  old_block->parent()->insert_after(new_block, old_block->list_e());
  old_block->parent()->remove(old_block->list_e());
  new_block->body()->append(old_block->list_e());
  zero_out_ref_converted_in_annotes = FALSE;
  proc_sym *stub_proc = outline(old_block, NULL, TRUE);
  if (new_block->proc()->peek_annote(k_no_recursion))
    stub_proc->append_annote(k_no_recursion);

  sprintf(buf, "_%s_%d_seq", orig_proc->name(), region_num);
  stub_proc->set_name(buf);
}



#ifdef ONE_BLOCK_INIT 

//----------------------------------------------------------
// handle any block size initializations stored at tree_proc

static void 
init_block_sizes(proc_sym * ps)
{
    tree_proc * tp = ps->block();
    immed_list * blist = (immed_list *) tp->get_annote(k_block_size);
    if (!blist) return;

    base_symtab * base = fileset->globals();
    while (blist->is_empty() == FALSE) {
        // read in description of block size
        char * nm = blist->pop().string();
        int bound = blist->pop().integer();
        int proc_dim = blist->pop().integer();

        // get var_sym for _bk#, create it from global symtab if needed
        var_sym *bk_var = base->lookup_var(nm);
        if (!bk_var) {
            bk_var = (var_sym *) base->new_var(type_signed, nm);
            var_def *def = new var_def(bk_var, get_alignment(type_signed));
            fileset->file_list()->head()->contents->symtab()->add_def(def);
        }
        var_sym *nprocs_var = base->lookup_var(nprocs_name(proc_dim));
        assert(bk_var && nprocs_var);

        // build instruction for initializing block size
        //    _bk# = divceil(bounds, _my_nprocs)
        //    where bounds = extent/size of array dimension
        assert(nprocs_var);
        block psize(nprocs_var);
        block dsize(bound);
        dsize.set(block::op(dsize, bop_divceil, psize));
        block bkname(bk_var);
        bkname.set(bkname = dsize);

        // insert at top of body of parallel task
        tp->body()->push(bkname.make_tree_node_list());
    }
}

#endif

