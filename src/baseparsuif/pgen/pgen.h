/* file "pgen.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Main header file */

#ifndef PGEN_H
#define PGEN_H

#pragma interface

#include <suif1.h>
#include <runtime_names.h>

RCS_HEADER(pgen_h, "$Id: pgen.h,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")


// Variables used within tasks
// 
#define MY_STRUCT_PARAM_NAME "_my_struct_param" // ptr to struct passed to task
#define MY_STRUCT_PTR_NAME   "_my_struct_ptr"   // ptr to struct w/ right type 

#define FLUSH_PUTC_NAME      "flush_putc"       // print out buffered I/O
#define ANY_PARALLEL_NAME    "any_parallel_io"  // check if buffered I/O
/* Type declarations */


/* Global variables */

extern int debug[];
extern int mask[];
extern int nprocs_flag;
extern boolean print_line_flag;
extern boolean check_work_flag;
extern boolean inline_sync_flag;
extern boolean sim_assign_flag;
extern int pipeline_grain_flag;
extern boolean fortran_form_flag;
extern boolean no_array_reductions_flag;
extern boolean reduction_guards_flag;
extern boolean no_cache_tile_flag;
extern boolean sequential_stubs_flag;
extern boolean add_params_flag;
extern int *feedback_info;
extern int num_feedback_doalls;
extern int region_num;

  // Annotations read 
extern const char *k_begin_parallel_region; 
extern const char *k_end_parallel_region; 
extern const char *k_loop_bounds;
extern const char *k_block_sizes;
extern const char *k_doall_region;
extern const char *k_doacross;
extern const char *k_tile_loops;
extern const char *k_tileable_loops;
extern const char *k_loop_cyclic;
extern const char *k_block_size;
extern const char *k_global_barrier;
extern const char *k_sync_neighbor;
extern const char *k_lock;
extern const char *k_unlock;
extern const char *k_privatized;
extern const char *k_privatizable;
extern const char *k_guard;
extern const char *k_reduction;
extern const char *k_reduced;
extern const char *k_sync_counter;
extern const char *k_HPF;
extern const char *k_data_layout;
extern const char *k_reduction_gen;
extern const char *k_reduced_gen;
extern const char *k_associativity;
extern const char *k_array_total_size;
extern const char *k_nprocs_var; 
extern const char *k_nprocs1_var; 
extern const char *k_nprocs2_var; 
extern const char *k_true_type;
extern const char *k_doall_annote;
extern const char *k_speculate;
extern const char *k_speculate_id;
extern const char *k_pipeline;
extern const char *k_producer;
extern const char *k_consumer;
extern const char *k_loop_unique_num;

extern const char *k_doall;  // from libtransform


  // Annotations written
extern const char *k_parallel_procedure;

  // Local annotations
extern const char *k_orig_lower_bound;
extern const char *k_orig_upper_bound;
extern const char *k_privatized_var;
extern const char *k_finalization_before;
extern const char *k_finalization_after;
extern const char *k_finalize_index;
extern const char *k_local_var;
extern const char *k_structure_field;
extern const char *k_reduction_identity;
extern const char *k_doall_conditions;
extern const char *k_passed_by_ref;

  // Symbols and types for objects from doall and I77_doall libraries
  // In pgen.cc:
extern func_type *task_type;
extern proc_sym *doall_proc;
extern proc_sym *setjmp_proc;
extern proc_sym *nprocs_proc;
extern proc_sym *doall_level_proc;
extern proc_sym *global_barrier_proc;
extern proc_sym *sync_neighbor_proc;
extern proc_sym *lock_proc;
extern proc_sym *unlock_proc;
extern proc_sym *assign_ids_proc;
extern proc_sym *speculate_begin_proc;
extern proc_sym *speculate_commit_proc;
extern proc_sym *speculate_terminate_proc;
extern proc_sym *par_begin_proc;
extern proc_sym *par_commit_proc;
extern proc_sym *par_terminate_proc;
extern proc_sym *counter_wait_proc;
extern proc_sym *counter_incr_proc;
extern proc_sym *counter_init_proc;
extern proc_sym *counter_set_proc;
extern proc_sym *counter_set_range_proc;
extern proc_sym *flush_putc_proc;
extern proc_sym *check_work_proc;
extern proc_sym *reduction_lock_proc;
extern proc_sym *reduction_unlock_proc;
extern proc_sym *start_packing_proc;
extern var_sym *any_parallel_var;
extern var_sym *suif_nprocs_var;
extern var_sym *suif_args_var;
extern var_sym *suif_task_var;
extern var_sym *suif_mynprocs_var;
extern var_sym *suif_mynprocs1_var;
extern var_sym *suif_mynprocs2_var;
extern var_sym *thread_map;
extern var_sym *mylocks;
extern var_sym *jmpbuf;

/* Function declarations */

  // In pgen.cc:
extern void create_runtime_symbols(file_set_entry *fse);
extern void parallelize(file_set_entry *fse, proc_sym *ps);
extern void modify_main(proc_sym *ps);

  // In sched.cc:
boolean make_block_sched(tree_for *tf,var_sym *my_id_var,var_sym *nprocs_var);
boolean make_cyclic_sched(tree_for *tf,var_sym *my_id_var,
			  var_sym *nprocs_var);

  // In par_region.cc:
void print_var_map(replacements *var_map, FILE *f = stdout);

#endif /* PGEN_H */

