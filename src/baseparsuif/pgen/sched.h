/* file "sched.h" */

/*----------------------------------------------------------------------*
 *
 *    (C) COPYRIGHT by the SUIF Compiler Group, Stanford University.
 *        All rights reserved.
 *
 *
 *  REVISION HISTORY
 *  ----------------
 *
 *     $Log: sched.h,v $
 *     Revision 1.1.1.1  1998/07/07 05:09:31  brm
 *     rcs--0-0-beta-3-alpha-168
 *
 *     Revision 1.1  1993/10/25 17:45:36  anderson
 *     Initial revision
 *
 *
 *
 *  NOTICE: All swine portrayed in this program are fictitious and any
 *      similarity to any real pigs, living or dead, is purely
 *      coincidental.
 *----------------------------------------------------------------------*/

//
// Code generation for task scheduling
//

#ifndef SCHED_H
#define SCHED_H

#pragma interface

#include "pgen.h"
#include "task.h"

RCS_HEADER(sched_h, "$Id: sched.h,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")

#define MY_PID_NAME  "__my_pid"    // variable passed in to task w/ curr pid
#define MY_LOCK_NAME "__my_lock"   // variable passed in to task for locking


// Scheduling classes
//

class Schedule {
protected:
    TaskProc *par_;
    tree_for *loop_;

    proc_symtab *proc_tab()                  { return par_->proc_tab(); }
    block_symtab *block_tab()                { return par_->cur_block_tab(); }
    var_sym *get_my_pid();
    var_sym *find_lock();

    var_sym *promote_lb();
    var_sym *promote_ub();
    var_sym *promote_step();

public:
    Schedule(TaskProc *tp, tree_for *tf) 
      { par_ = tp; loop_ = tf; }

    virtual sched_kinds kind()              { return -1; }
    virtual boolean make_sched()            { assert(FALSE); return FALSE; }
};



//
// Allocates iterations of parallel loop dynamically -- e.g. each processor
// picks up next iterations as soon as it's done with it's current
// iteration.
//
class SchedDynamic : public Schedule {
public:
    SchedDynamic(TaskProc *tp, tree_for *tf) : Schedule(tp, tf) { }

    sched_kinds kind()                         { return SCHED_DYNAMIC; }
    boolean make_sched();
};


//
// Allocates parallel loop in consecutive blocks of iterations
//
// E.g.  for (i = 0; i < 10; i++) and 2 processors:
//   p0 executes iterations 0-4
//   p1 executes iterations 5-9
//
//
class SchedBlock : public Schedule {
private:
    void make_range(var_sym **lbsym, var_sym **ubsym, var_sym **stsym,
		    var_sym **rsym);
    boolean normalize_test();

public:
    SchedBlock(TaskProc *tp, tree_for *tf) : Schedule(tp, tf) { }

    sched_kinds kind()                         { return SCHED_BLOCK; }
    boolean make_sched();

};


//
// Allocates parallel loop in cyclic fashion
//
// E.g.  for (i = 0; i < 10; i++) and 2 processors:
//   p0 executes iterations 0,2,4,6,8
//   p1 executes iterations 1,3,5,7,9
//
class SchedCyclic : public Schedule {
public:
    SchedCyclic(TaskProc *tp, tree_for *tf) : Schedule(tp, tf) { } 

    sched_kinds kind()                         { return SCHED_CYCLIC; }
    boolean make_sched();
};

#endif



