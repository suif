/* file "task.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Transform the code in the child (task) procedure */

#ifndef TASK_H
#define TASK_H

#pragma interface

#include "pgen.h"
#include "runtime_names.h"

RCS_HEADER(task_h, "$Id: task.h,v 1.1.1.1 1998/07/07 05:09:32 brm Exp $")

enum reduction_kinds {
    REDUCTION_SUM,
    REDUCTION_PRODUCT,
    REDUCTION_MAX,
    REDUCTION_MIN
};

class ParallelRegion;  


struct immed_list_pair {
    immed_list old_immeds;
    immed_list new_immeds;

    immed *lookup(immed &im);
};


// Task class

class TaskProc {
friend void find_by_reference_tree(tree_node *tn, void *x);
friend void find_by_reference_instr(instruction *i, void *x);
friend void find_privatized_tree(tree_node *tn, void *x);

private:
    ParallelRegion *my_region_;
    tree_block *body_block_;
    tree_block *stub_block_;
    var_sym *my_id_vars_[MAX_PROC_DIM+1];
    var_sym *nprocs_var_;
    var_sym *proc_id_;
    var_sym *sync_id_;
    var_sym *setjmp_val_;
    int num_barriers_;             // Current number of barriers
    int num_counters_;
    int num_sync_neighbors_;
    var_sym *priv_counter_;

    void set_nprocs();
    void find_by_reference(tree_node_list *l);
    void add_by_reference(var_sym *var, boolean check_list = TRUE);
    void fix_by_reference();
    void find_privatized(tree_node_list *l);
    void find_privatized_annotes(tree_for *tf);
    var_sym *find_privatized_var(sym_addr priv_addr, tree_for *tf,
             replacements *substitute_syms, immed_list_pair *substitute_addrs, 
	     base_symtab *curr_scope, boolean create_new_var = FALSE);
    var_sym *create_finalized_var(sym_addr priv_addr, base_symtab *curr_scope, 
	     replacements *substitute_syms, immed_list_pair *substitute_addrs);
    var_sym *add_privatized(var_sym *var, base_symtab *curr_scope,
        type_node *local_var_type = NULL, boolean create_new_var = FALSE);
    var_sym *add_privatized_heap(var_sym *var, base_symtab *curr_scope,
				 type_node *local_var_type);
    void fix_privatized();
    void find_par_loops(tree_node_list *l);

public:
    TaskProc(tree_block *tb, ParallelRegion *region);
    ~TaskProc();

    tree_block *body_block()                    { return body_block_; }
    tree_block *stub_block()                    { return stub_block_; }
    ParallelRegion *my_region()                 { return my_region_; }
    int num_counters()                          { return num_counters_; }
    void set_num_counters(int n)                { num_counters_ = n; }
    var_sym *nprocs_var()                       { return nprocs_var_; }
    var_sym *proc_id()                          { return proc_id_; }
    void set_proc_id();
    var_sym *sync_id()                          { return sync_id_; }
    void set_sync_id();
    var_sym *setjmp_val()                          { return setjmp_val_; }
    void set_setjmp_val();
    var_sym *priv_counter()                     { return priv_counter_; }
    void set_priv_counter(var_sym *v)           { priv_counter_ = v; }
    tree_for *pipelined_for;

    var_sym *my_id_var(int num)           
        { assert(num >= 0 && num <= MAX_PROC_DIM); 
  	  return my_id_vars_[num]; }

    void set_id_var(var_sym *var, int num)
        { assert(num >= 0 && num <= MAX_PROC_DIM); 
	  my_id_vars_[num] = var; }

    void create_task_params();
    void construct_task();
    void generate_task_code(tree_block *region_block);

    void schedule_loop(tree_for *tf);
    void schedule_pipelined_loop(tree_for *tf);
    void set_stub_block(tree_block *tb)         { stub_block_ = tb; }
    void set_body_block(tree_block *tb)         { body_block_ = tb; }

    void synch_doacross(tree_for *tf, int dim, int distance, 
		instruction *pid1, instruction *pid2);
    void doacross_synch(tree_for *tf);
    void synch_counter(tree_for *tf, annote *an);
    void generate_reduction(tree_for *tf, sym_addr global_var, 
			    sym_addr local_var, reduction_kinds kind);
    void generate_reduction_gen(tree_for *tf, sym_addr proc, in_cal *ic,
				immed_list *il);
    void generate_finalization(tree_for *tf, var_sym *global_var,
			       var_sym *local_var, var_sym *final_var);
    void generate_barrier(tree_node *tn);
    void generate_sync_neighbor(tree_node *tn);
    void generate_lock(tree_node *tn, int lock_id);
    void generate_unlock(tree_node *tn, int lock_id);
    void generate_counter_wait(tree_node *tn, int counter_id, int pid_offset);
    void generate_counter_incr(tree_node *tn, int counter_id, int pid_offset);
    void generate_counter_set(tree_node *tn, int counter_id, int pid_offset);
    void generate_reduction_locks(tree_for *tf);
    tree_node *generate_rlock(int lock_id, tree_for *scope, tree_node *pos, int before);
    tree_node *generate_runlock(int lock_id, tree_for *scope, tree_node *pos, int before);
    void generate_malloc(var_sym *var, annote *size_annote=NULL);
    void generate_free(var_sym *var);
    void unpack_exposed_vars(struct_type *copy_type);
    void cleanup(); 
};

#endif










