/* file "parloop.h" */

/*----------------------------------------------------------------------*
 *
 *    (C) COPYRIGHT by the SUIF Compiler Group, Stanford University.
 *        All rights reserved.
 *
 *
 *  REVISION HISTORY
 *  ----------------
 *
 *     $Log: parloop.h,v $
 *     Revision 1.1.1.1  1998/07/07 05:09:31  brm
 *     rcs--0-0-beta-3-alpha-168
 *
 *     Revision 1.1  1993/10/25 17:45:13  anderson
 *     Initial revision
 *
 *
 *
 *  NOTICE: All swine portrayed in this program are fictitious and any
 *      similarity to any real pigs, living or dead, is purely
 *      coincidental.
 *----------------------------------------------------------------------*/

// 
// Parallel loop implementation
//

#ifndef PARLOOP_H
#define PARLOOP_H

#pragma interface

#include "pgen.h"
#include "parent.h"
#include "task.h"

RCS_HEADER(parloop_h, "$Id: parloop.h,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")

class ParallelLoops;

//
// ParamMap class
//
// Used to represent the three-way mapping between symbols in the 
// parent, task and the global symbol.  
// 
// These symbols are used to pass parameters from the original procedure
// into the newly created procedure.
//
class ParamMap {
public:
    var_sym *parent_sym;
    var_sym *global_sym;
    var_sym *task_sym;
    boolean pass_by_reference;

    void print(FILE *fp);

    ParamMap() 
      { task_sym = NULL;
	parent_sym = NULL;
	global_sym = NULL; 
        pass_by_reference = FALSE; }
        
};

DECLARE_LIST_CLASSES(ParamMapList_base, ParamMapList_e, ParamMapList_iter, 
		     ParamMap*);

class ParamMapList : public ParamMapList_base {
public:
    ParamMap* lookup_by_task(var_sym *vs);
    ParamMap* lookup_by_parent(var_sym *vs);
    ParamMap* lookup_by_global(var_sym *vs);
    void print(FILE *fp);
};



class ParallelLoops {
private:
    ParentProc *parproc_;
    TaskProc *tsk_;    
    file_set_entry *fse_;
    static int num_loops_;             // *total* number of loops
    ParamMapList *param_mappings_;

    void create_params();

public:
    ParallelLoops(file_set_entry *f, proc_sym *ps);
    ~ParallelLoops();

    void add_loop(tree_for *tf, sched_kinds knd = SCHED_BLOCK);
    void enter_param(var_sym *psym, var_sym *tsym, boolean by_ref);
    void write_task();

    file_set_entry *fse()              { return fse_; }
    int num_loops()                    { return num_loops_; }
    ParamMapList *param_mappings()     { return param_mappings_; }
};

#endif




