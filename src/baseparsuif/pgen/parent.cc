/* file "parent.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Transform the code in the parent procedure */

#pragma implementation "parent.h"
#define RCS_BASE_FILE parent_cc

#include "parent.h"
#include "par_region.h"
#include <builder.h>
#include <useful.h>

RCS_BASE("$Id: parent.cc,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")


static void find_flow_tree(tree_node *n, void *x);
static void generate_work_check(block *test_bldr, tree_block *region_block,
				 base_symtab *symtab);
static void generate_doall_conditions(block *test_bldr, immed_list *iml);
static tree_for *single_for_in_list(tree_node_list *tnl);

/* Class implementations */

//
// class ParentProc
//

  /* Public methods */

ParentProc::ParentProc(proc_sym *ps, ParallelRegion *par_region)
{
    proc_ = ps;
    my_region_ = par_region;
    doall_call_site_ = NULL;
    original_region_ = NULL;
}

ParentProc::~ParentProc()
{ }


// Creates the doall_call_site.  This is done as follows:
// Check the current doall_level.  If the level is 0, then
// run the region on different processors, otherwise, run the original
// code.  This check is needed for inter-procedural parallel loops (where
// along one path the code is to be run in parallel and on another path
// it's already part of a task).
//
void ParentProc::create_doall_site(tree_block *region_block)
{
    block::set_proc(proc_->block());

    assert(region_block && region_block->proc() == proc_);
    assert(doall_call_site_ == NULL);

    tree_node_list *cur_list = region_block->parent();

    tree_node_list *then_list = new tree_node_list();
    tree_node_list *else_list = new tree_node_list();

    assert(doall_level_proc);
    base_symtab *symtab = region_block->symtab()->parent();
    assert(symtab);

    var_sym *doall_level_var;
    if (!(doall_level_var = symtab->lookup_var("_doall_level_result")))
        doall_level_var = symtab->new_var(type_signed,"_doall_level_result");
    block res_bldr(doall_level_var);
    block call_bldr(res_bldr = block::CALL(block(doall_level_proc)));
    cur_list->insert_before(call_bldr.make_tree_node_list(cur_list), 
			    region_block->list_e());

    var_sym *run_seq_var;
    if (!(run_seq_var = symtab->lookup_var("_run_sequential")))
        run_seq_var = symtab->new_var(type_signed,"_run_sequential");
    block run_seq_bldr(run_seq_var);

    if (check_work_flag || region_block->peek_annote(k_doall_conditions)) {
	block init_bldr(run_seq_bldr = block(TRUE));
	cur_list->insert_before(init_bldr.make_tree_node(cur_list),
			      region_block->list_e());
    }
    block test_bldr(run_seq_bldr = (block(0) < res_bldr));

    if (check_work_flag) 
	generate_work_check(&test_bldr, region_block, symtab);

    if (region_block->peek_annote(k_doall_conditions)) {
	annote *an = region_block->annotes()->peek_annote(k_doall_conditions);
        generate_doall_conditions(&test_bldr, an->immeds());
    }

    tree_node *test_node = test_bldr.make_tree_node(cur_list);
    cur_list->insert_before(test_node, region_block->list_e());

    block if_bldr(block::IF(run_seq_bldr, block(then_list), block(else_list)));
    tree_if *if_node = (tree_if *) if_bldr.make_tree_node(cur_list);
    cur_list->insert_before(if_node, region_block->list_e());
    cur_list->remove(region_block->list_e());

    tree_block *region_clone = region_block->clone(symtab);

    if_node->then_part()->append(region_clone);
    if_node->else_part()->append(region_block);

    doall_call_site_ = if_node->else_part();
    original_region_ = region_clone;
}


void ParentProc::remove_region(tree_block *region_block)
{
    assert(doall_call_site_ && region_block->parent() == doall_call_site_);
    doall_call_site_->remove(region_block->list_e());

    assert(doall_call_site_->is_empty());
    base_symtab *parent_symtab = doall_call_site_->scope();

    block_symtab *new_symtab = new block_symtab("");
    parent_symtab->add_child(new_symtab);
    tree_block *new_block = new tree_block(new tree_node_list(), new_symtab);
    doall_call_site_->append(new_block);

    doall_call_site_ = new_block->body();
}

//
// Add counter initializations: call suif_counter_init_range(num_counters)
//
void ParentProc::init_counters(int num)
{
    if (num <= 0) return;

    assert(counter_init_proc);
    assert(doall_call_site_);

    block::set_proc(proc_->block());

    var_sym *temp_var = doall_call_site_->scope()->new_unique_var(type_signed,
								  "_temp");
    block temp_var_bldr(temp_var);
    block assigns_bldr(temp_var_bldr = block(num-1));
    doall_call_site_->append(assigns_bldr.make_tree_node());

    // Use num-1 b/c suif_counter_init_range inits counters 0..num *inclusive*
    //
    block call_bldr;
    if (fortran_form_flag) 
        call_bldr.set(block::CALL(block(counter_init_proc), 
				  temp_var_bldr.addr()));
    else
        call_bldr.set(block::CALL(block(counter_init_proc), temp_var_bldr));

    doall_call_site_->append(call_bldr.make_tree_node());
}

//
// Add counter increments: call suif_counter_incr(pid-1, 0..num-1)
//
void ParentProc::incr_counters(int num, var_sym *pid)
{
    if (num <= 0) return;

    assert(counter_incr_proc);
    assert(doall_call_site_);

    block::set_proc(proc_->block());

    for (int i = 0; i < num; i++) {
	block call_bldr(block::CALL
        (block(counter_incr_proc), block(block(pid).dref()-block(1)), block(i)));
	doall_call_site_->append(call_bldr.make_tree_node());
    }
}
	
void ParentProc::call_doall(proc_sym *task_proc)
{
    assert(doall_call_site_);
    assert(doall_proc);

    tree_for *tf = single_for_in_list(original_region()->body());

    block iters_bldr;
    block const_iters_bldr;
    boolean const_iters = FALSE;

    if (tf) {
      operand lb = tf->lb_op();
      operand ub = tf->ub_op();
      operand step = tf->step_op();
      
      if (lb.is_instr() && ub.is_instr() && step.is_instr()) {
	fold_constants(lb.instr());
	fold_constants(ub.instr());
	fold_constants(step.instr());

	lb = tf->lb_op();
	ub = tf->ub_op();
	step = tf->step_op();

	// Assume for has been normalized to have step of +1 and test <=
	//
	int lbint; 
	int ubint;
	if (lb.is_const_int(&lbint) && ub.is_const_int(&ubint)) {
	  const_iters = TRUE;
	  iters_bldr.set(ubint - lbint + 1);
	  const_iters_bldr.set(1);
	}
      }

      if (!const_iters) {
	const_iters_bldr.set(block(0));
	iters_bldr.set(block(ub) - block(lb) + block(1));
      }
    } else {
      const_iters_bldr.set(block(0));
      iters_bldr.set(block(0));
    }

    // Generate the call to suif_named_doall
    //
    block::set_proc(proc_->block());
    operand name_op = string_literal_op(task_proc->name(), my_region_->fse());
    block name_bldr(name_op);
    block task_num_bldr(region_num);

    instruction *c = block::CALL(block(doall_proc), block(task_proc), 
        name_bldr, task_num_bldr, iters_bldr, 
	const_iters_bldr).make_instruction();

    tree_instr *ti = new tree_instr(c);
    doall_call_site_->append(ti);

    // Generate the Call to flush_putc
    //
    if (task_proc->src_lang() == src_fortran) {
	assert(any_parallel_var);
	assert(flush_putc_proc);

	block call_bldr(block::CALL(block(flush_putc_proc)));
	block test_bldr(block::IF(block(any_parallel_var), block(call_bldr)));

	doall_call_site_->append(test_bldr.make_tree_node(doall_call_site_));
    }

    if (sequential_stubs_flag) {
        char buf[256];
	sprintf(buf, "_%s_%d_seq", proc()->name(), region_num);
	block_symtab *new_symtab = new block_symtab(buf);
	tree_block *new_block = new tree_block(new tree_node_list, new_symtab);

	tree_block *old_block = original_region_;
	base_symtab *old_symtab = old_block->symtab();

	base_symtab *parent_symtab = old_symtab->parent();
	parent_symtab->add_child(new_symtab);
	parent_symtab->remove_child(old_symtab);
	new_symtab->add_child(old_symtab);
	old_block->parent()->insert_after(new_block, old_block->list_e());
	old_block->parent()->remove(old_block->list_e());
	new_block->body()->append(old_block->list_e());
	zero_out_ref_converted_in_annotes = FALSE;
	proc_sym *stub_proc = outline(old_block, NULL, TRUE);
	if (new_block->proc()->peek_annote(k_no_recursion))
	  stub_proc->append_annote(k_no_recursion);

	sprintf(buf, "_%s_%d_seq", proc()->name(), region_num);
	stub_proc->set_name(buf);

	original_region_ = stub_proc->block();
    }
}

// Copy all into globals, take address of vars passed by reference
// 
void ParentProc::pack_exposed_vars(struct_type *copy_type, proc_sym *task_proc)
{
    assert(copy_type);
    block::set_proc(proc_->block());

    // Declare a var of the copy_type
    char buf[256];
    sprintf(buf, "_%s_%d_struct_ptr", proc_->name(), region_num);

    file_symtab *file_tab = my_region_->fse()->symtab();
    base_symtab *copy_tab = copy_type->parent();
    base_symtab *btab = doall_call_site_->scope();
    assert(btab->is_ancestor(copy_tab));

    var_sym *my_struct_ptr = btab->new_var(copy_type->ptr_to(), buf);

    block struct_ptr_bldr(my_struct_ptr);
    block assigns_bldr;
    boolean first_assign = TRUE;

    assert(my_region_->var_map());
    sym_node_list_iter old_snli(&my_region_->var_map()->oldsyms);
    sym_node_list_iter new_snli(&my_region_->var_map()->newsyms);

    int field_count = 0;
    while (!old_snli.is_empty()) {
	assert(!new_snli.is_empty());

        sym_node *old_sym = old_snli.step();
        sym_node *new_sym = new_snli.step();

	assert(old_sym->is_var());
	assert(new_sym->is_var());

	var_sym *old_var = (var_sym *) old_sym;
	var_sym *new_var = (var_sym *) new_sym;

        block dest_bldr;
	block src_bldr;
        block old_var_bldr(old_var);

	type_node *dest_type;
	
	if (file_tab->is_ancestor(new_var->parent())) {
	    // Assign directly into global variable
	    //
	    dest_type = new_var->type();
	    dest_bldr.set(block(new_var));
        }
	else {
	    // Assign into structure var
	    //
 	    dest_type = copy_type->field_type(field_count);
	    dest_bldr.set(struct_ptr_bldr.field(field_count));
	    field_count++;
        }

	// Use the annotation on the variable.
	if (old_var->annotes()->get_annote(k_passed_by_ref)) {
	  // if (!dest_type->compatible(old_var->type())) {  
	  // Sanity check, though not sufficient
	  // It must be a pointer to the original type
	  assert(dest_type->is_ptr() && 
            ((ptr_type *)dest_type)->ref_type()->compatible(old_var->type()));

            if (old_var->type()->is_array()) 
		src_bldr.set(old_var_bldr);
            else {
		src_bldr.set(old_var_bldr.addr());
	    }
        }
	else {
  	    src_bldr.set(old_var_bldr);
	}

	if (first_assign) {
	    assigns_bldr.set(block(dest_bldr = src_bldr)); 
	    first_assign = FALSE;
	}
	else {
#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
	    assigns_bldr.set(assigns_bldr.statement_append(block(dest_bldr = 
								 src_bldr)));
#else
//
//  gcc version 2.6.3 generates incorrect code for the above statement. 
//  The block "assigns_bldr" gets cleared the first time the above code
//  is called in the loop, when instead a new statement should have
//  been appended to it. The code works fine with both gcc 2.5.8 and 
//  the IRIX 5.3 C++ compiler and Purify finds no memory errors.  
//  Thus it looks like the problem is caused by a bug in the gcc 2.6.3 
//  code generator.  A work around to the problem is below.
//
            block new_stmt(dest_bldr = src_bldr);
	    block new_assign(assigns_bldr.statement_append(new_stmt));
	    assigns_bldr.set(new_assign);
#endif
	}
    }

    if (!my_region_->var_map()->oldsyms.is_empty()) {
        assert(suif_args_var);
        block cvt_bldr(struct_ptr_bldr = block(suif_args_var));

        doall_call_site_->push(assigns_bldr.make_tree_node_list(
                                                            doall_call_site_));
        doall_call_site_->push(cvt_bldr.make_tree_node_list(doall_call_site_));
    }

    // Set task function pointer
    assert(suif_task_var);

    block task_var_bldr(suif_task_var);
    block task_var_dref(task_var_bldr.dref());
    block set_task_bldr(task_var_dref = block(immed(task_proc)));
    doall_call_site_->push(set_task_bldr.make_tree_node(doall_call_site_));

    // Call to dummy routine for simulator to tell it parameter packing
    // is starting
    assert(start_packing_proc);

    block::set_proc(proc_->block());
    operand name_op = string_literal_op(task_proc->name(), my_region_->fse());
    block name_bldr(name_op);

    block call_bldr(block::CALL(block(start_packing_proc), block(task_proc),
				name_bldr));
    doall_call_site_->push(call_bldr.make_tree_node(doall_call_site_));
}


static void generate_work_check(block *test_bldr, tree_block *region_block,
				base_symtab *symtab)
{
    // Scan region_block body to make sure there's only a single for 
    //
    tree_for *tf = single_for_in_list(region_block->body());
    if (!tf) return;

    //
    // If the loop has any control-flow, then don't do check -- we'll always
    // run the loop in this case.
    // 
    boolean any_control_flow = FALSE;
    tf->body()->map(find_flow_tree, &any_control_flow);

    // See if we can determine the amount of work statically:
    //
    double time_estimate = rough_time_estimate(tf->body());
    assert(time_estimate > 0);

    operand lb = tf->lb_op();
    operand ub = tf->ub_op();
    operand step = tf->step_op();

    if (lb.is_instr() && ub.is_instr() && step.is_instr()) {
	fold_constants(lb.instr());
	fold_constants(ub.instr());
	fold_constants(step.instr());

	lb = tf->lb_op();
	ub = tf->ub_op();
	step = tf->step_op();
    }

    // Generate run-time test
    //
    tree_node_list *cur_list = region_block->parent();
    operand temp_lb = cast_op(lb, type_signed);
    operand temp_ub = cast_op(ub, type_signed);

    var_sym *lb_var, *ub_var, *time_var, *cflow_var;
    lb_var = symtab->new_unique_var(type_signed, "_lb");
    ub_var = symtab->new_unique_var(type_signed, "_ub");
    time_var = symtab->new_unique_var(type_double, "_time");
    cflow_var = symtab->new_unique_var(type_signed, "_cflow");

    block lb_bldr(lb_var);
    block ub_bldr(ub_var);
    block time_bldr(time_var);
    block cflow_bldr(cflow_var);

    block assigns_bldr(lb_bldr = block(temp_lb));
    assigns_bldr.set(assigns_bldr.statement_append(ub_bldr = block(temp_ub)));
    assigns_bldr.set(assigns_bldr.statement_append(time_bldr = 
						   block(time_estimate)));
    assigns_bldr.set(assigns_bldr.statement_append(cflow_bldr =
						   block(any_control_flow)));
    tree_node_list *tnl = assigns_bldr.make_tree_node_list(cur_list);
    cur_list->insert_before(tnl, region_block->list_e());
           
    assert(check_work_proc);
    block check_bldr(check_work_proc);
    block call_bldr;

    if (fortran_form_flag)
      call_bldr.set(block::CALL(check_bldr, lb_bldr.addr(), ub_bldr.addr(), 
				time_bldr.addr(), cflow_bldr.addr()));
    else
      call_bldr.set(block::CALL(check_bldr, lb_bldr, ub_bldr, time_bldr, 
				cflow_bldr));

    block if_bldr(block::IF(call_bldr, *test_bldr));
    test_bldr->set(if_bldr);
}


static void find_flow_tree(tree_node *n, void *x)
{
    if (n->is_instr() || n->is_block()) return;  // Instr and blocks are ok

    boolean *any_control_flow = (boolean *) x;
    *any_control_flow = TRUE;
}


static void generate_doall_conditions(block *test_bldr, immed_list *iml)
{
    immed_list_iter ili(iml);
    while (!ili.is_empty())
      {
	immed im = ili.step();
	assert(im.is_instr());

	instruction *ins = im.instr();
	block if_bldr(block::IF(block(ins), *test_bldr));
	test_bldr->set(if_bldr);
      }
}


tree_for *single_for_in_list(tree_node_list *tnl)
{
  tree_for *tf = NULL;

  tree_node_list_iter tnli(tnl);
  while (!tnli.is_empty()) {
    tree_node *tn = tnli.step();

    if (tn->is_instr()) continue;

    else if (tn->is_for()) {
      if (tf) return NULL;
      else tf = (tree_for *) tn;
    }

    else return NULL;
  }

  return tf;
}



