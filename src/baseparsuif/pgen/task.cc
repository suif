/* file "task.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Transform the code in the child (task) procedure */

#pragma implementation "task.h"
#define RCS_BASE_FILE task_cc

#include "par_region.h"
#include "task.h"
#include <dependence.h>
#include <useful.h>
#include <builder.h>
#include <transform.h>
#include <ist.h>
#include <ipmath.h>
#include <string.h>

RCS_BASE("$Id: task.cc,v 1.2 1999/08/25 03:26:41 brm Exp $")

/* External function declarations */

static var_sym *promote_lb(tree_for *tf, tree_node *pos);
static var_sym *promote_ub(tree_for *tf, tree_node *pos);
static boolean lb_is_indep(tree_for **for_loops, int curr_loop_num);
static boolean ub_is_indep(tree_for **for_loops, int curr_loop_num);

/* Local function declarations */

void find_by_reference_tree(tree_node *tn, void *x);  // Referenced in class
void find_privatized_tree(tree_node *tn, void *x);    // TaskProc
void find_by_reference_instr(instruction *i, void *x);// 

static void find_par_loops_tree(tree_node *tn, void *x);

static void process_cyclic_annote(tree_for *tf, annote *an, TaskProc *task);
static operand create_cyclic_lb(tree_for *tf, int dim, int offset, 
				var_sym *my_id_var);
static operand create_cyclic_step(tree_for *tf, int dim);

static void process_block_sizes(tree_for *tf, annote *an);
static void process_decomp_annote(tree_for *tf, annote *an);
static void process_guard_annote(tree_for *tf, annote *an);
static void process_doall_annote(tree_for *tf, TaskProc *task);
static void process_pipeline_annote(tree_for *tf, TaskProc *task);

extern void process_speculate_annote(tree_for *tf, TaskProc *task);
extern void process_tile_annote(tree_for *tf, annote *an);
extern void replace_label(tree_node_list *tnl, label_sym *oldlab, label_sym *newlab);
extern void prepend_bjlab(tree_node_list *tnl, tree_node *tn, label_sym *lab);
static tree_for *next_inner_for(tree_node_list *tnl);
static tree_for *next_outer_for(tree_node_list *tnl);
static void tile_loops(tree_for *tf, int depth, int *trips);

static void process_doacross_annote(tree_for *tf, annote *an, TaskProc *task);
static void process_initialization_annote(tree_for *tf, annote *an);
static void process_finalization_annote(tree_for *tf, annote *an,
					TaskProc *task);

static void doacross_counters(tree_for *tf, int dim, int dist, 
    int counter_num, instruction *pid1, instruction *pid2, TaskProc *task);
static tree_for * process_reduced_annote(tree_for *tf, annote *an, 
					 TaskProc *task);
static tree_for * process_reduced_gen_annote(tree_for *tf, annote *an, 
					     TaskProc *task);
static void get_reduction_names(char *reduce_proc, char *init_proc, 
                                reduction_kinds kind, type_node *elem_type);
static void get_fortran_reduction_names(char *reduce_proc, char *init_proc, 
                                reduction_kinds kind, type_node *elem_type);
static void get_reduction_rotate_names(char *reduce_proc, char *init_proc, 
                                reduction_kinds kind, type_node *elem_type);
static void get_fortran_reduction_rotate_names(char *reduce_proc, 
	          char *init_proc, reduction_kinds kind, type_node *elem_type);
static void get_reduction_gen_names(char *init_proc, type_node *elem_type);
static void get_fortran_reduction_gen_names(char *init_proc, 
					    type_node *elem_type);
static void convert_global_myid(block & bldr, int dim, TaskProc *task);

static operand calc_num_elems(type_node *curr_type, tree_node *tn);
static operand generate_num_elems(type_node *curr_type, tree_node *tn);
static void replace_sym_addrs_tree(tree_node *tn, void *x);
static void replace_sym_addrs_instr(instruction *i, void *x);
static void kill_replacement_annotes_on_object(suif_object *the_object);
static annote *has_total_size_annote(tree_for *tf, var_sym *the_var);

static const char *runtime_type_name(type_node *the_type);
static const char *runtime_fortran_type_name(type_node *the_type);
extern tree_instr *assign_to_temp_before(tree_node *tn, immed im, 
				      type_node *im_type, var_sym **temp_var);
extern tree_instr *assign_to_temp_after(tree_node *tn, immed im, 
				     type_node *im_type, var_sym **temp_var);
static boolean is_nested(tree_for *outer, tree_for *inner);
static void init_counters(tree_for *tf, TaskProc *task, var_sym *pid);

struct local_info {
  var_sym *local_var;
  tree_for *the_loop;
}; 

/* Class implementations */

//
// class tnl
// Purpose: keep track of all tree_nodes that need to "insert_after"
// reduction locks.
// Allocated in find_par_loops_tree().
// initialized in TaskProc::generate_reduction() and
// used in TaskProc::generate_reduction_locks().
//

#define REDUCTION_THRESHOLD_LOCKING	32*32
#define REDUCTION_THRESHOLD_ROTATING	128*32

DECLARE_DLIST_CLASSES(tnl, tnl_e, tnl_iter, tree_node*);
// List of tree nodes of reduction finalization calls that mark the regions
// of serializations.
static tnl *locking_reduces, *rotating_reduces;
static tree_node *locking_reduce_end, *locking_reduce_begin;
static int num_bits;

static sym_node_list reduced_vars;

//
// class immed_list_pair
//


immed *immed_list_pair::lookup(immed &im)
{
    immed_list_iter old_ili(&old_immeds);
    immed_list_iter new_ili(&new_immeds);

    while (!old_ili.is_empty()) {
	immed old_im = old_ili.step();
	immed new_im = new_ili.step();

	if (old_im == im) return (new immed(new_im));
    }

    return NULL;
}


//
// class TaskProc
//

  /* Public methods */

TaskProc::TaskProc(tree_block *tb, ParallelRegion *region)
{
    assert(region);

    body_block_ = tb;
    stub_block_ = tb;
    my_region_ = region;

    nprocs_var_ = NULL;

    num_barriers_ = 0;
    num_sync_neighbors_ = 0;
    num_counters_ = 0;
    pipelined_for = NULL;
    proc_id_ = NULL;
    sync_id_ = NULL;
    setjmp_val_ = NULL;

    for (int i = 0; i <= MAX_PROC_DIM; i++)
        my_id_vars_[i] = NULL;

}

TaskProc::~TaskProc()
{ }


void TaskProc::construct_task()
{
    set_nprocs();

    // Find privatized and reduction vars, and fix their types if cbr
    //
    find_privatized(body_block_->body());
    fix_privatized();

    // Find vars passed by ref & update their types.  Modifies the types
    // of the variables and sets the is_param flags to true if they are
    // passed by reference. 
    //
    find_by_reference(stub_block_->body());   

    // Modify so that vars passed by ref are treated as pointers to the var
    fix_by_reference();                    

    generate_task_code(body_block_);
}


// Make standarized name for my_ids, taken from iset::myid_name() in popt
static char *myid_name(int num)
{
    assert ((num >= 0) && (num <= MAX_PROC_DIM));
    char * buf = new char[strlen(MY_PID_NAME)+5];
    if (num)
        sprintf(buf, "%s_%d", MY_PID_NAME, num);
    else
        strcpy(buf, MY_PID_NAME);
    return buf;
}

// Make standarized name for my_nprocs, taken from iset::nproc_name() in popt
char *nprocs_name(int num)
{
    assert ((num >= 0) && (num <= MAX_PROC_DIM));
    char * buf = new char[strlen(MY_NPROCS_NAME)+2];
    if (num)
        sprintf(buf, "%s%d", MY_NPROCS_NAME, num);
    else
        strcpy(buf, MY_NPROCS_NAME);
    return buf;
}

void TaskProc::generate_task_code(tree_block *region_block)
{
    proc_sym *curr_proc = region_block->proc();
    assert(curr_proc == body_block_->proc());

    block::set_proc(curr_proc->block());

    // Fill in access vectors and normalize:  Must first make sure we
    // have access vectors for the entire proc
    //
    fill_in_access(curr_proc->block(), FALSE);   

    // add initialization at head of parallel task as a call to:
    //   suif_assign_myids(MY_PID_NAME, &MY_PID_NAME1, &MY_PID_NAME2);

    block_symtab *stab = stub_block_->symtab();
    assert(assign_ids_proc);

    var_sym *myid2 = stab->lookup_var(myid_name(2));
    var_sym *myid1 = stab->lookup_var(myid_name(1));

    if (myid1 || myid2) {
        if (!myid1) myid1 = stab->new_var(type_signed, myid_name(1));
        if (!myid2) myid2 = stab->new_var(type_signed, myid_name(2));

        var_sym *myid = stab->lookup_var(myid_name(0));

        assert(myid && myid1 && myid2);

	block my(myid);
	if (fortran_form_flag) my.set(my.addr());

        block my1(myid1);
        block my2(myid2);
        block stmt(block::CALL(block(assign_ids_proc), my, my1.addr(),
               my2.addr()));

        stub_block_->body()->push(stmt.make_tree_node_list(stub_block_));
    }

    find_par_loops(region_block->body());      // look for loops to schedule
}


void TaskProc::schedule_loop(tree_for *tf)
{
    boolean result = make_block_sched(tf, my_id_vars_[0], nprocs_var_);
    assert_msg(result, ("Scheduling failed for loop %s", tf->index()->name()));
}


void TaskProc::schedule_pipelined_loop(tree_for *tf)
{
    boolean result = make_cyclic_sched(tf, my_id_vars_[0], nprocs_var_);
    assert_msg(result, ("Scheduling failed for loop %s", tf->index()->name()));
}


void TaskProc::synch_doacross(tree_for *tf, int dim, int dist,
    instruction *pid1, instruction *pid2)
{
    doacross_counters(tf, dim, dist, num_counters_, pid1, pid2, this);
    num_counters_++;
}

void TaskProc::generate_reduction(tree_for *tf, sym_addr global_sym,
				  sym_addr local_sym, reduction_kinds kind)
{
    assert(global_sym.symbol()->is_var() && local_sym.symbol()->is_var());
    var_sym *local_var = (var_sym *) local_sym.symbol();
    var_sym *global_var = (var_sym *) global_sym.symbol();
    global_symtab *global_tab = fileset->globals();

    // Figure out name of reduction routines to call, and number of elements
    //
    char reduction_proc_name[256];
    char init_proc_name[256];

    type_node *global_type = global_var->type()->unqual();
    type_node *temp_global_type = global_type;
    type_node *orig_local_type = local_var->type()->unqual();
    type_node *local_type;
    if (orig_local_type->is_ptr()) // to deal with local_var allocated on heap
	local_type = ((ptr_type *) orig_local_type)->ref_type()->unqual();
    else
	local_type = orig_local_type;

    type_node *elem_type = get_element_type(local_type);
    annote *size_annote = NULL;
    operand num_elems_op;

    if ((size_annote = has_total_size_annote(tf, global_var)) != NULL) {
	num_elems_op = (*size_annote->immeds())[1].op().clone(tf->scope());
    }
    else {
	num_elems_op = calc_num_elems(local_type, tf);
    }

    if (local_type->size() >= REDUCTION_THRESHOLD_ROTATING) {
	if (fortran_form_flag)
	    get_fortran_reduction_rotate_names(reduction_proc_name, 
				             init_proc_name, kind, elem_type);
	else 
	    get_reduction_rotate_names(reduction_proc_name, 
				       init_proc_name, kind, elem_type);
    } else {
	if (fortran_form_flag)
	    get_fortran_reduction_names(reduction_proc_name, init_proc_name, 
					kind, elem_type);
	else
	    get_reduction_names(reduction_proc_name, init_proc_name, kind, 
				elem_type);
    }

    proc_sym *reduction_proc_var= global_tab->lookup_proc(reduction_proc_name);
    proc_sym *init_proc_var = global_tab->lookup_proc(init_proc_name);
    if (!reduction_proc_var) {
	type_node *elem_num_type = type_ptr_diff;
	if (fortran_form_flag) 
	    elem_num_type = block::parse_type(global_tab,"%% *",type_ptr_diff);

	type_node *tn =
		new func_type(type_void, type_ptr, type_ptr, elem_num_type);
	tn = global_tab->install_type(tn);
        assert(tn->is_func());
        reduction_proc_var = global_tab->new_proc((func_type *) tn,
						  src_c, reduction_proc_name);
    }
    if (!init_proc_var) {
	type_node *elem_num_type = type_ptr_diff;
	if (fortran_form_flag) 
	    elem_num_type = block::parse_type(global_tab,"%% *",type_ptr_diff);

	type_node *tn =
		new func_type(type_void, type_ptr, elem_num_type);
	tn = global_tab->install_type(tn);
        assert(tn->is_func());
        init_proc_var = global_tab->new_proc((func_type *) tn,
					     src_c, init_proc_name);
    }

    // Generate the initialization and reduction calls
    //
    block local(local_var);
    if (!local_type->is_array() && !orig_local_type->is_ptr())
	local.set(local.addr());

    block global(global_var);
    operand global_oper;

    if (global_type->is_ptr()) {
	temp_global_type = ((ptr_type *) global_type)->ref_type()->unqual();

	// Only bother to dref if global var is group type, because 
	// next we have to get the right field.  This happens when the
	// global variable is passed into the task procedure.
	// Builder can't handle ptrs to structures so build our own 
	// instructions:
	//
	if (temp_global_type->op() == TYPE_GROUP) {
	    assert((global_sym.offset() % target.addressable_size) == 0);
	    in_ldc *ldc = new in_ldc(type_signed, operand(), 
  		       immed(global_sym.offset() / target.addressable_size));
	    global_oper = operand(ldc);
	}
    }

    if (temp_global_type->op() == TYPE_GROUP) {
	struct_type *group_type = (struct_type *) temp_global_type;
	int left;
	unsigned field_id = 
	    group_type->find_field_by_offset(global_sym.offset(), left);
	assert(left == 0);
	type_node *curr_field_type = group_type->field_type(field_id);
	type_node *result_type = block::parse_type(group_type->parent(),
						   "%% *", curr_field_type);
	if (global_type->is_ptr()) {
	    in_rrr *add = new in_rrr(io_add, result_type, operand(),
				     operand(global_var), global_oper);
	    global_oper = operand(add);
	}
	else {
	    in_ldc *ldc = new in_ldc(result_type, operand(),immed(global_sym));
	    global_oper = operand(ldc);
	    global_var->set_addr_taken();
	}
	global.set(block(global_oper));

	// Field annonation
	//
	immed_list *iml = new immed_list();
	iml->append(group_type->field_name(field_id));
	assert(global_oper.is_instr());
	annote *an = new annote(k_fields, NULL);
	an->set_immeds(iml, global_oper.instr());
	global_oper.instr()->annotes()->append(an);
    }

    // If global var is not an array or it's not a pointer then we pass
    // the addr (it could be a ptr if being passed by ref).  
    //
    else if (!global_type->is_array() && !global_type->is_ptr())
	global.set(global.addr());

    block reduction_proc(reduction_proc_var);
    block init_proc(init_proc_var);
    block num_elems(num_elems_op);
    block init_bldr;
    block temp_var_bldr;

    if (fortran_form_flag) {
	var_sym *temp_var = tf->scope()->new_unique_var(type_ptr_diff,"_temp");
	temp_var_bldr.set(temp_var);
	block assigns_bldr(temp_var_bldr = num_elems);
	tf->parent()->insert_before(assigns_bldr.make_tree_node(tf), 
				    tf->list_e());
	init_bldr.set(block::CALL(init_proc, local, temp_var_bldr.addr()));
    }
    else {
	init_bldr.set(block::CALL(init_proc, local, num_elems));
    }
    tf->parent()->insert_before(init_bldr.make_tree_node(tf), tf->list_e());

    if (local_var->is_scalar() && reduction_guards_flag)
      {
	char buf[256];
	sprintf(buf, "_dummy_%s", global_var->name());

	var_sym *dummy_var = 
	  local_var->parent()->new_unique_var(local_var->type(), buf);
	immed_list *iml = new immed_list();
	iml->append(immed(dummy_var));
	annote *an = new annote(k_reduction_identity);
	an->set_immeds(iml, local_var);
	local_var->annotes()->append(an);

	block dummy_var_bldr(dummy_var);
	block local_var_bldr(local_var);
	block identity_bldr(dummy_var_bldr = local_var_bldr);

	tf->parent()->insert_before(identity_bldr.make_tree_node(tf), 
				    tf->list_e());
      }

    block call_bldr;
    if (fortran_form_flag) {
	call_bldr.set(block::CALL(reduction_proc, global, local, 
				  temp_var_bldr.addr()));
    }
    else {
	call_bldr.set(block::CALL(reduction_proc, global, local, num_elems));
    }

    tree_node *treen =	
        tf->parent()->insert_after(call_bldr.make_tree_node(tf), tf->list_e());

    // Initializing locking_reduces, locking_reduce_begin, locking_reduce_end
    // and rotating_reduces:
    // 1. locking_reduces: list of tree nodes that need to
    //			   "insert_after" unlock+lock calls
    // 2. locking_reduce_begin: tree node that needs to "insert_before" lock
    //				call
    // 3. locking_reduce_end: tree_node that need to "insert_after" unlock call
    // 4. rotating_reduces: list of tree nodes that represent the
    //			    "reduce_rotate" in_cals. It is used to suppress
    //			    the generation of locking calls directed by 1, 2, 3
    // They are used in generate_reduction_locks() to "insert_after" reduction
    // locking calls later
    
    if (!locking_reduce_end) {  // boundary case
	locking_reduce_end = treen;
	locking_reduce_begin = treen;
	if (local_type->size() >= REDUCTION_THRESHOLD_ROTATING) {
	    rotating_reduces->append(treen);
	}
	num_bits += local_type->size();
    } else {
	locking_reduce_begin = treen;

	// Two occasions to insert_after locking code:
	// 1. accumulated num_bits is greater than REDUCTION_THRESHOLD_LOCKING
	// 2. treen is a finalization on an array over the size
	//    REDUCTION_THRESHOLD_ROTATING

	int generate_locking = 0;
	if (num_bits >= REDUCTION_THRESHOLD_LOCKING) {
	    generate_locking = 1;
	}
	if (local_type->size() >= REDUCTION_THRESHOLD_ROTATING) {
	    generate_locking = 1;
	    rotating_reduces->append(treen);
	}
	if (generate_locking) {
	    locking_reduces->append(treen);
	    num_bits = 0;
	}
	num_bits += local_type->size();
    }
}


void TaskProc::generate_reduction_gen(tree_for *tf, sym_addr proc,
				      in_cal *ic, immed_list *il)
{
    instruction *i_root = ic;
    
//    var_sym *i_sym = tf->scope()->new_unique_var(type_signed, "i");

    global_symtab *global_tab = fileset->globals();
    type_node *orig_local_type, *local_type, *global_type;
    assert(proc.symbol()->is_proc());
    proc_sym *reduce_proc_var = (proc_sym *) proc.symbol();

    // For finalization code
    //
    immed_list *assoc_info = 
      (immed_list*) reduce_proc_var->peek_annote(k_associativity);
    immed_list_iter aii(assoc_info);
    assert(! (assoc_info->count()%3) );
    
    immed_list_iter ili(il);
    immed im = ili.step();  // proc
    im = ili.step();  // ic
    while (!ili.is_empty()) {
	im = ili.step();
	sym_addr global_sym = im.addr();
	im = ili.step();
	sym_addr local_sym = im.addr();

	// Figure out init_proc_var, and number of elements
	//
	assert(global_sym.symbol()->is_var() && local_sym.symbol()->is_var());
	var_sym *local_var = (var_sym *) local_sym.symbol();
	var_sym *global_var = (var_sym *) global_sym.symbol();
	global_type = global_var->type()->unqual();
	type_node *temp_global_type = global_type;
	orig_local_type = local_var->type()->unqual();
	if (orig_local_type->is_ptr()) // to deal with local_var allocated on heap
	    local_type = ((ptr_type *) orig_local_type)->ref_type()->unqual();
	else
	    local_type = orig_local_type;

	type_node *elem_type = get_element_type(local_type);
	operand num_elems_op;
	annote *size_annote = NULL;

	if ((size_annote = has_total_size_annote(tf, global_var)) != NULL)  {
	    num_elems_op = (*size_annote->immeds())[1].op().clone(tf->scope());
	}
	else {
	    num_elems_op = calc_num_elems(local_type, tf);
	}

	char init_proc_name[256];

	if (fortran_form_flag) 
	    get_fortran_reduction_gen_names(init_proc_name, elem_type);
	else 
	    get_reduction_gen_names(init_proc_name, elem_type);

	proc_sym *init_proc_var = global_tab->lookup_proc(init_proc_name);
	if (!init_proc_var) {
	    type_node *elem_num_type = type_ptr_diff;
	    if (fortran_form_flag)
		elem_num_type = block::parse_type(global_tab, "%% *", 
						  type_ptr_diff);

	    type_node *tn =
		new func_type(type_void, type_ptr, type_ptr,
			      elem_num_type);
	    tn = global_tab->install_type(tn);
	    assert(tn->is_func());
	    init_proc_var = global_tab->new_proc((func_type *) tn,
						 src_c, init_proc_name);
	}

	assert_msg(local_type->is_base(),
		   ("Generalized reduction only handles base_type now"));

	// Figure out block local and global
	//
	block local(local_var);
	if (!local_type->is_array() && !orig_local_type->is_ptr())
	    local.set(local.addr());

	block global(global_var);
	operand global_oper;

	if (global_type->is_ptr()) {
	    temp_global_type = ((ptr_type *) global_type)->ref_type()->unqual();

	    // Only bother to dref if global var is group type, because 
	    // next we have to get the right field.  This happens when the
	    // global variable is passed into the task procedure.
	    // Builder can't handle ptrs to structures so build our own 
	    // instructions:
	    //
	    if (temp_global_type->op() == TYPE_GROUP) {
		assert((global_sym.offset() % target.addressable_size) == 0);
		in_ldc *ldc = new in_ldc(type_signed, operand(), 
		    immed(global_sym.offset() / target.addressable_size));
		global_oper = operand(ldc);
	    }
	}

	if (temp_global_type->op() == TYPE_GROUP) {
	    struct_type *group_type = (struct_type *) temp_global_type;
	    int left;
	    unsigned field_id = 
		group_type->find_field_by_offset(global_sym.offset(), left);
	    assert(left == 0);
	    type_node *curr_field_type = group_type->field_type(field_id);
	    type_node *result_type = block::parse_type
		(group_type->parent(), "%% *", curr_field_type);
	    if (global_type->is_ptr()) {
		in_rrr *add = new in_rrr(io_add, result_type, operand(),
					 operand(global_var), global_oper);
		global_oper = operand(add);
	    }
	    else {
		in_ldc *ldc = new in_ldc(result_type, operand(),immed(global_sym));
		global_oper = operand(ldc);
		global_var->set_addr_taken();
	    }
	    global.set(block(global_oper));

	    // Field annonation
	    //
	    immed_list *iml = new immed_list();
	    iml->append(group_type->field_name(field_id));
	    assert(global_oper.is_instr());
	    annote *an = new annote(k_fields, NULL);
	    an->set_immeds(iml, global_oper.instr());
	    global_oper.instr()->annotes()->append(an);
	}

	// If global var is not an array or it's not a pointer then we pass
	// the addr (it could be a ptr if being passed by ref).  
	//
	else if (!global_type->is_array() && !global_type->is_ptr())
	    global.set(global.addr());

	// Create initialization code
	//
	block init_proc(init_proc_var);
	block num_elems(num_elems_op);
	block init_bldr;

	if (fortran_form_flag) {
	    var_sym *temp_var = tf->scope()->new_unique_var(type_ptr_diff,
							    "_temp");
	    block temp_var_bldr(temp_var);
	    block assigns_bldr(temp_var_bldr = num_elems);
            tf->parent()->insert_before(assigns_bldr.make_tree_node(tf), 
	                                tf->list_e());
	    init_bldr.set(
	        block::CALL(init_proc, global, local, temp_var_bldr.addr()));
        }
	else {
	    init_bldr.set(block::CALL(init_proc, global, local, num_elems));
	}

	tf->parent()->insert_before(init_bldr.make_tree_node(tf),tf->list_e());
	operand local_op(local_var);  
	operand global_op, global_op_addr;
	if (global_type->is_ptr()) {
	    global_op_addr = operand(global_var);
	    global_op = operand
		(new in_rrr(io_lod, ((ptr_type*)global_type)->ref_type(), operand(), global_op_addr));
	} else {
	    type_node *t = global_type->ptr_to();
	    global_op_addr = operand
		(new in_ldc(t, operand(), immed(global_sym)));
	    global_op = operand(global_var);
	}

	// Find out which_one to replace in finalization call
	//
	immed which_one;
	int n;
	which_one = aii.step();  // Final operands

	if (which_one.is_integer()) {
	    n = which_one.integer();
	    kill_op(ic->argument(n));
	    ic->set_argument(n, global_op_addr);
	} else {
	    assert(which_one.is_string() && !strcmp(which_one.string(), "return value"));
	    if (global_op.is_symbol()) {
		ic->set_dst(global_op);
	    } else {
		i_root = new in_rrr
		    (io_str, type_void, operand(), global_op_addr, operand(ic)); 
	    }
	}

	which_one = aii.step();  // Init operands
	assert(which_one.is_integer());
	n = which_one.integer();
	kill_op(ic->argument(n));
	ic->set_argument(n, global_op);

	which_one = aii.step();  // Scanned operands
	assert(which_one.is_integer());
	n = which_one.integer();
	kill_op(ic->argument(n));
	ic->set_argument(n, local_op);
    }

    // Create finalization code:
    // for (i = 0; i < num_elems; i++) reduction_proc_var();
    //
    tree_node *t = new tree_instr(i_root);

    // Create for-loop
//    block i(i_sym);
//    block for_bldr(block::FOR(i, block(0), num_elems, block(t, FALSE)));

    tree_node *treen =	
    tf->parent()->insert_after(t /*for_bldr.make_tree_node(tf)*/, tf->list_e());

    // Remove the free-floating ic in il
    //
    ili.reset(il);
    ili.step();  // proc_sym
    immed_list_e *ile = (immed_list_e *) ili.glist_iter::step();  
    il->remove(ile);  // instruction
    
    
    // Initializing locking_reduces, locking_reduce_begin, locking_reduce_end
    // and rotating_reduces:
    // 1. locking_reduces: list of tree nodes that need to
    //			   "insert_after" unlock+lock calls
    // 2. locking_reduce_begin: tree node that needs to "insert_before" lock
    //				call
    // 3. locking_reduce_end: tree_node that need to "insert_after" unlock call
    // 4. rotating_reduces: list of tree nodes that represent the
    //			    "reduce_rotate" in_cals. It is used to suppress
    //			    the generation of locking calls directed by 1, 2, 3
    // They are used in generate_reduction_locks() to "insert_after" reduction
    // locking calls later
    
    if (!locking_reduce_end) {  // boundary case
	locking_reduce_end = treen;
	locking_reduce_begin = treen;
//	if (local_type->size() >= REDUCTION_THRESHOLD_ROTATING) {
//	    rotating_reduces->append(treen);
//	}
	num_bits += local_type->size();
    } else {
	locking_reduce_begin = treen;

	// Two occasions to insert_after locking code:
	// 1. accumulated num_bits is greater than REDUCTION_THRESHOLD_LOCKING
	// 2. treen is a finalization on an array over the size
	//    REDUCTION_THRESHOLD_ROTATING

	int generate_locking = 0;
	if (num_bits >= REDUCTION_THRESHOLD_LOCKING) {
	    generate_locking = 1;
	}
//	if (local_type->size() >= REDUCTION_THRESHOLD_ROTATING) {
//	    generate_locking = 1;
//	    rotating_reduces->append(treen);
//	}
	if (generate_locking) {
	    locking_reduces->append(treen);
	    num_bits = 0;
	}
	num_bits += local_type->size();
    }
}


void TaskProc::generate_finalization(tree_for *tf, var_sym *global_var,
				     var_sym *local_var, var_sym *final_var)
{
    if (tf->annotes()->peek_annote(k_speculate) && !setjmp_val_) {
	set_setjmp_val();
    }
    if (final_var) {
	tree_if *before_if = (tree_if *)tf->peek_annote(k_finalization_before);
	if (!before_if) {

	    // if (pid == nprocs - 1)
	    // or if (pid == -setjmp_val)
	    assert(nprocs_var_ && my_id_vars_[0]);
	    block nprocs(nprocs_var_);
	    block pid(my_id_vars_[0]);

	    block before_code;
	    if (setjmp_val_) {
		before_code.set((block::IF((pid == block( block(-block(setjmp_val_)) - 1)),
					   block(new tree_node_list()), block(new tree_node_list()))));
	    } else {
		before_code.set(block::IF((pid == nprocs - block(1)), 
					  block(new tree_node_list()), block(new tree_node_list())));
	    }
	    
	    before_if = (tree_if *) 
	      before_code.make_tree_node(body_block_->parent());
	    body_block_->parent()->insert_before(before_if, 
						 body_block_->list_e());
            tf->append_annote(k_finalization_before, before_if);
	}

	assert(local_var && global_var);
	block local(local_var);
	block global(global_var);
	block final(final_var);

	if (!local_var->type()->unqual()->is_ptr() && 
	    !local_var->type()->unqual()->is_array())  
	  local.set(local.addr());

	if (!global_var->type()->unqual()->is_ptr() &&
	    !global_var->type()->unqual()->is_array()) 
	  global.set(global.addr());

	block then_bldr(final = global);
	before_if->then_part()->append(
	    then_bldr.make_tree_node_list(before_if));
	block else_bldr(final = local);
	before_if->else_part()->append(
	    else_bldr.make_tree_node_list(before_if));
    }
    else {
	tree_if *after_if = (tree_if *)tf->peek_annote(k_finalization_after);
	if (!after_if) {

	    // if (pid == nprocs - 1)
	    //
	    assert(nprocs_var_ && my_id_vars_[0]);
	    block nprocs(nprocs_var_);
	    block pid(my_id_vars_[0]);

	    block after_code;
	    if (setjmp_val_) {
		after_code.set((block::IF((pid == block( block(-block(setjmp_val_)) - 1)),
					   block(new tree_node_list()), block(new tree_node_list()))));
	    } else {
		after_code.set(block::IF((pid == nprocs - block(1)), 
					  block(new tree_node_list()), block(new tree_node_list())));
	    }
	    after_if = (tree_if *) 
	      after_code.make_tree_node(body_block_->body());
	    body_block_->body()->append(after_if);
            tf->append_annote(k_finalization_after, after_if);
	}

	assert(local_var && global_var);
	block local(local_var);
	block global(global_var);

	if (global_var->type()->is_ptr()) global.set(global.dref());
	    
	block then_bldr(global = local);
	after_if->then_part()->append(then_bldr.make_tree_node_list(after_if));
    }
}


void TaskProc::generate_barrier(tree_node *tn)
{
    assert(tn);
    assert(global_barrier_proc);

    block var_bldr;
    tree_node *pos = tn;
    
    if (fortran_form_flag)
     {
       var_sym *temp_var = NULL;
       pos = assign_to_temp_after(tn, immed(num_barriers_), type_signed, 
				  &temp_var);
       block temp_var_bldr(temp_var);
       var_bldr.set(temp_var_bldr.addr());
      }
    else
      {
	var_bldr.set(block(num_barriers_));
      }

    tree_node *bar_node = block::CALL(block(global_barrier_proc), 
				      var_bldr).make_tree_node(pos->parent());
    pos->parent()->insert_after(bar_node, pos->list_e());

    num_barriers_++;
}

void TaskProc::generate_sync_neighbor(tree_node *tn)
{
    assert(tn);
    assert(sync_neighbor_proc);

    block var_bldr;
    tree_node *pos = tn;
    
    if (fortran_form_flag) 
      {
	var_sym *temp_var = NULL;
	pos = assign_to_temp_after(tn, immed(num_sync_neighbors_), type_signed,
				   &temp_var);
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());
      }
    else
      {
	var_bldr.set(block(num_sync_neighbors_));
      }

    tree_node *bar_node =  block::CALL(block(sync_neighbor_proc), 
			         var_bldr).make_tree_node(pos->parent());
    pos->parent()->insert_after(bar_node, pos->list_e());

    num_sync_neighbors_++;
}

void TaskProc::generate_lock(tree_node *tn, int lock_id)
{
    assert(tn);
    assert(lock_proc);

    block var_bldr;
    tree_node *pos = tn;
    
    if (fortran_form_flag) 
      {
	var_sym *temp_var = NULL;
	pos = assign_to_temp_after(tn, immed(lock_id), type_signed, &temp_var);
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());
      }
    else
      {
	var_bldr.set(block(lock_id));
      }

    tree_node *lock_node = 
        block::CALL(block(lock_proc), var_bldr).make_tree_node(pos->parent());
    pos->parent()->insert_after(lock_node, pos->list_e());
}


void TaskProc::generate_unlock(tree_node *tn, int lock_id)
{
    assert(tn);
    assert(unlock_proc);

    block var_bldr;
    tree_node *pos = tn;
    
    if (fortran_form_flag) 
      {
	var_sym *temp_var = NULL;
	pos = assign_to_temp_after(tn, immed(lock_id), type_signed, &temp_var);
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());
      }
    else
      {
	var_bldr.set(block(lock_id));
      }

    tree_node *unlock_node = 
      block::CALL(block(unlock_proc), var_bldr).make_tree_node(pos->parent());
    pos->parent()->insert_after(unlock_node, pos->list_e());
}


void TaskProc::generate_counter_wait(tree_node *tn, int counter_id, int pid_offset)
{
    if (debug['g'-'\0']) {
        printf("    Processing consumer synch\n");
    }

    assert(counter_wait_proc);
    assert(nprocs_var_ && my_id_vars_[0]);
    block id;
    if (proc_id_) {
	id.set(proc_id_);
    } else {
	id.set(my_id_vars_[0]);
    }
    block cid(counter_id);
    block np(nprocs_var_);
    assert(pipelined_for);
    block pc(pipelined_for->index());

    if (!pid_offset) {
	block call_bldr(block::CALL(block(counter_wait_proc), 
				    id, cid, pc));
	tn->parent()->insert_before(call_bldr.make_tree_node(), tn->list_e());
    } else {

    if (inline_sync_flag) {
	// Generate code like "do {} while (_mylocks[syncid][counter_id] <
	// val)"
	if (!sync_id_) {
	    set_sync_id();
	}
	block sid(sync_id_);
	block locks(mylocks);
	block locks1(locks[sid]);
	block locks2(locks1[cid]);
	operand op1(locks2.make_instruction());
	operand op2(pipelined_for->index());
	operand op3 = op1 < op2;
	label_sym *top = block::get_proc()->proc_syms()->new_unique_label("L_top");
	label_sym *cont = block::get_proc()->proc_syms()->new_unique_label("L_cont");
	label_sym *brk = block::get_proc()->proc_syms()->new_unique_label("L_brk");
	assert(top);
	//    in_lab *inl = new in_lab(ls);
	tree_instr *ti = new tree_instr(new in_bj(io_btrue, top, op3));
	
	//	block compare_bldr(locks2 < block(pc));
	//	block loop(block::DO(compare_bldr, block()));

	//	tree_loop *tl = (tree_loop*)loop.make_tree_node();
	tree_node_list *tnl = new tree_node_list;
	tnl->append(ti);
	tree_loop *tl = new tree_loop(new tree_node_list(), tnl, cont, brk, top);
 	tn->parent()->insert_before(tl, tn->list_e());	
	
    } else {
	
	// Generate code like this:
	// if (_my_id) 
	//    suif_counter_wait(_my_id - 1, 0, priv_counter);
	// else
	//    suif_counter_wait(_my_nprocs - 1, 0, priv_counter);
	//
	assert_msg(pid_offset==-1, ("Only support pid_offset to be 0 or -1 in this version\n"));
	block call_bldr1(block::CALL(block(counter_wait_proc), 
				     block(id - block(1)), cid, pc));
	block call_bldr2(block::CALL(block(counter_wait_proc), 
				     block(np - block(1)), cid, pc));
	block if_bldr(block::IF((id), (call_bldr1), (call_bldr2))); 
 	tn->parent()->insert_before(if_bldr.make_tree_node(), tn->list_e());
    }
    }
}


void TaskProc::generate_counter_set(tree_node *tn, int counter_id, int pid_offset)
{
    if (debug['g'-'\0']) {
        printf("    Processing producer synch\n");
    }

    block id;
    if (proc_id_) {
	id.set(proc_id_);
    } else {
	id.set(my_id_vars_[0]);
    }
    block cid(counter_id);
    assert(pipelined_for);
    block pc(pipelined_for->index());
    assert_msg(!pid_offset, ("Only support pid_offset to be 0 in this version\n"));

    if (inline_sync_flag) {
	// generate code like "_mylocks[pid][counter_id] = pc+1"
	block locks(mylocks);
	block locks1(locks[id]);
	block locks2(locks1[cid]);
	
	block assign_bldr(locks2 = block(pc+1));
	tn->parent()->insert_after(assign_bldr.make_tree_node(), tn->list_e());
    } else {
	assert(counter_set_proc);
	block call_bldr(block::CALL(block(counter_set_proc), id, cid, block(pc+1)));
	tn->parent()->insert_after(call_bldr.make_tree_node(), tn->list_e());
    }
}


void TaskProc::generate_counter_incr(tree_node *tn, int counter_id, int pid_offset)
{
    if (debug['g'-'\0']) {
        printf("    Processing producer synch\n");
    }

    assert(counter_incr_proc);
    assert(my_id_vars_[0]);

    block id;
    if (proc_id_) {
	id.set(proc_id_);
    } else {
	id.set(my_id_vars_[0]);
    }
    block cid(counter_id);

    assert_msg(!pid_offset, ("Only support pid_offset to be 0 in this version\n"));
    block call_bldr(block::CALL(block(counter_incr_proc), id, cid));

    tn->parent()->insert_after(call_bldr.make_tree_node(), tn->list_e());
}


void TaskProc::generate_reduction_locks(tree_for *tf)
{
    tree_node *first_lock = NULL;
    tree_node *last_unlock = NULL;  
    // put out locks and unlocks
    int i = 0;

    // locking_reduce_end
    if (locking_reduce_end && !rotating_reduces->lookup(locking_reduce_end)) 
      {
	last_unlock = generate_runlock(i, tf, locking_reduce_end, 0);
      }

    // locking_reduces:
    // Suppress the unlock calls if the tree node is in rotating_reduces.
    // Suppress the lock calls if the next tree nodes in the generated code
    // is in rotating_reduces
    // Note: "the next tree nodes in the generated code is in rotating_reduces"
    // iff "previous element in locking_reduces is in rotating_reduces"
    tree_node *lock = locking_reduce_end, *prevlock;
    tnl_iter locking_reduces_iter(locking_reduces);
    while (!locking_reduces_iter.is_empty()) {
	prevlock = lock;
	lock = locking_reduces_iter.step();
	if (prevlock && !rotating_reduces->lookup(prevlock)) {
	    generate_rlock(i, tf, lock, 0);
	    i++;
	}
	if (!rotating_reduces->lookup(lock)) {
	  generate_runlock(i, tf, lock, 0);
	}
    }

    // locking_reduce_begin
    if (locking_reduce_begin && 
	!rotating_reduces->lookup(locking_reduce_begin)) {
	first_lock = generate_rlock(i, tf, locking_reduce_begin, 1);
    }


    // Try to put "if" around locks if all priv variables are identity
    //
    if (reduction_guards_flag && 
        first_lock && last_unlock && 
        (first_lock->parent() == last_unlock->parent()) &&
        (tf->peek_annote(k_reduction_gen) == NULL)) 

      {
	tree_node_list *curr_list = first_lock->parent();
	block_symtab *btab = new block_symtab("reduced_block");
	first_lock->scope()->add_child(btab);
	tree_block *reduced_block = new tree_block(new tree_node_list(), btab);
	curr_list->insert_after(reduced_block, 
				last_unlock->list_e());

	tree_node_list_e *curr_list_e = first_lock->list_e();
	tree_node_list_e *next_list_e = curr_list_e;
	tree_node_list_e *last_list_e = last_unlock->list_e();
	while (curr_list_e) 
	  {
	    next_list_e = curr_list_e->next();
	    tree_node *curr_clone = 
	      curr_list_e->contents->clone(reduced_block->symtab());
	    curr_list->remove(curr_list_e);

	    reduced_block->body()->append(curr_clone);
	    if (curr_list_e == last_list_e) break;
	    curr_list_e = next_list_e;
	  }

	block body_bldr;
	block reduced_var_bldr;
	block test_bldr;
	block identity_bldr;
	boolean generate_test = TRUE;

	var_sym *do_reduction_var = 
	  curr_list->scope()->new_unique_var(type_signed,"do_reduction");
	block do_reduction_bldr(do_reduction_var);
	block init_bldr(do_reduction_bldr = block(TRUE));
	test_bldr.set(do_reduction_bldr = block(FALSE));

	sym_node_list_iter snli(&reduced_vars);
	while (!snli.is_empty())
	  {
	    sym_node *sn = snli.step();
	    assert(sn->is_var());
	    var_sym *reduced_var = (var_sym *) sn;
	    if (!reduced_var->is_scalar()) 
	      {
		generate_test = FALSE;
		break;
	      }
	    
	    annote *an = 
	      reduced_var->annotes()->get_annote(k_reduction_identity);
	    assert(an);
	    sym_node *identity_sym = (*an->immeds())[0].symbol();

	    body_bldr.set(test_bldr);
	    reduced_var_bldr.set(block(reduced_var));
	    identity_bldr.set(block(identity_sym));
	    test_bldr.set(block::IF((reduced_var_bldr == identity_bldr),
				    test_bldr));
	  }

	if (generate_test)
	  {
	    curr_list->insert_before(init_bldr.make_tree_node(curr_list),
				     reduced_block->list_e());
	    tree_node *tn = test_bldr.make_tree_node(curr_list);
	    curr_list->insert_before(tn, reduced_block->list_e());

	    block if_bldr(block::IF(do_reduction_bldr, block(reduced_block)));
	    tn = if_bldr.make_tree_node(curr_list);
	    curr_list->insert_before(tn, reduced_block->list_e());

	    curr_list->remove(reduced_block->list_e());
	    delete reduced_block;
	  }
      }
    
    delete locking_reduces;
    delete rotating_reduces;
}


#define MAX_RLOCK_ID	63

tree_node *TaskProc::generate_rlock(int id, tree_for *scope, tree_node *pos, 
				    int before)
{
    assert(pos);
    assert(reduction_lock_proc);

    block var_bldr;
    if (fortran_form_flag) {
	var_sym *temp_var = NULL;
	if (before) {
	  assign_to_temp_before(pos, immed(id&MAX_RLOCK_ID), type_signed, 
				&temp_var);
	} else {
	  pos = assign_to_temp_after(pos, immed(id&MAX_RLOCK_ID), type_signed,
				     &temp_var);
	}
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());
    } else {
	var_bldr.set(block(id&MAX_RLOCK_ID));
    }
      
    tree_node *tn = block::CALL(block(reduction_lock_proc), 
				var_bldr).make_tree_node(scope);

    if (before) {
      pos->parent()->insert_before(tn, pos->list_e());
    } else {
      pos->parent()->insert_after(tn, pos->list_e());
    }
    return tn;
}

tree_node *TaskProc::generate_runlock(int id, tree_for *scope, tree_node *pos,
				      int before)
{
    assert(pos);
    assert(reduction_unlock_proc);

    block var_bldr;
    if (fortran_form_flag) {
	var_sym *temp_var = NULL;
	if (before) {
	  assign_to_temp_before(pos, immed(id&MAX_RLOCK_ID), type_signed, 
				&temp_var);
	} else {
	  pos = assign_to_temp_after(pos, immed(id&MAX_RLOCK_ID), type_signed,
				     &temp_var);
	}
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());
    } else {
	var_bldr.set(block(id&MAX_RLOCK_ID));
    }
      
    tree_node *tn = block::CALL(block(reduction_unlock_proc), 
				var_bldr).make_tree_node(scope);

    if (before) {
      pos->parent()->insert_before(tn, pos->list_e());
    } else {
      pos->parent()->insert_after(tn, pos->list_e());
    }
    return tn;
}


void TaskProc::unpack_exposed_vars(struct_type *copy_type)
{
    assert(copy_type);
    block::set_proc(stub_block_->proc()->block());

    base_symtab *copy_tab = copy_type->parent();
    block_symtab *btab = stub_block_->symtab();
    assert(btab->is_ancestor(copy_tab));

    var_sym *my_struct_ptr = btab->new_var(copy_type->ptr_to(), 
					   MY_STRUCT_PTR_NAME);

    block struct_ptr_bldr(my_struct_ptr);

    block assigns_bldr;
    boolean first_assign = TRUE;

    assert(my_region_->var_map());
    sym_node_list_iter new_snli(&my_region_->var_map()->newsyms);

    int field_count = 0;
    while (!new_snli.is_empty()) {
        sym_node *new_sym = new_snli.step();
	assert(new_sym->is_var());

	var_sym *new_var = (var_sym *) new_sym;

	if (my_region_->fse()->symtab()->is_ancestor(new_var->parent()))
            continue;

 	type_node *field_type = copy_type->field_type(field_count);
	assert(field_type->compatible(new_var->type()));

	block src_bldr(struct_ptr_bldr.field(field_count));
        block dest_bldr(new_sym);

        if (first_assign) {
            assigns_bldr.set(block(dest_bldr = src_bldr));
            first_assign = FALSE;
        }
        else {
#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
            assigns_bldr.set(assigns_bldr.statement_append(block(dest_bldr = 
                                                                 src_bldr)));
#else
//
//  gcc version 2.6.3 generates incorrect code for the above statement. 
//  The block "assigns_bldr" gets cleared the first time the above code
//  is called in the loop, when instead a new statement should have
//  been appended to it. The code works fine with both gcc 2.5.8 and 
//  the IRIX 5.3 C++ compiler and Purify finds no memory errors.  
//  Thus it looks like the problem is caused by a bug in the gcc 2.6.3 
//  code generator.  A work around to the problem is below.
//
            block new_stmt(dest_bldr = src_bldr);
            block new_assign(assigns_bldr.statement_append(new_stmt));
            assigns_bldr.set(new_assign);
#endif
        }
	field_count++;
    }


    // my_nprocs
    //
    assert(suif_mynprocs_var && suif_mynprocs1_var && suif_mynprocs2_var);
    assert(nprocs_var_);

    block suif_mynprocs_bldr(suif_mynprocs_var);
    block task_mynprocs_bldr(nprocs_var_);
    block set_mynprocs_bldr(task_mynprocs_bldr = suif_mynprocs_bldr.dref());
    stub_block_->body()->push(set_mynprocs_bldr.make_tree_node(stub_block_));

    base_symtab *stab = stub_block_->symtab();
    var_sym *nprocs1_var = stab->lookup_var(nprocs_name(1));
    if (nprocs1_var) {
      suif_mynprocs_bldr.set(suif_mynprocs1_var);
      task_mynprocs_bldr.set(nprocs1_var);
      set_mynprocs_bldr.set(task_mynprocs_bldr = suif_mynprocs_bldr.dref());
      stub_block_->body()->push(set_mynprocs_bldr.make_tree_node(stub_block_));
    }

    var_sym *nprocs2_var = stab->lookup_var(nprocs_name(2));
    if (nprocs2_var) {
      suif_mynprocs_bldr.set(suif_mynprocs2_var);
      task_mynprocs_bldr.set(nprocs2_var);
      set_mynprocs_bldr.set(task_mynprocs_bldr = suif_mynprocs_bldr.dref());
      stub_block_->body()->push(set_mynprocs_bldr.make_tree_node(stub_block_));
    }

    if (field_count > 0) {
      tree_node_list *tnl = assigns_bldr.make_tree_node_list(stub_block_);
      stub_block_->body()->push(tnl);
      delete tnl;

      assert(suif_args_var);
      block cvt_bldr(struct_ptr_bldr = block(suif_args_var));

      stub_block_->body()->push(cvt_bldr.make_tree_node_list(stub_block_));
    }
}


void TaskProc::cleanup()
{
    // Add return instruction
    //
    stub_block_->body()->append(new tree_instr(new in_rrr(io_ret)));
}


static const char *runtime_fortran_type_name(type_node *the_type)
{
    if (the_type->unqual()->op() == TYPE_INT) {
	base_type *the_base = (base_type *)(the_type->unqual());
	if (the_base->size() == target.size[C_int]) {
	    return (the_base->is_signed() ? "si" : "ui");
                   /* "int" : "unsigned_int" */
	}
	else if (the_base->size() == target.size[C_char]) {
	    return (the_base->is_signed() ? "sc" : "uc");
	            /* "signed_char" : "unsigned_char" */
	}
	else if (the_base->size() == target.size[C_short]) {
	    return (the_base->is_signed() ? "ss" : "us");
	           /* "short" : "unsigned_short" */
	}
	else if (the_base->size() == target.size[C_long]) {
	    return (the_base->is_signed() ? "sl" : "ul");
	           /* "long" : "unsigned_long" */
	}
    }
    else if (the_type->unqual()->op() == TYPE_FLOAT) {
	if (the_type->size() == target.size[C_float]) {
	    return "f"; /* "float" */
	}
	else if (the_type->size() == target.size[C_double]) {
	    return "d"; /* "double" */
	}
	else if (the_type->size() == target.size[C_longdouble]) {
	    return "ld"; /* "long_double" */
	}
    }

    assert_msg(FALSE,
	       ("Unrecognized type (%d) in reduction", the_type->type_id()));
    return NULL;
}



static const char *runtime_type_name(type_node *the_type)
{
    if (the_type->unqual()->op() == TYPE_INT) {
	base_type *the_base = (base_type *)(the_type->unqual());
	if (the_base->size() == target.size[C_int]) {
	    return (the_base->is_signed() ? "int" : "unsigned_int");
	}
	else if (the_base->size() == target.size[C_char]) {
	    return (the_base->is_signed() ? "signed_char" : "unsigned_char");
	}
	else if (the_base->size() == target.size[C_short]) {
	    return (the_base->is_signed() ? "short" : "unsigned_short");
	}
	else if (the_base->size() == target.size[C_long]) {
	    return (the_base->is_signed() ? "long" : "unsigned_long");
	}
    }
    else if (the_type->unqual()->op() == TYPE_FLOAT) {
	if (the_type->size() == target.size[C_float]) {
	    return "float";
	}
	else if (the_type->size() == target.size[C_double]) {
	    return "double";
	}
	else if (the_type->size() == target.size[C_longdouble]) {
	    return "long_double";
	}
    }

    assert_msg(FALSE,
	       ("Unrecognized type (%d) in reduction", the_type->type_id()));
    return NULL;
}



  /* Private methods */

// Create parameters passed to task procedure by runtime system
// 
void TaskProc::create_task_params()
{
    block_symtab *curr_tab = stub_block_->symtab();
    proc_symtab *proc_tab = stub_block_->proc()->block()->proc_syms();
    assert(proc_tab->params()->is_empty());

    // param = processor id
    //
    assert(type_signed);
    if ((my_id_vars_[0] = curr_tab->lookup_var(MY_PID_NAME)) != NULL)
      {
	if (my_id_vars_[0]->parent() != proc_tab) 
	  {
	    my_id_vars_[0]->parent()->remove_sym(my_id_vars_[0]);
	    proc_tab->add_sym(my_id_vars_[0]);
	  }
      }
    else
      {
        my_id_vars_[0] = proc_tab->new_var(type_signed, MY_PID_NAME);
      }

    my_id_vars_[0]->set_param();
    proc_tab->params()->append(my_id_vars_[0]);
}

// Read number of processors 
//
void TaskProc::set_nprocs()
{
    base_symtab *stab = stub_block_->symtab();
    if (!(nprocs_var_ = stab->lookup_var(nprocs_name(0))))
      nprocs_var_ = stab->new_var(type_signed, nprocs_name(0));
    assert(nprocs_var_);

    block nprocs_bldr(nprocs_var_);
    block assign_bldr;

    if (nprocs_flag > 0) {  // Fixed number of processors
        assign_bldr.set(nprocs_bldr = block(nprocs_flag));
    }
    else {  // Read number of processors at runtime

        // nproc now assigned value by runtime library
        // we no longer need to set it here

       	// block bldr;
        // assign_bldr.set(nprocs_bldr = bldr.CALL(block(nprocs_proc)));

        return; 
    }
    stub_block_->body()->push(assign_bldr.make_tree_node(stub_block_));
}

void TaskProc::set_proc_id()
{
    char name[8] = "proc_id";
    
    base_symtab *stab = stub_block_->symtab();
    if (!(proc_id_ = stab->lookup_var(name)))
      proc_id_ = stab->new_var(type_signed, name);
    assert(proc_id_);
}

void TaskProc::set_sync_id()
{
    char name[8] = "sync_id";
    
    base_symtab *stab = stub_block_->symtab();
    if (!(sync_id_ = stab->lookup_var(name)))
      sync_id_ = stab->new_var(type_signed, name);
    assert(sync_id_);
}

void TaskProc::set_setjmp_val()
{
    char name[11] = "setjmp_val";
    
    base_symtab *stab = stub_block_->symtab();
    if (!(setjmp_val_ = stab->lookup_var(name)))
      setjmp_val_ = stab->new_var(type_signed, name);
    assert(setjmp_val_);
}

/*
---------------------------------------------------

 Handle variables passed by reference into the task

---------------------------------------------------
*/

// Variables are passed by reference in the following cases:
//   -- The var is outwards exposed  AND
//   -- The var is written OR it is not a scalar (b/c we don't want to copy
//          the whole structure)
//
void TaskProc::find_by_reference(tree_node_list *l)
{
    // Traverse replacements list looking for non-scalars:
    //
    sym_node_list *snl;

    assert(my_region_->var_map());
    snl = &(my_region_->var_map()->newsyms);
    sym_node_list_iter snli(snl);

    while (!snli.is_empty()) {
        sym_node *sn = snli.step();
        assert(sn->is_var());

        var_sym *var = (var_sym *) sn;
        if (!var->is_scalar()) {
	    add_by_reference(var);
        }
    }

    // Find written vars:
    //
    l->map(find_by_reference_tree, this, TRUE);  
}


void find_by_reference_instr(instruction *i, void *x)
{
  TaskProc *task = (TaskProc *) x;

  if (i->opcode() == io_ldc) {
      in_ldc *ldc = (in_ldc *) i;
      immed val = ldc->value();
      if (val.is_symbol())  {
	  sym_node *sn = val.symbol();
	  if (sn->is_var()) 
	    task->add_by_reference((var_sym *) sn);
      }
  }
}


void find_by_reference_tree(tree_node *tn, void *x)
{
    TaskProc *task = (TaskProc *) x;
    annote *an;

    if (tn->kind() == TREE_FOR) {
	annote_list_iter ali(tn->annotes());
	while (!ali.is_empty()) {
	    an = ali.step();

	    // Find global reduction variables:
	    //
	    //
	    if (an->name() == k_reduced) {
	        sym_node *sn = (*an->immeds())[1].symbol();
		var_sym *reduce_var = (var_sym *) sn;
		if (!reduce_var->type()->is_ptr() &&
		    !reduce_var->type()->is_array())
		    task->add_by_reference(reduce_var);
	    }
	    else if (an->name() == k_reduced_gen) {
		immed_list_iter ili(an->immeds());
		immed im = ili.step();  // proc_sym
		im = ili.step();  // in_cal
		while (!ili.is_empty()) {
		    im = ili.step();
		    sym_node *sn = im.symbol();
		    var_sym *reduce_var = (var_sym*) sn;
		    if (!reduce_var->type()->is_ptr() &&
			!reduce_var->type()->is_array())
			task->add_by_reference(reduce_var);
		}
	    }
	    //
	    // Initialized and/or finalized
	    else if (an->name() == code_gen_initialize_annote::k_annote) {
		code_gen_initialize_annote *initial_info =
		    (code_gen_initialize_annote *) an->data();
		var_sym *orig_var = initial_info->array;
		if (!orig_var->type()->is_ptr()) 
		    task->add_by_reference(orig_var);
	    }
	    else if (an->name() == code_gen_finalize_annote::k_annote) {
		code_gen_finalize_annote *final_info =
		    (code_gen_finalize_annote *) an->data();		

		var_sym *final_var = final_info->nosave_var3;
		if (final_var && !final_var->type()->is_ptr()) {
		  task->add_by_reference(final_var, FALSE);
		}

		var_sym *orig_var = final_info->array;
		if (orig_var && !orig_var->type()->is_ptr()) {
		  task->add_by_reference(orig_var);
		}
	    }
	}
    }

    // Grab written variables:
    //
    if (tn->kind() == TREE_INSTR) {
	tree_instr *ti = (tree_instr *) tn;
        instruction *ins = ti->instr();
	
	if (ins->dst_op().is_symbol()) {
            var_sym *dst_var = ins->dst_op().symbol();
	    task->add_by_reference(dst_var);
        }

	ti->instr_map(find_by_reference_instr, task);

    }
}


// Turn the variable into a variable that's passed by reference.  This
// involves changing the variable's original type to a pointer to the
// original type and replacing instances of the variable by lod's
// of the variable.  This is all accomplished using the useful library's
// "do_replacement" routine (it's called once for the entire task in
// "fix_by_reference").  Here, a "replacement" annotation with an
// operand containing a lod of the variable is added to the var_sym.
//
void TaskProc::add_by_reference(var_sym *var, boolean check_list /* = TRUE */)
{
    replacements *r = my_region_->var_map();

    if (var->peek_annote(k_replacement)) return;

    if (check_list && !r->newsyms.lookup(var)) return; 

    // If the variable is being passed in, the variable definition is
    // unneeded, so nuke it.
    //
    if (var->has_var_def()) {
      var_def *def = var->definition();
      def->parent()->remove_def(def);
      delete def;
    }

    // Find the corresponding variable in the 
    // Parent and mark it
    sym_node_list_iter old_snli(&r->oldsyms);
    sym_node_list_iter new_snli(&r->newsyms);
    
    while (!old_snli.is_empty()) {
      assert(!new_snli.is_empty());
       
      sym_node *new_sym = new_snli.step();
      sym_node *old_sym = old_snli.step();
       
      if (new_sym == var) {
	//if (!new_sym->annotes()->peek_annote(k_passed_by_ref))
	//new_sym->annotes()->append(new annote(k_passed_by_ref));
	if (!old_sym->annotes()->peek_annote(k_passed_by_ref))
	  old_sym->annotes()->append(new annote(k_passed_by_ref));
	break;
      }
      if (check_list) {
        assert(!old_snli.is_empty());
        assert(!new_snli.is_empty());
      }
    }                              
 
 

    type_node *old_type = var->type();
    type_node *new_type = old_type->ptr_to();
    var->reset_addr_taken();

    var->set_type(new_type);
    in_rrr *lod = new in_rrr(io_lod, old_type, operand(), operand(var));
    immed_list *iml = new immed_list();
    iml->append(immed(operand(lod)));
    annote *an = new annote(k_replacement, NULL);
    an->set_immeds(iml, var);
    var->annotes()->append(an);
    
    if (debug['r'-'\0']) {
        printf("Passing var by reference: ");
	var->print_full(stdout);
	printf("Old type\n");
	old_type->print_full(stdout);
	printf("New type\n");
	new_type->print_full(stdout);
	printf("\n");
    }
}

// Fix up all references to the variables that are passed by reference
// by calling the useful library's "do_replacement" routine.  All
// "replacement" annotations for this task should have already been
// added by the "add_reference" routine.
// 
void TaskProc::fix_by_reference()
{
    do_replacement(stub_block_);
    walk(stub_block_->proc()->block(), &kill_replacement_annotes_on_object);
}

/*
----------------------------------------------

 Handle privatized (and reduction) variables:

-----------------------------------------------
*/


void TaskProc::find_privatized(tree_node_list *l)
{
    l->map(find_privatized_tree, this, TRUE);      
}

void find_privatized_tree(tree_node *tn, void *x)
{
    // Look for privatized and reduced annotations:
    //
    TaskProc *task = (TaskProc *) x;

    if (tn->kind() != TREE_FOR) return;
    task->find_privatized_annotes((tree_for *) tn);
}



void TaskProc::find_privatized_annotes(tree_for *tf)
{
    replacements substitute_syms;
    immed_list_pair substitute_addrs;
    // proc_sym *curr_proc = tf->proc();

    annote_list_iter ali(tf->annotes());
    while (!ali.is_empty()) {
	annote *outer_an = ali.step();

	if (outer_an->name() == k_privatized) {

	    immed_list_iter ili(outer_an->immeds());
	    while (!ili.is_empty()) {
		immed im = ili.step();
		assert(im.is_symbol());
		sym_addr priv_addr = im.addr();
		assert(priv_addr.symbol()->is_var());
		var_sym *priv_var = (var_sym *) priv_addr.symbol();

		// Already privatized this variable
		//
		annote *priv_an = 
		  priv_var->annotes()->get_annote(k_privatized_var);
		if (priv_an && is_nested((tree_for *) priv_an->data(), tf)) {
		    priv_var->annotes()->append(priv_an);
		    continue;
		}

		// See if this variable needs initialization or finalization
		//
		annote_list_iter ali2(tf->annotes());
		boolean init_or_final = FALSE;

		while (!ali2.is_empty()) {
		    annote *an = ali2.step();
		    if (an->name() == code_gen_initialize_annote::k_annote) {
			code_gen_initialize_annote *init_info =
			    (code_gen_initialize_annote *) an->data();
			if (init_info->array == priv_var) {
			  var_sym *local_var;
			  annote *local_an = 
			      priv_var->annotes()->get_annote(k_local_var);

			  if (local_an && is_nested(
		            ((local_info *)local_an->data())->the_loop, tf)) {

			       local_var = 
				 ((local_info *) local_an->data())->local_var;
			  } else {
			      local_var = find_privatized_var(priv_addr, 
 			        tf, &substitute_syms, &substitute_addrs, 
                                body_block()->symtab(), TRUE);
			      local_info *li = new local_info;
			      li->the_loop = tf;
			      li->local_var = local_var;
			      local_an = new annote(k_local_var, li);
			  }

			  priv_var->annotes()->append(local_an);
			  init_info->set_from(priv_var);
			  init_info->set_to(local_var);
			  init_or_final = TRUE;
		        }
		    } 
		    else if (an->name()==code_gen_finalize_annote::k_annote) {
			code_gen_finalize_annote *final_info =
			    (code_gen_finalize_annote *) an->data();
			if (final_info->array == priv_var) {
			  var_sym *local_var;
			  annote *local_an = 
			      priv_var->annotes()->get_annote(k_local_var);

			  if (local_an && is_nested(
		            ((local_info *)local_an->data())->the_loop, tf)) {

			       local_var = 
				 ((local_info *) local_an->data())->local_var;
			  } else {
			      local_var = find_privatized_var(priv_addr, 
 			        tf, &substitute_syms, &substitute_addrs, 
				stub_block()->symtab(), TRUE);
			      local_info *li = new local_info;
			      li->the_loop = tf;
			      li->local_var = local_var;
			      local_an = new annote(k_local_var, li);
			  }

			  priv_var->annotes()->append(local_an);
			  var_sym *final_var = create_finalized_var(priv_addr, 
			      stub_block()->symtab(),
			      &substitute_syms, &substitute_addrs);

			  annote *priv_an = new annote(k_privatized_var, tf);
			  if (final_var) final_var->annotes()->append(priv_an);

			  final_info->nosave_var1 = priv_var;
			  final_info->nosave_var2 = local_var;
			  final_info->nosave_var3 = final_var;
			  init_or_final = TRUE;
		        }
		    }
		}
		if (!init_or_final) {
		    find_privatized_var(priv_addr, tf, &substitute_syms,
		      &substitute_addrs, body_block()->symtab(), FALSE);
		    annote *priv_an = new annote(k_privatized_var, tf);
		    priv_var->annotes()->append(priv_an);
		}
	    } // endwhile (!ili.is_empty()) 
	} // endif (an->name() == k_privatized)

	if (outer_an->name() == k_reduced) {
	    sym_addr reduce_addr =  (*outer_an->immeds())[1].addr();

	    var_sym *local_var = find_privatized_var(reduce_addr, tf,
	      &substitute_syms, &substitute_addrs, body_block()->symtab(),
	      TRUE);
	    assert(local_var);

	    // Reduction vars always get their own private copy in the task
	    // The var gets appended to the annotation so we can find
	    // it later.
	    outer_an->immeds()->append(local_var);
	}
	
 	if (outer_an->name() == k_reduced_gen) {
	    immed_list_iter ili(outer_an->immeds());
	    ili.glist_iter::step();  // proc_sym
	    ili.glist_iter::step();  // in_cal
	    while (!ili.is_empty()) {
		immed_list_e *ile = (immed_list_e *)ili.glist_iter::step();  
		immed im = ile->contents;  // var_sym's
		assert(im.is_symbol());
		sym_addr reduce_addr = im.addr();
		var_sym *local_var = find_privatized_var(reduce_addr, tf,
		  &substitute_syms, &substitute_addrs, body_block()->symtab(), 
		  TRUE);
		assert(local_var);
		// Reduction vars always get their own private copy in the task
		// The var gets appended to the annotation so we can find
		// it later.
		outer_an->immeds()->insert_after
		    (new immed_list_e(immed(local_var)), ile);
	    }
	}
    }


    if (debug['p'-'\0']>1) {
	print_var_map(&substitute_syms, stdout);

	printf("** Sym_addrs **\n");
	printf("  Old\n");
	immed_list_iter ili(&substitute_addrs.old_immeds);
	while (!ili.is_empty()) { ili.step().print(stdout); printf("\n"); }
	printf("  New\n");
	ili.reset(&substitute_addrs.new_immeds);
	while (!ili.is_empty()) { ili.step().print(stdout); printf("\n"); }
    }


    // Make sure index var is privatized and finalized
    annote *priv_an = tf->index()->annotes()->get_annote(k_privatized_var);
    if ((!priv_an || 
	 !is_nested((tree_for *) priv_an->data(), tf)) &&
	body_block()->symtab()->is_visible(tf->index()))
      {
	var_sym *local_var = find_privatized_var(sym_addr(tf->index(),0), tf, 
	  &substitute_syms, &substitute_addrs, body_block()->symtab(), TRUE);

	immed_list *iml = new immed_list();
	iml->append(tf->index());
	priv_an = new annote(k_privatized, NULL);
	priv_an->set_immeds(iml, tf);
	tf->annotes()->append(priv_an);

	code_gen_finalize_annote *final_info = new code_gen_finalize_annote();
	final_info->array = tf->index();
	final_info->nosave_var1 = tf->index();
	final_info->nosave_var2 = local_var;
	final_info->nosave_var3 = NULL;

	annote *final_an = 
	  new annote(code_gen_finalize_annote::k_annote, final_info);
	tf->annotes()->append(final_an);
    }

    // Don't want tree_fors annotations substituted, so remove them
    // from the tree_for temporarily
    // The only exception is sync_counter annotes (from popt), which do need
    // to be substituted
    //
    annote_list al;
    al.copy(tf->annotes());
    tf->annotes()->clear();
    annote *sync_an = al.get_annote(k_sync_counter);
    if (sync_an) tf->annotes()->append(sync_an);

    // Now substitute the global privatizable variables and the
    // reduction variables, 'no_copy'=TRUE
    //
    if (!substitute_syms.oldsyms.is_empty())
	tf->clone_helper(&substitute_syms, TRUE); 

    if (!substitute_addrs.old_immeds.is_empty())
	tf->body()->map(replace_sym_addrs_tree, &substitute_addrs);

    tf->annotes()->append(&al);
}


var_sym *TaskProc::find_privatized_var(sym_addr priv_addr,
				       tree_for *tf,
				       replacements *substitute_syms, 
				       immed_list_pair *substitute_addrs, 
				       base_symtab *curr_scope,
				       boolean create_new_var)
{
    assert(priv_addr.symbol()->is_var());
    var_sym *priv_var = (var_sym *) priv_addr.symbol();
    replacements *repl = my_region_->var_map();
    boolean is_group_member = FALSE;
    type_node *true_type = priv_var->type()->unqual();
    type_node *local_var_type = priv_var->type()->unqual();
    var_sym *local_var = NULL;
    proc_symtab *proc_tab = tf->proc()->block()->proc_syms();

    if (debug['p'-'\0']) {
	printf("Privatizing `%s' at offset %d\n", 
	       priv_var->name(), priv_addr.offset());
    }

    // Need to dereference if priv_var is call-by-reference var
    //
    if (true_type->peek_annote(k_call_by_ref)) {
	assert(true_type->is_ptr());
	true_type = ((ptr_type *) true_type)->ref_type()->unqual();
    }

    // The variable being privatized is a member of a group -- find
    // out the type of the member itself.
    //
    if (true_type->op() == TYPE_GROUP) {
	struct_type *group_type = (struct_type *) true_type;
	int left;
	unsigned field_id = 
	    group_type->find_field_by_offset(priv_addr.offset(), left);

	assert_msg((left == 0),
          ("Can't privatize variable at offset %d in `%s'",
	   priv_addr.offset(), priv_var->name()));

	local_var_type = true_type = group_type->field_type(field_id);
	is_group_member = TRUE;
    }

    // Now that we have the true_type, figure out how to privatize 
    // the variable
    //
    annote *size_annote = has_total_size_annote(tf, priv_var);

    if (constant_bounds(true_type) && !size_annote) {
	if (is_group_member) {
	    local_var = add_privatized(priv_var, curr_scope,
				       local_var_type, TRUE);
	    substitute_addrs->old_immeds.append(priv_addr);
	    substitute_addrs->new_immeds.append(local_var);

	    if (debug['p'-'\0']>1) {
		printf("   GROUP -- local var `%s'\n", local_var->name());
	    }
        }

	else if (priv_var->parent()->is_ancestor(proc_tab) &&
		 !create_new_var) { 

	    // Note: cloning already creates a local copy in the task proc
	    // Syms that are already local get removed from
	    // replacements so they are not copied into the task:
	    //
	    sym_node_list_iter old_snli(&repl->oldsyms);
	    sym_node_list_iter new_snli(&repl->newsyms);

	    while (!old_snli.is_empty()) {
		assert(!new_snli.is_empty());

		sym_node *new_sym = new_snli.step();
		old_snli.step();

		if (new_sym == priv_var) { 
		    repl->oldsyms.remove(old_snli.cur_elem());
		    repl->newsyms.remove(new_snli.cur_elem());
		    break;
		}                              
	    }

	    // Move the var into the current scope
	    //
	    if (priv_var->parent() != curr_scope) {
		priv_var->parent()->remove_sym(priv_var);
		curr_scope->add_sym(priv_var);
	    }
	    add_privatized(priv_var, curr_scope);
	}

	else {
	    local_var = add_privatized(priv_var, curr_scope, local_var_type, 
				       TRUE);
	    if (priv_var->is_addr_taken()) local_var->set_addr_taken();
	    substitute_syms->oldsyms.append(priv_var);
	    substitute_syms->newsyms.append(local_var);
	}
    } 

    else if (known_bounds(true_type) || size_annote) {
	local_var = add_privatized_heap(priv_var, stub_block()->symtab(), 
					local_var_type);

	if (is_group_member) {
	    substitute_addrs->old_immeds.append(priv_addr);
	    substitute_addrs->new_immeds.append(local_var);

	    if (debug['p'-'\0']>1) {
		printf("   GROUP -- local var `%s'\n", local_var->name());
	    }
        }
	else {
	    if (priv_var->is_addr_taken()) local_var->set_addr_taken();
	    substitute_syms->oldsyms.append(priv_var);
	    substitute_syms->newsyms.append(local_var);
	}

	// generate malloc code
	generate_malloc(local_var, size_annote);
	generate_free(local_var);
    }

    if (local_var) 
      {
	immed_list *iml = new immed_list();
	iml->append(local_var);
	annote *an = new annote(k_privatizable, NULL);
	an->set_immeds(iml, tf);
	tf->annotes()->append(an);
      }

    return local_var;
}

//
// Create extra local variable to hold "current" version of the
// variable.  For the last processor this variable gets assigned the
// original variable, all others get local copy.
//
var_sym *TaskProc::create_finalized_var(sym_addr priv_addr,
					base_symtab *curr_scope,
					replacements *substitute_syms, 
					immed_list_pair *substitute_addrs)
{
    assert(priv_addr.symbol()->is_var());
    var_sym *priv_var = (var_sym *) priv_addr.symbol();

    type_node *true_type = priv_var->type()->unqual();
    if (true_type->peek_annote(k_call_by_ref)) {
	assert(true_type->is_ptr());
	true_type = ((ptr_type *) true_type)->ref_type()->unqual();
    }

    if (true_type->is_scalar()) return NULL;

    if (debug['p'-'\0']) {
	printf("Creating finalized var `%s'\n", priv_var->name());
    }

    char *buf = new char[strlen(priv_var->name()) + 10];
    sprintf(buf, "__final_%s", priv_var->name());

    type_node *my_local_type = priv_var->type()->copy();
    my_local_type->get_annote(k_call_by_ref);
    my_local_type = priv_var->type()->parent()->install_type(my_local_type);
    var_sym *finalized_var = curr_scope->new_unique_var(my_local_type, buf);

    // Big hack -- remove local var from replacements and put this variable
    // in instead.
    //

    // If group type then look in substitute_addrs list, otherwise 
    // look in substitute_syms
    //
    if (true_type->op() == TYPE_GROUP) {
	immed_list_iter old_ili(&substitute_addrs->old_immeds);
	immed_list_iter new_ili(&substitute_addrs->new_immeds);

	while (!old_ili.is_empty()) {
	    assert(!new_ili.is_empty());
	    immed old_im = old_ili.step();

	    if (old_im == priv_addr) 
		new_ili.cur_elem()->contents = immed(finalized_var);
	}
    }
    else {
	sym_node_list_iter old_snli(&substitute_syms->oldsyms);
	sym_node_list_iter new_snli(&substitute_syms->newsyms);

	while (!old_snli.is_empty()) {
	    assert(!new_snli.is_empty());
	    sym_node *old_sn = old_snli.step();
	    new_snli.step();

	    if (old_sn == priv_addr.symbol()) 
		new_snli.cur_elem()->contents = finalized_var;
	}
    }

    return finalized_var;
}


// Privatize a variable.  If the variable is a call-by-ref
// variable then we need to remove the pointer.
// This involves removing the pointer to the variable's type
// and replacing instances of the variable by ldc's
// of the variable.  This is all accomplished using the useful library's
// "do_replacement" routine (it's called once for the entire task in
// "fix_privatized").  Here, a "replacement" annotation
// with an operand containing a ldc of the variable is added to the var_sym.
//
//
var_sym *TaskProc::add_privatized(var_sym *var, 
				  base_symtab *curr_scope,
				  type_node *local_var_type /* = NULL */,
				  boolean create_new_var /* = FALSE */)
{
    var_sym *local_var;

    if (create_new_var) {
	char *buf  = new char[strlen(var->name()) + 8];
	sprintf(buf, "__priv_%s", var->name());
	local_var = curr_scope->new_unique_var(local_var_type, buf);
    }
    else local_var = var;

    if (local_var->type()->peek_annote(k_call_by_ref)) {
	type_node *old_type = local_var->type();
	assert(old_type->is_ptr());
	type_node *new_type = ((ptr_type *) old_type)->ref_type();

	local_var->set_type(new_type);
	in_ldc *ldc = new in_ldc(new_type->ptr_to(), operand(), 
				 immed(local_var));
	local_var->set_addr_taken();
	immed_list *iml = new immed_list(immed(operand(ldc)));
	annote *an = new annote(k_replacement, NULL);
	an->set_immeds(iml, local_var);
	local_var->annotes()->append(an);

	if (debug['r'-'\0']) {
	    printf("Privatizing call-by-ref var: ");
	    local_var->print_full(stdout);
    	    printf("Current type\n"); local_var->type()->print_full(stdout);
	    printf("\n");
	}
    }
      
    return local_var;
}


// Remove call-by-ref annotation from variable, but keep in the pointer form
// because local_var is on heap anyway.
//
var_sym *TaskProc::add_privatized_heap(var_sym *var, base_symtab *curr_scope,
				       type_node *local_var_type)
{
    // Note: variable sized array can only occur as call-by-ref params
    // in Fortran. 
    //
/* Not good for C programs
    assert(local_var_type->peek_annote(k_call_by_ref));*/
    assert(local_var_type->is_ptr());

    char *buf  = new char[strlen(var->name()) + 8];
    sprintf(buf, "__priv_%s", var->name());

    type_node *my_local_type = local_var_type->copy();
    my_local_type->get_annote(k_call_by_ref);
    my_local_type = local_var_type->parent()->install_type(my_local_type);
    var_sym *local_var = curr_scope->new_unique_var(my_local_type, buf);
	
    return local_var;
}

static void replace_sym_addrs_tree(tree_node *tn, void *x)
{
    if (tn->is_instr()) 
	((tree_instr *) tn)->instr_map(replace_sym_addrs_instr, x);
}


static void replace_sym_addrs_instr(instruction *i, void *x)
{
    immed_list_pair *substitute_immeds = (immed_list_pair *) x;

    if (i->opcode() == io_ldc) {
	in_ldc *ldc = (in_ldc *) i;
	immed curr_im = ldc->value();

	immed *new_im = substitute_immeds->lookup(curr_im);
	if (new_im) {
	    ldc->set_value(*new_im);
	    assert(new_im->is_symbol() && new_im->symbol()->is_var());
	    var_sym *new_var = (var_sym *) new_im->symbol();
	    new_var->set_addr_taken();

	    while (ldc->get_annote(k_fields)) ;  // strip "fields" annotes
	}
    }
}


// Generate code like
// "var = (type) malloc(sizeof(elem_type)*dim1(var)*dim2(var)*...)"
//
void TaskProc::generate_malloc(var_sym *var, annote *size_annote /* = NULL */)
{
    type_node *typ = var->type()->unqual();
    assert(typ->is_ptr());
    typ = ((ptr_type *) typ)->ref_type()->unqual();

    global_symtab *gtab = fileset->globals();

    // Figure out name of memory allocation routines to call, and
    // number of elements
    //
    char alloc_proc_name[8] = "malloc";
    operand num_elems_op;

    if (size_annote) {
	num_elems_op = 
	  (*size_annote->immeds())[1].op().clone(body_block_->scope());
    } else {
	num_elems_op = calc_num_elems(typ, body_block_);// may be an expression
    }

    proc_sym *alloc_proc_var= gtab->lookup_proc(alloc_proc_name);
    if (!alloc_proc_var) {
	type_node *typen = new func_type(type_ptr, type_ptr_diff);
	typen = gtab->install_type(typen);
	assert(typen->is_func());
        alloc_proc_var = gtab->new_proc((func_type *) typen,
					src_c, alloc_proc_name);
    }

    // Generate the memory allocation calls
    //
    type_node *elem_typ = get_element_type(typ);
    in_cal *the_call =
        new in_cal(type_ptr, operand(), addr_op(alloc_proc_var),
                   num_elems_op * (elem_typ->size() / 8));
    body_block_->parent()->insert_before(assign(var, operand(the_call)), 
					 body_block_->list_e());
}


// Generate code like "free(var)"
//
void TaskProc::generate_free(var_sym *var)
{
//	type_node *typ = var->type()->unqual();
//	assert(typ->is_ptr());
//	typ = ((ptr_type *) typ)->ref_type()->unqual();

    global_symtab *gtab = fileset->globals();

    // Figure out name of free routines to call, and
    // number of elements
    //
    char free_proc_name[8] = "free";
    proc_sym *free_proc_var= gtab->lookup_proc(free_proc_name);
    if (!free_proc_var) {
	type_node *typen = block::parse_type(gtab, "void (void *)");
	assert(typen->is_func());
        free_proc_var = gtab->new_proc((func_type *) typen, src_c, 
				       free_proc_name);
    }

    // Generate the free calls
    //
    block free_proc_bldr(free_proc_var);
    block call_bldr(block::CALL(free_proc_bldr, block(var)));
					
    body_block_->parent()->insert_after(
      call_bldr.make_tree_node(body_block_->parent()), body_block_->list_e());
}


// Fix up all references to privatized variables using the useful library's
// "do_replacement" routine.  All of the "replacement" annotations have
// been added in the "add_privatized" routine.
// 
void TaskProc::fix_privatized()
{
    do_replacement(body_block_);
    walk(body_block_->proc()->block(), &kill_replacement_annotes_on_object);
}


/* 
----------------------------------

 For loop annotation processing:

---------------------------------- 
*/


// Traverse region looking for annotations
//
void TaskProc::find_par_loops(tree_node_list *l)
{
    assert(l);
    l->map(find_par_loops_tree, this, TRUE, TRUE);
}


static void find_par_loops_tree(tree_node *tn, void *x)
{
    TaskProc *task = (TaskProc *) x;

    if (tn->kind() == TREE_FOR) {
        tree_for *tf = (tree_for *) tn;

	// Add nprocs_var annotation
	//
	annote *an = new annote(k_nprocs_var, NULL);
	immed_list *iml = new immed_list();
	iml->append(immed(task->nprocs_var()));
	an->set_immeds(iml, tf);
	tf->annotes()->append(an);

	base_symtab *stab = task->stub_block()->symtab();
	var_sym *nprocs1_var = stab->lookup_var(nprocs_name(1));
	if (nprocs1_var) {
	  an = new annote(k_nprocs1_var, NULL);
	  iml = new immed_list();
	  iml->append(immed(nprocs1_var));
	  an->set_immeds(iml, tf);
	  tf->annotes()->append(an);
	}

	var_sym *nprocs2_var = stab->lookup_var(nprocs_name(2));
	if (nprocs2_var) {
	  an = new annote(k_nprocs2_var, NULL);
	  iml = new immed_list();
	  iml->append(immed(nprocs2_var));
	  an->set_immeds(iml, tf);
	  tf->annotes()->append(an);
	}

	// Store the original upper and lower bounds in an annotation; 
	// we may need them later:  
        // *Do not* delete the original bounds!
	operand *orig_lb = new operand(tf->lb_op());
	operand *orig_ub = new operand(tf->ub_op());

        an = new annote(k_orig_lower_bound, (void *) orig_lb);
        tf->annotes()->append(an);
        an = new annote(k_orig_upper_bound, (void *) orig_ub);
        tf->annotes()->append(an);

        // Look for annotations on for loop:
        //

        // Can have multiple of the following annotations:
        //
	tree_for * reduction_tf = NULL;

	locking_reduces = new tnl;
	locking_reduce_end = 0;
	locking_reduce_begin = 0;
	rotating_reduces = new tnl;
	num_bits = 0;
	reduced_vars.clear();

	annote_list_iter ali(tf->annotes());
        while (!ali.is_empty()) {
	    an = ali.step();
	    if (an->name() == k_reduced) {
		tree_for * tf2 = process_reduced_annote(tf, an, task);
		if (reduction_tf && reduction_tf != tf2) {
		    error_line(0, tf2, 
			       "Simultaneous reductions at 2 loop levels\n");
		}
		else reduction_tf = tf2;
	    }

	    if (an->name() == k_reduced_gen) {
		tree_for * tf2 = process_reduced_gen_annote(tf, an, task);
		if (reduction_tf && reduction_tf != tf2) {
		    error_line(0, tf2, 
			       "Simultaneous reductions at 2 loop levels\n");
		}
		else reduction_tf = tf2;
	    }

	    if (an->name() == code_gen_initialize_annote::k_annote) {
		process_initialization_annote(tf, an);
            }

	    if (an->name() == code_gen_finalize_annote::k_annote) {
		process_finalization_annote(tf, an, task);
            }
        }

        task->generate_reduction_locks(reduction_tf);

        //
	// The following are mutually exclusive:
        //
	if ((an = tf->annotes()->peek_annote(k_loop_cyclic))) {
	    process_cyclic_annote(tf, an, task);

	    an = tf->annotes()->get_annote(k_loop_bounds);  
	    if (an) delete an;
	    an = tf->annotes()->get_annote(k_doall);  
	    if (an) delete an;
        }
	else if ((an = tf->annotes()->peek_annote(k_loop_bounds))) {
	    process_decomp_annote(tf, an);

	    an = tf->annotes()->get_annote(k_doall);  
	    if (an) delete an;
	}
	else if ((an = tf->annotes()->peek_annote(k_doall)) ||
		 (an = tf->annotes()->peek_annote(k_doacross))) {
	    process_doall_annote(tf, task);  // Just schedules the loop
        }
	else if ((an = tf->annotes()->peek_annote(k_pipeline))) {
	    process_pipeline_annote(tf, task);
	    assert_msg(0, ("Should see k_doall too\n"));
	}

	// Block size annotes from pgen
	//
	if ((an = tf->annotes()->peek_annote(k_block_sizes))) {
  	    process_block_sizes(tf, an);
	}

	// Tile after scheduling
	if ((an = tf->annotes()->get_annote(k_tile_loops))) {
	    process_tile_annote(tf, an);
	    delete an;
	}

        //
        // Can only have one of each the following annotations:
        //
	if ((an = tf->annotes()->peek_annote(k_sync_counter))) {
	    task->synch_counter(tf, an);
	}
	if ((an = tf->annotes()->peek_annote(k_doacross))) {
	    process_doacross_annote(tf, an, task);
        }
	if ((an = tf->annotes()->peek_annote(k_guard))) {
	    process_guard_annote(tf, an);
        }
    }

    assert(tn);  // Make sure we still have the node
    annote_list *anl = tn->annotes();

    // Since annotations on tree_instr's are not supported, check the 
    // corresponding instruction:
    //
    if (tn->kind() == TREE_INSTR) {  
	instruction *ins = ((tree_instr *) tn)->instr();
	assert(ins);
	anl = ins->annotes();
    }

    annote *an;
    if ((an = anl->peek_annote(k_global_barrier))) {
        task->generate_barrier(tn);
    }

    if ((an = anl->peek_annote(k_sync_neighbor))) {
        task->generate_sync_neighbor(tn);
    }

    if ((an = anl->peek_annote(k_lock))) {
        assert(an->immeds()->count() > 0);
        immed im = an->immeds()->pop();
    	assert(im.is_integer());
	int lock_id = im.integer();

        task->generate_lock(tn, lock_id);
    }

    if ((an = anl->peek_annote(k_unlock))) {
        assert(an->immeds()->count() > 0);
        immed im = an->immeds()->pop();
    	assert(im.is_integer());
	int lock_id = im.integer();

        task->generate_unlock(tn, lock_id);
    }

    if ((an = anl->peek_annote(k_consumer))) {
	annote_list_iter ali(anl);
	while (!ali.is_empty()) {
	    an = ali.step();
	    if (an->name() == k_consumer) {
		immed_list *il = an->immeds();
		int counter_id = (*il)[0].integer();
		int pid_offset = (*il)[1].integer();
		task->generate_counter_wait(tn, counter_id, pid_offset);
	    }
	}
    }

    if ((an = anl->peek_annote(k_producer))) {
	annote_list_iter ali(anl);
	while (!ali.is_empty()) {
	    an = ali.step();
	    if (an->name() == k_producer) {
		immed_list *il = an->immeds();
		int counter_id = (*il)[0].integer();
		int pid_offset = (*il)[1].integer();
		task->generate_counter_set(tn, counter_id, pid_offset);
	    }
	}
    }
}

//
// ["reduced": type global_var local_var]
//
static tree_for * process_reduced_annote(tree_for *tf, annote *an, 
					 TaskProc *task)
{
    if (debug['g'-'\0']) {
        printf("    Processing a reduced loop with index %s\n",
	       tf->index()->name());

	if (debug['g'-'\0']>1) 
	  {
	    an->print(stdout);
	    printf("\n");
	  }
    }

    const char *reduction_str = (*an->immeds())[0].string();
    sym_addr global_sym = (*an->immeds())[1].addr();
    sym_addr local_sym = (*an->immeds())[2].addr();

    reduction_kinds kind;
    if (!strcmp(reduction_str, "sum")) 	        kind = REDUCTION_SUM;
    else if (!strcmp(reduction_str, "product")) kind = REDUCTION_PRODUCT;
    else if (!strcmp(reduction_str, "max"))     kind = REDUCTION_MAX;
    else if (!strcmp(reduction_str, "min"))     kind = REDUCTION_MIN;
    else assert_msg(FALSE, 
		    ("Bad reduction kind in loop `%s'", tf->index()->name()));

    // Now find outermost loop to put reduction calls around.  Any loop with a 
    // "reduction" annote is a candidate.
    //
    tree_for *reduced_loop = tf;
    tree_for *curr_loop = next_outer_for(tf->parent());

    while (curr_loop) {
	annote_list_iter ali(curr_loop->annotes());
	while (!ali.is_empty()) {
	    annote *curr_an = ali.step();
	    if ((curr_an->name() == k_reduction) && 
		((*curr_an->immeds())[1].addr() == global_sym)) {

		reduced_loop = curr_loop;
		break; // exits inner while only
	    }
	}
	curr_loop = next_outer_for(curr_loop->parent());
    }

    reduced_vars.append(local_sym.symbol());
    task->generate_reduction(reduced_loop, global_sym, local_sym, kind);

    return reduced_loop;  // so pgen knows where to insert lock sync
}


//
// ["reduced_gen": type global_var local_var]
//
static tree_for * process_reduced_gen_annote(tree_for *tf, annote *an, 
					 TaskProc *task)
{
    if (debug['g'-'\0']) {
        printf("    Processing a reduced_gen loop with index %s\n",
	       tf->index()->name());

	if (debug['g'-'\0']>1) 
	  {
	    an->print(stdout);
	    printf("\n");
	  }
    }

    //char *reduction_str = (*an->immeds())[0].string();
    sym_addr proc = (*an->immeds())[0].addr();
    instruction *in = (*an->immeds())[1].instr();
    sym_addr global_sym = (*an->immeds())[2].addr();
//    sym_addr local_sym = (*an->immeds())[3].addr();

    // Now find outermost loop to put reduction calls around.  Any loop with a 
    // "reduction" annote is a candidate.
    //
    tree_for *reduced_loop = tf;
    tree_for *curr_loop = next_outer_for(tf->parent());

    while (curr_loop) {
	annote_list_iter ali(curr_loop->annotes());
	while (!ali.is_empty()) {
	    annote *curr_an = ali.step();
	    if ((curr_an->name() == k_reduction_gen) && 
		((*curr_an->immeds())[2].addr() == global_sym)) {

		reduced_loop = curr_loop;
		break; // exits inner while only
	    }
	}
	curr_loop = next_outer_for(curr_loop->parent());
    }

    in_cal *ic = NULL;
    if (in->opcode() == io_cal) ic = (in_cal *) in;
    else {
	// Find the call -- even though a cal was originally put into the
	// annotation, the code to update passing variables by reference
	// may have changed the root instruction
	for (unsigned i = 0; i < in->num_srcs(); i++) {
	    operand op = in->src_op(i);
	    if (op.is_instr() && op.instr()->opcode() == io_cal) {
		ic = (in_cal *) op.instr();
		op.remove();
	    }
	}
    }
    assert(ic);
    task->generate_reduction_gen(reduced_loop, proc, ic, an->immeds());

    return reduced_loop;  // so pgen knows where to insert lock sync
}


//
// ["needs_initialization": global_var ... local_var]
// 
static void process_initialization_annote(tree_for *tf, annote *an)
{
    if (debug['g'-'\0']) {
	printf("    Processing a needs_initialization annote in loop %s\n", 
	       tf->index()->name());
    }

    struct code_gen_initialize_annote *init_info =
	(struct code_gen_initialize_annote *) an->data();

    tree_node_list *init_code = init_info->generate_code(tf->scope());
    tf->parent()->insert_before(init_code, tf->list_e());
}


//
// ["needs_finalization": global_var ... local_var final_var]
// 
static void process_finalization_annote(tree_for *tf, annote *an,
					TaskProc *task)
{
    if (debug['g'-'\0']) {
	printf("    Processing a needs_finalization annote in loop %s\n", 
	       tf->index()->name());

	if (debug['g'-'\0']>1) 
	  {
	    an->print(stdout);
	    printf("\n");
	  }
    }

    struct code_gen_finalize_annote *final_info =
	(struct code_gen_finalize_annote *) an->data();

    var_sym *local_var = final_info->nosave_var2;
    var_sym *final_var = final_info->nosave_var3;
    task->generate_finalization(tf,final_info->array,local_var,final_var);
}


//
// ["loop_cyclic": dimension offset]
//
static void process_cyclic_annote(tree_for *tf, annote *an, TaskProc *task)
{
    if (debug['g'-'\0']) {
        printf("    Parallelizing a cyclic loop with index %s\n",
	       tf->index()->name());
    }

    int dim = (*an->immeds())[0].integer();
    int offset = (*an->immeds())[1].integer();
    var_sym *my_id_var = task->my_id_var(dim);  
    assert(my_id_var);

    operand lb = create_cyclic_lb(tf, dim, offset, my_id_var);
    operand step = create_cyclic_step(tf, dim);

    kill_op(tf->lb_op());
    kill_op(tf->step_op());
    tf->set_lb_op(cast_op(lb, tf->index()->type()->unqual()));
    tf->set_step_op(cast_op(step, tf->index()->type()->unqual()));
}


//
// Cyclic lower bound (owner-computes)
//
//      lb_temp = (orig_lb-offset)%NPROC;
//      new_lb = orig_lb + my_id - lb_temp;
//      if (my_id < lb_temp) new_lb += NPROC;
//
static operand create_cyclic_lb(tree_for *tf, int dim, int offset, 
				var_sym *my_id_var)
{
    // Create new variables:
    //
    base_symtab *curr_tab = tf->scope();
    proc_symtab *proc_tab = tf->proc()->block()->proc_syms();
    var_sym *idx = tf->index();
    var_sym *new_lb_var = curr_tab->new_unique_var(idx->type(), "my_lb");
    var_sym *lb_temp_var = curr_tab->new_unique_var(idx->type(), "lb_temp");
    var_sym *nprocs_var = proc_tab->lookup_var(nprocs_name(dim));
    assert(nprocs_var);

    block orig_lb(tf->lb_op());
    block nprocs(nprocs_var);
    block lb_temp(lb_temp_var);
    block new_lb(new_lb_var);
    block my_id(my_id_var);

    block bldr(lb_temp = block((orig_lb - offset) % nprocs));
    bldr.set(bldr.statement_append(new_lb = orig_lb + my_id - lb_temp));
    bldr.set(bldr.statement_append(block::IF((my_id < lb_temp), 
					     (new_lb = new_lb + nprocs))));

    tf->parent()->insert_before(bldr.make_tree_node_list(tf), tf->list_e());
    return (operand(new_lb_var));
}

//
// Cyclic lower bound (owner-computes)
//
//     new_step = orig_step * NPROCS
//
static operand create_cyclic_step(tree_for *tf, int dim)
{
    proc_symtab *proc_tab = tf->proc()->block()->proc_syms();
    var_sym *nprocs_var = proc_tab->lookup_var(nprocs_name(dim));
    assert(nprocs_var);

    block nprocs(nprocs_var);
    block orig_step(tf->step_op());

    block bldr(orig_step * nprocs);
    return (operand(bldr.make_instruction()));
}

//
//  ["guard": expression tree]
//
static void process_guard_annote(tree_for *tf, annote *an)
{
    if (debug['g'-'\0']) {
	printf("    Putting in guard expression for loop with index %s\n", 
	       tf->index()->name());
    }

    assert(an->immeds()->count() > 0);

    // make up marker for current position
    tree_node_list *tnl = tf->parent();
    in_rrr *inst = new in_rrr(io_mrk);
    tree_instr *mrk = new tree_instr((instruction *) inst);
    tnl->insert_after(mrk, tf->list_e());
    tnl->remove(tf->list_e());

    // build up guard expressions
    block bldr((tree_node *) tf, FALSE);
    while (an->immeds()->count() > 0) {
    
        // build up expression for guard
	immed im = an->immeds()->pop();
	instruction * g = im.instr();
	block guardexp(g);
	bldr.set(block::IF(guardexp, bldr));
    }

    // insert before marker, remove marker
    tnl->insert_before(bldr.make_tree_node(tnl), mrk->list_e());
    tnl->remove(mrk->list_e());
    delete mrk;
}

//
//  ["block_sizes: expressions trees]
//
static void process_block_sizes(tree_for *tf, annote *an)
{
  while (!an->immeds()->is_empty()) {
    immed im = an->immeds()->pop();
    tree_instr *bk_instr = new tree_instr(im.instr());

    tf->parent()->insert_before(bk_instr, tf->list_e()); 
  }
}


//
//   ["loop_bounds": expression trees for lower & upper bounds]
//
static void process_decomp_annote(tree_for *tf, annote *an)
{	    
    if (debug['g'-'\0']) {
	printf("    Converting loop with index %s for SPMD\n", 
	       tf->index()->name());
    }

    // get computation decomposition
    immed lb = an->immeds()->pop();
    immed ub = an->immeds()->pop();

    // modify loop header
    kill_op(tf->lb_op());
    kill_op(tf->ub_op());
    operand new_lb = operand(lb.instr());
    operand new_ub = operand(ub.instr());

    tf->set_lb_op(cast_op(new_lb, tf->index()->type()->unqual()));
    tf->set_ub_op(cast_op(new_ub, tf->index()->type()->unqual()));
}


//
//   ["doall"]
//
static void process_doall_annote(tree_for *tf, TaskProc *task)
{
    // k_pipeline requires tf to have a k_doall annotation to generate
    // working code.  k_speculate requires tf to have a k_pipeline
    // annotation to generate working code.
    if (tf->annotes()->peek_annote(k_pipeline)) {  
	process_pipeline_annote(tf, task);
	if (tf->annotes()->peek_annote(k_speculate)) {
	    process_speculate_annote(tf, task);
	}
    } else {
	if (debug['g'-'\0']) {
	    printf("    Parallelizing a doall loop with index %s\n",
		   tf->index()->name());
	}
	task->schedule_loop(tf);
    }
}


//
// Add counter sets: counter_set(0..pid-1, 0..num-1, val))
//                   call suif_counter_set_range(pid, num, val)
static void init_counters(tree_for *tf, TaskProc *task, var_sym *nproc)
{
    int num = task->num_counters();
    if (num <= 0) return;

    assert(counter_set_range_proc);

    block call_bldr(block::CALL
		    (block(counter_set_range_proc), 
		     block(block(nproc)), 
		     block(num), 
		     block(tf->lb_op())
		     ));
    assert(task->my_id_var(0));
    block id(task->my_id_var(0));
    block if_bldr(block::IF(block(id==0), (call_bldr)));
    tf->parent()->insert_before(if_bldr.make_tree_node(tf), tf->list_e());
}


//
//   ["pipeline": num_counters]
//
static void process_pipeline_annote(tree_for *tf, TaskProc *task)
{
    if (debug['g'-'\0']) {
	printf("    Pipelining a loop with index %s\n",
	       tf->index()->name());
    }
    task->schedule_pipelined_loop(tf);

    task->pipelined_for = tf;
    init_counters(tf, task, task->nprocs_var());
    /*    if (task->num_counters()) {
      // Add a private counter:
      // proc()
      // {
      //   int priv_counter;
      //   ...
      //   priv_counter = 0;
      //   for (;;) {   // doall
      //     priv_counter = priv_counter + 1;
      //     ...
      //   }
      // }
      base_symtab *proc_tab = task->body_block()->symtab();
      task->set_priv_counter(proc_tab->new_var(type_signed, "priv_counter"));

      block pc(task->priv_counter());
      tf->parent()->insert_before(block(pc = 0).make_tree_node(tf), tf->list_e());
      tf->body()->push(block(pc = pc + 1).make_tree_node(tf));
    }*/
}


//
//   ["speculate": ]
//
extern void process_speculate_annote(tree_for *tf, TaskProc *task)
{
    var_sym *nproc = task->nprocs_var();
    assert(nproc);

    //    static int speculate_counter = 0;
    
    if (debug['g'-'\0']) {
	printf("    Speculating a loop with index %s\n",
	       tf->index()->name());
    }

    /*
      if (loop test failed) return;  // Only needed when tf is not infinite FOR
      proc_id = my_id;
      setjmp_val = restart();  // record all the registers
      if (setjmp_val != 0) { // exception
        _my_id = val;
      } else {
        speculate_begin();
      }
      for () {
      
        goto L_commit;   // originally "continue;"

        "speculate_commit();" removed.  break;    // originally "break;"

      L_commit:
        speculate_commit();
      }
      my_id = proc_id
      speculate_terminate();
    */

    /* Add
      if (loop test failed) return;  // Only needed when tf is not infinite FOR
       proc_id = my_id;
      setjmp_val = restart();  // record all the registers
      if (setjmp_val > 0) { // exception
        _my_id = val;
      } else if (setjmp_val == 0) {
        speculate_begin();
      } else {
        goto L_termin;
      }
      */
    task->set_proc_id();
    task->set_sync_id();
    task->set_setjmp_val();

    // ADD "if (loop test failed) return;"
    tree_node *tnode, *tn;
    boolean infinite_for = FALSE;
    operand ub = tf->ub_op();
    if (ub.is_instr()) {
	instruction *in = ub.instr();
	if (in->opcode() == io_ldc) {
	    in_ldc *inl = (in_ldc*) in;
	    immed val = inl->value();
	    if (val.is_unsigned_int()) {
		unsigned i = val.unsigned_int();
		if (i == 0x7fffffff) {
		    infinite_for = TRUE;
		}
	    }
	}
    }

    if (!infinite_for) {
	operand test = for_test_done( tf, tf->lb_op().clone() );
	tree_node_list *then_part = new tree_node_list;
	then_part->push(new tree_instr(new in_rrr(io_ret)));
	tree_node_list *else_part = new tree_node_list;

	tree_if *the_if = if_node(tf->scope(), test, then_part, else_part);
	tf->parent()->insert_before(the_if, tf->list_e());
    }
 
    // ADD "proc_id = my_id"
    block procid(task->proc_id());
    block myid(task->my_id_var(0));
    block assign_bldr(procid = myid);
    tnode = assign_bldr.make_tree_node();
    tf->parent()->insert_before(tnode, tf->list_e());

    // ADD "if (pid) sync_id = pid-1; else sync_id = nproc-1;
    block sid(task->sync_id());
    block np(nproc);

    assign_bldr.set(sid = block(procid - block(1)));
    block assign_bldr2(sid = block(np - block(1)));
    block if_bldr(block::IF((procid), (assign_bldr), (assign_bldr2))); 
    tf->parent()->insert_before(if_bldr.make_tree_node(), tf->list_e());

    // ADD L_termin
    label_sym *ls = block::get_proc()->proc_syms()->new_label("L_termin");
    in_lab *inl = new in_lab(ls);
    tree_node *tn_lab = new tree_instr(inl);
    tf->parent()->insert_after(tn_lab, tf->list_e());

    // ADD "setjmp_val = restart()"
    block val(task->setjmp_val());
    assert(setjmp_proc);
    //setjmp() doesn't work!
    //    block call_bldr(block::CALL(block(setjmp_proc), block(jmpbuf)));
    block call_bldr(block::CALL(block(setjmp_proc), procid));
    assign_bldr.set(val = call_bldr);
    tnode = assign_bldr.make_tree_node();
    tf->parent()->insert_before(tnode, tf->list_e());

    // ADD "if (== 0) suif_speculate_begin(); else goto L_termin"
    assert(speculate_begin_proc);
    call_bldr.set(block::CALL(block(speculate_begin_proc)));
    tree_node *the_call = call_bldr.make_tree_node();
    assign_bldr.set(val = block(-np));
    
    in_bj *inb = new in_bj(io_jmp, ls);
    tree_node *tn_goto = new tree_instr(inb);
    block child_if_bldr(block::IF(block(val == 0), block(block(the_call, FALSE), assign_bldr), block(tn_goto, FALSE /*NO duplicate*/)));

    // ADD "if (>0) _my_id = val; else child_if_bldr"
    assign_bldr.set(myid = val);
    if_bldr.set(block::IF(block(val > 0), (assign_bldr), (child_if_bldr) ));
    tnode = if_bldr.make_tree_node();
    tf->parent()->insert_before(tnode, tf->list_e());    
    
    // Annotate this call with a unique number.
    //
    assert(the_call->kind() == TREE_INSTR);
    instruction *i = ((tree_instr *)the_call)->instr();
    assert(i->opcode() == io_cal);
    annote *an = new annote(k_speculate_id);
    // Get speculate_begin_id from loop_unique_num
    annote *loopid;
    int speculate_begin_id;
  if ((loopid = tf->annotes()->peek_annote(k_loop_unique_num))) {
    speculate_begin_id = ( *(loopid->immeds()) )[0].integer();
    an->set_immeds(new immed_list(immed(speculate_begin_id), immed(0)), NULL);
    i->annotes()->append(an);
    an = new annote(k_speculate_id);
    an->set_immeds(new immed_list(immed(speculate_begin_id), immed(0)), NULL);
    tf->annotes()->append(an);

    // Add "my_id = proc_id" after the loop
    assign_bldr.set(myid = procid);
    tf->parent()->insert_after(assign_bldr.make_tree_node(), tn_lab->list_e());

    // Add suif_speculate_terminate()
    assert(speculate_terminate_proc);
    call_bldr.set(block::CALL(block(speculate_terminate_proc)));
    the_call = call_bldr.make_tree_node();
    tf->parent()->insert_after(the_call, tf->list_e());
    // Annotate this call with a unique number.
    //
    assert(the_call->kind() == TREE_INSTR);
    i = ((tree_instr *)the_call)->instr();
    assert(i->opcode() == io_cal);
    an = new annote(k_speculate_id);
    an->set_immeds(new immed_list(immed(speculate_begin_id), immed(2)), NULL);
    i->annotes()->append(an);

    if (sim_assign_flag) {
      // Add "thread_map[proc_id] = i" at the beginning of tf
      assert(tf->index());
      block i_bldr(tf->index());
      /*      assert(task->nprocs_var());
      block mod_bldr( block::mod( i_bldr, block(task->nprocs_var())) );*/
      block array_bldr(block::ARRAY( block(thread_map), procid ));
      assign_bldr.set( array_bldr = i_bldr );
      tn = assign_bldr.make_tree_node(tf);
      tf->body()->push(tn);

      /* COMMENT OUT THE FOLLOWING CODE BECAUSE porky -guard-for SHOULD
	 BE RUN BEFORE pgen ALWAYS
      // Add "if (loop_test failed) thread_map[proc_id] = i, commit()"
      // right after tf
      boolean infinite_for = FALSE;
      operand ub = tf->ub_op();
      if (ub.is_instr()) {
	  instruction *in = ub.instr();
	  if (in->opcode() == io_ldc) {
	      in_ldc *inl = (in_ldc*) in;
	      immed val = inl->value();
	      if (val.is_unsigned_int()) {
		  unsigned i = val.unsigned_int();
		  if (i == 0x7fffffff) {
		      infinite_for = TRUE;
		  }
	      }
	  }
      }

      if (!infinite_for) {
	  block test(for_test_done( tf, operand(tf->index()) ));
      
	  if_bldr.set(block::IF(test, block(tn, TRUE) )); // TRUE means duplicate
      
	  tnode = if_bldr.make_tree_node(tf);
	  tf->parent()->insert_after(tnode, tf->list_e());    
      }
      */
    }
    
    // Add "commit: suif_speculate_commit()" at the end of tf
    assert(speculate_commit_proc);
    call_bldr.set(block::CALL(block(speculate_commit_proc)));
    tn = call_bldr.make_tree_node(tf);
    tf->body()->append(tn);
    // Annotate this call with a unique number.
    //
    assert(tn->kind() == TREE_INSTR);
    i = ((tree_instr *)tn)->instr();
    assert(i->opcode() == io_cal);
    an = new annote(k_speculate_id);
    an->set_immeds(new immed_list(immed(speculate_begin_id), immed(1)), NULL);
    i->annotes()->append(an);

    // prepend "in_bj brklab" with "suif_speculate_commit()"
    //    prepend_bjlab(tf->body(), tn, tf->brklab());
    /*    
    // Annotate this call with a unique number.
    //
    assert(tn->kind() == TREE_INSTR);
    i = ((tree_instr *)the_call)->instr();
    assert(i->opcode() == io_cal);
    an = new annote(k_speculate_id);
    an->set_immeds(new immed_list(immed(speculate_begin_id), immed(1)), NULL);
    i->annotes()->append(an);
    */

    // base_symtab *curr_scope = task->body_block()->symtab();
    ls = block::get_proc()->proc_syms()->new_label("L_commit");
    inl = new in_lab(ls);
    tn_lab = new tree_instr(inl);
    tn->parent()->insert_before(tn_lab, tn->list_e());

    // Change "in_bj contlab" to "in_bj L_commit"
    replace_label(tf->body(), tf->contlab(), ls);
  }
}


void prepend_bjlab(tree_node_list *tnl, tree_node *tn, label_sym *lab)
{
    tree_node_list_iter tnli(tnl);
    while (!tnli.is_empty()) {
      tree_node *treen = tnli.step();

      switch (treen->kind()) {
	// inner loops: skip
        case TREE_FOR:
	case TREE_LOOP:
	  break;

        case TREE_BLOCK:
	  prepend_bjlab(((tree_block *)treen)->body(), tn, lab);
	  break;

        case TREE_IF:
	  prepend_bjlab(((tree_if*)treen)->header(), tn, lab);
	  prepend_bjlab(((tree_if*)treen)->then_part(), tn, lab);
	  prepend_bjlab(((tree_if*)treen)->else_part(), tn, lab);
	  break;
	  
        case TREE_INSTR:
	  instruction *in = ((tree_instr*)treen)->instr();
	  if (in->format() == inf_bj) {
	    in_bj *inb = (in_bj*)in;
	    if (inb->target() == lab) {
		instruction *i = ((tree_instr*)tn)->instr();
		instruction *i_clone = i->clone();
		treen->parent()->insert_before(new tree_instr(i_clone), treen->list_e());
	    }
	  } else if (in->format() == inf_mbr) {
	    in_mbr *inm = (in_mbr*)in;
	    unsigned n;
	    for (n = 0; n < inm->num_labs(); n++) {
	      if (inm->label(n) == lab) {
		  // Cannot handle inf_mbr because prepending is
		  // difficult. Should run "porky -Dmbr" before pgen
		  assert_msg(1, ("Warning: Doesn't handle inf_mbr because prepending is difficult. Should run `porky -Dmbr' before pgen.\n"));
	      }
	    }
	  }
	  break;
       }
    }
}


void replace_label(tree_node_list *tnl, label_sym *oldlab, label_sym *newlab)
{
    tree_node_list_iter tnli(tnl);
    while (!tnli.is_empty()) {
      tree_node *n = tnli.step();

      switch (n->kind()) {
	// inner loops: skip
        case TREE_FOR:
	case TREE_LOOP:
	  break;

        case TREE_BLOCK:
	  replace_label(((tree_block *) n)->body(), oldlab, newlab);
	  break;

        case TREE_IF:
	  replace_label(((tree_if*)n)->header(), oldlab, newlab);
	  replace_label(((tree_if*)n)->then_part(), oldlab, newlab);
	  replace_label(((tree_if*)n)->else_part(), oldlab, newlab);
	  break;
	  
        case TREE_INSTR:
	  instruction *in = ((tree_instr*)n)->instr();
	  if (in->format() == inf_bj) {
	    in_bj *inb = (in_bj*)in;
	    if (inb->target() == oldlab) {
	      inb->set_target(newlab);
	    }
	  } else if (in->format() == inf_mbr) {
	    in_mbr *inm = (in_mbr*)in;
	    unsigned n;
	    for (n = 0; n < inm->num_labs(); n++) {
	      if (inm->label(n) == oldlab) {
		inm->set_label(n, newlab);
	      }
	    }
	  }
	  break;
       }
    }
}


//
//   ["tile_loops": depth trip_size..trip_size] (tree_fors only)
//
extern void process_tile_annote(tree_for *tf, annote *an)
{
    if (debug['g'-'\0']) {
	printf("    Tiling loop nest starting with index %s at line #%d\n",
	       tf->index()->name(), source_line_num(tf));
    }

    int depth = (*an->immeds())[0].integer();
    if (depth <= 1) return;

    int *trips = new int[depth];
    for (int i = 0; i < depth; i++) {
	trips[i] = (*an->immeds())[i+1].integer();
    }
    tile_loops(tf, depth, trips);
    delete[] trips;
}

static void tile_loops(tree_for *tf, int depth, int *trips)
{
    tree_for **for_loops = new tree_for*[depth];
    tree_for *curr_loop = tf;

    int count = 0;
    while (curr_loop && (count < depth)) {
	for_loops[count++] = curr_loop;
	curr_loop = next_inner_for(curr_loop->body());
    }

    // Pgen may have mangled the loop nest so that the nest is no longer
    // tileable (e.g. creating calls to runtime in the parent procedure)
    //
    if (count != depth) return;

    // Make sure loops are tilable:
    //
    boolean normalize_ok = TRUE, bexpr_ok = TRUE;
    int i;
    for (i = depth-1; i >= 0; i--) {
        fill_in_access(for_loops[i], NORMALIZE_CODE, &normalize_ok);
	if (!normalize_ok) break;
    }

    for (i = 0; i < depth; i++) {
	curr_loop = for_loops[i];
	bexpr_ok = bounds_ok(curr_loop);

	if (!bexpr_ok) {
	    if (lb_is_indep(for_loops, i))  promote_lb(curr_loop, tf);
	    if (ub_is_indep(for_loops, i))  promote_ub(curr_loop, tf);

	    bexpr_ok = bounds_ok(curr_loop);
	}
	if (!bexpr_ok) break;
    }

    if (!bexpr_ok || !normalize_ok) {
	warning_line(NULL, "Loops cannot be tiled due to bad bounds");
	return;
    }

    boolean *doalls = new boolean[depth];
    for (i = 0; i < depth; i++)  doalls[i] = FALSE;

    // Do we coalesce this region to make 2n-1 loops rather than 2n?
    boolean coalesce[1];
    coalesce[0] = (trips[0] == 1);

    // Set the first loop for the tiling region.  There's only one 
    // tiling region here.
    int first[2];
    first[0] = 0;
    first[1] = depth;

    loop_transform trans(depth, for_loops, doalls);
    boolean success = trans.tile_transform(trips, 1, coalesce, first);
    assert_msg(success, ("Tiling failed at loop %s", tf->index()->name()));

    tree_for **outer_loops = new tree_for*[depth];
    curr_loop = next_outer_for(tf->parent());
    count = depth-1;
    while (curr_loop && count >= 0) {
	outer_loops[count--] = curr_loop;
	curr_loop = next_outer_for(curr_loop->parent());
    }
    assert(count <= 0);
}


static tree_for *next_outer_for(tree_node_list *tnl)
{
    tree_for *outer = NULL;
    tree_node *parent = tnl->parent();

    if (parent && parent->is_for()) 
	outer = (tree_for *) parent;

    return outer;
}

static tree_for *next_inner_for(tree_node_list *tnl)
{
    tree_for *inner = NULL;

    tree_node_list_iter tnli(tnl);
    while (!tnli.is_empty()) {
        tree_node *n = tnli.step();

        switch (n->kind()) {
            case TREE_FOR:
                if (inner) return NULL;
                inner = (tree_for *) n;
                break;

            case TREE_BLOCK:
                if (inner) return NULL;
                inner = next_inner_for(((tree_block *) n)->body());
                break;

            case TREE_INSTR:
		break;

            case TREE_IF:
            case TREE_LOOP:
                return NULL;
        }
    }

    return inner;
}


//
//   ["doacross": dimension, direction, type, pid1, pid2]
//
static void process_doacross_annote(tree_for *tf, annote *an, TaskProc *task)
{
    if (debug['g'-'\0']) {
	printf("    Adding doacross synchronization around loop %s\n",
	       tf->index()->name());
    }

    if (!an->immeds()->count())  
    {
        // from skweel, assume doacross from proc 0 to N-1 with distance -1

        var_sym * nprocs_var = 
	  task->stub_block()->symtab()->lookup_var(nprocs_name(0));
	assert(nprocs_var);
        block pid1_bk(0);
        block pid2_bk(nprocs_var);
        pid2_bk.set(pid2_bk - 1);
        instruction *pid1 = pid1_bk.make_instruction();
        instruction *pid2 = pid2_bk.make_instruction();
        task->synch_doacross(tf, 0, -1, pid1, pid2);
    }
    else 
    {
        // processors for doacross computed in popt

        int dim = (*an->immeds())[0].integer();
        int dist = (*an->immeds())[1].integer();

        assert_msg(!strcmp((*an->immeds())[2].string(), "block"), 
          ("Not implemented: Doacross with non-block type in loop %s",
           tf->index()->name()));

        instruction *pid1 = (*an->immeds())[3].instr();
        instruction *pid2 = (*an->immeds())[4].instr();
        task->synch_doacross(tf, dim, dist, pid1, pid2);
    }

}


//  Generate doacross synchronization around the given loop. Given a loop:
// 
//  for i lb..ub {     
//    ...
//  }
// 
//  If dist<0 (dist is defined as Proc_send-Proc_recv) the loop runs
//    foward and the generated code is:
// 
//  private int counter = 1;
//  if (first < _my_id <= last) {
//      suif_counter_wait(_my_id-dist,id,counter);
//     counter++;
//  }
//  for i = <new bounds> {
//         ...
//  }
//  if (first <= _my_id < last)
//     suif_counter_incr(_my_id,id) 
//
static void doacross_counters(tree_for *tf, int dim, int dist, 
    int counter_num, instruction *pid1, instruction *pid2, TaskProc *task)
{
    base_symtab *curr_scope = task->body_block()->symtab();
    tree_node_list *tnl = tf->parent();

    operand *orig_lb = (operand *)tf->peek_annote(k_orig_lower_bound);
    operand *orig_ub = (operand *)tf->peek_annote(k_orig_upper_bound);
    assert(orig_lb && orig_ub);

    var_sym *id_var = task->my_id_var(dim);
    assert(id_var);

    block my_id(id_var);

    // Look for outermost loop in the parallel region to insert
    // counter initialization code
    tree_node_list * tnl2 = tnl;
    tree_node * tf2 = tf;
    while (tnl2 && tnl2->parent()->is_for()) {
        tf2 = tnl2->parent();
        tnl2 = tf2->parent();
    }

    // Generate assignments to counter
    char buf[100];
    sprintf(buf, "_counter%d", counter_num);
    var_sym *counter_var = curr_scope->lookup_var(buf);
    if (!counter_var) counter_var = curr_scope->new_var(type_signed, buf);
    block counter(counter_var);
    block counter_init(counter = 1);
    tnl2->insert_before(counter_init.make_tree_node_list(tnl2), tf2->list_e());

    // Generate assignments to first/last proc
    sprintf(buf, "_first_proc%d", counter_num);
    var_sym *first_var = curr_scope->lookup_var(buf);
    if (!first_var) first_var = curr_scope->new_var(type_signed, buf);
    block first(first_var);

    sprintf(buf, "_last_proc%d", counter_num);
    var_sym *last_var = curr_scope->lookup_var(buf);
    if (!last_var) last_var = curr_scope->new_var(type_signed, buf);
    block last(last_var);

    block p1(pid1);
    block p2(pid2);
    block bldr(first = p1);
    bldr.set(bldr.statement_append(last = p2));
    tnl2->insert_before(bldr.make_tree_node_list(tnl2), tf2->list_e());

    // Generate call to suif_counter_wait(proc, counter_num, val),
    //   insert preceding loop
    assert(counter_wait_proc);
    if (dist < 0) bldr.set(my_id - -dist);
    else bldr.set(my_id + dist);

    // if multidimensional distribution, calculate physical id of sender
    if (dim) {  
        sprintf(buf, "_send_proc%d", counter_num);
        var_sym *send_var = curr_scope->lookup_var(buf);
        if (!send_var) send_var = curr_scope->new_var(type_signed, buf);
        convert_global_myid(bldr, dim, task);
        bldr.set(block(send_var) = bldr);
        tnl2->insert_before(bldr.make_tree_node(tnl2), tf2->list_e());
        bldr.set(block(send_var));
    }

    block var_bldr;
    block counter_bldr;
    
    if (fortran_form_flag) 
      {
	var_sym *temp_var = NULL;
        assign_to_temp_before(tf, immed(counter_num), type_signed, &temp_var);
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());

	var_sym *temp_var2 = tf->scope()->new_unique_var(type_signed,
							 "_temp");
	block var_bldr(temp_var2);
	bldr.set(var_bldr = bldr);
	tnl->insert_before(bldr.make_tree_node_list(tnl), tf->list_e());
	bldr.set(var_bldr.addr());
	counter_bldr.set(counter.addr());
      }
    else
      {
	var_bldr.set(block(counter_num));
	counter_bldr.set(counter);
      }

    bldr.set(block::CALL(block(counter_wait_proc), bldr, var_bldr, 
			 counter_bldr));

    bldr.set(bldr.statement_append(block(counter = counter + 1)));
    if (dist < 0) {
        bldr.set(block::IF(block(my_id <= last), bldr));
        bldr.set(block::IF(block(first < my_id), bldr));
    }
    else {
        bldr.set(block::IF(block(my_id < first), bldr));
        bldr.set(block::IF(block(last <= my_id), bldr));
    }
    tnl->insert_before(bldr.make_tree_node_list(tnl), tf->list_e());

    // Generate call to suif_counter_incr(proc, counter_num),
    //   insert following loop
    if (dim) {  // get physical id
        var_sym *id = task->my_id_var(0);
	assert(id);
        bldr.set(block(id));
    }
    else  bldr.set(my_id);
    assert(counter_incr_proc);

    tree_node *pos = tf;
    
    if (fortran_form_flag) 
      {
	var_sym *temp_var = NULL;
	pos = assign_to_temp_after(tf, immed(counter_num), type_signed,
				   &temp_var);
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());
	bldr.set(bldr.addr());
      }
    else
      {
	var_bldr.set(block(counter_num));
      }

    bldr.set(block::CALL(block(counter_incr_proc), bldr, var_bldr));
    if (dist < 0) {
        bldr.set(block::IF(block(my_id < last), bldr));
        bldr.set(block::IF(block(first <= my_id), bldr));
    }
    else {
        bldr.set(block::IF(block(my_id <= first), bldr));
        bldr.set(block::IF(block(last < my_id), bldr));
    }
    tnl->insert_after(bldr.make_tree_node(tnl), pos->list_e());
}

// convert_global_myid()  Builds physical proc id from local proc ids
//                        Pid = (_my_id2 * _nprocs1) + _my_id1

static void convert_global_myid(block & bldr, int dim, TaskProc *task)
{
    if (!dim) 
       return;

    else if ((dim > 2) || (dim < 0)) {
       printf("Processor dim %d not yet supported\n", dim);
       return;
    }

    var_sym * np1 = task->body_block()->scope()->lookup_var(nprocs_name(1));
    assert(np1);

    if (dim == 1) {
        var_sym * id2 = task->my_id_var(2);
	assert(id2);

	block id2_bldr(id2);
	block np1_bldr(np1);
        block term(id2_bldr * np1_bldr);
        bldr.set(term + bldr);
    }

    else if (dim == 2) {
        var_sym * id1 = task->my_id_var(1);
	assert(id1);

        bldr.set(bldr * block(np1));
        bldr.set(bldr + block(id1));
    }
}

//
//   ["synch_counter": send_proc_offset, vproc_dim, vproc_off, 
//                     vproc_varsym, upper_var, upper_idx ]
//
//  Generate counter synchronization for sequential loop enclosing
//  parallel loop. Given a for loop k and forall loop j as follows:
// 
//  for k = lb..ub {     
//    forall j {
//      ... body ...
//    } 
//  }
// 
//  The routine produces:
// 
//  private int counter = 1;
//  private int send_id = 0;
//  for k = lb..ub {     
//    if (k != lb) && (myid != send_id) 
//      suif_counter_wait(send_id, counter_id, counter)
//    send_id = (k+send_proc_offset+vproc_off) % nproc_(vproc_dim)
//    counter = counter + 1;
//    forall j {
//      ... body ...
//      if (j == last_nonlocal_iter) && (myid == sendid)
//        suif_counter_set(send_id, counter_id, counter)
//    }
//  }
//
//  Where:  last_nonlocal_iter = upper_var + upper_idx
//          j = vproc_varsym
//          myid = my_id_name(vproc_dim, vproc_off)
 
void TaskProc::synch_counter(tree_for *tf, annote *an)
{
    if (debug['g'-'\0']) {
	printf("    Adding lock synchronization in loop %s\n",
	       tf->index()->name());
    }

    int counter_num = num_counters_++;

    //-------------------------
    // read in arguments from annotation

    int send_proc_offset = (*an->immeds())[0].integer();
    int vproc_dim = (*an->immeds())[1].integer();
    int vproc_off = (*an->immeds())[2].integer();
    var_sym * vproc_varsym = (var_sym *) (*an->immeds())[3].symbol();
    var_sym * upper_var = (var_sym *) (*an->immeds())[4].symbol();
    int upper_idx = (*an->immeds())[5].integer();

    tree_node_list *tnl = tf->parent();
    base_symtab *curr_scope = body_block()->symtab();

    //-------------------------
    // Generate initialization & assignments to counter
    // counter = counter + 1

    char buf[100];
    sprintf(buf, "_counter%d", counter_num);
    var_sym *counter_var = curr_scope->lookup_var(buf);
    if (!counter_var) counter_var = curr_scope->new_var(type_signed, buf);

    var_sym *id_var = my_id_var(vproc_dim);
    assert(id_var);
    block my_id(id_var);

    block counter(counter_var);
    block bldr(counter = 1);
    tnl->insert_before(bldr.make_tree_node(tnl), tf->list_e());

    bldr.set(counter = block(counter + 1));
    tf->body()->push(bldr.make_tree_node_list(tf));

    //-------------------------
    // generate send_id, id of sending processor

    // send_id = (k+send_proc_offset+vproc_off) % nproc_(vproc_dim)

    sprintf(buf, "_send_id%d", counter_num);
    var_sym *send_var = curr_scope->lookup_var(buf);
    if (!send_var) send_var = curr_scope->new_var(type_signed, buf);
    block send_id(send_var);
    bldr.set(send_id = 0);
    tnl->insert_before(bldr.make_tree_node(tnl), tf->list_e());

    var_sym *nprocs_var = curr_scope->lookup_var(nprocs_name(vproc_dim));
    assert(nprocs_var);
    block nprocs(nprocs_var);
    var_sym *idx = tf->index();
    bldr.set(block(send_proc_offset) + block(vproc_off));
    bldr.set(block(idx) + bldr);
    bldr.set(bldr % nprocs);
    bldr.set(send_id = bldr);
    tf->body()->push(bldr.make_tree_node_list(tf));

    //-------------------------
    // generate guarded call to suif_counter_wait()

    // if (k != lb) && (myid != send_id) 
    //   suif_counter_wait(send_id, counter_id, counter)

    // operand *orig_lb = (operand *)tf->peek_annote(k_orig_lower_bound);
    // assert(orig_lb);

    block var_bldr;
    block counter_bldr;

    if (fortran_form_flag) 
      {
	var_sym *temp_var = NULL;
	assign_to_temp_before(tf->body()->head()->contents, 
			      immed(counter_num), type_signed, &temp_var);
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());
	send_id.set(send_id.addr());
	counter_bldr.set(counter.addr());
      }
    else
      {
	var_bldr.set(block(counter_num));
	counter_bldr.set(counter);
      }

    assert(counter_wait_proc);
    bldr.set(block::CALL(block(counter_wait_proc), send_id, var_bldr,
			 counter_bldr));

    bldr.set(block::IF(block(my_id != send_id), bldr));
    bldr.set(block::IF(block(block(idx) != block(tf->lb_op())), bldr));
    tf->body()->push(bldr.make_tree_node_list(tf));

    //-------------------------
    // generated guarded call to suif_counter_set()

    // if (j == last_nonlocal_iter) && (myid == sendid)
    //   suif_counter_set(send_id, counter_id, counter)

    // find inner loop
    tree_node_list_iter it(tf->body());
    tree_node *tn2;
    while (!it.is_empty()) {
       tn2 = it.step();
       if (tn2->is_for())
           break;
    }
    assert(!it.is_empty());
    tree_for * tf2 = (tree_for *) tn2;
    if ((tf2->index() != vproc_varsym) &&
            strcmp(tf2->index()->name(), vproc_varsym->name())) {
        assert(0);
    }
 
    if (fortran_form_flag) 
      {
	var_sym *temp_var = NULL;
	assign_to_temp_after(tf2->body()->tail()->contents, 
			     immed(counter_num), type_signed, &temp_var);
	block temp_var_bldr(temp_var);
	var_bldr.set(temp_var_bldr.addr());
      }
    else
      {
	var_bldr.set(block(counter_num));
      }

    assert(counter_set_proc);
    bldr.set(block::CALL(block(counter_set_proc), 
        send_id, var_bldr, counter_bldr));

    bldr.set(block::IF(block(my_id == send_id), bldr));

    block upper_var_bldr(upper_var);
    block upper_idx_bldr(upper_idx);
    block guard(upper_var_bldr + upper_idx_bldr);

    guard.set(block(tf2->index()) == guard);
    bldr.set(block::IF(guard, bldr));
    tf2->body()->append(bldr.make_tree_node_list(tf2));
}


static void get_fortran_reduction_names(char *reduce_proc, char *init_proc,
                                reduction_kinds kind, type_node *elem_type)
{
    char init_temp[256];
    char reduce_temp[256];

    switch (kind) {
       case REDUCTION_SUM: {
	   sprintf(init_temp, "rcis"); /* "init_sum" */
	   sprintf(reduce_temp, "rces"); /* "reduce_sum" */
	   break;
       }
       case REDUCTION_PRODUCT: {
	   sprintf(init_temp, "rcip"); /* "init_product" */
	   sprintf(reduce_temp, "rcep"); /* "reduce_product" */
	   break;
       }

       case REDUCTION_MAX: {
	   sprintf(init_temp, "rcix"); /* "init_max" */
	   sprintf(reduce_temp, "rcex"); /* "reduce_max" */
	   break;
       }

       case REDUCTION_MIN: {
	   sprintf(init_temp, "rcin"); /* "init_min" */
	   sprintf(reduce_temp, "rcen"); /* "reduce_min" */
	   break;
       }

       default: {
           assert_msg(FALSE, ("Not implemented: reduction kind %d", kind));
           break;
       }
    }

    sprintf(init_proc, "%s%s_", init_temp, 
	    runtime_fortran_type_name(elem_type));
    sprintf(reduce_proc, "%s%s_", reduce_temp, 
	    runtime_fortran_type_name(elem_type));
}

static void get_reduction_names(char *reduce_proc, char *init_proc,
				  reduction_kinds kind, type_node *elem_type)
{
    char init_temp[256];
    char reduce_temp[256];

    switch (kind) {
       case REDUCTION_SUM: {
	   sprintf(init_proc, "init_sum");
	   sprintf(reduce_proc, "reduce_sum");
	   break;
       }
       case REDUCTION_PRODUCT: {
	   sprintf(init_proc, "init_product");
	   sprintf(reduce_proc, "reduce_product");
	   break;
       }

       case REDUCTION_MAX: {
	   sprintf(init_proc, "init_max");
	   sprintf(reduce_proc, "reduce_max");
	   break;
       }

       case REDUCTION_MIN: {
	   sprintf(init_proc, "init_min");
	   sprintf(reduce_proc, "reduce_min");
	   break;
       }

       default: {
           assert_msg(FALSE, ("Not implemented: reduction kind %d", kind));
           break;
       }
    }

    strcpy(reduce_temp, reduce_proc);
    strcpy(init_temp, init_proc);

    sprintf(init_proc, "%s_%s", init_temp, runtime_type_name(elem_type));
    sprintf(reduce_proc, "%s_%s", reduce_temp, runtime_type_name(elem_type));
}


static void get_fortran_reduction_rotate_names(char *reduce_proc, 
                  char *init_proc, reduction_kinds kind, type_node *elem_type)
{
    char init_temp[256];
    char reduce_temp[256];

    switch (kind) {
       case REDUCTION_SUM: {
	   sprintf(init_temp, "rcis"); /* "init_sum" */
	   sprintf(reduce_temp, "rcrs"); /* "reduce_rotate_sum" */
	   break;
       }
       case REDUCTION_PRODUCT: {
	   sprintf(init_temp, "rcip"); /* "init_product" */
	   sprintf(reduce_temp, "rcrp"); /* "reduce_rotate_product" */
	   break;
       }

       case REDUCTION_MAX: {
	   sprintf(init_temp, "rcix"); /* "init_max" */
	   sprintf(reduce_temp, "rcrx"); /* "reduce_rotate_max" */
	   break;
       }

       case REDUCTION_MIN: {
	   sprintf(init_temp, "rcin"); /* "init_min" */
	   sprintf(reduce_temp, "rcrn"); /* "reduce_rotate_min" */
	   break;
       }

       default: {
           assert_msg(FALSE, ("Not implemented: reduction kind %d", kind));
           break;
       }
    }

    sprintf(init_proc, "%s%s_", init_temp, 
	    runtime_fortran_type_name(elem_type));
    sprintf(reduce_proc, "%s%s_", reduce_temp, 
	    runtime_fortran_type_name(elem_type));
}

static void get_reduction_rotate_names(char *reduce_proc, char *init_proc,
				   reduction_kinds kind, type_node *elem_type)
{
    char init_temp[256];
    char reduce_temp[256];

    switch (kind) {
       case REDUCTION_SUM: {
	   sprintf(init_proc, "init_sum");
	   sprintf(reduce_proc, "reduce_rotate_sum");
	   break;
       }
       case REDUCTION_PRODUCT: {
	   sprintf(init_proc, "init_product");
	   sprintf(reduce_proc, "reduce_rotate_product");
	   break;
       }

       case REDUCTION_MAX: {
	   sprintf(init_proc, "init_max");
	   sprintf(reduce_proc, "reduce_rotate_max");
	   break;
       }

       case REDUCTION_MIN: {
	   sprintf(init_proc, "init_min");
	   sprintf(reduce_proc, "reduce_rotate_min");
	   break;
       }

       default: {
           assert_msg(FALSE, ("Not implemented: reduction kind %d", kind));
           break;
       }
    }

    strcpy(reduce_temp, reduce_proc);
    strcpy(init_temp, init_proc);

    sprintf(init_proc, "%s_%s", init_temp, runtime_type_name(elem_type));
    sprintf(reduce_proc, "%s_%s", reduce_temp, runtime_type_name(elem_type));
}

static void get_fortran_reduction_gen_names(char *init_proc, 
					    type_node *elem_type)
{
    char init_temp[256];

    sprintf(init_temp, "rcig"); /* "init_gen" */
    sprintf(init_proc, "%s%s_",init_temp,runtime_fortran_type_name(elem_type));
}

static void get_reduction_gen_names(char *init_proc, type_node *elem_type)
{
    char init_temp[256];

    sprintf(init_proc, "init_gen");
    strcpy(init_temp, init_proc);
    sprintf(init_proc, "%s_%s", init_temp, runtime_type_name(elem_type));
}


static operand calc_num_elems(type_node *curr_type, tree_node *tn)
{
    operand result;
    if (constant_bounds(curr_type)) {
	type_node *elem_type = get_element_type(curr_type);
	assert(elem_type->size() > 0);

	block num_elems_bldr(curr_type->size() / elem_type->size());
	result = operand(num_elems_bldr.make_instruction(tn));
    }    

    else {
        result = generate_num_elems(curr_type, tn);
    }

    return cast_op(result, type_ptr_diff);
}


static operand generate_num_elems(type_node *curr_type, tree_node *tn)
{
    if (!curr_type->unqual()->is_array()) return operand();

    array_type *at = (array_type *) curr_type->unqual();
    assert(!at->are_bounds_unknown());

    block lower_bldr;
    block upper_bldr;

    // Lower bound
    //
    if (at->lower_bound().is_constant()) 
	lower_bldr.set(block(at->lower_bound().constant()));
    else 
	lower_bldr.set(block(at->lower_bound().variable()));

    // Upper bound
    //
    if (at->upper_bound().is_constant()) 
	upper_bldr.set(block(at->upper_bound().constant()));
    else 
	upper_bldr.set(block(at->upper_bound().variable()));

    operand remaining_dims = generate_num_elems(at->elem_type(), tn);
    block num_elems_bldr;

    if (remaining_dims.is_null()) {
	num_elems_bldr.set(upper_bldr - lower_bldr + 1);
    }
    else {
	num_elems_bldr.set(block(remaining_dims) * 
			   (upper_bldr - lower_bldr + 1));
    }

    return operand(num_elems_bldr.make_instruction(tn));
}



/* Private function specifications */

// Store lower bound into a variable. 
//
static var_sym *promote_lb(tree_for *tf, tree_node *pos)
{
    base_symtab *scope = pos->scope();
    assert(tf->scope()->is_ancestor(scope));

    operand cur_lb = tf->lb_op();
    cur_lb.remove();

    var_sym *vs = scope->new_unique_var(tf->index()->type(), "_lb");

    in_rrr *copy = new in_rrr(io_cpy, cur_lb.type(), operand(vs), cur_lb);
    pos->parent()->insert_before(new tree_instr(copy), pos->list_e());

    tf->set_lb_op(operand(vs));
    return (vs);
}

// Store upper bound into a variable.
//
static var_sym *promote_ub(tree_for *tf, tree_node *pos)
{
    base_symtab *scope = pos->scope();
    assert(tf->scope()->is_ancestor(scope));

    operand cur_ub = tf->ub_op();
    cur_ub.remove();

    var_sym *vs = scope->new_unique_var(tf->index()->type(), "_ub");

    in_rrr *copy = new in_rrr(io_cpy, cur_ub.type(), operand(vs), cur_ub);
    pos->parent()->insert_before(new tree_instr(copy), pos->list_e());
    tf->set_ub_op(operand(vs));

    return (vs);
}

// Store step into a variable.
//
/*
static var_sym *promote_step(tree_for *tf, tree_node *pos)
{
    base_symtab *scope = pos->scope();
    assert(tf->scope()->is_ancestor(scope));

    operand cur_st = tf->step_op();
    cur_st.remove();

    var_sym *vs = scope->new_unique_var(tf->index()->type(),"_step");

    in_rrr *copy = new in_rrr(io_cpy, cur_st.type(), operand(vs), cur_st);
    pos->parent()->insert_before(new tree_instr(copy), pos->list_e());
    tf->set_step_op(operand(vs));

    return (vs);
}
*/


static boolean lb_is_indep(tree_for **for_loops, int curr_loop_num)
{
  operand lb = for_loops[curr_loop_num]->lb_op();

  for (int i = 0;  i < curr_loop_num; i++)
    {
      if (operand_may_reference_var(lb, for_loops[i]->index())) 
	return FALSE;
    }

  return TRUE;
}


static boolean ub_is_indep(tree_for **for_loops, int curr_loop_num)
{
  operand ub = for_loops[curr_loop_num]->ub_op();

  for (int i = 0;  i < curr_loop_num; i++)
    {
      if (operand_may_reference_var(ub, for_loops[i]->index())) 
	return FALSE;
    }

  return TRUE;
}


static void kill_replacement_annotes_on_object(suif_object *the_object)
{
    annote_list_iter ali(the_object->annotes());
    while (!ali.is_empty())
      {
        annote *an = ali.step();
	if (an->name() == k_replacement)
	  {
	    annote_list_e *ale = the_object->annotes()->remove(ali.cur_elem());
	    delete ale->contents;
	    delete ale;
	  }
      }
}


static annote *has_total_size_annote(tree_for *tf, var_sym *the_var)
{
    annote_list_iter ali(tf->annotes());
    while (!ali.is_empty())
      {
	annote *an = ali.step();
	if (an->name() == k_array_total_size)
	  {
	    sym_node *sym = (*an->immeds())[0].symbol();
	    if (sym == the_var) return an;
	  }
      }
    return NULL;
}


tree_instr *assign_to_temp_after(tree_node *tn, immed im, type_node *im_type,
				 var_sym **temp_var)
{
    *temp_var = tn->scope()->new_unique_var(im_type, "_temp");
    in_ldc *ldc = new in_ldc(im_type, operand(*temp_var), im);
    tree_instr *ti = new tree_instr(ldc);
    tn->parent()->insert_after(ti, tn->list_e());        

    return ti;
}

tree_instr *assign_to_temp_before(tree_node *tn, immed im, type_node *im_type,
				  var_sym **temp_var)
{
    *temp_var = tn->scope()->new_unique_var(im_type, "_temp");
    in_ldc *ldc = new in_ldc(im_type, operand(*temp_var), im);
    tree_instr *ti = new tree_instr(ldc);
    tn->parent()->insert_before(ti, tn->list_e());        

    return ti;
}


boolean is_nested(tree_for *outer, tree_for *inner)
{
    if (outer == inner) return TRUE;

    tree_node *curr_node = inner->parent()->parent();
    while (curr_node && curr_node->parent())
      {
	if (curr_node == outer) return TRUE;
	curr_node = curr_node->parent()->parent();
      }

    return FALSE;
}


