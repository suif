/* file "parent.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Transform the code in the parent procedure */

#ifndef PARENT_H
#define PARENT_H

#pragma interface

#include "pgen.h"

RCS_HEADER(parent_h, "$Id: parent.h,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")

class ParallelRegion;

class ParentProc {
protected:
    ParallelRegion *my_region_;
    proc_sym *proc_;
    tree_node_list *doall_call_site_;
    tree_block *original_region_;

public:
    ParentProc(proc_sym *ps, ParallelRegion *par_region);
    ~ParentProc();

    proc_sym *proc()                            { return proc_; }
    tree_node_list *doall_call_site()           { return doall_call_site_; }
    ParallelRegion *my_region()                 { return my_region_; }
    tree_block *original_region()               { return original_region_; }

    void create_doall_site(tree_block *region_block);
    void remove_region(tree_block *region_block);  

    void call_doall(proc_sym *task_proc);
    void pack_exposed_vars(struct_type *copy_type, proc_sym *task_proc);
    void init_counters(int num);
    void incr_counters(int num, var_sym *pid);
};

#endif




