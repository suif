/* file "main.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>
#include <string.h>

/*  Main program */

/*
   Find parallel loops and implement them, using calls to runtime library.
   Pgen currently supports the following annotations:
   
   ["begin_parallel_region"] (any tree_node)
   ["end_parallel_region"]    

       All tree nodes between (and including) the begin and end annotations
       will be placed into a separate procedure, to be run on each processor.

   ["loop_bounds": expression trees for lower & upper bounds], (tree_fors only)

       Schedule iterations of the loop across the processors.  
       The expression trees are the new lower & upper bounds
       of the loop calculated by popt.

   ["guard" list of expression trees] (tree_fors only)

       Puts an if statement around the loop so that it is only executed if
       expression trees evaluate to TRUE.  

   ["doall"] (tree_fors only)

       Short-hand for a parallel region consisting of a single tree_for that's
       a doall loop.  Note that any doalls nested within an outer doall
       will get stripped off (i.e. pgen won't generate nested parallel 
       regions).

   ["doall_region"] (tree_fors only)

       Short-hand for a parallel region consisting of a single tree_for that
       contains a nested doall loop.  The outermost doall is distributed and
       any other nested doalls nested will get stripped off (i.e. pgen won't 
       generate nested parallel regions).

   ["doacross": dimension, direction, type]

       (tree_fors only) where dimension is the processor dimension (e.g.
       0,1,2), direction is an integer offset (giving the offset of the
       processor to wait on), type is a string (either "block" or "cyclic"),
       Generates counter synchronization around the loop.

   ["synch_counter": send_proc_offset, vproc_dim, vproc_off, 
                     vproc_varsym, upper_var, upper_idx ] (tree_fors only)

       Generate counter synchronization for sequential loop enclosing
       parallel loop. 

   ["loop_cyclic": dimension offset] (tree_fors only)

       Schedule the loop using a cyclic mapping in the given processor 
       dimension.  Offset gives the array offset.

   ["tile_loops": [locality] depth trip_size..trip_size] (tree_fors only)

       Tile the given loop nest.  A trip_size must be given for each loop
       in the nest.  A trip size of 1 in the outer loop will cause 
       the tile to be coalesced (generates 2n-1 loops rather than 2n).
       If [locality] if the first immed on the list, tile the loops in both
       parallel and sequential regions.

   ["lock": lock_num] 
   ["unlock": lock_num] (any tree_node, since annotes not supported on 
                        tree_instrs put annote on corresponding instruction)

       Generates lock/unlock statements *after* the tree_node with the annote.
       lock_num is the number of the lock.

   ["global_barrier"] (any tree_node, since annotes not supported on 
                       tree_instrs put annote on corresponding instruction)

       Generates a barrier statement *after* the tree_node with the annote
       (all processors must wait at barrier).
       Unique barriers are generated for each barrier annotation within a task.

   ["reduced": type var] (tree_fors only)

       Creates a private copy of the variable for each processor and then
       does a global computation of kind given by type (e.g. sum, product).

   ["privatized": vars] (tree_fors only)
       
       Makes a private copy within the task of each variable in the list vars.
   

   Command line options
   --------------------

   Command line options are as follows:

   -sf2c  This flag must be set if sf2c was run.
   
   -check-work  Generate code that checks the amount of work in a parallel
          loop before running it

   -sim-calls   Generate calls to the simulator that tell it when a 
          parallel region is entered/exited.

    -sim-assign Generate memory assignment to the simulator that tell it
          which thread maps to a iteration.

   -l     Print out the procedure and line number of parallelized regions.
			    
   -p<n>  number of processors, p = 0 means unknown.  If p is unknown then
          the number of processors is read at runtime.

   -no-array-reductions 
          Suppress reductions over arrays

   -reduction-guards
          Generate guards before reductions

   -fortran-form  
          Use stub procedures for some of the grossness of task procedures,
          with the stub calling a cleaner function that includes all the
          original code for the task and uses call-by-reference parameters
          when possible to give additional information to later passes.
	  Also use Fortran compatible names for runtime routines.

   -sequential-stubs  
          Create stub functions out of the sequential copy of the
	  task functions

   -d<c>  debugging information.  The possible debugging types are:

      d  dump modified procedures
      f  fileset/file symbol table (this code creates a lot global types, so 
           this option prints them out to make sure they were created 
	   correctly)
      g  general, high-level progress
      m  variable mapping from proc -> task
      p  info about vars being privatized (and reduced over)
      r  warns if variables passed by reference
      s  scheduling information
      x  reserved for debugging and testing


   General Algorithm:
   -----------------

   The body of the parallel loops are extracted into a separate procedure,
   and replaced by a call to the "doall" function (from the runtime library).

   The runtime library calls the procedure on each processor, and does a
   barrier before returning.

   This code does the following:
       -- Traverse the code looking for parallel_region annotations. 
          (See pgen.cc).

       -- Extracts the parallel region into a separate procedure.
          (Calls library to move code, and then passes in the proper 
	  parameters).  See task.{h,cc}.

       -- Replaces the original code with a call to the runtime library
          See parent.{h,cc}.

       -- Schedules the iterations of the parallel loops. See sched.{h,cc}.

       -- Changes the name of the main program from "main" to "_suif_start" (C
             programs only). This is necessary because the runtime library 
	     must define the main program.  See pgen.cc.

   Notes
   -----

   Libraries:

   The "runtime" library must be linked in with programs run through this 
   code.  Also, for FORTRAN programs you must link in the "F77_doall"
   library (F77_doall replaces the F77 library for parallelized
   FORTRAN programs).
*/


#define RCS_BASE_FILE main_cc

#include "pgen.h"

RCS_BASE("$Id: main.cc,v 1.2 1999/08/25 03:26:40 brm Exp $")

#define STRING_SIZE 512

/* Public global variables */

int debug[256];
int mask[256];
int nprocs_flag = 0;
boolean inline_sync_flag = FALSE;
boolean sim_assign_flag = FALSE;
boolean check_work_flag = FALSE;
boolean print_line_flag = FALSE;
int pipeline_grain_flag = 4;
boolean fortran_form_flag = FALSE;
boolean no_array_reductions_flag = FALSE;
boolean reduction_guards_flag = FALSE;
boolean no_cache_tile_flag = FALSE;
boolean sequential_stubs_flag = FALSE;
boolean add_params_flag = FALSE;
int *feedback_info = NULL;
int num_feedback_doalls = 0;
int region_num = -1;

  // Annotations read 
const char *k_begin_parallel_region; 
const char *k_end_parallel_region; 
const char *k_data_decomp;
const char *k_loop_bounds;
const char *k_block_sizes;
const char *k_doall_region;
const char *k_doacross;
const char *k_loop_cyclic;
const char *k_tile_loops;
const char *k_tileable_loops;
const char *k_block_size;
const char *k_guard;
const char *k_global_barrier;
const char *k_sync_neighbor;
const char *k_lock;
const char *k_unlock;
const char *k_privatized;
const char *k_privatizable;
const char *k_reduction;
const char *k_reduced;
const char *k_sync_counter;
const char *k_HPF;
const char *k_data_layout;
const char *k_reduction_gen;
const char *k_reduced_gen;
const char *k_associativity;
const char *k_structure_field;
const char *k_array_total_size;
const char *k_nprocs_var; 
const char *k_nprocs1_var; 
const char *k_nprocs2_var; 
const char *k_true_type;
const char *k_doall_annote;
const char *k_speculate;
const char *k_speculate_id;
const char *k_pipeline;
const char *k_producer;
const char *k_consumer;
const char *k_loop_unique_num;

    // Annotations writeen

const char *k_parallel_procedure;

    // Local annotations
const char *k_orig_lower_bound;
const char *k_orig_upper_bound;
const char *k_privatized_var;
const char *k_finalization_before;
const char *k_finalization_after;
const char *k_finalize_index;
const char *k_local_var;
const char *k_reduction_identity;
const char *k_doall_conditions;
const char *k_passed_by_ref;

/* Private global variables */

static boolean sf2c_flag = FALSE;
static boolean found_main = FALSE;


/* Private function declarations */

static void program_usage(char *progname);
static void pgen_initialize();
static void read_arguments(int argc, char **argv, char **input_file, 
			   char **output_file);
static void process_file(file_set_entry *fse);
static void process_procedure(file_set_entry *fse, proc_sym *ps);
static void process_feedback(char *feedback_file_name);

/* Function specifications */

static void program_usage(char *progname)
{
    fprintf(stderr,"usage: %s [options] [infile outfile]\n", progname);

    fprintf(stderr, "  -add-params\n");
    fprintf(stderr, "    pass global arrays as parameters\n");
    fprintf(stderr, "  -check-work\n");
    fprintf(stderr, "      creates function to check the amount of "
        "work before parallelizing\n");
    fprintf(stderr, "  -feedback <file>\n");
    fprintf(stderr, "      don't parallelize loops as specified by <file>\n");

    fprintf(stderr, "  -fortran-form\n");
    fprintf(stderr, "      create code that is convertible to Fortran\n");

    fprintf(stderr, "  -g <n>  pipeline granularity, default = 4\n");
    fprintf(stderr, "  -l      print out line numbers of parallel regions\n");
    fprintf(stderr, "  -m <c+> mask that tells which loops to parallelize\n");

    fprintf(stderr, "  -no-array-reductions\n");
    fprintf(stderr, "    suppress reductions over array variables\n");
    fprintf(stderr, "  -no-cache-tile\n");
    fprintf(stderr, "    don't tile for cache locality\n");

    fprintf(stderr, "  -p <n>  number of processors, p = 0 means unknown\n");
    fprintf(stderr, "  -reduction-guards\n");
    fprintf(stderr, "      generate guards before reductions\n");
    fprintf(stderr, "  -sf2c   was sf2c run for Fortran source?\n");

    fprintf(stderr, "  -sequential-stubs\n");
    fprintf(stderr, "      generate stubs for sequential versions of tasks\n");

    fprintf(stderr, "\n");

    fprintf(stderr, "  -d <c>  debugging info:\n");
    fprintf(stderr, "     'd'  dump modified procedures\n");
    fprintf(stderr, "     'f'  fileset and file symbol table info\n");
    fprintf(stderr, "     'g'  general, high-level progress\n");
    fprintf(stderr, "     'm'  variable mapping from proc -> task\n");
    fprintf(stderr, "     'p'  privatized and reduced variable\n");
    fprintf(stderr, "     'r'  warns if variables passed by reference\n");
    fprintf(stderr, "     's'  scheduling information\n");
    exit(-1);
}


static void pgen_initialize()
{
    // Read annotations
    ANNOTE(k_begin_parallel_region, "begin_parallel_region", TRUE);
    ANNOTE(k_end_parallel_region, "end_parallel_region", TRUE);
    ANNOTE(k_privatized, "privatized", TRUE);
    ANNOTE(k_privatizable, "privatizable", TRUE);
    ANNOTE(k_reduction, "reduction", TRUE);
    ANNOTE(k_reduced, "reduced", TRUE);

    ANNOTE(k_data_decomp, "data_decomp", FALSE);
    ANNOTE(k_loop_bounds, "loop_bounds", FALSE);
    ANNOTE(k_block_sizes, "block_sizes", FALSE);
    // ANNOTE(k_doall, "doall", TRUE);     // registered already in transform
    ANNOTE(k_doall_region, "doall_region", TRUE);
    ANNOTE(k_doacross, "doacross", TRUE);
    ANNOTE(k_loop_cyclic, "loop_cyclic", FALSE);
    ANNOTE(k_block_size, "block_size", FALSE);
    ANNOTE(k_tile_loops, "tile_loops", TRUE);
    ANNOTE(k_tileable_loops, "tileable_loops", TRUE);
    ANNOTE(k_guard, "guard", FALSE);
    ANNOTE(k_sync_counter, "sync_counter", FALSE);

    ANNOTE(k_lock, "lock", TRUE);
    ANNOTE(k_unlock, "unlock", TRUE);
    ANNOTE(k_global_barrier, "global_barrier", TRUE);
    ANNOTE(k_sync_neighbor, "sync_neighbor", TRUE);
    ANNOTE(k_HPF, "HPF", TRUE);
    ANNOTE(k_data_layout, "data_layout", TRUE);
    ANNOTE(k_reduction_gen, "reduction_gen", TRUE);
    ANNOTE(k_reduced_gen, "reduced_gen", TRUE);
    ANNOTE(k_associativity, "associativity", TRUE);
    ANNOTE(k_array_total_size, "array total size", TRUE);
    ANNOTE(k_reduction_identity, "reduction_identity", FALSE);
    ANNOTE(k_nprocs_var, "nprocs_var", TRUE);
    ANNOTE(k_nprocs1_var, "nprocs1_var", TRUE);
    ANNOTE(k_nprocs2_var, "nprocs2_var", TRUE);
    ANNOTE(k_parallel_procedure, "parallel_procedure", TRUE);

    ANNOTE(k_doall_annote, "doall_annote", TRUE);
    ANNOTE(k_speculate, "speculate", TRUE);
    ANNOTE(k_speculate_id, "speculate_id", TRUE);
    ANNOTE(k_pipeline, "pipeline", TRUE);
    ANNOTE(k_producer, "producer", TRUE);
    ANNOTE(k_consumer, "consumer", TRUE);
    ANNOTE(k_loop_unique_num, "loop_unique_num", TRUE);

    // Local annotations used only by pgen
    ANNOTE(k_finalize_index, "finalize_index", FALSE);
    ANNOTE(k_structure_field, "structure_field", FALSE);
    ANNOTE(k_doall_conditions, "doall_conditions", FALSE);
    ANNOTE(k_passed_by_ref, "passed_by_reference", FALSE);
    ANNOTE(k_true_type, "true_type", TRUE);

    k_orig_lower_bound = lexicon->enter("orig_lower_bound")->sp;
    k_orig_upper_bound = lexicon->enter("orig_upper_bound")->sp;
    k_finalization_before = lexicon->enter("finalization_before")->sp;
    k_finalization_after = lexicon->enter("finalization_after")->sp;
    k_local_var = lexicon->enter("local_var")->sp;
    k_privatized_var = lexicon->enter("privatized_var")->sp;
}


static void read_arguments(int argc, char **argv, char **input_file, 
			   char **output_file)
{
    static char *debug_arg = NULL;
    static char *mask_arg = NULL;
    static char *feedback_arg = NULL;

    static cmd_line_option option_table[] =
      {
	{CLO_NOARG,  "-add-params",          NULL, &add_params_flag},
        {CLO_NOARG,  "-check-work",          "",   &check_work_flag},
	{CLO_STRING, "-feedback",            "",   &feedback_arg},
	{CLO_NOARG,  "-fortran-form",        NULL, &fortran_form_flag},
        {CLO_STRING, "-d",                   "",   &debug_arg},
        {CLO_INT,    "-g",                   "4",  &pipeline_grain_flag},
        {CLO_NOARG,  "-l",                   NULL, &print_line_flag},
        {CLO_STRING, "-m",                   "",   &mask_arg},
	{CLO_NOARG,  "-inline-sync", "",   &inline_sync_flag},
	{CLO_NOARG,  "-sim-assign", "",   &sim_assign_flag},
	{CLO_NOARG,  "-no-array-reductions", "",   &no_array_reductions_flag},
        {CLO_NOARG,  "-no-cache-tile",       "",   &no_cache_tile_flag},
        {CLO_INT,    "-p",                   "0",  &nprocs_flag},
	{CLO_NOARG,  "-reduction-guards",    "",   &reduction_guards_flag},
	{CLO_NOARG,  "-sequential-stubs",    "",   &sequential_stubs_flag},
        {CLO_NOARG,  "-sf2c",                NULL, &sf2c_flag},
	{CLO_NOARG,  "-stub",                NULL, &fortran_form_flag},
      };

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    if (argc != 3) program_usage(argv[0]);

    *input_file = argv[1];
    *output_file = argv[2];

    // Handle debug flags
    //
    char *s;
    for(s = debug_arg; *s; s++)  debug[*s-'\0']++;

    // Handle mask flags
    //
    for(s = mask_arg; *s; s++)  mask[*s-'\0']++;

    // Process feedback file
    // 
    if (feedback_arg && (strlen(feedback_arg) > 0)) 
      process_feedback(feedback_arg);

}

static void process_procedure(file_set_entry *fse, proc_sym *ps)
{
    if (ps->src_lang() == src_fortran && !sf2c_flag) {
        warning_line(ps->block(), 
	    "Fortran procedure %s found and -sf2c not set", ps->name());
    }
 
    parallelize(fse, ps);

    // Change "main" to "start".  Note that for FORTRAN
    // programs (run through sf2c) there is no "main" -- "start"
    // is defined in the F77_doall library.
    //
    if (strcmp(ps->name(), "main") == 0) {
        found_main = TRUE;
        modify_main(ps);
    }
}

static void process_file(file_set_entry *fse)
{
    if (debug['g'-'\0']) {
        printf("Processing fileset %s\n",  fse->name());
    }

    create_runtime_symbols(fse);       // extern objects in doall lib

    fse->reset_proc_iter();
    proc_sym *ps;

    while ((ps = fse->next_proc())) {
        if (ps->is_readable() && !ps->is_written()) {
            if (debug['g'-'\0']) {
                printf("\nParallelizing procedure %s\n", ps->name());
            }

            ps->read_proc(TRUE, FALSE);  // exp-trees=TRUE, fortran=FALSE

	    process_procedure(fse, ps);

	    ps->write_proc(fse);
	    ps->flush_proc();
        }
    }
}


int main(int argc, char **argv)
{
    char *input_file, *output_file;

    start_suif(argc, argv);
    pgen_initialize();   

    for (int i = 0; i < 256; i++) {
	debug[i] = 0;
	mask[i] = 0;
    }

    read_arguments(argc, argv, &input_file, &output_file);
    fileset->add_file(input_file, output_file);

    if (debug['g'-'\0']) {
        printf("Options: \n");
        printf("    Number of processors = %d\n\n", nprocs_flag);
    }

    fileset->reset_iter();
    file_set_entry *fse;

    while ((fse = fileset->next_file())) {
	process_file(fse);
    }

    if (!found_main && !sf2c_flag) {
	warning_line(NULL, "**Danger! main procedure not found");
    }

    delete fileset;
    fileset = NULL;
    return 0;
}

static void process_feedback(char *feedback_file_name)
{
  FILE *fp;
  char line[STRING_SIZE]; 
  char name[STRING_SIZE];     
  boolean in_region = FALSE;
  int doall_index, iter_threshhold, max_parallelism;
  int i;

  assert(feedback_file_name);

  fileset->set_max_open_files(fileset->get_max_open_files() - 1);
  if (!(fp = fopen(feedback_file_name, "r"))) {
    warning_line(NULL, "unable to open file \"%s\" for reading",
		 feedback_file_name);
    return;
  }

  while (fgets(line, STRING_SIZE, fp) != NULL) {

    if (line[0] == '#') continue; // skip comments
    
    if ((!in_region) && 
	(sscanf(line, "DOALLS %d\n", &num_feedback_doalls) == 1)) {

      if (num_feedback_doalls < 0) {
	warning_line(NULL, "%s: Bad number of doalls %d\n", 
		     feedback_file_name, num_feedback_doalls);
	num_feedback_doalls = 0;
	return;
      } else {
	feedback_info = new int[num_feedback_doalls];
	in_region = TRUE;

	for (i = 0; i< num_feedback_doalls; i++) {
	  feedback_info[i] = 1024;  // Parallelize by default
	}
      }
    } else if ((in_region) && 
	       (sscanf(line, "ENTRY %s %d %d %d\n", name, &doall_index,
		       &iter_threshhold, &max_parallelism) == 4)) {

      if ((doall_index < 0) || (doall_index > num_feedback_doalls)) {
	  warning_line(NULL, "%s: Ignored line doall_index(=%d) \nline=%s\n",
		  feedback_file_name, doall_index, line);
      } else {
	feedback_info[doall_index] = max_parallelism; 
      }
    } else {
      warning_line(NULL, "%s: Bad format: in_region=%d, line=%s\n", 
		   feedback_file_name, in_region, line);
    }
  } // while

  fclose(fp);
}
