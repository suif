/* file "parloop.cc" */

/*----------------------------------------------------------------------*
 *
 *    (C) COPYRIGHT by the SUIF Compiler Group, Stanford University.
 *        All rights reserved.
 *
 *
 *  REVISION HISTORY
 *  ----------------
 *
 *     $Log: parloop.cc,v $
 *     Revision 1.1.1.1  1998/07/07 05:09:31  brm
 *     rcs--0-0-beta-3-alpha-168
 *
 *     Revision 1.1  1993/10/25 17:43:40  anderson
 *     Initial revision
 *
 *
 *  NOTICE: All swine portrayed in this program are fictitious and any
 *      similarity to any real pigs, living or dead, is purely
 *      coincidental.
 *----------------------------------------------------------------------*/

#pragma implementation "parloop.h"
#define RCS_BASE_FILE parloop_cc

#include "parloop.h"

RCS_BASE("$Id: parloop.cc,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")

//
// classes ParamMap and ParamMapList
//

void ParamMap::print(FILE *fp)
{
    fprintf(fp, "Parameter Mapping -- passed by %s\n", 
        (pass_by_reference ? "reference" : "value"));
    fprintf(fp, "  Task = %s\n", (task_sym ? task_sym->name() : "< >"));
    fprintf(fp, "  Parent = %s\n", (parent_sym ? parent_sym->name() : "< >"));
    fprintf(fp, "  Global = %s\n", (global_sym ? global_sym->name() : "< >"));
}


ParamMap* ParamMapList::lookup_by_task(var_sym *vs)
{
   ParamMapList_iter it(this);

   while (!it.is_empty()) {
       ParamMap *map = it.step();
       if (map->task_sym == vs) return (map);
   }

   return (NULL);
}

ParamMap* ParamMapList::lookup_by_parent(var_sym *vs)
{
   ParamMapList_iter it(this);

   while (!it.is_empty()) {
       ParamMap *map = it.step();
       if (map->parent_sym == vs) return (map);
   }

   return (NULL);
}

ParamMap* ParamMapList::lookup_by_global(var_sym *vs)
{
   ParamMapList_iter it(this);

   while (!it.is_empty()) {
       ParamMap *map = it.step();
       if (map->global_sym == vs) return (map);
   }

   return (NULL);
}

void ParamMapList::print(FILE *fp)
{
   ParamMapList_iter it(this);

   while (!it.is_empty()) {
       it.step()->print(fp);
       fprintf(fp, "\n");
   }
}


//
// class ParallelLoops
//

int ParallelLoops::num_loops_ = -1;
ParallelLoops::ParallelLoops(file_set_entry *f, proc_sym *ps)
{
    fse_ = f;
    param_mappings_ = new ParamMapList();
    parproc_ = new ParentProc(ps, this);
    num_loops_++;

    char bfr[256];
    sprintf(bfr, "_%s_%d", ps->name(), num_loops_);

    // Create new procedure for loop body:
    //
    proc_sym *psym = fse_->symtab()->new_proc(task_type, src_c, bfr); 
    proc_symtab *ptab = new proc_symtab(bfr);
    fse_->symtab()->add_child(ptab);

    psym->set_block(new tree_proc(new tree_node_list(), ptab));
    tsk_ = new TaskProc(f, psym, this);
}


//
// Add a loop to the parallel task
//
void ParallelLoops::add_loop(tree_for *tf, sched_kinds knd = SCHED_BLOCK)
{
    parproc_->call_doall(tf, tsk_->proc());
    parproc_->extract_loop(tf);
    tsk_->read_annotes(tf);
    tsk_->abstract(tf);

    // Now schedule the current loop
    tsk_->schedule(knd);
}


//
// Checks for duplicates, updates by_ref flag
//
void ParallelLoops::enter_param(var_sym *psym, var_sym *tsym, boolean by_ref)
{
    assert(psym && tsym);

    ParamMap *map = param_mappings_->lookup_by_parent(psym);
    if (map) assert(tsym == map->task_sym);
    else {
        map = new ParamMap;
	map->parent_sym = psym;
	map->task_sym = tsym;
	param_mappings_->append(map);
    }

    map->pass_by_reference = by_ref;
}


// 
//  Creates global variables to hold parameters
//
void ParallelLoops::create_params()
{
    ParamMapList_iter it(param_mappings_);
    while (!it.is_empty()) {
        ParamMap *map = it.step();
        assert(map->task_sym);
        var_sym *tsym = map->task_sym;

        char bfr[256];
        sprintf(bfr, "_%s_%d", tsym->name(), num_loops_);
               
        type_node *tn;
        if (map->pass_by_reference) {
            tn = fse_->symtab()->install_type(
	        new ptr_type(tsym->type()->copy()));

  	    if (debug['f']) {
 	        printf("Created new type: ");
	        tn->print(stdout);
		printf("\n");
  	    }
	}
        else tn = fse_->symtab()->install_type(tsym->type()->copy());

        var_sym *vs = fse_->symtab()->new_var(tn, bfr);
        if (tsym->is_addr_taken()) vs->set_addr_taken();
        vs->set_private();
	map->global_sym = vs;
    }
}

void ParallelLoops::write_task()
{
    // Traverse task and change all var's that are passed by reference to
    // deref the pointers
    tsk_->pass_by_reference();

    // Create global variables to hold params; assign to globals in parent
    // read from globals in task 
    create_params();
    tsk_->unpack_params();
    parproc_->pack_params();

    // Add return statement to task
    tsk_->proc()->block()->body()->append(new tree_instr(new in_rrr(io_ret)));

    if (debug['d']) {
        printf("*** Parallel loop completed ***\n");
        fse_->symtab()->print(stdout);
        printf("\nParent Procedure\n");
        printf("----------------\n");
        parproc_->proc()->block()->print(stdout);
        printf("\nTask Procedure\n");
        printf("--------------\n");
        tsk_->proc()->block()->print(stdout);
    }

    fse_->write_proc(tsk_->proc());
    fse_->flush_proc(tsk_->proc());
}


ParallelLoops::~ParallelLoops()
{
    delete parproc_;
    delete tsk_;

    delete param_mappings_;
}





