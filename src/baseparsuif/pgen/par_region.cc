/* file "par_region.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Regions of parallel code  */

#pragma implementation "par_region.h"
#define RCS_BASE_FILE   par_region_cc
#define BIG_STRING_SIZE     4096
#define SMALL_STRING_SIZE    256

#include "par_region.h"
#include <builder.h>
#include <string.h>
#include <useful.h>
#include <runtime_names.h>

// from popt/popt.h
#define BLOCK_SIZE_NAME     "_bk"        

RCS_BASE("$Id: par_region.cc,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")

void add_global_params(tree_node *tn, replacements *repl,
		       base_symtab *dst_scope);


/* Class implementations */

//
// class ParallelRegion
//


extern char * nprocs_name(int num);


  /* Public methods */

ParallelRegion::ParallelRegion(file_set_entry *fse, proc_sym *ps, 
			       tree_block *region_block, const char *extra_str)
{
    fse_ = fse;
    the_region_ = region_block;
    parent_ = new ParentProc(ps, this);
    region_num++;
    var_map_ = NULL;

    char buf[256];
    if (extra_str)
	sprintf(buf, "_%s_%d_%s_func", ps->name(), region_num, extra_str);
    else 
	sprintf(buf, "_%s_%d_func", ps->name(), region_num);

    // Create new procedure for the region:
    //
    assert(task_type);
    proc_sym *task_proc = 
	fse_->symtab()->new_proc(task_type, ps->src_lang(), buf); 
    proc_symtab *proc_tab = new proc_symtab(buf);
    task_proc->set_fse(fse_);
    fse_->symtab()->add_child(proc_tab);

    if (ps->peek_annote(k_no_recursion)) 
      task_proc->append_annote(k_no_recursion);
    task_proc->append_annote(k_parallel_procedure);

    tree_proc *tp = new tree_proc(new tree_node_list(), proc_tab);
    task_proc->set_block(tp);

    task_ = new TaskProc(tp, this);
}


ParallelRegion::~ParallelRegion()
{
    assert(parent_);
    assert(task_);

    delete parent_;
    delete task_;

    if (var_map_) delete var_map_;
}

// Move the code from the parent procedure to the task procedure,
// add calls to runtime library, adjust parameters.
//
void ParallelRegion::construct_region(int num_pipeline_counters)
{
    //    task_->set_num_counters(num_pipeline_counters);

    parent_->create_doall_site(the_region_);

    move_region();

    struct_type *copy_type = create_copy_type();  
    parent_->pack_exposed_vars(copy_type, task_->stub_block()->proc());
    task_->unpack_exposed_vars(copy_type); 

    /*    if (num_pipeline_counters) {
	//parent_->set_counters(num_pipeline_counters, suif_mynprocs_var);
        parent_->init_counters(num_pipeline_counters);
	parent_->incr_counters(num_pipeline_counters, suif_mynprocs_var);
    }*/
    parent_->init_counters(task_->num_counters());
    parent_->call_doall(task_->stub_block()->proc());

    if (fortran_form_flag) 
      {
	char buf[256];
	sprintf(buf, "_%s_%d_stubout", parent_->proc()->name(), region_num);
	block_symtab *new_symtab = new block_symtab(buf);
	tree_block *new_block = new tree_block(new tree_node_list, new_symtab);

	tree_node_list *doall_site = parent_->doall_call_site();
	assert(doall_site->parent()->is_block());
	tree_block *old_block = (tree_block *) doall_site->parent();
	base_symtab *old_symtab = old_block->symtab();

	base_symtab *parent_symtab = old_symtab->parent();
	parent_symtab->add_child(new_symtab);
	parent_symtab->remove_child(old_symtab);
	new_symtab->add_child(old_symtab);
	old_block->parent()->insert_after(new_block, old_block->list_e());
	old_block->parent()->remove(old_block->list_e());
	new_block->body()->append(old_block->list_e());
	zero_out_ref_converted_in_annotes = FALSE;
	proc_sym *stub_proc = outline(old_block, NULL, TRUE);
	if (new_block->proc()->peek_annote(k_no_recursion))
	  stub_proc->append_annote(k_no_recursion);

	sprintf(buf, "_%s_%d_stubout", parent_->proc()->name(), region_num);
	stub_proc->set_name(buf);
      }

    task_->cleanup();
}


void ParallelRegion::write_task()
{
    proc_sym *stub_proc = task_->stub_block()->proc();
    proc_sym *body_proc = task_->body_block()->proc();

    if (debug['d'-'\0']) {
        printf("\n*** Parallel region completed ***\n");

	if (debug['f'-'\0'] > 1) {
	    fileset->globals()->print(stdout);
	    fse_->symtab()->print(stdout);
	}

        printf("\nParent Procedure\n");
        printf("----------------\n");
        parent_->proc()->block()->print(stdout);
        printf("\nTask Procedure\n");
        printf("--------------\n");
        body_proc->block()->print(stdout);

	if (fortran_form_flag) {
	  printf("\nStub Procedure\n");
	  printf("--------------\n");
	  stub_proc->block()->print(stdout);
        }
    }

    body_proc->write_proc(fse_);
    body_proc->flush_proc();

    if (fortran_form_flag) {
      assert(!stub_proc->is_written());
      stub_proc->write_proc(fse_);
      stub_proc->flush_proc();
    }
}


  /* Private methods */

// Move the code from parent to task
//
void ParallelRegion::move_region()
{
    if (debug['f'-'\0'] > 1) {
	printf("\n\nBefore Cloning\n");
	printf("** Global symbol table **\n");
	fileset->globals()->print(stdout);
	printf("** Fileset symbol table **\n");
	fse_->symtab()->print(stdout);
    }

    base_symtab *dst_scope = task_->body_block()->symtab();
    replacements *repl = new replacements();
    the_region_->find_exposed_refs(dst_scope, repl);
    dst_scope->resolve_exposed_refs(repl);      

    if (add_params_flag) {
      add_global_params(the_region_, repl, dst_scope);

      if (debug['m'-'\0'] > 1) {
        printf("Added global params, replacement list is:\n");
        print_var_map(repl);
      }
    }

    tree_block *task_body = (tree_block *) the_region_->clone_helper(repl);

    task_->body_block()->body()->append(task_body);
    task_->set_body_block(task_body);
    task_->create_task_params();

    if (debug['f'-'\0'] > 1) {
	printf("\n\nAfter Cloning\n");
	printf("** Global symbol table **\n");
	fileset->globals()->print(stdout);
	printf("** Fileset symbol table **\n");
	fse_->symtab()->print(stdout);
    }

    if (debug['m'-'\0']) {
        printf("Finished cloning, replacement list is:\n");
        print_var_map(repl);
    }

    create_var_map(repl, the_region_->symtab());

    if (debug['m'-'\0']) {
        printf("Created variable mapping list\n");
        print_var_map(var_map_);
    }

    parent_->remove_region(the_region_);   // remove original region
    delete the_region_;

    the_region_ = task_->body_block();
    task_->construct_task();

    if (debug['m'-'\0']) {
        printf("Constructed task\n");
        print_var_map(var_map_);
    }

    // Note: this must be done *after* the task is constructed because
    // some of the promoted symbols might be passed by reference.  
    // The task takes care of modifying its code to handle all passed
    // by ref symbols, then the vars can be promoted to the global scope.
    promote_invisible_types();

    if (fortran_form_flag) {
	char buf[256];
	sprintf(buf, "_%s_%d_stubin", parent_->proc()->name(), region_num);
	block_symtab *new_symtab = new block_symtab(buf);
        tree_block *new_region =
                new tree_block(new tree_node_list, new_symtab);

	tree_block *old_block = task_->body_block();
        base_symtab *old_symtab = old_block->symtab();

        base_symtab *parent_symtab = old_symtab->parent();
	parent_symtab->add_child(new_symtab);
        parent_symtab->remove_child(old_symtab);
        new_symtab->add_child(old_symtab);

        old_block->parent()->insert_after(new_region, old_block->list_e());
        old_block->parent()->remove(old_block->list_e());
        new_region->body()->append(old_block->list_e());
	zero_out_ref_converted_in_annotes = FALSE;
        proc_sym *task_proc = outline(old_block, NULL, FALSE);

	if (new_region->proc()->peek_annote(k_no_recursion))
	  task_proc->append_annote(k_no_recursion);

	proc_sym *stub_proc = task_->stub_block()->proc();
	stub_proc->get_annote(k_parallel_procedure);
	task_proc->append_annote(k_parallel_procedure);

	task_proc->set_fse(fse_);
	task_->set_body_block(task_proc->block());

	// Update the names of the stub and task function
	task_proc->set_name(stub_proc->name());
	task_proc->block()->proc_syms()->set_name(task_proc->name());

	stub_proc->set_name(buf);
	stub_proc->block()->proc_syms()->set_name(stub_proc->name());

	// Pass in my_id, update annotations
	tree_node *tn = new_region->body()->tail()->contents;
	assert(tn && tn->is_instr());
	tree_instr *ti = (tree_instr *) tn;
	assert(ti->instr()->opcode() == io_cal);
	in_cal *cal = (in_cal *) ti->instr();
	int nargs = cal->num_args();
	proc_symtab *task_tab = task_proc->block()->proc_syms();

	boolean found_my_id = FALSE;
	for (int i = 0; i < nargs; i++) {
	  operand curr_arg_op = cal->argument(i);

	  if (curr_arg_op.is_symbol()) {
	    sym_node *actual_sym = curr_arg_op.symbol();
	    sym_node *formal_sym = (*task_tab->params())[i];

	    actual_sym->copy_annotes(formal_sym);

	    if (actual_sym == task_->my_id_var(0)) {
	      found_my_id = TRUE;
	      assert(formal_sym->is_var());
	      task_->set_id_var((var_sym *) formal_sym, 0);
	    } 
	  }
	}

	if (!found_my_id) {
	    cal->set_num_args(nargs + 1);
	    cal->set_argument(nargs, operand(task_->my_id_var(0)));

	    var_sym *id_var = task_tab->new_var(task_->my_id_var(0)->type(),
						task_->my_id_var(0)->name());
	    id_var->set_param();
	    task_tab->params()->append(id_var);
	    task_->set_id_var(id_var, 0);
	}
    }

}

// Remove any sym_nodes that are *not* going to be copied from the
// parent proc into the task proc.  This includes:
// -- any sym_nodes that are not var_syms, 
// -- sym_nodes that are not outwards exposed (e.g. they are declared 
//    inside symtabs that are inside the region cloned).
// -- children variables (pass only parent variables b/c children are 
//        included)
//    
void ParallelRegion::create_var_map(replacements *repl, 
				    base_symtab *orig_scope)
{
    assert(!var_map_);

    sym_node_list_iter old_snli(&repl->oldsyms);
    sym_node_list_iter new_snli(&repl->newsyms);

    while (!old_snli.is_empty()) {
	assert(!new_snli.is_empty());

	sym_node *old_sym = old_snli.step();
	sym_node *new_sym = new_snli.step();


    // Avoid passing block size or processor id variables in as arguments,
    // they will be assigned in the task procedure itself.  Don't need to
    // do this check if pgen was smarter about eliminating unnecessary
    // parameter passing, since these variables are privatizable.

	const char *str = old_sym->name();
	for (int i = 0; i <= MAX_PROC_DIM; i++) {
	  char *nprocs_str = nprocs_name(i);
	  if (!strncmp(str, nprocs_str, strlen(nprocs_str))) {
	    repl->oldsyms.remove(old_snli.cur_elem());
	    repl->newsyms.remove(new_snli.cur_elem());
	  }
	}

	if (!strncmp(str, MY_PID_NAME, strlen(MY_PID_NAME)) ||
	    !strncmp(str, BLOCK_SIZE_NAME, strlen(BLOCK_SIZE_NAME))) {
	    repl->oldsyms.remove(old_snli.cur_elem());
	    repl->newsyms.remove(new_snli.cur_elem());
	}

	else if (!old_sym->is_var()) {
	    assert(!new_sym->is_var());
	    repl->oldsyms.remove(old_snli.cur_elem());
	    repl->newsyms.remove(new_snli.cur_elem());
	}

        else {  // We know it's a variable
	    var_sym *old_var = (var_sym *) old_sym;
	    assert(new_sym->is_var());
	    var_sym *new_var = (var_sym *) new_sym;
	    var_sym *old_parent_var = old_var->parent_var();

	    if (old_var->is_addr_taken()) new_var->set_addr_taken();

	    if (old_sym->parent()->is_ancestor(orig_scope)) {
		repl->oldsyms.remove(old_snli.cur_elem());
		repl->newsyms.remove(new_snli.cur_elem());
	    }

	    // If this variable has a parent:
	    // -- make sure parent is on the replacements list
	    // -- remove the child from the replacements list
	    //
	    else if (old_parent_var) {
		assert(repl->oldsyms.lookup(old_parent_var));
		repl->oldsyms.remove(old_snli.cur_elem());
		repl->newsyms.remove(new_snli.cur_elem());
	    } else {
		new_var->reset_param();                  // None are params
	    }
        }
    }

    var_map_ = repl;
}


// When cloning, the library may not be able to put all the types into 
// the scope enclosing the source and destination scopes.  This is because
// some types refer to symbols that are local to the source scope -- and
// these symbols are *copied* into the destination scope.  This method
// finds those types and promotes them (and any associated symbols) 
// to the enclosing scope.
//
void ParallelRegion::promote_invisible_types()
{
    assert(var_map_);

    replacements r;
    file_symtab *dst_scope = fse_->symtab();

    type_node_list_iter new_tnli(&var_map_->newtypes);
    type_node_list_iter old_tnli(&var_map_->oldtypes);

    // Find all types that are not in the enclosing scope (i.e. the old and
    // types are not equal)
    while(!new_tnli.is_empty()) {
	assert(!old_tnli.is_empty());
	
	type_node *new_type = new_tnli.step();
	type_node *old_type = old_tnli.step();

	if (new_type != old_type) {
	    // recursively adds the associated symbols and types to 'r'
	    r.add_type_ref(new_type, dst_scope);  
        }                                 
    }

    if (debug['m'-'\0']) {
	printf("Promoting invisible types and symbols\n");
	print_var_map(&r);
    }

    // Move the symbols and types to the enclosing scope:
    // Symbols must be moved first, so that types that contain symbols
    // can be installed.
    // For types, first remove the type (and all associated types) and
    // then install into new symbol table.
    // 
    sym_node_list_iter old_snli(&r.oldsyms);
    while (!old_snli.is_empty()) {
	sym_node *old_sym = old_snli.step();
	assert(old_sym->is_var());
	var_sym *old_var = (var_sym *) old_sym;

	char buf[256];
	sprintf(buf, "_%s_%d", old_sym->name(), region_num);
	old_sym->set_name(buf);

        old_sym->parent()->remove_sym(old_sym);
	old_sym->clear_sym_id();
	dst_scope->add_sym(old_sym);

	var_def *def = new var_def(old_var, get_alignment(old_var->type()));
	dst_scope->add_def(def);
    }

    old_tnli.reset(&r.oldtypes);
    while (!old_tnli.is_empty()) {
	type_node *old_type = old_tnli.step();
        old_type->parent()->remove_type(old_type);
    }

    old_tnli.reset(&r.oldtypes);
    while (!old_tnli.is_empty()) {
	type_node *old_type = old_tnli.step();
	old_type->clear_type_id();
	dst_scope->install_type(old_type);
    }

}

//  Creates structure type.  The structures will be used to copy exposed
//  variables from the parent to the task.
//
struct_type *ParallelRegion::create_copy_type()
{
    assert(var_map_);

    sym_node_list_iter new_snli(&var_map_->newsyms);
    sym_node_list_iter old_snli(&var_map_->oldsyms);

    char buf[BIG_STRING_SIZE];
    char temp[BIG_STRING_SIZE];
    char field_name[SMALL_STRING_SIZE];
    char id_str[SMALL_STRING_SIZE];
    buf[0] = '\0';

    // Builder uses type numbers, so make sure that the global and file 
    // symtabs have unique ids:
    // 
    fileset->globals()->number_globals();
    fse_->symtab()->number_globals();

    int field_count = 0;
    while (!new_snli.is_empty()) {
	assert(!old_snli.is_empty());

	sym_node *new_sym = new_snli.step();
	sym_node *old_sym = old_snli.step();
	assert(new_sym->is_var());

	annote *an;
	an = new_sym->annotes()->get_annote(k_structure_field);
	if (an) delete an;
	an = old_sym->annotes()->get_annote(k_structure_field);
	if (an) delete an;

        type_node *field_type = ((var_sym *) new_sym)->type();
	assert(fse_->symtab()->is_ancestor(field_type->parent()));

        // Don't include symbols that are visible to both parent and task
	// 
	if (fse_->symtab()->is_ancestor(new_sym->parent())) continue;
	
	immed_list *iml = new immed_list();
	iml->append(immed(field_count));
	an = new annote(k_structure_field, iml);
	new_sym->annotes()->append(an);
	iml = new immed_list();
	iml->append(immed(field_count));
	an = new annote(k_structure_field, iml);
	old_sym->annotes()->append(an);

	sprintf(field_name, "_field_%d", field_count++);
	sprintf(id_str, "%%%u", field_type->type_id());
	strcpy(temp, buf);
	int len = strlen(temp) + strlen(field_name) + strlen(id_str) + 4;
	assert_msg(len < BIG_STRING_SIZE, 
	  ("String size %d too small in create_copy_type() -- need %d", 
	   BIG_STRING_SIZE, len));

	sprintf(buf, "%s %s %s;", temp, id_str, field_name);
    }

    if (field_count == 0) {   // Create dummy struct
        sprintf(buf, "int _dummy_field;");
    }

    strcpy(temp, buf);
    sprintf(buf, "struct { %s }", temp);

    type_node *copy_type = block::parse_type(fse_->symtab(), buf);
    copy_type = fse_->symtab()->install_type(copy_type);  // double-checking
    assert(copy_type->is_struct());

    if (debug['f'-'\0']) {
        printf("Created new STRUCT type: ");
        copy_type->print_full(stdout);
	printf("\n");
    }

    int copy_type_bytes = copy_type->size() / suif_byte_size;
    int runtime_struct_bytes = MAX_ARGS_SIZE * target.size[C_char];
    assert_msg(copy_type_bytes <= runtime_struct_bytes,
       ("Size of arguments structure (%d) exceeds runtime structure (%d) "
	"in procedure %s; change MAX_ARGS_SIZE in runtime library",
	copy_type_bytes, runtime_struct_bytes, parent()->proc()->name()));

    return ((struct_type  *) copy_type);
}


static void find_global_refs_on_object(sym_node *the_sym,
                                       so_walker *the_walker)
{
    if ((!the_sym->is_global()) || (!the_sym->is_var()))
        return;
    if (the_walker->in_annotation())
        return;

    var_sym *the_var = (var_sym *) the_sym;
    if (!the_var->type()->is_array()) 
      return;

    temp_marker *the_marker = (temp_marker *)(the_walker->get_data(0).ptr);
    the_marker->mark(the_sym);
}


void add_global_params(tree_node *tn,replacements *repl,base_symtab *dst_scope)
{
  temp_marker global_used;
  so_walker the_walker;
  the_walker.set_data(0, &global_used);
  the_walker.walk(tn, &find_global_refs_on_object);

  suif_object_list_iter global_iter(global_used.list());
  while (!global_iter.is_empty()) {
    suif_object *this_object = global_iter.step();
    assert(this_object->is_sym_obj());
    sym_node *this_sym = (sym_node *)this_object;
    assert(this_sym->is_global());
    assert(this_sym->is_var());
    var_sym *this_var = (var_sym *)this_sym;

    repl->oldsyms.append(this_var);
    repl->newsyms.append(dst_scope->new_unique_var(this_var->type(),
						   this_var->name()));
  }
}


/* Public Functions */

// Print out symbols and types in replacement list
// Used for debugging only.
//
void print_var_map(replacements *var_map, FILE *f)
{
    assert(var_map);

    sym_node_list_iter old_snli(&var_map->oldsyms);
    sym_node_list_iter new_snli(&var_map->newsyms);

    fprintf(f, "** Symbols **\n");
    fprintf(f, "  Old\n");
    while (!old_snli.is_empty()) {
	sym_node *old_sym = old_snli.step();
	old_sym->print_full(f, 6);
    }

    fprintf(f, "\n  New\n");
    while (!new_snli.is_empty()) {
	sym_node *new_sym = new_snli.step();
	new_sym->print_full(f, 6);
    }

    type_node_list_iter old_tnli(&var_map->oldtypes);
    type_node_list_iter new_tnli(&var_map->newtypes);

    fprintf(f, "** Types **\n");
    fprintf(f, "  Old\n");
    while (!old_tnli.is_empty()) {
	type_node *old_type = old_tnli.step();
	old_type->print_full(f, 6);
    }

    fprintf(f, "\n  New\n");
    while (!new_tnli.is_empty()) {
	type_node *new_type = new_tnli.step();
	new_type->print_full(f, 6);
    }
}


