/* file "par_region.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Regions of parallel code  */

#ifndef PAR_REGION_H
#define PAR_REGION_H

#pragma interface

#include "pgen.h"
#include "parent.h"
#include "task.h"

RCS_HEADER(par_region_h, "$Id: par_region.h,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")

class ParallelRegion {
private:
    ParentProc *parent_;                 // original procedure
    TaskProc *task_;                     // proc to run on all processors
    tree_block *the_region_;                 

    replacements *var_map_;

    file_set_entry *fse_;

    void move_region();
    void create_var_map(replacements *repl, 
			base_symtab *orig_scope);
    void promote_invisible_types();
    struct_type *create_copy_type();

public:
    ParallelRegion(file_set_entry *fse,proc_sym *ps,tree_block *region_block,
		   const char *extra_str);
    ~ParallelRegion();

    ParentProc *parent()               { return parent_; }
    TaskProc *task()                   { return task_; }

    file_set_entry *fse()              { return fse_; }
    replacements *var_map()            { return var_map_; }

    void construct_region(int num_pipeline_counters);
    void write_task();
};

#endif




