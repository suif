/* file "sched.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  Schedule a parallel loop -- used with doall loops only */

#define RCS_BASE_FILE sched_cc

#include "pgen.h"
#include <builder.h>

RCS_BASE("$Id: sched.cc,v 1.1.1.1 1998/07/07 05:09:31 brm Exp $")

/* Public function declarations */

boolean make_block_sched(tree_for *tf,var_sym *my_id_var,var_sym *nprocs_var);
boolean make_cyclic_sched(tree_for *tf,var_sym *my_id_var,
			  var_sym *nprocs_var);

/* Private function declarations */

static boolean normalize_test(tree_for *tf);

/* Public function specifications */


//
// Block schedule is as follows (for sign {s,u}lt):
//
// range = divceil(ub - lb, step)
//
// lb_new = MAX(range * pid / num_proc, lb)*step + lb          
// ub_new = MIN(range * (pid+1) / num_proc, ub)*step + lb
// step_new = step
//
//
// This assumes that the tests have been normalized to {s,u}lt 
//
boolean make_block_sched(tree_for *tf, var_sym *my_id_var,var_sym *nprocs_var)
{
    assert(nprocs_var);
    assert(my_id_var);

    // First check if a block schedule is possible:
    
    if (!normalize_test(tf)) {
      if (debug['s']) {
        printf("Block schedule not possible, Bad bounds/step in loop %s\n",
	       tf->index()->name());
      }
      return FALSE;
    }

    block ub(tf->ub_op());
    block lb(tf->lb_op());
    block step(tf->step_op());

    block diff_bldr(ub - lb);
    block range(diff_bldr.dobinop(bop_divceil, step));

    block pid(my_id_var);
    block nprocs(nprocs_var);

    block new_lb(((range * pid) / nprocs) * step + lb);
    block new_ub(((range * (pid+1)) / nprocs) * step + lb);

    if (tf->test() == FOR_SLT || tf->test() == FOR_ULT) {
          new_lb.set(new_lb.dobinop(bop_max, lb));
          new_ub.set(new_ub.dobinop(bop_min, ub));
    }
    else if (tf->test() == FOR_SGT || tf->test() == FOR_UGT) {
          new_lb.set(new_lb.dobinop(bop_min, lb));
          new_ub.set(new_ub.dobinop(bop_max, ub));
    }
    else assert(FALSE);

    operand new_lb_op = operand(new_lb.make_instruction(tf));
    operand new_ub_op = operand(new_ub.make_instruction(tf));

    tf->lb_op().remove();
    tf->ub_op().remove();

    tf->set_lb_op(new_lb_op);
    tf->set_ub_op(new_ub_op);

    return TRUE;
}



//
// Cyclic schedule is as follows (for sign {s,u}lt):
//
// lb_new = MAX((pid*step) + lb,lb)
// ub_new = ub
// step_new = (step*numprocs)
//
// MAX becomes MIN if sign is {s,u}gt
//
// This assumes that the tests have been normalized to {s,u}lt or {s,u}gt
//
boolean make_cyclic_sched(tree_for *tf,var_sym *my_id_var,var_sym *nprocs_var)
{
    assert(nprocs_var);
    assert(my_id_var);

    // First check if a cyclic schedule is possible:

    if (tf->index()->type()->op() != TYPE_INT && 
        tf->index()->type()->op() != TYPE_FLOAT) {
      if (debug['s']) {
        printf("Cyclic schedule not possible, Bad index in loop %s\n",
	       tf->index()->name());
      }
      return FALSE;
    }

    block lb(tf->lb_op());
    block step(tf->step_op());
    block pid(my_id_var);
    block np(nprocs_var);

    block new_lb((pid * step) + lb);
    block new_step(step * np);

    if (tf->test() == FOR_SLT || tf->test() == FOR_ULT ||
        tf->test() == FOR_SLTE || tf->test() == FOR_ULTE) {
          new_lb.set(new_lb.dobinop(bop_max, lb));
    }
    else if (tf->test() == FOR_SGT || tf->test() == FOR_UGT ||
             tf->test() == FOR_SGTE || tf->test() == FOR_UGTE) {
          new_lb.set(new_lb.dobinop(bop_min, lb));
    }
    else assert(FALSE);

    operand new_lb_op = operand(new_lb.make_instruction(tf));
    operand new_step_op = operand(new_step.make_instruction(tf));

    tf->lb_op().remove();
    tf->step_op().remove();

    tf->set_lb_op(new_lb_op);
    tf->set_step_op(new_step_op);

    return TRUE;
}


/* Private function specifications */

// Change bounds so that {s,u}lte -> {s,u}lt and {s,u}gte -> {s,u}gt
//
static boolean normalize_test(tree_for *tf)
{
    //
    // First make sure bounds/step are in a form we can handle:
    //
    if (tf->index()->type()->op() != TYPE_INT && 
        tf->index()->type()->op() != TYPE_FLOAT) return FALSE;

    if (tf->test() == FOR_ULTE || tf->test() == FOR_SLTE || 
	tf->test() == FOR_UGTE || tf->test() == FOR_SGTE) {

        int diff = 
	  ((tf->test() == FOR_ULTE || tf->test() == FOR_SLTE) ? 1 : -1);

        block ub_bldr(tf->ub_op());
        block ub_new_bldr(ub_bldr + diff);
        tf->ub_op().remove();
	tf->set_ub_op(operand(ub_new_bldr.make_instruction()));

        tree_for_test test = (tf->test() == FOR_ULTE) ? FOR_ULT :
	                     (tf->test() == FOR_SLTE) ? FOR_SLT :
                             (tf->test() == FOR_UGTE) ? FOR_UGT : FOR_SGT;
        tf->set_test(test);
        return TRUE;
    }

    else if (tf->test() == FOR_ULT || tf->test() == FOR_SLT || 
	     tf->test() == FOR_UGT || tf->test() == FOR_SGT)    
       return TRUE;

    return FALSE;
}

