/* file "reflist.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef REFLIST_H
#define REFLIST_H

#include "reduction.h"

DECLARE_LIST_CLASSES(reflist_base, reflist_e, reflist_iter, reference *);

class reflist : public reflist_base {
  public:
    reflist() {}
    ~reflist() {}
    reduction_types reduction_type();
    reference *find(operand &o);
    boolean partition();
    boolean same_ref(reference *pivot);
    void purge();
};

#endif
