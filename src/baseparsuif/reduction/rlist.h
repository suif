/* file "rlist.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef RLIST_H
#define RLIST_H

#include "reduction.h"

/* BEGIN rlist : glist Declaration */

class rlist_e: public glist_e {                            
public:                                       
    reference *contents;                                
    rlist_e(reference* t) { contents = t; }                         
    rlist_e *next() { return (rlist_e*)glist_e::next(); }              
//    EXTRA                                     
};

class rlist : public glist
{
  protected:                                    
    virtual void set_elem(rlist_e *e) { }                     
    void set_elems(rlist *l)                              
    { rlist_e *i = l->head(); while (i) { set_elem(i); i = i->next(); } } 
  public:
    rlist_e *head() { return (rlist_e*)glist::head(); }              
    rlist_e *tail() { return (rlist_e*)glist::tail(); }              
    reference* push(reference* t) { return push(new rlist_e(t))->contents; }
	rlist_e *push(rlist_e *e)                             
    { set_elem(e); return (rlist_e*)glist::push(e); }            
    reference* pop() { rlist_e *e = (rlist_e *)glist::pop();           
         reference* t = e->contents; delete e; return t; }              
    reference* append(reference* t) { return append(new rlist_e(t))->contents; }          
    rlist_e *append(rlist_e *e)                           
    { set_elem(e); return (rlist_e*)glist::append(e); }          
    reference* insert_before(reference* t, rlist_e *pos)                      
    { return insert_before(new rlist_e(t), pos)->contents; }          
    rlist_e *insert_before(rlist_e *e, rlist_e *pos)                  
    { set_elem(e); return (rlist_e*)glist::insert_before(e, pos); }  
    reference* insert_after(reference* t, rlist_e *pos)                   
    { return insert_after(new rlist_e(t), pos)->contents; }           
    rlist_e *insert_after(rlist_e *e, rlist_e *pos)               
    { set_elem(e); return (rlist_e*)glist::insert_after(e, pos); }   
    rlist_e *remove(rlist_e *e) { return (rlist_e*)glist::remove(e); }
    void copy(rlist *l) {                             
    rlist_e *i = l->head();                           
    while (i) { append(i->contents); i = i->next(); } }           
    void grab_from(rlist *l) { set_elems(l); glist::grab_from(l); }      
    void push(rlist *l) { set_elems(l); glist::push(l); }            
    void append(rlist *l) { set_elems(l); glist::append(l); }        
    void insert_before(rlist *l, rlist_e *pos)                    
    { set_elems(l); glist::insert_before(l, pos); }              
    void insert_after(rlist *l, rlist_e *pos)                     
    { set_elems(l); glist::insert_after(l, pos); }           
    reference* operator[](int ndx)                              
    { return ((rlist_e*)glist::operator[](ndx))->contents; }         
    rlist_e *lookup(reference* t) {                             
    rlist_e *i = head();                              
    while (i) { if (i->contents == t) break; i = i->next(); }         
    return i; }                               

//    EXTRA
	boolean partition();

/* Auxiliary Search Methods */
//	reference *search(tree_instr *ti);
	reference *search(reference *r);
	reference *partial_search(reference *r);
	reference *very_partial_search(reference* r);
	reference *search(operand &o);
	reference *partial_search(operand &o);
	reference *very_partial_search(operand &o);
	rlist_e *very_partial_search1(operand &o);

/* Auxiliary Methods */
	void purge();
};

class rlist_iter: public glist_iter {                          
public:                                       
    rlist_iter() : glist_iter() { }                        
    rlist_iter(rlist *l) : glist_iter(l) { }                   
    reference* step() { return ((rlist_e*)glist_iter::step())->contents; }
    reference* peek() { assert_msg(!is_empty(),                     
                 ("%s_iter::peek - no next element", "rlist"));    
          return ((rlist_e*)glist_iter::peek())->contents; }       
    rlist_e *cur_elem() { return (rlist_e*)glist_iter::cur_elem(); }       
};

/* END rlist : glist Declaration */

#endif
