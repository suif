/* file "misc.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"

boolean impure(instruction *ins);
type_node *elem_type(instruction *in);
var_sym *get_sym(operand o, int *error);
sym_node *get_proc_sym_of_cal(instruction *i);
sym_node *get_proc_sym_of_cal(in_cal *ic);
annote *search_annote(annote_list *al, const char *k_, int loc, sym_node *snode);
boolean found(var_sym *sym, immed_list *il);
boolean found(int j, immed_list *il);

boolean impure(instruction *ins)
{
    if (ins->opcode() == io_cal) {
        in_cal *ic = (in_cal *) ins;

        operand addr = ic->addr_op();
        if(!addr.is_instr()) {
	  return TRUE; // Can't figure out
	}
        instruction *ins = addr.instr();

        if (ins->opcode() != io_ldc) {
            return TRUE;  // Can't figure it out
        } else {
            in_ldc *ld = (in_ldc *) ins;
            immed im = ld->value();
            assert(im.is_symbol());

            sym_node *sn = im.symbol();

            if (sn->peek_annote(k_pure_function)) {
                return FALSE;
            } else {
                if (option['d'] >= 2) {
		    printf("Found impure call to %s",sn->name());
		    fflush(stdout);
		}
                return TRUE;
            }
        }
    } else {
        return FALSE;
    }
}

type_node *elem_type(instruction *in)
{
    while (in->opcode() == io_cpy) {
	operand src = ((in_rrr*)in)->src1_op();
	if (src.is_symbol()) {
	    return src.symbol()->type();
	} else {
	    in = src.instr();
	}
    }
    if (in->opcode() != io_array) {
	return type_void;
    } else {
	return ((in_array*)in)->elem_type();
    }
}

var_sym *get_sym(operand o, int *too_messy)
{
    instruction *i;
    var_sym *sym;

    *too_messy = 0;
    switch (o.kind()) {
      case OPER_SYM:
        return o.symbol();
      case OPER_INSTR:
        i = o.instr();
	while (i->opcode() == io_cpy || i->opcode() == io_cvt) {
	    operand op = ((in_rrr*)i)->src1_op();
	    if (op.is_symbol()) return op.symbol();
	    else i = op.instr();
	}
        if (i->opcode() == io_array) {
	    sym = get_sym_of_array((in_array*)i);;
	    if (!sym) *too_messy = 1;
	    return sym;
	} else return NULL;
      default:
        assert_msg(1, ("get_sym() sees OPER_NULL"));
    }
    return NULL;
}

sym_node *get_proc_sym_of_cal(instruction *i)
{
    if (i->format() == inf_cal) {
	return get_proc_sym_of_cal((in_cal*)i);
    } else {
	return NULL;
    }
}
	
sym_node *get_proc_sym_of_cal(in_cal *ic)
{
    return proc_for_call(ic);
}

annote *search_annote(annote_list *al, const char *k_, int loc, sym_node *snode)
{
    annote_list_iter ali(al);
    while (!ali.is_empty()) {
        annote *an = ali.step();

        if (an->name() == k_) {
	    sym_node *sn = (*(an->immeds()))[loc].symbol();
	    if (sn == snode) return an;
	}
    }
    return NULL;
}

boolean found(var_sym *sym, immed_list *il)
{
    immed_list_iter ili(il);
    while (!ili.is_empty()) {
        immed im = ili.step();
        if (im.is_symbol() && im.symbol() == sym) return TRUE;
    }
    return FALSE;
}

boolean found(int j, immed_list *il)
{
    immed_list_iter ili(il);
    while (!ili.is_empty()) {
        immed im = ili.step();
        if (im.is_integer() && im.integer() == j) return TRUE;
    }
    return FALSE;
}
