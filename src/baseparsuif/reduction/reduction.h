/* file "reduction.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef REDUCTION_H
#define REDUCTION_H

#include <suif1.h>
#include <suifmath.h>
#include <dependence.h>
#include <useful.h>
#include <builder.h>
#include "reference.h"
#include "reflist.h"
#include "summary.h"
#include "nest.h"

/* driver.cc */
extern int option[256];
extern const char *k_reduction;
extern const char *k_reduction_gen;
extern const char *k_call_mod_syms;
extern const char *k_call_ref_syms;
extern const char *k_associativity;
extern const char *k_write_ref;
extern const char *k_pure_function;  // Defined in useful.cc

/* nest.cc */
// Reduction_types for associative functions are numbered from 
// REDUCTION_GEN to MAX_REDUCTION_TYPE
//
#define MAX_REDUCTION_TYPE	100
extern int proc_assoc_type;
extern reflist * assoc_refs[MAX_REDUCTION_TYPE];

/* region.cc */
extern boolean equiv(operand o1, operand o2);
extern boolean equiv(instruction *i1, instruction *i2);
extern boolean intersect(instruction *i1, instruction *i2);

extern region_types region_type(instruction *i);
extern region_types region_type(in_array *ia);
extern boolean is_modified(in_array *ia);

/* misc.cc */
extern boolean impure(instruction *in);
extern type_node *elem_type(instruction *in);
extern var_sym *get_sym(operand o, int *too_messy);
extern sym_node *get_proc_sym_of_cal(instruction *i);
extern sym_node *get_proc_sym_of_cal(in_cal *ic);
extern annote *search_annote(annote_list *al, const char *k_, int loc, sym_node *snode);
extern boolean found(var_sym *sym, immed_list *il);
extern boolean found(int j, immed_list *il);

#endif



