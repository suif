/* file "nest.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"

nest::nest(tree_for *tf, nest *par)
{
    info = new summaryFor(tf);
    parent = par;
    children = new nest_list();
    abort = FALSE;
}

void nest::for_map(tree_node_list *body)
{
    tree_node_list_iter tnli(body);
    while (!tnli.is_empty()) {
        tree_node *t = tnli.step();

        switch (t->kind()) {
          case TREE_FOR: {
	    tree_for *tf = (tree_for *) t;
	    nest *inner_nest = new nest(tf, this);
	    children->append(inner_nest);
	    inner_nest->for_map(tf->body());  // Build children
            break;
          }

          case TREE_BLOCK:
            for_map(((tree_block*)t)->body());
            break;

          case TREE_IF:
            for_map(((tree_if*)t)->header());
            for_map(((tree_if*)t)->then_part());
            for_map(((tree_if*)t)->else_part());
            break;

          case TREE_LOOP:
            for_map(((tree_loop*)t)->body());
            for_map(((tree_loop*)t)->test());
            break;

          case TREE_INSTR:
            break;
        }
    }
}    

// Depth first search
//
void nest::dfs_map()
{
    // For every tree_for in the children: recursively calls dfs_map()
    //
    nest_list_iter iter(children);
    while (!iter.is_empty()) { 
        nest *inner_nest = iter.step();
	inner_nest->dfs_map();
	if (!abort) {
	    this->propagate(inner_nest);
	}
    }

    if (option['d'] >= 2) {
	printf("Loop %s\n\n", info->tf->index()->name());
	fflush(stdout);
    }

    if (abort) {
	if (option['d'] >= 2) {
	    printf("Bailing.\n");
	}
    } else if (info->map()) {  // PROCESS THIS TREE_FOR
	this->abort_path();
    }

    if (option['d'] >= 2) {
	printf("\nEnd Loop %s\n-------------------\n", info->tf->index()->name());
	fflush(stdout);
    }
}

void nest::propagate(nest *inner_nest)
{
    if (info->propagate(inner_nest->info)) {
	this->abort_path();
    }
    delete inner_nest->info;
    inner_nest->info = NULL;
}

void nest::abort_path()
{
    nest *outer_nest;
    for (outer_nest = this; outer_nest; outer_nest = outer_nest->parent) {
	outer_nest->abort = TRUE;
    }
}

summaryFor *nest::lookup(tree_for *tf)
{
    if (info->tf == tf) {
	return info;
    } else {
	nest_list_iter iter(children);
	while (!iter.is_empty()) {
	    nest *inner_nest = iter.step();
	    summaryFor *sum;
	    if ((sum = inner_nest->lookup(tf))) {
		return sum;
	    }
	}
    }
    return NULL;
}	

// Garbage Collection
//
nest::~nest()
{
    // delete children
    nest_list_iter iter(children);
    while (!iter.is_empty()) {
	nest *inner_nest = iter.step();
	delete inner_nest;
    }

    // delete itself
    delete info;
}
