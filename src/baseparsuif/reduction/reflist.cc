/* file "reflist.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"

// class reflist ********************************************************
//
// public methods
//

reduction_types reflist::reduction_type()
{
    assert(!is_empty());

    reflist_iter refIter(this);
    reference *ref = refIter.step();
    reduction_types rtype = ref->reduction_type();

    while (!refIter.is_empty()) {
	ref = refIter.step();
	if (rtype != ref->reduction_type()) {
	    if (option['d'] >= 2) {
		printf("Irreducible (two writes having incompatible reduction types): ");
		int too_messy;
		get_sym(ref->write, &too_messy)->print();
		printf("\n");
		fflush(stdout);
	    }
	    return REDUCTION_NONE;
	}
    }
    if (rtype == REDUCTION_UNKNOWN) {
	rtype = REDUCTION_NONE;
	if (option['d'] >= 2) {
	    printf("Irreducible (Only writes): ");
	    int too_messy;
	    get_sym(ref->write, &too_messy)->print();
	    printf("\n");
	    fflush(stdout);
	}
    }
    return rtype;
}

reference *reflist::find(operand &o)
{
    if (o.is_null()) return NULL;
    
    reflist_iter iter(this);
    while (!iter.is_empty()) {
        reference *ref = iter.step();
	operand w = ref->write;
	while (w.kind() == OPER_INSTR) {
	    instruction *in = w.instr();
	    if (in->opcode() == io_cpy || in->opcode() == io_cvt) {
		w = ((in_rrr*)in)->src1_op();
	    } else break;
	}

        if (w.kind() == o.kind()) {
            if (o.is_symbol()) {
                if (o.symbol() == w.symbol()) return ref;
            } else {
		assert(o.is_instr());
                if (equiv(o.instr(), w.instr())) return ref;
            }
        }
    }
    return NULL;
}

// partition reduction references of REGION_SINGLE region_type
//
boolean reflist::partition()
{
    reflist_e *pivotE, *refE;
    reference *pivot, *ref;

    pivotE = head();
    while (pivotE) {
        pivot = pivotE->contents;
        refE = pivotE->next();
        while (refE) {
            ref = refE->contents;
	    if (ref->region_type() != REGION_SINGLE) return FALSE;
	    
            if (pivot->equiv(ref)) {
                if (pivot->promoted_to == NULL) {  // initialize promoted_to
		    proc_symtab *procScope =
			pivot->i_write->parent()->proc()->block()->proc_syms();
		    pivot->promoted_to = procScope->new_unique_var
			(elem_type(pivot->write.instr()), "__red_");
                }
                ref->promoted_to = pivot->promoted_to;
            } else if (intersect(pivot->write.instr(), ref->write.instr())) {
                return FALSE;
            }
            refE = refE->next();
        }
        pivotE = pivotE->next();
    }
    
    /* Initialize ref->promoted_to */
    refE = pivotE;
    while (refE) {
        ref = refE->contents;
        if (!ref->promoted_to) {
	    proc_symtab *procScope =
		pivot->i_write->parent()->proc()->block()->proc_syms();
	    pivot->promoted_to = procScope->new_unique_var
		(elem_type(pivot->write.instr()), "__red_");
        }
        refE = refE->next();
    }
    return TRUE;
}

boolean reflist::same_ref(reference *pivot)
{
    reflist_iter refIter(this);
    refIter.step();
    while (!refIter.is_empty()) {
	reference *ref = refIter.step();
	if (!pivot->equiv(ref)) {
	    return FALSE;
	}
    }
    return TRUE;
}

// purge references
//
void reflist::purge()
{
    reflist_iter iter(this);
    while (!iter.is_empty()) {
        reference *ref = iter.step();
	delete ref;
    }
}

// class reflist ********************************************************
//
// private methods
//

