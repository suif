/* file "nest.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef NEST_H
#define NEST_H

#include "reduction.h"

class nest;

DECLARE_LIST_CLASS(nest_list, nest *);

class nest {
  public:
    nest(tree_for *tf, nest *par = NULL);
    void for_map(tree_node_list *body);
    void dfs_map();
    void propagate(nest *inner_nest);
    summaryFor *lookup(tree_for *tf);
    ~nest();
    
    summaryFor *info;

  private:
    void abort_path();

    nest_list *children;
    nest *parent;
    boolean abort;
};   

#endif

