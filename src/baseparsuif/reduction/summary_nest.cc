/* file "summary_nest.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"

loop::loop(tree_for *tf, loop *par = NULL)
{
    key = tf;
    info = new summary();
    parent = par;
    children = new summary_nest();
    abort = 0;
}

void loop::for_map(tree_node_list *body)
{
    tree_node_list_iter tnli(body);
    while (!tnli.is_empty()) {
        tree_node *t = tnli.step();

        switch (t->kind()) {
          case TREE_FOR:
	    tree_for *tf = (tree_for *) t;
	    loop *inner_loop = new loop(tf, this);
	    children->append(inner_loop);
	    inner_loop->for_map(tf->body());  // Build summary_nest *children
            break;
            
          case TREE_BLOCK:
            for_map(((tree_block*)t)->body());
            break;
            
          case TREE_IF:
            for_map(((tree_if*)t)->header());
            for_map(((tree_if*)t)->then_part());
            for_map(((tree_if*)t)->else_part());
            break;
            
          case TREE_LOOP:
            for_map(((tree_loop*)t)->body());
            for_map(((tree_loop*)t)->test());
            break;

          case TREE_INSTR:
            break;
        }
    }
}    

// Depth first search
//
void loop::dfs()
{
    // For every tree_for in the children: recursively calls dfs()
    //
    summary_nest_iter titer(children);
    while (!titer.is_empty()) { 
        loop *inner_loop = titer.step();
	inner_loop->dfs();
    }

    // Process this loop
    //
    if (tdebug) {
	printf("-------------------\n");
	printf("Loop %s\n\n", key->index()->name());
	fflush(stdout);
    }

    if (abort) {
	if (tdebug) {
	    printf("abort\n");
	    fflush(stdout);
	}
	return;
    }
    
    /* Pass 1: Record all writes */
    if (info->dst_map(key->body(), this)) {
	this->abort_path();
    }
        
    /* Pass 2: Map all src operands */
    if (info->src_map(key->lb_list()) || info->src_map(key->ub_list()) ||
	info->src_map(key->step_list()) || info->src_map(key->landing_pad()) ||
	info->src_map(key->body())) {
	this->abort_path();
    }

    /* Pass 3: Merging */
    info->merge();
    info->check_modref_calls();
    info->reduced_area();
        
    /* Pass 4: Scalarize and Put annotations */
    info->scalarize_and_annote(key);

    if (tdebug) {
	printf("\nEnd Loop %s\n-------------------\n", key->index()->name());
	fflush(stdout);
    }
}

void loop::abort_path()
{
    loop *outer_loop;
    for (outer_loop = this; outer_loop; outer_loop = outer_loop->parent) {
	outer_loop->abort = 1;
    }
}

summary *loop::lookup(tree_for *tf)
{
    if (key == tf) {
	return info;
    } else {
	summary_nest_iter titer(children);
	while (!titer.is_empty()) {
	    loop *inner_loop = titer.step();
	    summary *sum;
	    if (sum = inner_loop->lookup(tf)) return sum;
	}
    }
    return NULL;
}	

// Garbage Collection
//
loop::~loop()
{
    // delete children
    summary_nest_iter titer(children);
    while (!titer.is_empty()) {
	loop *inner_loop = titer.step();
	delete inner_loop;
    }

    // delete itself
    delete info->ti_ref_cache;
    delete info->calls;

    for (summary_e *sum_e = info->head(); sum_e; sum_e = sum_e->next()) {
	if (sum_e->info) {
	    sum_e->info->purge();
	    delete sum_e->info;
	}
    }
    delete info;
}
