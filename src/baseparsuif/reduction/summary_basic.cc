static int assoc(instruction *i, int a_type);
static int find(int j, immed_list *il);

/* ti_ref */
ti_ref_e *ti_ref::search (tree_instr *k)
{
    ti_ref_e *ap = head();
    while (ap) {
        if (ap->key == k)
            return ap;
        ap = ap->next();
    }
    return NULL;
}

reference *ti_ref::lookup (tree_instr *k)
{
    ti_ref_e *ap = search(k);
    assert_msg(ap, ("ti_ref::lookup - attempt to lookup %x failed", k));
    return ap->info;
}

/*
 *  Check if the list contains the specified key.  If so, optionally
 *  return the associated info pointer through the second argument.
 */
boolean ti_ref::exists(tree_instr *k, reference **i)
{
    ti_ref_e *ap = search(k);
    if (ap) {
        if (i) *i = ap->info;
        return TRUE;
    }
    return FALSE;
}

int summary::check_in_write(operand &write, tree_instr *ti, reference **refer)
{
    reference *ref;
    rlist *r;
    summary_e *sum_e;
    var_sym *vsym;
    int too_messy;

    *refer = NULL;
    if (vsym = get_sym(write, &too_messy)) {
        if (sum_e = search(vsym)) {
            if (sum_e->reduction_type) {
                /* Note that our definition of reductions does not require
                   that write be distinct to the found reducible references */
                ref = new reference(write, ti->instr());
//              ti->prepend_annote(k_write_ref, ref);
                ti_ref_cache->append(new ti_ref_e(ti, ref, vsym));
                sum_e->info->push(ref);
		*refer = ref;
            }
        } else {
            r = new rlist;
            ref = new reference(write, ti->instr());
//          ti->prepend_annote(k_write_ref, ref);
            ti_ref_cache->append(new ti_ref_e(ti, ref, vsym));
            r->push(ref);
            push(new summary_e(vsym, r));
	    *refer = ref;
        }
    } else {
	if (too_messy) {  // treat all sum_e's in summary as irreducible
	    return 1;
	}
    }
    return 0;
}

// Recursive function for each inner for.
// We don't find read-modify-write reductions that span over block boundaries
int summary::src_map(tree_node_list *body)
{
    tree_node_list_iter tnli(body);
    while (!tnli.is_empty()) {
        tree_node *t = tnli.step();

        switch (t->kind()) {
          case TREE_FOR:
            break;
            
          case TREE_BLOCK:
            if (src_map(((tree_block*)t)->body())) return 1;
            break;
            
          case TREE_IF:
            if (src_map(((tree_if*)t)->header())) return 1;
            if (src_map(((tree_if*)t)->then_part())) return 1;
            if (src_map(((tree_if*)t)->else_part())) return 1;
            break;
            
          case TREE_LOOP:
            if (src_map(((tree_loop*)t)->body())) return 1;
            if (src_map(((tree_loop*)t)->test())) return 1;
            break;

          case TREE_INSTR:
            if (src_map((tree_instr *)t)) return 1; 
            break;
        }
    }
    return 0; 
}

int summary::src_map(tree_instr *ti)
{
    instruction *i;
    var_sym *dst_sym;
    
//  reference *dst = (reference *)ti->get_annote(k_write_ref);
    ti_ref_e *tmp;
    reference *dst;
    if (tmp = ti_ref_cache->search(ti)) {
	dst = tmp->info;
	dst_sym = tmp->sym;
    } else {
	dst = NULL;
	dst_sym = NULL;
    }

    /* map srcs in dst */
    if (dst && dst->write.kind() == OPER_INSTR) {
        i = dst->write.instr();
	// must skip the dst_sym in the left-hand side
        if (src_map(i, dst, assoc(i, dst->reduction_type), dst_sym)) return 1;
    }

    /* map rhs srcs */
    i = ti->instr();
    if (src_map(i, dst, assoc(i, -1), NULL)) return 1;
    return 0;
}

#define SRC_MAP(s) \
if (s.kind() != OPER_NULL) if (src_map(s, i, dst, a_type, dst_sym)) return 1

int summary::src_map(instruction *i, reference *dst, int a_type,
		     var_sym *dst_sym)
{
    operand src;
	int n;

    switch (i->format()) {
      case inf_rrr:
        in_rrr *ir = (in_rrr*)i;
        if (i->opcode() == io_str) {
            // must ignore dst_addr_op
            src = ir->src_op();
            SRC_MAP(src);
        } else if (i->opcode() == io_memcpy || i->opcode() == io_lod) {
            // must ignore dst_addr_op
            src = ir->src_addr_op();
            SRC_MAP(src);
        } else {
            src = ir->src1_op();
            SRC_MAP(src);
            if (i->opcode() == io_sub || i->opcode() == io_div) {
                /* src_op(1) is the last operand in io_sub and io_div.
                   It is not associative on that branch (conservative here) */
                a_type = 0;
            }
            src = ir->src2_op();
            SRC_MAP(src);
	}
        break;
        
      case inf_bj:
        in_bj *ib = (in_bj*)i;
        src = ib->src_op();
        SRC_MAP(src);
        break;

      case inf_mbr:
        in_mbr *im = (in_mbr*)i;
        src = im->src_op();
        SRC_MAP(src);
        break;
        
      case inf_cal:
	in_cal *ic = (in_cal*)i;
	sym_node *sn;
	if (sn = get_proc_sym_of_cal(ic)) {
	    void *data = sn->peek_annote(k_associativity);
	    if (data) {
		if (src_map_gen(ic, dst, dst->reduction_type, data, dst_sym)) {
		    return 1;
		}
		break;
	    }
	}

        if (IMPURE(i)) {
            PROCESS_IMPURE(i)
        } else {
            in_cal *ic = (in_cal*)i;
            operand addr_op();
            for (n = 0; n < ic->num_args(); n++) {
                src = ic->argument(n);
                SRC_MAP(src);
            }
        }
        break;

      case inf_ldc:
      case inf_lab:
        break;
        
      case inf_array:
//        for (n = 0; n < ((in_array*)i)->dims(); n++) {
//            src = i->src_op(n+2);
//            SRC_MAP(src);
//        }
        for (n = 0; n < i->num_srcs(); n++) {
            src = i->src_op(n);
            SRC_MAP(src);
        }
        break;

      default:
        for (n = 0; n < i->num_srcs(); n++) {
            src = i->src_op(n);
            SRC_MAP(src);
        }
        break;
    }
    return 0;
}

int summary::src_map_gen(in_cal *i, reference *dst, int a_type, void *data,
			 var_sym *dst_sym)
{
    int j;

    immed_list *il = (immed_list*)data;
    immed_list_iter ili(il);
    assert(! (il->count()%3) );
    for (j = 0; j < il->count()/3; j++) {
        operand opnd;
        
	// Final/Init operands: cannot be read/written elsewhere
	immed im = ili.step();
	if (im.is_integer()) {
	    im = ili.step();  // recursively src_map the init opnd
	    opnd = i->argument(im.integer());
            if (opnd.kind() == OPER_INSTR) {
                instruction *in = opnd.instr();
                if (src_map(in, dst, assoc(in, a_type), dst_sym))
                    return 1;
            }
	} else {
	    assert(im.is_string() && !strcmp(im.string(), "return value"));
	    im = ili.step();  // recursively src_mapped already
	}

        // Scanned operands: cannot be written elsewhere
	im = ili.step();
	assert(im.is_integer());
	SRC_MAP(i->argument(im.integer())); 
    }

    // Intermediate operands: cannot be written elsewhere
    il = (immed_list*)data;
    for (j = 0; j < i->num_args(); j++) {
	if (!find(j, il)) {  // j not in il => argument(j) is intermediate opnd
	    SRC_MAP(i->argument(j));
	}
    }
    return 0;
}

int summary::src_map(operand &src, instruction *i, reference *dst, int a_type,
		     var_sym *dst_sym)
{
    rlist_e *rl_e;
    reference *ref, *lhs; // matched left-hand side reference
    var_sym *vsym;
    summary_e *sum_e;
    int too_messy;
    
    if (vsym = get_sym(src, &too_messy)) {
	if (vsym != dst_sym) {
        if (sum_e = this->search(vsym)) {
            if (a_type >= GEN_REDUCTION_TYPE &&
                sum_e->info /* vsym was accessed before */) {
                // Dependence on intermediate opnds found:
                // Kill the associative functions as reduction.
                // We do this for skweel becaus skweel ignores all
                // the references in the associative functions
		//
                if (assoc_refs[a_type])
                    delete assoc_refs[a_type];
                assoc_refs[a_type] = 0;
            }
            if (sum_e->reduction_type) {
            if (lhs = sum_e->info->search(src)) {  // Exact match
		if (lhs == dst || equiv(lhs, dst)) {
		    dst->check_in_read(i, src, a_type);
		    return 0;
                } else {
		    if (tdebug) {
			printf("Irreducible (writes and reads across exp-trees): ");
			sum_e->key->print(stdout);
			printf("\n");
			fflush(stdout);
		    }
                    sum_e->reduction_type = 0;
                }
            } else {
		if (src.is_symbol()) {
		    if (tdebug) {
			printf("Irreducible scalar (writes and reads mismatched, shouldn't happen!): ");
			sum_e->key->print(stdout);
			printf("\n");
			fflush(stdout);
		    }
		    sum_e->reduction_type = 0;
                } else {  // src must be distinct to all writes
                    for (rl_e = sum_e->info->head(); rl_e; rl_e=rl_e->next()) {
                        ref = rl_e->contents;
                        if (ref->m == -5) ref->m = m_value(ref->write);
                        if (ref->m) break;
                        if (intersect(src, ref->write)) break;
                    }
                    if (rl_e) {
			if (tdebug) {
			    printf("Irreducible array (reads are not distinct to all writes): ");
			    sum_e->key->print(stdout);
			    printf("\n");
			    fflush(stdout);
			}
			sum_e->reduction_type = 0;
		    }
                }
            }
            }
        } else {  // Create a summary_e for src for future propagation
	    if (tdebug) {
		printf("Irreducible (only reads): ");
		vsym->print(stdout);
		printf("\n");
		fflush(stdout);
	    }
            push(new summary_e(vsym, NULL, 0));
        }
        }
    } else {
	if (too_messy) {  // treat all sum_e's in summary as irreducible
	    return 1;
	}
    }

    // Recursively trace down the src
    if (src.kind() == OPER_INSTR) {
        instruction *in = src.instr();
        if (src_map(in, dst, assoc(in, a_type), dst_sym))
            return 1;
    }
    return 0;
}

void summary::merge()
{
    summary_e *sum_e;
    rlist_e *rl_e;
    reference *ref;
    int redtype;
        
    for (sum_e = head(); sum_e; sum_e = sum_e->next()) {
        if (sum_e->reduction_type) {
            rl_e = sum_e->info->head();
            if (rl_e) {
                ref = rl_e->contents;
                redtype = ref->reduction_type;
                for (rl_e = rl_e->next(); rl_e; rl_e= rl_e->next()) {
                    ref = rl_e->contents;
                    if (redtype != ref->reduction_type) break;
                }
		if (redtype <= 0) {
		    if (tdebug) {
			printf("Irreducible (only one write): ");
			sum_e->key->print();
			printf("\n");
			fflush(stdout);
		    }
		    sum_e->reduction_type = 0;
		} else if (rl_e) {
		    if (tdebug) {
			printf("Irreducible (can't merge writes): ");
			sum_e->key->print();
			printf("\n");
			fflush(stdout);
		    }
		    sum_e->reduction_type = 0;
                } else {
		    sum_e->reduction_type = redtype;
		}
            } else {
                sum_e->reduction_type = 0;
            }
        }
    }
}

void summary::reduced_area()
{
    summary_e *sum_e;
    rlist_e *rl_e;
    reference *ref;
        
    for (sum_e = head(); sum_e; sum_e = sum_e->next()) {
        if (sum_e->reduction_type) {
            for (rl_e = sum_e->info->head(); rl_e; rl_e = rl_e->next()) {
                ref = rl_e->contents;
                if (ref->m == -5) ref->m = m_value(ref->write);
                if (ref->m < 0) {  // Full range reduction
                    break;
                }
            }
            if (!rl_e) {
                // no full range reduction: suppress if there is
                // subrange reduction or unknown m-value reduction
                for (rl_e = sum_e->info->head(); rl_e; rl_e = rl_e->next()) {
                    ref = rl_e->contents;
                    if (ref->m > 0) break;
                }
                if (rl_e) {  // SUPPRESSION
		    if (tdebug) {
			printf("Suppress sub-range reductions: ");
			sum_e->key->print();
			printf("\n");
			fflush(stdout);
		    }
                    sum_e->reduction_type = 0;
                } else {  // all m = 0: PARTITION
                    // PARTITION will make sure that references are either
                    // distinct or equivalent
                    if (!sum_e->info->partition()) {
			if (tdebug) {
			    printf("Irreducible array (Access ranges cannot be partitioned): ");
			    sum_e->key->print();
			    printf("\n");
			    fflush(stdout);
			}
			sum_e->reduction_type = 0;
		    }
                }
            }
        }
    }
}

summary_e *summary::search (var_sym *k)
{
    summary_e *ap = head();
    while (ap) {
        if (ap->key == k)
            return ap;
        ap = ap->next();
    }
    return NULL;
}

rlist *summary::lookup (var_sym *k)
{
    summary_e *ap = search(k);
    assert_msg(ap, ("summary::lookup - attempt to lookup %x failed", k));
    return ap->info;
}

/*
 *  Check if the list contains the specified key.  If so, optionally
 *  return the associated info pointer through the second argument.
 */
boolean summary::exists(var_sym *k, rlist **i)
{
    summary_e *ap = search(k);
    if (ap) {
        if (i) *i = ap->info;
        return TRUE;
    }
    return FALSE;
}

// a_type (internal argument):
//  -1: unknown
//  0: not associative
//  1: sum-type associativity
//  2: product-type associativity
//  3: min-type associativity
//  4: max-type associativity
//
static int assoc(instruction *i, int a_type)
{
    if (a_type == 0 || a_type >= GEN_REDUCTION_TYPE) return a_type;
    switch (i->opcode()) {
      case io_add:
        if (a_type == -1 || a_type == 1) return 1; else return 0;
        break;
      case io_sub:
        if (a_type == -1 || a_type == 1) return 1; else return 0;
        break;
      case io_mul:
        if (a_type == -1 || a_type == 2) return 2; else return 0;
        break;
      case io_div:
        if (a_type == -1 || a_type == 2) return 2; else return 0;
        break;
      case io_min:
        if (a_type == -1 || a_type == 3) return 3; else return 0;
        break;
      case io_max:
        if (a_type == -1 || a_type == 4) return 4; else return 0;
        break;
      case io_neg: case io_rem: case io_mod:
      case io_not: case io_and: case io_ior: case io_xor:
      case io_abs: case io_asr: case io_lsl: case io_rot: case io_divfloor:
      case io_divceil: case io_array: case io_cal:
        return 0;
      default:
        return a_type;
    }
}

static int find(int j, immed_list *il)
{
    immed_list_iter ili(il);
    while (!ili.is_empty()) {
	immed im = ili.step();
	if (im.is_integer() && im.integer() == j) return 1;
    }
    return 0;
}
