/* file "reference.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef REFERENCE_H
#define REFERENCE_H

#include "reduction.h"

enum reduction_types {
    REDUCTION_UNKNOWN = -1,
    REDUCTION_NONE = 0,
    REDUCTION_SUM = 1,
    REDUCTION_PRODUCT = 2,
    REDUCTION_MIN = 3,
    REDUCTION_MAX = 4,
    REDUCTION_GEN = 5
};

enum region_types {
    REGION_UNKNOWN,
    REGION_NONE,
    REGION_SCALAR,
    REGION_FULL,
    REGION_SUB,
    REGION_SINGLE,
    REGION_MESSY
};

class reflist;

// Reference objects are associated with every write and those reads whose
// var_sym's are not written anywhere.
// Methods:
//  check_in_read(): check in a read operand.
//  promote(): array promotion of reduction variables.
//  promoted_to(): promoted to this symbol
//  annotate(): inserting reduction annotations.
//  region_type()
//  reduction_type()
//  reducible()
//  reducible_sum()
//  reducible_product()
//  reducible_min()
//  reducible_max()
//  reducible_gen()
//
class reference
{
    friend class reflist;  // friend boolean reflist::partition();
  public:
    reference(operand w, instruction *iw, int nr = 0,
	      reduction_types redt = REDUCTION_UNKNOWN,
	      region_types regt = REGION_UNKNOWN, var_sym *s = NULL);
    ~reference();
    void check_in_read(instruction* i, operand src, reduction_types a_type);
    var_sym *promote(tree_for *tf);
    void generate(tree_for *tf, var_sym *vsym = NULL);

    boolean equiv(reference *ref);
    region_types region_type();
    inline void region_type(region_types x) { regtype = x; }

    inline reduction_types reduction_type() const { return redtype; }
    inline void reduction_type(reduction_types x) { redtype = x; }
    inline boolean reducible() const { return redtype > 0; }
    inline boolean reducible_sum() const { return redtype == REDUCTION_SUM; }
    inline boolean reducible_product() const { return redtype == REDUCTION_PRODUCT; }
    inline boolean reducible_min() const { return redtype == REDUCTION_MIN; }
    inline boolean reducible_max() const { return redtype == REDUCTION_MAX; }
    inline boolean reducible_gen() const { return redtype >= REDUCTION_GEN; }
    
    operand write, read;
    instruction *i_write, *i_read;
    int num_read;

  private:
    void annotate(tree_for *tf, const char *k_, immed_list *iml, int loc, sym_node *snode);

    reduction_types redtype;
    region_types regtype;
    var_sym *promoted_to;
};

#endif  
