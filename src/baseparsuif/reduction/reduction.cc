/* file "reduction.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"

extern int verbosity_level;
extern boolean flag_debug;
void reduction_init();

/* Reduction recognition */

const char *k_reduction;
const char *k_pure_function;

void reduction_init()
{
	k_pure_function = lexicon->enter("pure function")->sp;
	ANNOTE(k_reduction, "reduction", TRUE /* external annotation */);
}

reduction_info::~reduction_info()
{
}

/* For every tree_for: */
		/* Pass 1: Record all writes */
		/* Pass 2: Map all src operands */
		/* Pass 3: Merge reflist into loopres */
		/* Pass 4: Propagate up and put annotations */

/* Build reduction_info for a for-nest */
reduction_info::reduction_info(tree_for *tf)
{
	rlist *loopres;
  
	/* Pass 0: Build tfors */
	tfors.push(tf);
	scan_for(tf->body());
	
	/* Main Task */
	tflist_iter titer(&tfors);
	while (!titer.is_empty()) { // for every tree_for
		tree_for *tf = titer.step();

		/* Pass 1: Record all writes */
		reflist = new rlist;
		reflist->scan_write(tf, tf->body());

		/* Pass 2: Map all src operands */
		rec_for_enter(tf); 		/* Enters for */

		/* Assume that the landing pad is empty */
		int error = rec_block(tf->body());

		rec_for_exit();		/* Exits for */

		if (error) { // means call seen
			delete reflist;
			continue;
		}

		/* Pass 3: Merge reflist into loopres */
		reference *ref, *tmpref;
		loopres = new rlist;
		rlist_iter riter(reflist);
		while (!riter.is_empty()) { 
			ref = riter.step();
			if (tmpref = loopres->very_partial_search(ref)) {
				if (tmpref->reduction_type != ref->reduction_type) {
					tmpref->reduction_type = 0;
				} else {
					loopres->push(ref);
				}
			} else {
				loopres->push(ref);
			}
		}

		/* Pass 4: Propagate up and put annotations */
		riter.reset(loopres);
		while (!riter.is_empty()) { 
			ref = riter.step();
			ref->annotes(k_reduction, tf);
		}
		delete reflist;
		delete loopres;
	} // while loop
}



void reduction_info::scan_for(tree_node_list *body)
{
	tree_node_list_iter tnli(body);
	while (!tnli.is_empty()) {
		tree_node *t = tnli.step();

		switch (t->kind()) {
		  case TREE_FOR:
			tfors.push((tree_for*)t);
			scan_for(((tree_for*)t)->body());
			break;
			
		  case TREE_BLOCK:
			scan_for(((tree_block*)t)->body());
			break;
			
		  case TREE_IF:
			scan_for(((tree_if*)t)->header());
			scan_for(((tree_if*)t)->then_part());
			scan_for(((tree_if*)t)->else_part());
			break;
			
		  case TREE_LOOP:
			scan_for(((tree_loop*)t)->body());
			scan_for(((tree_loop*)t)->test());
			break;

		  case TREE_INSTR:
			break;
		}
	}
}
	
/* Recursive function for each inner for */
/* ASSUMPTION: read-modify-write reductions do not span
   over blocks and procedures */
int reduction_info::rec_block(tree_node_list *body)
{
	tree_node_list_iter tnli(body);
	while (!tnli.is_empty()) {
		tree_node *t = tnli.step();

		switch (t->kind()) {
		  case TREE_FOR:
			tree_for *tf = (tree_for *)t;
			rec_for_enter(tf);			/* Enters for */

			/* Assume that the landing pad is empty */
			rec_block(tf->body());
			
			rec_for_exit();			/* Exits for */
			break;
			
		  case TREE_BLOCK:
			if (rec_block(((tree_block*)t)->body())) return 1;
			break;
			
		  case TREE_IF:
			if (rec_block(((tree_if*)t)->header())) return 1;
			if (rec_block(((tree_if*)t)->then_part())) return 1;
			if (rec_block(((tree_if*)t)->else_part())) return 1;
			break;
			
		  case TREE_LOOP:
			/* In src_map() we check lhs->chainable to make sure that
			   the reduction chain does not go across basic block boundaries:
			   As a result, no need to do rec_for_enter() nor rec_for_exit() */
			if (rec_block(((tree_loop*)t)->body())) return 1;
			if (rec_block(((tree_loop*)t)->test())) return 1;
			break;

		  case TREE_INSTR:
			/* if (error) return 1; */
			if (reflist->rec_instr((tree_instr *)t)) return 1; 
			break;
		}
	}
	return 0; 
}

void reduction_info::rec_for_enter(tree_for *tf)
{
	var_sym *index;
	if (index = tf->index())
		reflist->src_map(operand(index), NULL, NULL, -1);
	operand opnd = tf->lb_op();
	if (opnd.kind() != OPER_NULL)
		reflist->src_map(opnd, NULL, NULL, -1);
	opnd = tf->ub_op();
	if (opnd.kind() != OPER_NULL)
		reflist->src_map(opnd, NULL, NULL, -1);
	opnd = tf->step_op();
	if (opnd.kind() != OPER_NULL)
		reflist->src_map(opnd, NULL, NULL, -1);
}

void reduction_info::rec_for_exit()
{
/*	rlist_iter iter(reflist);

	while (!iter.is_empty()) {
		reference *ref = iter.step();
		if (ref->reduction_type <= 0) { // IRREDUCIBLE
			ref->reduction_type = 0;
		}
	}*/
}
