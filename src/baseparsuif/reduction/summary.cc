/* file "summary.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"
#include <annotes.h>
#include <string.h>

// for pointer analysis
// Algorithm to incorporate pointer analysis:
// 1. Record wilbyr_annotes_lists in the Pass 1 and 2
//    (a) In Pass 1, record wilbyr_store_to of the dest addr.
//    (b) In Pass 2, record wilbyr_load_from of the src's. Record the
//         wilbyr_array_base in "src_map" array operands.
// 2. In Pass 3, if s is reducible, then go through the summaryFor and 
//    make sure s->annote_list doesn't conflict with 
//     (a) summaryFor->wilbyr_al and
//     (b) other summarySym->wilbyr_al's
//
enum alias_kind {
    INDEPENDENT,
    SAME_BASE,
    ALIASED_WITH_OFFSET,
    ALIASED_NO_OFFSET
};

static inline boolean is_impure(instruction *i);
static int process_impure(instruction *i, summaryFor *sumF);
static int assoc(instruction *i, int aType);
static operand get_instr_dst_wilbyr(instruction *i, summaryFor *sfor);
static operand get_instr_dst(instruction *i);
// static alias_kind compare_alias_lists(annote_list *al1, annote_list_base *al2);
static annote_list *peek_annotes(instruction *i, const char *k_);
static int peek_array(operand op);

// for pointer analysis
//
static alias_kind compare_alias_lists(annote_list_base *al1, annote_list_base *al2)
{
    assert(al1 && !al1->is_empty());
    assert(al2 && !al2->is_empty());
    w_points_to *wpt1, *wpt2;
    int stride, offset;

    alias_kind alias = INDEPENDENT;

    annote_list_iter ali1(al1);
    annote_list_iter ali2(al2);
    int cur_clone_num;
    while (!ali1.is_empty() && !ali2.is_empty()) {
      wpt1 = WILBYR_ARRAY_BASE(ali1.step());
      wpt2 = WILBYR_ARRAY_BASE(ali2.step());

      cur_clone_num = MAX(wpt1->clone_num(), wpt2->clone_num());

      // Ignore clones that don't match.  Assumes array_base
      // annotations are in order.
      //
      while ((wpt1->clone_num() != cur_clone_num) && (!ali1.is_empty())) {
	wpt1 = WILBYR_ARRAY_BASE(ali1.step());
      }
      while ((wpt2->clone_num() != cur_clone_num) && (!ali2.is_empty())) {
	wpt2 = WILBYR_ARRAY_BASE(ali2.step());
      }

      assert(wpt1->clone_num() == wpt2->clone_num());
      if (wpt1->points_to()->aliased(wpt2->points_to(), &offset, &stride)) {

	  if ((stride == 0) && (offset == 0)) alias = ALIASED_NO_OFFSET;
	  else return ALIASED_WITH_OFFSET;
      }
    }

    return alias;
}

// Extract all the "k_" annotations
// 
static annote_list *
peek_annotes(instruction *i, const char *k_)
{
    annote_list *al = new annote_list();

    annote_list_iter ali(i->annotes());
    while (!ali.is_empty()) {
	annote *an = ali.step();
	if (an->name() == k_) {
	    al->append(an);
	}
    }

    return al;
}

static int peek_array(operand op)
{
    if (op.is_instr()) {
	instruction *i = op.instr();
	while (i->opcode() == io_cpy || i->opcode() == io_cvt) {
	    operand op = ((in_rrr*)i)->src1_op();
	    if (op.is_symbol()) return 0;
	    else i = op.instr();
	}
	return (i->opcode() == io_array);
    } else {
	return 0;
    }
}

// class summarySym ******************************************************
//
// public methods
//

summarySym::summarySym(summarySym *s)
{
    key = s->key;
    info = s->info;
    s->info = NULL;
    reduction_type(s->reduction_type());
    wilbyr_al = s->wilbyr_al;
}

summarySym::summarySym(var_sym *k, reflist *i, reduction_types x)
{
    key = k;
    info = i;
    reduction_type(x);
    wilbyr_al = new annote_list;
}

summarySym::~summarySym()
{
    if (info) {  // cleaning up references
	info->purge();
	delete info;
    }
    //    delete wilbyr_al;
}

void summarySym::propagate(summarySym *s)
{
    if (reduction_type() && s->reduction_type()) {
	if (reduction_type() == REDUCTION_UNKNOWN) {
	    reduction_type(s->reduction_type());
	} else if (reduction_type() != s->reduction_type()) {
	    reduction_type(REDUCTION_NONE);
	}
    } else {
	reduction_type(REDUCTION_NONE);
    }
}

int summarySym::summarize(in_list *calls)
{
    if (reduction_type() != REDUCTION_NONE) {
	if (!info->is_empty()) {
	    reduction_type(info->reduction_type());  // compute reduction_type
	}
	if (reduction_type() == REDUCTION_UNKNOWN) {
	    reduction_type(REDUCTION_NONE);

	    if (option['d'] >= 2) {
		printf("Irreducible (write(s) irreducible): ");
		key->print();
		printf("\n");
		fflush(stdout);
	    }
	}

	// REDUCTION_UNKNOWN shouldn't occur below this point
	
	if (reducible()) {
	    if (!in_modref_syms(calls)) {  // sanity check
		return 1;  // reduction found!
	    } else {
		reduction_type(REDUCTION_NONE);
		
		if (option['d'] >= 2) {
		    printf("Irreducible (found in the k_call_mod_sym or k_call_ref_sym): ");
		    key->print(stdout);
		    printf("\n");
		    fflush(stdout);
		}
	    }
	}
    }
    return 0;
}

void summarySym::promote_and_generate(tree_for *tf)
{
    reflist_iter refIter(info);
    while (!refIter.is_empty()) {
	reference *ref = refIter.step();
	assert(ref->reducible());

	assert(!ref->reducible_gen());
	var_sym *vsym = ref->promote(tf);
	ref->region_type(REGION_SCALAR);

	ref->generate(tf, vsym);
    }
}

void summarySym::generate(tree_for *tf)
{
    reflist_iter refIter(info);
    while (!refIter.is_empty()) {
	reference *ref = refIter.step();
	assert(ref->reducible());
	ref->generate(tf);
    }
}

void summarySym::generate_sub(tree_for *tf)
{
    // In the future, should generate sub-region reduction
    generate(tf);
}

inline region_types summarySym::region_type() const
{
    if (info->is_empty()) return REGION_NONE;

    region_types region = info->head()->contents->region_type();
    if (region == REGION_SINGLE) {
	//
	// if (all others are partitionable REGION_SINGLE) REGION_SINGLE
	// otherwise REGION_FULL
	//
	if (info->partition()) {
	    if (!option['p']) {  // promotion turned off
		printf("Promotion possible but turned off: ");
		key->print();
		printf("\n");
		fflush(stdout);
		return REGION_FULL;
	    }
	    return REGION_SINGLE;
	} else {
	    if (option['S']) {
		printf("Suppress reductions (unpromotable reductions): ");
		key->print();
		printf("\n");
		fflush(stdout);
		return REGION_NONE;
	    }
	    return REGION_FULL;
	}
    } else if (region == REGION_SUB) {
	//
	// if (all others are the same REGION_SUB) REGION_SUB
	// otherwise REGION_FULL
	//
	if (info->same_ref(info->head()->contents)) {
	    return REGION_SUB;
	} else {
	    if (option['S']) {
		if (option['d'-'\0'] >= 1) {
		    printf("Suppress reductions (sub-regions are not the same): ");
		    key->print();
		    printf("\n");
		    fflush(stdout);
		}
		return REGION_NONE;
	    }
	    return REGION_FULL;
	}
    } else {
	return region;
    }
}

inline reduction_types summarySym::reduction_type() const
{
    return redtype;
}

inline void summarySym::reduction_type(reduction_types x)
{
    redtype = x;
}

inline boolean summarySym::reducible() const
{
    return (redtype > 0);
}

// class summarySym ******************************************************
//
// private methods
//

inline boolean equiv(reference *r1, reference *r2);

inline boolean equiv(reference *r1, reference *r2)
{
    return (r1 && r2) ? equiv(r1->write, r2->write) : FALSE;
}

int summarySym::src_map(operand src, instruction *i, reference *dst,
			reduction_types aType)
{
    if (aType >= REDUCTION_GEN && info /* vsym was written before */) {
	//
	// Dependence on intermediate opnds found:
	// Kill the associative functions as reduction.
	// We do this for skweel becaus skweel ignores all
	// the references in the associative functions
	//
	if (assoc_refs[aType]) {
	    boolean match = TRUE;
	    reflist_iter iter(info);
	    while (!iter.is_empty()) {
		reference *r = iter.step();
		if (!equiv(src, r->write)) {
		    match = FALSE;
		    break;
		}
	    }
	    if (!match) {   // if (src and info match) OK
		delete assoc_refs[aType];
		assoc_refs[aType] = 0;
	    }
	}
    }	    

    if (reduction_type() == REDUCTION_NONE) return 0;

    reference *matched_lhs;
    if ((matched_lhs = info->find(src))) {// Exactly matched: src = matched_lhs
	if (matched_lhs == dst || equiv(matched_lhs, dst)) {
	    // Exactly matched: matched_lhs = dst
	    dst->check_in_read(i, src, aType);
	} else {
	    if (option['d'-'\0'] >= 2) {
		printf("Irreducible (writes and reads across exp-trees): ");
		key->print(stdout);
		printf("\n");
		fflush(stdout);
	    }
	    reduction_type(REDUCTION_NONE);
	}
    } else {  // Partially matched
	//
	// Actually if src is distinct to all writes, it can be ignored for
	// this loop. In that case, src is most likely a REGION_SINGLE. A
	// array promotion prepass should handle it.
	//
	if (option['d'] >= 2) {
	    printf("Irreducible (writes and reads mismatched): ");
	    key->print(stdout);
	    printf("\n");
	    fflush(stdout);
	}
	reduction_type(REDUCTION_NONE);
    }
    return 0;
}

boolean summarySym::in_modref_syms(in_list *calls)
{
    in_list_iter callsIter(calls);

    while (!callsIter.is_empty()) {
	instruction *i = callsIter.step();
	if (found(key, (immed_list*) i->peek_annote(k_call_mod_syms)) ||
	    found(key, (immed_list*) i->peek_annote(k_call_ref_syms))) {
	    return TRUE;
        }
    }
    return FALSE;
}


// class summaryFor ******************************************************
//
// public methods
//

summaryFor::summaryFor(tree_for *tfor)
{
    tf = tfor;
    calls = new in_list;
    wilbyr_al = new annote_list;
}

summaryFor::~summaryFor()
{
    delete calls;

    summaryFor_iter iter(this);
    while (!iter.is_empty()) {  // cleaning up summarySym's
	summarySym *s = iter.step();
	delete s;
    }
    //    delete wilbyr_al;
}

int summaryFor::propagate(summaryFor *inner)
{
    summaryFor_iter iter(inner);
    while (!iter.is_empty()) {
	summarySym *sumSym = iter.step();
	summarySym *thisSym;
	if ((thisSym = this->find(sumSym->key))) {
	    thisSym->propagate(sumSym);
	} else {
	    push(new summarySym(sumSym));
	}
    }

    if (!inner->calls->is_empty()) {
	calls->append(inner->calls);
    }

    if (src_map(inner->tf->lb_list()) || src_map(inner->tf->ub_list()) ||
	src_map(inner->tf->step_list()) || src_map(inner->tf->landing_pad())) {
	return 1;
    }
    return 0;
}

int summaryFor::map()
{
    // Note: tf->lb_list(), tf->ub_list(), tf->step_list(), and
    // tf->landing_pad() are executed only once before the tf->body().
    // No need to map them here.
    
    // Pass 1: Map all the writes
    if (dst_map(tf->body())) {
	return 1;
    }
        
    // Pass 2: Map all the src operands
    if (src_map(tf->body())) {
	return 1;
    }

    // Pass 3: Summarize each summarySym
    summaryFor_iter symIter(this);
    while (!symIter.is_empty()) {
	summarySym *s = symIter.step();
	if (s->summarize(calls)) {  // reducible

	    // Pointer analysis:
	    // Go through the summaryFor and make sure s->annote_list doesn't
	    // conflict with 
	    // 1. summaryFor->wilbyr_al and
	    // 2. other summarySym->wilbyr_al's
	    //
	    alias_kind alias = INDEPENDENT;
	    annote_list_base *sw = s->wilbyr_al;
	    if (!sw->is_empty()) {
		if (!this->wilbyr_al->is_empty()) {
		    alias = compare_alias_lists(sw, this->wilbyr_al);
		}
		if (alias != INDEPENDENT) {
		    s->reduction_type(REDUCTION_NONE);
		    continue;
		}
		summaryFor_iter iter(this);
		while (!iter.is_empty()) {
		    summarySym *others = iter.step();
		    if (others != s && !others->wilbyr_al->is_empty()) {
			alias = compare_alias_lists(sw, others->wilbyr_al);
			if (alias != INDEPENDENT) {
			    s->reduction_type(REDUCTION_NONE);
			    goto end;
			}
		    }
		}
	    }

	    switch (s->region_type()) {
	      case REGION_NONE:  // don't generate reduction annotations.
		break;

	      case REGION_SINGLE:
		s->promote_and_generate(tf);
		break;

	      case REGION_SUB:
		s->generate_sub(tf);
		break;

	      default:  // case REGION_SCALAR, REGION_FULL, REGION_MESSY:
		s->generate(tf);
		break;
	    }
	}
      end:;
    }
    return 0;
}

summarySym *summaryFor::find(var_sym *vsym)
{
    summaryFor_iter iter(this);
    while (!iter.is_empty()) {
	summarySym *sumSym = iter.step();
	if (sumSym->key == vsym) return sumSym;
    }
    return NULL;
}

// class summaryFor ******************************************************
//
// private methods
//

// Map all the writes
//
int summaryFor::dst_map(tree_node_list *body)
{
    tree_node_list_iter tnli(body);
    while (!tnli.is_empty()) {
        tree_node *t = tnli.step();

        switch (t->kind()) {
          case TREE_FOR:  // summary information propagated already
            break;
            
          case TREE_BLOCK:
            if (dst_map(((tree_block*)t)->body())) return 1;
            break;
            
          case TREE_IF:
            if (dst_map(((tree_if*)t)->header())) return 1;
            if (dst_map(((tree_if*)t)->then_part())) return 1;
            if (dst_map(((tree_if*)t)->else_part())) return 1;
            break;
            
          case TREE_LOOP:
            if (dst_map(((tree_loop*)t)->body())) return 1;
            if (dst_map(((tree_loop*)t)->test())) return 1;
            break;

          case TREE_INSTR:
            tree_instr *ti = (tree_instr*)t;
            instruction *i = ti->instr();
	    sym_node *sn = get_proc_sym_of_cal(i);
	    if (sn) {
		void *data = sn->peek_annote(k_associativity);
		if (data) {
		    // ASSUMPTION for porky -assoc:
		    // Call instruction is the root instruction
		    if (dst_map_gen((in_cal*)i, proc_assoc_type, data))
			return 1;
		    proc_assoc_type++;
		    break;
		}
	    }
            operand write = get_instr_dst_wilbyr(i, this);
            if (!write.is_null()) {
		reference *ref;
                if (check_in_write(write, ti, &ref)) return 1;
            }
            break;
        }
    }
    return 0;
}   

int summaryFor::dst_map_gen(in_cal *i, int aType, void *data)
{
    int j;
    operand opnd;
    reference *ref;

    if (aType >= MAX_REDUCTION_TYPE) {
        assert_msg(0, ("Run out of memory for assoc_refs[]"));
    } else {
        assoc_refs[aType] = new reflist;
    }

    immed_list *il = (immed_list*)data;
    immed_list_iter ili(il);
    assert(! (il->count()%3) );
    for (j = 0; j < il->count()/3; j++) {
	// Final operands should be in the same location as the corresponding
	// initial opernads. 
	
	immed im = ili.step();
	if (im.is_integer()) {
	    im = ili.step();  // skip the final operand
	    opnd = i->argument(im.integer());
	} else {
	    assert(im.is_string() && !strcmp(im.string(), "return value"));
	    opnd = get_instr_dst(i);  // i->dst_op()
	    im = ili.step();  // skip the init operand
	}
	if (check_in_write(opnd, i->parent(), &ref)) return 1;

	if (ref) {
	    ref->check_in_read(i, opnd, (reduction_types)aType);
            (assoc_refs[proc_assoc_type])->append(ref);
        } else return 1;
	    
	ili.step();
    }
    return 0;
}

int summaryFor::check_in_write(operand &write, tree_instr *ti, reference **refer)
{
    reference *ref;
    reflist *r;
    summarySym *sumS;
    var_sym *vsym;
    int too_messy;

    *refer = NULL;
    if ((vsym = get_sym(write, &too_messy))) {
        if ((sumS = find(vsym))) {
            if (sumS->reduction_type()) {
                /* Note that our definition of reductions does not require
                   that write be distinct to the found reducible references */
                ref = new reference(write, ti->instr());
		ti->instr()->prepend_annote(k_write_ref, ref);
//                cache->append(new instrRef(ti->instr(), ref));
                sumS->info->push(ref);
		*refer = ref;
            }
        } else {
            r = new reflist;
            ref = new reference(write, ti->instr());
	    ti->instr()->prepend_annote(k_write_ref, ref);
//            cache->append(new instrRef(ti->instr(), ref));
            r->push(ref);
            push(new summarySym(vsym, r));
	    *refer = ref;
        }
    } else {
	if (too_messy) {  // treat all sum_e's in summary as irreducible
	    return 1;
	}
    }
    return 0;
}

int summaryFor::src_map(tree_node_list *body)
{
    tree_node_list_iter tnli(body);
    while (!tnli.is_empty()) {
        tree_node *t = tnli.step();

        switch (t->kind()) {
          case TREE_FOR:
            break;
            
          case TREE_BLOCK:
            if (src_map(((tree_block*)t)->body())) return 1;
            break;
            
          case TREE_IF:
            if (src_map(((tree_if*)t)->header())) return 1;
            if (src_map(((tree_if*)t)->then_part())) return 1;
            if (src_map(((tree_if*)t)->else_part())) return 1;
            break;
            
          case TREE_LOOP:
            if (src_map(((tree_loop*)t)->body())) return 1;
            if (src_map(((tree_loop*)t)->test())) return 1;
            break;

          case TREE_INSTR:
            if (src_map((tree_instr *)t)) return 1; 
            break;
        }
    }
    return 0; 
}

int summaryFor::src_map(tree_instr *ti)
{
    reference *dst = (reference *)ti->instr()->get_annote(k_write_ref);
//    reference *dst = cache->find(ti->instr());

    if (dst && dst->write.kind() == OPER_INSTR) {
	instruction *i = dst->write.instr();
	// must skip the dst->sym in the left-hand side
        if (src_map(i, dst, assoc(i, dst->reduction_type()), TRUE)) return 1;
    }

    /* map rhs srcs */
    if (src_map(ti->instr(), dst, assoc(ti->instr(), -1), FALSE)) return 1;
    return 0;
}

#define SRC_MAP(s) if (!s.is_null() && src_map(s, i, dst, aType, lhs)) return 1

int summaryFor::src_map(instruction *i, reference *dst, int aType, boolean lhs)
{
    operand src;
    int n;
    annote_list *al;

    switch (i->format()) {
      case inf_rrr: {
        in_rrr *ir = (in_rrr*)i;
	switch (ir->opcode()) {
	  case io_str:        // dst_addr_op processed in dst_map() already
            src = ir->src_op();
            SRC_MAP(src);
	    break;

	  case io_memcpy:     // dst_addr_op processed in dst_map() already
	      if ((al = peek_annotes(ir, k_wilbyr_load_from))) { // pointer analysis
		  if (!peek_array(ir->src_addr_op())) {
		      // if array_base, in_array will take care of it. 
		      if (!al->is_empty()) {
			  this->wilbyr_al->push(al);
		      }
		  }
	      }

            src = ir->src_addr_op();
            SRC_MAP(src);
	    break;

	  case io_lod:        // dst_addr_op processed in dst_map() already
	      if ((al = peek_annotes(ir, k_wilbyr_load_from))) { // pointer analysis
		  if (!peek_array(ir->src_addr_op())) {
		      // if array_base, in_array will take care of it. 
		      if (!al->is_empty()) {
			  this->wilbyr_al->push(al);
		      }
		  }
	      }
		  
            src = ir->src_addr_op();
            SRC_MAP(src);
	    break;

	  default:
            src = ir->src1_op();
            SRC_MAP(src);
            if (i->opcode() == io_sub || i->opcode() == io_div) {
                // not associative along the src2_op() branch of the exp-tree
		// (conservative here: don't maintain polarity here)
                aType = 0;
            }
            src = ir->src2_op();
            SRC_MAP(src);
	    break;
	}
        break;
      }
        
      case inf_cal: {
	in_cal *ic = (in_cal*)i;
	sym_node *sn;
	if ((sn = get_proc_sym_of_cal(ic))) {
	    void *data = sn->peek_annote(k_associativity);
	    if (data) {
		if (src_map_gen(ic, dst, dst->reduction_type(), data, lhs)) {
		    return 1;
		}
		break;
	    }
	}

        if (is_impure(ic)) {
            if (process_impure(ic, this)) return 1;
        } else {
            operand addr_op();
            for (n = 0; n < (int)ic->num_args(); n++) {
                src = ic->argument(n);
                SRC_MAP(src);
            }
        }
        break;
      }

      case inf_ldc: {
	in_ldc *il = (in_ldc*)i;
	immed val = il->value();
	assert(!val.is_instr());
	if (val.is_symbol()) {
	    sym_node *sn = val.symbol();
	    if (sn->is_var()) {
		if (sym_map((var_sym*)sn)) return 1;
	    }
	}
	break;
      }
	
      case inf_gen:
	printf("Generic instructions seen. Assuming it does not affect associativity.\n");
	break;
	
      case inf_array:
	// Skip the base operand because we know that base_operand must look
	// like "ldc <sym>" (otherwise, too messy and bail-out) and we already
	// "dst_map" or "src_map" it.
	//
        for (n = 1; n < (int)i->num_srcs(); n++) {
            src = i->src_op(n);
            SRC_MAP(src);
        }
        break;

      case inf_bj:
      case inf_mbr:
        for (n = 0; n < (int)i->num_srcs(); n++) {
            src = i->src_op(n);
            SRC_MAP(src);
        }
        break;

      case inf_lab:
      case inf_none:
        break;
    }
    return 0;
}

int summaryFor::src_map_gen(in_cal *i, reference *dst, int aType, void *data,
			    boolean lhs)
{
    int j;

    immed_list *il = (immed_list*)data;
    immed_list_iter ili(il);
    assert(! (il->count()%3) );
    for (j = 0; j < il->count()/3; j++) {
        operand opnd;
        
	// Final/Init operands: cannot be read/written elsewhere
	immed im = ili.step();
	if (im.is_integer()) {
	    im = ili.step();  // recursively src_map the init opnd
	    opnd = i->argument(im.integer());
            if (opnd.kind() == OPER_INSTR) {
                instruction *in = opnd.instr();
                if (src_map(in, dst, assoc(in, aType), lhs))
                    return 1;
            }
	} else {
	    assert(im.is_string() && !strcmp(im.string(), "return value"));
	    im = ili.step();  // recursively src_mapped already
	}

        // Scanned operands: cannot be written elsewhere
	im = ili.step();
	assert(im.is_integer());
	SRC_MAP(i->argument(im.integer())); 
    }

    // Intermediate operands: cannot be written elsewhere
    il = (immed_list*)data;
    for (j = 0; j < (int)i->num_args(); j++) {
	if (!found(j, il)) {  // j not in il => argument(j) is intermediate opnd
	    SRC_MAP(i->argument(j));
	}
    }
    return 0;
}

int summaryFor::src_map(operand src, instruction *i, reference *dst, int aType,
			boolean lhs)
{
//    reflist_e *rl_e;
//    reference *ref, *lhs; // matched left-hand side reference
//    summary_e *sum_e;
    var_sym *vsym;
    int too_messy;

    // skip io_cpy instructions.
    // axiom: src can never be a cpy instruction
    while (src.kind() == OPER_INSTR) {
	instruction *in = src.instr();
	if (in->opcode() == io_cpy || in->opcode() == io_cvt) {
	    src = ((in_rrr*)in)->src1_op();
	} else break;
    }
        
    if ((vsym = get_sym(src, &too_messy))) {
	if (sym_map(vsym, src, i, dst, aType, lhs)) return 1;
    } else {
	if (too_messy) {  // treat all summarySym's as irreducible
	    return 1;
	}
    }

    // Recursively trace down the src
    if (src.kind() == OPER_INSTR) {
        instruction *in = src.instr();
        if (src_map(in, dst, assoc(in, aType), lhs))
            return 1;
    }
    return 0;
}

int summaryFor::sym_map(var_sym *vsym, operand src, instruction *i,
			reference *dst, int aType, boolean lhs)
{
    int too_messy;
    summarySym *sumSym = NULL;
    
    if ((sumSym = find(vsym))) {
	if (!(lhs && vsym == get_sym(dst->write, &too_messy))) {
	    if (sumSym->src_map(src, i, dst, (reduction_types)aType)) return 1;
	}
    } else {  // Create a summarySym for future propagation
	if (option['d'-'\0']) {
	    printf("Irreducible (Only reads): ");
	    vsym->print(stdout);
	    printf("\n");
	    fflush(stdout);
	}
	push(sumSym = new summarySym(vsym, NULL, REDUCTION_NONE));
    }

    // for pointer analysis
    if (sumSym && src.is_instr()) {
	instruction *in = src.instr();
	annote_list *al;
	if (in->opcode() == io_array && 
	    (al = peek_annotes(in, k_wilbyr_array_base))) {
	    if (!al->is_empty()) {
		sumSym->wilbyr_al->push(al);
	    }
	}
    }
    return 0;
}

int summaryFor::sym_map(var_sym *vsym)
{
    summarySym *sumSym;
    
    if ((sumSym = find(vsym))) {
	if (option['d'-'\0'] >= 2) {
	    printf("Irreducible (Address taken): ");
	    sumSym->key->print(stdout);
	    printf("\n");
	    fflush(stdout);
	}
	sumSym->reduction_type(REDUCTION_NONE);
    } else {  // Create a summarySym for future propagation
	if (option['d'-'\0']) {
	    printf("Irreducible (Only reads): ");
	    vsym->print(stdout);
	    printf("\n");
	    fflush(stdout);
	}
	push(new summarySym(vsym, NULL, REDUCTION_NONE));
    }
    return 0;
}
				
	

//***********************************************************************
//
// private functions
//

// aType (internal argument):
//  -1: unknown
//  0: not associative
//  1: sum-type associativity
//  2: product-type associativity
//  3: min-type associativity
//  4: max-type associativity
//
static int assoc(instruction *i, int aType)
{
    if (aType == 0 || aType >= REDUCTION_GEN) return aType;
    switch (i->opcode()) {
      case io_add:
        if (aType == -1 || aType == 1) return 1; else return 0;
      case io_sub:
        if (aType == -1 || aType == 1) return 1; else return 0;
      case io_mul:
        if (aType == -1 || aType == 2) return 2; else return 0;
      case io_div:
        if (aType == -1 || aType == 2) return 2; else return 0;
      case io_min:
        if (aType == -1 || aType == 3) return 3; else return 0;
      case io_max:
        if (aType == -1 || aType == 4) return 4; else return 0;
      case io_neg: case io_rem: case io_mod:
      case io_not: case io_and: case io_ior: case io_xor:
      case io_abs: case io_asr: case io_lsl: case io_rot: case io_divfloor:
      case io_divceil: case io_array: case io_cal:
        return 0;
      default:
        return aType;
    }
}

static inline boolean is_impure(instruction *i)
{
    return impure(i);
}

static int process_impure(instruction *i, summaryFor *sumF)
{
    if (!i->annotes()->peek_annote(k_call_mod_syms) &&
	!i->annotes()->peek_annote(k_call_ref_syms)) {
	if (option['d'-'\0'] >= 2) {
	    printf(" without modref information. Bailing out.\n");
	    fflush(stdout);
	}
	return 1;
    } else {
	if (option['d'-'\0'] >= 2) {
	    printf(". Modref information found. Keey trying.\n");
	    fflush(stdout);
	}
	sumF->calls->push(i);
	return 0;
    }
}

static operand get_instr_dst_wilbyr(instruction *i, summaryFor *sfor)
{
    annote_list *al;

    if (i->opcode() == io_str || i->opcode() == io_memcpy) {
	if ((al = peek_annotes(i, k_wilbyr_store_to))) {  // pointer analysis
	    if (!peek_array( ((in_rrr *)i) ->dst_addr_op())) {
		// if array_base, in_array will take care of it. 
		if (!al->is_empty()) {
		    sfor->wilbyr_al->push(al);
		}
	    }
	}
        return ((in_rrr *)i)->dst_addr_op();
    } else {
        return i->dst_op();
    }
}

static operand get_instr_dst(instruction *i)
{
    if (i->opcode() == io_str || i->opcode() == io_memcpy) {
        return ((in_rrr *)i)->dst_addr_op();
    } else {
        return i->dst_op();
    }
}
