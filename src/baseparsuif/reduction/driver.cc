/* file "driver.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"
#include <annotes.h>

int option[256];

// Reduction annotations
const char *k_reduction;

// Generalized reductions
const char *k_reduction_gen;
const char *k_associativity;

// Ipa -mr
const char *k_call_mod_syms;
const char *k_call_ref_syms;

// Unregistered annotation
const char *k_write_ref;

static void reduction_init();
static void do_proc(tree_proc *);
static void reduce(tree_node_list *);
static void parse_arg(int, char **);
static void usage(void);

extern int main(int argc, char *argv[])
{
    start_suif(argc, argv);

    parse_arg(argc, argv);

    reduction_init();
    suif_proc_iter(3, argv, do_proc, TRUE, TRUE, TRUE);
    return 0;
}

static void reduction_init()
{
    ANNOTE(k_reduction, "reduction", TRUE /* external annotation */);
    ANNOTE(k_reduction_gen, "reduction_gen", TRUE /* external annotation */);
    k_associativity = lexicon->enter("associativity")->sp;
    k_call_mod_syms = lexicon->enter("call mod syms")->sp;
    k_call_ref_syms = lexicon->enter("call ref syms")->sp;
    k_write_ref = lexicon->enter("write reference")->sp;
    k_pure_function = lexicon->enter("pure function")->sp;

    wilbyr_init_annotes();
}

static void do_proc(tree_proc *tp)	
{
    block::set_proc(tp);
    if (option['d'] >= 2) {
	printf("=======================================\n");
	printf("Proc ");
	tp->proc()->print();
	printf("\n");
	fflush(stdout);
    }
    reduce(tp->body());
}

int proc_assoc_type;
reflist *assoc_refs[MAX_REDUCTION_TYPE];

static void reduce(tree_node_list *tnl)
{
    tree_node_list_iter tnli(tnl);
    while (!tnli.is_empty()) {
	tree_node *t = tnli.step();
		
	switch (t->kind()) {
	  case TREE_FOR: {
	    // Deal with this loop nest
	    //
	    proc_assoc_type = REDUCTION_GEN;
	    for (int j = REDUCTION_GEN; j < MAX_REDUCTION_TYPE; j++) {
		if (assoc_refs[j]) delete assoc_refs[j];
		assoc_refs[j] = 0;
	    }
	    tree_for *tf = (tree_for *) t;
	    nest *tmp = new nest(tf);
            tmp->for_map(tf->body());
	    tmp->dfs_map();
	    delete tmp;
	    break;
          }

	  case TREE_LOOP:
	    reduce(((tree_loop*)t)->body());
	    reduce(((tree_loop*)t)->test());
	    break;
	    
	  case TREE_IF:
	    reduce(((tree_if*)t)->header());
	    reduce(((tree_if*)t)->then_part());
	    reduce(((tree_if*)t)->else_part());
	    break;
	    
	  case TREE_INSTR:
	    break;
	    
	  case TREE_BLOCK:
	    reduce(((tree_block*)t)->body());
	    break;
	    
	}
    }			
}

static void parse_arg(int argc, char **argv)
{
    char *inhibit = NULL;
    static cmd_line_option option_table[] =
	{
	    {CLO_NOARG, "-h", NULL, &(option['h'])},
	    {CLO_NOARG, "-D", NULL, &(option['D'])},
	    {CLO_INT, "-d", "0", &(option['d'])},
	    {CLO_NOARG, "-no-array", NULL, &(option['a'])},
	    {CLO_NOARG, "-S", NULL, &(option['S'])},
	    {CLO_NOARG, "-p", NULL, &(option['p'])},
	    {CLO_INT, "-s", "10000", &(option['s'])},
	    {CLO_STRING, "-no", "", &inhibit}
	};
    parse_cmd_line(argc, argv, option_table,
		   sizeof(option_table) / sizeof(cmd_line_option));

    if (argc != 3) {
	usage();
	exit(1);
    }
    if (option['h']) usage();
    if (option['D']) option['d'] = 2;
    if (!option['p']) option['p'] = 1;  // default is on
    for (char *s = inhibit; *s; s++) option[*s] = 0;
}

static void usage(void)
{
    char *usage_message[] = {
"usage: reduction [options] SUIF-file SUIF-file\n",
"\n",
"  options:\n",
"    -h\n",
"        print out usage information\n",
"    -D\n",
"        print out all the debugging information. Same as \"-d 2\".\n",
"    -d <debug-level>\n",
"        available debug levels are 0, 1 and 2.\n",
"    -no-array\n",
"        Suppress array reductions. Default is no suppression.\n",
"    -S\n",
"        Suppress inefficient reductions. Default is off.\n",
"    -p\n",
"        do array promotion for reductions over scalars. Default is on\n",
"    -s <small size threshold>\n",
"        treat a dimension of reduction arrays as full-range reduced if its\n",
"        size is less than or equal to the threshold. Default is 10000.\n",
"    -no <options>\n",
"        turn off the options. For example, \"-no SD\" will turn off\n",
"        suppressions and debugging. \"-no\" overrides other options.\n" };

    for (unsigned line_num = 0;
         line_num < sizeof(usage_message) / sizeof(char *); ++line_num)
      {
        fprintf(stderr, "%s", usage_message[line_num]);
      }
}

