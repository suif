/* file "rlist.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"

boolean equiv(reference *r1, reference *r2);
boolean equiv(operand &o1, operand &o2);
boolean equiv(instruction *i1, instruction *i2);
boolean very_partial_equiv(reference *r1, reference *r2);
boolean very_partial_equiv(operand &o1, operand &o2);
boolean very_partial_equiv(instruction *i1, instruction *i2);
boolean intersect(operand &o1, operand &o2);
boolean intersect(instruction *i1, instruction *i2);

static boolean overlaps(immed &i1, immed &i2);
static boolean overlaps(sym_addr sa1, sym_addr sa2);
static boolean intersect_ai(in_array *ia1, in_array *ia2);
static named_lin_ineq *include_fors(tree_for *forlist[], int number);
static named_lin_ineq *include_for(tree_for *tf, named_lin_ineq *c);
static void append_to_list(array_info *a, operand & op, tree_node *tn, if_ops multi, int style);

boolean rlist::partition()
{
    rlist_e *i = head(), *j;
    reference *ref, *ref1;

    while (i) {
        j = i->next();
        ref = i->contents;
        while (j) {
            ref1 = j->contents;
            if (equiv(ref, ref1)) {
                /* initialize ref->sym and ref1->sym */
                if (ref->sym == NULL) {
                    block red(block::new_sym(elem_type(ref->write.instr()), "red"));
                    ref->sym = (var_sym*)red.get_sym();
                }
                ref1->sym = ref->sym;
            } else if (intersect(ref->write, ref1->write)) {
                return FALSE; //mark_irreducible(tmp);
            }
            j = j->next();
        }
        i = i->next();
    }
    
    /* Initialize ref->sym */
    i = head();
    while (i) {
        ref = i->contents;
        if (ref->sym == NULL) {
            block red(block::new_sym(elem_type(ref->write.instr()), "red"));
            ref->sym = (var_sym*)red.get_sym();
        }
        i = i->next();
    }
    return TRUE;
}

/* Auxiliary Search Methods */

/* Not in use now */
reference *rlist::search(reference* r)
{
    if (r) return search(r->write); else return NULL;
}

/* Not in use now */
reference *rlist::partial_search(reference *r)
{
    if (r) return partial_search(r->write); else return NULL;
}

reference *rlist::very_partial_search(reference* r)
{
    if (r) return very_partial_search(r->write); else return NULL;
}

reference *rlist::search(operand &o)
{
    rlist_e *i = head();
    while (i) {
        reference *ref = i->contents;
        if (ref->write.kind() == o.kind()) {
            if (o.kind() == OPER_SYM) {
                if (o.symbol() == ref->write.symbol()) return ref;
            } else { // OPER_INSTR
                if (equiv(o.instr(), ref->write.instr())) return ref;
            }
        }
        i = i->next();
    }
    return NULL;
}

reference *rlist::partial_search(operand &o) {
    rlist_e *i = head();
    while (i) {
        reference *ref = i->contents;
        if (ref->write.kind() == o.kind()) {
            if (o.kind() == OPER_SYM) {
                if (o.symbol()->overlaps(ref->write.symbol())) return ref;
            } else { // OPER_INSTR
                if (intersect(o.instr(), ref->write.instr()))
                    return ref;
            }
        }
        i = i->next();
    }
    return NULL;
}

reference *rlist::very_partial_search(operand &o)
{
    rlist_e *i = head();
    while (i) {
        reference *ref = i->contents;
        if (ref->write.kind() == o.kind()) {
            if (o.kind() == OPER_SYM) {
                if (o.symbol()->overlaps(ref->write.symbol())) return ref;
            } else { // OPER_INSTR
                if (very_partial_equiv(o.instr(), ref->write.instr()))
                    return ref;
            }
        }
        i = i->next();
    }
    return NULL;
}

rlist_e *rlist::very_partial_search1(operand &o)
{
    rlist_e *i = head();
    while (i) {
        reference *ref = i->contents;
        if (ref->write.kind() == o.kind()) {
            if (o.kind() == OPER_SYM) {
                if (o.symbol()->overlaps(ref->write.symbol())) return i;
            } else { // OPER_INSTR
                if (very_partial_equiv(o.instr(), ref->write.instr()))
                    return i;
            }
        }
        i = i->next();
    }
    return NULL;
}

/* Auxiliary Methods */

/* purge all */
void rlist::purge()
{ 
    rlist_e *i = head();
    while (i) { delete i->contents; i = i->next(); } // delete reference
}

/* Some internal routines */

boolean equiv(reference *r1, reference *r2)
{
    if (r1 && r2) return equiv(r1->write, r2->write);
    return FALSE;
}

boolean equiv(operand &o1, operand &o2)
{
    if (o1.kind() == o2.kind()) {
        if (o1.kind() == OPER_SYM) {
            if (o1.symbol() == o2.symbol()) return TRUE;
        } else { // OPER_INSTR
            return equiv(o1.instr(), o2.instr());
        }
    }
    return FALSE;
}

boolean equiv(instruction *i1, instruction *i2)
{
    int n;
    operand src1, src2;
    
    if (i1->opcode() != i2->opcode() || i1->num_srcs() != i2->num_srcs())
        return FALSE;
    /* check for ldc immediates */
    if (i1->opcode() == io_ldc) 
        if ( ((in_ldc *)i1)->value() != ((in_ldc *)i2)->value() ) 
            return FALSE;

    if (i1->opcode() == io_array) {
        in_array *ia1 = (in_array*)i1, *ia2 = (in_array*)i2;
        if (ia1->elem_size() != ia2->elem_size() ||
            ia1->offset() != ia2->offset())
            return TRUE;
    }
    for (n = 0; n < i1->num_srcs(); n++) {
        src1 = i1->src_op(n);
        src2 = i2->src_op(n);
        if (src1.kind() != src2.kind()) return FALSE;
        switch (src1.kind()) {
          case OPER_SYM:
            if (src1.symbol() != src2.symbol()) return FALSE;
            break;
          case OPER_INSTR:
            if (!equiv(src1.instr(), src2.instr())) return FALSE;
            break;
          default:
            continue;
        }
    }
    return TRUE;
}

boolean very_partial_equiv(reference *r1, reference *r2)
{
    if (r1 && r2) return very_partial_equiv(r1->write, r2->write);
    return FALSE;
}

boolean very_partial_equiv(operand &o1, operand &o2)
{
    if (o1.kind() == o2.kind()) {
        if (o1.kind() == OPER_SYM) {
            if (o1.symbol()->overlaps(o2.symbol())) return TRUE;
        } else { // OPER_INSTR
            return very_partial_equiv(o1.instr(), o2.instr());
        }
    }
    return FALSE;
}

boolean very_partial_equiv(instruction *i1, instruction *i2)
{
    int n;
    operand src1, src2;
    
    if (i1->opcode() != i2->opcode() || i1->num_srcs() != i2->num_srcs())
        return FALSE;

    switch (i1->opcode()) {
      case io_ldc:      // check for ldc immediates 
        if ( ((in_ldc *)i1)->value() == ((in_ldc *)i2)->value() ) return TRUE;
        else return FALSE;
      case io_array:    
        in_array *ia1 = (in_array*)i1, *ia2 = (in_array*)i2;
        // Check base_op only
        src1 = ia1->base_op();
        src2 = ia2->base_op();
        var_sym *sn1, *sn2;
        if ((sn1 = get_sym_of_array(ia1)) && (sn2 = get_sym_of_array(ia2))) {
            if (!sn1->overlaps(sn2)) return FALSE;
        } else return FALSE;
        return TRUE;
      default:
        for (n = 0; n < i1->num_srcs(); n++) {
            src1 = i1->src_op(n);
            src2 = i2->src_op(n);
            if (src1.kind() != src2.kind()) return FALSE;
            switch (src1.kind()) {
              case OPER_SYM:
                if (!src1.symbol()->overlaps(src2.symbol())) return FALSE;
                break;
              case OPER_INSTR:
                if (!very_partial_equiv(src1.instr(), src2.instr())) return FALSE;
                break;
              default:
                continue;
            }
        }
        return TRUE;
    }
}

boolean intersect(operand &o1, operand &o2)
{
    if (o1.kind() == o2.kind()) {
        if (o1.kind() == OPER_SYM) {
            if (o1.symbol()->overlaps(o2.symbol())) return TRUE;
        } else { // OPER_INSTR
            return intersect(o1.instr(), o2.instr());
        }
    }
    return FALSE;
}

static boolean overlaps(sym_addr sa1, sym_addr sa2)
{
    if (!sa1.symbol()->is_var() || !sa2.symbol()->is_var()) return FALSE;
    
    var_sym *s1 = (var_sym*) sa1.symbol();
    var_sym *s2 = (var_sym*) sa2.symbol();
    if (s1->root_ancestor() != s2->root_ancestor()) return FALSE;

    int offset1 = s1->root_offset()+sa1.offset();
    int offset2 = s2->root_offset()+sa2.offset();
    if (offset1 <= offset2)
        return ((offset1 + s1->type()->size()) > offset2);
    else
        return ((offset2 + s2->type()->size()) > offset1);
}

static boolean overlaps(immed &i1, immed &i2)
{
    if (i1.is_symbol() && i2.is_symbol()) {
        return overlaps(i1.addr(), i2.addr());
    } else return (i1==i2);
}

/* with array indices disambiguation */
/* return TRUE if there is INTERSECTION */
boolean intersect(instruction *i1, instruction *i2) 
{
    int n;
    operand src1, src2;
    
    if (i1->opcode() != i2->opcode() || i1->num_srcs() != i2->num_srcs())
        return FALSE;

    switch (i1->opcode()) {
      case io_ldc:      // check for ldc immediates 
        if (overlaps( ((in_ldc *)i1)->value(), ((in_ldc *)i2)->value() ))
            return TRUE;
        else return FALSE;
      case io_array:    
        in_array *ia1 = (in_array*)i1, *ia2 = (in_array*)i2;
        // Check base_op
        var_sym *sn1, *sn2;
        if ((sn1 = get_sym_of_array(ia1)) && (sn2 = get_sym_of_array(ia2))) {
            if (!sn1->overlaps(sn2)) return FALSE;
            else if (sn1 != sn2) return TRUE; // be conservative
        } else return TRUE; // be conservative now. Can return FALSE?

/*      // Check bounds
        if (ia1->dims() != ia2->dims() || ia1->elem_size() != ia2->elem_size()
            || ia1->offset() != ia2->offset()) return TRUE;
        for (n = 0; n < ia1->dims(); n++) {
            if (!equiv(ia1->bound(n), ia2->bound(n))) return TRUE;
        }
        
        // Check indices (hacking)
        for (n = 0; n < ia1->dims(); n++) {
            src1 = ia1->index(n);
            src2 = ia2->index(n);
            if (src1.kind() == src2.kind()) {
                switch (src1.kind()) {
                  case OPER_INSTR:
                    instruction *t1 = src1.instr(), *t2 = src2.instr();
                    if (t1->opcode() == t2->opcode() && t1->opcode() == io_ldc) {
                        if (((in_ldc *)t1)->value() != ((in_ldc *)t2)->value())
                            return FALSE;
                        else continue;
                    } else break;
                    break;
                  case OPER_SYM:
                  default:
                    break;
                }
            } else break;
        }*/
        // at this point, knows that ia1 and ia2 has the same base symbol
        return intersect_ai(ia1, ia2);
        
      default:
        for (n = 0; n < i1->num_srcs(); n++) {
            src1 = i1->src_op(n);
            src2 = i2->src_op(n);
            if (src1.kind() != src2.kind()) return FALSE;
            switch (src1.kind()) {
              case OPER_SYM:
                if (!src1.symbol()->overlaps(src2.symbol())) return FALSE;
                break;
              case OPER_INSTR:
                if (!intersect(src1.instr(), src2.instr())) return FALSE;
                break;
              default:
                continue;
            }
        }
        return TRUE;
    }
}

#define MAXDEPTH 20

/* See ~saman/shadow/interproc/apdriver.cc and summary.cc */
/* ia2 is a single elment array access, but we do not want to take advantage
   of it */
/* In this version, be conservative: doing dim by dim */
static boolean intersect_ai(in_array *ia1, in_array *ia2)
{
    /* Recognize the for-nest and generate lin_ineqs */
    tree_for *forlist1[MAXDEPTH], *forlist2[MAXDEPTH];

    tree_node *tn = ia1->parent()->parent()->parent();
    for (int i1 = 0; tn && tn->parent(); tn = tn->parent()->parent()) {
        if (tn->kind() == TREE_FOR) {
            forlist1[i1++] = (tree_for*)tn;
            if (i1 == MAXDEPTH) break;
        }
    }
    tn = ia2->parent()->parent()->parent();
    for (int i2 = 0; tn && tn->parent(); tn = tn->parent()->parent()) {
        if (tn->kind() == TREE_FOR) {
            forlist1[i2++] = (tree_for*)tn;
            if (i2 == MAXDEPTH) break;
        }
    }
    named_lin_ineq *c;
    if (i1 >= i2) c = include_fors(forlist1, i1);
    else c = include_fors(forlist2, i2);

    /* COPIED from ap_driver::include_array() */
    array_info *ai1 = new array_info(ia1, TRUE);
    array_info *ai2 = new array_info(ia2, TRUE);
    array_info_iter iter1(ai1);
    array_info_iter iter2(ai2);
    while (!iter1.is_empty()) {
        access_vector *av1 = iter1.step();
        access_vector *av2 = iter2.step();

        named_lin_ineq *tc1 = named_lin_ineq::mk_named_lin_ineq(*av1, immed("foo"), FALSE);
        named_lin_ineq *tc2 = named_lin_ineq::mk_named_lin_ineq(*av2, immed("foo"), FALSE);
        if (tc1 && tc2) {
                    *tc1 &= tc1->ineqs()*-1;
                    *tc2 &= tc2->ineqs()*-1;
                    named_lin_ineq tmp = *tc1 & *tc2;
                    if(c) tmp &= *c;
            if (~tmp) { // no intersection
            } else {
                            delete tc1;
                            delete tc2;
                            if(c) delete c;
                            return TRUE;
                        }
        }
                if(tc1) delete tc1;
                if(tc2) delete tc2;
    }
        if(c) delete c;
    return FALSE;
}

static named_lin_ineq *include_fors(tree_for *forlist[], int number)
{
    if (number == 0) return NULL;
    named_lin_ineq *c = include_for(forlist[0]);
    for (int i = 1; i < number; i++) {
        c = include_for(forlist[i], c);
    }
    return c;
}

static named_lin_ineq *include_for(tree_for *tf, named_lin_ineq *c)
{
    /* COPIED from dep_for_annote::fill_in_access() */
    access_vector *stp0;
    array_info *lb0, *ub0;

    stp0 = new access_vector(tf->step_op(), tf, TRUE);
    lb0 = new array_info;
    append_to_list(lb0, tf->lb_op(), tf, io_max, TRUE);
    ub0 = new array_info;
    append_to_list(ub0, tf->ub_op(), tf, io_min, TRUE);

    if(lb0->is_empty()) {
        printf("*** WARNING: EMPTY LOWER BOUND *** loop %s \n",tf->index()->name());
//BUGBUG        tf->lb_op().print(stdout);
        fflush(stdout);
    }
    if(ub0->is_empty()) {
        printf("*** WARNING: EMPTY UPPER BOUND *** loop %s \n",tf->index()->name\
());
//BUGBUG        tf->ub_op().print(stdout);
        fflush(stdout);
    }

    /* ? normalize */

    /* COPIED from named_lin_ineq::include_for() */ 
    named_lin_ineq * stp = named_lin_ineq::mk_named_lin_ineq(stp0, tf->index(), FALSE);
    boolean fwd = TRUE;
    if(stp)
        if((stp->n() == 2)&&                            // no sym variables
           (stp->ineqs().m() == 1)) {                   // no max or mins
            if(stp->ineqs().n() == 2)                   // constant step
                if(ABS(stp->ineqs()[0][0]) == 1) {      // step of 1 or -1
                    int stride = stp->ineqs()[0][0];
                    if(stride == -1) fwd = FALSE;
                }
        }

    named_lin_ineq * cl = named_lin_ineq::mk_named_lin_ineq(lb0, tf->index(), fwd);
    if(cl == NULL) {
        printf("Unknown lower bound\n");
        if(c) return c;
        return NULL;
    }

    named_lin_ineq * cu = named_lin_ineq::mk_named_lin_ineq(ub0, tf->index(), !fwd);

    if(cu == NULL) {
        printf("Unknown upper bound\n");
        delete cl;
        if(c) return c;
        return NULL;
    }
    cu = named_lin_ineq::and(cl, cu, 0, 1);
    boolean stpbad = TRUE;
    if(stp)
        if((stp->n() == 2)&&                            // no sym variables
           (stp->ineqs().m() == 1)) {                   // no max or mins
            stpbad = FALSE;
            if(ABS(stp->ineqs()[0][0]) != 1) {               // non-unit step
                int stride = stp->ineqs()[0][0];
                named_lin_ineq str(cl);
                name_table_entry aux;
                aux.mark_aux();
                str.add_col(aux, str.n());
                str.ineqs()[0][str.n()-1] = stride;
                str &= str.ineqs()*-1;
                cu = named_lin_ineq::and(cu, &str, 1, 0);
            }
        }
    if(stpbad) {
        printf("Unknown or symbolic step size\n");
        delete cl;
        delete cu;
        if(stp) delete stp;
        if(c) return c;
        return NULL;
    }
    
    cu = named_lin_ineq::and(c, cu, 0, 1);
    cu->cleanup();
    delete cl;
    delete stp;
    return cu;
}

static void append_to_list(array_info *a, operand & op, tree_node *tn, if_ops multi, int style)
{
    if((!op.is_instr()) || op.instr()->opcode() != multi ) {
        //    if(si->instr->opcode != multi && si->instr->opcode != io_cpy) {
        // if it's a copy, r2==nil_register and you recurse
        a->append(new access_vector(op, tn, style));
    } else {
        assert(op.instr()->opcode() == multi);
        in_rrr * ir = (in_rrr *)op.instr();
        append_to_list(a, ir->src1_op(), tn, multi, style);
        append_to_list(a, ir->src2_op(), tn, multi, style);
    }
}


