/* file "region.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"

//
// This file contains two categories of functions:
// 1. Array region disambiguation (for partitioning)
// 2. Array region size (for array promotion)
//

// 1. Array region disambiguation *****************************************
//
// Reference: ~saman/shadow/interproc/apdriver.cc and summary.cc
//

boolean equiv(operand o1, operand o2);
boolean equiv(instruction *i1, instruction *i2);
boolean intersect(instruction *i1, instruction *i2);

static boolean overlaps(immed i1, immed i2);
static boolean overlaps(sym_addr sa1, sym_addr sa2);
static boolean intersect_ai(in_array *ia1, in_array *ia2);
static named_lin_ineq *include_fors(tree_for *forlist[], int number);
static void append_to_list(array_info *a, operand op, tree_node *tn, if_ops multi, int style);

// 1. Array region disambiguation *****************************************
//
// Public functions
//

boolean equiv(operand o1, operand o2)
{
    if (o1.kind() == o2.kind()) {
        if (o1.is_symbol()) {
	    return (o1.symbol() == o2.symbol());
        } else if (o1.is_instr()) {
            return equiv(o1.instr(), o2.instr());
        } else return TRUE;
    }
    return FALSE;
}

boolean equiv(instruction *i1, instruction *i2)
{
    operand src1, src2;
    
    // Preprocessing: skip io_cpy's
    while (i1->opcode() == io_cpy) {
	src1 = ((in_rrr*)i1)->src1_op();
	if (src1.is_symbol()) {
	    while (i2->opcode() == io_cpy) {
		src2 = ((in_rrr*)i2)->src1_op();
		if (src2.is_symbol()) {
		    return (src1.symbol() == src2.symbol());
		} else {
		    i2 = src2.instr();
		}
	    }
	    return FALSE;
	} else {
	    i1 = src1.instr();
	}
    }
    while (i2->opcode() == io_cpy) {
	src2 = ((in_rrr*)i2)->src1_op();
	if (src2.is_symbol()) {
	    return FALSE;
	} else {
	    i2 = src2.instr();
	}
    }

    // Quick check first
    if (i1->opcode() != i2->opcode() || i1->num_srcs() != i2->num_srcs()) {
	return FALSE;
    }

    switch (i1->opcode()) {
      case io_ldc:
	if ( ((in_ldc *)i1)->value() != ((in_ldc *)i2)->value() ) {
            return FALSE;
	} else {
	    return TRUE;
	}

      case io_array: {
        in_array *ia1 = (in_array*)i1, *ia2 = (in_array*)i2;

	// sanity check
        if (ia1->elem_size() != ia2->elem_size() || ia1->offset() != ia2->offset()) {
            return FALSE;
	}
      }

      default:
	for (int n = 0; n < (int)i1->num_srcs(); n++) {
	    src1 = i1->src_op(n);
	    src2 = i2->src_op(n);

	    if (src1.kind() != src2.kind()) return FALSE;
	    if (src1.is_symbol()) {
		if (src1.symbol() != src2.symbol()) return FALSE;
	    } else if (src1.is_instr()) {
		if (!equiv(src1.instr(), src2.instr())) return FALSE;
	    }
	}
	return TRUE;
    }
}

// With array indices disambiguation
//
boolean intersect(instruction *i1, instruction *i2) 
{
    int n;
    operand src1, src2;
    
    if (i1->opcode() != i2->opcode() || i1->num_srcs() != i2->num_srcs())
        return FALSE;

    switch (i1->opcode()) {
      case io_ldc:      // check for ldc immediates 
        if (overlaps( ((in_ldc *)i1)->value(), ((in_ldc *)i2)->value() ))
            return TRUE;
        else return FALSE;
      case io_array: {
        in_array *ia1 = (in_array*)i1, *ia2 = (in_array*)i2;
        // Check base_op
        var_sym *sn1, *sn2;
        if ((sn1 = get_sym_of_array(ia1)) && (sn2 = get_sym_of_array(ia2))) {
            if (!sn1->overlaps(sn2)) return FALSE;
            else if (sn1 != sn2) return TRUE; // be conservative
        } else return TRUE; // be conservative now. Can return FALSE?

/*      // Check bounds
        if (ia1->dims() != ia2->dims() || ia1->elem_size() != ia2->elem_size()
            || ia1->offset() != ia2->offset()) return TRUE;
        for (n = 0; n < ia1->dims(); n++) {
            if (!equiv(ia1->bound(n), ia2->bound(n))) return TRUE;
        }
        
        // Check indices (hacking)
        for (n = 0; n < ia1->dims(); n++) {
            src1 = ia1->index(n);
            src2 = ia2->index(n);
            if (src1.kind() == src2.kind()) {
                switch (src1.kind()) {
                  case OPER_INSTR:
                    instruction *t1 = src1.instr(), *t2 = src2.instr();
                    if (t1->opcode() == t2->opcode() && t1->opcode() == io_ldc) {
                        if (((in_ldc *)t1)->value() != ((in_ldc *)t2)->value())
                            return FALSE;
                        else continue;
                    } else break;
                    break;
                  case OPER_SYM:
                  default:
                    break;
                }
            } else break;
        }*/
        // at this point, knows that ia1 and ia2 has the same base symbol
        return intersect_ai(ia1, ia2);
      }
        
      default:
        for (n = 0; n < (int)i1->num_srcs(); n++) {
            src1 = i1->src_op(n);
            src2 = i2->src_op(n);
            if (src1.kind() != src2.kind()) return FALSE;
            switch (src1.kind()) {
              case OPER_SYM:
                if (!src1.symbol()->overlaps(src2.symbol())) return FALSE;
                break;
              case OPER_INSTR:
                if (!intersect(src1.instr(), src2.instr())) return FALSE;
                break;
              default:
                continue;
            }
        }
        return TRUE;
    }
}

// 1. Array region disambiguation ***************************************
//
// Private functions
//

static boolean overlaps(sym_addr sa1, sym_addr sa2)
{
    if (!sa1.symbol()->is_var() || !sa2.symbol()->is_var()) return FALSE;
    
    var_sym *s1 = (var_sym*) sa1.symbol();
    var_sym *s2 = (var_sym*) sa2.symbol();
    if (s1->root_ancestor() != s2->root_ancestor()) return FALSE;

    int offset1 = s1->root_offset()+sa1.offset();
    int offset2 = s2->root_offset()+sa2.offset();
    if (offset1 <= offset2)
        return ((offset1 + s1->type()->size()) > offset2);
    else
        return ((offset2 + s2->type()->size()) > offset1);
}

static boolean overlaps(immed i1, immed i2)
{
    if (i1.is_symbol() && i2.is_symbol()) {
        return overlaps(i1.addr(), i2.addr());
    } else return (i1==i2);
}

//
// Reference: ~saman/shadow/interproc/apdriver.cc and summary.cc
//

#define MAXDEPTH 20

// ia2 is a single elment array access, but we do not want to take advantage
// of it
// In this version, be conservative: doing dim by dim
//   
static boolean intersect_ai(in_array *ia1, in_array *ia2)
{
    /* Recognize the for-nest and generate lin_ineqs */
    tree_for *forlist1[MAXDEPTH], *forlist2[MAXDEPTH];

    tree_node *tn = ia1->parent()->parent()->parent();
    int i1;
    for (i1 = 0; tn && tn->parent(); tn = tn->parent()->parent()) {
        if (tn->kind() == TREE_FOR) {
            forlist1[i1++] = (tree_for*)tn;
            if (i1 == MAXDEPTH) break;
        }
    }
    tn = ia2->parent()->parent()->parent();
    int i2;
    for (i2 = 0; tn && tn->parent(); tn = tn->parent()->parent()) {
        if (tn->kind() == TREE_FOR) {
            forlist1[i2++] = (tree_for*)tn;
            if (i2 == MAXDEPTH) break;
        }
    }
    named_lin_ineq *c;
    if (i1 >= i2) c = include_fors(forlist1, i1);
    else c = include_fors(forlist2, i2);

    /* Reference: from ap_driver::include_array() */
    array_info *ai1 = new array_info(ia1, TRUE);
    array_info *ai2 = new array_info(ia2, TRUE);
    array_info_iter iter1(ai1);
    array_info_iter iter2(ai2);
    while (!iter1.is_empty()) {
        access_vector *av1 = iter1.step();
        access_vector *av2 = iter2.step();

        named_lin_ineq *tc1 = named_lin_ineq::mk_named_lin_ineq(*av1, immed("foo"), FALSE);
        named_lin_ineq *tc2 = named_lin_ineq::mk_named_lin_ineq(*av2, immed("foo"), FALSE);
        if (tc1 && tc2) {
                    *tc1 &= tc1->ineqs()*-1;
                    *tc2 &= tc2->ineqs()*-1;
                    named_lin_ineq tmp = *tc1 & *tc2;
                    if(c) tmp &= *c;
            if (~tmp) { // no intersection
            } else {
                            delete tc1;
                            delete tc2;
                            if(c) delete c;
                            return TRUE;
                        }
        }
                if(tc1) delete tc1;
                if(tc2) delete tc2;
    }
        if(c) delete c;
    return FALSE;
}

static named_lin_ineq *include_fors(tree_for *forlist[], int number)
{
    if (number == 0) return NULL;
    named_lin_ineq *c = include_for(forlist[0]);
    for (int i = 1; i < number; i++) {
        c = include_for(forlist[i], c);
    }
    return c;
}

static void append_to_list(array_info *a, operand op, tree_node *tn, 
			   if_ops multi, int style)
{
    if((!op.is_instr()) || op.instr()->opcode() != multi ) {
        //    if(si->instr->opcode != multi && si->instr->opcode != io_cpy) {
        // if it's a copy, r2==nil_register and you recurse
        a->append(new access_vector(op, tn, style));
    } else {
        assert(op.instr()->opcode() == multi);
        in_rrr * ir = (in_rrr *)op.instr();
        append_to_list(a, ir->src1_op(), tn, multi, style);
        append_to_list(a, ir->src2_op(), tn, multi, style);
    }
}

// 2. Array region size ***************************************************
//
// Reference: ~saman/hawg/dependence/named_sc_dep.cc and foo.cc
//

region_types region_type(instruction *i);
region_types region_type(in_array *ia);
boolean is_modified(in_array *ia);

// Reference: dependence library
tree_for *find_inner(tree_node *n, var_sym * v);
tree_for *is_index(var_sym * v, tree_node *tn);
static boolean is_modified(in_array * ia, name_table & nt);
static boolean is_var_modified(in_array * ia, immed im);
static int num_variant_or_small_size(in_array *ia);
#if 0
// This function is not currently used but may be useful someday.
static int num_variant(in_array *ia);
#endif

// 2. Array region size ***************************************************
//
// Public functions
//

region_types region_type(instruction *i)
{
    while (i->opcode() == io_cpy) {
	operand src = ((in_rrr*)i)->src1_op();
	if (src.is_symbol()) {
	    return REGION_SCALAR;
	} else {
	    i = src.instr();
	}
    }
    if (i->opcode() != io_array) {
	return REGION_MESSY;
    } else {
	return region_type((in_array*)i);
    }
}

region_types region_type(in_array *ia)
{
    int count = num_variant_or_small_size(ia);
    if (count == (int)ia->dims()) {
	return REGION_FULL;
    } else {
	return REGION_SUB;  // return count;
    }
}

boolean is_modified(in_array * ia)
{
    array_info ai(ia, TRUE);

    array_info_iter aii(&ai);
    while(!aii.is_empty()) {
        access_vector *av = aii.step();
        named_lin_ineq * ineq = named_lin_ineq::mk_named_lin_ineq(*av, immed("foo"), FALSE);
        if(ineq == NULL) return TRUE; // handles a[b[i]] case ?
        if(is_modified(ia, ineq->names())) return TRUE;
    }
    return FALSE;
}

// 2. Array region size ***************************************************
//
// Private functions
//

static boolean is_var_modified(in_array * ia, immed im)
{
//    if(!im.is_symbol()) return FALSE;
    if(!im.symbol()->is_var()) return FALSE;
    if(is_index((var_sym *)im.symbol(), ia->parent())) return TRUE;//FALSE;
    tree_for * tf = find_inner(ia->parent(), (var_sym *)im.symbol());
    if(tf == NULL) return FALSE;
    return TRUE;
}

static boolean is_modified(in_array * ia, name_table & nt)
{
    for(int i=1; i<nt.n(); i++) {
        immed im(nt[i].name());
        if(im.is_symbol())
            if(is_var_modified(ia, im)) return TRUE;
    }
    return FALSE;
}

static int num_variant_or_small_size(in_array *ia)
{
    int count = 0;
    array_info ai(ia, TRUE);

    array_info_iter aii(&ai);
    int i = 0;
    while(!aii.is_empty()) {
        access_vector *av = aii.step();
        // "count" is the number of dimensions that either have small
        // sizes or their index functions are loop variant.

        // small-sized dimension: if (ia->bound(i++) < option['s']) continue; 
        operand b = ia->bound(i++);
        if (b.is_instr() && b.instr()->format() == inf_ldc) {
            immed im = ((in_ldc*) b.instr())->value();
            if (im.is_integer() && im.integer() <= option['s']) { // small dim
                count++;
                continue;
            }
        }
                
        named_lin_ineq * ineq = named_lin_ineq::mk_named_lin_ineq(*av, immed("foo"), FALSE);
        if(ineq == NULL) { count++; continue; }
        if(is_modified(ia, ineq->names())) count++;
    }
    return count;
}

#if 0
// This function is not currently used but may be useful someday.
static int num_variant(in_array *ia)
{
    int count = 0;
    array_info ai(ia, TRUE);

    array_info_iter aii(&ai);
    while(!aii.is_empty()) {
        access_vector *av = aii.step();
        // "count" is the number of dimensions whose index functions are
        // loop variant.
        named_lin_ineq * ineq = named_lin_ineq::mk_named_lin_ineq(av, immed("foo"), FALSE);
        if(ineq == NULL) { count++; continue; }
        if(is_modified(ia, ineq->names())) count++;
    }
    return count;
}
#endif
