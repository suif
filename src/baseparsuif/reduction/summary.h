/* file "summary.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef SUM_H
#define SUM_H

#include "reduction.h"

DECLARE_LIST_CLASS(in_list, instruction *);

// We don't find reductions across block boundaries

//
// class summarySym
//

class summarySym {
    friend class summaryFor;
  public:
    summarySym(summarySym *s);
    summarySym(var_sym *k, reflist *i, reduction_types x = REDUCTION_UNKNOWN);
    ~summarySym();
    void propagate(summarySym *s);
    int summarize(in_list *calls);
    void promote_and_generate(tree_for *tf);
    void generate(tree_for *tf);
    void generate_sub(tree_for *tf);

    inline region_types region_type() const;
    inline reduction_types reduction_type() const;
    inline void reduction_type(reduction_types x);
    inline boolean reducible() const;
    
    var_sym *key;
    reflist *info;
    annote_list_base *wilbyr_al;
    
  private:
    int src_map(operand src, instruction *i, reference *dst, reduction_types aType);
    boolean in_modref_syms(in_list *calls);

    reduction_types redtype;
};

//
// class instrRef: to be removed soon
//

class instrRef {
  public:
    instruction *key;
    reference *info;
    var_sym *sym;
    instrRef(instruction *k, reference *i) {
        key = k; info = i;
    }
};

DECLARE_LIST_CLASSES(instrRef_list_base, instrRef_e, instrRef_iter, instrRef*);

class instrRef_list : public instrRef_list_base {
  public:
    reference *find(instruction *k) {
	instrRef_iter iter(this);
	while (!iter.is_empty()) {
	    instrRef *x = iter.step();
	    if (x->key == k) return x->info;
	}
	return NULL;
    }
};

//
// class summaryFor
//

DECLARE_LIST_CLASSES(summaryFor_base, summaryFor_e, summaryFor_iter,
		     summarySym*);

class summaryFor : public summaryFor_base {
  public:
    summaryFor(tree_for *tfor);
    ~summaryFor();
    int propagate(summaryFor *inner);
    int map();
    summarySym *find(var_sym *vsym);
    
    tree_for *tf;
    in_list *calls;
    annote_list_base *wilbyr_al;
    
  private:
    int dst_map(tree_node_list *body);
    int check_in_write(operand &write, tree_instr *ti, reference **refer);
    int src_map(tree_node_list *body);
    int src_map(tree_instr* ti);
    int src_map(instruction *i, reference *dst, int a_type, boolean lhs);
    int src_map(operand src, instruction *i, reference *dst, int a_type,
		boolean lhs);
    int sym_map(var_sym *vsym, operand src, instruction *i, reference *dst,
		int a_type, boolean lhs);
    int sym_map(var_sym *vsym);
    int src_map_gen(in_cal *i, reference *dst, int a_type, void *data,
		    boolean lhs);
    int dst_map_gen(in_cal *ic, int a_type, void *data);

    instrRef_list *cache;
};

#endif

