/* file "reference.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include "reduction.h"

static int reducible(reduction_types x) { return (x > REDUCTION_NONE); }

reference::reference(operand w, instruction *iw, int nr, reduction_types redt,
                     region_types regt, var_sym *s)
{
    // public variables
    write = w;
    i_write = iw;
    i_read = NULL;
    num_read = nr;

    // private variables
    reduction_type(redt);
    region_type(regt);
    promoted_to = s;
}

reference::~reference()
{
}

void reference::check_in_read(instruction* i, operand src, reduction_types a_type)
{
    int too_messy;
    
    if (reducible()) {
	if (option['d'-'\0'] >= 2) {
	    printf("Irreducible (more than one reducible reads): ");
	    get_sym(write, &too_messy)->print();
	    printf("\n");
	    fflush(stdout);
	}
	reduction_type(REDUCTION_NONE);
	return;
    }
    
    i_read = i;
    read = src;
    if (++num_read > 1) {
	if (option['d'-'\0'] >= 2) {
	    printf("Irreducible (more than one irreducible read): ");
	    get_sym(write, &too_messy)->print();
	    printf("\n");
	    fflush(stdout);
	}
        reduction_type(REDUCTION_NONE);
    } else if (::reducible(a_type)) {
        reduction_type(a_type);
    } else {
        reduction_type(REDUCTION_NONE);
    }
}

static char *s[5] = { "", "sum", "product", "min", "max" };

void reference::generate(tree_for *tf, var_sym *vsym)
{
    const char *k_;
    immed_list *iml;

    assert(reducible());
    if (!reducible_gen()) {
	// [reduction: "sum" snode]
	k_ = k_reduction;

	iml = new immed_list;
        iml->append(immed(s[redtype]));

	if (!vsym) {
	    int too_messy;
	    vsym = get_sym(write, &too_messy);
	}
	assert_msg(vsym, ("Cannot find symbol"));
	iml->append(immed(vsym));

	if (option['d'-'\0'] >= 2) {
	    printf("Reducible (%s-type): ", s[redtype]);
	    vsym->print();
	    printf("\n");
	    fflush(stdout);
	}

	annotate(tf, k_, iml, 1, vsym);
	
    } else {
	// [reduction_gen: proc_sym i_write->clone() var_sym var_sym ...] 
	k_ = k_reduction_gen;

	if (!assoc_refs[redtype]) return;

	iml = new immed_list;
	sym_node *psym = get_proc_sym_of_cal((in_cal*)i_write);
//	if (search_annote(tf->annotes(), k_, 0, psym)) {  // visited already
//	    return;
//	}
	iml->append(immed(psym));

	iml->append(i_write->clone());

	if (option['d'-'\0'] >= 2) {
	    printf("Generalized Reducible (");
	    psym->print();
	    printf(", %d):", redtype);
	}

	reflist_iter ri(assoc_refs[redtype]);
	while (!ri.is_empty()) {
	    reference *r = ri.step();
	    assert(region_type() != REGION_SINGLE);
	    int too_messy;
	    sym_node *snode = get_sym(r->write, &too_messy);
	    assert_msg(snode, ("Cannot find symbol"));
	    iml->append(immed(snode));

	    if (option['d'-'\0'] >= 2) {
		snode->print();
		printf("  ");
	    }
	}

	if (option['d'-'\0'] >= 2) {
	    printf("\n");
	    fflush(stdout);
	}

	annotate(tf, k_, iml, 0, psym);
    }
}

var_sym *reference::promote(tree_for *tfor)
{
    in_rrr *i_write = (in_rrr*)this->i_write;
    in_rrr *i_read = (in_rrr*)this->i_read;
    
    /* Replace write with promoted_to */
    instruction *itmp, *itmp1;
    var_sym *sss;
    write.instr()->remove();
    if (i_write->opcode() == io_str) {
        if (i_write->src_op().is_instr()) {
            itmp = i_write->src_op().instr();
            itmp->remove();
            i_write->set_opcode(io_cpy);
            i_write->set_src1(operand(itmp));
        } else {
            sss = i_write->src_op().symbol();
            i_write->set_opcode(io_cpy);
            i_write->set_src1(operand(sss));
        }
    } else if (i_write->opcode() == io_memcpy) {
        if (i_write->src_addr_op().is_instr()) {
            itmp = i_write->src_addr_op().instr();
            itmp->remove();
            i_write->set_opcode(io_lod);
            i_write->set_src_addr_op(operand(itmp));
        } else {
            sss = i_write->src_addr_op().symbol();
            i_write->set_opcode(io_lod);
            i_write->set_src_addr_op(operand(sss));
        }
    } else {
	assert_msg(0, ("Opcode error"));
    }
    i_write->set_result_type(promoted_to->type());
    i_write->set_src2(operand());
    i_write->set_dst(operand(promoted_to));
    
    /* Replace read with promoted_to */
    /*
    for (int n = 0; n < i_read->num_srcs(); n++) {
        operand src = i_read->src_op(n);
        if (src == read) break;
    }
    assert(n != i_read->num_srcs());
    read.instr()->remove();
    i_read->set_src_op(n, operand(promoted_to));
    */
    if (i_read->opcode() == io_lod) {
        if (i_read->src_addr_op().is_instr()) {
            itmp = i_read->src_addr_op().instr();
            itmp->remove();
        }
        i_read->set_opcode(io_cpy);
        i_read->set_src1(operand(promoted_to));
        i_read->set_src2(operand());
    } else if (i_read->opcode() == io_memcpy) {
        if (i_read->src_addr_op().is_instr()) {
            itmp = i_read->src_addr_op().instr();
            itmp->remove();
            assert(i_read->dst_addr_op().is_instr()); // XXX
            itmp1 = i_read->dst_addr_op().instr();
            itmp1->remove();
            i_read->set_opcode(io_str);
            i_read->set_src_op(2, operand(promoted_to));
            i_read->set_dst_addr_op(operand(itmp1));
            i_read->set_dst(operand());
        } else {
            assert(i_read->dst_addr_op().is_instr());
            itmp1 = i_read->dst_addr_op().instr();
            itmp1->remove();
            i_read->set_opcode(io_str);
            i_read->set_src_op(2, operand(promoted_to));
            i_read->set_dst_addr_op(operand(itmp1));
            i_read->set_dst(operand());
        }
    } else {
	assert_msg(0, ("Opcode error"));
    }

    /* Insert pre instruction and post instruction */
    /* A. calculate the outmost for */
    tree_for *outfor;
    tree_node *tn = (tree_node*)tfor;
    for ( ; tn && tn->parent(); tn = tn->parent()->parent()) {
        if (tn->kind() == TREE_FOR) outfor = (tree_for*)tn;
    }
    assert(outfor);

    /* B. insert pre and post */
    in_rrr *pre = new in_rrr(io_lod, promoted_to->type(), operand(promoted_to), read);
    in_rrr *post = new in_rrr(io_str, type_void, operand(), write, operand(promoted_to));

    tree_node_list *tnl = outfor->parent();
//  tree_node_list_e *pos = tnl->lookup(outfor);
//  assert(pos);
    tree_node *t1 = new tree_instr(pre);
    tree_node *t2 = new tree_instr(post);
    tnl->insert_before(new tree_node_list_e(t1), outfor->list_e());
    tnl->insert_after(new tree_node_list_e(t2), outfor->list_e());

    return promoted_to;
}

region_types reference::region_type()
{
    if (regtype == REGION_UNKNOWN) {  // Compute regtype
	switch (write.kind()) {
	  case OPER_SYM:
	    region_type(REGION_SCALAR);
	    break;
	    
	  case OPER_INSTR:
	    region_type(::region_type(write.instr()));
	    break;

	  case OPER_NULL:
	    region_type(REGION_NONE);
	    break;

	default:
	    assert(0);
	}
    }
    return regtype;
}

boolean reference::equiv(reference *ref)
{
    return (ref) ? ::equiv(write, ref->write) : FALSE;
}

// class reference ********************************************************
//
// private methods
//

void reference::annotate(tree_for *tf, const char *k_, immed_list *iml, int loc, sym_node *snode)
{
    immed_list *iml0;
    annote *an;

    if (option['a'-'\0'] && ((var_sym*)snode)->type()->unqual()->is_array()) {
	return;
    }
    
    // i_write
    //
    if (!search_annote(i_write->annotes(), k_, loc, snode)) {
        an = new annote(k_, iml);
        i_write->annotes()->push(an);
    }

    // i_read
    //
    if (!search_annote(i_read->annotes(), k_, loc, snode)) {
        iml0 = new immed_list;
        iml0->copy(iml);

	immed_list_e *pivot = iml0->head();
	while (pivot) {
	    immed im = pivot->contents;
	    if (im.is_instr()) {
		pivot->contents = immed((im.instr())->clone());
	    }
	    pivot = pivot->next();
	}
	
        an = new annote(k_, iml0);
        i_read->annotes()->push(an);
    }
    
    // tree_for:
    // Should be able to remove the if-test here.
    // But then duplicate annotes found.
    //
    if (!search_annote(tf->annotes(), k_, loc, snode)) { 
        iml0 = new immed_list;
        iml0->copy(iml);

	immed_list_e *pivot = iml0->head();
	while (pivot) {
	    immed im = pivot->contents;
	    if (im.is_instr()) {
		pivot->contents = immed((im.instr())->clone());
	    }
	    pivot = pivot->next();
	}
	
        an = new annote(k_, iml0);
        tf->annotes()->push(an);
    }
}

