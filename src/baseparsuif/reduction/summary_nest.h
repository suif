/* file "summary_nest.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#ifndef SUM_NEST_H
#define SUM_NEST_H

#include <suif_copyright.h>

/* Document:

   This file implements the classes "summary_nest", which is
   associated with every loop nest.
 */

#include "reduction.h"

/* summary_nest */

class loop;

DECLARE_LIST_CLASS(summary_nest, loop *);

class loop {
  public:
    loop(tree_for *tf, loop *par = NULL);
    void for_map(tree_node_list *body);
    void dfs();
    summary *lookup(tree_for *tf);
    ~loop();
    tree_for *key;
    summary *info;

  private:
    void abort_path();
    summary_nest *children;
    loop *parent;
    int abort;
};   

#endif

