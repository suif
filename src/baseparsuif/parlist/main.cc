/* file "main.cc" of the parlist program for SUIF */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for the parlist program for
 *  SUIF.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/07/07 05:09:38 brm Exp $")

INCLUDE_SUIF_COPYRIGHT


static const char *k_doall;
static FILE *output_fp = NULL;


static void usage(void);
static void list_on_proc(tree_proc *the_proc);
static void list_on_object(suif_object *the_object);


extern int main(int argc, char *argv[])
  {
    static char *output_file_name = NULL;
    static cmd_line_option option_table[] =
      {
        {CLO_STRING, "-o", NULL, &output_file_name}
      };

    start_suif(argc, argv);

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    if (argc < 2)
        usage();

    if (output_file_name != NULL)
      {
        fileset->set_max_open_files(fileset->get_max_open_files() - 1);
        output_fp = fopen(output_file_name, "w");
        if (output_fp == NULL)
          {
            error_line(1, NULL, "unable to open file \"%s\" for output",
                       output_file_name);
          }
      }
    else
      {
        output_fp = stdout;
      }

    ANNOTE(k_doall, "doall", TRUE);

    for (int arg_num = 1; arg_num < argc; ++arg_num)
        fileset->add_file(argv[arg_num], NULL);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = fse->next_proc();
            if (this_proc == NULL)
                break;
            this_proc->read_proc(FALSE, FALSE);
            list_on_proc(this_proc->block());
            this_proc->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }


static void usage(void)
  {
    fprintf(stderr, "usage: %s <infile> { <infile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void list_on_proc(tree_proc *the_proc)
  {
    walk(the_proc, &list_on_object);
  }

static void list_on_object(suif_object *the_object)
  {
    if (!the_object->is_tree_obj())
        return;
    tree_node *the_node = (tree_node *)the_object;
    if (!the_node->is_for())
        return;
    tree_for *the_for = (tree_for *)the_node;
    if (the_for->peek_annote(k_doall) == NULL)
        return;
    fprintf(output_fp,
            "Doall loop: loop on `%s' at line %u of source file \"%s\".\n",
            the_for->index()->name(), source_line_num(the_for),
            source_file_name(the_for));
  }
