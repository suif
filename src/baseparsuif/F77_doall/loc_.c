/* file "loc_.c" */

/*
 *  This file contains an implementation of the FORTRAN LOC() routine needed
 *  by the SPICE Perfect Club benchmark but not implemented in the standard
 *  f2c library.  --csw 9/13/93
 */

int loc_(ivar)
    int *ivar;
  {
    return (int)ivar;
  }

