#include "stdio.h"
#include "sf2c.h"

VOID s_stop(s, n)
char *s;
long int n;
{
int i;

if(n > 0)
	{
	fprintf(stderr, "STOP ");
	for(i = 0; i<n ; ++i)
		putc(*s++, stderr);
	fprintf(stderr, " statement executed\n");
	}
suif_exit_log();  /* from runtime.c, notify other threads of Fortran STOP */
f_exit();
exit(0);
}
