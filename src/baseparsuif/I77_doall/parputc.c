#include <stdio.h>
#include <runtime.h>

#define MAXBUFFERSIZE           2048

int any_parallel_io = 0;
static int index[DEFAULT_MAXPROC];
static char buffer[DEFAULT_MAXPROC][MAXBUFFERSIZE];
static FILE *  fp[DEFAULT_MAXPROC][MAXBUFFERSIZE];


flush_putc()
{
    int p;
    int i;
    if(any_parallel_io) {
	if(suif_doall_level() == 0) {
	    for(p=0; p<DEFAULT_MAXPROC; p++) {
		for(i=0; i<index[p]; i++)
		    putc(buffer[p][i], fp[p][i]);
		index[p] = 0;
	    }
	    any_parallel_io = 0;
	}
    }
}
	    

int par_putc(c, stream)
 char c;
 FILE *stream;
{
    if(suif_doall_level()) {
	int i = suif_get_my_id();
	any_parallel_io = 1;
	if(index[i]>=MAXBUFFERSIZE) {
	    fprintf(stderr, "parallel I/O Buffer overflow, %d is too small", MAXBUFFERSIZE);
	    return;
	}
	buffer[i][index[i]] = c;
	fp[i][index[i]] = stream;
	index[i]++;
    } else {
	putc(c, stream);
    }
}

