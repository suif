/* file "partune.c" */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

/* #include <suif_copyright.h> */

/*
 *  This is a stand-alone program written in C instead of C++ for
 *  portability.  It is a shell to run a program that has gone through
 *  the SUIF parallelizer to generate feedback information to tune the
 *  parallelization.
 */

/* Chris Wilson */


#define boolean __boolean
#define TRUE __TRUE
#define FALSE __FALSE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

#undef boolean
#undef TRUE
#undef FALSE

typedef int boolean;
#define TRUE 1
#define FALSE 0


#define STDERR_TMP_FILE_NAME "stderr_tmp"
#define INIT_NAME_BUF_SIZE 30
#define INIT_FUNC_TABLE_SIZE 30


struct func_data
  {
    char *func_name;
    double best_time;
    unsigned long best_num_procs;
  };


static char **child_envp;
static struct func_data *func_table = NULL;
static unsigned long func_table_size = 0;


static void gather_single_run_stats(char *program, char *test_args[],
                                    unsigned long num_procs);
static char **modify_environment(char **original_environment,
                                 char *variable_name, char *new_value);
static void run_test(char *program, char *test_args[], char *stderr_out_file);
static void record_data(char *function_name, double time,
                        unsigned long num_procs);
static void write_data(void);
static void skip_past_star_line(FILE *fp);
static void bad_stderr_format(void);


extern int main(int argc, char *argv[], char *envp[])
  {
    char *maxproc_string;
    int count;
    unsigned long max_procs;
    unsigned long num_procs;

    if (argc < 2)
      {
        fprintf(stderr, "No program name was given.\n");
        exit(1);
      }
    child_envp = envp;

    maxproc_string = getenv("PL_MAXPROC");
    if (maxproc_string == NULL)
      {
        fprintf(stderr, "The PL_MAXPROC environment variable is not set.\n");
        exit(1);
      }
    count = sscanf(maxproc_string, "%lu", &max_procs);
    if (count != 1)
      {
        fprintf(stderr,
                "The value of the PL_MAXPROC environment variable is not a "
                "legal integer.\n");
        exit(1);
      }
    for (num_procs = 0; num_procs < max_procs; ++num_procs)
        gather_single_run_stats(argv[1], &(argv[1]), num_procs + 1);
    write_data();
    return 0;
  }


static void gather_single_run_stats(char *program, char *test_args[],
                                    unsigned long num_procs)
  {
    char num_threads_buffer[sizeof(unsigned long) * 3 + 2];
    FILE *output_fp;
    int inchar;

    sprintf(num_threads_buffer, "%ld", num_procs);
    child_envp =
            modify_environment(child_envp, "PL_NUM_THREADS",
                               num_threads_buffer);

    run_test(program, test_args, STDERR_TMP_FILE_NAME);

    output_fp = fopen(STDERR_TMP_FILE_NAME, "r");
    if (output_fp == NULL)
      {
        fprintf(stderr, "The attempt to open `%s' for reading failed.\n",
                STDERR_TMP_FILE_NAME);
        exit(1);
      }

    skip_past_star_line(output_fp);
    skip_past_star_line(output_fp);
    skip_past_star_line(output_fp);

    while (TRUE)
      {
        static char *name_buffer = NULL;
        static unsigned long name_buffer_length = 0;
        unsigned long name_position;
        int count;
        double time;
        char charvalue;

        inchar = fgetc(output_fp);
        if (inchar != ' ')
            bad_stderr_format();
        inchar = fgetc(output_fp);
        if (inchar == '*')
          {
            inchar = fclose(output_fp);
            if (inchar != 0)
              {
                fprintf(stderr,
                        "The attempt to close `%s' after reading failed.\n",
                        STDERR_TMP_FILE_NAME);
                exit(1);
              }
            inchar = unlink(STDERR_TMP_FILE_NAME);
            if (inchar != 0)
              {
                fprintf(stderr,
                        "The attempt to remove `%s' after reading failed.\n",
                        STDERR_TMP_FILE_NAME);
                exit(1);
              }
            return;
          }
        else if (inchar != '`')
          {
            bad_stderr_format();
          }

        if (name_buffer == NULL)
          {
            name_buffer = malloc(INIT_NAME_BUF_SIZE);
            name_buffer_length = INIT_NAME_BUF_SIZE;
          }
        name_position = 0;
        while (TRUE)
          {
            inchar = fgetc(output_fp);
            if (inchar == EOF)
                bad_stderr_format();
            if (inchar == '\'')
                break;
            name_buffer[name_position] = inchar;
            ++name_position;
            if (name_position == name_buffer_length)
              {
                char *new_buffer;

                new_buffer = malloc(name_buffer_length * 2);
                memcpy(new_buffer, name_buffer, name_buffer_length);
                free(name_buffer);
                name_buffer = new_buffer;
                name_buffer_length *= 2;
              }
          }
        name_buffer[name_position] = 0;

        count = fscanf(output_fp, " Time = %lf", &time);
        if (count != 1)
            bad_stderr_format();
        count = fscanf(output_fp, " sec%c", &charvalue);
        if (count != 1)
            bad_stderr_format();
        if (charvalue != '\n')
            bad_stderr_format();
        record_data(name_buffer, time, num_procs);
      }
  }

static char **modify_environment(char **original_environment,
                                 char *variable_name, char *new_value)
  {
    char **follow;
    size_t var_length;
    char **new_environment;
    size_t num_vars;
    char *new_string;

    var_length = strlen(variable_name);
    new_string = malloc(var_length + strlen(new_value) + 2);
    sprintf(new_string, "%s=%s", variable_name, new_value);

    follow = original_environment;
    while (*follow != NULL)
      {
        if ((strncmp(*follow, variable_name, var_length) == 0) &&
            ((*follow)[var_length] == '='))
          {
            *follow = new_string;
            return original_environment;
          }
        ++follow;
      }

    num_vars = follow - original_environment;
    new_environment = malloc((num_vars + 2) * sizeof(char *));
    memcpy(new_environment, original_environment, num_vars * sizeof(char *));
    new_environment[num_vars] = new_string;
    new_environment[num_vars + 1] = NULL;
    return new_environment;
  }

static void run_test(char *program, char *test_args[], char *stderr_out_file)
  {
    pid_t fork_result;
    boolean stopped;
    boolean redo;

    stopped = FALSE;
    redo = FALSE;
    fork_result = 0;
    while (TRUE)
      {
        if (fork_result == 0)
            fork_result = fork();
        if (fork_result == 0)
          {
            int old_stderr_fd;
            int new_stderr_fd;
            int close_result;

            /* 2 is the file descriptor for stderr */
            old_stderr_fd = dup(2);
            if (old_stderr_fd == -1)
              {
                perror("The attempted dup() of stderr failed");
                exit(1);
              }
            close_result = close(2);
            if (close_result != 0)
              {
                perror("The attempted close() of the old stderr failed");
                exit(1);
              }
            new_stderr_fd = creat(stderr_out_file, 0644);
            if (new_stderr_fd != 2)
              {
#define error_string "The attempt to open the temporary stderr file failed.\n"
                write(old_stderr_fd, error_string, strlen(error_string));
#undef error_string
                exit(1);
              }
            execve(program, test_args, child_envp);
            write(old_stderr_fd, "execvp() failed.\n",
                  strlen("execvp() failed.\n"));
            exit(1);
          }
        else
          {
            pid_t wait_result;
            int return_status;

            wait_result = wait(&return_status);
            if (wait_result != fork_result)
              {
                fprintf(stderr, "wait() failed for test run.\n");
                exit(1);
              }
            else if (WIFEXITED(return_status))
              {
                if (WEXITSTATUS(return_status) == 0)
                  {
                    if (redo)
                      {
                        redo = FALSE;
                        stopped = FALSE;
                        fork_result = 0;
                      }
                    else
                      {
                        return;
                      }
                  }
                else
                  {
                    fprintf(stderr,
                            "Test run failed: test returned exit code %d.\n",
                            WEXITSTATUS(return_status));
                    exit(1);
                  }
              }
            else if (WIFSIGNALED(return_status))
              {
                fprintf(stderr, "Test run failed: test died with signal %d.\n",
                        WTERMSIG(return_status));
                exit(1);
              }
            else if (WIFSTOPPED(return_status))
              {
                fprintf(stderr,
                        "Test run stopped by signal %d; test will be "
                        "re-run.\n", WSTOPSIG(return_status));
                stopped = TRUE;
                redo = TRUE;
              }
#ifdef WIFCONTINUED
            /* WIFCONTINUED is not Posix */
            else if (WIFCONTINUED(return_status))
              {
                if (stopped)
                  {
                    stopped = FALSE;
                  }
                else
                  {
                    fprintf(stderr,
                            "Unexpected ``contune'' status returned from "
                            "wait().\n");
                    exit(1);
                  }
              }
#endif
            else
              {
                fprintf(stderr, "Confusing status returned by wait().\n");
                exit(1);
              }
          }
      }
  }

static void record_data(char *function_name, double time,
                        unsigned long num_procs)
  {
    struct func_data *follow;

    if (func_table == NULL)
      {
        func_table = malloc(INIT_FUNC_TABLE_SIZE * sizeof(struct func_data));
        func_table[0].func_name = NULL;
        func_table_size = INIT_FUNC_TABLE_SIZE;
      }
    follow = func_table;
    while (follow->func_name != NULL)
      {
        if (strcmp(follow->func_name, function_name) == 0)
          {
            if (time < follow->best_time)
              {
                follow->best_time = time;
                follow->best_num_procs = num_procs;
              }
            return;
          }
        ++follow;
      }
    if ((follow - func_table) == func_table_size - 1)
      {
        struct func_data *new_table;

        new_table = malloc(func_table_size * 2 * sizeof(struct func_data));
        memcpy(new_table, func_table,
               (func_table_size - 1) * sizeof(struct func_data));
        func_table = new_table;
        follow = &(func_table[func_table_size - 1]);
        func_table_size *= 2;
      }
    follow->func_name = malloc(strlen(function_name) + 1);
    strcpy(follow->func_name, function_name);
    follow->best_time = time;
    follow->best_num_procs = num_procs;
    ++follow;
    follow->func_name = NULL;
  }

static void write_data(void)
  {
    struct func_data *follow;

    if (func_table == NULL)
        return;
    follow = func_table;
    while (follow->func_name != NULL)
      {
        fprintf(stderr, "BESTPAR(\"%s\", %ld)\n", follow->func_name,
                follow->best_num_procs);
        ++follow;
      }
  }

static void skip_past_star_line(FILE *fp)
  {
    while (TRUE)
      {
        int inchar;

        inchar = fgetc(fp);
        if (inchar == ' ')
          {
            inchar = fgetc(fp);
            if (inchar == '*')
              {
                inchar = fgetc(fp);
                while (inchar == '*')
                    inchar = fgetc(fp);
                if (inchar != '\n')
                    bad_stderr_format();
                return;
              }
          }
        while (TRUE)
          {
            if (inchar == EOF)
                bad_stderr_format();
            if (inchar == '\n')
                break;
            inchar = fgetc(fp);
          }
      }
  }

static void bad_stderr_format(void)
  {
    fprintf(stderr, "The stderr of the test program has a bad format.\n");
    exit(1);
  }
