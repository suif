/* file "main.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <suif1.h>
#include <dependence.h>
#include <builder.h>
#include "predep.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>

const char * k_break_up;
boolean verbose = FALSE;
boolean do_norm = FALSE;
boolean do_presc = FALSE;

char * timestamp;
int num_expand = 2;



/***************************************************************************
 * pre-process each procedure for symbolic coefficients                    *
 ***************************************************************************/
void do_symconst(tree_proc * tp)
{
    // identify the current procedure for the builder
    block::set_proc(tp);                     

    immed_list * iml;
    // is depset_is_run annotation already in the input procedure??
    if ((iml = (immed_list *)tp->get_annote(k_depset_is_run))) {
        // if so print when it has been run.
        fprintf(stderr, 
                " Warning: the input file has previously "
                "been processed by predep -presc\n");
        immed_list_iter ili(iml);
        while(!ili.is_empty()) 
            fprintf(stderr, "   on %s", ili.step().string());
    } else
        iml = new immed_list;    // empty list

    // at the current timestamp to the list of timestamps
    iml->append(immed(timestamp));
    
    // put the depset_done annotation on tree_proc so that
    // 1) later predep passes will print a warning message
    // 2) dependence library will activate the test when
    //    symbolic coefficients are present.
    tp->append_annote(k_depset_is_run, iml);

    depset R;

    if(verbose) 
        printf("======== Processing %10s:           Pass I  ========\n", 
               tp->proc()->name());
    // First pass, identify the loop nests with symbolic coefficients
    R.find_and_mark(tp->body());
    // Second pass, duplicate those loop nests
    if(verbose) 
        printf("                                          Pass II ========\n");
    R.break_loops(tp->body());
}


void do_proc(tree_proc * tp)
{
    // Create access vectors, need to be run if dependence library is used
    fill_in_access(tp);

    if(do_norm)
        do_normalize(tp->body());

    if(do_presc)
        do_symconst(tp);

}


/***************************************************************************
 * Do initialization, and call the proc iterator                           *
 ***************************************************************************/
int main(int argc, char * argv[])
{
    // suif initializer
    start_suif(argc, argv);

    boolean found = TRUE;
    int opt_num = 1;
    while(found && (opt_num < argc)) {
        found = FALSE;
        if(strcmp(argv[opt_num], "-verbose") == 0) {
            verbose = TRUE;
            found = TRUE;
        } else if(strcmp(argv[opt_num], "-norm") == 0) {
            do_norm = TRUE;
            found = TRUE;
        } else if(strcmp(argv[opt_num], "-normalize") == 0) {
            do_norm = TRUE;
            found = TRUE;
        } else if(strcmp(argv[opt_num], "-presc") == 0) {
            do_presc = TRUE;
            found = TRUE;
        } else if(strcmp(argv[opt_num], "-presymconst") == 0) {
            do_presc = TRUE;
            found = TRUE;
        } else if(strcmp(argv[opt_num], "-expand") == 0) {
            num_expand = atoi(argv[++opt_num]);
            found = TRUE;
        }
        if(found) opt_num++;
    }

    if(!do_norm && !do_presc) {
        fprintf(stderr, 
                "No option given.\n"
                "Options are:\n"
                "   -normalize          Normalize the for loops\n"
                "   -presc              Pre-process to handle symbolic coefficients\n");
        exit(-1);
    }
                
    // initialize a local annotation used to communicate between the 2 passes
    ANNOTE(k_break_up, "breakup", FALSE);

    // create a time-stamp
    time_t tm;
    tm = time(NULL);
    timestamp = ctime(&tm);
    assert(timestamp[24] == '\n');
    timestamp[24] = 0;

    // iterate over all the procedures in the input files and 
    // produce output files.
    suif_proc_iter(argc - (opt_num - 1), &(argv[opt_num - 1]), do_proc, TRUE,
                   TRUE, TRUE);
    return 0;
}    

