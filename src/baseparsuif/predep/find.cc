/* file "find.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <stdlib.h>
#include <suif1.h>
#include <useful.h>
#include <suifmath.h>
#include <builder.h>
#include <dependence.h>
#include "predep.h"

/***************************************************************************
 *************************************************************************** 
 ****   First Pass                                                       ***
 *************************************************************************** 
 ***************************************************************************/

tree_for *is_index(var_sym * v, tree_node *tn);  // defined in dependence lib
tree_for *find_inner(tree_node *n, var_sym * v); // defined in dependence lib
tree_for *find_outermost(tree_node *n);          // defined below



/***************************************************************************
 * Iterate over a tree_node_list (see find_and_mark(tree_node *))          *
 ***************************************************************************/
void depset::find_and_mark(tree_node_list * tnl)
{
    tree_node_list_iter iter(tnl);
    while(!iter.is_empty()) {
        tree_node * tn = iter.step();
        find_and_mark(tn);
    }
}


/***************************************************************************
 * Iterate over all the control flow structure and call                    *
 * find_and_mark(instruction *) on each expression tree                    *
 ***************************************************************************/
void depset::find_and_mark(tree_node * tn)
{
    switch(tn->kind()) {
    case TREE_FOR: {
        tree_for * tnf = (tree_for *)tn;
        find_and_mark(tnf->lb_list());
        find_and_mark(tnf->ub_list());
        find_and_mark(tnf->step_list());
        find_and_mark(tnf->landing_pad());
        find_and_mark(tnf->body());
        break;
      }
        
    case TREE_IF: {
        tree_if * tni = (tree_if *)tn;
        find_and_mark(tni->header());
        find_and_mark(tni->then_part());
        find_and_mark(tni->else_part());
        break;
      }
        
    case TREE_LOOP: {
        tree_loop * tnl = (tree_loop *)tn;
        find_and_mark(tnl->body());
        find_and_mark(tnl->test());
        break;
      }
        
    case TREE_BLOCK: {
        tree_block * tnb = (tree_block *)tn;
        find_and_mark(tnb->body());
        break;
      }
        
    case TREE_INSTR: {
        tree_instr * tnin = (tree_instr *)tn;
        find_and_mark(tnin->instr());
        break;
      }

    default:
        assert(0);
        break;
    }
}


/***************************************************************************
 * Iterate over the expression tree. When an array instruction is found    *
 * check if any of the access functions are non-linear expressions.        *
 * if so call find_and_mark(in_array *, int) with the array instruction    *
 * and the array dimension where the access function is non-linear.        *
 ***************************************************************************/
void depset::find_and_mark(instruction * ins)
{
    for(unsigned i=0; i<ins->num_srcs(); i++) {
        operand op(ins->src_op(i));
        if(op.is_instr()) {
            find_and_mark(op.instr());
            if(op.instr()->opcode() == io_array) {
                in_array * ina = (in_array *)op.instr();
                array_info *ai = new array_info(ina, TRUE);

                array_info_iter iter(ai);
                int j = 0;
                while(!iter.is_empty()) {
                    access_vector * av = iter.step();
                    if(av->too_messy) {
                        find_and_mark(ina, j);
                    }
                    j++;
                }
            }
        }
    }
}



/***************************************************************************
 * Convert the access function to a linear inequality with symbolic        * 
 * coefficients.  If the access function cannot be converted into a liner  *
 * ineq with symbolic coefficients, it is beyond the dependence analizer's *
 * scope; thus give-up on that access                                      *
 * Find all the symbolic coefficients and mark the outermost loopnest so   * 
 * that the next pass will duplicate the nests to handle the symbolic      *
 * coefficients correctly                                                  *
 * (calls find_and_mark(tree_node * n, in_array *, var_sym *v))            *
 ***************************************************************************/
void depset::find_and_mark(in_array * ina, int i)
{
    operand op(ina->index(i));
    assert(op.is_instr()); // otherwise cannot be too messy
    instruction * acc = op.instr();
    named_symcoeff_ineq * ineq;
    ineq = named_symcoeff_ineq::convert_exp(acc);
    if(ineq) {
        if(verbose) {
            printf(" Dimension %d of %s [",
                   i, get_sym_of_array(ina)->name());
            ineq->print_exp(pet_system);
            printf("]  at line %d \n",                    
                   source_line_num(ina->parent()));
        }
        for(int i=1; i<ineq->p(); i++) 
            find_and_mark(ina->parent(), ina, ineq->planes()[i].var());
    } else {
        if(verbose) 
            printf(" Dimension %d of %s at line %d is too messy\n",
                   i, 
                   get_sym_of_array(ina)->name(),
                   source_line_num(ina->parent()));
                   
    }
}


/***************************************************************************
 * Find the outermost for loop and mark it with a annotation so that the   *
 * 2nd pass will duplicate the loop.                                       *
 * The annotation has a list of variable symbols that are used as symbolic *
 * coefficients within the loop nest                                          *
 ***************************************************************************/
void depset::find_and_mark(tree_node * n, in_array * ina, var_sym * v)
{
    tree_for * tnf;
    // if symbolc coefficient is a outer loop nest\'s induction variable.
    if ((tnf = is_index(v, n))) {
        if(verbose)
            printf("                Variable `%s' is the index "  
                   " of the loop at line %d\n", 
                   v->name(), 
                   source_line_num(tnf));
        return;
    }

    // if symbolic coefficient is modified within the loop nest.
    if ((tnf = find_inner(n, v))) {
        if(verbose)
            printf("                Variable `%s' is assigned in the loop"
                   " at line %d\n", 
                   v->name(), 
                   source_line_num(tnf));
        return;
    }
    
    
    // find the outermost loop nest
    tnf = find_outermost(n);
    if(tnf == NULL) {
        if(verbose)
            printf("                No outer loop nest\n");
        return;
    }

    immed_list * iml = (immed_list *)tnf->peek_annote(k_break_up);
    if(iml)
        if(iml->count() >= num_expand) {
            if(verbose)
                printf("                More "
                       "than %d expantions of the same nest\n", num_expand);

            return;
        }
        
    // make the annotation that marks break_up 
    update_annotation(k_break_up, tnf, v);

    // annotate the array instruciton with the symbolic coefficient name
    update_annotation(k_depset_symconst_ok, ina, v);
}


/***************************************************************************
 * Annotation update:                                                      *
 * If an annotation with a given name exist in the suif object update it   *
 * otherwise create a new one.  If an annotation exist, check if the       * 
 * variable name is already in it; if not add the name to the list.        *
 ***************************************************************************/
void depset::update_annotation(const char * ann, suif_object * sa, var_sym * v)
{
    immed_list * il = (immed_list *)sa->get_annote(ann);
    // there will be no annotation if v is the first var found.
    // thus create a new list 
    if(il == NULL) il = new immed_list;

    // Entries in the list should be unique, check if v is already in the list
    immed_list_iter iter(il);
    boolean found = FALSE;
    while(!iter.is_empty()) 
        if(iter.step().symbol() == v) found = TRUE;
    // if not in the list, add v to the list
    if(!found)
        il->append(immed(v));
    
    // Put back the updated annotation
    sa->prepend_annote(ann, il); 
}



/***************************************************************************
 * Find the outermost for loop                                             *
 ***************************************************************************/
tree_for *find_outermost(tree_node *n)
{
    tree_for * outermost = NULL;

    while(n) {
        if(n->kind() == TREE_FOR)
            outermost = (tree_for *)n;
        n = (n->parent())?n->parent()->parent():NULL;
    }

    return outermost;
}

