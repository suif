/* file "normalize.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <stdlib.h>
#include <suif1.h>
#include <useful.h>
#include <suifmath.h>
#include <builder.h>
#include <dependence.h>
#include "predep.h"


void do_normalize(tree_node * tn)
{
    switch(tn->kind()) {
    case TREE_FOR: {
        tree_for * tnf = (tree_for *)tn;
        boolean normalize_ok;
        fill_in_access(tnf, NORMALIZE_CODE, &normalize_ok);
        if(!normalize_ok) 
            if(verbose)
                printf(" Normalization failed for loop `%s' at line %d\n",
                       tnf->index()->name(),
                       source_line_num(tnf));
                       
        do_normalize(tnf->landing_pad());
        do_normalize(tnf->body());
        break;
      }

    case TREE_IF: {
        tree_if * tni = (tree_if *)tn;
        do_normalize(tni->header());
        do_normalize(tni->then_part());
        do_normalize(tni->else_part());
        break;
      }

    case TREE_LOOP: {
        tree_loop * tnl = (tree_loop *)tn;
        do_normalize(tnl->body());
        do_normalize(tnl->test());
        break;
      }

    case TREE_BLOCK: {
        tree_block * tnb = (tree_block *)tn;
        do_normalize(tnb->body());
        break;
      }

    case TREE_INSTR:
        break;

    default:
        assert(0);
        break;
    }
}

void do_normalize(tree_node_list * tnl)
{
    tree_node_list_iter iter(tnl);
    while(!iter.is_empty()) {
        tree_node * tn = iter.step();
        do_normalize(tn);
    }
}
