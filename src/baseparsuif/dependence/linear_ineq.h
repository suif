/* file "linear_ineq.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#pragma interface
//
// System of linear inequalities
//

#ifndef LINEAR_INEQ_H
#define LINEAR_INEQ_H


class name_store;

/**********************************************************************************
 *** constraint                                                                 ***
 ***                                                                            ***
 **********************************************************************************/
class constraint:public integer_row {
    friend lin_ineq;
public:
    constraint(constraint & rw):integer_row((integer_row &) rw) { }
    constraint(constraint * rw):integer_row((integer_row *) rw) { }
    constraint(int s):integer_row(s) { }
    constraint() { }
    ~constraint() { }
    void init(constraint & rw) { integer_row::init((integer_row &)rw); }
    void init(constraint * rw) { integer_row::init((integer_row *)rw); }
    void init(int s) { integer_row::init(s); }
    void init(int * ilist, int s) { integer_row::init(ilist, s); }
    void init(){ integer_row::init(); }

    int row_gcd();
    int row_lcm();

    void normalize(int norm_bounds=0);
    
    void complement();

    int rank();
    int unique();

    int highest_order(constraint & so);

    int hashkey() { return integer_row::hashkey(); }

    // Complement
    constraint operator-() { constraint x(*this); x.complement(); return x; }
  
    boolean operator==(constraint & a) { return (((integer_row &) *this) == ((integer_row &)a)); }
    boolean operator!=(constraint & a) { return !((*this)==a); }

    constraint & operator=(constraint & row) { return (constraint &)((*((integer_row *)this)) = (integer_row &) row); }
    void operator^=(constraint & row) { (*((integer_row *)this)) ^= (integer_row &) row; } // Swap Rows
    constraint operator+(constraint & row) { return (constraint &)((*(integer_row *)this) + (integer_row &) row); }
    constraint operator-(constraint & row) { return (constraint &)((*(integer_row *)this) - (integer_row &) row); }
    constraint operator*(constraint & row) { return (constraint &)((*(integer_row *)this) * (integer_row &) row); }
    constraint operator/(constraint & row) { return (constraint &)((*(integer_row *)this) / (integer_row &) row); }
//    constraint operator+(int r) { return (constraint &)((*(integer_row *)this) +  r); }
//    constraint operator-(int r) { return (constraint &)((*(integer_row *)this) -  r); }
//    constraint operator*(int r) { return (constraint &)((*(integer_row *)this) *  r); }
//    constraint operator/(int r) { return (constraint &)((*(integer_row *)this) /  r); }
    constraint operator%(constraint & row) { return (constraint &)((*(integer_row *)this) % (integer_row &) row); } // shuffle
    constraint & operator+=(constraint & row) { return (constraint &)((*(integer_row *)this) += (integer_row &) row); }
    constraint & operator-=(constraint & row) { return (constraint &)((*(integer_row *)this) -= (integer_row &) row); }
    constraint & operator*=(constraint & row) { return (constraint &)((*(integer_row *)this) *= (integer_row &) row); }
    constraint & operator/=(constraint & row) { return (constraint &)((*(integer_row *)this) /= (integer_row &) row); }
    constraint & operator=(int val) { return (constraint &)((*(integer_row *)this) = val); }
    constraint & operator+=(int val) { return (constraint &)((*(integer_row *)this) += val); }
    constraint & operator-=(int val) { return (constraint &)((*(integer_row *)this) -= val); }
    constraint & operator*=(int val) { return (constraint &)((*(integer_row *)this) *= val); }
    constraint & operator/=(int val) { return (constraint &)((*(integer_row *)this) /= val); }

    constraint make_filter(int type, int lower, int upper); // 1 for the lower-th to upper-th of type type
    constraint make_filter(int type, int pos) { return make_filter(type, pos, pos); }
    constraint make_filter(int type) { return make_filter(type, 0, 1000); }
    int filter(constraint & kernal, int sign);
};


/**********************************************************************************
 *** linear inequality                                                          ***
 ***                                                                            ***
 **********************************************************************************/
class lin_ineq:public integer_matrix {
    
public:
    lin_ineq(int rows,int cols):integer_matrix(rows, cols) {  }
    lin_ineq(lin_ineq * m):integer_matrix((integer_matrix *)m) {  } 
    lin_ineq(lin_ineq & m):integer_matrix((integer_matrix &)m) {  } 
    lin_ineq(lin_ineq * m, int rows):integer_matrix((integer_matrix *)m, rows) {  } 
    lin_ineq(lin_ineq & m, int rows):integer_matrix((integer_matrix &)m, rows) {  } 
    lin_ineq() {  }
    lin_ineq(integer_matrix * m):integer_matrix(m) {  } 
    lin_ineq(integer_matrix & m):integer_matrix(m) {  } 
    lin_ineq(coeff * c):integer_matrix(c) {  } 
    lin_ineq(coeff & c):integer_matrix(&c) {  } 
    
    ~lin_ineq() { }
    
    void init(int rows,int cols) { integer_matrix::init(rows, cols); } 
    void init(lin_ineq * m) { integer_matrix::init((integer_matrix *)m); } 
    void init(lin_ineq & m) { integer_matrix::init((integer_matrix &)m); } 
    void init(lin_ineq * m, int rows) {integer_matrix::init((integer_matrix *)m, rows);} 
    void init(FILE * fp) { integer_matrix::init(fp); } 
    void init(FILE * fp, int r, int c) { integer_matrix::init(fp, r, c); } 
    void init(lin_ineq & m, int rows) {integer_matrix::init((integer_matrix &)m, rows);} 
    constraint & operator [](int row) { assert((row>=0)&&(row<mm)); 
                                        return (constraint &)A[row]; }

    lin_ineq & operator=(lin_ineq & in) { return (lin_ineq &)((*((integer_matrix *)this)) = ((integer_matrix &)in)); }

    lin_ineq operator||(lin_ineq & mat);        // Concatination operator
//    lin_ineq operator||(constraint & c);        // Concatination operator
    int operator~();                            // True if no answer exist for linear ineqs
    int operator>>(lin_ineq & mat);             // Is contained in opeartor ( mat is contained in this)
    int operator<<(lin_ineq & mat) { return (mat >> (*this)); }
    int operator==(lin_ineq & mat) { return ((mat >> (*this))&&((*this) >> mat)); }
    lin_ineq operator%(constraint & row);
    lin_ineq operator&(lin_ineq & a) { return conjunct(a); } // Concatinate both sets of constraints

    lin_ineq conjunct(lin_ineq &);

    int col_lcm(int col);
    int col_gcd(int col);

    
    void normalize(int norm_bounds=0);
    void del_zeros();
    void del_unused_var();
    void del_repatition();
    void del_repatition(int, int);
    void del_loose();
    void min_constant();
    
    void row_swap(int i, int j);
    
    void sort(constraint * sort_order = 0, int dir = 1);

    int num_constraint() { return m(); }
    int max_rank();
    int num_variable() { return n(); }
    int is_empty() { return m() == 0; }


    lin_ineq del_col(int i) { lin_ineq * ret = (lin_ineq *) new integer_matrix(integer_matrix::del_col(i));  
                              return *ret; }
    lin_ineq del_col(int i, int j) { lin_ineq * ret = (lin_ineq *) new integer_matrix(integer_matrix::del_col(i,j));  
                                     return *ret; }
    
    void print(FILE *fp=stdout, int flag=0);
    void sprint_eq(char * buffer, char * names[], int upper, int lhs=0);

private:
    lin_ineq filter(constraint & kernal, int sign, int op);
public:
    lin_ineq filter_thru(constraint & kernal, int sign) { return filter(kernal, sign,  1); }
    lin_ineq filter_away(constraint & kernal, int sign) { return filter(kernal, sign, -1); }
};

typedef lin_ineq * p_lin_ineq;


class lin_ineq_negate_iter {
    lin_ineq L;
    unsigned long curr, prev;
    unsigned long done_mark;
public:
    lin_ineq_negate_iter(lin_ineq & leq) { init(leq); }
    lin_ineq_negate_iter(lin_ineq * leq) { init(*leq); }
    int done() { return (curr & done_mark); }
    lin_ineq * step();
private:
    void init(lin_ineq & l);
};

class lin_ineq_difference_iter {
    int curr;
    lin_ineq D;
    int indep;
public:
    lin_ineq_difference_iter(lin_ineq & A, lin_ineq & B) { curr=0;
                                                           init(A, B); }
    lin_ineq_difference_iter(lin_ineq * A, lin_ineq * B) { curr=0;
                                                           init(*A, *B); }
    ~lin_ineq_difference_iter();
    int done() { return (!indep)?(curr==D.n()):(curr>0); }
    lin_ineq * step();
private:
    void init(lin_ineq & A, lin_ineq & B);
};
    



/**********************************************************************************
 ***  operations on linear inequality wrt set of loops and array space          ***
 ***                                                                            ***
 **********************************************************************************/
class lin_ineq_op {
    constraint kind;

public:
    lin_ineq_op(int sz, int * k);
    lin_ineq_op(int i, int j);
    lin_ineq_op(constraint & k) { kind = k; }

    lin_ineq get_upper(lin_ineq & LEQ, int nloc) { return get(LEQ, nloc, -1); }
    lin_ineq get_lower(lin_ineq & LEQ, int nloc) { return get(LEQ, nloc,  1); }
    lin_ineq get_lesser(lin_ineq & LEQ, int nloc);
    lin_ineq get_greater(lin_ineq & LEQ, int nloc);
    lin_ineq get(lin_ineq & LEQ, int nloc, int sgn);
    lin_ineq rem(lin_ineq & LEQ, int nloc, int sgn);
private:
    lin_ineq rem_upper(lin_ineq & LEQ, int nloc) { return rem(LEQ, nloc, -1); }
    lin_ineq rem_lower(lin_ineq & LEQ, int nloc) { return rem(LEQ, nloc,  1); }
    lin_ineq get_exclusive(lin_ineq & LEQ, int nloc, int sgn);
    lin_ineq get_exclusive_upper(lin_ineq & LEQ, int nloc) { return get_exclusive(LEQ, nloc, -1); }
    lin_ineq get_exclusive_lower(lin_ineq & LEQ, int nloc) { return get_exclusive(LEQ, nloc,  1); }
    int check_gap(lin_ineq & a, lin_ineq & b);
    
public:
    lin_ineq overlap_or_contig(lin_ineq & a, lin_ineq & b, int nloc);

private:
    
public:
    int print_code(lin_ineq & a, lin_ineq * strides, name_store * nm, FILE * fp=stdout, int pr_acc=1);

    int print_code(lin_ineq a, lin_ineq * strides, char ** name, FILE * fp=stdout, int pr_acc=1);

    void single_update(lin_ineq & a, constraint repl, int rnk);

    int new_print_code(lin_ineq & a, lin_ineq * strides, char ** name, FILE * fp=stdout, int pr_acc=1);
};

#endif /* LINEAR_INEQ_H */
