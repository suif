/* file "named_lin_ineq.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#pragma interface

#ifndef NAMED_LIN_INEQ_H
#define NAMED_LIN_INEQ_H

enum name_table_entry_kind { nte_none, 
                             nte_symconst, 
                             nte_cond, 
                             nte_loop,
                             nte_dim,
                             nte_summary,
                             nte_aux };

/**************************************************************************
 ***                                                                    ***
 *** The name of each column.                                           ***
 ***                                                                    ***
 **************************************************************************/
class name_table_entry {
friend class name_table;
    name_table_entry_kind knd;
    union {
        var_sym * v;
        char * s;
    } vsname;
    boolean is_var;

public:
    name_table_entry()                          { init(); }
    name_table_entry(immed & im)                { init(im); }
    name_table_entry(name_table_entry_kind k,
                     immed & nm)                { init(k, nm); }
    ~name_table_entry();

    void init()                                 { knd = nte_none; 
                                                  is_var = FALSE;
                                                  vsname.s = NULL;
                                              }
    void init(immed &);
    void init(name_table_entry_kind k, immed & nm);
    void init(name_table_entry &);
    
    immed name();
    char * string();
    var_sym * var()                             { assert(is_var);       
                                                  return vsname.v;
                                              }

    void set_name(immed &);

    name_table_entry_kind kind()               { return knd; }

    void mark_sym();
    void mark_cond();
    void mark_loop();
    void mark_dim();
    void mark_summary();
    void mark_aux();

    boolean operator==(name_table_entry & nte);
    boolean operator!=(name_table_entry & nte)  { return !(*this == nte); }

    void print(FILE * fp=stdout);
};


/**************************************************************************
 ***                                                                    ***
 *** A list of names identifying all the column.                        ***
 ***                                                                    ***
 *** The 0th column is for the constant, thus does not have a name.     ***
 *** All the other columns will be named by entries in the table        ***
 ***                                                                    ***
 **************************************************************************/
class name_table {
    name_table_entry * L;
    int sz;
public:
    name_table()                                { L = NULL; sz = 1; }
    name_table(name_table * nt)                 { L = NULL; sz = 1; init(*nt); }
    name_table(name_table & nt)                 { L = NULL; sz = 1; init(nt); }
    name_table(int s)                           { L = NULL; sz = 1; init(s); }
    name_table(immed * v1,        immed * v2 =NULL, 
               immed * v3 =NULL,  immed * v4 =NULL, 
               immed * v5 =NULL,  immed * v6 =NULL, 
               immed * v7 =NULL,  immed * v8 =NULL, 
               immed * v9 =NULL,  immed * v10=NULL, 
               immed * v11=NULL,  immed * v12=NULL, 
               immed * v13=NULL,  immed * v14=NULL, 
               immed * v15=NULL,  immed * v16=NULL);
    ~name_table();
    void init(name_table &);
    void init(int s);

    int n()                                     { return sz; }
    name_table_entry & operator[](int i)        { assert((i>=1)&&(i<sz)); 
                                                  return L[i-1]; }

    int find(name_table_entry &);
    int find(immed & vs);

    void remove(int i, int j);
    void remove(int i)                          { remove(i, i); }
    void insert(name_table_entry & nte, int i);
    void insert(immed & v, int i);

    name_table operator&(name_table &);            // merge 2 name tables

    static void change_name_types(name_table & A, 
                                  name_table & B); // use A and change B
    static boolean is_aligned(name_table & A, 
                               name_table & B);
    static name_table * mk_align(name_table & na, name_table & nb);

    static name_table * align_tables(name_table na, name_table nb,
                                      integer_row & ca, integer_row & cb);
                                        


    constraint lio_code_type();

    void print(FILE * fp=stdout);
};
    

enum print_exp_type { pet_single, pet_system, pet_system_nl, pet_max, pet_min};

/**************************************************************************
 ***                                                                    ***
 *** System of linear inequalities with names associated with           ***
 *** each column.                                                       ***
 ***                                                                    ***
 **************************************************************************/
class named_lin_ineq {
friend class named_lin_ineq_list;
    int unum;
    static int unum_cnt;
    name_table nt;
    lin_ineq   lq;
public:
    named_lin_ineq() : lq(0, 1)                 { init(); }
    named_lin_ineq(named_lin_ineq & c)          { init(); init(c);  }
    named_lin_ineq(named_lin_ineq * c)          { init(); init(*c); }
    named_lin_ineq(immed_list & il)             { init(); init(il); }
    named_lin_ineq(name_table &n, lin_ineq &l)  { init(); 
                                                  nt.init(n);
                                                  lq = l;       
                                              }
    ~named_lin_ineq();
private:
    void init();
public:
    void init(named_lin_ineq & c);
    int init(immed_list & il, int c=0);
    immed_list * cvt_immed_list();

    int uid() { return unum; }

    name_table & names()                        { return nt; }
    lin_ineq   & ineqs()                        { return lq; }


    int n()                                     { assert(nt.n() == lq.n()); 
                                                  return nt.n(); }
    int m()                                     { return lq.m(); }

    void swap_col(int i, int j);
    void add_col(name_table_entry & nte, int i);
    void del_col(int i, int j);
    void del_col(int i)                         { del_col(i, i); }
    void del_col(integer_row &);

    int find(immed & v)                       { return names().find(v); }

    void project();
    void project_away(immed &);
    void project_away(name_table &);

    named_lin_ineq & operator=(named_lin_ineq & c);
    named_lin_ineq & operator=(lin_ineq & l);

    named_lin_ineq operator&(named_lin_ineq & c);
    named_lin_ineq & operator&=(named_lin_ineq & c);
    named_lin_ineq & operator&=(lin_ineq & l);
    static named_lin_ineq * and(named_lin_ineq * c1, 
                                named_lin_ineq * c2, 
                                boolean del1=FALSE, 
                                boolean del2=FALSE);

    void operator||(named_lin_ineq & c);

    boolean operator==(named_lin_ineq & c2);
    boolean operator>>(named_lin_ineq & c2);    // Is contained in opeartor 
    boolean operator<<(named_lin_ineq & c2)     { return (c2 >> (*this)); }
    boolean operator~();

    void find_bounds();

    void cleanup();
    void del_zero_cols();

    void print();
    void print_exp(print_exp_type tp=pet_single);

    instruction * mk_bounds();
    instruction * create_expression(immed & v, 
                                    boolean is_ub, 
                                    block_symtab * sym=NULL);


    static named_lin_ineq * mk_named_lin_ineq(array_info & ai, 
                                              immed & ind, 
                                              boolean lb);
    static named_lin_ineq * mk_named_lin_ineq(access_vector & av, 
                                              immed & ind, 
                                              boolean lb);

    static void align_named_lin_ineqs(named_lin_ineq & A, 
                                       named_lin_ineq & B);

    void align(name_table & nt);

    static boolean is_aligned(named_lin_ineq & A, 
                               named_lin_ineq & B);

    static named_lin_ineq * merge_named_lin_ineqs(named_lin_ineq & A, 
                                                  named_lin_ineq & B);
};

typedef named_lin_ineq * p_named_lin_ineq;

named_lin_ineq * include_for(tree_for * tf, 
                             named_lin_ineq * c = NULL);

#endif
