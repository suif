/* file "mat_lex.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <suif1.h>
#include <suifmath.h>
#include <dependence.h>
#include "mat_calc.h"
#include "mat_grammar.tab.h"

int matparse (void);


int matlex ()
{
    int c;
    Files.sttoken();
    
    /* Ignore whitespace, get first nonwhite character.  */
    while ((c = Files.gc()) == ' ' || c == '\t');
    
    if (c == EOF) {
        Files.endtoken();
        return 0;
    }
    
    /* Char starts a number => parse the number.         */
    int digit = 0;
    if (isdigit (c)) digit = 1;
    if(c == '-') {
        char cc = Files.gc();
        Files.ugc(cc);
        if(isdigit(cc)) digit = 1;
    }
    if(digit) {
        Files.ugc (c);
        matlval.val = Files.gi();
        Files.endtoken();
        return NUM;
    }

    if(c == '/') {
        char cc = Files.gc();
        if(cc == '/') {
            while (((c = Files.gc()) != '\n') && (c != EOF));
            Files.endtoken();
            return c;
        } else
            Files.ugc(cc);
    }
    
    /* Char starts an identifier => read the name.       */
    if (isalpha (c))
        {
            mat_symrec *s;
            static char *symbuf = 0;
            static int length = 0;
            int i;
            
            /* Initially make the buffer long enough
               for a 40-character symbol name.  */
            if (length == 0)
                length = 40, symbuf = (char *)malloc (length + 1);
            
            i = 0;
            do
                
                {
                    /* If buffer is full, make it bigger.        */
                    if (i == length)
                        {
                            length *= 2;
                            symbuf = (char *)realloc (symbuf, length + 1);
                        }
                    /* Add this character to the buffer.         */
                    symbuf[i++] = c;
                    /* Get another character.                    */
                    c = Files.gc();
                }
            
            while (c != EOF && isalnum (c));
            
            Files.ugc(c);
            symbuf[i] = '\0';

            int x = chktable(symbuf);
            if(x) {
                Files.endtoken();
                return x;
            }
            s = getsym (symbuf);
            if (s == 0)
                s = putsym (symbuf, VAR);
            matlval.tptr = s;
            Files.endtoken();
            return s->type;
        }
    
    /* Any other character is a token by itself.        */
    Files.endtoken();
    return c;
}



