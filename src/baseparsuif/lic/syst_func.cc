/* file "syst_func.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <suif1.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <dependence.h>
#include "syst_calc.h"
#include "syst_grammar.tab.h"


struct fntable
{
    char *fname;
    int token;
};


fntable SystFnTable[] = {
    {"saman",                   TEST}, // by popular request test was changed 
    {"isin",                    CONTAINED},
    {"iscontained",             CONTAINED},
    {"project",                 PROJECT},
    {"proj",                    PROJECT},
    {"simplify",                SIMPLIFY},
    {"simp",                    SIMPLIFY},
    {"isol",                    IVALID},
    {"intsol",                  IVALID},
    {"integersol",              IVALID},
    {"intsolution",             IVALID},
    {"integersolution",         IVALID},
    {"rsol",                    RVALID},
    {"realsol",                 RVALID},
    {"realsolution",            RVALID},
    {"merge",                   MERGE},
    {"order",                   ORDER},
    {"ord",                     ORDER},
    {"rename",                  RENAME},
    {"ren",                     RENAME},
    {"code",                    CODE},
    {"cd",                      CODE},
    {"help",                    HELP},
    {"h",                       HELP},
    {"end",                     END},
    {"quit",                    EXIT},
    {"exit",                    EXIT},
    {"bye",                     EXIT},
    {NULL,                      0}
};


void print_help()
{
    printf("         A    R A P I D     P R O T O T Y P I N G      S Y S T E M\n");
    printf("               F O R      C O D E      G E N E R A T I O N \n\n");

    printf("In this system, the user can define and use variables\n"
           "(alphabetic character string as name) for both integers\n"
           "and linear inequalities.\n");
    printf("   n = 4\n"
           "   ineq = [  ir > 0 ]\n");
    printf(" \n");
    printf("A system of linear inequalities is given by defining each\n"
           "inequality in a single line and delimiting them by `[' `]'\n");
    printf("Alphabetic character strings can be used for variable names"
           " and \n"
           "symbolic constants (variable names should be different from\n"
           "symbolic constants)\n");
    printf("   iter = [ 0 <= ir <= U\n"
           "            0 <= jr <= U ]\n");
    printf("   decomp =  [ L*Pr <= ir < L*Pr + L \n"
           "               L*Pw <= iw < L*Pw + L ]\n");
    printf("   lwt = [ iw = 2*ir - 1\n"
           "           jw = jr ]\n");
    printf("\n");
    
    printf("Multiple linear inequalities can be concatenated by listing them\n"
           "separated by commas (`,') or a new line and delimited by `{' `}'\n");
    printf("   res = { iter, decomp, lwt }\n");
    printf("   nocomm = { res\n"
           "              [ Pr == Pw ] }\n");
    printf("\n");
    
    printf("To check if a system of inequalities is valid and perform\n" 
           "Fourier-Motzkin elimination on one or all variables,\n"
           "   v = final.realsol()        variable `v' is set to 1 if there\n"
           "                              is a real solution, 0 otherwise\n"
           "   v = final.intsol()         variable `v' is set to 1 if there\n"
           "                              is an integer solution, 0 otherwise\n");

    printf("   final = final.project()    will project all variables\n"
           "   final = final.project(ir)  will project only the variable `ir'\n");
    printf("   final = final.simplify()   will project all variables and \n"
           "                              get rid of redundant constraints\n"); 
    printf("\n");

    printf("For printing the corresponding loop nest,\n"
           " first, order the variables in the scanning order\n");
    printf("   final = nocomm.order(U Pr ir jr Pw iw jw)\n"
           " followed by\n"     
           "   final.code()\n");
    printf("\n");

    printf("Input can be given by an script file by  '< filename'\n");
    
    printf("   Command 'end' should be the last line in the script file.\n");
    printf("   All characters after `//' to newline are"
           " considered as comments.\n");
    printf("   Results are not printed if the first character of the line is `#'.\n");
    printf(" \n");
    printf("use `lic -m' to invoke the matrix and linear inequality calculator\n\n");
    printf("Any questions, bugs and spelling errors should be directed to\n"
           "saman@wildhog.stanford.edu\n\n");

}

int chk_builtin(char * sym_name)
{
    for (int i=0; SystFnTable[i].fname != NULL; i++)
        if (strcmp (SystFnTable[i].fname, sym_name) == 0)
            return SystFnTable[i].token;
    return 0;
}


