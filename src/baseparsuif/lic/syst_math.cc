/* file "syst_math.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <suif1.h>
#include <dependence.h>
#include "syst_calc.h"

triplet::triplet(triplet * t)
{
    init();
    coif = t->coif;
    var = t->var;
    symvar = t->symvar;
}

triplet::triplet(int i)
{
    init();
    coif = i;
}

triplet::triplet(char * v, int i)
{
    init();
    coif = i;
    var = v;
}

triplet::triplet(char * v, int i, char * s)
{
    init();
    coif = i;
    var = v;
    symvar = s;
}


void triplet::init()
{
    coif = 1;
    symvar = var = NULL;
    next = NULL;
}

triplet & triplet::operator*=(int i)
{
    coif *= i;
    if(next) *next *= i;
    return *this;
}

triplet * triplet::duplicate()
{
    triplet * t = new triplet(this);
    if(next) t->next = next->duplicate();
    return t;
}

triplet * concat(triplet * a, triplet * b)
{
    triplet * c = a;
    while(c->next) c = c->next;
    c->next = b;
    return a;
}






static void add_sym(name_table & t, char * n)
{
    if(n==NULL) return;
    var_sym * v = get_var(n);
    if(t.find(v)>0) return;
    t.insert(v, t.n());
}





named_symcoeff_ineq * mk_sc(triplet_list * L)
{
    name_table N;
    name_table P;

    int mcnt = 0;
    triplet_list_iter iter(L);
    while(!iter.is_empty()) {
        mcnt++;
        triplet * c = iter.step();
        while(c) {
            add_sym(N, c->var);
            add_sym(P, c->symvar);
            c = c->next;
        }
    }

    // Same var cannot be both a plane and column
    for(int i=1; i<N.n(); i++) 
        if(P.find(N[i].name()) != -1)
            N.remove(i);
    
    named_symcoeff_ineq * ret = new named_symcoeff_ineq;

    ret->init(P.n(), mcnt, N.n());
    ret->planes().init(P);
    ret->cols().init(N);

    
    int im = 0;
    triplet_list_iter iter2(L);
    while(!iter2.is_empty()) {
        triplet * c = iter2.step();
        while(c) {
            int in = 0;
            int ip = 0;
            if(c->var)    in = N.find(get_var(c->var));
            if(c->symvar) ip = P.find(get_var(c->symvar));
            // check if a constant plane value
            if((in == -1)&&(ip == 0)) {
                int xp = P.find(get_var(c->var));
                if(xp > 0) {
                    ip = xp;
                    in = 0;
                }
            }
            assert(in>=0);
            assert(ip>=0);
            (*ret)[ip][im][in] += c->coif;
            c = c->next;
        }
        im++;
    }
    
    return ret;
}


triplet_list * docoif(triplet * a, boolean neq, triplet * b)
{
    *b *= -1;
    triplet * tr =  concat(a, b);
    if(neq) {
        triplet * m1 = new triplet(-1);
        tr = concat(tr, m1);
    }
    triplet_list * ret = new triplet_list;
    ret->append(tr);
    return ret;
}





named_symcoeff_ineq * order(named_symcoeff_ineq * sc, triplet * tr)
{
    name_table N;

    triplet * c = tr;
    while(c) {
        assert(c);
        assert(c->var);
        var_sym * v = get_var(c->var);
        if(N.find(v) != -1) {
            printf("`%s' is already in the system\n", v->name());
            assert(0);
        }
        N.insert(v, N.n());
        c = c->next;
    }

    for(int i=1; i < sc->n(); i++) {
        immed v(sc->cols()[i].name());
        if(N.find(v) == -1)
            N.insert(v, N.n());
    }
     
    assert(sc);
    assert(sc->n() == N.n());

    named_symcoeff_ineq * ret = new named_symcoeff_ineq(sc);

    ret->cols().init(N);
    
    for(int in=0; in<sc->n(); in++) {
        int nin = (in)?N.find(sc->cols()[in].name()):0;
        if(nin < 0) {
            printf("`%s' is not in the system\n", sc->cols()[in].string());
            assert(0);
        }
        for(int ip=0; ip<sc->p(); ip++)
            for(int im=0; im<sc->m(); im++)
                (*ret)[ip][im][nin] = (*sc)[ip][im][in];
    }
    return ret;
}


named_symcoeff_ineq * rename(named_symcoeff_ineq * sc, triplet * tr)
{
    name_table N;

    triplet * c = tr;
    while(c) {
        assert(c);
        assert(c->var);
        var_sym * v = get_var(c->var);
        if(N.find(v) != -1) {
            printf("`%s' is already in the system\n", v->name());
            assert(0);
        }
        N.insert(v, N.n());
        c = c->next;
    }

    assert(sc);
    if(N.n() != sc->n()) {
        printf("Number of column does not match %d vs %d", N.n(), sc->n());
        assert(0);
    }

    named_symcoeff_ineq * ret = new named_symcoeff_ineq(sc);

    for(int i=1; i < sc->n(); i++) {
        ret->cols()[i].init(N[i]);
    }
     
    return ret;
}

static lin_ineq convert(named_symcoeff_ineq & val)
{
    assert(val.p() == 1);

    lin_ineq ret(val.m(), val.n());

    for(int i=0; i<val.m(); i++)
        for(int j=0; j<val.n(); j++)
            ret[i][j] = val[0][i][j];

    return ret;
}

int is_valid_int(named_symcoeff_ineq * sc)
{
    if(sc->p() > 1) {
        printf("Finding integer solutions with symbolic coefficients" 
               " is not implemented");
        assert(0);
    }

    lin_ineq leq(convert(*sc));

    return (~leq == FALSE);
}

int is_contained(named_symcoeff_ineq * sc1, named_symcoeff_ineq * sc2)
{
    if((sc1->p() > 1)||(sc2->p() > 1)) {
        printf("Finding containment with symbolic coefficients" 
               " is not implemented");
        assert(0);
    }

    lin_ineq leq1(convert(*sc1));
    lin_ineq leq2(convert(*sc2));
    
    int ret = (leq1 << leq2);
    return ret;
}

named_symcoeff_ineq * project(named_symcoeff_ineq * sc, char * nm)
{
    named_symcoeff_ineq * ret;
    if(nm) {
        var_sym * v = get_var(nm);
        assert(v);
        int i = sc->cols().find(v);
        assert(i>0);

        named_symcoeff_ineq nsi(sc);
        nsi.swap_col(i, nsi.n()-1);

        named_sc_fm FM(nsi);
        FM.fm_project(nsi.n()-1, nsi.n());
        ret =  FM.internal_get();
        ret->swap_col(i, ret->n()-1);
	ret->del_col(i);
    } else {
        named_sc_fm FM(sc);
        FM.fm_project();
        ret =  FM.get();
    }

    return ret;
}

named_symcoeff_ineq * simplify(named_symcoeff_ineq * sc, char * nm)
{
    named_symcoeff_ineq * ret;
    if(nm) {
        var_sym * v = get_var(nm);
        assert(v);
        int i = sc->cols().find(v);
        assert(i>0);

        named_symcoeff_ineq nsi(sc);
        nsi.swap_col(i, nsi.n()-1);

        named_sc_fm FM(nsi);
        FM.fm_bounds(nsi.n()-1, nsi.n());
        ret =  FM.internal_get();
        ret->swap_col(i, ret->n()-1);
    } else {
        named_sc_fm FM(sc);
        FM.fm_bounds();
        ret =  FM.get();
    }

    return ret;
}



void do_print_code(named_symcoeff_ineq * sc)
{
    do_print_code(sc, 1);
}

void do_print_code(named_symcoeff_ineq * sc, int s1)
{
    assert(sc);
    do_print_code(sc, s1, sc->n());
}


void do_print_code(named_symcoeff_ineq * sc, int s1, int s2)
{
    named_symcoeff_ineq R(sc);
    if(~R) {
        printf("Inconsistant ineqalities\n");
        assert(0);
    }
    named_sc_fm FM(sc);
    FM.fm_bounds(s1, s2);
    FM.get(&R);
    R.print_code(TRUE, s1);
}


void merge_and_print(int i, int j, named_symcoeff_ineq_list * nsil)
{
    named_sc_merge M(nsil, smt_full);
    if(i>0) M.mark_cond(i);
    if(j>0) M.mark_excess(j);
    M.merge();
    M.print(1);
}

void do_test(named_symcoeff_ineq * sc)
{
    named_symcoeff_ineq R(sc);
    if(~R) {
        printf("Inconsistant ineqalities\n");
        assert(0);
    }
    named_sc_fm FM(sc);
    FM.fm_bounds(2,3);
    FM.get(&R);
    R.print();
    R.print_code(TRUE);

}

