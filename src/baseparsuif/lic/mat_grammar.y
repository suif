/* file "mat_grammar.y" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

     %{
     #include <suif_copyright.h>

     #include <suif1.h>  
     #include <suifmath.h>  
     #include <dependence.h>  
     #include "mat_calc.h"  /* Contains definition of `mat_symrec'        */
     %}
     %union {
     int       val;
     lin_ineq *ineq;  /* For returning numbers.                   */
     integer_row * cnst; 
     mat_symrec  *tptr;   /* For returning symbol-table pointers      */
     compose_list * comp;
     var_list * vlst;
     }

     %token <val>  NUM
     %token <ineq> LININEQ        /* Simple double precision number   */
     %token <cnst> CONSTRAINT
     %token <tptr> VAR LVAR IVAR    /* Variable and Function          */
     %token HELP
     %token PARSE
     %token IDENT
     %token SOLVE
     %token CODE
     %token FILT_THRU
     %token FILT_AWAY
     %token ROW_SWAP
     %token COL_SWAP
     %token ROW_DEL
     %token COL_DEL
     %token ROW_INS
     %token COL_INS
     %token INVERSE
     %token TRANSPOSE
     %token DETERMINANT
     %token MATCH
     %token END
     %token EXIT
     %type  <val>   snum
     %type  <ineq>  exp
     %type  <ineq>  create
     %type  <cnst>  cr_row
     %type  <comp>  compose
     %type  <ineq>  parse_op
     %type  <ineq>  parse
     %type  <vlst>  varlist
     
     %left '>' '<'
     %left '|'
     %right '='
     %left '-' '+'
     %left '*' '/' 
     %left '.'
     %left '~'
     %right '^'    /* Exponentiation        */
     %left NEG     /* Negation--unary minus */


     /* Grammar follows */
     
     %%
     input:   /* empty */
             | input line
     ;

     stt1:   '['                  { Files.sttab();                       }
     ;

     stt2:   '{'                  { Files.sttab();                       }
     ;


     line:   
              '\n'                { if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                }

             | '?' '\n'           { mat_print_help(); 
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }                      
                                }

             | HELP '\n'          { mat_print_help();
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }                       
                                }

             | EXIT '\n'          { printf("done\n");                   
                                    exit(0);                            }

             | '#' snum '\n'      { if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                } 

             | '#' exp '\n'       { if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                }

             | snum '\n'          { printf("%d\n", $1);
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                } 

             | exp '\n'           { $1->print(stdout); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

             | exp '.' CODE '(' varlist ')' '\n'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot create code")
                                    else {
                                        $5->print($1);
                                        if(Files.eok()) {
                                            printf(" > "); 
                                            fflush(stdout);
                                        }
                                    }
                                }

             | CODE '(' exp ',' varlist ')' '\n'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot create code")
                                    else {
                                        $5->print($3);
                                        if(Files.eok()) {
                                            printf(" > "); 
                                            fflush(stdout);
                                        }
                                    }
                                }

             |  MATCH '(' exp ',' exp ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do match")
                                    else {
                                        match($3, $5);
                                    }
                                }

             | '<' VAR '\n'       { Files.push($2->name);  
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }             
                                }

             | END '\n'           { Files.pop();
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                }

             | error '\n'         { yyerrok; 
                                    Files.endtab();                     
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                }
     ;



     snum:     NUM                { $$ = $1;                             }
             | IVAR               { if($1->var)
                                        ERR("inrq, not an int")
                                     else
                                         $$ = $1->num;                   } 
             | VAR '=' snum       { $$ = $3; 
                                    $1->var = NULL;
                                    $1->type = IVAR;
                                    $1->num = $3;                        }  

             | IVAR '=' snum      { $$ = $3; 
                                    $1->var = NULL;
                                    $1->type = IVAR;
                                    $1->num = $3;                        } 

             | LVAR '[' snum ']' '[' snum ']'
                                   { if($1->var)
                                        ERR("invalid index")
                                     else {
                                         lin_ineq tmp($1->var);
                                         $$ = tmp[$3][$6];
                                     }
                                 }

             | snum '+' snum      { $$ = $1 + $3;                        }

             | snum '-' snum      { $$ = $1 - $3;                        }

             | snum '*' snum      { $$ = $1 * $3;                        }

             | snum '/' snum      { $$ = $1 / $3;                        }

             | '(' snum ')'       { $$ = $2;                             }

             | '^' '(' exp ')'    {  if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = ~(*$3);
                                     }
                                }

             | '(' exp '<' '<' exp  ')'
                                   { if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = (*$2) << (*$5);
                                     }
                                }

             | '(' exp '>' '>' exp ')'
                                   { if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = (*$2) >> (*$5);
                                     }
                                } 

     ;



     exp:      LININEQ            { $$ = $1;                            }

             | LVAR               { if(setjmp(Env) != 0)       
                                        ERR("cannot find")
                                    else {
                                        if($1->var == NULL)
                                            ERR("variable not an ineq")
                                        else
                                            $$ = $1->var;            
                                    }
                                  }

             | exp '|' exp        { if(setjmp(Env) != 0)       
                                         ERR("cannot concatinate")
                                    else {
                                        match($1, $3);
                                        lin_ineq tmp = Compose(2, 1,
                                                               NIM($1),
                                                               NIM($3));
                                        $$ = new lin_ineq(tmp);  
                                     }
                                } 

             | VAR '=' exp        { $$ = $3;              
                                    if(setjmp(Env) != 0)       
                                        ERR("cannot do")
                                    else {
                                        $1->var = new lin_ineq($3);     
                                        $1->num = 0;
                                        $1->type = LVAR;
                                    }
                                }

             | LVAR '=' exp       { $$ = $3;              
                                    if(setjmp(Env) != 0)       
                                        ERR("cannot do")
                                    else {
                                        $1->var = new lin_ineq($3);     
                                        $1->num = 0;
                                        $1->type = LVAR;
                                    }
                                }

             | exp '+' exp        {  if(setjmp(Env) != 0) 
                                         ERR("Unmatched matrixes")
                                     else
                                         $$ = new lin_ineq(*$1 + *$3);  }

             | exp '-' exp        { if(setjmp(Env) != 0)       
                                         ERR("Unmatched matrixes")
                                     else
                                         $$ = new lin_ineq(*$1 - *$3);  }

             | exp '*' exp        { if(setjmp(Env) != 0)       
                                         ERR("Unmatched to multiply")
                                    else
                                         $$ = new lin_ineq(*$1 * *$3);  }

             | '-' exp  %prec NEG { if(setjmp(Env) != 0)       
                                        ERR("Cannot negate")
                                    else {
                                        lin_ineq tmp($2);
                                        tmp *= -1;
                                        $$ = new lin_ineq(tmp);    
                                    }
                                }

/*             | '~' exp            { lin_ineq * ret = solve($2);
                                    if(ret == NULL)
                                        ERR("Inconsistant")
                                    else
                                        $$ = ret;                       } */

             | '(' exp ')'        { $$ = $2;                            }

             | stt1 create ']'    { $$ = $2;                     
                                    Files.endtab();                     }

             | stt2 compose '}'    { if(setjmp(Env) != 0)       
                                        ERR("Cannot compose")
                                     else {
                                         $$ = $2->compose();             
                                         Files.endtab();
                                         if($$ == NULL) 
                                             ERR("Cannot compose");          
                                     }
                                }

             | PARSE '(' parse_op ')'
                                  { $$ = $3;                            }

             | '!' parse_op '!'   { $$ = $2;                            }

             | IDENT NUM          { if(setjmp(Env) != 0) 
                                      ERR("cannot create ineqs")
                                    else
                                        $$ = new lin_ineq(Ident($2));   
                                }

             | IDENT '(' snum ')'
                                { if(setjmp(Env) != 0) 
                                      ERR("cannot create ineqs")
                                    else
                                        $$ = new lin_ineq(Ident($3));   
                                }

             | FILT_THRU '(' exp ',' exp ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("Filter # of column mismatch")
                                    else if($5->m() != 1)
                                        ERR("Filter can only have one row")

                                    else {
                                        lin_ineq tmp;
                                        tmp = $3->filter_thru((*$5)[0], $7);
                                        $$ = new lin_ineq(tmp);
                                    }
                                }

             | exp '.' FILT_THRU '(' exp ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("Filter # of column mismatch")
                                    else if($5->m() != 1) 
                                        ERR("Filter can only have one row")
                                    else {
                                        lin_ineq tmp;
                                        tmp = $1->filter_thru((*$5)[0], $7);
                                        $$ = new lin_ineq(tmp);
                                    }
                                }

             | FILT_AWAY '(' exp ',' exp ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("Filter # of column mismatch")
                                    else if($5->m() != 1) 
                                        ERR("Filter can only have one row")
                                    else {
                                        lin_ineq tmp;
                                        tmp = $3->filter_away((*$5)[0], $7);
                                        $$ = new lin_ineq(tmp);
                                    }
                                }

             | exp '.' FILT_AWAY '(' exp ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("Filter # of column mismatch")
                                    else if($5->m() != 1) 
                                        ERR("Filter can only have one row")
                                    else {
                                        lin_ineq tmp;
                                        tmp = $1->filter_away((*$5)[0], $7);
                                        $$ = new lin_ineq(tmp);
                                    }
                                }


             | exp '.' ROW_SWAP '(' snum ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do swap")
                                    else {
                                        lin_ineq * t = new lin_ineq($1);
                                        t->swap($5, $7);
                                        $$ = t;
                                    }
                                }

             | ROW_SWAP '(' exp ',' snum ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do swap")
                                    else  {
                                        lin_ineq * t = new lin_ineq($3);
                                        t->swap($5, $7);
                                        $$ = t;
                                    }
                                }

             | exp '.' COL_SWAP '(' snum ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do swap")
                                    else 
                                        $$ = new lin_ineq($1->swap_col($5, $7));
                                }

             | COL_SWAP '(' exp ',' snum ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do swap")
                                    else 
                                        $$ = new lin_ineq($3->swap_col($5, $7));
                                }

             | exp '.' ROW_DEL '(' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do del")
                                    else 
                                        $$ = new lin_ineq($1->del_row($5));
                                }

             | exp '.' ROW_DEL '(' snum ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do del")
                                    else 
                                        $$ = new lin_ineq($1->del_row($5, $7));
                                }

             | ROW_DEL '(' exp ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do del")
                                    else 
                                        $$ = new lin_ineq($3->del_row($5));
                                }

             | ROW_DEL '(' exp ',' snum ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do del")
                                    else 
                                        $$ = new lin_ineq($3->del_row($5, $7));
                                }


             | exp '.' COL_DEL '(' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do del")
                                    else 
                                        $$ = new lin_ineq($1->del_col($5));
                                }

             | exp '.' COL_DEL '(' snum ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do del")
                                    else 
                                        $$ = new lin_ineq($1->del_col($5, $7));
                                }

             | COL_DEL '(' exp ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do del")
                                    else 
                                        $$ = new lin_ineq($3->del_col($5));
                                }

             | COL_DEL '(' exp ',' snum ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do del")
                                    else 
                                        $$ = new lin_ineq($3->del_col($5, $7));
                                }

             | exp '.' COL_INS '(' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do ins")
                                    else 
                                        $$ = new lin_ineq($1->insert_col($5));
                                }

             | COL_INS '(' exp ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do ins")
                                    else 
                                        $$ = new lin_ineq($3->insert_col($5));
                                }

             | exp '.' INVERSE 
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do inv")
                                    else 
                                        $$ = new lin_ineq($1->inverse());
                                }


             | exp '.' INVERSE '(' ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do inv")
                                    else 
                                        $$ = new lin_ineq($1->inverse());
                                }

             | INVERSE '(' exp ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do inv")
                                    else 
                                        $$ = new lin_ineq($3->inverse());
                                }

             | exp '.' TRANSPOSE 
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do tran.")
                                    else 
                                        $$ = new lin_ineq($1->transpose());
                                }


             | exp '.' TRANSPOSE '(' ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do trans.")
                                    else 
                                        $$ = new lin_ineq($1->transpose());
                                }

             | TRANSPOSE '(' exp ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do trans.")
                                    else 
                                        $$ = new lin_ineq($3->transpose());
                                }

             | exp '.' SOLVE 
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do solve")
                                    else 
                                        $$ = solve($1);
                                }

             | exp '.' SOLVE '(' ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do solve")
                                    else 
                                        $$ = solve($1);
                                }

             | SOLVE '(' exp ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do solve")
                                    else 
                                        $$ = solve($3);
                                }

             | exp '.' SOLVE '(' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do solve")
                                    else 
                                        $$ = solve($1, $5);
                                }

             | SOLVE '(' exp ',' snum ')'
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot do solve")
                                    else 
                                        $$ = solve($3, $5);
                                }
     ;


     create:   cr_row             {  if(setjmp(Env) != 0)       
                                         ERR("cannot create")
                                      else {
                                          lin_ineq * ret = new lin_ineq(1, $1->n()); 
                                          (*ret)[0].init((constraint *)$1);  
                                          $$ = ret;
                                      }
                                 }

             | create '\n' cr_row {  if(setjmp(Env) != 0)       
                                         ERR("cannot create")
                                      else {
                                          lin_ineq tmp(1, $3->n());
                                          tmp[0].init((constraint *)$3); 
                                          $$ = new lin_ineq(Compose(2, 1,
                                                                    NIM($1),
                                                                    NIM(tmp)));
                                      }
                                 }
     ;


     cr_row:   snum                { if(setjmp(Env) != 0)       
                                        ERR("cannot create")
                                    else {
                                        $$ = new integer_row(1);
                                        (*$$)[0] = $1;
                                    }
                                  }

            | snum '<' snum '>'    { if(setjmp(Env) != 0)       
                                        ERR("cannot create ")
                                    else {
                                        $$ = new integer_row($3);
                                        for(int i=0; i<$3; i++)
                                            (*$$)[i] = $1;
                                    }
                                  }

             | snum cr_row         {  if(setjmp(Env) != 0)       
                                        ERR("cannot create")
                                    else {
                                        integer_row tmp($2->insert_col(0));
                                        tmp[0] = $1;
                                        $$ = new integer_row(tmp);  
                                    }
                                 }

             | snum '<' snum '>' cr_row 
                                   {  if(setjmp(Env) != 0)       
                                        ERR("cannot create")
                                    else {
                                        integer_row tmp($5->n() + $3);
                                        int i;
                                        for(i=0; i<$3; i++)
                                            tmp[i] = $1;
                                        for(i=0; i<$5->n(); i++)
                                            tmp[i+$3] = (*$5)[i];
                                        $$ = new integer_row(tmp);  
                                    }
                                 }

             | snum ',' cr_row     {  if(setjmp(Env) != 0)       
                                        ERR("cannot create")
                                    else {
                                        integer_row tmp($3->insert_col(0));
                                        tmp[0] = $1;
                                        $$ = new integer_row(tmp);
                                    }
                                 }

             | snum '<' snum '>' ',' cr_row 
                                   {  if(setjmp(Env) != 0)       
                                        ERR("cannot create")
                                    else {
                                        integer_row tmp($6->n() + $3);
                                        int i;
                                        for(i=0; i<$3; i++)
                                            tmp[i] = $1;
                                        for(i=0; i<$6->n(); i++)
                                            tmp[i+$3] = (*$6)[i];
                                        $$ = new integer_row(tmp);  
                                    }
                                 }

     ;


     compose:  compose ',' exp    {  if(setjmp(Env) != 0)       
                                        ERR("cannot compose")
                                    else {
                                        $$ = new compose_list($1, $3);      
                                        if($$->is_error()) 
                                            ERR("Unmatched # of columns"); 
                                    }
                                 }

     compose:  compose ',' '.'    { if(setjmp(Env) != 0)       
                                        ERR("cannot compose")
                                    else {
                                        $$ = new compose_list($1, NULL);      
                                        if($$->is_error()) 
                                            ERR("Unmatched # of columns"); 
                                    }
                                }

             | compose '\n' exp   {  if(setjmp(Env) != 0)       
                                        ERR("cannot compose")
                                    else {
                                        $1->set_nl();
                                        $$ = new compose_list($1, $3);      
                                        if($$->is_error()) 
                                            ERR("Unmatched # of columns");  
                                    }
                                 }

             | compose '\n' '.'   {  if(setjmp(Env) != 0)       
                                        ERR("cannot compose")
                                    else {
                                        $1->set_nl();
                                        $$ = new compose_list($1, NULL);      
                                        if($$->is_error()) 
                                            ERR("Unmatched # of columns");  
                                    }
                                 }

             | exp                { if(setjmp(Env) != 0)       
                                        ERR("cannot compose")
                                    else {
                                        $$ = new compose_list($1);          
                                    }
                                }

             | '.'                { $$ = new compose_list(NULL);        }

             | compose '\n'       { if(setjmp(Env) != 0)       
                                        ERR("cannot compose")
                                    else {
                                        $$ = $1; $$->set_nl();              
                                        if($$->is_error()) 
                                            ERR("Unmatched # of columns");  
                                    }
                                }
     ;



     parse_op:  parse             { $$ = $1;                            }

              | parse '>' '=' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        $$ = create_agtb($1, $4);
                                    }
                                }

            | parse '>' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        $$ = create_agtb($1, $3, NULL, 0);
                                    }
                                }


              | parse '<' '=' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        $$ = create_agtb($4, $1);
                                    }
                                }

            | parse '<' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        $$ = create_agtb($3, $1, NULL, 0);
                                    }
                                }

              | parse '=' '=' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        lin_ineq * r;
                                        r = create_agtb($1, $4);
                                        $$ = create_agtb($4, $1, r);
                                    }
                                }

              | parse '=' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        lin_ineq * r;
                                        r = create_agtb($1, $3);
                                        $$ = create_agtb($3, $1, r);
                                    }
                                }

              | parse '<' parse '<' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        lin_ineq * r;
                                        r = create_agtb($3, $1, NULL, 0);
                                        $$ = create_agtb($5, $3, r, 0);
                                    }
                                }

              | parse '<' '=' parse '<' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        lin_ineq * r;
                                        r = create_agtb($4, $1, NULL);
                                        $$ = create_agtb($6, $4, r, 0);
                                    }
                                }

              | parse '<' parse '<' '=' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        lin_ineq * r;
                                        r = create_agtb($3, $1, NULL, 0);
                                        $$ = create_agtb($6, $3, r);
                                    }
                                }

              | parse '<' '=' parse '<' '=' parse
                                  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse ineq")
                                    else {
                                        lin_ineq * r;
                                        r = create_agtb($4, $1, NULL);
                                        $$ = create_agtb($7, $4, r);
                                    }
                                }


     ;


     parse:  NUM                   { if(setjmp(Env) != 0)       
                                        ERR("cannot parse 1")
                                    else {
                                        lin_ineq tmp(1, 1);
                                        tmp[0][0] = $1;                   
                                        $$ = new lin_ineq(tmp);   
                                    }
                                }

             | IVAR               { if($1->var)
                                        ERR("inrq, not an int")
                                     else {
                                        lin_ineq tmp(1, 1);
                                        tmp[0][0] = $1->num;                   
                                        $$ = new lin_ineq(tmp);   
                                    }
                                }

           | '@' NUM             {  if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        lin_ineq tmp(1, $2+1);   
                                        tmp[0][$2] = 1;                 
                                        $$ = new lin_ineq(tmp);   
                                    }
                                 }

           | NUM  '@' NUM        {  if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        lin_ineq tmp(1, $3+1);   
                                        tmp[0][$3] = $1;                 
                                        $$ = new lin_ineq(tmp);   
                                    }
                                 }

           | IVAR '@' NUM        {  if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        lin_ineq tmp(1, $3+1);   
                                        tmp[0][$3] = $1->num;                 
                                        $$ = new lin_ineq(tmp);   
                                    }
                                 }

           | '(' parse ')'       { $$ = $2;                            }

           | NUM '(' parse ')'  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        $$ = new lin_ineq((*$3) * $1);      
                                    }
                                }

           | IVAR '(' parse ')' { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        $$ = new lin_ineq((*$3) * $1->num);    
                                    }
                                }

           | '(' parse ')' NUM  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        $$ = new lin_ineq((*$2) * $4);      
                                    }
                                }

           | '(' parse ')' IVAR { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        $$ = new lin_ineq((*$2) * $4->num);    
                                    }
                                }

           | parse '+' parse      { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        match($1, $3);
                                        $$ = new lin_ineq((*$1) + (*$3));   
                                    }
                                }

           | parse '-' parse      { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        match($1, $3);
                                        $$ = new lin_ineq((*$1) - (*$3));   
                                    }
                                }

           | parse '*' NUM        { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        $$ = new lin_ineq((*$1) * $3);      
                                    }
                                }


           | parse '*' IVAR       { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        $$ = new lin_ineq((*$1) * $3->num);   
                                    }
                                }

           | '-' parse %prec NEG  { if(setjmp(Env) != 0)       
                                        ERR("cannot parse")
                                    else {
                                        lin_ineq tmp($2);
                                        lin_ineq tmp2 = tmp;
                                        tmp2 *= -1;
                                        $$ = new lin_ineq(tmp2);      
                                    }
                                } 
     ;


     varlist:  VAR                { var_list * vl = new var_list;
                                    vl->push($1->name);
                                    $$ = vl;
                                }

             | IVAR               { var_list * vl = new var_list;
                                    vl->push($1->name);
                                    $$ = vl;
                                }

             | LVAR               { var_list * vl = new var_list;
                                    vl->push($1->name);
                                    $$ = vl;
                                }

             | varlist VAR        { $1->push($2->name);
                                    $$ = $1;
                                }

             | varlist IVAR       { $1->push($2->name);
                                    $$ = $1;
                                }

             | varlist LVAR       { $1->push($2->name);
                                    $$ = $1;
                                }
;
     /* End of grammar */
     %%

