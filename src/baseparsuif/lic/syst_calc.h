/* file "syst_calc.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Data type for links in the chain of symbols.      */
#include <setjmp.h>

class symrec;

#define DO_SIGNAL

extern jmp_buf MyEnv;
extern jmp_buf Env;
void resetjmp();


struct triplet {
    int coif;
    char * symvar;
    char * var;
    triplet * next;

    triplet(triplet *);
    triplet(int);
    triplet(char * var, int=1);
    triplet(char * var, int, char *);

    void init();

    triplet & operator*=(int);
    triplet * duplicate();

friend triplet * concat(triplet *, triplet *);
};


DECLARE_LIST_CLASS(triplet_list, triplet *);

named_symcoeff_ineq * mk_sc(triplet_list *);
var_sym * get_var(char * nm);


enum symtype { st_none, st_int, st_sc };

class symrec
{
friend class symbol_table;
    char *name;  

    symtype t;
    union {
        named_symcoeff_ineq * sc;         
        int i;
    } data;
    symrec *next;
public:
    symrec(char *);

    void set(int);
    void set(named_symcoeff_ineq *);

    symtype type()                              { return t; }
    int token();
    int get_int()                               { assert(type()==st_int);
                                                  return data.i; }
    named_symcoeff_ineq * get_sc()              { assert(type()==st_sc);
                                                  return new named_symcoeff_ineq(data.sc); }

    char * nm()                                 { return name; }
    var_sym * var();

    void set_int(int i);
    void set_sc(named_symcoeff_ineq *);
};



class symbol_table {
    symrec * head;
public:
    symbol_table();
    symrec * find(char *);
    symrec * put(char *);
};    

/* The symbol table: a chain of `struct symrec'.     */



#define NFILES          32
struct file_manager {
    char * fn[NFILES];
    FILE * fp[NFILES];
    int curr;
    int tab;
    int tl;
    int nostd;
    
    char xbuf[512];
    int x;

    file_manager();

    FILE * file();

    void push(char *);
    void pop();
    int  eok()          { return curr==0; }
    void sttab()        { tab = 1; 
                          tl = x; }
    void endtab()       { tab = 0; }

    void no_stdout()    { nostd = 1; }

    void sttoken();
    void endtoken();
    char gc();
    void ugc(char);
    int gi();
private:
    void echo(int nl=0);
};


//#define YYDEBUG 1
#define ERR(s) { fprintf(stderr, "%s\n", s); YYERROR; }



triplet_list * docoif(triplet * a, boolean nleq, triplet * b);

#define DOCOIF1(RET, A, NLEQ, B)                                      \
    if(setjmp(Env) != 0)                               		      \
        ERR("cannot create coif")                      		      \
    else {                                             		      \
        RET = docoif((A), (NLEQ), (B));                               \
        resetjmp();                                                   \
    } 


#define   DOCOIF2(RET, A, NLEQAB, B, NLEQBC, C)                       \
    if(setjmp(Env) != 0)                               		      \
        ERR("cannot create coif")                      		      \
    else {                                             		      \
       triplet * bx = (B)->duplicate();                               \
       RET = docoif((A), (NLEQAB), (B));                              \
       triplet_list * tr = docoif(bx, (NLEQBC), (C));                 \
       (RET)->append(tr);                                             \
        resetjmp();                                                   \
    }

#define DOCOIF3(RET, A, B)                                            \
    if(setjmp(Env) != 0)                               		      \
        ERR("cannot create coif")                      		      \
    else {                                             		      \
        triplet * ax = (A)->duplicate();                              \
        triplet * bx = (B)->duplicate();                              \
        RET = docoif((A), FALSE, (B));                                \
        triplet_list * tr = docoif(bx, FALSE, ax);                    \
        (RET)->append(tr);                                            \
        resetjmp();                                                   \
    } 






extern file_manager Files;
extern symbol_table SymTab;


void print_help();
int yylex ();
int yyparse (void);
void yyerror (char * s) ;
int chk_builtin(char * sym_name);
named_symcoeff_ineq * order(named_symcoeff_ineq *, triplet * vlist);
named_symcoeff_ineq * rename(named_symcoeff_ineq *, triplet * vlist);
named_symcoeff_ineq * project(named_symcoeff_ineq *, char * nm);
named_symcoeff_ineq * simplify(named_symcoeff_ineq *, char * nm);
void do_print_code(named_symcoeff_ineq *);
void do_print_code(named_symcoeff_ineq *, int);
void do_print_code(named_symcoeff_ineq *, int, int);
void exit_calc(int rc=0);
void merge_and_print(int, int, named_symcoeff_ineq_list *);
void do_test(named_symcoeff_ineq *);
int is_valid_int(named_symcoeff_ineq *);
int is_contained(named_symcoeff_ineq *, named_symcoeff_ineq *);

