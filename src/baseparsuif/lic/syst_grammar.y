/* file "syst_grammar.y" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

%{
    #include <suif_copyright.h>

    #include <suif1.h>  
    #include <suifmath.h>  
    #include <dependence.h>  
    #include "syst_calc.h" 
%}

%union {
    int val;
    named_symcoeff_ineq * sc;
    named_symcoeff_ineq_list * scl;
    symrec * sym;
    triplet * trp;
    triplet_list * trplst;
}

    %token <val> NUM
    %token <sc> INEQ   
    %token <sym> LVAR IVAR QVAR
    %token ORDER
    %token RENAME
    %token CODE
    %token IVALID
    %token RVALID
    %token PROJECT
    %token SIMPLIFY
    %token CONTAINED
    %token MERGE
    %token TEST
    %token HELP 
    %token EXIT
    %token END
    %type  <val> assnum
    %type  <val> snum
    %type  <val> snum2
    %type  <sc> exp 
    %type  <sc> compose
    %type  <trp> coif1
    %type  <trp> coifn
    %type  <trplst> ineq1
    %type  <trplst> ineqlist
    %type  <trp> varlist
    %type  <scl> mergelist


    %left '{' '}'
    %left '[' ']'
    %left '>' '<' LEFTEQ
    %right '='
    %left '-' '+'
    %left '*' '/' 
    %left '.'
    %left '~'
    %right '^'    /* Exponentiation        */
    %left NEG     /* Negation--unary minus */


     /* Grammar follows */
     
     %%

 input:   /* empty */
    | input line
    ;

 line:   
    '\n'                          { if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                }

  | '?' '\n'                      { print_help(); 
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }                      
                                }

  | HELP '\n'                     { print_help();
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }                       
                                }

  | EXIT '\n'                     { exit_calc(0);                        }

  | '#' assnum '\n'                 { if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                } 

  | '#' exp '\n'                  { if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                }

  | assnum '\n'                     { printf("%d\n", $1);
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                } 

  | exp '\n'                      { $1->print_exp(); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | CODE '(' exp ')' '\n'         { do_print_code($3); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | exp '.' CODE '(' ')' '\n'     { do_print_code($1); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | CODE '(' exp snum snum ')' '\n'
                                  { do_print_code($3, $4, $5); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | exp '.' CODE '(' snum snum ')' '\n' 
                                  { do_print_code($1, $5, $6); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | CODE '(' exp snum ')' '\n'
                                  { do_print_code($3, $4); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | MERGE '(' snum ',' snum ',' mergelist ')' 
                                  {  if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         merge_and_print($3, $5, $7);
                                     }
                                     resetjmp();
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | MERGE '(' snum snum ',' mergelist ')' 
                                  {  if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         merge_and_print($3, $4, $6);
                                     }
                                     resetjmp();
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | MERGE '(' snum snum mergelist ')' 
                                  {  if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         merge_and_print($3, $4, $5);
                                     }
                                     resetjmp();
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | MERGE '(' snum ',' mergelist ')' {  if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         merge_and_print($3, -1, $5);
                                     }
                                        resetjmp();
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | MERGE '(' snum  mergelist ')' {  if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         merge_and_print($3, -1, $4);
                                     }
                                     resetjmp();
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | MERGE '(' mergelist ')'     {  if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         merge_and_print(-1, -1, $3);
                                     }
                                   resetjmp();
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | exp '.' CODE '(' snum ')' '\n' 
                                  { do_print_code($1, $5); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | TEST '(' exp ')' '\n'         { do_test($3); 
                                    if(Files.eok()) {
                                        printf(" > "); 
                                        fflush(stdout);
                                    }
                                }

  | '<' LVAR '\n'                 { Files.push($2->nm());  
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }             
                                }

  | END '\n'                      { Files.pop();
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                }

  | error '\n'                    { yyerrok; 
                                    Files.endtab();                     
                                    if(Files.eok()) {
                                       printf(" > "); 
                                       fflush(stdout);
                                   }
                                }
     ;




 assnum: snum                     { $$ = $1;                             }
  | snum2                         { $$ = $1;                             }
  | IVAR                          { $$ = $1->get_int();                  } 
  | LVAR '=' snum                 { $$ = $3; 
                                    $1->set($3);                         }

  | IVAR '=' snum                 { $$ = $3; 
                                    $1->set($3);                         }
  | LVAR '=' snum2                { $$ = $3; 
                                    $1->set($3);                         }

  | IVAR '=' snum2                { $$ = $3; 
                                    $1->set($3);                         }
;



 snum: NUM                        { $$ = $1;                             }

  | QVAR '[' snum ']' '[' snum ']'  '[' snum ']'
                                  { named_symcoeff_ineq tmp($1->get_sc());
                                    $$ = tmp[$3][$6][$9];
                                }

  | snum '+' snum                 { $$ = $1 + $3;                        }
  | snum '-' snum                 { $$ = $1 - $3;                        }
  | snum '*' snum                 { $$ = $1 * $3;                        }
  | snum '/' snum                 { if(setjmp(Env) != 0)       
                                        ERR("div by 0?")
                                    else 
                                        $$ = $1 / $3; 
                                    resetjmp();                          }

  | '(' snum ')'                  { $$ = $2;                             }

  | '^' '(' exp ')'               {  if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = ~(*$3);
                                     }
                                     resetjmp();
                                }

  | '(' exp '<' '<' exp  ')'
                                   { if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = (*$2) << (*$5);
                                     }
                                     resetjmp();
                                }

  | '(' exp '>' '>' exp ')'
                                   { if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = (*$2) >> (*$5);
                                     }
                                     resetjmp();
                                } 
;



 snum2: 
     RVALID '(' exp ')'             
                                   { if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = (~(*$3) == FALSE);
                                     }
                                     resetjmp();
                                }    

  | exp '.' RVALID '(' ')'             
                                   { if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = (~(*$1) == FALSE);
                                     }
                                     resetjmp();
                                }    

  | IVALID '(' exp ')'             
                                   { if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = is_valid_int($3);
                                     }
                                     resetjmp();
                                }    

  | exp '.' IVALID '(' ')'             
                                   { if(setjmp(Env) != 0)       
                                        ERR("invalid")
                                     else {
                                         $$ = is_valid_int($1);
                                     }
                                     resetjmp();
                                }    
  | CONTAINED '(' exp exp ')'  {
                                   if(setjmp(Env) != 0)
                                        ERR("cannot perform is contained")
                                   else {
                                       $$ = is_contained($3, $4);
                                   }
                                     resetjmp();
                               }

    ;



 exp: INEQ                        { $$ = $1;                            }

  | QVAR                          { if(setjmp(Env) != 0)       
                                        ERR("cannot find")
                                    else {
                                        $$ = $1->get_sc();            
                                    }
                                    resetjmp();
                                }

  | QVAR '=' exp                  { $$ = $3;
                                    if(setjmp(Env) != 0)       
                                        ERR("cannot set")
                                    else {
                                        $1->set($3);
                                    }
                                    resetjmp();
                                }

  | LVAR '=' exp                  { $$ = $3;
                                    if(setjmp(Env) != 0)       
                                        ERR("cannot set")
                                    else {
                                        $1->set($3);
                                    }
                                }

  | '[' ineqlist ']'              {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot create inequality")
                                    else {
                                        $$ = mk_sc($2);
                                    }
                                   resetjmp();
                               }

  | ORDER '(' exp varlist ')'     {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot order inequality")
                                   else {
                                       $$ = order($3, $4);
                                   }
                                   resetjmp();
                               } 

  | exp '.' ORDER '(' varlist ')' {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot order inequality")
                                    else {
                                        $$ = order($1, $5);
                                  }
                                   resetjmp();
                               }

  | RENAME '(' exp varlist ')'     {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot rename inequality")
                                   else {
                                       $$ = rename($3, $4);
                                   }
                                   resetjmp();
                               } 

  | exp '.' RENAME '(' varlist ')' {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot rename inequality")
                                    else {
                                        $$ = rename($1, $5);
                                    }
                                   resetjmp();
                               }

  | PROJECT '(' exp ')'     {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot project inequality")
                                   else {
                                       $$ = project($3, NULL);
                                   }
                                   resetjmp();
                               }

  | exp '.' PROJECT '(' ')' {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot project inequality")
                                    else {
                                        $$ = project($1, NULL);
                                  }
                                   resetjmp();
                               }

  | PROJECT '(' exp LVAR ')'  {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot project inequality")
                                   else {
                                       $$ = project($3, $4->nm());
                                   }
                                   resetjmp();
                               }

  | exp '.' PROJECT '(' LVAR ')' {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot project inequality")
                                    else {
                                        $$ = project($1, $5->nm());
                                  }
                                   resetjmp();
                               }

  | SIMPLIFY '(' exp ')'     {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot simplify inequality")
                                   else {
                                       $$ = simplify($3, NULL);
                                   }
                                   resetjmp();
                               }

  | exp '.' SIMPLIFY '(' ')' {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot simplify inequality")
                                    else {
                                        $$ = simplify($1, NULL);
                                  }
                                   resetjmp();
                               }

  | SIMPLIFY '(' exp LVAR ')'  {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot simplify inequality")
                                   else {
                                       $$ = simplify($3, $4->nm());
                                   }
                                   resetjmp();
                               }

  | exp '.' SIMPLIFY '(' LVAR ')' {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot simplify inequality")
                                    else {
                                        $$ = simplify($1, $5->nm());
                                  }
                                   resetjmp();
                               }

  | '{' compose '}'               { $$ = $2;                            }
;




 compose: exp                     { $$ = $1;                            }
  | compose '\n' exp              {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot compose multiple")
                                    else {
                                        $$ = named_symcoeff_ineq::and2($1, $3,
                                                                     TRUE,
                                                                     TRUE);
                                    }
                                   resetjmp();
                               }
  | compose ',' exp              {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot compose multiple")
                                    else {
                                        $$ = named_symcoeff_ineq::and2($1, $3,
                                                                     TRUE,
                                                                     TRUE);
                                    }
                                   resetjmp();
                               }
;




 ineqlist: ineq1                  {  
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot create coif")
                                    else {
                                        $$ = $1;
                                    }
                                   resetjmp();
                               }

  | ineq1 '\n'                    {  
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot create coif")
                                    else {
                                        $$ = $1;
                                    }
                                   resetjmp();
                               }

  | ineq1 '\n' ineqlist           {
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot create coif")
                                    else {
                                        $3->append($1);
                                        $$ = $3;
                                    }
                                   resetjmp();
                               }
;




 ineq1: coifn '>' '=' coifn           { DOCOIF1($$, $1, 0, $4);            }
  | coifn '<' '=' coifn               { DOCOIF1($$, $4, 0, $1);            }
  | coifn '>' coifn                   { DOCOIF1($$, $1, 1, $3);            }
  | coifn '<' coifn                   { DOCOIF1($$, $3, 1, $1);            }
  | coifn '>' '=' coifn '>' '=' coifn { DOCOIF2($$, $1, 0, $4, 0, $7);     }
  | coifn '>' '=' coifn '>' coifn     { DOCOIF2($$, $1, 0, $4, 1, $6);     }
  | coifn '>' coifn '>' '=' coifn     { DOCOIF2($$, $1, 1, $3, 0, $6);     }
  | coifn '>' coifn '>' coifn         { DOCOIF2($$, $1, 1, $3, 1, $5);     }
  | coifn '<' '=' coifn '<' '=' coifn { DOCOIF2($$, $7, 0, $4, 0, $1);     }
  | coifn '<' '=' coifn '<' coifn     { DOCOIF2($$, $6, 1, $4, 0, $1);     }
  | coifn '<' coifn '<' '=' coifn     { DOCOIF2($$, $6, 0, $3, 1, $1);     }
  | coifn '<' coifn '<' coifn         { DOCOIF2($$, $5, 1, $3, 1, $1);     }
  | coifn '=' '=' coifn %prec LEFTEQ  { DOCOIF3($$, $1, $4);               } 
  | coifn '=' coifn %prec LEFTEQ      { DOCOIF3($$, $1, $3);               } 
;
                                        
 coifn: coif1                     { $$ = $1;                              }
 | coifn '+' coif1                { 
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot create coif")
                                    else {
                                        triplet & c1 = *$1;
                                        triplet & c2 = *$3;
                                        $$ = concat(&c1, &c2);
                                    }
                                   resetjmp();
                               }
 | coifn '-' coif1               { 
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot create coif")
                                    else {
                                        triplet & c1 = *$1;
                                        triplet & c2 = *$3;
                                        c2 *= -1;
                                        $$ = concat(&c1, &c2);
                                    }
                                   resetjmp();
                               }
;


 coif1: snum                      { $$ = new triplet($1);                 }
 | snum '*' LVAR                  { $$ = new triplet($3->nm(), $1);       }
 | LVAR                           { $$ = new triplet($1->nm(),  1);       }
 | '-' LVAR                       { $$ = new triplet($2->nm(), -1);       } 
 | snum '*' LVAR '*' LVAR         { $$ = new triplet($5->nm(), $1, $3->nm()); }
 | LVAR '*' LVAR                  { $$ = new triplet($3->nm(),  1, $1->nm()); }
 | '-' LVAR '*' LVAR              { $$ = new triplet($4->nm(), -1, $2->nm()); } 
;



 varlist: LVAR                    { 
                                   if(setjmp(Env) != 0)       
                                        ERR("cannot create coif")
                                    else {
                                        $$ = new triplet($1->nm());           
                                    }
                                   resetjmp();
                               }
  | LVAR varlist                  { 
                                   if(setjmp(Env) != 0)       
                                       ERR("cannot create coif")
                                   else {
                                       triplet * c1 = new triplet($1->nm());
                                       triplet * c2 = $2;
                                       $$ = concat(c1, c2);
                                   }
                                   resetjmp();
                               }
;

 mergelist: exp                 {  if(setjmp(Env) != 0)       
                                       ERR("cannot merge")
                                   else {
                                       named_symcoeff_ineq_list * scl;
                                       scl = new named_symcoeff_ineq_list;
                                       scl->append($1);
                                       $$ = scl;
                                   }
                                   resetjmp();
                               }

 | mergelist exp                 {  if(setjmp(Env) != 0)       
                                       ERR("cannot merge")
                                   else {
                                       named_symcoeff_ineq_list * scl;
                                       scl = $1;
                                       scl->append($2);
                                       $$ = scl;
                                   }
                                    resetjmp();
                               }
;
                                       
                                   

     /* End of grammar */
     %%


     
