/* file "mat_scr.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <stdio.h>
#include <stdlib.h>
#include <suif1.h>
#include <suifmath.h>
#include <dependence.h>
#include "mat_calc.h"


void mat_print_help()
{
    printf("          A    C A L C U L A T O R    T O    M A N I P U L A T E \n");
    printf("    M A T R I C E S    A N D    L I N E A R    I N E Q U A L I T I E S \n\n");

    printf("Simple operations are available for both integers ");
    printf(" and liner inequalities.\n('+'   '-'   '*'   '/') \n");
    printf("Can define and use variables(any alpha char string as name) ");
    printf("for both integers \nand linear inequalities.\n");
    printf("   n = 4\n");
    printf("   A = ident(n)*A\n\n");

    printf("Create a matrix\n   [ 0, 0, 0\n");
    printf("    0<3>                                          0<3> is same as 0, 0, 0\n");
    printf("   0, 0, 0 ]\n");
    printf("Compose a matrix\n");
    printf("   { A, ., B                                      . is a matrix with 0's\n");
    printf("     ., C, D }\n");
    printf("Create a set of inequalities\n"); 
    printf("   parse(inequality)                              inequality is of the form:\n");
    printf("   !ineqality!                                       !1+2(2@2-4@5)>=0!\n");
    printf("                                                  resulting:\n");
    printf("                                                     1   0   4   0   0  -8\n");

    printf("\nAvaiable function calls for inequalities:\n");
    printf(" Create an identity matrix\n");
    printf("   identity(n)            ident(n)      ");
    printf("   id(n)\n");
    printf(" Solve the set of inequalities by fourier-motzkin elimination\n");
    printf("   solve(M)               s(M)          ");
    printf("   M.solve()              M.s()\n");
    printf("   M.solve                M.s           ");
    printf("   M.solve()              M.s()\n");
    printf(" Filter thru the Matrix M using Filter F, and the sign s\n");
    printf("   filterthru(M, F, s)     ft(M, F, s)  ");
    printf("   M.filterthru(F, s)      M.ft(F, s)\n");
    printf(" Filter away the Matrix M using Filter F, and the sign s\n");
    printf("   filteraway(M, F, s)     fa(M, F, s)  ");
    printf("   M.filteraway(F, s)      M.fa(F, s)\n");
    printf(" Swap rows i and j of the matrix M\n");
    printf("   row_swap(M, i, j)       rs(M, i, j)  ");
    printf("   M.row_swap(i, j)        M.rs(i, j)\n");
    printf(" Swap columns i and j of the matrix M\n");
    printf("   col_swap(M, i, j)       cs(M, i, j)  ");
    printf("   M.col_swap(i, j)        M.cs(i, j)\n");
    printf(" Delete row(s) of the matrix M\n");
    printf("   row_del(M, i)           rd(M, i)     ");
    printf("   M.row_del(i)            M.rd(i)\n");
    printf("   row_del(M, i, j)        rd(M, i, j)  ");
    printf("   M.row_del(i, j)         M.rd(i, j)\n");
    printf(" Delete columns(s) of the matrix M\n");
    printf("   col_del(M, i)           cd(M, i)     ");
    printf("   M.col_del(i)            M.cd(i)\n");
    printf("   col_del(M, i, j)        cd(M, i, j)  ");
    printf("   M.col_del(i, j)         M.cd(i, j)\n");
    printf(" Insert column of the matrix M\n");
    printf("   col_ins(M, i)           ci(M, i)     ");
    printf("   M.col_ins(i)            M.ci(i)\n");
    printf(" Find the inverse of a matrix M\n");
    printf("   inverse(M)              inv(M)       ");
    printf("   M.inverse()             M.inv()\n");
    printf("   M.inverse               M.inv\n");
    printf(" Find the transpose of a matrix M\n");
    printf("   transpose(M)            t(M)         ");
    printf("   M.transpose()           M.t()\n");
    printf("   M.transpose             M.t\n");
    printf(" Print code corresponding to a set of inequalities\n");
    printf("   M.code(namelist)                 namelist is of the form 'i, j, k' \n");
    printf("                                    where the items name columns 1 to last\n");
    printf(" \n");
    printf("Input can be given by an script file by  '< filename'\n");
    
    printf("   Command 'end' should be the last line in the script file.\n");
    printf("   All characters after '//' to newline are considered as comments.\n");
    printf("   Results are not printed if the first character of the line is '#'.\n");
    printf(" \n");
}


lin_ineq * solve(lin_ineq * in, int level)
{
    if(~(*in)) return NULL;
    poly_iterator Poly(*in);
    constraint del_list(in->n());
    del_list = 0;
    Poly.set_sort_order(del_list.data_array());

    lin_ineq M;
    if(level == 0) {
        M = Poly.get_iterator(0);
        M = Poly.reduce_extra_constraints2();
        M.sort();
    } else if(level == 1) {
        M = Poly.get_iterator(0);
        M.sort();
    } else if(level == 2) {
        M = Poly.get_iterator(1);
        M.sort();
    } else if(level == 3) {
        M = Poly.get_iterator(0);
        M = Poly.reduce_extra_constraints();
        M.sort();
    } 
        
    return new lin_ineq(M);
}


void match(lin_ineq * a, lin_ineq * b)
{
    if(a->n() == b->n()) return;

    if(a->n() > b->n()) 
        b->init(Compose(1, 2,
                        NIM(b), NIM(b->m(), a->n()- b->n())));
    else 
        a->init(Compose(1, 2,
                        NIM(a), NIM(a->m(), b->n()- a->n())));
}


compose_list::compose_list(lin_ineq * l)
{
    m = n = 0;
    err = FALSE;
    
    X = new lin_ineq_p[1];
    X[0] = l;
    i = 1;
}


compose_list::compose_list(compose_list * CL, lin_ineq * l)
{
    err = CL->err;
    m = CL->m;
    n = CL->n;
    i = CL->i;

    X = new lin_ineq_p[m*n + i + 1];
    int j;
    for(j=0; j<m*n+i; j++) X[j] = CL->X[j];
    X[j] = l;
    i++;
}

void compose_list::set_nl()
{
    if(m>0) {
        if(n != i) err = TRUE;
    } else
        n = i;

    m++;
    i=0;
}

#define MAXLIST 20
lin_ineq * compose_list::compose()
{
    if(i>0) set_nl();
    if(err) return NULL;
    if(m*n > MAXLIST) return NULL;
    lin_ineq x;
    if(n == 1) {
        int min = 1000;
        int max = 0;
        int i;
        for(i=0; i<m; i++)
            if(X[i]) {
                max = MAX(max, X[i]->n());
                min = MIN(min, X[i]->n());
            }
        if(min != max)
            for(i=0; i<m; i++)
                if(X[i]) 
                    if(X[i]->n() != max)
                        X[i] = new lin_ineq(X[i]->resize(0, X[i]->m(), 0, max));
    }
    lin_ineq ret = x.composeL(m, n, (const integer_matrix *const *)X, FALSE);
    if(&ret == NULL) return NULL;
    if(ret.m() == 0) return NULL;
    return new lin_ineq(ret);
}



lin_ineq * create_agtb(lin_ineq * a, lin_ineq * b, lin_ineq * r, int iseq)
{
    assert(a);
    assert(b);
    match(a, b);
    lin_ineq ret = (*a) - (*b);
    assert(ret.m() == 1);
    if(!iseq) ret[0][0] -= 1;

    if(r) {
        match(r, &ret);
        ret = Compose(2,1,
                      NIM(ret),
                      NIM(r));
    }
                      
    return new lin_ineq(ret);
}


void var_list::print(lin_ineq * l)
{
    lin_ineq * res = solve(l);
    assert(res);
    constraint kind(res->n());
    for(int i=1; i<res->n(); i++) 
        kind[i] = NM_LOCATIONS;
    lin_ineq_op Op(kind);
    Op.print_code(*res, NULL, L, stdout);
    delete res;
}
