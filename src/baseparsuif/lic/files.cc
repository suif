/* file "files.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <suif1.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <suifmath.h>
#include <dependence.h>
#include "syst_calc.h"
#include "syst_grammar.tab.h"

file_manager Files;

file_manager::file_manager()
{
    curr = 0;
    x = 0;
    nostd = 0;
}


FILE * file_manager::file()
{
    if(curr==0) return stdin;
    else return fp[curr-1];
}

void file_manager::push(char * fname)
{
    FILE * f = fopen(fname, "r");
    if(f == 0) printf("Cannot open %s as input\n", fname);
    else {
        if(curr >= NFILES) {
            printf("Maximum file # exceeded\n");
            exit_calc(-1);
        }
        fn[curr] = fname;
        fp[curr] = f;
        curr++;
    }
}


void file_manager::pop()
{
    if(curr == 0) exit_calc(0);

    fclose(fp[curr-1]);

    if(nostd&&(curr == 1)) exit_calc(0);

    curr--;
}


void file_manager::sttoken()
{
    x = 0;
}


void file_manager::endtoken()
{
    echo();
}

char file_manager::gc()
{
    int c = getc(file());
    if(c == EOF) {
        echo();
        printf("Error end of file reached");
        pop();
    }
    if(c == '\n') {
        echo(1);
        if(eok()&&tab) {
            for(int i=0; i<tl+3; i++)
                printf(" ");
            fflush(stdout);
        } 
    } 
    xbuf[x++] = c;

    return c;
}

void file_manager::ugc(char c)
{
    x--;
    ungetc(c, file());
}



int file_manager::gi()
{
    int val;
    char tmpbuf[32];
    fscanf(file(), "%d", &val);
    sprintf(tmpbuf, "%d", val);
    xbuf[x] = '\0';
    strcat(xbuf, tmpbuf);
    x = strlen(xbuf)+1;
    assert(x<512);
    return val;
}



void file_manager::echo(int /* nl */)
{
    xbuf[x] = '\0';
    if((!eok())&&x) {
        printf("%s", xbuf);  
//        if(nl) printf("\n");
        fflush(stdout);
    }
    x = 0;
}
