/* file "syst_start.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#define SUIF_NEED_GETOPT
#include <suif1.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <dependence.h>

#define yyparse systparse

#include "syst_calc.h"

extern int optind, opterr;
extern char *optarg;

extern jmp_buf * _suif_longjmp_env;


void init();

void syst_start(int argc, char * argv[])
{
    init();

    int c;
    while((c = getopt(argc, argv, "s:hcC")) != EOF) { 
        switch(c) {
        case 's':
            if(*optarg == 0) {
                fprintf(stderr, "missing script file\n");
                exit_calc(-1);
            } else {
                Files.push(optarg);
                Files.no_stdout();
            }
            break;

        case 'c':
        case 'C':
            break;

        case 'h':
            print_help();
            exit_calc(0);
            break;

        default: 
            print_help();
            exit_calc(0);
            break;
	} 
    }

    fprintf(stderr, "Rapid Prototyping System for Code Generation\n");
    fflush(stdout);
    fflush(stderr);
    printf(" > "); 
    fflush(stdout); 

    yyparse();
}


static void quitfun(int)
{
    printf("\n lic terminated\n");
    exit_calc(0);
}

static void contfun(int)
{
    printf("signal\n");
    longjmp(Env, 1);
}

#ifndef sgi
typedef void (*SIG_PF) (int);
#endif

void init()  
{
#ifdef YYDEBUG
    extern int yydebug;
    yydebug = 1;
#endif
    _suif_longjmp_env = &Env;
#ifdef DO_SIGNAL
    signal(SIGTERM, (SIG_PF)quitfun);
    signal(SIGINT,  (SIG_PF)contfun);
    signal(SIGTRAP, (SIG_PF)contfun);
    signal(SIGFPE,  (SIG_PF)contfun);
#endif
}


void exit_calc(int rc)
{
    named_sc_fm::delete_all();
    fflush(stdout);
    fflush(stderr);
    fprintf(stderr, "done(%d)\n\n", rc);
    exit(rc);
}
