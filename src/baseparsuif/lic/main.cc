/* file "main.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <suif1.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <setjmp.h>
#include <dependence.h>
#include <string.h>

extern void mat_start(int argc, char * argv[]);
extern void syst_start(int argc, char * argv[]);

jmp_buf MyEnv;
extern jmp_buf Env;

void resetjmp() 
{
    memcpy(&Env, &MyEnv, sizeof(jmp_buf));
}

int main(int argc, char * argv[])
{
    start_suif(argc, argv);
    
    for(int i=1; i<argc; i++)
        if(strcmp(argv[i], "-m") == 0) {
            if(setjmp(MyEnv) != 0) {
                
            } else {
                resetjmp();
            }
            mat_start(argc, argv);
            exit(0);
        } else if(strcmp(argv[i], "-c") == 0) {
            if(setjmp(MyEnv) != 0) {
                
            } else {
                resetjmp();
            } 
            syst_start(argc, argv);
            exit(0);
        }

    fprintf(stderr, "options are:\n");
    fprintf(stderr, "     %s -m        matrix and linear inequality calculator\n", argv[0]);
    fprintf(stderr, "     %s -c        rapid prototyping system for code generation\n", argv[0]);
    fprintf(stderr, "     for more information, type '%s -[c|m] -h'\n\n", argv[0]);
    exit_suif();
    return 0;
}
