/* file "mat_calc.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Data type for links in the chain of symbols.      */
#include <setjmp.h>
#include "syst_calc.h"



struct mat_symrec
{
    char *name;  /* name of symbol                     */
    lin_ineq * var;           /* value of a VAR          */
    int type;
    int num;
    mat_symrec *next;    /* link field              */
    mat_symrec();
};

/* The symbol table: a chain of `struct mat_symrec'.     */


typedef lin_ineq * lin_ineq_p;
struct compose_list {
    int m;
    int n;
    int i;
    boolean err;
    lin_ineq_p * X;

    compose_list(lin_ineq *);
    compose_list(compose_list *, lin_ineq *);

    boolean is_error() { return err; }
    void set_nl();
    lin_ineq * compose();
};


typedef char * charp;
#define MAXVARS         64
struct var_list {
    char * L[MAXVARS];
    int curr;
    var_list() { curr = 0; }
    void push(char * in) { assert(curr<MAXVARS); L[curr++] = in; }
    void print(lin_ineq *);
};


extern file_manager Files;



extern jmp_buf Env;
extern mat_symrec *sym_table;
extern file_manager Files;

void mat_print_help();
int yylex ();
int yyparse (void);
void yyerror (char * s) ;
int chktable (char * sym_name);
void init_table ();
mat_symrec * putsym (char * sym_name, int sym_type);
mat_symrec * getsym (char * sym_name);
lin_ineq * solve(lin_ineq *, int level=0);
void match(lin_ineq *, lin_ineq *);
void match(constraint *, constraint *);
lin_ineq * create_agtb(lin_ineq * a, lin_ineq * b, 
                       lin_ineq * r = NULL, 
                       int iseq =1);
lin_ineq * test_func1(lin_ineq * a);
lin_ineq * test_func2(lin_ineq * a, lin_ineq * b);


//#define YYDEBUG 1
#define ERR(s) { fprintf(stderr, "%s\n", s); YYERROR; }

