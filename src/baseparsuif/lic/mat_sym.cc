/* file "mat_sym.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#define SUIF_NEED_GETOPT
#include <suif1.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <suifmath.h>
#include <dependence.h>

#define yyparse matparse
#define yyerror materror

#include "mat_calc.h"
#include "mat_grammar.tab.h"


mat_symrec::mat_symrec()
{ 
    type = VAR; 
    next = NULL; 
    var = NULL; 
    name = NULL; 
    num = 0; 
}

extern int optind, opterr;
extern char *optarg;

void mat_start(int argc, char * argv[])
{
    fprintf(stderr, "Matrix and Linear Inequality Calculator\n");
    fflush(stdout);
    fflush(stderr);    

    init_table ();

    int c;
    while((c = getopt(argc, argv, "s:hmM")) != EOF) {
        switch(c) {
        case 's':
            if(*optarg == 0) {
                fprintf(stderr, "missing script file\n");
                exit(-1);
            } else {
                Files.push(optarg);
                Files.no_stdout();
            }
            break;

        case 'm':
        case 'M':
            break;

        case 'h':
            mat_print_help();
            exit(0);
            break;

        default: 
            mat_print_help();
            exit(0);
            break;
	} 
    }
    yyparse ();
}

void yyerror (char * s)  /* Called by yyparse on error */
{
    printf ("%s\n", s);
}

struct fntable
{
    char *fname;
    int token;
};

fntable MatFnTable[] = {
    {"help",                    HELP},
    {"h",                       HELP},
    {"identity",                IDENT},
    {"ident",                   IDENT},
    {"id",                      IDENT},
    {"code",                    CODE},
    {"parse",                   PARSE},
    {"solve",                   SOLVE},
    {"s",                       SOLVE},
    {"filterthru",              FILT_THRU},
    {"ft",                      FILT_THRU},
    {"filteraway",              FILT_AWAY},
    {"fa",                      FILT_AWAY},
    {"rowswap",                 ROW_SWAP},
    {"rs",                      ROW_SWAP},
    {"colswap",                 COL_SWAP},
    {"cs",                      COL_SWAP},
    {"rowdel",                  ROW_DEL},
    {"rd",                      ROW_DEL},
    {"coldel",                  COL_DEL},
    {"cd",                      COL_DEL},
    {"rowins",                  ROW_INS},
    {"ri",                      ROW_INS},
    {"colins",                  COL_INS},
    {"ci",                      COL_INS},
    {"inverse",                 INVERSE},
    {"inv",                     INVERSE},
    {"transpose",               TRANSPOSE},
    {"t",                       TRANSPOSE},
    {"determinant",             DETERMINANT},
    {"det",                     DETERMINANT},
    {"match",                   MATCH},
    {"m",                       MATCH},
    {"end",                     END},
    {"exit",                    EXIT},
    {"quit",                    EXIT},
    {"done",                    EXIT},
    {NULL,                      0}
};

int chktable (char * sym_name)
{
//    printf("!%s!\n", sym_name);
    for (int i=0; MatFnTable[i].fname != NULL; i++)
        if (strcmp (MatFnTable[i].fname, sym_name) == 0)
            return MatFnTable[i].token;
    return 0;
}


/* The symbol table: a chain of `struct mat_symrec'.  */

mat_symrec *sym_table = (mat_symrec *)0;


jmp_buf Env;
extern jmp_buf * _suif_longjmp_env;

void init_table ()  /* puts arithmetic functions in table. */
{
#ifdef YYDEBUG
    extern int yydebug;
    yydebug = 1;
#endif
//    mat_symrec *ptr;
//    for (int i = 0; arith_fncts[i].fname != 0; i++)
//        {
//            ptr = putsym (arith_fncts[i].fname, FNCT);
//            ptr->value.fnctptr = arith_fncts[i].fnct;
//        }
    printf(" > "); 
    fflush(stdout); 
    _suif_longjmp_env = &Env;
}


mat_symrec * putsym (char * sym_name, int sym_type)
{
    mat_symrec *ptr;
    ptr = (mat_symrec *) new mat_symrec;
    ptr->name = (char *) malloc (strlen (sym_name) + 1);
    strcpy (ptr->name,sym_name);
    ptr->type = sym_type;
    ptr->var = NULL; /* set value to 0 even if fctn.  */
    ptr->next = (struct mat_symrec *)sym_table;
    sym_table = ptr;
    return ptr;
}

mat_symrec * getsym (char * sym_name)
{
    mat_symrec *ptr;
    for (ptr = sym_table; ptr != (mat_symrec *) 0;
         ptr = (mat_symrec *)ptr->next)
        if (strcmp (ptr->name,sym_name) == 0)
            return ptr;
    return 0;
}


