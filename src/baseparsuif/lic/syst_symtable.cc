/* file "syst_symtable.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <suif1.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <suifmath.h>
#include <dependence.h>
#include "syst_calc.h"
#include "syst_grammar.tab.h"

symbol_table SymTab;

symrec::symrec(char * nm)
{
    name = new char[strlen(nm)+1];
    strcpy(name, nm);
    next = NULL;
    t = st_none;
}


void symrec::set(int i)
{
    t = st_int;
    data.i = i;
}


void symrec::set(named_symcoeff_ineq * i)
{
    t = st_sc;
    data.sc = i;
}


int symrec::token()
{
    switch(type()) {
    case st_none:
        return LVAR;

    case st_int:
        return IVAR;

    case st_sc:
        return QVAR;
    }
    assert(0);
    return 0;
}

var_sym * get_var(char * nm)
{
    base_symtab * symtab = fileset->globals();
    var_sym * sym;
    sym = symtab->lookup_var(nm);
    if(sym == NULL) {
        sym = symtab->new_unique_var(type_v0, nm);
        sym->set_name(nm);
    }

    return sym;
}

var_sym * symrec::var()
{
    return get_var(nm());
}


symbol_table::symbol_table()
{
    head = NULL;
}


symrec * symbol_table::find(char * nm)
{
    symrec * c = head;
    while(c) {
        if(strcmp(c->name, nm) == 0) return c;
        c = c->next;
    }
    return NULL;
}


symrec * symbol_table::put(char * nm)
{
    symrec * c = find(nm);
    if(c) return c;
    
    c = new symrec(nm);

    c->next = head;
    head = c;

    return c;
}

