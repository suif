/* file "String.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  String Class Definitions */

#ifndef PSCC_STRING_H
#define PSCC_STRING_H

#pragma interface

/*  the following file must be included before this point:
#include <string.h>
*/

#define INITSPACE 80


class String {
    char *data;				/* cannot be null */
    int space;				/* amount of space available */
    int used;				/* space used so far (string length) */

    void alloc_space(int length);	/* allocate space for initial string */
    void realloc_space();		/* reallocate space for "used" chars */

public:
    char *string() const		{ return data; }
    char *value() const			{ return strcpy(new char[used], data); }
    int length() const			{ return used - 1; }
    void clear()			{ used = 1; data[0] = '\0'; }

    String();
    String(const char *s);
    String(const char *s1, const char *s2);
    String(const String &s);
    ~String()				{ delete[] data; }

    void operator=(const String &s);
    void operator+=(char c);
    void operator+=(const char *s);
    void operator+=(const String &s)	{ *this += s.string(); }
};

#endif /* PSCC_STRING_H */
