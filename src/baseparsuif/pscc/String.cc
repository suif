/* file "String.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  String Implementation */

#pragma implementation "String.h"

#include <assert.h>
#include <string.h>
#include "String.h"


void String::alloc_space(int length)
{
    used = length + 1;

    /* allocate enough space to hold the initial string */
    space = INITSPACE;
    while (used > space) space = space * 2;
    data = new char[space];
}



String::String()
{
    alloc_space(0);
    data[0] = '\0';
}



String::String(const char *s)
{
    if (s) {
	alloc_space(strlen(s));
	strcpy(data, s);
    } else {
	alloc_space(0);
	data[0] = '\0';
    }
}



String::String(const char *s1, const char *s2)
{
    /* concatenate two character arrays to make a new String */
    assert(s1 && s2);

    int s1len = strlen(s1);
    alloc_space(s1len + strlen(s2));

    strcpy(data, s1);

    /* We could just do a strcat() here, but since we already know where
     * the end of the first string is, it's more efficient to do another
     * strcpy().  */

    strcpy(data + s1len, s2);
}



String::String(const String &s)
{
    used = s.used;
    space = s.space;
    data = new char[space];

    strcpy(data, s.string());
}



void String::operator=(const String &s)
{
    delete[] data;

    used = s.used;
    space = s.space;
    data = new char[space];

    strcpy(data, s.string());
}



void String::realloc_space()
{
    while (used > space) space = space * 2;
    char *tmp = new char[space];
    strcpy(tmp, data);
    delete[] data;
    data = tmp;
}



void String::operator+=(char c)
{
    if (c != '\0') {
	used += 1;
	if (used > space) realloc_space();
	data[used - 2] = c;
    }
    data[used - 1] = '\0';
}



void String::operator+=(const char *s)
{
    if (s) {

	int end_of_string = used - 1;
	used += strlen(s);
	if (used > space) realloc_space();

	/*  We could also use strcat() here, but it's more efficient to
	 *  just do a strcpy().  */

	strcpy(data + end_of_string, s);
    }
}
