/* file "runtime_names.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*------------------------------------------s----------------------------*
 *
 *  Names of variables and routines needed by popt and pgen
 *
 *----------------------------------------------------------------------*/

#ifndef RUNTIME_NAMES_H
#define RUNTIME_NAMES_H

/* Maximum size of the arguments passed into task functions */
#define MAX_ARGS_SIZE               1024  

/* Name of pointer to argument space */
#define SUIF_ARGS_NAME              "_suif_aligned_args"
#define SUIF_TASK_NAME              "_suif_aligned_task_f"
#define SUIF_MYNPROCS_NAME          "_suif_aligned_my_nprocs"
#define SUIF_MYNPROCS1_NAME         "_suif_aligned_my_nprocs1"
#define SUIF_MYNPROCS2_NAME         "_suif_aligned_my_nprocs2"

#define THREAD_MAP_NAME		    "_thread_map"

/* Define names from runtime library: */

#define NPROCS_NAME                 "suif_num_total_ids"  
#define DOALL_NAME                  "suif_named_doall"          
#define DOALL_LIMITED               "suif_limited_doall"
#define DOALL_LEVEL_NAME            "suif_doall_level"    
#define GLOBAL_BARRIER_NAME         "suif_global_barrier" 
#define SYNC_NEIGHBOR_NAME          "suif_sync_neighbor"  
#define LOCK_NAME                   "suif_lock"           
#define UNLOCK_NAME                 "suif_unlock"         
#define ASSIGN_MYIDS_NAME           "suif_assign_ids"     
#define ASSIGN_NPROCS_NAME          "suif_assign_nprocs"  
#define SPECULATE_BEGIN_NAME	    "suif_speculate_begin"   
#define SPECULATE_COMMIT_NAME	    "suif_speculate_commit"   
#define SPECULATE_TERMINATE_NAME    "suif_speculate_terminate"   
#define PAR_BEGIN_NAME	    	    "suif_par_begin"   
#define PAR_COMMIT_NAME	            "suif_par_commit"   
#define PAR_TERMINATE_NAME          "suif_par_terminate"   
#define COUNTER_WAIT_NAME           "suif_counter_wait"   
#define COUNTER_SET_NAME            "suif_counter_set"    
#define COUNTER_SET_RANGE_NAME      "suif_counter_set_range"    
#define COUNTER_INCR_NAME           "suif_counter_incr"   
#define COUNTER_INIT_NAME           "suif_counter_init_range"  
#define REDUCTION_LOCK_NAME         "suif_reduction_lock"    
#define REDUCTION_UNLOCK_NAME       "suif_reduction_unlock"    
#define CHECK_WORK_NAME             "suif_check_work" 
#define START_PACKING_NAME          "suif_start_packing"

/* FORTRAN compatible version of above names (six characters plus underbar)
   that can be called by suif-generated FORTRAN subroutines */

#define NPROCS_FORTRAN_NAME         "numids_" 
#define DOALL_LEVEL_FORTRAN_NAME    "doalev_" 
#define GLOBAL_BARRIER_FORTRAN_NAME "glbar_"
#define SYNC_NEIGHBOR_FORTRAN_NAME  "sync_" 
#define LOCK_FORTRAN_NAME           "slock_"
#define UNLOCK_FORTRAN_NAME         "sulock_" 
#define ASSIGN_MYIDS_FORTRAN_NAME   "setids_" 
#define ASSIGN_NPROCS_FORTRAN_NAME  "setnp_"  
#define SPECULATE_BEGIN_FORTRAN_NAME "sbegin_"  
#define SPECULATE_COMMIT_FORTRAN_NAME "commit_"  
#define SPECULATE_TERMINATE_FORTRAN_NAME "termin_"  
#define PAR_BEGIN_FORTRAN_NAME "pbegin_"  
#define PAR_COMMIT_FORTRAN_NAME "pcommi_"  
#define PAR_TERMINATE_FORTRAN_NAME "ptermi_"  
#define COUNTER_WAIT_FORTRAN_NAME   "cwait_"  
#define COUNTER_SET_FORTRAN_NAME    "cset_"   
#define COUNTER_SET_RANGE_FORTRAN_NAME    "csetr_"   
#define COUNTER_INCR_FORTRAN_NAME   "cincr_"  
#define COUNTER_INIT_FORTRAN_NAME   "cinitr_"  
#define REDUCTION_LOCK_FORTRAN_NAME "rlock_"    
#define REDUCTION_UNLOCK_FORTRAN_NAME "rulock_"    
#define CHECK_WORK_FORTRAN_NAME     "checkw_" 


/* Procedures in suif-generated code */

#define SUIF_START_NAME     "_suif_start"         
#define SUIF_NPROCS_NAME    "_suif_nproc"         


/* Variables used within tasks */

#define MAX_PROC_DIM 2   

#define MY_NPROCS_NAME       "_my_nprocs"   
#define MY_PID_NAME          "_my_id"       
#define MY_VPID_NAME         "_my_vid"      

#endif /* RUNTIME_NAMES_H */
