/* file "fastsync.h" */

/* ************************************************************
 * Fast synchronization routines
 * Originally developped for SimOS
 * For optimal performance, we assume that locks and barriers
 * are cache-line aligned
 * ************************************************************/

#ifndef FASTSYNC_H
#define FASTSYNC_H

/* ****************************************************
 * MasterSlaveBarrier. 
 * This special form of barrier is optimized with
 * for Master-Slave type of interactions, where
 * the master wants to know its slaves in the 
 * do some processing, pass along some information
 * to the slaves before releasing them.
 *
 * The slave processes must enter the barrier 
 * through MSlaveBarrier_SlaveEnter.
 * The master must first call MSlaveBarrier_Wait
 * and then call MSlaveBarrier_Release
 * MSlaveBarrier_SlaveFirstr is identical to ..SlaveEnter
 * but there to separate the initial spin from the rest.
 * 
 * This supports up to CACHE_LINE_SIZE processors only.
 * The master must have the id 0 and the slave 1..(numProcs-1)/sizeof(int)
 * *********************************************************/

#ifndef CACHE_LINE_SIZE
#define CACHE_LINE_SIZE 128
#endif

typedef struct MasterSlaveBarrier {
   volatile int entered[CACHE_LINE_SIZE/sizeof(int)];
   volatile int genNumber;
   char _pad0[CACHE_LINE_SIZE - sizeof(int)];
   int copyGenNumber;
   char _pad1[CACHE_LINE_SIZE - sizeof(int)];
} MasterSlaveBarrier;



extern void MSlaveBarrier_SlaveFirst(MasterSlaveBarrier *, int myid);
extern void MSlaveBarrier_SlaveEnter(MasterSlaveBarrier *, int myid);
extern void MSlaveBarrier_Wait      (MasterSlaveBarrier *, int numProcs);
extern void MSlaveBarrier_Release   (MasterSlaveBarrier *);


typedef struct FlashBarrierStruct {
  volatile int entered;
  volatile int cacheTouch;
  char _pad0[CACHE_LINE_SIZE - 2*sizeof(int)];
  volatile int genNumber;
  char _pad1[CACHE_LINE_SIZE - sizeof(int)];
} FlashBarrier;

typedef struct FlashLockStruct {
  volatile int lock;
  char _pad0[CACHE_LINE_SIZE - sizeof(int)];
} FlashLock;

#ifdef FAST_SYNC

#ifdef FAST_SYNC_ASM
extern void Simcp0Asm_Barrier( FlashBarrier *, int numProcs);
extern void Simcp0Asm_Lock(FlashLock *);
extern void Simcp0Asm_Unlock(FlashLock *);
#else /* FAST_SYNC_ASM */
extern void Simcp0_Barrier( FlashBarrier *, int numProcs);
extern void Simcp0_Lock(FlashLock *);
extern void Simcp0_Unlock(FlashLock *);
#endif /* FAST_SYNC_ASM */

#ifndef LOADLOCKED
#  define LOADLOCKED(value, address) \
    (value) = Simcp0_ll(&address)
#  define STORECOND(success, value, address) \
    (success) = Simcp0_sc(value, &address)
extern int Simcp0_ll(volatile int *address);
extern int Simcp0_sc(int value, volatile int *address);
#endif /* not LOADLOCKED */

#endif /* FAST_SYNC */

#endif
