	.verstamp 3 11 2 7 2
	.set noreorder
	.set volatile
	.set noat
	.file	1 "fastsync_pthreads.c"
gcc2_compiled.:
__gnu_compiled_c:
.text
	.align 3
	.globl Simcp0Asm_Barrier
	.ent Simcp0Asm_Barrier
Simcp0Asm_Barrier:
Simcp0Asm_Barrier..ng:
	.frame $30,0,$26,0
	.prologue 0
	mb
	ldl $1,128($16)
	zapnot $17,15,$17
	addl $1,$31,$3
	.align 5
$57:
	stl $31,4($16)
	ldl_l $1,0($16)
	addl $1,1,$2
	zapnot $2,15,$1
	cmpeq $1,$17,$1
	cmovne $1,0,$2
	bis $2,$2,$1
	stl_c $1,0($16)
	addl $1,$31,$1
	beq $1,$57
	bne $2,$63
	addq $3,1,$1
	stl $1,128($16)
	mb
	ret $31,($26),1
	.align 4
	.align 5
$63:
	ldl $1,128($16)
	subq $1,$3,$1
	beq $1,$63
	mb
	ret $31,($26),1
	.end Simcp0Asm_Barrier
	.align 3
	.globl Simcp0Asm_Lock
	.ent Simcp0Asm_Lock
Simcp0Asm_Lock:
Simcp0Asm_Lock..ng:
	.frame $30,0,$26,0
	.prologue 0
	.align 5
$69:
	ldl_l $1,0($16)
	addl $1,$31,$1
	bne $1,$69
	bis $31,1,$1
	stl_c $1,0($16)
	addl $1,$31,$1
	beq $1,$69
	mb
	ret $31,($26),1
	.end Simcp0Asm_Lock
	.align 3
	.globl Simcp0Asm_Unlock
	.ent Simcp0Asm_Unlock
Simcp0Asm_Unlock:
Simcp0Asm_Unlock..ng:
	.frame $30,0,$26,0
	.prologue 0
	mb
	stl $31,0($16)
	ret $31,($26),1
	.end Simcp0Asm_Unlock
