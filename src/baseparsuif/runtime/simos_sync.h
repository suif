/* file "simos_sync.h" */

/* ************************************************************
 * Fast synchronization routines
 * Originally developped for SimOS
 * For optimal performance, we assume that locks and barriers
 * are cache-line aligned
 * ************************************************************/

#ifndef SIMOS_SYNC_H
#define SIMOS_SYNC_H

#ifndef PAGE_SIZE 
#define PAGE_SIZE  4096
#endif
#ifndef CACHE_LINE_SIZE
#define CACHE_LINE_SIZE   128
#endif

/* ****************************************************
 * MasterSlaveBarrier. 
 * This special form of barrier is optimized with
 * for Master-Slave type of interactions, where
 * the master wants to know its slaves in the 
 * do some processing, pass along some information
 * to the slaves before releasing them.
 *
 * The slave processes must enter the barrier 
 * through MSlaveBarrier_SlaveEnter.
 * The master must first call MSlaveBarrier_Wait
 * and then call MSlaveBarrier_Release
 * MSlaveBarrier_SlaveFirstr is identical to ..SlaveEnter
 * but there to separate the initial spin from the rest.
 * 
 * This supports up to CACHE_LINE_SIZE processors only.
 * The master must have the id 0 and the slave 1..numProcs-1
 * *********************************************************/


typedef struct MasterSlaveBarrier {
   volatile char entered[CACHE_LINE_SIZE];
   volatile int genNumber;
   char _pad0[CACHE_LINE_SIZE - sizeof(int)];
   int copyGenNumber;
   char _pad1[CACHE_LINE_SIZE - sizeof(int)];
} MasterSlaveBarrier;



extern void MSlaveBarrier_SlaveFirst(MasterSlaveBarrier *, int myid);
extern void MSlaveBarrier_SlaveEnter(MasterSlaveBarrier *, int myid);
extern void MSlaveBarrier_Wait      (MasterSlaveBarrier *, int numProcs);
extern void MSlaveBarrier_Release   (MasterSlaveBarrier *);

#endif
