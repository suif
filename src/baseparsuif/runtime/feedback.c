char *feedback_name_table[] =
  {
#define BESTPAR(x, y) x,
#include "partune.data"
#undef BESTPAR
    (char *)0
  };

int feedback_nproc_limit_table[] =
  {
#define BESTPAR(x, y) y,
#include "partune.data"
#undef BESTPAR
    0
  };
