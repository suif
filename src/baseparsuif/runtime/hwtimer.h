
/*
 * Challenge Hardware timer interface
 * 
 * This service is only available on Challanges 
 * The address of the high order word of the timer *(iotimer_addr + 1) 
 * is the low order
 */



#ifndef SIMHWTIMER_H
#define SIMHWTIMER_H

/* This service is only available on Challanges */

/* The address of the high order word of the timer *(iotimer_addr + 1) is the low order */
extern unsigned* iotimer_addr;

/* Called from SimdetailInit */
void HwTimerInit( void );
void HwTimerStart(void);
void HwTimerEnd(void);
double HwTimerMicroSec(void);
double HwTimerSec(void);

#endif
