/*
 * This is a fast synchronization package to 
 * be used with machines that have 
 * Load Linked/Store Conditional support.
 *
 * It is written in code that GCC understands,
 * but for the sake of "portability", we generate
 * assembly for the machines that we are interested
 * in and those are considered the "sources"
 */

/* Optimized routines for fast barrier and locks */
/*  This is Standalone code with NO includes 
 *  That of course means that the assembly files need
 *  to be MANUALLY kept up to date 
 *  
 *  generate assembly files on an SGI with
 *  gcc -O2 -mips2 -DSGI -C -o fastsync_sgi.s fastsync.c
 *
 *  generate assembly files on an ALPHA with
 *  make -f Makefile.standalone fastsync_pthreads.c
 *  gcc -O2 -DALPHA -S -o fastsync_asm_alpha.s fastsync_pthreads.c
 *  Global replace cp0_ with cp0Asm_
 *  remove the code for the Master slave code
 *  Easy, huh...
 */


EXTERN_ENV

#define SC_FAIL 0

#include "fastsync.h"


/* **********************************************************
 * MSlaveBarrier
 * 
 * This implements the C routines of simos_sync.h. The
 * assembly ones are in simos_sycasm.s
 *
 * These routines implement the Master-Slave barriers.
 * The implementation ASSUMES SEQUENTIAL CONSISTENCY
 * unless the MEMORY_BARRIER macros are correctly defined 
 * The code has never been tested on machines that do not
 * have SC, the MEMORY_BARRIER calls are more of a hint for
 * the porting than anything else
 * ***********************************************************/

void MSlaveBarrier_SlaveFirst(MasterSlaveBarrier *b, int myid)
{
   int g = b->genNumber;

   MEMORY_BARRIER

   b->entered[myid] = 1;
   while( g == b->genNumber ) 
      continue;

   MEMORY_BARRIER
}


void MSlaveBarrier_SlaveEnter(MasterSlaveBarrier *b, int myid)
{
   int g = b->genNumber;

   MEMORY_BARRIER

   b->entered[myid] = 1;
   while( g == b->genNumber ) 
      continue;

   MEMORY_BARRIER
}

void MSlaveBarrier_Wait(MasterSlaveBarrier *b, int numProcs)
{
   int i;

   for(i=1;i<numProcs;i++) {
      while( !b->entered[i] ) 
         continue;
   }
   memset((void*)b->entered, 0, numProcs*sizeof(int));
}

void MSlaveBarrier_Release(MasterSlaveBarrier *b)
{
   MEMORY_BARRIER

   b->copyGenNumber++;
   b->genNumber = b->copyGenNumber;

   MEMORY_BARRIER
}

#ifdef FAST_SYNC
#ifndef FAST_SYNC_ASM

/*
 * Use these if compiling the C code
 * Otherwise, use the assembly routines.
 */

/* **********************************************************
 * Simcp_Barrier
 * Simcp_Lock
 * 
 * These routines implement a fast barrier and lock
 * by using Load Linked (locked) and Store Conditional
 * instructions
 * These routines are Somewhat dangerous.
 * There are a number of restrictions on the operations
 * allowed between LL/SC.  Suggest compilation
 * with gcc -O2 to inline the LL and SC as assembly
 * and remove all memory accesses between the LL/SC
 *
 * ***********************************************************/

void Simcp0_Barrier(FlashBarrier *b, int NumProcs) {
  register unsigned count=0;
  register int num_procs = NumProcs;
  int rv = SC_FAIL;
  int my_generation;
  
  MEMORY_BARRIER

  /* get the current generation */
  my_generation = b->genNumber;

  
  /* The loop for incrementing the Bar counter */
  while (rv == SC_FAIL) {
#ifdef PREFETCH_ON_LL
    b->cacheTouch = 0;
#endif

    LOADLOCKED(count, b->entered);
    count++;
    if (count == num_procs) { count = 0; }
    STORECOND(rv, count, b->entered);
  }

  /*
   * If this is the last process to arrive, release the rest.
   */
  if (count == 0) {
    b->genNumber = my_generation+1;
    MEMORY_BARRIER
    return;
  } 
    
  /*
   * If this is NOT the last process to arrive spin/wait
   */
#ifndef USE_NAP
  while (1) {
    if (b->genNumber != my_generation) {
      MEMORY_BARRIER
      return;
    }
  }
#else
  for (i = 0; i< 1000000 ; i++) {
    if (b->genNumber != my_generation) {
      MEMORY_BARRIER
      return;
    }
  }
  
  /* nap and then spin again */
  while(1) {
    NAPFUNC(1);
    for (i = 0; i< 10; i++) {
      if (b->genNumber != my_generation) {
        MEMORY_BARRIER
	return;
      }
    }
  }
#endif /* USE_NAP */
  MEMORY_BARRIER
  return;
}


/*
 * Simple spinning/waiting lock
 *
 */
void Simcp0_Lock(FlashLock *lock) {
  register int val;
  register int rv;
  while (1) {
    /* 
     * Load locked until the value is 0 and we succeed with a
     * Store conditional
     */
    LOADLOCKED(val, lock->lock);
    if (val == 0) {
      STORECOND(rv, 1, lock->lock);
      if (rv != SC_FAIL) {
	MEMORY_BARRIER
	return;
      }
    }
#ifdef USE_NAP    
    NAPFUNC(1);
#endif /* USE_NAP */
  }
}


/*
 *      Object:
 *              Simcp0_Unlock(int *)
 *
 *        Unock the specified lock.
 *
 *      Arguments:
 *              O0  lock = Address of the lock.
 *
 */
void Simcp0_Unlock(FlashLock *lock) {
  MEMORY_BARRIER
  lock->lock = 0;
}

/*
 * Use these routines to generate the assembly for
 * machines without gcc.  Place the assembly routine for
 * them in fastsync_asm_MACHINE.s
 */
#ifdef GENERATE_ASM
int Simcp0Asm_ll(int *lock) {
  int count;
  LOADLOCKED(count, *lock);
  return(count);
}

int Simcp0Asm_sc(int value, int *lock) {
  int rv;
  STORECOND(rv, value, *lock) ;
  return(rv);
}

#endif /* GENERATE_ASM */


#endif /* FAST_SYNC_ASM */
#endif /* FAST_SYNC */
