/* file "runtime.c" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" file. */

#include <suif_copyright.h>

/*----------------------------------------------------------------------*
 *
 *  runtime.c   Run-time routines for output of SUIF shared-memory compiler
 *
 *  GLOBAL DEFINITIONS
 *  ------------------
 *  int suif_get_my_id()                 return logical processor id
 *  int suif_num_total_ids()             return total # of processors
 *
 *  void suif_global_barrier(id)         block until all procs arrive
 *  void suif_barrier(id,n)              block until n procs arrive
 *
 *  void suif_sync_neighbor(id)          block until neighbors arrive
 *
 *  void suif_lock(id)                   block until lock acquired
 *  void suif_unlock(id)                 release lock
 *
 *  double suif_clock()                  return time in seconds
 *  double suif_hi_res_clock()           return time in seconds (better
 *                                       resolution than suif_clock(),
 *                                       but not guaranteed not to
 *                                       overflow
 *  void suif_start_timer()              start accurate timer
 *  double suif_end_timer()              finish accurate timer, return
 *                                       time in seconds
 *
 *  void suif_doall(func,ptr)            parallel call
 *  int suif_doall_level() 	         return level of parallelism
 *
 *  void suif_restore_state(pid, my_id)  called by hardware simulator to restore
 *  void restart(pid)                    called by hardware simulator to restore
 *
 *  void suif_finc_init(id)              reset fetch and increment counter to 0
 *  int suif_finc(id)                    fetch value and increment counter
 *
 *  void suif_counter_init_all()         reset all counters to 0
 *  void suif_counter_init_range(id)     reset counters in range 0..id to 0
 *  void suif_counter_init(proc,id)      reset counter to 0
 *  void suif_counter_incr(proc,id)      increment counter
 *  void suif_counter_set(proc,id,val)   set counter to val
 *  void suif_counter_set_range(proc,id,val)   set counters [0..proc-1][0..id-1] to val
 *  void suif_counter_wait(proc,id,val)  block until counter >= value
 *
 *  void suif_speculate_begin()          begin speculation
 *  void suif_speculate_commit()         commit speculation
 *  void suif_speculate_terminate()      terminate speculation (also commit)
 *  void suif_par_begin()          begin speculation
 *  void suif_par_commit()         commit speculation
 *  void suif_par_terminate()      terminate speculation (also commit)
 *  void suif_infinite_loop()            infinite loop
 *  void *suif_malloc(size_t size)       allocate data, cache-line aligned
 *
 *  void reduce_OP_TYPE()                reductions with OP on TYPE
 *
 *  void suif_reset_stats()              reset statistics
 *
 *  void suif_exit_log()                 log exit for other threads
 *
 *  EXTERNAL SYMBOLS REQUIRED
 *  -------------------------
 *  int _suif_start(argc,argv,envp)    root of application program
 *  int _suif_nproc                    required # of procs if non-zero
 *
 *  These routines are intended to support the output of the SUIF
 *  shared-memory compiler on a variety of architectures.
 *  Currently support is provided for SGI, DASH, KSR and generic pthreads.
 *
 *  This system is modeled after the run-time routines provided by
 *  Martin Rinard.  It uses the ANL macros provided by JP Singh,
 *  Truman Joe, and other members of the DASH group at Stanford.
 *  Names & calling conventions of the run-time system roughly match
 *  those of the p4 parallel programming system by Butler & Lusk
 *  at Argonne.
 *
 *  The root of the SUIF application program is named _suif_start().
 *  Parallel execution is provided by calling suif_doall() with
 *  a pointer to the function to be executed in parallel.
 *
 *  Example program:
 *
 *  int _suif_nproc = 0;     // does not required fixed # of procs
 *  extern void *volatile _suif_aligned_args;
 *  extern void (**volatile _suif_aligned_task_f)(int);
 *
 *
 *  int _suif_start(argc, argv, envp)
 *    int argc;
 *    char **argv, **envp;
 *  {
 *    struct my_args args;
 *
 *    ...                           // work to be performed sequentially
 *    *_suif_aligned_task_f = foo;
 *    *_suif_aligned_args = .../    // arguments to task funcs
 *    suif_doall(foo);              // foo is executed in parallel
 *    ...                           // work to be performed sequentially
 *  }
 *
 *  void foo(myid)
 *    int myid;               // id of worker thread, from 0...P-1
 *  {
 *    ...                     // work to be performed in parallel
 *  }
 *
 *  ENVIRONMENTAL VARIABLES
 *  -----------------------
 *  PL_NUM_THREADS            // number of thread to execute
 *  PL_PROC_START             // physical proc id of 1st thread
 *  PL_MAXPROC                // total # of procs (max physical id+1)
 *                            Do not execute loops iff:
 *  PL_LIMITS_ITER            // number of iterations is less than ..
 *  PL_LIMITS_LOADBALANCE     // % of load imbalance is less than ..
 *  PL_LIMITS_WORK            // amount of work in the loop body *
 *                                # of iterations is less than ..
 *
 *  Environmental variables are used to specify the number and mapping
 *  of threads.  The system creates a number of threads equal to
 *  PL_NUM_THREADS, then maps them to physical processors starting
 *  at PL_PROC_START, wrapping at PL_MAXPROC back to processor 0.
 *
 *  Threads are assigned logical ids from 0 to PL_NUM_THREADS-1.
 *  The original master thread is guaranteed to be thread 0.  The
 *  run-time system supports only a single level of parallelism.
 *  Nested calls to suif_doall() result in a single thread with id
 *  equal to -1.
 *
 *  If the SUIF compiler produces code that requires a fixed number of
 *  processors, the global variable _suif_nproc is set to the number.
 *  If a variable number of processors is allowed, it is zeroed.
 *
 *  Lock, barrier, and fetch&inc counter routines require an id.
 *  Currently eight of each are supported.   Counter routines require
 *  an processor and counter id.  Currently DEFAULT_MAXCOUNTER are
 *  supported for each processor.
 *
 *  Locks, barriers, and fetch&inc counters provide mutual exclusion
 *  on updates; they can be safely updated by multiple processors.
 *  In comparison, normal counters do not provide mutual exclusion.
 *  For safety, each processor should thus only write to its own
 *  counter, though this restriction is not enforced.
 *
 *----------------------------------------------------------------------*/

MAIN_ENV

#include "runtime.h"
#include "runtime_names.h"
#include <setjmp.h>

/*----------------------------------------------------------------------
 *  Declarations
 *----------------------------------------------------------------------
 */


/* Create padded version of variables to avoid false sharing */

typedef struct suif_aligned_vars {
  int _nproc;
  int _proc_start;
  int _maxproc;
  char _pad1[CACHE_LINE_SIZE - ((3 * sizeof(int)) % CACHE_LINE_SIZE)];

  MasterSlaveBarrier barrier;
} suif_aligned_vars;

char _aligned_vars_space[sizeof(suif_aligned_vars) + (8 * CACHE_LINE_SIZE)];
suif_aligned_vars *_suif_aligned_vars = 0;

/* _aligned_args_space holds doall_level, task function pointer and task
   arguments.  doall_level is set in runtime, task function pointer and
   task arguments are set in generated code
 */
char _aligned_args_space[(2 * sizeof(void *)) + MAX_ARGS_SIZE +
                         (8 * CACHE_LINE_SIZE)];

/* Variables used in SUIF compiled code */

int *volatile _suif_aligned_my_nprocs = NULL;
int *volatile _suif_aligned_my_nprocs1 = NULL;
int *volatile _suif_aligned_my_nprocs2 = NULL;

int *volatile _suif_aligned_doall_level = NULL;
task_f *volatile _suif_aligned_task_f = NULL;
void *volatile _suif_aligned_args = NULL;

int _thread_map[DEFAULT_MAXPROC];
int restart_map[DEFAULT_MAXPROC];
jmp_buf jmpbuf[DEFAULT_MAXPROC];

/* OBSOLETE: The following three variables are provided for
 * compatibility with previous releases of this package.  Eventually,
 * they will be removed entirely.  Note that code that uses these
 * variables instead of the pointers to the aligned versions will not
 * get the benefit of suif_limited_doall() or feedback to cut the
 * degree of parallelism -- the code will run on all processors in
 * that case. */

int _my_nprocs;
int _my_nprocs1;
int _my_nprocs2;

/* Runtime structure Used for feedback to
 * limit parallelism
 */
typedef struct suif_dynamic_feedback_struct {
  int iter_threshhold;   /* do not parallelize below this number of
			    iterations */
  int max_parallelism;   /* never use more than this number of processors */
} suif_dynamic_feedback;


/* External vars */

extern int _suif_start(int argc, char **argv, char **envp);
extern int _suif_nproc;

/* Private vars */

static int _argc;
static char **_argv;
static char **_envp;

static int _limits_iter;
static int _limits_ldb;
static int _limits_work;
static int _limits_body;
static int _limits_work_small_iter;
static int _dynamic_feedback_on;
static char *_dynamic_feedback_file_name;

static PIDTYPE _master_pid;

static volatile int _main_stop;
static volatile int _mylocks[DEFAULT_MAXPROC][DEFAULT_MAXCOUNTER];

static void suif_initenv(void);
static void suif_init_dynamic_feedback(void);
static void suif_create(void (*)(void));
static void suif_worker(void);
static void _barrier(void);
static void _barrier2(void);
static void *cache_line_align(void *addr);
static double safe_divide(double a, double b);


#if (defined(STATS) && defined(ALL_STATS)) || defined(FEEDBACK) || \
    defined(GEN_TRACE)
static void **task_func_table = NULL;
static char **task_name_table = NULL;
static char *current_task_name = NULL;
static int table_size = 0;
#endif
static int current_task_num = 0;

/*
 * These are used for integrating feedback
 * to limit parallelism
 */
#define DYNAMIC_FEEDBACK_TABLE_SIZE 512

static suif_dynamic_feedback *suif_dynamic_feedback_table;
static suif_dynamic_feedback
        static_dynamic_feedback_table[DYNAMIC_FEEDBACK_TABLE_SIZE];
static int suif_dynamic_feedback_doalls = 0;

#if (defined(STATS) && defined(ALL_STATS)) || defined(GEN_TRACE)
static int current_task_iters_const = 0;
#endif
#if defined(STATS) || defined(ALL_STATS) || defined(GEN_TRACE)
static int current_task_num_iters = 0;
#endif

#if defined(STATS)
static SUIF_HI_RES_TIMER_DATA_TYPE alltime_hi_res;
static double alltime_low_res;
static double partime;
static int num_doalls;
#if defined(SEQUENTIAL)
static int num_barriers;
static int num_sync_neighbors;
static int num_locks;
static int num_reductions;
#endif
#if defined(ALL_STATS)
static double *stat_time_table = NULL;
static int *stat_doalls_table = NULL;
static double *stat_iters_table = NULL;
#if defined(SEQUENTIAL)
static int *stat_barriers_table = NULL;
static int *stat_sync_neighbors_table = NULL;
static int *stat_locks_table = NULL;
static int *stat_reductions_table = NULL;
#endif
#endif
#endif


#if defined(GEN_TRACE)
typedef struct trace_data_struct {
  int    func_num;
  int    num_iters;
  double time;
} trace_data;
static trace_data *trace_space;
static int num_trace_dumps = 0;
static int num_traces = 0;
static int max_traces = 1024*1024;  /*Take 1 meg at a time */
static void gen_trace(int, int, double);
static void suif_inittrace(void);
static void dump_trace(void);
#if defined(STATS)
static double trace_dump_time = 0.0;
#endif
#endif

#if defined(FEEDBACK)
extern char *feedback_name_table[];
extern int feedback_nproc_limit_table[];
static int *limit_nproc_table = NULL;
#endif

/* system barriers & locks */

BARDEC(_sysbar1)
BARDEC(_sysbar2)
FOPDEC(_sysfop)

/* user barriers & locks */

BARDEC(_bar1)
BARDEC(_bar2)
BARDEC(_bar3)
BARDEC(_bar4)
BARDEC(_bar5)
BARDEC(_bar6)
BARDEC(_bar7)
BARDEC(_bar8)

BARDEC(_bar_global0)
BARDEC(_bar_global1)
BARDEC(_bar_global2)
BARDEC(_bar_global3)
BARDEC(_bar_global4)
BARDEC(_bar_global5)
BARDEC(_bar_global6)
BARDEC(_bar_global7)

FOPDEC(_fop1)
FOPDEC(_fop2)
FOPDEC(_fop3)
FOPDEC(_fop4)
FOPDEC(_fop5)
FOPDEC(_fop6)
FOPDEC(_fop7)
FOPDEC(_fop8)

LOCKDEC(_lock1)
LOCKDEC(_lock2)
LOCKDEC(_lock3)
LOCKDEC(_lock4)
LOCKDEC(_lock5)
LOCKDEC(_lock6)
LOCKDEC(_lock7)
LOCKDEC(_lock8)

/* reduction barriers & locks */

#define NUM_RLOCK	64

ALOCKDEC(_lock_reduction,NUM_RLOCK)

/* extra function called by main when compiled with gcc */
#ifdef __GNUC__
void __main() { }
#endif


/*----------------------------------------------------------------------*/
/* main routine */

/* main() - read parameters, do initializations, start threads, then   */
/*          pass control to the application by calling _suif_start().  */

int main(int argc, char **argv, char **envp)
{
    char **ptr;
    int i;

    /* initialize suif_aligned_{vars,args} to be aligned on cache-lines */
  {
     char *args;

     _suif_aligned_vars =
         (suif_aligned_vars *) cache_line_align(&_aligned_vars_space[0]);

     args = (char *) cache_line_align(&_aligned_args_space[0]);

     _suif_aligned_doall_level = (int *) args;
     _suif_aligned_task_f = (task_f *) (args + sizeof(void *));
     _suif_aligned_my_nprocs = (int *) (args + (2 * sizeof(void *)));
     _suif_aligned_my_nprocs1 = (int *) (args + (3 * sizeof(void *)));
     _suif_aligned_my_nprocs2 = (int *) (args + (4 * sizeof(void *)));
     _suif_aligned_args = (void *) (args + (6 * sizeof(void *)));
  }

#if defined(STATS)
    partime = 0.0;
    num_doalls = 0 ;
#if defined(SEQUENTIAL)
    num_barriers = 0 ;
    num_sync_neighbors = 0 ;
    num_locks = 0 ;
    num_reductions = 0 ;
#endif
#endif

    /* get some parameters from environment variables */

    _suif_aligned_vars->_nproc      = DEFAULT_NUM_THREADS;
    _suif_aligned_vars->_proc_start = DEFAULT_PROC_START;
    _suif_aligned_vars->_maxproc    = DEFAULT_MAXPROC;
    _limits_iter = DEFAULT_LIMITS_ITER;
    _limits_ldb  = DEFAULT_LIMITS_LOADBALANCE;
    _limits_work = DEFAULT_LIMITS_WORK;
    _limits_body = DEFAULT_LIMITS_BODY;
    _limits_work_small_iter = DEFAULT_LIMITS_WORK_SMALL_ITER;
    _dynamic_feedback_on = 0;

    for (ptr=envp; *ptr; ptr++) {
	if (!strncmp("PL_NUM_THREADS=", *ptr, 15)) {
	    _suif_aligned_vars->_nproc = atoi(*ptr+15);

	    /* synchronization in simos_sync.h can only handle
	       nproc < CACHE_LINE_SIZE */
	    if ((_suif_aligned_vars->_nproc  < 1) ||
		(_suif_aligned_vars->_nproc >= CACHE_LINE_SIZE)) {
		fprintf(stderr, "Illegal value for PL_NUM_THREADS\n");
		_suif_aligned_vars->_nproc = DEFAULT_NUM_THREADS;
	    }
	}
	if (!strncmp("PL_PROC_START=", *ptr, 14)) {
	    _suif_aligned_vars->_proc_start = atoi(*ptr+14);
	}
	if (!strncmp("PL_MAXPROC=", *ptr, 11)) {
	    if ((_suif_aligned_vars->_maxproc = atoi(*ptr+11)) < 1) {
		fprintf(stderr, "Illegal value for PL_MAXPROC\n");
		_suif_aligned_vars->_maxproc = DEFAULT_MAXPROC;
	    }
	}
	if (!strncmp("PL_LIMITS_ITER=", *ptr, 15)) {
	    _limits_iter = atoi(*ptr+15);
	}
	if (!strncmp("PL_LIMITS_LOADBALANCE=", *ptr, 22)) {
	    _limits_ldb = atoi(*ptr+22);
	}
	if (!strncmp("PL_LIMITS_WORK=", *ptr, 15)) {
	    _limits_work = atoi(*ptr+15);
	}
	if (!strncmp("PL_LIMITS_BODY=", *ptr, 15)) {
	    _limits_body = atoi(*ptr+15);
	}
	if (!strncmp("PL_LIMITS_WORK_SMALL_ITER=", *ptr, 26)) {
	    _limits_work_small_iter = atoi(*ptr+26);
	}
	if (!strncmp("PL_DYNAMIC_FEEDBACK=", *ptr, 20)) {
	    _dynamic_feedback_on = 1;
	    _dynamic_feedback_file_name = &((*ptr)[20]);
	}
    }

    /* initializations */

#if defined(SEQUENTIAL)
    _suif_aligned_vars->_nproc = 1;
#endif

    /* if _suif_nproc is nonzero, use compiler predetermined # of procs */
    if (_suif_nproc != 0)
        _suif_aligned_vars->_nproc = _suif_nproc;

    suif_initenv();
    _argc = argc;
    _argv = argv;
    _envp = envp;


    /* read in the dynamic feedback file for suppressing paralllelism */
    if (_dynamic_feedback_on) suif_init_dynamic_feedback();
#if defined(GEN_TRACE)
    suif_inittrace();
#endif

#if defined(STATS)
    SUIF_INIT_HI_RES_CLOCK
    SUIF_START_TIMER(alltime_hi_res, alltime_low_res);
#endif

    /* get a unique identifier to identify the master thread */
    _master_pid = GETUNIQUEID;

    /* spawn worker threads, begin computation */

    for(i=1; i < _suif_aligned_vars->_nproc; i++) {
	suif_create(suif_worker);
    }

    if (_suif_aligned_vars->_nproc > 1) {
	SETSCHED()
    }

    suif_worker();

    suif_wait_for_end();
#if defined(GEN_TRACE)
    dump_trace();
#endif
    return 0;
}

double safe_divide(double a, double b) {
  if (b == 0.0) return(0.0);
  return(a / b);
}

void *cache_line_align(void *addr)
{
     ptrdiff_t ptr = (ptrdiff_t) addr;
     ptr = (ptr + (ptrdiff_t) (CACHE_LINE_SIZE - 1)) &
         (~((ptrdiff_t) (CACHE_LINE_SIZE - 1)));
     return (void *) ptr;
}


void *suif_malloc(size_t size)
{
  int num_bytes = size + (2*CACHE_LINE_SIZE);
  void *ptr = malloc(num_bytes);

  return (cache_line_align(ptr));
}


/*----------------------------------------------------------------------*/
/* read in the runtime dynamic feedback file if it exists
 * Looks for a file named by the PL_DYNAMIC_FEEDBACK environment variable.
 * Creates a table indexed by doall loop numbers
 * for fast runtime access to determine number of
 * processors to run on
 */
static void suif_init_dynamic_feedback()
{
  FILE *fp;
  char line[512];      /* reasonable limit to a line size */
  char name[512];      /* reasonable limit for a string */
  char *fname = _dynamic_feedback_file_name;
  int in_region = 0;   /* flag to determine when the first line is read */
  int num_doalls = 0;

  fp = fopen(fname, "r");
  if (!fp) {
    fprintf(stderr, "Unable to open dynamic feedback file `%s', ignoring\n",
            fname);
    _dynamic_feedback_on = 0;
    return;
  }

  /* read in lines until we run out */
  while (fgets(line, 512, fp) != NULL) {
    /* ignore comments */
    if (line[0] == '#') { continue; }

    /* Use scanf for simplicity */
    if (!in_region) {
      int i, rv;
      if ((rv = sscanf(line, "DOALLS %d\n", &num_doalls)) == 1) {
	if (num_doalls < 0) {
	  fprintf(stderr, "Error in dynamic feedback file `%s', ignoring\n",
                  fname);
	  _dynamic_feedback_on = 0;
	  fclose(fp);
	  return;
	}

        if (num_doalls > DYNAMIC_FEEDBACK_TABLE_SIZE)
          {
            suif_dynamic_feedback_table =
                (suif_dynamic_feedback *)malloc(sizeof(suif_dynamic_feedback) *
                                                num_doalls);
            if (suif_dynamic_feedback_table == NULL)
                perror("malloc failed");
          }
        else
          {
            suif_dynamic_feedback_table = static_dynamic_feedback_table;
          }

	for (i = 0; i< num_doalls; i++) {
	  /* do not supress by default */
	  suif_dynamic_feedback_table[i].iter_threshhold = 0;
	  suif_dynamic_feedback_table[i].max_parallelism = 1000;
	}
	in_region = 1;
	continue;
      }
    }

    if (in_region) {
      int doall_index, iter_threshhold, max_parallelism;
      int rv;

      if ((rv = sscanf(line, "ENTRY %s %d %d %d\n", name, &doall_index,
		       &iter_threshhold, &max_parallelism)) == 4) {
	/* fprintf(stderr,"ENTRY %s %d %d %d\n", name, doall_index,
		iter_threshhold, max_parallelism);  */
	if ((doall_index < 0) || (doall_index > num_doalls)) {
	  fprintf(stderr,
		  "IGNORED: %s: doall_index(=%d) > num_doalls(=%d)\nline=%s\n",
		  fname, doall_index, num_doalls, line);
	} else {
	  suif_dynamic_feedback_table[doall_index].iter_threshhold =
                  iter_threshhold;
	  suif_dynamic_feedback_table[doall_index].max_parallelism =
                  max_parallelism;
	}
	continue;
      }
    }
    fprintf(stderr, "IGNORED: %s: inregion=%d, line=%s\n",
	    fname, in_region, line);
  }

  suif_dynamic_feedback_doalls = num_doalls;
  fclose(fp);
}


/* use the feedback data to determine the number of processors
 * to run on  With any luck, it will be inlined
 */
static int suif_dynamic_feedback_get_nprocs(int task_func_num,
                                            int num_iters, int num_procs) {
  int max_parallelism;
  if (!_dynamic_feedback_on)  return(num_procs);

  /* blindly assert that task_func_num is ALWAYS >0 and < num_doalls */
  if ((task_func_num < 0) || (task_func_num >= suif_dynamic_feedback_doalls)) {
    return(num_procs);
  }
  /* if we are below the threshhold, run on one processor */
  if (suif_dynamic_feedback_table[task_func_num].iter_threshhold > num_iters) {
    return(1);
  }
  /* use the min(max_parallelism, num_procs, num_iters); */
  max_parallelism = suif_dynamic_feedback_table[task_func_num].max_parallelism;
  max_parallelism = ((max_parallelism <= num_procs)
		     ? max_parallelism : num_procs);
  max_parallelism = ((max_parallelism <= num_iters)
		     ? max_parallelism : num_iters);
  return(max_parallelism);
}

#if defined(GEN_TRACE)
/*
 * init generate trace
 * allocate
 */
static void suif_inittrace() {
  trace_space = (trace_data *)malloc(sizeof(trace_data) * max_traces);
  /* for the challenge */
}

/* ignmore errors when dumping trace */
static void dump_trace(void) {
  FILE *fp;
  char *fname = "RTdumpfile";
  int i;
  if (num_traces == 0) return;
  if (num_trace_dumps == 0) {
    fp = fopen(fname, "w");
  } else {
    fp = fopen(fname, "a");
  }
  fwrite ((void *) trace_space, sizeof(trace_data), num_traces, fp);
  fclose(fp);
  num_traces = 0;
  num_trace_dumps++;
}

static void gen_trace(int func_num, int num_iters, double time) {
  trace_space[num_traces].func_num = func_num;
  trace_space[num_traces].num_iters = num_iters;
  trace_space[num_traces].time = time;
  num_traces++;
  if (num_traces == max_traces) {
#if defined(STATS)
    SUIF_HI_RES_TIMER_DATA_TYPE hi_res_trace_start;
    double low_res_trace_start;
    double test_time;

    SUIF_START_TIMER(hi_res_trace_start, low_res_trace_start);
#endif
    dump_trace();
#if defined(STATS)
    SUIF_END_TIMER(test_time, hi_res_trace_start, low_res_trace_start);
    trace_dump_time += test_time;
#endif
  }
}

#endif

/*----------------------------------------------------------------------*/
/* process management */

/* suif_initenv() - initialize environment & synchronization constructs */

static void suif_initenv(void)
{
  MAIN_INITENV_LITE(,)

  /* system barriers & locks */

  BARINIT(_sysbar1)
  BARINIT(_sysbar2)
  FOPINIT(_sysfop,0)

  /* user barriers & locks */

  BARINIT(_bar1)
  BARINIT(_bar2)
  BARINIT(_bar3)
  BARINIT(_bar4)
  BARINIT(_bar5)
  BARINIT(_bar6)
  BARINIT(_bar7)
  BARINIT(_bar8)

  BARINIT(_bar_global0)
  BARINIT(_bar_global1)
  BARINIT(_bar_global2)
  BARINIT(_bar_global3)
  BARINIT(_bar_global4)
  BARINIT(_bar_global5)
  BARINIT(_bar_global6)
  BARINIT(_bar_global7)

  FOPINIT(_fop1,0)
  FOPINIT(_fop2,0)
  FOPINIT(_fop3,0)
  FOPINIT(_fop4,0)
  FOPINIT(_fop5,0)
  FOPINIT(_fop6,0)
  FOPINIT(_fop7,0)
  FOPINIT(_fop8,0)

  LOCKINIT(_lock1)
  LOCKINIT(_lock2)
  LOCKINIT(_lock3)
  LOCKINIT(_lock4)
  LOCKINIT(_lock5)
  LOCKINIT(_lock6)
  LOCKINIT(_lock7)
  LOCKINIT(_lock8)

  /* reduction locks */
  ALOCKINIT(_lock_reduction,NUM_RLOCK)

  suif_counter_init_all();

  _main_stop = 0;
  *_suif_aligned_doall_level = 0;
  *_suif_aligned_my_nprocs = _suif_aligned_vars->_nproc;
  _my_nprocs = *_suif_aligned_my_nprocs;

  suif_assign_nprocs();
  _my_nprocs1 = *_suif_aligned_my_nprocs1;
  _my_nprocs2 = *_suif_aligned_my_nprocs2;
}

/*----------------------------------------------------------------------*/
/* suif_create() - creates lightweight thread to execute function f */

static void suif_create(void (*f)(void))
{
  CREATE_LITE(f)
}


/*----------------------------------------------------------------------*/
/* suif_get_my_id() - return logical thread id (0...P-1) */

int suif_get_my_id(void)
{
  int id;

  GETID(id);

  return id;  /* logical id from 0 to PL_NUM_THREADS-1 */
}


/*----------------------------------------------------------------------*/
/* suif_num_total_ids() - return total number of thread */

int suif_num_total_ids(void)
{
  return _suif_aligned_vars->_nproc;  /* total number of threads */
}


int numids_(void)
{
  return _suif_aligned_vars->_nproc;
}



/*----------------------------------------------------------------------*/
/* suif_assign_ids() - initializes _my_ids */

void suif_assign_ids(int myid, int *myid1, int *myid2)
{
  int my_nprocs1 = *_suif_aligned_my_nprocs1;
  *myid1 = myid % my_nprocs1;
  *myid2 = myid / my_nprocs1;
}


void setids_(int *myid, int *myid1, int *myid2)
{
    suif_assign_ids(*myid, myid1, myid2);
}


/*----------------------------------------------------------------------*/
/* suif_assign_nprocs() - initializes _my_nprocs */

void suif_assign_nprocs(void)
{
    int my_nprocs1, my_nprocs2;
    int nproc = *_suif_aligned_my_nprocs;

    if (nproc < 4)
        my_nprocs2 = 1;
    else if (nproc < 9)
        my_nprocs2 = 2;
    else if ((nproc == 9) ||
	     (nproc == 15) ||
             (nproc == 18))
        my_nprocs2 = 3;
    else if ((nproc < 25) ||
	     (nproc == 32))
        my_nprocs2 = 4;
    else if ((nproc == 25) ||
	     (nproc == 35) ||
             (nproc == 40) ||
	     (nproc == 50))
        my_nprocs2 = 5;
    else if (nproc < 49)
        my_nprocs2 = 6;
    else if (nproc == 49)
        my_nprocs2 = 7;
    else
        my_nprocs2 = 8;

    my_nprocs1 =
      nproc/my_nprocs2;

    /* only modify the global variable if needed */
    if (my_nprocs1 != *_suif_aligned_my_nprocs1) {
      *_suif_aligned_my_nprocs1 = my_nprocs1;
    }
    if (my_nprocs2 != *_suif_aligned_my_nprocs2) {
      *_suif_aligned_my_nprocs2 = my_nprocs2;
    }
}


void setnp_(void)
{
    suif_assign_nprocs();
}


/*----------------------------------------------------------------------*/
/* suif_restore_state() - called by hardware simulator to restore registers */

void suif_restore_state(int pid, int my_id /* _thread_map[pid] */)
{
    longjmp(jmpbuf[pid], my_id);
}


/*----------------------------------------------------------------------*/
/* restart() - called by hardware simulator to restore registers */

int restart(int pid)
{
    restart_map[pid] = pid;
    return 0;
}


/*----------------------------------------------------------------------*/
/* suif_doall_level() - return depth of nesting in doall loops */

int suif_doall_level(void)
{
  /* (*_suif_aligned_doall_level) begins at 0 and is incremented */
  /* after each nested call to suif_doall() */

  return (*_suif_aligned_doall_level);
}

int doalev_(void)
{
  /* (*_suif_aligned_doall_level) begins at 0 and is incremented */
  /* after each nested call to suif_doall() */

  return (*_suif_aligned_doall_level);
}


/*----------------------------------------------------------------------*/
/* suif_check_work() - return whether the amount of work in the given */
/*    parameters is greater than the threshold set by the environment */
/*    variables */


int checkw_(int *lb, int *ub, double *work, int *cflow)
{
    return suif_check_work(*lb, *ub, *work, *cflow);
}


int suif_check_work(int lb, int ub, double work, int cflow)
{
    int i = ub-lb+1;
    double total_work;
    if (i < _suif_aligned_vars->_nproc) return 0;

    total_work = i * work;
    if(total_work < (double)_limits_work) return 0;
    if(i < _limits_iter &&
       total_work < (double)_limits_work_small_iter) return 0;

    return 1;
}



/*----------------------------------------------------------------------*/
/* suif_wait_for_end() - clean up routines */

void suif_wait_for_end(void)
{
#if defined(STATS)
  double alltime;

  SUIF_END_TIMER(alltime, alltime_hi_res, alltime_low_res);
#if defined(GEN_TRACE)
  alltime = alltime - trace_dump_time;
#endif /* GEN_TRACE */

  fprintf(stderr, " ***************************************************************************\n");
  fprintf(stderr, " Dynamic Feedback %s\n",
          (_dynamic_feedback_on) ? "ON" : "OFF");

#if defined(GEN_TRACE)
  fprintf(stderr, " Trace output GENERATED. Dump time removed from time\n");
#endif /* GEN_TRACE */

  fprintf(stderr, " Sequential Time = %11.6f sec,   \
     Doall Invocations   = %9d \n", alltime - partime, num_doalls );
#if defined(SEQUENTIAL)
   {
     double avg_task_time = safe_divide(partime, num_doalls);
     
  fprintf(stderr, " Parallel Time   = %11.6f sec,   \
     Barrier Invocations = %9d \n", partime, num_barriers );
  fprintf(stderr, " Avg Task Time   = %11.6f sec,   \
     Sync Neighbors      = %9d \n",
     avg_task_time, num_sync_neighbors );
  fprintf(stderr, " Coverage        = %9.2f %%,       \
     Reductions          = %9d \n",
    safe_divide( partime * 100.0, alltime), num_reductions );
   }
#else
  fprintf(stderr, " Parallel Time   = %11.6f sec\n", partime);
  fprintf(stderr, " Avg Task Time   = %11.6f sec\n",
          safe_divide(partime, num_doalls));
  fprintf(stderr, " Coverage        = %9.2f %%\n", 
	  safe_divide(partime * 100.0, alltime));
#endif /* SEQUENTIAL */
  fprintf(stderr, " ***************************************************************************\n");

#if defined(ALL_STATS)
    {
      int func_num;

      fprintf(stderr, "\n ***************************************************************************\n");
      for (func_num = 0; task_func_table[func_num] != NULL; ++func_num)
        {
          char *current_name = task_name_table[func_num];
          current_name = ((current_name == NULL) ? "??" : current_name);
          if (*current_name == '&')
              ++current_name;
          fprintf(stderr, " ``%s'' Time = %11.6f sec\n", current_name,
                  stat_time_table[func_num]);
          fprintf(stderr, " ``%s'' Time/iter = %11.6f sec\n", current_name,
                  safe_divide(stat_time_table[func_num],  
			      stat_iters_table[func_num] ));
        }
      fprintf(stderr, " ***************************************************************************\n");

      fprintf(stderr, "\n ***************************************************************************\n");
      for (func_num = 0; task_func_table[func_num] != NULL; ++func_num)
        {
          char *current_name = task_name_table[func_num];
          current_name = ((current_name == NULL) ? "??" : current_name);
          if (*current_name == '&')
              ++current_name;
          fprintf(stderr, " ``%s'' Doalls = %d\n", current_name,
                  stat_doalls_table[func_num]);
        }
      fprintf(stderr, " ***************************************************************************\n");

#if defined(SEQUENTIAL)
      fprintf(stderr, "\n ***************************************************************************\n");
      for (func_num = 0; task_func_table[func_num] != NULL; ++func_num)
        {
          char *current_name = task_name_table[func_num];
          current_name = ((current_name == NULL) ? "??" : current_name);
          if (*current_name == '&')
              ++current_name;
          fprintf(stderr, " ``%s'' Barriers = %d\n", current_name,
                  stat_barriers_table[func_num]);
        }
      fprintf(stderr, " ***************************************************************************\n");

      fprintf(stderr, "\n ***************************************************************************\n");
      for (func_num = 0; task_func_table[func_num] != NULL; ++func_num)
        {
          char *current_name = task_name_table[func_num];
          current_name = ((current_name == NULL) ? "??" : current_name);
          if (*current_name == '&')
              ++current_name;
          fprintf(stderr, " ``%s'' Sync Neighbors = %d\n", current_name,
                  stat_sync_neighbors_table[func_num]);
        }
      fprintf(stderr, " ***************************************************************************\n");

      fprintf(stderr, "\n ***************************************************************************\n");
      for (func_num = 0; task_func_table[func_num] != NULL; ++func_num)
        {
          char *current_name = task_name_table[func_num];
          current_name = ((current_name == NULL) ? "??" : current_name);
          if (*current_name == '&')
              ++current_name;
          fprintf(stderr, " ``%s'' Locks = %d\n", current_name,
                  stat_locks_table[func_num]);
        }
      fprintf(stderr, " ***************************************************************************\n");

      fprintf(stderr, "\n ***************************************************************************\n");
      for (func_num = 0; task_func_table[func_num] != NULL; ++func_num)
        {
          char *current_name = task_name_table[func_num];
          current_name = ((current_name == NULL) ? "??" : current_name);
          if (*current_name == '&')
              ++current_name;
          fprintf(stderr, " ``%s'' Reductions = %d\n", current_name,
                  stat_reductions_table[func_num]);
        }
      fprintf(stderr, " ***************************************************************************\n");
#endif /* SEQUENTIAL */
    }
#endif /* ALL_STATS */
#endif /* STATS */
}


/*----------------------------------------------------------------------*/
/* suif_reset_stats() - reset statistics */

void suif_reset_stats(void)
{
#if defined(STATS)
    SUIF_START_TIMER(alltime_hi_res, alltime_low_res);
    num_doalls = 0;
#if defined(SEQUENTIAL)
    num_barriers = 0;
    num_sync_neighbors = 0;
    num_locks = 0;
    num_reductions = 0 ;
#endif /* SEQUENTIAL */
#endif /* STATS */
}




/*----------------------------------------------------------------------*/
/* serial execution */

/* suif_serial() - start parallel region, run only on master */

void suif_serial(task_f f)
{
  (*_suif_aligned_doall_level)++;

  if ((*_suif_aligned_doall_level) > 1) /* only 1 level of parallelism */
    (*f)(-1);

  else
  {
#if defined(STATS) || defined(FEEDBACK) || defined(GEN_TRACE)
      {
#if defined(ALL_STATS) || defined(FEEDBACK)
      int func_num = 0;
#endif
#if defined(FEEDBACK)
      int old_nprocs = *_suif_aligned_my_nprocs;
#endif
#if defined(STATS) || defined(GEN_TRACE)
      SUIF_HI_RES_TIMER_DATA_TYPE partime_hi_res;
      double partime_low_res;
      double diff_time;

      SUIF_START_TIMER(partime_hi_res, partime_low_res);
#endif
#if defined(STATS)
      num_doalls++;
#endif
#if defined(ALL_STATS) || defined(FEEDBACK)
      if (task_func_table == NULL)
        {
          table_size = 20;
          task_func_table = (void **)(malloc(table_size * sizeof(void *)));
          task_name_table = (char **)(malloc(table_size * sizeof(char *)));
#if defined(ALL_STATS)
          stat_time_table = (double *)(malloc(table_size * sizeof(double)));
          stat_doalls_table = (int *)(malloc(table_size * sizeof(int)));
          stat_iters_table = (double *)(malloc(table_size * sizeof(double)));
#if defined(SEQUENTIAL)
          stat_barriers_table = (int *)(malloc(table_size * sizeof(int)));
          stat_sync_neighbors_table =
                  (int *)(malloc(table_size * sizeof(int)));
          stat_locks_table = (int *)(malloc(table_size * sizeof(int)));
          stat_reductions_table = (int *)(malloc(table_size * sizeof(int)));
#endif
#endif
#if defined(FEEDBACK)
          limit_nproc_table = (int *)(malloc(table_size * sizeof(int)));
#endif
          task_func_table[0] = NULL;
        }
      while (task_func_table[func_num] != f)
        {
          if (task_func_table[func_num] == NULL)
            {
              if (func_num == table_size - 1)
                {
                  void **new_func_table;
                  char **new_name_table;
#if defined(ALL_STATS)
                  double *new_time_table;
                  int *new_doalls_table;
                  double *new_iters_table;
#if defined(SEQUENTIAL)
                  int *new_barriers_table;
                  int *new_sync_neighbors_table;
                  int *new_locks_table;
                  int *new_reductions_table;
#endif
#endif
#if defined(FEEDBACK)
                  int *new_nproc_table;
#endif
                  new_func_table =
                          (void **)(malloc(table_size * 2 * sizeof(void *)));
                  new_name_table =
                          (char **)(malloc(table_size * 2 * sizeof(char *)));
#if defined(ALL_STATS)
                  new_time_table =
                          (double *)(malloc(table_size * 2 * sizeof(double)));
                  new_doalls_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
                  new_iters_table =
                          (double *)(malloc(table_size * 2 * sizeof(double)));
#if defined(SEQUENTIAL)
                  new_barriers_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
                  new_sync_neighbors_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
                  new_locks_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
                  new_reductions_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
#endif
#endif
#if defined(FEEDBACK)
                  new_nproc_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
#endif
                  memcpy(new_func_table, task_func_table,
                         table_size * sizeof(void *));
                  memcpy(new_name_table, task_name_table,
                         table_size * sizeof(char *));
#if defined(ALL_STATS)
                  memcpy(new_time_table, stat_time_table,
                         table_size * sizeof(double));
                  memcpy(new_doalls_table, stat_doalls_table,
                         table_size * sizeof(int));
                  memcpy(new_iters_table, stat_iters_table,
                         table_size * sizeof(double));
#if defined(SEQUENTIAL)
                  memcpy(new_barriers_table, stat_barriers_table,
                         table_size * sizeof(int));
                  memcpy(new_sync_neighbors_table, stat_sync_neighbors_table,
                         table_size * sizeof(int));
                  memcpy(new_locks_table, stat_locks_table,
                         table_size * sizeof(int));
                  memcpy(new_reductions_table, stat_reductions_table,
                         table_size * sizeof(int));
#endif
#endif
#if defined(FEEDBACK)
                  memcpy(new_nproc_table, limit_nproc_table,
                         table_size * sizeof(int));
#endif
                  free(task_func_table);
                  free(task_name_table);
#if defined(ALL_STATS)
                  free(stat_time_table);
                  free(stat_doalls_table);
                  free(stat_iters_table);
#if defined(SEQUENTIAL)
                  free(stat_barriers_table);
                  free(stat_sync_neighbors_table);
                  free(stat_locks_table);
                  free(stat_reductions_table);
#endif
#endif
#if defined(FEEDBACK)
                  free(limit_nproc_table);
#endif
                  task_func_table = new_func_table;
                  task_name_table = new_name_table;
#if defined(ALL_STATS)
                  stat_time_table = new_time_table;
                  stat_doalls_table = new_doalls_table;
                  stat_iters_table = new_iters_table;
#if defined(SEQUENTIAL)
                  stat_barriers_table = new_barriers_table;
                  stat_sync_neighbors_table = new_sync_neighbors_table;
                  stat_locks_table = new_locks_table;
                  stat_reductions_table = new_reductions_table;
#endif
#endif
#if defined(FEEDBACK)
                  limit_nproc_table = new_nproc_table;
#endif
                  table_size *= 2;
                }
              task_func_table[func_num] = f;
              task_name_table[func_num] = current_task_name;
#if defined(ALL_STATS)
              stat_time_table[func_num] = 0.0;
              stat_doalls_table[func_num] = 0;
              stat_iters_table[func_num] = 0.0;
#if defined(SEQUENTIAL)
              stat_barriers_table[func_num] = 0;
              stat_sync_neighbors_table[func_num] = 0;
              stat_locks_table[func_num] = 0;
              stat_reductions_table[func_num] = 0;
#endif
#endif
#if defined(FEEDBACK)
              limit_nproc_table[func_num] = 0;
                {
                  unsigned long entry_num;

                  entry_num = 0;
                  while (feedback_name_table[entry_num] != NULL)
                    {
                      if (strcmp(feedback_name_table[entry_num],
                                 current_task_name) == 0)
                        {
                          limit_nproc_table[func_num] =
                                  feedback_nproc_limit_table[entry_num];
                          break;
                        }
                      ++entry_num;
                    }
                }
#endif
              task_func_table[func_num + 1] = NULL;
              break;
            }
          ++func_num;
        }
    current_task_num = func_num;
#if defined(ALL_STATS)
    stat_doalls_table[current_task_num]++;
#endif
#endif
#endif

#if defined(FEEDBACK)
    if ((limit_nproc_table[current_task_num] != 0) &&
        (limit_nproc_table[current_task_num] < *_suif_aligned_my_nprocs))
      {
        *_suif_aligned_my_nprocs = limit_nproc_table[current_task_num];
      }
#endif

    (*f)(0);

#if defined(SEQUENTIAL) && defined(STATS)
    num_barriers++;
#if defined(ALL_STATS)
    stat_barriers_table[current_task_num]++;
#endif
#endif

#if defined(STATS) || defined(GEN_TRACE)
    SUIF_END_TIMER(diff_time, partime_hi_res, partime_low_res);

#  if defined(GEN_TRACE)
      gen_trace(current_task_num, current_task_num_iters, diff_time);
#  endif
#endif

#if defined(STATS)
    /* time doesn't count as parallel if we have a feedback
     * table where the iteration count should shut it off
     */
    if ((!_dynamic_feedback_on) ||
	((suif_dynamic_feedback_table[current_task_num].iter_threshhold <=
	  current_task_num_iters) &&
	 (suif_dynamic_feedback_table[current_task_num].max_parallelism != 1)))
    {
      partime = partime + diff_time;
    }
#endif
#if defined(ALL_STATS)
      stat_time_table[current_task_num] += diff_time;
      stat_iters_table[current_task_num] += current_task_num_iters;
#endif
#if defined(FEEDBACK)
      *_suif_aligned_my_nprocs = old_nprocs;
#endif
#if defined(STATS) || defined(FEEDBACK)
      }
#endif

    if (_main_stop)
      exit(0);          /* some worker encountered STOP, stop also */
  }

  (*_suif_aligned_doall_level)--;
}


/*----------------------------------------------------------------------*/
/* parallel execution */

/* suif_doall() - start parallel region */

void suif_doall(task_f f)
{
  (*_suif_aligned_doall_level)++;

  if ((*_suif_aligned_doall_level) > 1) /* only 1 level of parallelism */
    (*f)(-1);

  else
  {
#if defined(STATS) || defined(FEEDBACK) || defined(GEN_TRACE)
      {
#if defined(ALL_STATS) || defined(FEEDBACK)
      int func_num = 0;
#endif
#if defined(FEEDBACK)
      int old_nprocs = *_suif_aligned_my_nprocs;
#endif
#if defined(STATS) || defined(GEN_TRACE)
      SUIF_HI_RES_TIMER_DATA_TYPE partime_hi_res;
      double partime_low_res;
      double diff_time;

      SUIF_START_TIMER(partime_hi_res, partime_low_res);
#endif
#if defined(STATS)
      num_doalls++;
#endif
#if defined(ALL_STATS) || defined(FEEDBACK)
      if (task_func_table == NULL)
        {
          table_size = 20;
          task_func_table = (void **)(malloc(table_size * sizeof(void *)));
          task_name_table = (char **)(malloc(table_size * sizeof(char *)));
#if defined(ALL_STATS)
          stat_time_table = (double *)(malloc(table_size * sizeof(double)));
          stat_doalls_table = (int *)(malloc(table_size * sizeof(int)));
          stat_iters_table = (double *)(malloc(table_size * sizeof(double)));
#if defined(SEQUENTIAL)
          stat_barriers_table = (int *)(malloc(table_size * sizeof(int)));
          stat_sync_neighbors_table =
                  (int *)(malloc(table_size * sizeof(int)));
          stat_locks_table = (int *)(malloc(table_size * sizeof(int)));
          stat_reductions_table = (int *)(malloc(table_size * sizeof(int)));
#endif
#endif
#if defined(FEEDBACK)
          limit_nproc_table = (int *)(malloc(table_size * sizeof(int)));
#endif
          task_func_table[0] = NULL;
        }
      while (task_func_table[func_num] != f)
        {
          if (task_func_table[func_num] == NULL)
            {
              if (func_num == table_size - 1)
                {
                  void **new_func_table;
                  char **new_name_table;
#if defined(ALL_STATS)
                  double *new_time_table;
                  int *new_doalls_table;
                  double *new_iters_table;
#if defined(SEQUENTIAL)
                  int *new_barriers_table;
                  int *new_sync_neighbors_table;
                  int *new_locks_table;
                  int *new_reductions_table;
#endif
#endif
#if defined(FEEDBACK)
                  int *new_nproc_table;
#endif
                  new_func_table =
                          (void **)(malloc(table_size * 2 * sizeof(void *)));
                  new_name_table =
                          (char **)(malloc(table_size * 2 * sizeof(char *)));
#if defined(ALL_STATS)
                  new_time_table =
                          (double *)(malloc(table_size * 2 * sizeof(double)));
                  new_doalls_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
                  new_iters_table =
                          (double *)(malloc(table_size * 2 * sizeof(double)));
#if defined(SEQUENTIAL)
                  new_barriers_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
                  new_sync_neighbors_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
                  new_locks_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
                  new_reductions_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
#endif
#endif
#if defined(FEEDBACK)
                  new_nproc_table =
                          (int *)(malloc(table_size * 2 * sizeof(int)));
#endif
                  memcpy(new_func_table, task_func_table,
                         table_size * sizeof(void *));
                  memcpy(new_name_table, task_name_table,
                         table_size * sizeof(char *));
#if defined(ALL_STATS)
                  memcpy(new_time_table, stat_time_table,
                         table_size * sizeof(double));
                  memcpy(new_doalls_table, stat_doalls_table,
                         table_size * sizeof(int));
                  memcpy(new_iters_table, stat_iters_table,
                         table_size * sizeof(double));
#if defined(SEQUENTIAL)
                  memcpy(new_barriers_table, stat_barriers_table,
                         table_size * sizeof(int));
                  memcpy(new_sync_neighbors_table, stat_sync_neighbors_table,
                         table_size * sizeof(int));
                  memcpy(new_locks_table, stat_locks_table,
                         table_size * sizeof(int));
                  memcpy(new_reductions_table, stat_reductions_table,
                         table_size * sizeof(int));
#endif
#endif
#if defined(FEEDBACK)
                  memcpy(new_nproc_table, limit_nproc_table,
                         table_size * sizeof(int));
#endif
                  free(task_func_table);
                  free(task_name_table);
#if defined(ALL_STATS)
                  free(stat_time_table);
                  free(stat_doalls_table);
                  free(stat_iters_table);
#if defined(SEQUENTIAL)
                  free(stat_barriers_table);
                  free(stat_sync_neighbors_table);
                  free(stat_locks_table);
                  free(stat_reductions_table);
#endif
#endif
#if defined(FEEDBACK)
                  free(limit_nproc_table);
#endif
                  task_func_table = new_func_table;
                  task_name_table = new_name_table;
#if defined(ALL_STATS)
                  stat_time_table = new_time_table;
                  stat_doalls_table = new_doalls_table;
                  stat_iters_table = new_iters_table;
#if defined(SEQUENTIAL)
                  stat_barriers_table = new_barriers_table;
                  stat_sync_neighbors_table = new_sync_neighbors_table;
                  stat_locks_table = new_locks_table;
                  stat_reductions_table = new_reductions_table;
#endif
#endif
#if defined(FEEDBACK)
                  limit_nproc_table = new_nproc_table;
#endif
                  table_size *= 2;
                }
              task_func_table[func_num] = f;
              task_name_table[func_num] = current_task_name;
#if defined(ALL_STATS)
              stat_time_table[func_num] = 0.0;
              stat_doalls_table[func_num] = 0;
              stat_iters_table[func_num] = 0.0;
#if defined(SEQUENTIAL)
              stat_barriers_table[func_num] = 0;
              stat_sync_neighbors_table[func_num] = 0;
              stat_locks_table[func_num] = 0;
              stat_reductions_table[func_num] = 0;
#endif
#endif
#if defined(FEEDBACK)
              limit_nproc_table[func_num] = 0;
                {
                  unsigned long entry_num;

                  entry_num = 0;
                  while (feedback_name_table[entry_num] != NULL)
                    {
                      if (strcmp(feedback_name_table[entry_num],
                                 current_task_name) == 0)
                        {
                          limit_nproc_table[func_num] =
                                  feedback_nproc_limit_table[entry_num];
                          break;
                        }
                      ++entry_num;
                    }
                }
#endif
              task_func_table[func_num + 1] = NULL;
              break;
            }
          ++func_num;
        }
    current_task_num = func_num;
#if defined(ALL_STATS)
    stat_doalls_table[current_task_num]++;
#endif
#endif
#endif

#if defined(FEEDBACK)
    if ((limit_nproc_table[current_task_num] != 0) &&
        (limit_nproc_table[current_task_num] < *_suif_aligned_my_nprocs))
      {
        *_suif_aligned_my_nprocs = limit_nproc_table[current_task_num];
      }
#endif

    MSlaveBarrier_Release(&_suif_aligned_vars->barrier);

    (*f)(0);

#if defined(SEQUENTIAL) && defined(STATS)
    num_barriers++;
#if defined(ALL_STATS)
    stat_barriers_table[current_task_num]++;
#endif
#endif

    MSlaveBarrier_Wait(&_suif_aligned_vars->barrier,
		       _suif_aligned_vars->_nproc);

#if defined(STATS) || defined(GEN_TRACE)
      SUIF_END_TIMER(diff_time, partime_hi_res, partime_low_res);

#  if defined(GEN_TRACE)
      gen_trace(current_task_num, current_task_num_iters, diff_time);
#  endif
#endif

#if defined(STATS)
      /* time doesn't count as parallel if we have a feedback
       * table where the iteration count should shut it off
       */
      if ((!_dynamic_feedback_on) ||
          ((suif_dynamic_feedback_table[current_task_num].iter_threshhold <=
            current_task_num_iters) &&
           (suif_dynamic_feedback_table[current_task_num].max_parallelism !=
            1))) {
        partime = partime + diff_time;
      }
#if defined(ALL_STATS)
      stat_time_table[current_task_num] += diff_time;
      stat_iters_table[current_task_num] += current_task_num_iters;
#endif
#endif
#if defined(FEEDBACK)
      *_suif_aligned_my_nprocs = old_nprocs;
#endif
#if defined(STATS) || defined(FEEDBACK)
      }
#endif

    if (_main_stop)
      exit(0);          /* some worker encountered STOP, stop also */
  }

  (*_suif_aligned_doall_level)--;
}


void suif_limited_doall(task_f f, int nproc_limit)
{
    int old_nprocs = *_suif_aligned_my_nprocs;
    if (nproc_limit < old_nprocs)
        *_suif_aligned_my_nprocs = nproc_limit;
    suif_doall(f);
    if (nproc_limit < old_nprocs)
        *_suif_aligned_my_nprocs = old_nprocs;
}

void suif_named_doall(task_f f, char *task_func_name,
		      int task_func_num, int num_iters,
		      int is_iters_const)
{
    int old_nprocs;
    int new_nprocs;

#if (defined(STATS) && defined(ALL_STATS)) || defined(GEN_TRACE)
    current_task_name = task_func_name;
    current_task_iters_const = is_iters_const;
    current_task_num_iters = num_iters;
#endif
    current_task_num = task_func_num;
    old_nprocs = *_suif_aligned_my_nprocs;
    new_nprocs = suif_dynamic_feedback_get_nprocs(task_func_num,
                                                  num_iters, old_nprocs);
    if (new_nprocs != old_nprocs) {
      *_suif_aligned_my_nprocs = new_nprocs;
      setnp_();
    }

    if (new_nprocs > 1) {
      suif_doall(f);
    } else {
      suif_serial(f);
    }

    if (new_nprocs != old_nprocs) {
      *_suif_aligned_my_nprocs = old_nprocs;
    }
}


/*----------------------------------------------------------------------*/
/* suif_worker() - worker threads spin until work provided via suif_doall() */

static void suif_worker(void)
{
  int myid;            /* id of worker thread */

  myid = (THREADS_EQUAL(_master_pid, GETUNIQUEID)) ? 0 : FINC(_sysfop) + 1;
  INITID(myid)

  PROCASSIGN(((myid + _suif_aligned_vars->_proc_start) %
	      _suif_aligned_vars->_maxproc))

  suif_counter_init(myid, 0);

  if (myid == 0)
  {

#if defined(SEQUENTIAL) && defined(STATS)
  num_barriers++;
#if defined(ALL_STATS)
  stat_barriers_table[current_task_num]++;
#endif
#endif

     MSlaveBarrier_Wait(&_suif_aligned_vars->barrier,
			_suif_aligned_vars->_nproc);
     _suif_start(_argc, _argv, _envp);
     *_suif_aligned_task_f = 0;

     MSlaveBarrier_Release(&_suif_aligned_vars->barrier);
  }
  else
  {
    MSlaveBarrier_SlaveFirst(&_suif_aligned_vars->barrier,myid);
    while(1) {

      if (!(*_suif_aligned_task_f)) {
	return;
      } else {
	(**_suif_aligned_task_f)(myid);
      }

      MSlaveBarrier_SlaveEnter(&_suif_aligned_vars->barrier,myid);
    }
  }
}

/*----------------------------------------------------------------------*/
/* suif_exit_log() - Log exit for other threads, so they don't keep waiting */
/*                 Called when Fortran STOP statement is encountered      */

void suif_exit_log(void)
{
  if ((*_suif_aligned_doall_level) > 0) {    /* inside parallel region */
    MSlaveBarrier_Wait(&_suif_aligned_vars->barrier,
		       _suif_aligned_vars->_nproc);
  }

  *_suif_aligned_task_f = 0;
  MSlaveBarrier_Release(&_suif_aligned_vars->barrier);

#if defined(STATS)
  if (!suif_get_my_id())
      suif_wait_for_end();
#endif
#if defined(GEN_TRACE)
  dump_trace();
#endif
}


/*----------------------------------------------------------------------*/
/* barriers */

/* _barrier() - barrier used by runtime system */

static void _barrier(void)
{
#if defined(SEQUENTIAL) && defined(STATS)
  num_barriers++;
#if defined(ALL_STATS)
  stat_barriers_table[current_task_num]++;
#endif
#endif

  BARRIER(_sysbar1, _suif_aligned_vars->_nproc)
}

static void _barrier2(void)   /* this barrier used at end of parallel task */
{
#if defined(SEQUENTIAL) && defined(STATS)
  num_barriers++;
#if defined(ALL_STATS)
  stat_barriers_table[current_task_num]++;
#endif
#endif

  BARRIER(_sysbar2, _suif_aligned_vars->_nproc)
}

/*----------------------------------------------------------------------*/
/* suif_global_barrier() - block until all threads enter barrier */

void suif_global_barrier(int id)
{
#if defined(SEQUENTIAL) && defined(STATS)
  num_barriers++;
#if defined(ALL_STATS)
  stat_barriers_table[current_task_num]++;
#endif
#endif

  switch (id % 8) {
    case 0:
      BARRIER(_bar_global0, _suif_aligned_vars->_nproc)
      break;
    case 1:
      BARRIER(_bar_global1, _suif_aligned_vars->_nproc)
      break;
    case 2:
      BARRIER(_bar_global2, _suif_aligned_vars->_nproc)
      break;
    case 3:
      BARRIER(_bar_global3, _suif_aligned_vars->_nproc)
      break;
    case 4:
      BARRIER(_bar_global4, _suif_aligned_vars->_nproc)
      break;
    case 5:
      BARRIER(_bar_global5, _suif_aligned_vars->_nproc)
      break;
    case 6:
      BARRIER(_bar_global6, _suif_aligned_vars->_nproc)
      break;
    case 7:
      BARRIER(_bar_global7, _suif_aligned_vars->_nproc)
      break;
  }
}

void glbar_(int *id)
{
    suif_global_barrier(*id);
}


/*----------------------------------------------------------------------*/
/* suif_barrier() - block until "nproc" processor enter barrier "id" */

void suif_barrier(int id, int nproc)
{
#if defined(SEQUENTIAL) && defined(STATS)
  num_barriers++;
#if defined(ALL_STATS)
  stat_barriers_table[current_task_num]++;
#endif
#endif

  switch (id)   /* block until nproc threads reach barrier */
  {
    case 1: BARRIER(_bar1, nproc) break;
    case 2: BARRIER(_bar2, nproc) break;
    case 3: BARRIER(_bar3, nproc) break;
    case 4: BARRIER(_bar4, nproc) break;
    case 5: BARRIER(_bar5, nproc) break;
    case 6: BARRIER(_bar6, nproc) break;
    case 7: BARRIER(_bar7, nproc) break;
    case 8: BARRIER(_bar8, nproc) break;
    default: fprintf(stderr, "Barrier[%d] not supported\n", id); break;
  }
}


/*----------------------------------------------------------------------*/
/* suif_sync_neighbor() - block until neighbor threads enter barrier */

void suif_sync_neighbor(int id)
{
#if defined(SEQUENTIAL) && defined(STATS)
  num_sync_neighbors++;
#if defined(ALL_STATS)
  stat_sync_neighbors_table[current_task_num]++;
#endif
#endif

  suif_global_barrier(id);  /* just use normal barrier for now */
}

void sync_(int *id)
{
    suif_sync_neighbor(*id);
}



/*----------------------------------------------------------------------*/
/* locks */

/* suif_lock() - block until able to acquire lock "id" */

void suif_lock(int id)
{
#if defined(SEQUENTIAL) && defined(STATS)
  num_locks++;
#if defined(ALL_STATS)
  stat_locks_table[current_task_num]++;
#endif
#endif

  switch (id)
  {
    case 1: LOCK(_lock1) break;
    case 2: LOCK(_lock2) break;
    case 3: LOCK(_lock3) break;
    case 4: LOCK(_lock4) break;
    case 5: LOCK(_lock5) break;
    case 6: LOCK(_lock6) break;
    case 7: LOCK(_lock7) break;
    case 8: LOCK(_lock8) break;
    default: fprintf(stderr, "Lock[%d] not supported\n", id); break;
  }
}

void slock_(int *id)
{
    suif_lock(*id);
}

/*----------------------------------------------------------------------*/
/* suif_reduction_lock() - block until able to acquire lock "id" */
#define MAX_RLOCK_ID	(NUM_RLOCK-1)

void suif_reduction_lock(int id)
{
#if defined(SEQUENTIAL) && defined(STATS)
  num_locks++;
#if defined(ALL_STATS)
  stat_locks_table[current_task_num]++;
#endif
#endif

  ALOCK(_lock_reduction,(id&MAX_RLOCK_ID))
}


void rlock_(int *id)
{
  suif_reduction_lock(*id);
}


/*----------------------------------------------------------------------*/
/* suif_unlock() - release lock "id" */

void suif_unlock(int id)
{
  switch (id)
  {
    case 1: UNLOCK(_lock1) break;
    case 2: UNLOCK(_lock2) break;
    case 3: UNLOCK(_lock3) break;
    case 4: UNLOCK(_lock4) break;
    case 5: UNLOCK(_lock5) break;
    case 6: UNLOCK(_lock6) break;
    case 7: UNLOCK(_lock7) break;
    case 8: UNLOCK(_lock8) break;
    default: fprintf(stderr, "Lock[%d] not supported\n", id); break;
  }
}


void sulock_(int *id)
{
    suif_unlock(*id);
}


/*----------------------------------------------------------------------*/
/* suif_reduction_unlock() - release lock "id" */

void suif_reduction_unlock(int id)
{
  AUNLOCK(_lock_reduction,(id&MAX_RLOCK_ID))
}

void rulock_(int *id)
{
  suif_reduction_unlock(*id);
}


/*----------------------------------------------------------------------*/
/* fetch & increment counters */

/* suif_finc_init() - initialize fetch&inc counter "id" to 0 */

void suif_finc_init(int id)
{
  switch (id)
  {
    case 1: FOPINIT(_fop1,0); break;
    case 2: FOPINIT(_fop2,0); break;
    case 3: FOPINIT(_fop3,0); break;
    case 4: FOPINIT(_fop4,0); break;
    case 5: FOPINIT(_fop5,0); break;
    case 6: FOPINIT(_fop6,0); break;
    case 7: FOPINIT(_fop7,0); break;
    case 8: FOPINIT(_fop8,0); break;
    default: fprintf(stderr, "Getsub[%d] not supported\n", id); break;
  }
}



/*----------------------------------------------------------------------*/
/* suif_finc() - fetch & increment counter "id" */

int suif_finc(int id)
{
  switch (id)
  {
    case 1: return FINC(_fop1);
    case 2: return FINC(_fop2);
    case 3: return FINC(_fop3);
    case 4: return FINC(_fop4);
    case 5: return FINC(_fop5);
    case 6: return FINC(_fop6);
    case 7: return FINC(_fop7);
    case 8: return FINC(_fop8);
    default: fprintf(stderr, "Getsub[%d] not supported\n", id); return -1;
  }
}



/*----------------------------------------------------------------------*/
/* counters */

/* suif_counter_init_all() - clear all counters to 0 */

void suif_counter_init_all(void)
{
  memset((void *) _mylocks, 0, sizeof(_mylocks));
}



/*----------------------------------------------------------------------*/
/* suif_counter_init_range() - zero counters from 0 to "id" for all procs */

void suif_counter_init_range(int id)
{
  int i, j;

  if ((id < 0) || (id >= DEFAULT_MAXCOUNTER))
    id = DEFAULT_MAXCOUNTER-1;

  for (i = 0; i < DEFAULT_MAXPROC; i++) {
    for (j = 0; j <= id; j++) {
      _mylocks[i][j] = 0;
    }
  }
}

void cinitr_(int *id)
{
    suif_counter_init_range(*id);
}



/*----------------------------------------------------------------------*/
/* suif_counter_init() - set "id" counter for processor "proc" to 0 */

void suif_counter_init(int proc, int id)
{
  if ((proc < 0) || (proc >= DEFAULT_MAXPROC))
    fprintf(stderr, "Counter: proc %d out of range \n", proc);
  if ((id < 0) || (id >= DEFAULT_MAXCOUNTER))
    fprintf(stderr, "Counter: id %d out of range \n", id);

  _mylocks[proc][id] = 0;
}


/*----------------------------------------------------------------------*/
/* suif_counter_incr() - increment "id" counter for processor "proc" by 1 */

void suif_counter_incr(int proc, int id)
{
  if ((proc < 0) || (proc >= DEFAULT_MAXPROC))
    fprintf(stderr, "Counter: proc %d out of range \n", proc);
  if ((id < 0) || (id >= DEFAULT_MAXCOUNTER))
    fprintf(stderr, "Counter: id %d out of range \n", id);

  MEMORY_BARRIER

  _mylocks[proc][id]++;

  MEMORY_BARRIER
}

void cincr_(int *proc, int *id)
{
    suif_counter_incr(*proc, *id);
}


/*----------------------------------------------------------------------*/
/* suif_counter_set() - set "id" counter for processor "proc" to "val" */

void suif_counter_set(int proc, int id, int val)
{
  if ((proc < 0) || (proc >= DEFAULT_MAXPROC))
    fprintf(stderr, "Counter: proc %d out of range \n", proc);
  if ((id < 0) || (id >= DEFAULT_MAXCOUNTER))
    fprintf(stderr, "Counter: id %d out of range \n", id);

  _mylocks[proc][id] = val;

  MEMORY_BARRIER
}


void cset_(int *proc, int *id, int *val)
{
    suif_counter_set(*proc, *id, *val);
}


/*----------------------------------------------------------------------*/
/* suif_counter_set_range() - set "0..id-1" counter for processors "0..proc-1" to "val" */

void suif_counter_set_range(int proc, int id, int val)
{
  int i, j;

  if ((proc < 0) || (proc >= DEFAULT_MAXPROC))
    fprintf(stderr, "Counter: proc %d out of range \n", proc);
  if ((id < 0) || (id >= DEFAULT_MAXCOUNTER))
    fprintf(stderr, "Counter: id %d out of range \n", id);

  for (i = 0; i < proc; i++) {
      for (j = 0; j < id; j++) {
	  _mylocks[i][j] = val;
      }
  }
}


void csetr_(int *proc, int *id, int *val)
{
    suif_counter_set_range(*proc, *id, *val);
}


/*----------------------------------------------------------------------*/
/* suif_counter_wait() - block until counter achieves value "val" or
   greater */

void suif_counter_wait(int proc, int id, int val)
{
  if ((proc < 0) || (proc >= DEFAULT_MAXPROC))
    fprintf(stderr, "Counter: proc %d out of range \n", proc);
  if ((id < 0) || (id >= DEFAULT_MAXCOUNTER))
    fprintf(stderr, "Counter: id %d out of range \n", id);

#if defined(SEQUENTIAL)
  fprintf(stderr, "Counters not supported in SEQUENTIAL mode \n");
  return;
#endif

  while (_mylocks[proc][id] < val) ;          /* spin */

  MEMORY_BARRIER
}


void cwait_(int *proc, int *id, int *val)
{
    suif_counter_wait(*proc, *id, *val);
}


/*----------------------------------------------------------------------*/
/* suif_speculate_begin() */

void suif_speculate_begin()
{
}


void sbegin_()
{
}


void suif_par_begin()
{
}


void pbegin_()
{
}

/*----------------------------------------------------------------------*/
/* suif_infinite_loop() */

void suif_infinite_loop()
{
    while (1);
}


/*----------------------------------------------------------------------*/
/* suif_speculate_commit() */

void suif_speculate_commit()
{
}


void commit_()
{
}


void suif_par_commit()
{
}


void pcommi_()
{
}


/*----------------------------------------------------------------------*/
/* suif_speculate_terminate() */

void suif_speculate_terminate()
{
}


void termin_()
{
}


void suif_par_terminate()
{
}


void ptermi_()
{
}


/* dummy routine for simulator */
void suif_start_packing(task_f f, char *name) { }

/*----------------------------------------------------------------------*/
/* timers */

/* suif_clock() - return user time in seconds */

static SUIF_HI_RES_TIMER_DATA_TYPE hi_res_start_time;
static double low_res_start_time;

double suif_clock(void)
{
  double t;

  SECONDCLOCK(t)
  return t;
}

double suif_hi_res_clock(void)
{
  double t;

  SUIF_HI_RES_CLOCK(t);
  return t;
}

void suif_start_timer(void)
  {
    SUIF_START_TIMER(hi_res_start_time, low_res_start_time);
  }

double suif_end_timer(void)
  {
    double result;
    SUIF_END_TIMER(result, hi_res_start_time, low_res_start_time);
    return result;
  }


/*----------------------------------------------------------------------*/
/* reductions & scans */

#if defined(SEQUENTIAL) && defined(STATS)
#if defined(ALL_STATS)
#define statistics() \
  { num_reductions++; stat_reductions_table[current_task_num]++; }
#else
#define statistics() { num_reductions++; }
#endif
#else
#define statistics()
#endif

/* reduce routines */

REDUCTION_ROUTINES(signed char,    signed_char,      sc)
REDUCTION_ROUTINES(unsigned char,  unsigned_char,    uc)
REDUCTION_ROUTINES(short,          short,            ss)
REDUCTION_ROUTINES(unsigned short, unsigned_short,   us)
REDUCTION_ROUTINES(int,            int,              si)
REDUCTION_ROUTINES(unsigned int,   unsigned_int,     ui)
REDUCTION_ROUTINES(long,           long,             sl)
REDUCTION_ROUTINES(unsigned long,  unsigned_long,    ul)
REDUCTION_ROUTINES(float,          float,            f)
REDUCTION_ROUTINES(double,         double,           d)
REDUCTION_ROUTINES(long double,    long_double,      ld)

/* Initializations */
REDUCTION_INITS(signed char,    signed_char,    sc, 0,    1,    SCHAR_MIN,
                SCHAR_MAX)
REDUCTION_INITS(unsigned char,  unsigned_char,  uc, 0,    1,    0,
                UCHAR_MAX)
REDUCTION_INITS(short,          short,          ss, 0,    1,    SHRT_MIN,
                SHRT_MAX)
REDUCTION_INITS(unsigned short, unsigned_short, us, 0,    1,    0,
                USHRT_MAX)
REDUCTION_INITS(int,            int,            si, 0,    1,    INT_MIN,
                INT_MAX)
REDUCTION_INITS(unsigned int,   unsigned_int,   ui, 0u,   1u,   0u,
                UINT_MAX)
REDUCTION_INITS(long,           long,           sl, 0l,   1l,   LONG_MIN,
                LONG_MAX)
REDUCTION_INITS(unsigned long,  unsigned_long,  ul, 0ul,  1ul,  0ul,
                ULONG_MAX)
REDUCTION_INITS(float,          float,          f,  0.0f, 1.0f, -FLT_MAX,
                FLT_MAX)
REDUCTION_INITS(double,         double,         d,  0.0,  1.0,  -DBL_MAX,
                DBL_MAX)
REDUCTION_INITS(long double,    long_double,    ld, 0.0l, 1.0l, -LDBL_MAX,
                LDBL_MAX)

INITGEN(signed char,    signed_char,       sc)
INITGEN(unsigned char,  unsigned_char,     uc)
INITGEN(short,          short,             ss)
INITGEN(unsigned short, unsigned_short,    us)
INITGEN(int,            int,               si)
INITGEN(unsigned int,   unsigned_int,      ui)
INITGEN(long,           long,              sl)
INITGEN(unsigned long,  unsigned_long,     ul)
INITGEN(float,          float,             f)
INITGEN(double,         double,            d)
INITGEN(long double,    long_double,       ld)

/* eof */
