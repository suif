/* file "runtime_test.c" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*----------------------------------------------------------------------*
 *
 *  runtime_test.c   Test SUIF run-time routines 
 *
 *----------------------------------------------------------------------*/

#include "runtime.h"
#include <stdio.h>

double mytime;

int _suif_nproc = 0;  /* compiler does not required fixed # of procs */

extern void *volatile _suif_aligned_args;
extern void (**volatile _suif_aligned_task_f)(int);

void foo(id)
  int id;
{
  int i;
  int myid = suif_get_my_id();
  int nproc = suif_num_total_ids();
  double *args = (double *) _suif_aligned_args;

  printf("Proc[%d] id=%d nproc=%d time=%f \n", id, myid, nproc, *args); 

  if (mytime != *args) printf("Proc[%d] incorrect argument passed \n", id);

  /* test barriers & locks */

  suif_barrier(1,nproc);
  suif_lock(1);
  printf("Proc[%d] lock...", id);
  for (i=0; i<1000; i++) ;
  printf("unlock Proc[%d] \n", id);
  suif_unlock(1);

  /* test counters & fetch&inc counters */

  suif_barrier(2,nproc);
  if (!id) suif_finc_init(1);
  suif_counter_init(id,0);
  suif_global_barrier(1);

  if (id > 0) suif_counter_wait(id-1,0,1);
  printf("Proc[%d] forward counter=%d \n", id, suif_finc(1));
  suif_counter_incr(id,0);

  suif_barrier(1,nproc);
  if (!id) suif_finc_init(2);
  suif_counter_set(id,1,-1);
  suif_barrier(2,nproc);

  if (id < nproc-1) suif_counter_wait(id+1,1,0);
  printf("Proc[%d] backward counter=%d \n", id, suif_finc(2));
  suif_counter_incr(id,1);
}


_suif_start(argc, argv, envp)
  int argc;
  char **argv;
  char **envp;
{
  int i;
  char **ptr;
  double time;

  for (i=0, ptr=argv; *ptr; i++, ptr++)
    printf("argv[%d] %s\n", i, *ptr);

  for (i=0, ptr=envp; *ptr; i++, ptr++)
    printf("envp[%d] %s\n", i, *ptr);

  printf("----------------------------------\n");
  time = suif_clock();
  mytime = time;

  *_suif_aligned_task_f = foo;
  *((double * ) _suif_aligned_args) = time;
  suif_named_doall(foo, "foo", 0, 1, 1);

  time = suif_clock() - time;
  printf("Time = %f seconds \n", time);

  printf("----------------------------------\n");
  time = suif_clock();
  mytime = time;

  *((double * ) _suif_aligned_args) = time;
  suif_named_doall(foo, "foo", 0, 1, 1);

  time = suif_clock() - time;
  printf("Time = %f seconds \n", time);

  printf("Test finished \n");
}


