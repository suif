/* file "fastsync_asm_sgi.s" */
	
/* *****************************************************************
 * SimOS fast synchronization routines for SGI (MIPS2)
 * *****************************************************************/

#include <asm.h>
#include <sys/regdef.h>

/*
 * These offset assumes that lock+0 and lock+4 are in the same
 * cache line while lock+128 is in a difference cache line.
 */
	
#define COUNT_OFFSET    0
#define PREFETCH_OFFSET 4
#define GEN_OFFSET    128   /* On next cache line */
#define PREFETCH_ON_LL

/*
 * Simcp0Asm_Barrier(FlashBarrier *barrier, int num_procs)
 */
LEAF(Simcp0Asm_Barrier)

	/*
	 *  Read the generation number for this barrier. This memory
         *  location will be changed when all have arrived.
         *  s0:  address of barrier
	 */
        addiu   sp, sp,-24
        sw      ra, 4(sp)
        sw      s0, 8(sp)
        sw      s1, 12(sp)
        sw      s2, 16(sp)
        move    s0,a0
        move    s1,a1  
	lw	s2, GEN_OFFSET(s0)
  loop:		
	/*
         * count = LOAD_LINKED(counter);
	 * if (count ==  2*num_proc) count = 0;
         * STORE_CONDITIONAL(counter, count);
         */
#ifdef PREFETCH_ON_LL	
	sb      zero, PREFETCH_OFFSET(s0)
#endif
    .set noreorder	
	ll	v0, COUNT_OFFSET(s0)
	addu	v0, v0, 1
	mul	a2, a1, 2
	bne	v0, a2, 1f
    nop
	move	v0, zero
1:
	move    t1, v0
	sc	v0, COUNT_OFFSET(s0)
	beq	v0, 0, loop
    nop
    .set reorder
	/*
	 * If this is the last process to arrive, release the rest.
         * if ((count == 0) || (count == num_proc)) goto release
         */
	beq     t1, 0, 3f
	beq     t1, s1, 3f

       /*
        * t2 == spin counter. Sginap at 1  million. 
        */
        li    t2, 1000000
         
       /*
        * while(my_gen == *cur_gen) continue;
        */
        .set noreorder
2:      lw      t1, GEN_OFFSET(s0)  /* volatile */
	addiu   t2, t2,-1           
        bne     s2, t1, leave       /* gen. nr increment */
        nop
#ifdef USE_SGINAP
        bne     t2,zero, 2b         /* time to nap */
        nop

       /*
        * Reset t2 (spin count) and sginap(2)
        */
nap:    li      a0,2
       	li      v0,1082
	syscall
        nop
        .set reorder
        li      t2, 100            /*  only give it 100 iterations now */
        b       2b
#else  /* NEVER NAP */
     	.set reorder
	b        2b                 /* Being obnoxious and never napping */
#endif
	/*
         * cur_gen++;  // Release all waiters
         */	
/*  release:	*/
3:	addu    s2, s2, 1
	sw      s2, GEN_OFFSET(s0)

leave:  
        lw      ra,4(sp)
        lw      s0,8(sp)
        lw      s1,12(sp)
        lw      s2,16(sp)
        addiu   sp,sp,24
   	j       ra

        
    END(Simcp0Asm_Barrier)


 

LEAF(Simcp0Asm_Lock)
	.set noreorder  
1:      ll      t0, (a0)
#ifdef USE_SGINAP
   	bnez    t0, Simcp0Asm_SlowLock
#else
        bnez    t0, 1b
#endif
	li      v0, 1
	sc	v0, (a0)
	beqz    v0, 1b
	nop
    .set reorder 
	j    ra
	END(Simcp0Asm_Lock)


/*
 *      Object:
 *              Simcp0SlowLock(int *)
 *
 *        Lock the specified lock.
 *
 *      Arguments:
 *              a0  Address of the lock.
 */
NESTED(Simcp0Asm_SlowLock, 56, $31 )
	.set noreorder
	.cpload $25
	.set reorder
	subu    $sp,56
	sw	ra, 36($sp)
	sw	a0, 40($sp)
2:      ll      t0, (a0)
   	beqz    t0, 1f
	li      a0, 1
	jal	sginap
	lw	a0, 40($sp)
	b	2b

1:	li      v0, 1
	sc	v0, (a0)
	beqz    v0, 2b

	lw	ra, 36($sp)	
	addu    $sp,56
	j    ra
	END(Simcp0Asm_SlowLock)

/*
 *      Object:
 *              Simcp0Asm_Unlock(int *)
 *
 *        Unock the specified lock.
 *
 *      Arguments:
 *              O0  Address of the lock.
 *
 */
LEAF(Simcp0Asm_Unlock)
	sw  zero, (a0)
	j ra
	END(Simcp0Asm_Unlock)

/* file llsc_sgi.s */

        .globl  Simcp0Asm_ll
        .ent    Simcp0Asm_ll
Simcp0Asm_ll:
        .frame  $sp,0,$31               # vars= 0, regs= 0/0, args= 0, extra= 0
        .mask   0x00000000,0
        .fmask  0x00000000,0
        .set    noreorder
        .cpload $25
        .set    reorder
 #APP
        ll $2,0($4)
 #NO_APP
        j       $31
        .end    Simcp0Asm_ll

        .align  2
        .globl  Simcp0Asm_sc
        .ent    Simcp0Asm_sc
Simcp0Asm_sc:
        .frame  $sp,0,$31               # vars= 0, regs= 0/0, args= 0, extra= 0
        .mask   0x00000000,0
        .fmask  0x00000000,0
        .set    noreorder
        .cpload $25
        .set    reorder
        move    $2,$4
 #APP
        sc $2,0($5)
 #NO_APP
        j       $31
        .end    Simcp0Asm_sc

