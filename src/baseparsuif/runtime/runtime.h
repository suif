/* file "runtime.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

/* #include <suif_copyright.h> */

#include <stddef.h>

/*----------------------------------------------------------------------*
 *
 *  runtime.h    Definitions for SUIF run-time routines 
 *
 *----------------------------------------------------------------------*/

#define DEFAULT_NUM_THREADS 4
#define DEFAULT_MAXCOUNTER  32
#define DEFAULT_LIMITS_ITER            0
#define DEFAULT_LIMITS_LOADBALANCE   100
#define DEFAULT_LIMITS_WORK            0
#define DEFAULT_LIMITS_BODY            0
#define DEFAULT_LIMITS_WORK_SMALL_ITER 0

#ifndef DEFAULT_MAXPROC
#define DEFAULT_MAXPROC 64 
#endif

#ifndef DEFAULT_PROC_START
#define DEFAULT_PROC_START 0
#endif

/*----------------------------------------------------------------------*/
/* declarations */

typedef void (*task_f)(int);               /* task function type */

extern int suif_get_my_id(void);           /* logical processor id */ 
extern int suif_num_total_ids(void);  	   /* number of processors */
extern void suif_global_barrier(int);      /* block until all procs arrive */
extern void suif_barrier(int,int);         /* block until nprocs arrive */
extern void suif_lock(int); 	           /* block until lock acquired */
extern void suif_unlock(int);	           /* release lock */
extern double suif_clock(void);            /* return time in seconds */
extern double suif_hi_res_clock(void);     /* return time in seconds (better */
                                           /* resolution than suif_clock(), */
                                           /* but not guaranteed not to */
                                           /* overflow) */
extern void suif_start_timer(void);        /* start accurate timer */
extern double suif_end_timer(void);        /* finish accurate timer, return */
                                           /* time in seconds */
extern void suif_wait_for_end(void);       /* clean up routine for exit() */ 

extern void suif_restore_state(int pid, int my_id);  /* called by hardware simulator to restore registers */
extern int restart(int pid);  /* called by hardware simulator to restore registers */

extern void suif_doall(task_f);     /* parallel call */
extern void suif_limited_doall(task_f, int);

extern void *suif_malloc(size_t size);
                                           /* parallel call, limited number */
                                           /* of processors */
extern void suif_named_doall(task_f, char *, int, int, int);
extern int suif_doall_level(void);         /* return level of parallelism */

extern void suif_finc_init(int);           /* set fetch&inc counter to 0 */
extern int suif_finc(int);                 /* fetch & increment counter */
extern void suif_counter_init_all(void);   /* init all counters */
extern void suif_counter_init_range(int);  /* init counters from 0 to range */
extern void suif_counter_init(int,int);    /* reset producer counter */
extern void suif_counter_incr(int,int);    /* increment counter */
extern void suif_counter_set(int,int,int); /* set counter value */
extern void suif_counter_set_range(int,int,int); /* set counter range value */
extern void suif_counter_wait(int,int,int);/* block until counter >= value */
extern void suif_exit_log(void);           /* log exit for remaining threads */

extern void suif_assign_ids(int,int*,int*);/* init vars for local ids  */
extern void suif_assign_nprocs(void);      /* initialize vars for # of procs */
extern void suif_reduction_lock(int);      /* lock for reductions */
extern void suif_reduction_unlock(int);    /* unlock for reductions */

extern int suif_check_work(int,int,double,int); /* test amout of work */
extern void suif_start_packing(task_f f, char *name);

/* Versions of the above routines callable from FORTRAN */

extern int numids_(void);                  /* suif_num_total_ids() */
extern void glbar_(int *);                 /* suif_global_barrier() */
extern void sync_(int *);                  /* suif_sync_neighbor() */
extern void slock_(int *);                 /* suif_lock() */
extern void sulock_(int *);                /* suif_unlock() */

extern int doalev_(void);                  /* suif_doall_level() */    

extern void setids_(int * ,int *, int *);  /* suif_assign_ids() */
extern void setnp_(void);                  /* suif_assign_nprocs() */
extern void cinitr_(int *);                /* suif_counter_init_range() */
extern void cincr_(int *, int *);          /* suif_counter_incr() */
extern void csetr_(int *, int *, int *);    /* suif_counter_set() */ 
extern void cset_(int *, int *, int *);    /* suif_counter_set() */ 
extern void cwait_(int *, int *, int *);   /* suif_counter_wait() */
extern void rlock_(int *);                 /* suif_reduction_lock() */
extern void rulock_(int *);                 /* suif_reduction_unlock() */
int checkw_(int *, int *, double *, int *);/* suif_check_work() */


/* reductions */
extern void reduce_sum_signed_char(signed char *g, signed char *l,
                                   size_t num_elem);
extern void reduce_product_signed_char(signed char *g, signed char *l,
                                       size_t num_elem);
extern void reduce_min_signed_char(signed char *g, signed char *l,
                                   size_t num_elem);
extern void reduce_max_signed_char(signed char *g, signed char *l,
                                   size_t num_elem);
extern void reduce_sum_unsigned_char(unsigned char *g, unsigned char *l,
                                     size_t num_elem);
extern void reduce_product_unsigned_char(unsigned char *g, unsigned char *l,
                                         size_t num_elem);
extern void reduce_min_unsigned_char(unsigned char *g, unsigned char *l,
                                     size_t num_elem);
extern void reduce_max_unsigned_char(unsigned char *g, unsigned char *l,
                                     size_t num_elem);
extern void reduce_sum_short(short *g, short *l, size_t num_elem);
extern void reduce_product_short(short *g, short *l, size_t num_elem);
extern void reduce_min_short(short *g, short *l, size_t num_elem);
extern void reduce_max_short(short *g, short *l, size_t num_elem);
extern void reduce_sum_unsigned_short(unsigned short *g, unsigned short *l,
                                      size_t num_elem);
extern void reduce_product_unsigned_short(unsigned short *g, unsigned short *l,
                                          size_t num_elem);
extern void reduce_min_unsigned_short(unsigned short *g, unsigned short *l,
                                      size_t num_elem);
extern void reduce_max_unsigned_short(unsigned short *g, unsigned short *l,
                                      size_t num_elem);
extern void reduce_sum_int(int *g, int *l, size_t num_elem);
extern void reduce_product_int(int *g, int *l, size_t num_elem);
extern void reduce_min_int(int *g, int *l, size_t num_elem);
extern void reduce_max_int(int *g, int *l, size_t num_elem);
extern void reduce_sum_unsigned_int(unsigned int *g, unsigned int *l,
                                    size_t num_elem);
extern void reduce_product_unsigned_int(unsigned int *g, unsigned int *l,
                                        size_t num_elem);
extern void reduce_min_unsigned_int(unsigned int *g, unsigned int *l,
                                    size_t num_elem);
extern void reduce_max_unsigned_int(unsigned int *g, unsigned int *l,
                                    size_t num_elem);
extern void reduce_sum_long(long *g, long *l, size_t num_elem);
extern void reduce_product_long(long *g, long *l, size_t num_elem);
extern void reduce_min_long(long *g, long *l, size_t num_elem);
extern void reduce_max_long(long *g, long *l, size_t num_elem);
extern void reduce_sum_unsigned_long(unsigned long *g, unsigned long *l,
                                     size_t num_elem);
extern void reduce_product_unsigned_long(unsigned long *g, unsigned long *l,
                                         size_t num_elem);
extern void reduce_min_unsigned_long(unsigned long *g, unsigned long *l,
                                     size_t num_elem);
extern void reduce_max_unsigned_long(unsigned long *g, unsigned long *l,
                                     size_t num_elem);
extern void reduce_sum_float(float *g, float *l, size_t num_elem);
extern void reduce_product_float(float *g, float *l, size_t num_elem);
extern void reduce_min_float(float *g, float *l, size_t num_elem);
extern void reduce_max_float(float *g, float *l, size_t num_elem);
extern void reduce_sum_double(double *g, double *l, size_t num_elem);
extern void reduce_product_double(double *g, double *l, size_t num_elem);
extern void reduce_min_double(double *g, double *l, size_t num_elem);
extern void reduce_max_double(double *g, double *l, size_t num_elem);
extern void reduce_sum_long_double(long double *g, long double *l,
                                   size_t num_elem);
extern void reduce_product_long_double(long double *g, long double *l,
                                       size_t num_elem);
extern void reduce_min_long_double(long double *g, long double *l,
                                   size_t num_elem);
extern void reduce_max_long_double(long double *g, long double *l,
                                   size_t num_elem);

extern void reduce_rotate_sum_signed_char(signed char *g, signed char *l,
                                   size_t num_elem);
extern void reduce_rotate_product_signed_char(signed char *g, signed char *l,
                                       size_t num_elem);
extern void reduce_rotate_min_signed_char(signed char *g, signed char *l,
                                   size_t num_elem);
extern void reduce_rotate_max_signed_char(signed char *g, signed char *l,
                                   size_t num_elem);
extern void reduce_rotate_sum_unsigned_char(unsigned char *g, unsigned char *l,
                                     size_t num_elem);
extern void reduce_rotate_product_unsigned_char(unsigned char *g,
                                                unsigned char *l,
                                                size_t num_elem);
extern void reduce_rotate_min_unsigned_char(unsigned char *g, unsigned char *l,
                                     size_t num_elem);
extern void reduce_rotate_max_unsigned_char(unsigned char *g, unsigned char *l,
                                     size_t num_elem);
extern void reduce_rotate_sum_short(short *g, short *l, size_t num_elem);
extern void reduce_rotate_product_short(short *g, short *l, size_t num_elem);
extern void reduce_rotate_min_short(short *g, short *l, size_t num_elem);
extern void reduce_rotate_max_short(short *g, short *l, size_t num_elem);
extern void reduce_rotate_sum_unsigned_short(unsigned short *g,
                                             unsigned short *l,
                                             size_t num_elem);
extern void reduce_rotate_product_unsigned_short(unsigned short *g,
                                                 unsigned short *l,
                                                 size_t num_elem);
extern void reduce_rotate_min_unsigned_short(unsigned short *g,
                                             unsigned short *l,
                                             size_t num_elem);
extern void reduce_rotate_max_unsigned_short(unsigned short *g,
                                             unsigned short *l,
                                             size_t num_elem);
extern void reduce_rotate_sum_int(int *g, int *l, size_t num_elem);
extern void reduce_rotate_product_int(int *g, int *l, size_t num_elem);
extern void reduce_rotate_min_int(int *g, int *l, size_t num_elem);
extern void reduce_rotate_max_int(int *g, int *l, size_t num_elem);
extern void reduce_rotate_sum_unsigned_int(unsigned int *g, unsigned int *l,
                                    size_t num_elem);
extern void reduce_rotate_product_unsigned_int(unsigned int *g,
                                               unsigned int *l,
                                               size_t num_elem);
extern void reduce_rotate_min_unsigned_int(unsigned int *g, unsigned int *l,
                                    size_t num_elem);
extern void reduce_rotate_max_unsigned_int(unsigned int *g, unsigned int *l,
                                    size_t num_elem);
extern void reduce_rotate_sum_long(long *g, long *l, size_t num_elem);
extern void reduce_rotate_product_long(long *g, long *l, size_t num_elem);
extern void reduce_rotate_min_long(long *g, long *l, size_t num_elem);
extern void reduce_rotate_max_long(long *g, long *l, size_t num_elem);
extern void reduce_rotate_sum_unsigned_long(unsigned long *g, unsigned long *l,
                                     size_t num_elem);
extern void reduce_rotate_product_unsigned_long(unsigned long *g,
                                                unsigned long *l,
                                                size_t num_elem);
extern void reduce_rotate_min_unsigned_long(unsigned long *g, unsigned long *l,
                                     size_t num_elem);
extern void reduce_rotate_max_unsigned_long(unsigned long *g, unsigned long *l,
                                     size_t num_elem);
extern void reduce_rotate_sum_float(float *g, float *l, size_t num_elem);
extern void reduce_rotate_product_float(float *g, float *l, size_t num_elem);
extern void reduce_rotate_min_float(float *g, float *l, size_t num_elem);
extern void reduce_rotate_max_float(float *g, float *l, size_t num_elem);
extern void reduce_rotate_sum_double(double *g, double *l, size_t num_elem);
extern void reduce_rotate_product_double(double *g, double *l,
                                         size_t num_elem);
extern void reduce_rotate_min_double(double *g, double *l, size_t num_elem);
extern void reduce_rotate_max_double(double *g, double *l, size_t num_elem);
extern void reduce_rotate_sum_long_double(long double *g, long double *l,
                                   size_t num_elem);
extern void reduce_rotate_product_long_double(long double *g, long double *l,
                                       size_t num_elem);
extern void reduce_rotate_min_long_double(long double *g, long double *l,
                                   size_t num_elem);
extern void reduce_rotate_max_long_double(long double *g, long double *l,
                                   size_t num_elem);

extern void init_sum_signed_char(signed char *l, size_t num_elem);
extern void init_product_signed_char(signed char *l, size_t num_elem);
extern void init_min_signed_char(signed char *l, size_t num_elem);
extern void init_max_signed_char(signed char *l, size_t num_elem);
extern void init_sum_unsigned_char(unsigned char *l, size_t num_elem);
extern void init_product_unsigned_char(unsigned char *l, size_t num_elem);
extern void init_min_unsigned_char(unsigned char *l, size_t num_elem);
extern void init_max_unsigned_char(unsigned char *l, size_t num_elem);
extern void init_sum_short(short *l, size_t num_elem);
extern void init_product_short(short *l, size_t num_elem);
extern void init_min_short(short *l, size_t num_elem);
extern void init_max_short(short *l, size_t num_elem);
extern void init_sum_unsigned_short(unsigned short *l, size_t num_elem);
extern void init_product_unsigned_short(unsigned short *l, size_t num_elem);
extern void init_min_unsigned_short(unsigned short *l, size_t num_elem);
extern void init_max_unsigned_short(unsigned short *l, size_t num_elem);
extern void init_sum_int(int *l, size_t num_elem);
extern void init_product_int(int *l, size_t num_elem);
extern void init_min_int(int *l, size_t num_elem);
extern void init_max_int(int *l, size_t num_elem);
extern void init_sum_unsigned_int(unsigned int *l, size_t num_elem);
extern void init_product_unsigned_int(unsigned int *l, size_t num_elem);
extern void init_min_unsigned_int(unsigned int *l, size_t num_elem);
extern void init_max_unsigned_int(unsigned int *l, size_t num_elem);
extern void init_sum_long(long *l, size_t num_elem);
extern void init_product_long(long *l, size_t num_elem);
extern void init_min_long(long *l, size_t num_elem);
extern void init_max_long(long *l, size_t num_elem);
extern void init_sum_unsigned_long(unsigned long *l, size_t num_elem);
extern void init_product_unsigned_long(unsigned long *l, size_t num_elem);
extern void init_min_unsigned_long(unsigned long *l, size_t num_elem);
extern void init_max_unsigned_long(unsigned long *l, size_t num_elem);
extern void init_sum_float(float *l, size_t num_elem);
extern void init_product_float(float *l, size_t num_elem);
extern void init_min_float(float *l, size_t num_elem);
extern void init_max_float(float *l, size_t num_elem);
extern void init_sum_double(double *l, size_t num_elem);
extern void init_product_double(double *l, size_t num_elem);
extern void init_min_double(double *l, size_t num_elem);
extern void init_max_double(double *l, size_t num_elem);
extern void init_sum_long_double(long double *l, size_t num_elem);
extern void init_product_long_double(long double *l, size_t num_elem);
extern void init_min_long_double(long double *l, size_t num_elem);
extern void init_max_long_double(long double *l, size_t num_elem);

extern void init_gen_signed_char(signed char *l, signed char *g,
                                 size_t num_elem);
extern void init_gen_unsigned_char(unsigned char *l, unsigned char *g,
                                   size_t num_elem);
extern void init_gen_short(short *l, short *g, size_t num_elem);
extern void init_gen_unsigned_short(unsigned short *l, unsigned short *g,
                                    size_t num_elem);
extern void init_gen_int(int *l, int *g, size_t num_elem);
extern void init_gen_unsigned_int(unsigned int *l, unsigned int *g,
                                  size_t num_elem);
extern void init_gen_long(long *l, long *g, size_t num_elem);
extern void init_gen_unsigned_long(unsigned long *l, unsigned long *g,
                                   size_t num_elem);
extern void init_gen_float(float *l, float *g, size_t num_elem);
extern void init_gen_double(double *l, double *g, size_t num_elem);
extern void init_gen_long_double(long double *l, long double *g,
                                 size_t num_elem);

/* here are the shortened names */
/* They really do make sense in a Fortran sort of way.
   Here's the conversion:
       rcABCC
   where "rcA" is:

   reduce_rotate_   -> rcr_
   init_            -> rci_
   reduce_          -> rce_

   where "B" is:

   _sum_            -> s_
   _product_        -> p_
   _min_            -> n_
   _max_            -> x_
   _gen_            -> g_

   where "CC" is:

   _unsigned_char(  -> uc_(
   _signed_char(    -> sc_(
   _unsigned_short( -> us_(
   _short(          -> ss_(
   _unsigned_int(   -> ui_(
   _int(            -> si_(
   _unsigned_long(  -> ul_(
   _long(           -> sl_(
   _long_double(    -> ld_(
   _double(         -> d_(
   _float(          -> f_(

   */
/* All parameters need to be pass by reference */

extern void rcessc_(signed char *g, signed char *l,
                                   size_t *num_elem);
extern void rcepsc_(signed char *g, signed char *l,
                                       size_t *num_elem);
extern void rcensc_(signed char *g, signed char *l,
                                   size_t *num_elem);
extern void rcexsc_(signed char *g, signed char *l,
                                   size_t *num_elem);
extern void rcesuc_(unsigned char *g, unsigned char *l,
                                     size_t *num_elem);
extern void rcepuc_(unsigned char *g, unsigned char *l,
                                         size_t *num_elem);
extern void rcenuc_(unsigned char *g, unsigned char *l,
                                     size_t *num_elem);
extern void rcexuc_(unsigned char *g, unsigned char *l,
                                     size_t *num_elem);
extern void rcesss_(short *g, short *l, size_t *num_elem);
extern void rcepss_(short *g, short *l, size_t *num_elem);
extern void rcenss_(short *g, short *l, size_t *num_elem);
extern void rcexss_(short *g, short *l, size_t *num_elem);
extern void rcesus_(unsigned short *g, unsigned short *l,
                                      size_t *num_elem);
extern void rcepus_(unsigned short *g, unsigned short *l,
                                          size_t *num_elem);
extern void rcenus_(unsigned short *g, unsigned short *l,
                                      size_t *num_elem);
extern void rcexus_(unsigned short *g, unsigned short *l,
                                      size_t *num_elem);
extern void rcessi_(int *g, int *l, size_t *num_elem);
extern void rcepsi_(int *g, int *l, size_t *num_elem);
extern void rcensi_(int *g, int *l, size_t *num_elem);
extern void rcexsi_(int *g, int *l, size_t *num_elem);
extern void rcesui_(unsigned int *g, unsigned int *l,
                                    size_t *num_elem);
extern void rcepui_(unsigned int *g, unsigned int *l,
                                        size_t *num_elem);
extern void rcenui_(unsigned int *g, unsigned int *l,
                                    size_t *num_elem);
extern void rcexui_(unsigned int *g, unsigned int *l,
                                    size_t *num_elem);
extern void rcessl_(long *g, long *l, size_t *num_elem);
extern void rcepsl_(long *g, long *l, size_t *num_elem);
extern void rcensl_(long *g, long *l, size_t *num_elem);
extern void rcexsl_(long *g, long *l, size_t *num_elem);
extern void rcesul_(unsigned long *g, unsigned long *l,
                                     size_t *num_elem);
extern void rcepul_(unsigned long *g, unsigned long *l,
                                         size_t *num_elem);
extern void rcenul_(unsigned long *g, unsigned long *l,
                                     size_t *num_elem);
extern void rcexul_(unsigned long *g, unsigned long *l,
                                     size_t *num_elem);
extern void rcesf_(float *g, float *l, size_t *num_elem);
extern void rcepf_(float *g, float *l, size_t *num_elem);
extern void rcenf_(float *g, float *l, size_t *num_elem);
extern void rcexf_(float *g, float *l, size_t *num_elem);
extern void rcesd_(double *g, double *l, size_t *num_elem);
extern void rcepd_(double *g, double *l, size_t *num_elem);
extern void rcend_(double *g, double *l, size_t *num_elem);
extern void rcexd_(double *g, double *l, size_t *num_elem);
extern void rcesld_(long double *g, long double *l,
                                   size_t *num_elem);
extern void rcepld_(long double *g, long double *l,
                                       size_t *num_elem);
extern void rcenld_(long double *g, long double *l,
                                   size_t *num_elem);
extern void rcexld_(long double *g, long double *l,
                                   size_t *num_elem);

extern void rcrssc_(signed char *g, signed char *l,
                                   size_t *num_elem);
extern void rcrpsc_(signed char *g, signed char *l,
                                       size_t *num_elem);
extern void rcrnsc_(signed char *g, signed char *l,
                                   size_t *num_elem);
extern void rcrxsc_(signed char *g, signed char *l,
                                   size_t *num_elem);
extern void rcrsuc_(unsigned char *g, unsigned char *l,
                                     size_t *num_elem);
extern void rcrpuc_(unsigned char *g,
                                                unsigned char *l,
                                                size_t *num_elem);
extern void rcrnuc_(unsigned char *g, unsigned char *l,
                                     size_t *num_elem);
extern void rcrxuc_(unsigned char *g, unsigned char *l,
                                     size_t *num_elem);
extern void rcrsss_(short *g, short *l, size_t *num_elem);
extern void rcrpss_(short *g, short *l, size_t *num_elem);
extern void rcrnss_(short *g, short *l, size_t *num_elem);
extern void rcrxss_(short *g, short *l, size_t *num_elem);
extern void rcrsus_(unsigned short *g,
                                             unsigned short *l,
                                             size_t *num_elem);
extern void rcrpus_(unsigned short *g,
                                                 unsigned short *l,
                                                 size_t *num_elem);
extern void rcrnus_(unsigned short *g,
                                             unsigned short *l,
                                             size_t *num_elem);
extern void rcrxus_(unsigned short *g,
                                             unsigned short *l,
                                             size_t *num_elem);
extern void rcrssi_(int *g, int *l, size_t *num_elem);
extern void rcrpsi_(int *g, int *l, size_t *num_elem);
extern void rcrnsi_(int *g, int *l, size_t *num_elem);
extern void rcrxsi_(int *g, int *l, size_t *num_elem);
extern void rcrsui_(unsigned int *g, unsigned int *l,
                                    size_t *num_elem);
extern void rcrpui_(unsigned int *g,
                                               unsigned int *l,
                                               size_t *num_elem);
extern void rcrnui_(unsigned int *g, unsigned int *l,
                                    size_t *num_elem);
extern void rcrxui_(unsigned int *g, unsigned int *l,
                                    size_t *num_elem);
extern void rcrssl_(long *g, long *l, size_t *num_elem);
extern void rcrpsl_(long *g, long *l, size_t *num_elem);
extern void rcrnsl_(long *g, long *l, size_t *num_elem);
extern void rcrxsl_(long *g, long *l, size_t *num_elem);
extern void rcrsul_(unsigned long *g, unsigned long *l,
                                     size_t *num_elem);
extern void rcrpul_(unsigned long *g,
                                                unsigned long *l,
                                                size_t *num_elem);
extern void rcrnul_(unsigned long *g, unsigned long *l,
                                     size_t *num_elem);
extern void rcrxul_(unsigned long *g, unsigned long *l,
                                     size_t *num_elem);
extern void rcrsf_(float *g, float *l, size_t *num_elem);
extern void rcrpf_(float *g, float *l, size_t *num_elem);
extern void rcrnf_(float *g, float *l, size_t *num_elem);
extern void rcrxf_(float *g, float *l, size_t *num_elem);
extern void rcrsd_(double *g, double *l, size_t *num_elem);
extern void rcrpd_(double *g, double *l,
                                         size_t *num_elem);
extern void rcrnd_(double *g, double *l, size_t *num_elem);
extern void rcrxd_(double *g, double *l, size_t *num_elem);
extern void rcrsld_(long double *g, long double *l,
                                   size_t *num_elem);
extern void rcrpld_(long double *g, long double *l,
                                       size_t *num_elem);
extern void rcrnld_(long double *g, long double *l,
                                   size_t *num_elem);
extern void rcrxld_(long double *g, long double *l,
                                   size_t *num_elem);

extern void rcissc_(signed char *l, size_t *num_elem);
extern void rcipsc_(signed char *l, size_t *num_elem);
extern void rcinsc_(signed char *l, size_t *num_elem);
extern void rcixsc_(signed char *l, size_t *num_elem);
extern void rcisuc_(unsigned char *l, size_t *num_elem);
extern void rcipuc_(unsigned char *l, size_t *num_elem);
extern void rcinuc_(unsigned char *l, size_t *num_elem);
extern void rcixuc_(unsigned char *l, size_t *num_elem);
extern void rcisss_(short *l, size_t *num_elem);
extern void rcipss_(short *l, size_t *num_elem);
extern void rcinss_(short *l, size_t *num_elem);
extern void rcixss_(short *l, size_t *num_elem);
extern void rcisus_(unsigned short *l, size_t *num_elem);
extern void rcipus_(unsigned short *l, size_t *num_elem);
extern void rcinus_(unsigned short *l, size_t *num_elem);
extern void rcixus_(unsigned short *l, size_t *num_elem);
extern void rcissi_(int *l, size_t *num_elem);
extern void rcipsi_(int *l, size_t *num_elem);
extern void rcinsi_(int *l, size_t *num_elem);
extern void rcixsi_(int *l, size_t *num_elem);
extern void rcisui_(unsigned int *l, size_t *num_elem);
extern void rcipui_(unsigned int *l, size_t *num_elem);
extern void rcinui_(unsigned int *l, size_t *num_elem);
extern void rcixui_(unsigned int *l, size_t *num_elem);
extern void rcissl_(long *l, size_t *num_elem);
extern void rcipsl_(long *l, size_t *num_elem);
extern void rcinsl_(long *l, size_t *num_elem);
extern void rcixsl_(long *l, size_t *num_elem);
extern void rcisul_(unsigned long *l, size_t *num_elem);
extern void rcipul_(unsigned long *l, size_t *num_elem);
extern void rcinul_(unsigned long *l, size_t *num_elem);
extern void rcixul_(unsigned long *l, size_t *num_elem);
extern void rcisf_(float *l, size_t *num_elem);
extern void rcipf_(float *l, size_t *num_elem);
extern void rcinf_(float *l, size_t *num_elem);
extern void rcixf_(float *l, size_t *num_elem);
extern void rcisd_(double *l, size_t *num_elem);
extern void rcipd_(double *l, size_t *num_elem);
extern void rcind_(double *l, size_t *num_elem);
extern void rcixd_(double *l, size_t *num_elem);
extern void rcisld_(long double *l, size_t *num_elem);
extern void rcipld_(long double *l, size_t *num_elem);
extern void rcinld_(long double *l, size_t *num_elem);
extern void rcixld_(long double *l, size_t *num_elem);

extern void rcigsc_(signed char *l, signed char *g,
                                 size_t *num_elem);
extern void rciguc_(unsigned char *l, unsigned char *g,
                                   size_t *num_elem);
extern void rcigss_(short *l, short *g, size_t *num_elem);
extern void rcigus_(unsigned short *l, unsigned short *g,
                                    size_t *num_elem);
extern void rcigsi_(int *l, int *g, size_t *num_elem);
extern void rcigui_(unsigned int *l, unsigned int *g,
                                  size_t *num_elem);
extern void rcigsl_(long *l, long *g, size_t *num_elem);
extern void rcigul_(unsigned long *l, unsigned long *g,
                                   size_t *num_elem);
extern void rcigf_(float *l, float *g, size_t *num_elem);
extern void rcigd_(double *l, double *g, size_t *num_elem);
extern void rcigld_(long double *l, long double *g,
                                 size_t *num_elem);
