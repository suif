

/*
 * hwtimer.c
 */

#include "hwtimer.h"
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/syssgi.h>
#include <sys/immu.h>

static unsigned cycleval;
unsigned* iotimer_addr=0;
static long long startCount,endCount;

void HwTimerInit( void )
{
  unsigned phys_addr, raddr;
  int fd;
  static char buf[NBPP*2];
  unsigned int vaddr;
  extern int _is_mips2(void);
  int poffmask;
	
  iotimer_addr = 0;

  if( _is_mips2() )
	{
	  poffmask = getpagesize() - 1;
	  phys_addr = syssgi(SGI_QUERY_CYCLECNTR, &cycleval);
	  if( (int)phys_addr == -1 ) {
		 perror("HW counter init error");
		 return;
	  }
          printf("HwTimer: tick resolution is %6.3f nanosecs\n",
                 cycleval / 1000.0 );
	  raddr = ((unsigned)phys_addr & ~poffmask);
	  fd = open("/dev/mmem", O_RDONLY);

	  if( fd < 0 ) {
		 perror("HW counter /dev/mmem open error");
		 return;
	  }

	  vaddr = (unsigned int) buf;
	  vaddr = (vaddr + NBPP-1) & ~(NBPP-1);
          printf("mmap args: %x %x %x %x %x %x\n",
                 (void *)vaddr, poffmask, PROT_READ,
                 MAP_FIXED|MAP_PRIVATE, fd, (int)raddr);
	  fflush(stdout);
	  iotimer_addr = (unsigned*)mmap((void *)vaddr, poffmask, PROT_READ,
                                         MAP_FIXED|MAP_PRIVATE, fd, (int)raddr);

	  if( (int)iotimer_addr < 0 )
		perror("HW counter mmap error");

	  iotimer_addr = (unsigned*)
		((unsigned)iotimer_addr + poff(phys_addr));
	}
}


long long Hw_Counter_Read( void )
{
  unsigned hi, lo;
  if( !iotimer_addr )
	 return 0;
  while (1) {
	hi = *iotimer_addr;
	lo = *(iotimer_addr + 1);
	if (hi == *iotimer_addr) {
	  return (unsigned long long)hi<<32 | lo;
	}
  }
  return 0;
}

void HwTimerStart(void)
{
   startCount = Hw_Counter_Read();
#if 0
   printf(" start   %lld \n",startCount);
#endif
}

void HwTimerEnd(void)
{
   endCount = Hw_Counter_Read();
#if 0
   printf(" end     %lld diff =%lld \n",endCount,endCount-startCount);
#endif
}

double  HwTimerMicroSec(void)
{
   double retval;
   retval = 1.0 *  cycleval * (endCount - startCount) /( 1000.0* 1000.0);
#if 0
   printf("start %10lld end %10lld diff %10lld micro %f \n",
          startCount,endCount,(endCount-startCount),retval);
#endif
   return retval;
}

double  HwTimerSec(void)
{
   double retval;
   retval = 1.0 *  cycleval * (endCount - startCount) /( 1000.0* 1000.0 *1000.0 * 1000.0);
#if 0
   printf("start %10lld end %10lld diff %10lld micro %f \n",
          startCount,endCount,(endCount-startCount),retval);
#endif
   return retval;
}

