/* file "simos_sync.c" */ 
 
/* **********************************************************
 * simos_sync.c
 * 
 * This implements the C routines of simos_sync.h. The
 * assembly ones are in simos_sycasm.s
 *
 * These routines implement the Master-Slave barriers.
 * The implementation ASSUMES SEQUENTIAL CONSISTENCY
 * unless the MEMORY_BARRIER macros are correctly defined 
 * The code has never been tested on machines that do not
 * have SC, the MEMORY_BARRIER calls are more of a hint for
 * the porting than anything else
 * ***********************************************************/

#include <bstring.h>
#include <stdio.h>
#include "simos_sync.h"


void MSlaveBarrier_SlaveFirst(MasterSlaveBarrier *b, int myid)
{
   int g = b->genNumber;

   b->entered[myid] = 1;
   while( g == b->genNumber ) 
      continue;

   MEMORY_BARRIER
}


void MSlaveBarrier_SlaveEnter(MasterSlaveBarrier *b, int myid)
{
   int g = b->genNumber;

   b->entered[myid] = 1;
   while( g == b->genNumber ) 
      continue;

   MEMORY_BARRIER
}

void MSlaveBarrier_Wait(MasterSlaveBarrier *b, int numProcs)
{
   int i;

   for(i=1;i<numProcs;i++) {
      while( !b->entered[i] ) 
         continue;
   }
   bzero((void*)b->entered,numProcs);
}

void MSlaveBarrier_Release(MasterSlaveBarrier *b)
{
   WRITE_MEMORY_BARRIER

   b->copyGenNumber++;
   b->genNumber = b->copyGenNumber;

   MEMORY_BARRIER
}





