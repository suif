<HTML>
<HEAD>
<!-- This HTML file has been created by texi2html 1.54
     from parguide.texi on 28 April 1999 -->

<TITLE>The SUIF Parallelizing Compiler Guide - SUIF Limitations</TITLE>
<link href="parguide_5.html" rel=Previous>
<link href="parguide_toc.html" rel=ToC>

</HEAD>
<BODY>
<p>Go to the <A HREF="parguide_1.html">first</A>, <A HREF="parguide_5.html">previous</A>, next, last section, <A HREF="parguide_toc.html">table of contents</A>.
<P><HR><P>


<H2><A NAME="SEC6" HREF="parguide_toc.html#TOC6">SUIF Limitations</A></H2>

<P>
In the SUIF vs. KAP tables (see section <A HREF="parguide_4.html#SEC4">Performance Results</A>), a number of
the <EM>KAP only</EM> loops are due to SUIF parallelizing an outer loop
while KAP is parallelizing an inner loop.  However, there are a number
of limitations in the current SUIF compiler that cause it to miss some
loops that KAP is able to parallelize.

</P>

<H3>Loop Distribution</H3>
<P>
SUIF does not perform loop distribution, whereas the KAP compiler does.
This accounts for more of the <EM>KAP only</EM> loops than any of the
other limitations listed below.  For example, the following loop from
the Perfect club benchmark APS.f performs I/O in the loop and thus is not
parallelizable:

</P>

<PRE>
      DO 30 K=1,NZ
          UM(K)=REAL(WM(K))
          VM(K)=AIMAG(WM(K))
          WRITE(6,40) K,ZET(K),UG(K),VG(K),TM(K),DKM(K),UM(K),VM(K)
          WRITE(8,40) K,ZET(K),UG(K),VG(K),TM(K),DKM(K),UM(K),VM(K)
 30   CONTINUE
</PRE>

<P>
However, KAP is able to parallelize some of the statements in the loop
by applying loop distribution:

</P>

<PRE>
      DO 2 K=1,NZ
          UM(K)=REAL(WM(K))
          VM(K)=AIMAG(WM(K))
 2    CONTINUE

      DO 3 K=1,NZ
          WRITE(6,40) K,ZET(K),UG(K),VG(K),TM(K),DKM(K),UM(K),VM(K)
          WRITE(8,40) K,ZET(K),UG(K),VG(K),TM(K),DKM(K),UM(K),VM(K)
 3    CONTINUE
</PRE>

<P>
The <CODE>DO 2 K</CODE> loop can now be parallelized, and the <CODE>DO 3 K</CODE> loop
runs sequentially.

</P>


<H3>FORTRAN Equivalences</H3>
<P>
Another reason KAP is able to parallelize loops that SUIF cannot
is due to the way SUIF handles FORTRAN equivalences.  SUIF carries no
information about which variables could potentially refer to the same
memory location.  Accesses to equivalenced variables are
represented as offsets from a base memory location.  The parallelizer,
upon seeing these offsets, assumes that any location in memory could
potentially be touched, and thus cannot parallelize the loop.  This
problem shows up in the SPEC92 benchmark wave5.f, for example.  SUIF's
handling of equivalences is an artifact of our FORTRAN front end
being based on the FORTRAN-to-C converter <TT>`f2c'</TT>.

</P>
<P>
Each of the remaining SUIF limitations listed below have a lesser impact
than loop distribution and equivalences.  They each account for only a
small number of the <EM>KAP only</EM> loops.

</P>

<H3>Normalization with Symbolic Loop Steps</H3>
<P>
SUIF currently does not normalize loops with symbolic step sizes.  If a
loop is not normalized, the parallelizer will not consider parallelizing
the loop.

</P>

<H3>Non-Linear Loop Bounds</H3>
<P>
SUIF's dependence analysis is based on linear affine functions.  It will not
take the loop bounds into account if they are non-linear.  For example,
in the following loop from the SPEC92 benchmark wave5.f:

</P>

<PRE>
      NS2 = (N+1)/2
      NP2 = N+2
        ...
      DO 102 K=2,NS2
         KC = NP2-K
         XH(K) = W(K-1)*X(KC)+W(KC-1)*X(K)
         XH(KC) = W(K-1)*X(K)-W(KC-1)*X(KC)
 102  CONTINUE
</PRE>

<P>
SUIF replaces <CODE>NS2</CODE> with <CODE>(N+1)/2</CODE> and <CODE>KC</CODE> with 
<CODE>N+2-K</CODE>, leaving the following code:

</P>

<PRE>
      DO 102 K=2,(N+1)/2
         KC = NP2-K
         XH(K) = W(K-1)*X(N+2-K)+W(N-K+1)*X(K)
         XH(N+2-K) = W(K-1)*X(K)-W(N-K+1)*X(N+2-K)
 102  CONTINUE
</PRE>

<P>
However, since SUIF cannot determine whether <CODE>N+1</CODE> is even, the
function <CODE>(N+1)/2</CODE> is non-linear.  The dependence library 
then does not use the bounds to determine that the accesses to
<CODE>XH(K)</CODE> and <CODE>X(N+2-K)</CODE> are independent.

</P>

<H3>Multiple Symbolic Coefficients</H3>
<P>
We have extended SUIF's dependence analysis to handle simple non-linear
array index functions.  However, this analysis will not work when there
are multiple symbolic coefficients.  For example in the following loop
from the Perfect club benchmark APS.f:

</P>

<PRE>
     DO 30 K=1,NZTOP
       DO 20 J=1,NY
         DO 10 I=1,NX
           L=L+1
           DCDX(L)=-(UX(L)+UM(K))*DCDX(L)-(VY(L)+VM(K))*DCDY(L)+Q(L)
 10      CONTINUE
 20    CONTINUE
 30  CONTINUE
</PRE>

<P>
The auxiliary induction variable <CODE>L</CODE> is replaced with a function of
the loop indices, and the resulting accesses to the array <CODE>DCDX</CODE>
become <CODE>DCDX(NY * NX * (K-1) + NX * (J-1) + I - 1)</CODE>.  Because 
of the multiple symbolic coefficients <CODE>NY</CODE> and <CODE>NX</CODE>, the
dependence library is not able to analyze the expression.

</P>

<H3>Symbolic Analysis</H3>
<P>
SUIF's scalar analysis uses only localized information, and does not
perform symbolic analysis.  For example, symbolic analysis is needed for
the following loop from the Perfect benchmark WSS.f:

</P>

<PRE>
 14  KBOT = KK - 1
     KTOP = KK

     ... code containing IF statements deleted ...

     DO 18 K=KK,KTOP
 18  Q(K) = Q(KBOT)
</PRE>

<P>
KAP is able replace <CODE>KBOT</CODE> with <CODE>KK-1</CODE> and can thus determine
that the two accesses to array <CODE>Q</CODE> are independent.

</P>

<H3>Scalarized Array Accesses</H3>
<P>
KAP is able to copy references to array elements into scalars if they
are only accessed by constant indices within a loop.  It is then able to
privatize the scalars, and parallelize the loop.  SUIF's <CODE>porky
-scalarize</CODE> pass will only turn array elements into scalars if they are
accessed by constant indices throughout the entire program.

</P>

<H3>FORTRAN Intrinsics</H3>
<P>
SUIF's FORTRAN front-end, <TT>`sf2c'</TT>, converts some FORTRAN intrinsics
into procedures that are not pure functions.  Thus any loops containing
calls to these procedures are not parallelized.  For example, the
intrinsic <CODE>EXP</CODE> (on complex values) gets translated to a procedure
with two arguments in the <TT>`F77'</TT> library.

</P>

<H3>Imperfect Reductions</H3>
<P>
KAP is able to handle some forms of reductions that SUIF cannot yet
handle.  For example, consider the following loop from the Perfect club
benchmark OCS.f:

</P>

<PRE>
      DO 30 I=2,N2P
 30   WORK(1) = MAX(WORK(1), WORK(I))
</PRE>

<P>
Elements <CODE>WORK(2)</CODE> through <CODE>WORK(N2P)</CODE> are reduced into
<CODE>WORK(1)</CODE>.  Currently, SUIF will not find reductions of arrays into
a subsection of the array.

</P>

<H3>Reductions over Complex Variables</H3>
<P>
SUIF is not be able to recognize some reductions over complex variables.
This is due to the fact that SUIF represents complex variables as arrays
of size 2 -- one element for the complex part and one for the real part.
Also, the front-end often generates extra temporaries for these complex
variables.  The array references to complex variables are turned into
scalars by the <CODE>porky -scalarize</CODE> pass.  However, the reduction
recognition pass cannot handle the indirect reductions through the
temporaries.

</P>

<H3>Conditional Parallelization</H3>
<P>
Finally, KAP performs conditional parallelization, whereas SUIF does
not.  KAP will duplicate a loop to create a parallel version and a
sequential version.  For example, consider the following loop from the
SPEC92 benchmark su2cor.f:

</P>

<PRE>
      DO 40 I=0,249
          IREG(I)=IREG(I+LVEC)
 40   CONTINUE
</PRE>

<P>
This loop is sequential only when <CODE>LVEC &#60;= 249</CODE>, and
thus the second loop below can run in parallel:

</P>

<PRE>
      IF (LVEC .LE. 249) THEN

C Sequential
      DO 40 I=0,249
          IREG(I)=IREG(I+LVEC)
 40   CONTINUE

      ELSE

C Parallel
      DO 2 I=0,249
          IREG(I) = IREG(I+LVEC)
    2 CONTINUE
      END IF
</PRE>

<P><HR><P>
<p>Go to the <A HREF="parguide_1.html">first</A>, <A HREF="parguide_5.html">previous</A>, next, last section, <A HREF="parguide_toc.html">table of contents</A>.
</BODY>
</HTML>
