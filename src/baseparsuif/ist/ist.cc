/* file "ist.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  Implementation of the SUIF Interprocedural Symbolic Translation
 *  library.
 */

#define _MODULE_ "libist.a"

#pragma implementation "ist.h"

#define RCS_BASE_FILE ist_cc

#include "ist.h"
#include <useful.h>

RCS_BASE(
    "$Id: ist.cc,v 1.1.1.1 1998/07/07 05:09:29 brm Exp $")


static char *k_ist_temp = "ist temp";
static char *k_ist_placeholder = "ist placeholder";
static char *k_ist_save_var = "ist save var";
static char *k_ist_solid_version = "ist solid version";


class placeholder_data
  {
public:
    operand value;
    in_cal *callsite;

    placeholder_data(operand new_value, in_cal *new_callsite)
      {
        value = new_value.clone();
        callsite = new_callsite;
      }
    ~placeholder_data(void)
      {
        if (value.is_expr())
            delete value.instr();
      }
  };

class type_translation_data
  {
public:
    type_node *translated_type;
    in_cal *callsite;

    type_translation_data(type_node *new_translated_type, in_cal *new_callsite)
      {
        translated_type = new_translated_type;
        callsite = new_callsite;
      }
  };


/*
 *  The variable old_local should be a true local variable, which
 *  includes placeholder variables but not parameters or
 *  sub-variables.  The result is a local variable on the other side
 *  of the call, either up or down depending on which side old_local
 *  was on.  If old_local was a placeholder variable for crossing the
 *  call, the result will be the original variable on which that
 *  placeholder was based.  Otherwise, the result will be an
 *  appropriate placeholder variable.
 */
var_sym *ist::local_through_call(in_cal *the_call, var_sym *old_local,
                                 base_symtab *dest_scope)
  {
    alist_iter data_iter(&placeholder_list);
    while (!data_iter.is_empty())
      {
        alist_e *this_elem = data_iter.step();
        placeholder_data *data_block = (placeholder_data *)(this_elem->info);
        if (data_block->callsite == the_call)
          {
            if (data_block->value == operand(old_local))
                return (var_sym *)(this_elem->key);
            if ((this_elem->key == old_local) && data_block->value.is_symbol())
                return data_block->value.symbol();
          }
      }

    placeholder_data *new_block =
            new placeholder_data(operand(old_local), the_call);
    var_sym *new_var = dest_scope->new_unique_var(type_signed, "__ist_");
    new_var->append_annote(k_ist_placeholder, this);
    placeholder_list.enter(new_var, new_block);
    new_var->set_type(type_through_call(the_call, old_local->type(),
                                        dest_scope));
    return new_var;
  }

type_node *ist::type_through_call(in_cal *the_call, type_node *old_type,
                                  base_symtab *dest_scope)
  {
    if (dest_scope->is_ancestor(old_type->parent()))
        return old_type;

    alist_iter data_iter(&type_translation_list);
    while (!data_iter.is_empty())
      {
        alist_e *this_elem = data_iter.step();
        type_translation_data *data_block =
                (type_translation_data *)(this_elem->info);
        if (data_block->callsite == the_call)
          {
            if (data_block->translated_type == old_type)
                return (type_node *)(this_elem->key);
            if (this_elem->key == old_type)
                return data_block->translated_type;
          }
      }

    type_node *new_type;

    switch (old_type->op())
      {
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
          {
            base_type *old_base = (base_type *)old_type;
            new_type = new base_type(old_base->op(), old_base->size(),
                                     old_base->is_signed());
            break;
          }
        case TYPE_PTR:
          {
            new_type = new ptr_type(type_signed);
            break;
          }
        case TYPE_ARRAY:
          {
            new_type =
                    new array_type(type_signed, unknown_bound, unknown_bound);
            break;
          }
        case TYPE_FUNC:
          {
            new_type = new func_type(type_signed);
            break;
          }
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *old_struct = (struct_type *)old_type;
            new_type =
                    new struct_type(old_struct->op(), old_struct->size(),
                                    old_struct->name(),
                                    old_struct->num_fields());
            break;
          }
        case TYPE_ENUM:
          {
            enum_type *old_enum = (enum_type *)old_type;

            unsigned num_values = old_enum->num_values();
            enum_type *new_enum =
                    new enum_type(old_enum->name(), old_enum->size(),
                                  old_enum->is_signed(), num_values);

            for (unsigned value_num = 0; value_num < num_values; ++value_num)
              {
                new_enum->set_member(value_num,
                                     old_enum->member(value_num));
                new_enum->set_value(value_num, old_enum->value(value_num));
              }

            new_type = new_enum;
            break;
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *old_modifier = (modifier_type *)old_type;
            new_type = new modifier_type(old_modifier->op(), type_signed);
            break;
          }
        default:
            assert(FALSE);
      }

    /*
     *  This type must be installed now because it will be referenced
     *  by other things.  But after it is possibly referenced by
     *  recursion among types, it must be changed, to get the
     *  sub-types right.  So we need it installed but not merged with
     *  anything else that might be the same up to this point.  So we
     *  put on a temporary annotation and remove it.
     */
    new_type->append_annote(k_ist_temp);
    new_type = dest_scope->install_type(new_type);
    new_type->get_annote(k_ist_temp);

    type_translation_data *new_block =
            new type_translation_data(old_type, the_call);
    new_type->append_annote(k_ist_placeholder, this);
    type_translation_list.enter(new_type, new_block);

    switch (old_type->op())
      {
        case TYPE_PTR:
          {
            ptr_type *old_ptr = (ptr_type *)old_type;
            ptr_type *new_ptr = (ptr_type *)new_type;

            new_ptr->set_ref_type(type_through_call(the_call,
                                                    old_ptr->ref_type(),
                                                    dest_scope));
            break;
          }
        case TYPE_ARRAY:
          {
            array_type *old_array = (array_type *)old_type;
            array_type *new_array = (array_type *)new_type;

            type_node *old_elem_type = old_array->elem_type();
            new_array->set_elem_type(type_through_call(the_call, old_elem_type,
                                                       dest_scope));

            array_bound old_lower = old_array->lower_bound();
            new_array->set_lower_bound(bound_through_call(the_call, old_lower,
                                                          dest_scope));

            array_bound old_upper = old_array->upper_bound();
            new_array->set_upper_bound(bound_through_call(the_call, old_upper,
                                                          dest_scope));

            break;
          }
        case TYPE_FUNC:
          {
            func_type *old_func = (func_type *)old_type;
            func_type *new_func = (func_type *)new_type;

            type_node *old_return_type = old_func->return_type();
            new_func->set_return_type(type_through_call(the_call,
                                                        old_return_type,
                                                        dest_scope));
            if (old_func->args_known())
              {
                unsigned num_args = old_func->num_args();
                new_func->set_num_args(num_args);

                for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                  {
                    type_node *new_arg_type =
                            type_through_call(the_call,
                                              old_func->arg_type(arg_num),
                                              dest_scope);
                    new_func->set_arg_type(arg_num, new_arg_type);
                  }

                if (old_func->has_varargs())
                    new_func->set_varargs(TRUE);
              }
            else
              {
                new_func->set_args_unknown();
              }

            break;
          }
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *old_struct = (struct_type *)old_type;
            struct_type *new_struct = (struct_type *)new_type;

            unsigned num_fields = old_struct->num_fields();
            for (unsigned field_num = 0; field_num < num_fields; ++num_fields)
              {
                const char *old_field_name = old_struct->field_name(field_num);
                new_struct->set_field_name(field_num, old_field_name);

                type_node *old_field_type =
                        old_struct->field_type(field_num);
                type_node *new_field_type =
                        type_through_call(the_call, old_field_type,
                                          dest_scope);
                new_struct->set_field_type(field_num, new_field_type);

                new_struct->set_offset(field_num,
                                       old_struct->offset(field_num));
              }

            break;
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *old_modifier = (modifier_type *)old_type;
            modifier_type *new_modifier = (modifier_type *)new_type;

            new_modifier->set_base(type_through_call(the_call,
                                                     old_modifier->base(),
                                                     dest_scope));
            break;
          }
        default:
            break;
      }

    annotations_through_call(the_call, old_type, new_type, dest_scope);

    return new_type;
  }

array_bound ist::bound_through_call(in_cal *the_call, array_bound old_bound,
                                    base_symtab *dest_scope)
  {
    if (old_bound.is_variable())
      {
        operand new_op =
                op_through_call(operand(old_bound.variable()), the_call,
                                dest_scope);

        int result;
        boolean is_int = operand_is_int_const(new_op, &result);
        if (is_int)
            return array_bound(result);

        var_sym *new_var = op_to_var(new_op, dest_scope);
        if (new_op.is_expr())
            delete new_op.instr();
        return array_bound(new_var);
      }
    else
      {
        return old_bound;
      }
  }

void ist::annotations_through_call(in_cal *the_call, suif_object *old_object,
                                   suif_object *new_object,
                                   base_symtab *dest_scope)
  {
    annote_list_iter annote_iter(old_object->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();

        immed_list *new_immeds = NULL;
        if (this_annote->data() != NULL)
          {
            new_immeds = new immed_list;

            immed_list_iter immed_iter(this_annote->immeds());
            while (!immed_iter.is_empty())
              {
                immed this_immed = immed_iter.step();
                switch (this_immed.kind())
                  {
                    case im_symbol:
                      {
                        sym_addr new_addr =
                                sym_addr_through_call(this_immed.addr(),
                                                      the_call, dest_scope);
                        new_immeds->append(immed(new_addr));
                        break;
                      }
                    case im_type:
                      {
                        type_node *new_type =
                                type_through_call(the_call, this_immed.type(),
                                                  dest_scope);
                        new_immeds->append(immed(new_type));
                        break;
                      }
                    default:
                        new_immeds->append(this_immed);
                  }
              }
          }

        annote *new_annote = new annote(this_annote->name(), new_immeds);
        new_object->annotes()->append(new_annote);
      }
  }

var_sym *ist::op_to_var(operand the_op, base_symtab *scope)
  {
    if (the_op.is_symbol())
        return the_op.symbol();

    alist_iter data_iter(&placeholder_list);
    while (!data_iter.is_empty())
      {
        alist_e *this_elem = data_iter.step();
        placeholder_data *data_block = (placeholder_data *)(this_elem->info);
        if ((data_block->callsite == NULL) &&
            (operands_are_same_expr(data_block->value, the_op)))
          {
            return (var_sym *)(this_elem->key);
          }
      }

    placeholder_data *new_block = new placeholder_data(the_op, NULL);
    var_sym *new_var = scope->new_unique_var(the_op.type(), "__ist_");
    new_var->append_annote(k_ist_placeholder, this);
    placeholder_list.enter(new_var, new_block);
    return new_var;
  }

operand ist::op_through_call(operand original_op, in_cal *the_call,
                             base_symtab *dest_scope)
  {
    switch (original_op.kind())
      {
        case OPER_NULL:
            return original_op;
        case OPER_SYM:
          {
            var_sym *the_var = original_op.symbol();
            if (dest_scope->is_ancestor(the_var->parent()))
                return original_op;

            if (the_var->is_param() &&
                dest_scope->is_ancestor(the_call->owner()->scope()))
              {
                operand arg_op = param_up(the_var, the_call);

                if (the_var->type()->is_call_by_ref())
                  {
                    type_node *new_type = the_var->type()->unqual()->ptr_to();
                    arg_op = fold_real_1op_rrr(io_cvt, new_type, arg_op);
                    arg_op = fold_load(arg_op);
                  }

                return arg_op;
              }
            else
              {
                var_sym *new_var =
                        local_through_call(the_call, the_var->root_ancestor(),
                                           dest_scope);
                if (the_var->parent_var() == NULL)
                  {
                    return operand(new_var);
                  }
                else
                  {
                    type_node *new_type =
                            type_through_call(the_call, the_var->type(),
                                              dest_scope);
                    var_sym *new_sub =
                            new_var->find_child(the_var->offset(), new_type);
                    if (new_sub != NULL)
                        return new_sub;

                    in_ldc *new_ldc =
                            new in_ldc(new_type->ptr_to(), operand(),
                                       immed(new_var, the_var->offset()));
                    in_rrr *new_load =
                            new in_rrr(io_lod, new_type, operand(),
                                       operand(new_ldc));
                    return operand(new_load);
                  }
              }
          }
        case OPER_INSTR:
          {
            instruction *old_instr = original_op.instr();

            type_node *new_result_type =
                    type_through_call(the_call, old_instr->result_type(),
                                      dest_scope);
            instruction *new_instr;
            switch (old_instr->format())
              {
                case inf_none:
                    assert(FALSE);
                case inf_rrr:
                  {
                    in_rrr *old_rrr = (in_rrr *)old_instr;
                    new_instr = new in_rrr(old_rrr->opcode(), new_result_type);
                    break;
                  }
                case inf_bj:
                  {
                    in_bj *old_bj = (in_bj *)old_instr;
                    new_instr = new in_bj(old_bj->opcode(), old_bj->target());
                    break;
                  }
                case inf_ldc:
                  {
                    in_ldc *old_ldc = (in_ldc *)old_instr;

                    immed old_value = old_ldc->value();
                    if (old_value.is_symbol())
                      {
                        sym_node *old_sym = old_value.symbol();
                        if (!dest_scope->is_ancestor(old_sym->parent()))
                          {
                            if (old_sym->is_var())
                              {
                                var_sym *old_var = (var_sym *)old_sym;
                                if (old_var->type()->is_call_by_ref() &&
                                    dest_scope->is_ancestor(
                                            the_call->owner()->scope()))
                                  {
                                    return param_up(old_var, the_call);
                                  }
                              }
                            var_sym *new_var =
                                    op_to_var(operand(old_ldc),
                                              old_sym->parent());
                            return op_through_call(operand(new_var), the_call,
                                                   dest_scope);
                          }
                      }

                    new_instr =
                            new in_ldc(new_result_type, operand(), old_value);
                    break;
                  }
                case inf_cal:
                  {
                    in_cal *old_cal = (in_cal *)old_instr;
                    new_instr =
                            new in_cal(new_result_type, operand(), operand(),
                                       old_cal->num_args());
                    break;
                  }
                case inf_array:
                  {
                    in_array *old_array = (in_array *)old_instr;
                    new_instr =
                            new in_array(new_result_type, operand(), operand(),
                                         old_array->elem_size(),
                                         old_array->dims(),
                                         old_array->offset());
                    break;
                  }
                case inf_mbr:
                  {
                    in_mbr *old_mbr = (in_mbr *)old_instr;
                    unsigned num_labs = old_mbr->num_labs();
                    in_mbr *new_mbr =
                            new in_mbr(operand(), old_mbr->lower(),
                                       num_labs, old_mbr->default_lab());
                    for (unsigned lab_num = 0; lab_num < num_labs; ++lab_num)
                        new_mbr->set_label(lab_num, old_mbr->label(lab_num));
                    new_instr = new_mbr;
                    break;
                  }
                case inf_lab:
                  {
                    in_lab *old_lab = (in_lab *)old_instr;
                    new_instr = new in_lab(old_lab->label());
                    break;
                  }
                case inf_gen:
                  {
                    in_gen *old_gen = (in_gen *)old_instr;
                    new_instr =
                            new in_gen(old_gen->name(), new_result_type,
                                       operand(), old_gen->num_srcs());
                    break;
                  }
                default:
                    assert(FALSE);
              }

            annotations_through_call(the_call, old_instr, new_instr,
                                     dest_scope);

            unsigned num_srcs = old_instr->num_srcs();
            for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
              {
                operand new_op =
                        op_through_call(old_instr->src_op(src_num), the_call,
                                        dest_scope);
                new_instr->set_src_op(src_num, new_op);
              }

            return operand(new_instr);
          }
        default:
            assert(FALSE);
            return operand();
      }
  }

sym_addr ist::sym_addr_through_call(sym_addr original_addr, in_cal *the_call,
                                    base_symtab *dest_scope)
  {
    sym_node *old_sym = original_addr.symbol();
    if (!old_sym->is_var())
        return original_addr;

    var_sym *old_var = (var_sym *)old_sym;
    if (old_var->parent_var() != NULL)
      {
        var_sym *old_parent = old_var->parent_var();
        type_node *new_parent_type =
                type_through_call(the_call, old_parent->type(), dest_scope);
        sym_addr new_parent_addr =
                sym_addr_through_call(sym_addr(old_parent, 0), the_call,
                                      dest_scope);
        if ((new_parent_addr.offset() == 0) &&
            new_parent_addr.symbol()->is_var())
          {
            var_sym *new_parent_var = (var_sym *)(new_parent_addr.symbol());
            if (new_parent_var->type() == new_parent_type)
              {
                type_node *new_sub_type =
                        type_through_call(the_call, old_var->type(),
                                          dest_scope);
                var_sym *new_sub =
                        new_parent_var->find_child(old_var->offset(),
                                                   new_sub_type);
                if (new_sub == NULL)
                  {
                    new_sub = new_parent_var->build_child(old_var->offset(),
                                                          new_sub_type,
                                                          old_var->name());
                  }
                return sym_addr(new_sub, original_addr.offset());
              }
          }

        return sym_addr(new_parent_addr.symbol(),
                        original_addr.offset() + new_parent_addr.offset() +
                        old_var->offset());
      }
    else
      {
        operand new_op =
                op_through_call(operand(old_var), the_call, dest_scope);
        var_sym *new_var = op_to_var(new_op, dest_scope);
        if (new_op.is_expr())
            delete new_op.instr();
        return sym_addr(new_var, original_addr.offset());
      }
  }

operand ist::param_up(var_sym *the_param, in_cal *the_call)
  {
    assert(the_param->is_param());
    base_symtab *param_symtab = the_param->parent();

    assert(param_symtab->is_proc());
    proc_symtab *the_proc_symtab = (proc_symtab *)param_symtab;

    unsigned param_num = 0;
    sym_node_list_iter param_iter(the_proc_symtab->params());
    while (!param_iter.is_empty())
      {
        sym_node *this_symbol = param_iter.step();
        if (the_param == this_symbol)
            break;
        ++param_num;
        if (param_num == 0)
            error_line(1, NULL, "overflow in parameter count");
      }

    if (param_num >= the_call->num_args())
      {
        error_line(1, NULL, "not enough arguments to function call");
      }

    return the_call->argument(param_num).clone();
  }

void ist::solidify_var(var_sym *the_var)
  {
    if (!is_ist_placeholder(the_var))
        return;
    if (the_var->annotes()->peek_annote(k_ist_solid_version) != NULL)
        return;

    alist_iter data_iter(&placeholder_list);
    while (!data_iter.is_empty())
      {
        alist_e *this_elem = data_iter.step();
        var_sym *key_var = (var_sym *)(this_elem->key);
        if (key_var == the_var)
          {
            placeholder_data *data_block =
                    (placeholder_data *)(this_elem->info);
            in_cal *callsite = data_block->callsite;
            the_var->append_annote(k_ist_save_var, this);
            the_var->append_annote(k_ist_solid_version);
            if (data_block->callsite->owner()->scope()->is_visible(the_var))
              {
                /*
                 * In this case, we are using the value of a callee's
                 * local variable in the caller.  So the value is used
                 * either before or after the call, which means either
                 * before or after the variable's scope is entered.
                 * So the value is undefined in either case and we can
                 * just leave the variable unassigned.
                 */
              }
            else
              {
                /* @@@ -- Here we really should do something about the
                 * callee's function type, and also about multiple
                 * call sites. */
                unsigned old_arg_count = callsite->num_args();
                callsite->set_num_args(old_arg_count + 1);
                callsite->set_argument(old_arg_count,
                                       data_block->value.clone());
                base_symtab *base_parent = the_var->parent();
                assert(base_parent->is_proc());
                proc_symtab *proc_parent = (proc_symtab *)base_parent;
                the_var->set_param();
                proc_parent->params()->append(the_var);
              }
            solidify_type(the_var->type());
            return;
          }
      }

    assert(FALSE);
  }

void ist::solidify_type(type_node *the_type)
  {
    if (the_type->peek_annote(k_ist_placeholder) != this)
        return;
    if (the_type->annotes()->peek_annote(k_ist_solid_version) != NULL)
        return;
    the_type->append_annote(k_ist_save_var, this);
    the_type->append_annote(k_ist_solid_version);
    if (the_type->op() == TYPE_ARRAY)
      {
        array_type *the_array = (array_type *)the_type;
        if (the_array->lower_bound().is_variable())
            solidify_var(the_array->lower_bound().variable());
        if (the_array->upper_bound().is_variable())
            solidify_var(the_array->upper_bound().variable());
      }
    unsigned num_ref_types = the_type->num_ref_types();
    for (unsigned ref_num = 0; ref_num < num_ref_types; ++ref_num)
        solidify_type(the_type->ref_type(ref_num));
  }

ist::ist(void)
  {
    save_temp_vars = FALSE;
  }

ist::~ist(void)
  {
    while (!placeholder_list.is_empty())
      {
        alist_e *this_elem = placeholder_list.pop();
        placeholder_data *data_block = (placeholder_data *)(this_elem->info);
        var_sym *this_var = (var_sym *)(this_elem->key);
        delete this_elem;
        if (!save_temp_vars && (this_var->peek_annote(k_ist_save_var) != this))
          {
            this_var->parent()->remove_sym(this_var);
            delete this_var;
          }
        delete data_block;
      }

    while (!type_translation_list.is_empty())
      {
        alist_e *this_elem = type_translation_list.pop();
        type_translation_data *data_block =
                (type_translation_data *)(this_elem->info);
        type_node *this_type = (type_node *)(this_elem->key);
        delete this_elem;
        if (!save_temp_vars &&
            (this_type->peek_annote(k_ist_save_var) != NULL))
          {
            this_type->parent()->remove_type(this_type);
            delete this_type;
          }
        delete data_block;
      }
  }

operand ist::up_from_call(operand op_in_call, in_cal *the_call)
  {
    return op_through_call(op_in_call, the_call, the_call->owner()->scope());
  }

operand ist::down_through_call(operand op_in_caller, in_cal *the_call)
  {
    proc_sym *call_proc = proc_for_call(the_call);
    if (call_proc == NULL)
      {
        error_line(1, the_call->owner(),
                   "ist::down_through_call() attempted at callsite for which "
                   "the callee procedure is unknown");
      }
    if (call_proc->block() == NULL)
      {
        error_line(1, the_call->owner(),
                   "ist::down_through_call() attempted at callsite for which "
                   "the callee procedure is not in memory");
      }
    base_symtab *crossover_symtab = call_proc->block()->proc_syms();

    return op_through_call(op_in_caller, the_call, crossover_symtab);
  }

boolean ist::is_ist_placeholder(var_sym *the_var)
  {
    return (this == the_var->peek_annote(k_ist_placeholder));
  }

operand ist::placeholder_value(var_sym *the_placeholder)
  {
    assert(is_ist_placeholder(the_placeholder));

    placeholder_data *data_block =
            (placeholder_data *)(placeholder_list.lookup(the_placeholder));
    return data_block->value.clone();
  }

in_cal *ist::placeholder_callsite(var_sym *the_placeholder)
  {
    assert(is_ist_placeholder(the_placeholder));

    placeholder_data *data_block =
            (placeholder_data *)(placeholder_list.lookup(the_placeholder));
    return data_block->callsite;
  }

operand ist::solidify(operand to_solidify, tree_node *place)
  {
    switch (to_solidify.kind())
      {
        case OPER_NULL:
            return to_solidify;
        case OPER_SYM:
            solidify_var(to_solidify.symbol());
            return to_solidify;
        case OPER_INSTR:
          {
            instruction *the_instr = to_solidify.instr();
            solidify_type(the_instr->result_type());
            unsigned num_srcs = the_instr->num_srcs();
            for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
                solidify(the_instr->src_op(src_num), place);
            return to_solidify;
          }
        default:
            assert(FALSE);
            return to_solidify;
      }
  }

void ist::save_placeholders(void)
  {
    save_temp_vars = TRUE;
  }
