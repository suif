/* file "ist.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  Header for the SUIF Interprocedural Symbolic Translation library.
 */

#ifndef IST_H
#define IST_H

#include <suif1.h>

/*
 *      Purpose
 *      -------
 *
 *  An object of the ist class acts as a translator to move
 *  expressions in one part of the SUIF code to the corresponding
 *  expression in another part of the SUIF code, in particular in or
 *  out of a calling context.
 *
 *  There are two primary complications.  The first is that the
 *  expression may reference symbols that are outside the scope into
 *  which it is to be moved.
 *
 *  If such a symbol is a parameter, there is a translation and that
 *  translation is made by the ist object.  For the case of
 *  parameters, the actual parameter expressions are simply
 *  substituted for the formal parameter symbols.
 *
 *  The remaining case is of a symbol that is truely local, neither a
 *  parameter nor a common block member.  How it would be best to
 *  translate such a symbol depends on what the translation is being
 *  used for.  If there is the possibility of recursion, there is no
 *  correct way to make the symbol visible in the calling context.
 *  Changing the local into a global would change the semantics.  Even
 *  for Fortran, where recursion is not allowed, it would not be
 *  desirable to make lots of locals into globals during analysis
 *  since the globals would make further analysis more difficult.  The
 *  way we've chosen to deal with this translation is also influenced
 *  by anther consideration, which is the second main complication.
 *
 *  This second primary complication is in translating types to a
 *  different scope.  In particular, Fortran array types can have
 *  bounds that are expressions of the parameters of a function or
 *  members of a common block that are set outside the function.
 *  These expressions can be moved up one calling context and there
 *  will always be a translation, but the translations may include
 *  local variables of the caller, so moving up through another
 *  calling context may lead to a value that cannot be expresssed in
 *  the new calling context.
 *
 *  In addition, there is the problem that the SUIF type system allows
 *  only variable symbols, not arbitrary expressions, as loop bounds.
 *  The Fortran front end creates dummy variables to represent the
 *  bound expressions, then assigns to these variables at the very
 *  beginning of each procedure.  For interprocedural analysis we want
 *  to see the expressions for types; dummy variables are not enough.
 *
 *  The ist class deals with array types with variable bounds by
 *  creating new dummy variables and keeping track of the expressions
 *  these place holder symbols represent.  By keeping its own table of
 *  dummy variables and their values, an object of this class can make
 *  it easy for the application to see the real expression for each
 *  bound and its relation to other bounds.  It also facilitates
 *  moving types up through several procedure calls by not introducing
 *  unnecessary local variables into the final bound expression.
 *
 *  Since the ist object keeps track of the expressions for the values
 *  of its dummy variables, it does not have to change the SUIF code
 *  to actually assign to these variables.  In many cases, the
 *  expressions propagated through call sites will only be used for
 *  analysis and never placed in the SUIF code.  In that case the ist
 *  object can even remove all the dummy symbols and types it has
 *  introduced when the analysis is complete.  The class does provide
 *  a mechanism to ``solidify'' the dummy variables.  This will insert
 *  the necessary SUIF code to give the dummy variables the correct
 *  values at a given point.
 *
 *  Given that all of this mechanism is used for array type
 *  considerations, the same thing is done with local variables in
 *  expressions moved outside their scopes.  Dummy variables are
 *  created to represent the value of each local variable for a
 *  particular calling context, as needed.  These dummy variables are
 *  just like those created for array types -- the code to give them
 *  the correct values is not added unless it is needed.
 *
 *      Interface
 *      ---------
 *
 *  up_from_call()
 *  down_through_call()
 *      These two methods constitute the main interface to an ist
 *      object.  Given a callsite and an expression in the context of
 *      the called function, up_from_call() returns an expression in
 *      the context of the caller.  The down_through_call() method
 *      does the inverse translation: given an expression in the
 *      context of the caller, it returns an expression in the context
 *      of the callee.  Note that the returned expression may in
 *      general refer to placeholder variables.  If the expression is
 *      to be put in executable SUIF code, or used by any code that
 *      does not understand about the placeholders, the
 *      ist::solidify() method must first be called on that
 *      expression.
 *
 *      The ist class manipulates entire expression trees through the
 *      SUIF operand class.  This method returns an entirely new
 *      expression tree, with all new nodes, even if no changes are
 *      needed; hence it should be deallocated by the caller.  The
 *      input expression is unchanged.
 *
 *  is_ist_placeholder()
 *      This method will return TRUE iff the given symbol is a
 *      placeholder created by the ist.
 *
 *  placeholder_value()
 *      This method will return a copy of the expression for a
 *      placeholder variable.  It is an error to call this method on a
 *      non-placeholder variable.  The placeholder variable is always
 *      local (to the scope containing the callsite for which it was
 *      created).  The value expression is either in terms of that
 *      same scope it is local to, for the case of placeholders just
 *      for array bounds, or it is local to the callee of the callsite
 *      for which it was created, for the case of placeholders to
 *      translate local variables out of their scopes.  These cases
 *      can be distinguished by a call to the placeholder_callsite()
 *      method.
 *
 *  placeholder_callsite()
 *      This method returns either the callsite of a placeholder
 *      variable if that placeholder was used to translate from an
 *      instance of a local variable in the callee at that callsite,
 *      or NULL if its argument is a placeholder whose value is in
 *      terms of its own scope (i.e. a placeholder used for an array
 *      bound).  It is an error to call this method on a variable
 *      which is not a placeholder of this ist object.
 *
 *  solidify()
 *      This method converts an operand using placeholders to one that
 *      uses only symbols that have the correct values at the given
 *      point in the SUIF code.  The SUIF code is modified to assign
 *      the necessary variables.  Note that this method is destructive
 *      -- its argument is changed and then returned, so the original
 *      operand cannot be used or deallocated.
 *
 *  save_placeholders()
 *      This method instructs the ist object not to remove any
 *      placeholder variables or types which use them when its
 *      destructor is called.  By default, all placeholders and types
 *      created that reference them are removed from their symbol
 *      tables and deleted, except those that are used in expressions
 *      that have been solidified.
 */
class ist
  {
private:
    boolean save_temp_vars;
    alist placeholder_list;
    alist type_translation_list;

    var_sym *local_through_call(in_cal *the_call, var_sym *old_local,
                                base_symtab *dest_scope);
    type_node *type_through_call(in_cal *the_call, type_node *old_type,
                                 base_symtab *dest_scope);
    array_bound bound_through_call(in_cal *the_call, array_bound old_bound,
                                   base_symtab *dest_scope);
    void annotations_through_call(in_cal *the_call, suif_object *old_object,
                                  suif_object *new_object,
                                  base_symtab *dest_scope);
    var_sym *op_to_var(operand the_op, base_symtab *scope);
    operand op_through_call(operand original_op, in_cal *the_call,
                            base_symtab *dest_scope);
    sym_addr sym_addr_through_call(sym_addr original_addr, in_cal *the_call,
                                   base_symtab *dest_scope);
    operand param_up(var_sym *the_param, in_cal *the_call);
    void solidify_var(var_sym *the_var);
    void solidify_type(type_node *the_type);

public:
    ist(void);
    ~ist(void);

    operand up_from_call(operand op_in_call, in_cal *the_call);
    operand down_through_call(operand op_in_caller, in_cal *the_call);
    boolean is_ist_placeholder(var_sym *the_var);
    operand placeholder_value(var_sym *the_placeholder);
    in_cal *placeholder_callsite(var_sym *the_placeholder);
    operand solidify(operand to_solidify, tree_node *place);
    void save_placeholders(void);
  };

#endif /* IST_H */
