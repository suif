/* file "main.cc" of the parfeedback program for SUIF */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for parfeedback.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <runtime_names.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/07/07 05:09:37 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

struct feedback_entry
  {
    char *name;
    i_integer max_procs;

    feedback_entry(void) : name(NULL)  { }
  };

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

feedback_entry *feedback_data = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void read_feedback_data(FILE *feedback_fp);
static void do_proc(tree_proc *the_proc);
static void feedback_on_object(suif_object *the_object);
static i_integer max_procs_for_name(char *the_name);
static boolean attempt_to_sequentialize_doall(in_cal *doall_call);
static proc_sym *psym_for_limited_doall(void);
static boolean is_mark_node(tree_node *the_node);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    static char *feedback_file_name = NULL;
    static cmd_line_option option_table[] =
      {
        {CLO_STRING, "-feedback-data", NULL, &feedback_file_name}
      };

    start_suif(argc, argv);

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    if (feedback_file_name == NULL)
        error_line(1, NULL, "no feedback file name given");

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    fileset->set_max_open_files(fileset->get_max_open_files() - 1);

    FILE *feedback_fp = fopen(feedback_file_name, "r");
    if (feedback_fp == NULL)
      {
        error_line(1, NULL, "unable to open file \"%s\" for input",
                   feedback_file_name);
      }

    read_feedback_data(feedback_fp);
    fclose(feedback_fp);

    fileset->set_max_open_files(fileset->get_max_open_files() + 1);

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            if (!this_proc_sym->is_readable())
                continue;
            this_proc_sym->read_proc(TRUE, FALSE);
            do_proc(this_proc_sym->block());
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr,
            "usage: %s -feedback-data <feedback-file> <infile> <outfile> "
            "{ <infile> <outfile> }*\n", _suif_prog_base_name);
    exit(1);
  }

static void read_feedback_data(FILE *feedback_fp)
  {
    unsigned long table_size = 30;
    unsigned long current_position = 0;
    feedback_data = new feedback_entry[30];
    while (!feof(feedback_fp))
      {
        char test_char;
        int count = fscanf(feedback_fp, "%c", &test_char);
        if ((count == EOF) && feof(feedback_fp))
            break;
        if ((count != 1) || (test_char != 'B'))
            error_line(1, NULL, "format error in data file");
        count = fscanf(feedback_fp, "ESTPAR(%c", &test_char);
        if ((count != 1) || (test_char != '"'))
            error_line(1, NULL, "format error in data file");
        unsigned long name_length = 30;
        unsigned long name_position = 0;
        char *this_name = new char[name_length];
        while (TRUE)
          {
            int inchar = fgetc(feedback_fp);
            if (inchar == EOF)
                error_line(1, NULL, "format error in data file");
            if (inchar == '"')
                break;
            this_name[name_position] = inchar;
            ++name_position;
            if (name_position == name_length)
              {
                char *new_string = new char[name_length * 2];
                memcpy(new_string, this_name, name_length);
                delete[] this_name;
                this_name = new_string;
                name_length *= 2;
              }
          }
        this_name[name_position] = 0;
        feedback_data[current_position].name = this_name;

        int inchar = fgetc(feedback_fp);
        if (inchar != ',')
            error_line(1, NULL, "format error in data file");
        inchar = fgetc(feedback_fp);
        if (inchar != ' ')
            error_line(1, NULL, "format error in data file");
        inchar = fgetc(feedback_fp);
        if ((inchar < '0') || (inchar > '9'))
            error_line(1, NULL, "format error in data file");
        i_integer max_procs = inchar - '0';
        while (TRUE)
          {
            inchar = fgetc(feedback_fp);
            if (inchar == ')')
                break;
            if ((inchar < '0') || (inchar > '9'))
                error_line(1, NULL, "format error in data file");
            max_procs = (max_procs * 10) + (inchar - '0');
          }
        feedback_data[current_position].max_procs = max_procs;
        inchar = fgetc(feedback_fp);
        if (inchar != '\n')
            error_line(1, NULL, "format error in data file");
        ++current_position;
        if (current_position == table_size)
          {
            feedback_entry *new_table = new feedback_entry[table_size * 2];
            memcpy(new_table, feedback_data, table_size);
            delete[] feedback_data;
            feedback_data = new_table;
            table_size *= 2;
          }
      }
  }

static void do_proc(tree_proc *the_proc)
  {
    walk(the_proc, &feedback_on_object);
  }

static void feedback_on_object(suif_object *the_object)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_cal)
        return;
    in_cal *the_call = (in_cal *)the_instr;
    proc_sym *the_psym = proc_for_call(the_call);
    if (the_psym == NULL)
        return;
    if (strcmp(the_psym->name(), DOALL_NAME) != 0)
        return;
    if (the_call->num_args() != 3)
        error_line(1, NULL, "bad format for " DOALL_NAME "() call");
    operand arg_op = the_call->argument(2);
    while (arg_op.is_instr() &&
           ((arg_op.instr()->opcode() == io_cpy) ||
            (arg_op.instr()->opcode() == io_cvt)))
      {
        in_rrr *the_rrr = (in_rrr *)(arg_op.instr());
        if ((!the_rrr->result_type()->is_ptr()) ||
            (!the_rrr->src_op().type()->is_ptr()))
          {
            break;
          }
        arg_op = the_rrr->src_op();
      }
    if (!arg_op.is_instr())
        return;
    instruction *arg_instr = arg_op.instr();
    if (arg_instr->opcode() != io_ldc)
        return;
    in_ldc *the_ldc = (in_ldc *)arg_instr;
    immed value = the_ldc->value();
    if (!value.is_symbol())
        return;
    if (value.offset() != 0)
        return;
    sym_node *the_sym = value.symbol();
    if (!the_sym->is_var())
        return;
    var_sym *the_var = (var_sym *)the_sym;
    if (!the_var->has_var_def())
        return;
    var_def *the_def = the_var->definition();
    assert(the_def != NULL);
    char *name_string = string_from_init_data(the_def);
    if (name_string == NULL)
        return;

    i_integer max_procs = max_procs_for_name(name_string);
    delete[] name_string;
    if (max_procs.is_undetermined())
        return;

    if (max_procs == 1)
      {
        boolean sequentialized = attempt_to_sequentialize_doall(the_call);
        if (sequentialized)
            return;
      }

    arg_op = the_call->argument(2);
    arg_op.remove();
    kill_op(arg_op);
    the_call->set_argument(2, const_op(max_procs, type_signed));

    operand old_addr_op = the_call->addr_op();
    old_addr_op.remove();
    kill_op(old_addr_op);
    the_call->set_addr_op(addr_op(psym_for_limited_doall()));
  }

static i_integer max_procs_for_name(char *the_name)
  {
    assert(feedback_data != NULL);
    feedback_entry *follow = feedback_data;
    while (follow->name != NULL)
      {
        if (strcmp(follow->name, the_name) == 0)
            return follow->max_procs;
        ++follow;
      }
    return i_integer();
  }

static boolean attempt_to_sequentialize_doall(in_cal *doall_call)
  {
    tree_node *owner = doall_call->owner();
    if (owner == NULL)
        return FALSE;
    tree_node_list *parent_list = owner->parent();
    if (parent_list == NULL)
        return FALSE;
    tree_node *grandparent_node = parent_list->parent();
    if ((grandparent_node == NULL) || (!grandparent_node->is_if()))
        return FALSE;
    tree_if *grandparent_if = (tree_if *)grandparent_node;
    if (grandparent_if->else_part() != parent_list)
        return FALSE;
    tree_node_list_e *previous_e = grandparent_if->list_e();
    if (previous_e == NULL)
        return FALSE;
    previous_e = previous_e->prev();
    while ((previous_e != NULL) && is_mark_node(previous_e->contents))
        previous_e = previous_e->prev();
    if (previous_e == NULL)
        return FALSE;
    tree_node *previous_node = previous_e->contents;
    if (!previous_node->is_instr())
        return FALSE;
    tree_instr *previous_tree_instr = (tree_instr *)previous_node;
    instruction *previous_instr = previous_tree_instr->instr();
    if (previous_instr->opcode() != io_cal)
        return FALSE;
    in_cal *prev_cal = (in_cal *)previous_instr;
    proc_sym *prev_proc = proc_for_call(prev_cal);
    if (prev_proc == NULL)
        return FALSE;
    if (strcmp(prev_proc->name(), DOALL_LEVEL_NAME) != 0)
        return FALSE;
    operand dest_op = prev_cal->dst_op();
    if (!dest_op.is_symbol())
        return FALSE;
    instruction *new_ldc = new in_ldc(dest_op.type(), dest_op, immed(1));
    previous_tree_instr->remove_instr(previous_instr);
    delete previous_instr;
    previous_tree_instr->set_instr(new_ldc);
    return TRUE;
  }

static proc_sym *psym_for_limited_doall(void)
  {
    sym_node *existing_sym =
            fileset->globals()->lookup_sym(DOALL_LIMITED, SYM_LABEL, FALSE);
    if (existing_sym != NULL)
      {
        error_line(1, existing_sym,
                   "conflicting global symbol for " DOALL_LIMITED "()");
      }
    existing_sym =
            fileset->globals()->lookup_sym(DOALL_LIMITED, SYM_VAR, FALSE);
    if (existing_sym != NULL)
      {
        error_line(1, existing_sym,
                   "conflicting global symbol for " DOALL_LIMITED "()");
      }
    existing_sym =
            fileset->globals()->lookup_sym(DOALL_LIMITED, SYM_PROC, FALSE);
    if (existing_sym != NULL)
      {
        assert(existing_sym->is_proc());
        return (proc_sym *)existing_sym;
      }

    type_node *task_func_type =
            new func_type(type_void, type_signed, type_ptr);
    task_func_type = fileset->globals()->install_type(task_func_type);
    func_type *new_type =
            new func_type(type_void, task_func_type->ptr_to(), type_ptr,
                          type_signed);
    new_type = (func_type *)(fileset->globals()->install_type(new_type));
    return fileset->globals()->new_proc(new_type, src_unknown, DOALL_LIMITED);
  }

static boolean is_mark_node(tree_node *the_node)
  {
    if (!the_node->is_instr())
        return FALSE;
    tree_instr *the_tree_instr = (tree_instr *)the_node;
    return (the_tree_instr->instr()->opcode() == io_mrk);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
