/*  Call Graph Nodes */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include "suif_copyright.h"

#ifndef CG_NODE_H
#define CG_NODE_H

#pragma interface

RCS_HEADER(cnode_h,"$Id: cnode.h,v 1.1.1.1 1998/07/07 05:11:15 brm Exp $")


#include <suif1.h>

/*
 *  Each call graph node contains only an ID number and information
 *  related to the call graph.  Other information can be associated with
 *  a node by storing it in a separate array where the node ID number
 *  is used to index the array.
 */

class cg_node;

DECLARE_LIST_CLASSES(cg_node_list_base, cg_node_list_e, 
		     cg_node_list_iter, cg_node*);

class cg_node_list : public cg_node_list_base {
friend class cg;
friend class cg_node;
private:
    void toposort_helper(cg_node_list *l,bit_set *mark,boolean reverse=FALSE);

public:
    void remove_node(cg_node *n);
    void print(FILE *fp = stdout);
};

class cg_node {
friend class cg;
friend class cg_node_list;

private:
    unsigned nnum;			// node number 
    cg *par;				// parent call graph
    cg_node_list prs;			// predecessors
    cg_node_list scs;			// successors 
    proc_sym *psym;                     // proc_sym this node represents 
    int unknown;                        // number of calls to unknown procs

    void toposort_helper(cg_node_list *l,bit_set *mark,boolean reverse=FALSE);
    void set_number(unsigned n)		{ nnum = n; }
    void set_parent(cg *p)		{ par = p; }

public:
    cg_node(proc_sym *ps);
    ~cg_node();

    unsigned number()			{ return nnum; }
    cg *parent()			{ return par; }
    proc_sym *suif_proc()               { return psym; }
    int unknown_callees()               { return unknown; }
  
    cg_node_list *preds()		{ return &prs; }
    cg_node_list *succs()		{ return &scs; }
    void inc_unknowns(int val = 1)      { unknown += val; }

    void add_pred(cg_node *n);
    void add_succ(cg_node *n);
    void remove_pred(cg_node *n);
    void remove_succ(cg_node *n);

    void toposort(cg_node_list *l, boolean reverse = FALSE);

    void print(FILE *fp=stdout);
};

#endif /* CG_NODE_H */











