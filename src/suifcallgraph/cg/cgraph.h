/*  Call Graph */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef CG_GRAPH_H
#define CG_GRAPH_H

#pragma interface

RCS_HEADER(cgraph_h,"$Id: cgraph.h,v 1.1.1.1 1998/07/07 05:11:15 brm Exp $")

#include <suif1.h>

class cg_node;
class cg_node_list;

/*  This code builds a call graph for the current fileset.  The nodes
 *  of the call graph have pointers to the proc_sym's of the procedures 
 *  they represent.  As the call graph is built, each procedure is
 *  read in and then flushed from memory.  If "include_externs" is TRUE
 *  then the graph will include nodes for external procedures 
 *  (these procedures will be leaves in the call graph since we can't
 *  scan them to see if they call any other procedures).  Recursive calls
 *  are represented in the graph, thus the graph may have cycles.
 */

DECLARE_X_ARRAY(cg_node_array, cg_node, 10);

typedef void (*cg_map_f)(cg_node *n, void *x);

class cg {
friend class cg_node;

private:
    cg_node_array *nds;		        // xarray of nodes 
    cg_node *main;			// main program, first node in graph
    boolean ext;

    // methods for building the graph 
    void build_call_graph();
    void process_procedure(proc_sym *ps);

public:
    cg(boolean include_externs = FALSE);
    ~cg();

    unsigned num_nodes();
    cg_node *node(unsigned i)		  { return (*nds)[i]; }
    void set_node(unsigned i, cg_node *n) { (*nds)[i] = n; }
    cg_node *&operator[](unsigned i)	  { return (*nds)[i]; }

    cg_node *main_node()		  { return main; }
    boolean externs_included()            { return ext; }

    void add_node(cg_node *n);
    void set_main_node(cg_node *n)	  { main = n; }

    void map(cg_map_f f, void *x,
	     boolean bottom_up = TRUE,    // otherwise top-down
	     cg_node *start_node = NULL); // if NULL, start @ "main"
    void print(FILE *fp = stdout);
};


//  initialization and finalization functions 
//
void init_cg(int &argc, char * argv[]);
void exit_cg();

// Annotations 
extern const char *k_cg_node;

#endif /* CG_GRAPH_H */
