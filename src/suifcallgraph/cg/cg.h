/*  Top-level Call Graph Include File */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include "suif_copyright.h"

#ifndef CG_H
#define CG_H

RCS_HEADER(cg_h, "$Id: cg.h,v 1.1.1.1 1998/07/07 05:11:15 brm Exp $")

/*
 *  Use a macro to include files so that they can be treated differently
 *  when compiling the library than when compiling an application.
 */

#ifdef CGLIB
#define CGINCLFILE(F) #F
#else
#define CGINCLFILE(F) <cg/F>
#endif

#include CGINCLFILE(cgraph.h)
#include CGINCLFILE(cnode.h)

#endif /* CG_H */
