/*  Call Graph Implementation */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include "suif_copyright.h"

#define _MODULE_ "libcg.a"

#pragma implementation "cgraph.h"

#define RCS_BASE_FILE cgraph_cc

#include <string.h>

#include <suif1.h>
#include <useful.h>
#include <annotes.h>
#include "cg.h"

RCS_BASE("$Id: cgraph.cc,v 1.2 1999/08/25 03:30:35 brm Exp $")

#define SMALL_STRING_SIZE  40

/* ------------------------------------------------------------------------ 
       External variable declarations
   ------------------------------------------------------------------------ */

const char *k_cg_node;


/* ------------------------------------------------------------------------ 
       Local functions declarations
   ------------------------------------------------------------------------ */

static cg_node *get_cg_node(cg *call_graph, proc_sym *ps);
static void find_call_tree(tree_node *n, void *x);
static void find_call_instr(instruction *i, void *x);
static  sym_node_list *find_procs_for_call(in_cal *cal, int *num_unknowns);

/* ------------------------------------------------------------------------ 
       Class Implementations
   ------------------------------------------------------------------------ */

//===========
//
// Class cg
//
//===========

    /* Public methods */

cg::cg(boolean include_externs)
{
    nds = new cg_node_array;
    set_main_node(NULL);
    ext = include_externs;

    build_call_graph();
}


cg::~cg()
{
    for (unsigned i = 0; i < num_nodes(); i++) {
	delete (*nds)[i];
    }
    delete nds;
}


unsigned cg::num_nodes()
{
    int n = nds->ub();
    return (unsigned) n;
}


void cg::add_node(cg_node *n)
{
    int num = nds->extend((void *)n);
    n->set_number((unsigned)num);
    n->set_parent(this);
}


void cg::print(FILE *fp)
{
    fprintf(fp, "** Call graph **\n");
    for (unsigned i = 0; i < num_nodes(); i++) {
	node(i)->print(fp);
	fputs("\n", fp);
    }
}

/*****************************************************************************/
//
// Iterate over the nodes in the call graph, in either pre-order or post-order
//

void cg::map(cg_map_f f, void *x, boolean bottom_up, cg_node *start_node)
{
    cg_node *first_node = start_node;
    if (!first_node) first_node = main_node();
    assert_msg(first_node, ("No starting node found in cg::map"));

    // Put nodes into topological (top-down) or 
    // reverse-topological (bottom-up) list
    //
    cg_node_list *topolist = new cg_node_list;
    first_node->toposort(topolist, bottom_up);

    // Now call "f" on nodes in sorted list
    cg_node_list_iter node_iter(topolist);
    while (!node_iter.is_empty())  f(node_iter.step(), x);
}



/*****************************************************************************/

    /* Private methods */

//
// Build a new call graph.  
//

void cg::build_call_graph() 
{
    fileset->reset_iter();
    file_set_entry *fse;

    while ((fse = fileset->next_file())) {
	fse->reset_proc_iter();
	proc_sym *ps;

	while ((ps = fse->next_proc())) {
	    if (ps->is_in_memory()) 
	        process_procedure(ps);
	    
	    else if (ps->is_readable()) {

		if (ps->src_lang() == src_fortran)
		    ps->read_proc(TRUE, TRUE);  // exp-trees,call-by-ref=TRUE
		else 
		    ps->read_proc(TRUE, FALSE); // exp-trees=T,call-by-ref=F

		process_procedure(ps);
		ps->flush_proc();
	    }
        }
    }
}


void cg::process_procedure(proc_sym *ps)
{
    cg_node *curr_node = get_cg_node(this, ps);

    // Check if this is the main procedure
    //
    if ((strcmp(ps->name(), "main") == 0) ||
	 strcmp(ps->name(), "MAIN__") == 0) {
	
	assert_msg(!main_node(), 
	    ("Already found main procedure before scanning %s", ps->name()));
	set_main_node(curr_node);
    }

    // Traverse the procedure looking for calls:
    //
    ps->block()->body()->map(find_call_tree, curr_node);
}


	
/* ------------------------------------------------------------------------ 
       Local functions implementation
   ------------------------------------------------------------------------ */

//
// Get the cg_node associated with "ps".  Check if a cg_node has already
// been created by looking for a "k_cg_node" annotation on "ps".  Otherwise,
// create the node and add it to "call_graph".
//
static cg_node *get_cg_node(cg *call_graph, proc_sym *ps)
{
    cg_node *curr_node = (cg_node *) ps->peek_annote(k_cg_node);

    if (!curr_node) {  // Create the node
	curr_node = new cg_node(ps);
	call_graph->add_node(curr_node);
	ps->append_annote(k_cg_node, (void *) curr_node);
    }

    return (curr_node);
}


static void find_call_tree(tree_node *n, void *x)
{
    if (n->is_instr()) {
	tree_instr *ti = (tree_instr *) n;
	ti->instr_map(find_call_instr, x);
    }
}


static void find_call_instr(instruction *i, void *x)
{
    if (i->opcode() != io_cal) return;

    cg_node *caller_node = (cg_node *) x;
    cg *the_graph = caller_node->parent();
    in_cal *cal = (in_cal *) i;

    int num_unknowns = 0;
    sym_node_list *callee_sym_list = find_procs_for_call(cal, &num_unknowns);

    sym_node_list_iter snli(callee_sym_list);
    while (!snli.is_empty()) {
        sym_node *sn = snli.step();
	assert(sn->is_proc());
	proc_sym *callee_sym = (proc_sym *) sn;

	// Get the callee's call graph node and install pred/succ links
	//
	if (callee_sym->is_readable() || the_graph->externs_included()) {
	    cg_node *callee_node = get_cg_node(the_graph, callee_sym);
	    caller_node->add_succ(callee_node);
	}
    }

    caller_node->inc_unknowns(num_unknowns);
}

static  sym_node_list *find_procs_for_call(in_cal *cal, int *num_unknowns)
{
    annote *an;
    w_obj *call_targ;
    sym_node *the_sym;
    sym_node_list *callee_sym_list = new sym_node_list;
    *num_unknowns = 0;

    annote_list_iter ali(cal->annotes());
    while (!ali.is_empty()) {
        an = ali.step();

	if (an->name() == k_wilbyr_call_tgt) {
	    the_sym = NULL;
	    call_targ = (WILBYR_CALL_TGT(an))->target();
	  
	    if (call_targ->kind() == W_SYM_OBJ) {
		the_sym = call_targ->symbol();
	    } else if (call_targ->kind() == W_NON_VIS) {
		the_sym = call_targ->non_visible()->symbol();
	    }

	    if (the_sym && the_sym->is_proc()) { 
		callee_sym_list->append(the_sym);
	    } else {
		(*num_unknowns)++;
	    }
	}
    }

    // No k_wilbyr_call_tgt annotes, find the call target using the 
    // proc_for_call in useful lib
    
    if ((*num_unknowns == 0) && callee_sym_list->is_empty()) {
	proc_sym *the_callee = proc_for_call(cal);
	if (the_callee) {
	    callee_sym_list->append(the_callee);
	} else {
	    (*num_unknowns)++;
	}
    }

    return callee_sym_list;
}


/* ------------------------------------------------------------------------ 
       External functions implementation
   ------------------------------------------------------------------------ */

void init_cg(int & /* argc */, char * /* argv */ [])
{
    k_cg_node = lexicon->enter("cg_node")->sp;
}


void exit_cg ()
{
}
