/*  Call Graph Node Implementation */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include "suif_copyright.h"

#define _MODULE_ "libcg.a"

#pragma implementation "cnode.h"

#define RCS_BASE_FILE cnode_cc

#include <suif1.h>
#include "cg.h"

RCS_BASE("$Id: cnode.cc,v 1.1.1.1 1998/07/07 05:11:15 brm Exp $")
 
/* ------------------------------------------------------------------------ 
       Local functions declarations
   ------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------ 
       Class Implementations
   ------------------------------------------------------------------------ */

//================
//
// Class cg_node
//
//================


    /* Public Methods */

cg_node::cg_node(proc_sym *ps)
{
    set_number((unsigned)-1);
    set_parent(NULL);

    psym = ps;
    unknown = 0;
}

cg_node::~cg_node()
{
    suif_proc()->get_annote(k_cg_node);
}


void cg_node::add_pred(cg_node *n)
{
    // avoid duplicate edges
    cg_node_list_iter pred_iter(preds());
    while (!pred_iter.is_empty()) {
	cg_node *pred = pred_iter.step();
	if (pred == n) return;
    }

    // add the forward and backward edges
    preds()->append(n);
    n->succs()->append(this);
}


void cg_node::add_succ(cg_node *n)
{
    // avoid duplicate edges 
    cg_node_list_iter succ_iter(succs());
    while (!succ_iter.is_empty()) {
	cg_node *succ = succ_iter.step();
	if (succ == n) return;
    }

    // add the forward and backward edges 
    succs()->append(n);
    n->preds()->append(this);
}


void cg_node::remove_pred(cg_node *n)
{
    preds()->remove_node(n);
    n->succs()->remove_node(this);
}


void cg_node::remove_succ(cg_node *n)
{
    succs()->remove_node(n);
    n->preds()->remove_node(this);
}


void cg_node::toposort(cg_node_list *l, boolean reverse /* = FALSE */)
{
    bit_set mark(0, parent()->num_nodes());
    toposort_helper(l, &mark, reverse);
}


void cg_node::print(FILE *fp)
{
    fprintf(fp, "%s %10s, Node %5u: ", 
        ((parent()->main_node() == this) ? "-->" : "   "),suif_proc()->name(), 
	number());
    fputs("  Succs:", fp);
    succs()->print(fp);
    fputs("  Preds:", fp);
    preds()->print(fp);
}

/* Private methods */

void cg_node::toposort_helper(cg_node_list *l, bit_set *mark,
			      boolean reverse /* = FALSE */)
{
    mark->add(number());
    succs()->toposort_helper(l, mark, reverse);

    if (reverse) l->append(this);
    else         l->push(this);
}

//====================
//
// Class cg_node_list
//
//====================

    /* Public methods */

void cg_node_list::remove_node(cg_node *n)
{
    cg_node_list_iter node_iter(this);
    while (!node_iter.is_empty()) {
	cg_node *node = node_iter.step();
	if (node == n) {
	    delete remove(node_iter.cur_elem());
	    return;
	}
    }
    assert_msg(FALSE, ("remove_node - node '%u' not found", n->number()));
}


void cg_node_list::print(FILE *fp)
{
    cg_node_list_iter cg_iter(this);
    while (!cg_iter.is_empty()) {
	cg_node *n = cg_iter.step();
	fprintf(fp, " %u", n->number());
    }
}


/* Private methods */

void cg_node_list::toposort_helper(cg_node_list *l, bit_set *mark, 
				   boolean reverse /* = FALSE */)
{
    cg_node_list_iter node_iter(this);
    while (!node_iter.is_empty()) {
	cg_node *node = node_iter.step();
	if (!mark->contains(node->number())) 
	    node->toposort_helper(l, mark, reverse);
    }
}


