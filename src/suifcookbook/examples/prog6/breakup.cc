#include <suif1.h>
#include <stdlib.h>
#include <suifmath.h>
#include <builder.h>
#include <dependence.h>
#include "predep.h"

/***************************************************************************
 *************************************************************************** 
 ****   Second Pass                                                      ***
 *************************************************************************** 
 ***************************************************************************/



/***************************************************************************
 * Go through  the tree node list, if any tree_node in the list is have an *
 * duplicate annotation (with a symbolic coefficient variable), duplicate  *
 * the body three times, create a construct such that each body will       *
 * represent symconst==0, symconst>0 and symconst<0.                       *
 ***************************************************************************/
void depset::break_loops(tree_node_list * tnl)
{
    tree_node_list_iter iter(tnl);
    // iterate over tree node list
    while(!iter.is_empty()) {
        tree_node * tn = iter.step();
        immed_list * il = (immed_list *)tn->get_annote(k_break_up);
        // if a break_up annotation exist and the annotation has at least
        // one symconst entry in it?
        if(il && (il->count())) {
            // take the first symbolic coefficient variable and put the rest back
            immed im(il->pop());
            tn->prepend_annote(k_break_up, il); 
            vs = (var_sym *)im.symbol();
            
            // the scope of the current tree_node
            base_symtab * bs = tn->scope();
            assert(bs->is_block());
            
            // use builder (blocks) to create the resulting code
            // triplicate the tree_node (0, 1, -1 is the sign of symconst
            block bl_zero(mk_blk(bs, tn, 0));
            block bl_pos(mk_blk(bs, tn, 1));
            block bl_neg(mk_blk(bs, tn, -1));

            // placeholder for symbolic coefficient variable (vs)
            block var(vs);
            // the conditional that triplicate the tree_node into
            // symconst==0, symconst>0 and symconst<0
            block code(block::IF(var == block(0),
                                 bl_zero,
                                 block::IF(var > block(0),
                                           bl_pos,
                                           bl_neg))); 
            
            // create suif code out of the builder structure
            tree_node * new_tn = code.make_tree_node((block_symtab *)bs);
            
            // insert the suif code just before the current block.
            // read suif/glist.h to understand how to do this insertion
            // into the list 
            tnl->insert_after(new_tn, iter.cur_elem());

            // remove the old tree_node (now it is in there from the 
            // above structure); 
            tnl->remove(iter.cur_elem());
            
            delete tn;
            
            // recusively go through the newly created structure (because
            // the iterator will not do it)
            break_loops(new_tn);
            
        } else 
            break_loops(tn);    // no break-up found
    }
}


/***************************************************************************
 * Recursively go through the structured control flow and perform break-up *
 ***************************************************************************/
void depset::break_loops(tree_node * tn)
{
    switch(tn->kind()) {
    case TREE_FOR: {
        tree_for * tnf = (tree_for *)tn;
        break_loops(tnf->body());
        break;
      }

    case TREE_IF: {
        tree_if * tni = (tree_if *)tn;
        break_loops(tni->then_part());
        break_loops(tni->else_part());
        break;
      }

    case TREE_LOOP: {
        tree_loop * tnl = (tree_loop *)tn;
        break_loops(tnl->body());
        break;
      }

    case TREE_BLOCK: {
        tree_block * tnb = (tree_block *)tn;
        break_loops(tnb->body());
        break;
      }

    case TREE_INSTR:
        break;

    default:
        assert(0);
        break;
    }
}



/***************************************************************************
 * Create a new tree_node (in the builder's block form) using the the      *
 * tree_node.  The base symtab (bs) indicate where that block will be      *
 * inserted in the original code (this is needed to do cloneing of symbol  *
 * tables, symbols and types etc.)                                         *
 * Mode will indicate if the resulting block will represent (vs==0),       *
 * (vs>0) or (vs<0).                                                       *
 * A new variable is created to reflect this fact, and the old vs is       *
 * substituted by this new variable.                                       *
 * When vs==0 vs is subsituted by 0, otherwise it is substitiuted by the   *
 * new variable which is always >0                                         *
 ***************************************************************************/
block depset::mk_blk(base_symtab * bs, tree_node * tn, int mode)
                     
{
    char nm[128];
    // name of the new variable
    sprintf(nm, "%s_%s",vs->name(), (mode)?((mode>0)?"pos":"neg"):"zero");
    // create a new variable
    block n_s(block::new_sym(cst_int, nm));
    // placeholder for the old symconst variable
    block o_s(vs);
    // two new blocks
    block assign;
    block replace;

    // according to the mode create the replacement for symconst and
    // the initialization of the new variable. Note that assign.set()
    // is used insted of assign =  because (block = block) will create  
    // a new block that represents a C equal operator(i.e. = is overloaded)
    if(mode == 0) {
        assign.set(n_s = block(0));
        replace.set(block(0));
    } else if(mode > 0) {
        assign.set(n_s = o_s);
        replace.set(block(1)*n_s);
    } else {
        assign.set(n_s = block(-1)*o_s);
        replace.set(block(-1)*n_s);
    }

    // create the suif code for replacement and set the repl operand to 
    // the resulting instruction.
    instruction * ins = replace.make_instruction((block_symtab *)bs);
    repl.set_instr(ins);

    // clone the tree_node and replace all the sym consts's in the 
    // new tree node with the replacement
    tree_node * tnx = tn->clone(bs);
    tnx->map(each_tree_node, this);

    // add the initialization of the new variable
    block code(block::statement(assign, block(tnx, FALSE)));
    return code;
}
                             

/***************************************************************************
 * Map function for structured control flow, when a tree instruction is    *
 * found, call replace_var().                                              *
 ***************************************************************************/
void depset::each_tree_node(tree_node * tn, void * x)
{
    depset * ds = (depset *) x;
    if(tn->kind() == TREE_INSTR) {
        tree_instr * ti = (tree_instr *)tn;
        instruction * inst = ti->instr();
        ds->replace_var(inst);
    }
}


/***************************************************************************
 * Recurse over the expression tree. Check if any operand is a symbolic    *
 * coefficient variable, if so replace it with the repl operand.           *
 ***************************************************************************/
void depset::replace_var(instruction * ins)
{
    for(unsigned i=0; i<ins->num_srcs(); i++) {
        operand op(ins->src_op(i));
        if(op.is_instr())
            replace_var(op.instr());
        else if(op.is_symbol() && (op.symbol() == vs)) {   
            op.remove();
            ins->set_src_op(i, repl.clone());
        }
    }
}    

