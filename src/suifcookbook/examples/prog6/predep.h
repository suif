/**************************************************************************
 ***                                                                    ***
 *** Support for two passes.                                            ***
 ***                                                                    ***
 *** 1) Identify array accesses that have symbolic coefficients         ***
 ***                                                                    ***
 *** 2) Duplicate the loop nests such that in each nest the symbolic    ***
 ***    coefficient can only be >0, <0 or ==0.                          ***
 **************************************************************************/
class depset {
    var_sym * vs;               // place-holders used in a map function
    operand repl;
public: 
    depset()                                            {       }

    // For the first pass
    void find_and_mark(tree_node_list * tnl);
    void find_and_mark(tree_node * tn);
    void find_and_mark(instruction * ins);
    void find_and_mark(in_array *, int i);
    void find_and_mark(tree_node * n, in_array * ia, var_sym * v);

    void update_annotation(const char * annote, suif_object * sa, var_sym * v);

    // For the Second pass
    void break_loops(tree_node_list * tnl);
    void break_loops(tree_node * tn);
    void break_loops(instruction * ins);
    void replace_var(instruction * ins);
    static void each_tree_node(tree_node * tn, void * x);
    block mk_blk(base_symtab * bs, tree_node * tn, int mode);
};


extern const char * k_break_up;     // local annotation to communicate between passes
extern const char * k_depset_done;  // write-back annotation to inform dependence lib
extern const char * k_symconst_ok;  // write-back the list of symbolic coefficients 

