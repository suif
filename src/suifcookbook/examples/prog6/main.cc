#include <suif1.h>
#include <stdlib.h>
#include <time.h>
#include <dependence.h>
#include <builder.h>
#include "predep.h"

const char * k_break_up;
const char * k_depset_done;
const char * k_symconst_ok;

char * timestamp;


/***************************************************************************
 * process each procedure                                                  *
 ***************************************************************************/
void do_proc(tree_proc * tp)
{
    // identify the current procedure for the builder
    block::set_proc(tp);                     

    immed_list * iml;
    // is depset_is_run annotation already in the input procedure??
    if ((iml = (immed_list *)tp->get_annote(k_depset_done))) {
        // if so print when it has been run.
        fprintf(stderr, 
                " Warnning: the input file has previously "
                "been processed by predep\n");
        immed_list_iter ili(iml);
        while(!ili.is_empty()) 
            fprintf(stderr, "   on %s", ili.step().string());
    } else
        iml = new immed_list;    // empty list

    // add the current timestamp to the list of timestamps
    iml->append(immed(timestamp));
    
    // put the depset_done annotation on tree_proc so that
    // 1) later predep passes will print a warnning message
    // 2) dependence library will activate the test when
    //    symbolic coefficients are present.
    tp->prepend_annote(k_depset_done, iml);

    // Create access vectors, need to be run if dependence library is used
    fill_in_access(tp);

    depset R;

    // First pass, identify the loop nests with symbolic coefficients
    R.find_and_mark(tp->body());
    // Second pass, duplicate those loop nests
    R.break_loops(tp->body());
}


/***************************************************************************
 * Do initialization, and call the proc iterator                           *
 ***************************************************************************/
int main(int argc, char * argv[])
{                
    // suif initializer
    start_suif(argc, argv);
    
    // initialize a local annotation used to communicate between the 2 passes
    ANNOTE(k_break_up, "breakup", FALSE);
    // initilize the written-back annotation to communicate with later passes
    // (running predep again and calls to the dependence library)
    ANNOTE(k_depset_done, "depset_is_run", TRUE);
    // If a symbolic coefficient was processed, write that fact down at the 
    // outermost loopnest to help the dependence checker.
    ANNOTE(k_symconst_ok, "depset_symconst_ok", TRUE);

    // create a time-stamp
    time_t tm;
    tm = time(NULL);
    timestamp = ctime(&tm);

    // iterate over all the procedures in the input files and 
    // produce output files.
    suif_proc_iter(argc, argv, do_proc, TRUE, TRUE, TRUE);
    return 0;
}
