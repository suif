#include <stdlib.h>
#include <suif1.h>

void do_proc(tree_proc * tp)
{
    proc_sym * psym = tp->proc();
    printf("%s \n", psym->name());
}

int main(int argc, char * argv[])
{
    start_suif(argc, argv);
    suif_proc_iter(argc, argv, do_proc);
    return 0;
}
