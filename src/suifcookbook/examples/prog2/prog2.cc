#include <stdlib.h>
#include <suif1.h>

void do_instr(instruction * instr)
{
    if(instr->format() == inf_array) 
        instr->print();

    for(int i=0; i<(int)instr->num_srcs(); i++) {
        operand op(instr->src_op(i));
        if(op.is_instr())
            do_instr(op.instr());
    }
}

void each_tree_node(tree_node * tn, void *)
{
    if(tn->kind() == TREE_INSTR) {
        tree_instr * ti = (tree_instr *)tn;
        instruction * inst = ti->instr();
        do_instr(inst);
    }
}

void do_proc(tree_proc * tp)
{
    proc_sym * psym = tp->proc();
    printf("%s \n", psym->name());
    tp->map(each_tree_node, NULL);
}

int main(int argc, char * argv[])
{
    start_suif(argc, argv);
    suif_proc_iter(argc, argv, do_proc);
    return 0;
}
