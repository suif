#define MAXDEPTH        64

class rename {
    static int unum;
    var_sym * from_name[MAXDEPTH];
    var_sym * to_name[MAXDEPTH];
    int dep;
    boolean kill_ind_var;
public: 
    rename()                            { dep = 0; 
                                          kill_ind_var = FALSE; }
    void rename_for(tree_proc * tp);
    void rename_for(tree_node_list * tnl);
    void rename_for(tree_node * tn);
    void rename_for(instruction * ins);
    sym_node * rename_for(sym_node *);
    void begin_for(tree_for * tf);
    void finish_for(tree_for * tf);
    var_sym * find_map(var_sym *);
};
