#include <stdlib.h>
#include <suif1.h>
#include <suifmath.h>
#include <builder.h>
#include "rename.h"


int rename::unum = 0;


void rename::rename_for(tree_proc * tp)
{
    block::set_proc(tp);
    kill_ind_var = (tp->proc()->src_lang() == src_fortran);
    rename_for(tp->body());
}



/***************************************************************************
 * Recurse through the control flow structure renaming loop indices        *
 ***************************************************************************/
void rename::rename_for(tree_node_list * tnl)
{
    tree_node_list_iter iter(tnl);
    while(!iter.is_empty()) {
        tree_node * tn = iter.step();
        rename_for(tn);
    }
}

void rename::rename_for(tree_node * tn)
{
    switch(tn->kind()) {
    case TREE_FOR: {
        tree_for * tnf = (tree_for *)tn;
//        printf("For loop %s\n", tnf->index()->name());
        rename_for(tnf->lb_list());
        rename_for(tnf->ub_list());
        rename_for(tnf->step_list());
        begin_for(tnf);
        rename_for(tnf->landing_pad());
        rename_for(tnf->body());
        finish_for(tnf);
        break;
      }

    case TREE_IF: {
        tree_if * tni = (tree_if *)tn;
        rename_for(tni->header());
        rename_for(tni->then_part());
        rename_for(tni->else_part());
        break;
      }

    case TREE_LOOP: {
        tree_loop * tnl = (tree_loop *)tn;
        rename_for(tnl->body());
        rename_for(tnl->test());
        break;
      }

    case TREE_BLOCK: {
        tree_block * tnb = (tree_block *)tn;
        rename_for(tnb->body());
        break;
      }

    case TREE_INSTR: {
        tree_instr * tnin = (tree_instr *)tn;
        rename_for(tnin->instr());
        break;
      }

    default:
        assert(0);
        break;
    }
}


void rename::rename_for(instruction * ins)
{
    for(unsigned i=0; i<ins->num_srcs(); i++) {
        operand op(ins->src_op(i));
        if(op.is_instr())
            rename_for(op.instr());
        else if(op.is_symbol()) {
            if(op.symbol()->is_var()) {
                var_sym * map = find_map((var_sym *)op.symbol());
                if(map) ins->set_src_op(i, operand(map));
            }
        }
    }
}

/***************************************************************************
 * Starting a for loop, find variable name to rename the index.            *
 ***************************************************************************/
void rename::begin_for(tree_for * tf)
{
    assert(dep < MAXDEPTH);
    from_name[dep] = tf->index();
    
    tree_proc * tp = tf->proc()->block();
    type_node * tn = tf->index()->type();
    char name[64];
    sprintf(name, "%s_%d", tf->index()->name(), unum++);
    to_name[dep] = (var_sym *)tp->symtab()->new_unique_var(tn, name);
    tf->set_index(to_name[dep]);
    dep++;
}


/***************************************************************************
 * Ending a for loop.  If needed, update the original index to curr. value *
 ***************************************************************************/
void rename::finish_for(tree_for * tf)
{
    if(!kill_ind_var) {
        block bf(from_name[dep-1]);
        block bt(to_name[dep-1]);
        block b(bf = bt);
        tree_node * tn = b.make_tree_node();
        tf->parent()->insert_after(tn, tf->list_e());
    }

    dep--;
    assert(dep >= 0);
}


var_sym * rename::find_map(var_sym *v)
{
    for(int i=0; i<dep; i++)
        if(from_name[i] == v) return to_name[i];
    return NULL;
}

/***************************************************************************
 * process each procedure                                                  *
 ***************************************************************************/
void do_proc(tree_proc * tp)
{
    class rename R;
    R.rename_for(tp);
}


/***************************************************************************
 *                                                                         *
 ***************************************************************************/
int main(int argc, char * argv[])
{
    start_suif(argc, argv);
    suif_proc_iter(argc, argv, do_proc, TRUE, TRUE, TRUE);
    return 0;
}
