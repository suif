/*----------------------------------------------------------------------
 * vsuif.h
 *
 * Visual Suif header file
 *
 */

#ifndef VSUIF_H
#define VSUIF_H

void init_vsuif(void);
void exit_vsuif(void);
void start_vsuif(int argc, char * argv[]);

/* include files */

#ifdef VSUIFLIB
#define VSUIF_INCLFILE(F) #F
#else
#define VSUIF_INCLFILE(F) <vsuif/F>
#endif

#include VSUIF_INCLFILE(suif_vnode.h)
#include VSUIF_INCLFILE(suif_print.h)
#include VSUIF_INCLFILE(suif_utils.h)
#include VSUIF_INCLFILE(suif_event.h)

#include VSUIF_INCLFILE(base_viewer.h)
#include VSUIF_INCLFILE(suif_viewer.h)
#include VSUIF_INCLFILE(info_viewer.h)
#include VSUIF_INCLFILE(src_viewer.h)
#include VSUIF_INCLFILE(prof_viewer.h)
#include VSUIF_INCLFILE(text_viewer.h)
#include VSUIF_INCLFILE(main_window.h)
#include VSUIF_INCLFILE(list_viewer.h)
#include VSUIF_INCLFILE(output_viewer.h)
#include VSUIF_INCLFILE(profile.h)
#include VSUIF_INCLFILE(code_tree.h)
#include VSUIF_INCLFILE(misc_viewers.h)
#include VSUIF_INCLFILE(cg_viewer.h)


#endif
