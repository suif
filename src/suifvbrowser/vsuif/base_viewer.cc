/*-------------------------------------------------------------------
 * base_viewer.cc
 *
 */

#include "base_viewer.h"
#include "includes.h"
#include "suif_types.h"
#include "suif_event.h"
#include <stdlib.h>

/*--------------------------------------------------------------------
 */
static void event_helper(event &e, base_viewer *viewer)
{
  viewer->handle_event(e);
}

/*--------------------------------------------------------------------
 * base_viewer::base_viewer
 */
base_viewer::base_viewer(void)
{
  event_binding = new binding((bfun) &event_helper, this);
  add_event_binding(event_binding, VISUAL_EVENTS | X_EVENTS | SUIF_EVENTS);
}

/*--------------------------------------------------------------------
 * base_viewer::~base_viewer
 *
 */
base_viewer::~base_viewer(void)
{
  remove_event_binding(event_binding);
  delete (event_binding);
}

/*--------------------------------------------------------------------
 * base_viewer::add_close_command
 *
 */
void base_viewer::add_close_command(vmenu *menu, char *parent_menu)
{
  binding *b = new binding((bfun) &do_close_command, this);
  menu->add_command(b, parent_menu, "Close");
}

/*--------------------------------------------------------------------
 * base_viewer::add_close_button
 *
 */
void base_viewer::add_close_button(vbuttonbar *button_bar)
{
  binding *b = new binding((bfun) &do_close_command, this);
  button_bar->add_button(b, "Close");
}

/*--------------------------------------------------------------------
 * base_viewer::do_close_command
 *
 */
void base_viewer::do_close_command(event &, base_viewer *viewer)
{
  viewer->destroy();
}

/*--------------------------------------------------------------------
 * text_base_viewer::text_base_viewer
 */
text_base_viewer::text_base_viewer(void)
{
}

/*--------------------------------------------------------------------
 * text_base_viewer::~text_base_viewer
 *
 */
text_base_viewer::~text_base_viewer(void)
{
  if (menu) delete (menu);
  if (frame) delete (frame);
  if (text) delete (text);
}

/*--------------------------------------------------------------------
 * text_base_viewer::create_window
 */
void text_base_viewer::create_window(void)
{
  inherited::create_window();

  menu = new vmenu(toplevel);
  frame = new vframe(toplevel);
  text = new vtext(frame);
}

/*--------------------------------------------------------------------
 * text_base_viewer::create_window
 */
void text_base_viewer::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case PROP_CHANGE:
    {
      vprop *p = (vprop *) e.get_param();
      text->update_prop(p);
    }
    break;

  default:
    break;
  }
}

/*--------------------------------------------------------------------
 * graph_base_viewer::graph_base_viewer
 */
graph_base_viewer::graph_base_viewer(void)
{

}

/*--------------------------------------------------------------------
 * graph_base_viewer::~graph_base_viewer
 *
 */
graph_base_viewer::~graph_base_viewer(void)
{
  if (menu) delete (menu);
  if (frame) delete (frame);
  if (graph_wdgt) delete (graph_wdgt);
}

/*--------------------------------------------------------------------
 * graph_base_viewer::create_window
 */
void graph_base_viewer::create_window(void)
{
  inherited::create_window();

  menu = new vmenu(toplevel);
  frame = new vframe(toplevel);
  graph_wdgt = new vgraph(frame);
}

/*--------------------------------------------------------------------
 * list_base_viewer::list_base_viewer
 */
list_base_viewer::list_base_viewer(void)
{
  list_title = NULL;
}

/*--------------------------------------------------------------------
 * list_base_viewer::~list_base_viewer
 *
 */
list_base_viewer::~list_base_viewer(void)
{
  if (listbox) delete (listbox);
}

/*--------------------------------------------------------------------
 * list_base_viewer::set_title
 */
void list_base_viewer::set_title(char *s)
{
  list_title = s;
}

/*--------------------------------------------------------------------
 * list_base_viewer::create_window
 */
void list_base_viewer::create_window(void)
{
  inherited::create_window();
  listbox_frame = new vframe(toplevel);
  button_frame = new vframe(toplevel);
  listbox = new vlistbox(listbox_frame, list_title);
  button_bar = new vbuttonbar(button_frame);
}

/*--------------------------------------------------------------------
 * form_base_viewer::form_base_viewer
 */
form_base_viewer::form_base_viewer(void)
{
}

/*--------------------------------------------------------------------
 * form_base_viewer::~form_base_viewer
 *
 */
form_base_viewer::~form_base_viewer(void)
{
  if (form) delete (form);
}

/*--------------------------------------------------------------------
 * form_base_viewer::create_window
 */
void form_base_viewer::create_window(void)
{
  inherited::create_window();

  menu = new vmenu(toplevel);
  form_frame = new vframe(toplevel);
  button_frame = new vframe(toplevel);
  form = new vform(form_frame);
  info_bar = new vmessage(toplevel);
  button_bar = new vbuttonbar(button_frame);
}

/*--------------------------------------------------------------------
 * form_base_viewer::set_info_bar
 */
void form_base_viewer::set_info_bar(char *msg)
{
  info_bar->set_message(msg);
}

/*----------------------------------------------------------------------
 * form_base_viewer::add_immed_list
 */
void form_base_viewer::add_immed_list(immed_list *immeds)
{
  int i = 0;

  immed_list_iter iter(immeds);
  while (!iter.is_empty()) {

    char field_string[20];

    i++;
    sprintf(field_string, "Field %d", i);

    immed imm = iter.step();
    add_immed(imm, field_string);
  }
}

/*----------------------------------------------------------------------
 * form_base_viewer::add_immed
 */
void form_base_viewer::add_immed(immed imm, char *field_name)
{
  const char *type;
  const char *val_string = NULL;
  char val_buffer[100];

  switch (imm.kind()) {
    
  case im_int:			/* integer */
    {
      type = t_int;
      sprintf(val_buffer, "%d", imm.integer());
      val_string = val_buffer;
    }
    break;
    
  case im_string:		/* string */
    {
      type = t_string;
      val_string = imm.string();
    }
    break;
    
  case im_extended_int:		/* extended precision integer */
    {
      type = t_extended_int;
      sprintf(val_buffer, "%ld", imm.long_int());
      val_string = val_buffer;
    }
    break;

  case im_float:		/* float */
    {
      type = t_float;
      sprintf(val_buffer, "%f", imm.flt());
      val_string = val_buffer;
    }
    break;

  case im_extended_float:	/* extended precision float */
    {
      type = t_extended_float;
      val_string = imm.ext_flt();
    }
    break;

  case im_symbol:		/* symbolic address */
    {
      type = t_symbol;
      sym_addr_to_string(val_buffer, imm.addr());
      val_string = val_buffer;
    }
    break;

  case im_type:			/* high-level type */
    {
      type = t_type;
      type_node *typenode = imm.type();
      type_node_to_string(val_buffer, typenode);
      val_string = val_buffer;
    }
    break;

  case im_op:			/* operand */
    {
      type = t_operand;
      operand op = imm.op();
      operand_to_string(val_buffer, op);
      val_string = val_buffer;
    }
    break;

  case im_instr:		/* instruction */
    {
      type = t_instruction;
      instruction *instr = imm.instr();
      instruction_to_string(val_buffer, instr);
      val_string = val_buffer;
    }
    break;

  case im_undef:		/* undefined */
    {
      type = t_unknown;
    }
    break;

  default:
    type = t_unknown;
  }

  form->add_field(field_name, type, val_string ? val_string : "");
}

/*----------------------------------------------------------------------
 * form_base_viewer::get_immed_list
 */
immed_list *form_base_viewer::get_immed_list(suif_object *obj, int
					     first_field_num)
{
  immed_list *immlist = new immed_list;

  int n = form->num_fields();
  for (int i = first_field_num; i < n; i++) {
    char *error_msg;

    immed imm = get_immed(obj, i, error_msg);
    if (error_msg) {
      display_message(this, "%s", error_msg);
      form->focus_field(i);
      delete immlist;
      return (NULL);
    }

    immlist->append(imm);
  }

  return (immlist);
}

/*----------------------------------------------------------------------
 * form_base_viewer::get_immed_list
 */

immed form_base_viewer::get_immed(suif_object *obj, int field_num,
				  char *&error_msg)
{
  error_msg = NULL;
  const char *type = form->get_field_type(field_num);
  char *val = form->get_field_data(field_num);

  assert (val && type);
  
  immed imm;

  if (type == t_int) {
    int intval = string_to_int(val, error_msg);
    if (!error_msg) {
      imm = immed(intval);
    }
    
  } else if (type == t_float) {
    float floatval = string_to_float(val, error_msg);
    if (!error_msg) {
      imm = immed(floatval);
    }
    
  } else if (type == t_string) {
    imm = immed(strdup(val));
    
  } else if (type == t_extended_int) {
    long longval = string_to_extended_int(val, error_msg);
    if (!error_msg) {
      imm = immed(longval);
    }
    
  } else if (type == t_extended_float) {
    imm = immed(im_extended_float, val);
    
  } else if (type == t_symbol) {
    sym_addr addr = string_to_sym_addr(obj, val, error_msg);
    if (!error_msg) {
      imm = immed(addr);
    }
    
  } else if (type == t_type) {
    type_node *type = string_to_type_node(obj, val, error_msg);
    if (!error_msg) {
      imm = immed(type);
    }
    
  } else if (type == t_operand) {
    operand op = string_to_operand(val, error_msg);
    if (!error_msg) {
      imm = immed(op);
    }
    
  } else if (type == t_instruction) {
    instruction *instr = string_to_instruction(val, error_msg);
    if (!error_msg) {
      imm = immed(instr);
    }
    
  } else {
    /* unknown type */
    error_msg = "Cannot handle unknown type";
  }
  
  return imm;
}
