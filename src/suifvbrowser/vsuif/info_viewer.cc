/*-------------------------------------------------------------------
 * info_viewer
 *
 */

#include "info_viewer.h"
#include "src_viewer.h"
#include "suif_vnode.h"
#include "suif_print.h"
#include "suif_event.h"
#include "suif_utils.h"
#include "suif_menu.h"
#include <stdlib.h>

/*--------------------------------------------------------------------
 * info_viewer::info_viewer
 */
info_viewer::info_viewer(void)
{
}

/*--------------------------------------------------------------------
 * info_viewer::~info_viewer
 *
 */
info_viewer::~info_viewer(void)
{
}

/*--------------------------------------------------------------------
 * info_viewer::create_window
 */
void info_viewer::create_window(void)
{
  inherited::create_window();
  text->set_text_wrap(TRUE);

  clear();

  vnode *last_sel = vman->get_selection();
  if (last_sel) {
    view(last_sel);
  }
}

/*--------------------------------------------------------------------
 * info_viewer::refresh
 */
void info_viewer::refresh(void)
{
  /* set up menus */
  menu->remove(ROOT_MENU);
  create_obj_menu();
  create_edit_menu();
  add_std_go_menu(menu);

  vnode *last_sel = vman->get_selection();
  if (last_sel) {
    view(last_sel);
  }
}

/*--------------------------------------------------------------------
 * info_viewer::create_obj_menu
 */
void info_viewer::create_obj_menu(void)
{
  menu->clear("Object");

  base_symtab *symtab = fileset->globals();
  vnode *symtab_vn = create_vnode(symtab);
  binding *b = new binding((bfun2) &do_show_obj_cmd, this, symtab_vn);
  menu->add_command(b, "Object", "Global Symbol Table");

  fileset->reset_iter();
  file_set_entry *fse;
  while ((fse = fileset->next_file())) {

    vnode *vn = create_vnode(fse);

    binding *b = new binding((bfun2) &do_show_obj_cmd, this, vn);

    char buffer[100];
    sprintf(buffer, "File Set Entry:[%s]", fse->name());
    menu->add_command(b, "Object", buffer);
  }

  menu->add_separator("Object");
  add_close_command(menu, "Object");
}

/*--------------------------------------------------------------------
 * info_viewer::create_edit_menu
 */
void info_viewer::create_edit_menu(void)
{
  menu->clear("Edit");
  add_std_edit_menu(menu, "Edit");
}

/*--------------------------------------------------------------------
 * info_viewer::do_show_obj_cmd
 */
void info_viewer::do_show_obj_cmd(event &,
                                  info_viewer * /* viewer */, /* unused */
                                  vnode *vn)
{
  post_event(event(vn, SELECTION));
}

/*--------------------------------------------------------------------
 * event handler
 *
 */
void info_viewer::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case SELECTION:
    {
      if (e.get_source() == text) {
	return;			// ignore local selection
      }
      vnode *vn = e.get_object();
      if (vn == NULL) {
	return;
      }
      view(vn);
    }
    break;

  case INVOCATION:
    {
      if (e.get_source() == text) {
	/* local event */
	vnode *vn = e.get_object();
	if (vn) {
	  view(vn);
	}
      }
    }
    break;

  case CLOSE_FILESET:
  case NEW_FILESET:
    {
      clear();
    }
    break;

  case REFRESH:
  case PROC_MODIFIED:
  case FSE_MODIFIED:
    {
      refresh();
    }

  default:
    break;
  }
}

/*--------------------------------------------------------------------
 * info_viewer::clear
 *
 */
void info_viewer::clear(void)
{
  text->clear();
  fprintf(text->fd(), "No object selected\n");
  text->update();

  /* set up menus */
  menu->remove(ROOT_MENU);
  create_obj_menu();
  create_edit_menu();
  add_std_go_menu(menu);
}

/*--------------------------------------------------------------------
 * info_viewer::view
 *
 */
void info_viewer::view(vnode *vn) 
{
  text->clear();
 
  text->tag_begin(vn);

  FILE *fd = text->fd();
  const char *tag = vn->get_tag();
  text->tag_style(BOLD_BEGIN);
  fprintf(fd, "Object: 0x%p (%s)\n", vn->get_object(), tag);
  text->tag_style(BOLD_END);

  /* properties */
  text->tag_style(BOLD_BEGIN);
  fprintf(fd, "Properties:\n");
  text->tag_style(BOLD_END);

  vprop_list *plist = vn->get_prop_list();
  if (plist) {
    for (vprop_list_e *e = plist->head(); e; e = e->next()) {
      vprop *p = e->contents;
      if (p->name()) {
	char *desc = p->description();
	fprintf(fd, "[%s]: %s\n", p->name(), desc ? desc : "");
      }
    }
  }
  fputc('\n', fd);

  /* suif object */
  if (tag == tag_suif_object) {

    suif_object *obj = (suif_object *) vn->get_object();
    text->tag_style(BOLD_BEGIN);
    fprintf(fd, "Suif object type: ");
    text->tag_style(BOLD_END);
    char *name = info_pr.get_object_name(obj);
    fprintf(fd, "%s\n\n", name);
    
    info_pr.print_suif_object(text, obj);
    fputc('\n', fd);
    text->tag_style(BOLD_BEGIN);
    fprintf(fd, "Annotations:\n");
    text->tag_style(BOLD_END);
    info_pr.print_annotations(text, obj);

  } else if (tag == tag_annote) {

    info_pr.print_annote(text, (annote *) vn->get_object());

  } else if (tag == tag_code_fragment) {

    /* code fragment */
    code_fragment *f = (code_fragment *) vn->get_object();
    tree_node *tn = f->node();
    if (tn) {
      vnode *tn_vn = create_vnode(tn);
      text->tag_begin(tn_vn);
      info_pr.print_tree_node(text, tn, 0, PRINT_FULL);

      fputc('\n', fd);
      text->tag_style(BOLD_BEGIN);
      fprintf(fd, "Annotations:\n");
      text->tag_style(BOLD_END);
      info_pr.print_annotations(text, tn);
      text->tag_end(tn_vn);
    }
  }

  text->tag_end(vn);
  text->update();
}

/*-------------------------------------------------------------------
 * info_printer
 */
void info_printer::print_instruction_info(suif_printer * /* pr */, /* unused */
                                          vtty *text,
					  instruction *instr, int depth,
					  int /* elab */, /* unused */
                                          int * /* en */) /* unused */
{
  FILE *fd = text->fd();

  indent(fd, depth);
  fprintf(fd, "Parent: 0x%p\n", instr->parent());
  indent(fd, depth);
  fprintf(fd, "Owner: 0x%p\n\n", instr->owner());
  instr->print(fd, depth);
}

void info_printer::print_tree_node_info(suif_printer *pr, vtty *text,
					tree_node *tn, int depth,
                                        int /* detail */) /* unused */
{
  FILE *fd = text->fd();

  text->tag_style(BOLD_BEGIN);
  fprintf(fd, "Tree node: ");
  text->tag_style(BOLD_END);
  switch (tn->kind()) {
  case TREE_INSTR:
    {
      instruction *instr = ((tree_instr *) tn)->instr();
      fprintf(fd, "tree_instr\n");
      pr->print_instruction(text, instr, depth+1);
    }
    break;
  case TREE_LOOP:
    {
      fprintf(fd, "tree_loop\n");
    }
    break;
  case TREE_FOR:
    {
      fprintf(fd, "tree_for\n");
    }
    break;
  case TREE_IF:
    {
      fprintf(fd, "tree_if\n");
    }
    break;
  case TREE_BLOCK:
    {
      fprintf(fd, "tree_block\n");
      if (tn->is_proc()) {
	const char *proc_name = ((tree_proc *) tn)->proc()->name();
	text->tag_style(BOLD_BEGIN);
	fprintf(fd, "Procedure: ");
	text->tag_style(BOLD_END);
	fprintf(fd, "%s\n", proc_name);
      }
    }
    break;
  default:
    break;
  }

  info_printer::print_parallel_region(pr, text, tn, depth);
}

/*
 * Print parallel region information
 *
 * Scan for "begin_parallel_region" annotation, and print out all the
 * annotations that appear after that.
 *
 */
void info_printer::print_parallel_region(suif_printer *pr, vtty *text, 
					 tree_node *node, int depth)
{
  /* scan for "begin_parallel_region" */
  const char *k_beg_par_region = lexicon->enter("begin_parallel_region")->sp;
  const char *k_end_par_region = lexicon->enter("begin_parallel_region")->sp;
  const char *k_HPF = lexicon->enter("HPF")->sp;
  const char *k_data_layout = lexicon->enter("data_layout")->sp;

  for (tree_node *tn = node; tn; tn = prev_tree_node(tn)) {
    if (tn->is_instr()) {
      instruction *instr = ((tree_instr *) tn)->instr();
      if (instr->peek_annote(k_beg_par_region)) {

	text->tag_style(BOLD_BEGIN);
	fprintf(text->fd(), "\nParallel region:\n");
	text->tag_style(BOLD_END);

	/* scan for annotations */
	for (; tn && tn != node; tn = next_tree_node(tn)) {
	  if (tn->is_instr()) {
	    instruction *instr = ((tree_instr *) tn)->instr();
	    if (instr->peek_annote(k_HPF) ||
		instr->peek_annote(k_data_layout)) {
	      pr->print_annotations(text, instr, depth+1);
	    }
	  }
	}

	return;
      }
      if (instr->peek_annote(k_end_par_region)) {
	return;
      }
    }
  }
}
