/*----------------------------------------------------------------------
 * main_window.cc
 *
 *
 */

#include "includes.h"
#include "main_window.h"
#include "text_viewer.h"
#include "suif_event.h"
#include "suif_vnode.h"
#include <string.h>

extern void exit_vsuif(void);

char *application_help_text = "No help available.";

/*----------------------------------------------------------------------
 * main_window
 *
 */
main_window::main_window(void)
{
  current_argc = 0;
  current_argv = NULL;
  fileset_modified = FALSE;
}

/*----------------------------------------------------------------------
 * ~main_window
 *
 */
main_window::~main_window(void)
{
  if (fileset_modified &&
      fileset->file_list()->count() > 0) {
    int result = display_dialog(this, 
				"Do you want to save the current file set?",
				"Yes No", 0);
    if (result == 0) {
      save_fileset();
    }
  }

  delete (menu);
  delete (text);

  exit_vsuif();
  exit_visual1();
  exit_suif();
  exit(0);
}

/*----------------------------------------------------------------------
 * main_window::create_window
 *
 */
void main_window::create_window(void)
{
  inherited::create_window();

  menu = new vmenu(toplevel);
  button_frame = new vframe(toplevel);
  text_frame = new vframe(toplevel);
  button_bar = new vbuttonbar(button_frame);
  text = new vtext(text_frame);

  fprintf(text->fd(), "No file set entry\n");
  text->update();

  /*
   * File menu
   */
  binding *b = new binding((bfun) &do_open_cmd, this);
  menu->add_command(b, "File", "Open File Set...");

  b = new binding((bfun) &do_reload_cmd, this);
  menu->add_command(b, "File", "Reload File Set");

  b = new binding((bfun) &do_save_cmd, this);
  menu->add_command(b, "File", "Save & Close File Set");

  menu->add_separator("File");

  b = new binding((bfun) &do_exit_cmd, this);
  menu->add_command(b, "File", "Exit");

  /*
   * Create list of modules in the "module" menu
   */

  module_list_iter mod_iter (vman->get_module_list());
  while (!mod_iter.is_empty()) {
    module *mod = mod_iter.step();

    char buffer[100];
    sprintf(buffer, "Module/%s", mod->class_name());
    mod->attach_menu(menu, buffer);
    mod->create_menu();
  }

  /* 
   * Create list of viewers in the "window" menu
   */
  window_class_list_iter win_iter (vman->get_window_classes());
  while (!win_iter.is_empty()) {
    window_class *wclass = win_iter.step();

    b = new binding((bfun2) &do_show_window_cmd, this, wclass);
    menu->add_command(b, "Windows", wclass->name());
  }

  /* 
   * Help menu
   */
  b = new binding((bfun) &do_help_cmd, this);
  menu->add_command(b, "Help", "Help!");

  /*
   * Button bar
   */

  window_class *wclass = vman->find_window_class("Callgraph Viewer");
  if (wclass) {
    b = new binding((bfun2) &do_show_window_cmd, this, wclass);
    button_bar->add_button(b, "Call Graph");
  }

  wclass = vman->find_window_class("Source Viewer");
  if (wclass) {
    b = new binding((bfun2) &do_show_window_cmd, this, wclass);
    button_bar->add_button(b, "Source");
  }

  wclass = vman->find_window_class("Suif Viewer");
  if (wclass) {
    b = new binding((bfun2) &do_show_window_cmd, this, wclass);
    button_bar->add_button(b, "Suif");
  }

  wclass = vman->find_window_class("Output Viewer");
  if (wclass) {
    b = new binding((bfun2) &do_show_window_cmd, this, wclass);
    button_bar->add_button(b, "C Output");
  }

  wclass = vman->find_window_class("Info Viewer");
  if (wclass) {
    b = new binding((bfun2) &do_show_window_cmd, this, wclass);
    button_bar->add_button(b, "Info");
  }
}

/*----------------------------------------------------------------------
 * open fileset
 *
 */
void main_window::do_open_cmd(event &, main_window *win)
{
  char *new_fs = select_fileset(win, "Load SUIF file(s):", NULL);
  if (new_fs) {
    const char *argv[100];
    argv[0] = NULL;
    int argc = 1;
    for (char *p = strtok(new_fs, " "); p; p = strtok(NULL, " ")) {
      argv[argc] = p;
      argc++;
      assert(argc < 100);
    }
    argv[argc] = NULL;
    win->browse_fileset(argc, argv);
  }
}

/*----------------------------------------------------------------------
 * reload fileset
 *
 */
void main_window::do_reload_cmd(event &, main_window *win)
{
  const char *argv[100];
  argv[0] = NULL;

  fileset->reset_iter();
  file_set_entry *fse;

  int argc = 1;
  while ((fse = fileset->next_file())) {
    argv[argc] = fse->in_name(); // the fse name should be in lexicon table
    argc++;
  }
  argv[argc] = NULL;
  win->browse_fileset(argc, argv);
}

/*----------------------------------------------------------------------
 * save fileset
 *
 */
void main_window::do_save_cmd(event &, main_window *win)
{
  win->save_fileset();
  win->close_fileset();

  /* broadcast fileset changed */
  post_event(event(NULL, NEW_FILESET));
}

/*----------------------------------------------------------------------
 * do_exit_cmd
 *
 */
void main_window::do_exit_cmd(event &, main_window *win)
{
  win->destroy();
}

/*----------------------------------------------------------------------
 * do_help_cmd
 *
 */
void main_window::do_help_cmd(event &, main_window * /* win */) /* unused */
{
  text_viewer *help = new text_viewer;
  help->create_window();
  help->insert_text(application_help_text);
}

/*----------------------------------------------------------------------
 * do_show_window_cmd
 *
 */
void main_window::do_show_window_cmd(event &,
                                     main_window * /* main_win */, /* unused */
				     window_class *wclass)
{
  vman->show_window(wclass);
}

/*----------------------------------------------------------------------
 * main_window::close_fileset
 */
void main_window::close_fileset(void)
{
  /* reset suif */
  if (fileset) {
    delete fileset;
    fileset = NULL;
   }

  fileset = new file_set;
  fileset_modified = FALSE;
  current_argc = 0;
  current_argv = NULL;

  vman->remove_all_vnodes();

  update_display();

  post_event(event(NULL, CLOSE_FILESET));
}

/*----------------------------------------------------------------------
 * main_window::browse_fileset
 */
void main_window::browse_fileset(int argc, const char * const argv[])
{
  if (fileset_modified &&
      fileset->file_list()->count() > 0) {
    int result = display_dialog(this, 
				"Do you want to save the current file set?",
				"Yes No Cancel", 0);
    if (result == 0) {
      save_fileset();
    } else if (result == 2) {
      return;
    }
  }

  /* reset suif */
  close_fileset();

  if (argc > 0) {

    /* create fileset */
    for (int i = 1; i < argc; i++) {

      if (!argv[i] ||
	  argv[i][0] == '-') {	// command line option
	continue;		
      }
     
      suif_check_result result = check_for_suif_file(argv[i]);
      if (result == SF_NOT_SUIF) {
	display_message(this, "File `%s' is not a SUIF file.",
			argv[i]);
	
      } else if (result == SF_CANT_OPEN) {
	display_message(this, "Cannot open file `%s'.",
			argv[i]);
      } else {
	fileset->add_file(argv[i], NULL);
      }
      
      post_progress(this, "Initializing SUIF file set..",
		    ((float)i)/(argc-1) * 100);
    }
    unpost_progress(this);
    
    update_display();

    /* broadcast new fileset */
    post_event(event(NULL, NEW_FILESET));
  }
}

/*----------------------------------------------------------------------
 * main_window::save_fileset
 */
void main_window::save_fileset(void)
{
  char output_file[200];

  fileset->reset_iter();
  file_set_entry *fse;

  while ((fse = fileset->next_file())) {

    if (!fse->out_name()) {

      // add an output name
      strcpy(output_file, fse->name());
      int i;
      for (i = strlen(output_file)-1; i; i--) {
	if (output_file[i] == '.') {
	  strcpy(&output_file[i], ".sbr");
	  break;
	}
	if (output_file[i] == '/') {
	  strcat(output_file, ".sbr");
	  break;
	}
      }
      if (i == 0) {
	strcat(output_file, ".sbr");
      }
      
      fse->add_outfile(output_file);
    }

    fse->reset_proc_iter();
    proc_sym *ps;
    while ((ps = fse->next_proc())) {
      boolean in_memory = TRUE;
      if (!ps->is_in_memory()) {
	in_memory = FALSE;
	ps->read_proc();
      }
      ps->write_proc(fse);
      if (!in_memory)
	ps->flush_proc();
    }
  }

  int n = fileset->file_list()->count();
  if (n == 1) {
    display_message(this, "File set saved as `%s'", output_file);
  } else if (n > 1) {
    display_message(this, "File set saved as *.sbr");
  }
}

/*----------------------------------------------------------------------
 * main_window::update_display
 */
void main_window::update_display(void)
{
  text->clear();
  FILE *fd = text->fd();

  fprintf(fd, "File Set Entries:\n");

  for (file_set_entry_list_e *e = fileset->file_list()->head();
       e; e = e->next()) {
    file_set_entry *fse = e->contents;

    vnode *vn = create_vnode(fse);
    text->tag_begin(vn);
    fprintf(fd, "[`%-s']\n", fse->name());
    text->tag_end(vn);
  }

  text->update();
}

/*----------------------------------------------------------------------
 * main_window::handle_event
 */
void main_window::handle_event(event &e)
{
  switch (e.kind()) {
  case PROC_MODIFIED:
  case FSE_MODIFIED:
    {
      fileset_modified = TRUE;
    }
    break;

  case SELECTION:
    {
      if (e.get_source() == text) {
	vnode *vn = e.get_object();
	if (vn->get_tag() == tag_suif_object &&
	    ((suif_object *) vn->get_object())->object_kind() == FILE_OBJ) {
	  (void) vman->show_window("Info Viewer");
	}
      }
    }
    break;

  default:
    break;
  }

}
