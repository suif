#ifndef SUIF_VNODE_H
#define SUIF_VNODE_H

/*
 * Suif object vnodes
 *
 * Note:
 * 1) For annote vnode, the aux_data stores the pointer to the parent
 * suif_object of the annote.
 *
 */

#include "includes.h"

void init_vnode_tags(void);

/*
 * Tags of suif vnodes.
 */

extern const char *tag_suif_object;
extern const char *tag_code_fragment;
extern const char *tag_operand;
extern const char *tag_annote;
extern const char *tag_annote_list;
extern const char *tag_sym_addr;

/*
 * Functions to help create vnodes of suif objects.
 */

vnode *create_vnode(suif_object *suifobj);
vnode *create_vnode(annote *ann, suif_object *parent_obj);
vnode *create_vnode(annote_list *annlist, suif_object *parent_obj);

boolean is_suif_object(const vnode *vn);


#endif
