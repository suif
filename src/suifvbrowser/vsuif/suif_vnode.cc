/*-------------------------------------------------------------------
 * suif_vnode.cc
 *
 */

#include "suif_vnode.h"

const char *tag_suif_object;
const char *tag_code_fragment;
const char *tag_annote;
const char *tag_annote_list;

/*-------------------------------------------------------------------
 * init_vnode_tags
 */
void init_vnode_tags(void)
{
  tag_suif_object = lexicon->enter("suif_object")->sp;
  tag_code_fragment = lexicon->enter("code_fragment")->sp;
  tag_annote = lexicon->enter("annote")->sp;
  tag_annote_list = lexicon->enter("annote_list")->sp;
}

/*-------------------------------------------------------------------
 * create_vnode
 */
vnode *create_vnode(suif_object *suifobj)
{
  vnode *vn = vman->find_vnode(suifobj);
  if (!vn) {
    vn = new vnode(suifobj, tag_suif_object);
  } else {

    if (vn->get_tag() != tag_suif_object) {
      assert_msg(FALSE, 
	       ("Internal error. (`%s')", vn->get_tag()));
    }

  }
  return (vn);
}

vnode *create_vnode(annote *ann, suif_object *parent_obj)
{
  vnode *vn = vman->find_vnode(ann);
  if (!vn) {
    vn = new vnode(ann, tag_annote, parent_obj);
  } else {

#if 1
    assert_msg(vn->get_tag() == tag_annote,
	       ("Internal error. (`%s')", vn->get_tag()));
#endif

  }
  return (vn);
}

vnode *create_vnode(annote_list *annlist, suif_object *parent_obj)
{
  vnode *vn = vman->find_vnode(annlist);
  if (!vn) {
    vn = new vnode(annlist, tag_annote_list, parent_obj);
  } else {
    assert(vn->get_tag() == tag_annote_list);
  }
  return (vn);
}

/*-------------------------------------------------------------------
 * is_suif_object
 */
boolean is_suif_object(const vnode *vn)
{
  return (vn && vn->get_tag() == tag_suif_object);
}

