/*-------------------------------------------------------------------
 * code_tree.cc
 *
 */

#include "code_tree.h"
#include <stdlib.h>
#include <limits.h>
#include "suif_utils.h"
#include "suif_vnode.h"

/*--------------------------------------------------------------------
 * code_fragment::code_fragment
 */
code_fragment::code_fragment(tree_node *n)
{
  instrs = new instruction_list;
  son = last_son = next = NULL;
  tn = n;
}

/*--------------------------------------------------------------------
 * code_fragment::~code_fragment
 */
code_fragment::~code_fragment(void)
{
  delete instrs;
  vnode *vn = vman->find_vnode(this);
  if (vn) delete vn;
}

/*--------------------------------------------------------------------
 * code_fragment::add_son
 */
void code_fragment::add_son(code_fragment *f)
{
  if (last_son) {
    last_son->next = f;
  } else {
    son = f;
  }
  last_son = f;
}

/*--------------------------------------------------------------------
 * code_fragment::find_fragment
 *
 * look for a tree node in code_fragment subtree
 */

code_fragment *code_fragment::find_fragment(tree_node *n)
{
  if (tn == n) return this;
  if (n->is_instr()) {
    if (instrs->lookup(((tree_instr *) n)->instr())) {
      return (this);
    }
  }

  for (code_fragment *child = son; child; child = child->next) {
    code_fragment *found = child->find_fragment(n);
    if (found) return (found);
  }
  return (NULL);
}


/*--------------------------------------------------------------------
 * code_fragment::free_subtree(void)
 *
 */
void code_fragment::free_subtree(void)
{
  code_fragment *next_child;
  for (code_fragment *child = son; child; child = next_child) {
    next_child = child->next;
    child->free_subtree();
    delete (child);
  }
  son = last_son = NULL;
}

/*--------------------------------------------------------------------
 * code_tree::code_tree
 */
code_tree::code_tree(void)
{
  root = new code_fragment(NULL);
  map_fn = NULL;
}

/*--------------------------------------------------------------------
 * code_tree::~code_tree
 *
 */
code_tree::~code_tree(void)
{
  clear();
  delete (root);
}

/*--------------------------------------------------------------------
 * code_tree::clear
 *
 */
void code_tree::clear(void)
{
  root->free_subtree();
}

/*--------------------------------------------------------------------
 * code_tree::build
 *
 */
void code_tree::build(tree_proc *proc)
{
  map_to_source(proc, NULL, root);
}

/*--------------------------------------------------------------------
 * code_tree::map_to_source
 *
 * Create the map between the source code and suif nodes
 *
 */
code_fragment *code_tree::map_to_source(tree_node *tn,
					code_fragment *current_f,
					code_fragment *parent_f)
{
  if (tn->is_instr()) {

    /*
     * Instruction node
     *
     * Find the line number. If different from last line number,
     * create a new node.
     */

    /* instruction node */
    instruction *instr = ((tree_instr *) tn)->instr();

#if 1
    if (instr->opcode() == io_mrk) {
      /* ignore it */
      return (current_f);
    }
#endif

    code_range r = (*map_fn)(tn, client_data);
    if (!r.first_line) {
      /* no line number info */
      if (current_f) {
	current_f->instrs->append(instr);
      }
    } else {
      /* line number found */
      if (!current_f || current_f->l_line != r.first_line) {
	current_f = new code_fragment(tn);
	current_f->f_line = r.first_line;
	current_f->l_line = r.last_line;
	parent_f->add_son(current_f);
      }
      current_f->instrs->append(instr);
    }

  } else {

    /*
     * Not a leaf node. Scan children also.
     */

    code_range r = (*map_fn)(tn, client_data);
  
    code_fragment *new_f = new code_fragment(tn);
    if (r.first_line) {
      new_f->f_line = r.first_line;
      new_f->l_line = r.last_line;
    } else {
      new_f->f_line = INT_MAX;
      new_f->l_line = 0;
    }

    /* 
     * Map children
     *
     * Determine the first and start line of current node by taking the
     * min and max lines of the children.
     *
     */
    code_fragment *child_f = NULL;
    code_fragment *prev_child_f = NULL;
    int n = tn->num_child_lists();
    for (int i = 0; i < n; i++) {
      tree_node_list *l = tn->child_list_num(i);

      for (tree_node_list_e *e = l->head(); e; e = e->next()) {
	tree_node *child = e->contents;

	child_f = map_to_source(child, child_f, new_f);
	if (child_f) {
	  if (prev_child_f && prev_child_f != child_f) {
	    /* adjust the last line of the previous child fragment
	     * to make sure no lines are skipped. */
	    prev_child_f->l_line = 
	      MAX(child_f->f_line - 1, prev_child_f->l_line);
	  }
	  new_f->f_line = MIN(new_f->f_line,
				  child_f->f_line);
	  new_f->l_line = MAX(new_f->l_line,
				 child_f->l_line);
	}
	prev_child_f = child_f;
      }
    }

    /*
     * Check code fragment. If it has no line info, then
     * discard this node.
     */
    if (new_f->f_line == INT_MAX) {
      delete (new_f);
    } else {
      switch (tn->kind()) {
	case TREE_FOR:
	    new_f->f_line -= 1;
	    break;
	case TREE_LOOP:
	case TREE_IF:
	case TREE_BLOCK:
	default:
	    break;
      }
      parent_f->add_son(new_f);
      current_f = new_f;
    }
  }

  return (current_f);
}


/*--------------------------------------------------------------------
 * code_tree::lookup
 *
 * lookup the code_fragment that corresponds with a tree node
 */

code_fragment *code_tree::lookup(tree_node *tn)
{
  tree_proc *proc = tn->proc()->block();
  for (code_fragment *proc_f = root->son; proc_f; proc_f = proc_f->next) {
    if (proc_f->tn == proc) {
      /* this is the procedure, now, look for the code_fragment! */
      return (proc_f->find_fragment(tn));
    }
  }
  return (NULL);
}

/*--------------------------------------------------------------------
 * create_tags
 *
 */
void code_tree::create_tags(vtext *text)
{
  for (code_fragment *f = get_root()->son; f; f = f->next) {
    create_tags(f, text->root_tag());
  }
}

void code_tree::create_tags(code_fragment *f, tag_node *parent_tag)
{
  /* create tags */
  assert(f->tn); // this should not be NULL
  vnode *vn = vman->find_vnode(f);
  if (!vn) vn = new vnode(f, tag_code_fragment);

  tag_node *tag = new tag_node;
  tag->set_object(vn);
  tag->set_begin_coord(text_coord(f->f_line, 0));
  tag->set_end_coord(text_coord(f->l_line + 1, 0));
  parent_tag->add_son(tag);

  /* children */
  for (code_fragment *child = f->son; child; child = child->next) {
    create_tags(child, tag);
  }
}

/*--------------------------------------------------------------------
 * print
 *
 */
void code_tree::print(FILE *fd)
{
  for (code_fragment *f = get_root()->son; f; f = f->next) {
    fprintf(fd, "Proc '%s'\n", f->tn->proc()->name());
    print_helper(fd, f, 0);
  }
}

void code_tree::print_helper(FILE *fd, code_fragment *f, int depth)
{
  suif_indent(fd, depth);
  fprintf(fd, "- (first: %d, last: %d, tn = %p)\n",
	  f->f_line, f->l_line, f->tn);

  for (code_fragment *child = f->son; child; child = child->next) {
    print_helper(fd, child, depth + 1);
  }
}

/*--------------------------------------------------------------------
 * create properties on code fragments
 *
 */
void code_tree::suif_to_code_prop(vprop *suif_prop, vprop *code_prop)
{
  code_prop->erase();

  vnode_list_iter iter (suif_prop->get_node_list());
  while (!iter.is_empty()) {
    
    /* find the corresponding code_fragment */
    vnode *vn = iter.step();
    const char *tag = vn->get_tag();
    if (tag == tag_suif_object) {
      suif_object *obj = (suif_object *) vn->get_object();
      tree_node *tn = NULL;
      
      switch (obj->object_kind()) {
      case TREE_OBJ:
	{
	  tn = (tree_node *) obj;
	}
	break;
      case INSTR_OBJ:
	{
	  tn = ((instruction *) obj)->parent();
	}
      default:
	break;
      }

      if (tn) {
	code_fragment *f = lookup(tn);
	if (f) {
	  vnode *vn = vman->find_vnode(f);
	  if (vn) code_prop->add_node(vn);
	}
      }
    }
  } /* end while */
}
