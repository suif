/*-------------------------------------------------------------------
 * proc_viewer
 *
 */

#include "proc_viewer.h"
#include "src_viewer.h"
#include "suif_utils.h"
#include "suif_vnode.h"
#include "suif_print.h"
#include "list_viewer.h"
#include <stdlib.h>

enum { FIND_NODE = 0, FIND_BLOCK, FIND_ANNOTE}; 

static boolean match_block_kind(tree_node *tn, void *client_data);
static boolean match_annote_name(tree_node *tn, void *client_data);
static boolean match_node(tree_node *tn, void *client_data);

/*--------------------------------------------------------------------
 * proc_printer::print_instruction
 *
 */
void proc_printer::print_instruction(vtext *text, instruction *instr,
				int depth)
{
  /* filter away the marks */
  if (filter_mrk && instr->opcode() == io_mrk) {
    return;
  }

  suif_printer::print_instruction(text, instr, depth);
}

/*--------------------------------------------------------------------
 * proc_viewer::proc_viewer
 */
proc_viewer::proc_viewer(void)
{
  current_proc = NULL;

  toplevel = new vtoplevel("Procedure viewer", this);
  menu = new vmenu(toplevel);
  frame = new vframe(toplevel);
  message = new vmessage(toplevel);
  text = new vtext(frame);

  /* events */
  binding *b = new binding;
  bindings->append(b);
  b->set_function((bfun) &global_select_event, this);
  b->attach(SELECTION);

  /* menu */
  item_bindings = new binding_list;
  create_procs_menu();
  create_view_menu();

  /* text */
  highlight = new vprop;
  text->add_prop(highlight);
  b = new binding;
  bindings->append(b);
  b->set_function((bfun) &local_select_event, this);
  text->add_binding(b, SELECTION);

  update_message();
}

/*--------------------------------------------------------------------
 * proc_viewer::~proc_viewer
 *
 */
proc_viewer::~proc_viewer(void)
{
  delete (toplevel);
  delete (frame);
  delete (menu);
  delete (text);
  delete (message);
  delete (highlight);

  delete_bindings(item_bindings);
  delete (item_bindings);
}

/*--------------------------------------------------------------------
 * proc_viewer::create_procs_menu
 *
 */
void proc_viewer::create_procs_menu(void)
{
  menu->clear("Procedure");

  sym_node_list proc_symbols;

  int num_procs = 0;
  fileset->reset_iter();
  while (file_set_entry *fse = fileset->next_file()) {
    fse->reset_proc_iter();
    while (proc_sym *psym = fse->next_proc()) {
      if (psym->is_readable()) {
	proc_symbols.append(psym);
	num_procs++;
	if (num_procs > 10) break;
      }
    }
  }

  if (num_procs <= 10) {
    /* add the procedues to the menu */
    sym_node_list_iter iter(&proc_symbols);
    while (!iter.is_empty()) {
      proc_sym *psym = (proc_sym *) iter.step();
      binding *b = new binding;
      b->set_function((bfun2) &show_proc, this, psym);
      menu->add_command(b, "Procedure", psym->name());
    }
  }
  
  /* add procedure list command to the menu */
  binding *b = new binding;
  b->set_function((bfun) &show_proc_list, this);
  menu->add_command(b, "Procedure", "Procedure list..");
}

/*--------------------------------------------------------------------
 * proc_viewer::show_proc
 *
 */
void proc_viewer::show_proc(event &e, proc_viewer *viewer,
			    proc_sym *psym)
{
  viewer->view(psym);
  vnode *vn = create_vnode(psym);
  if (vn) {
    post_event(event(vn, SELECTION, viewer));
  }
}

/*--------------------------------------------------------------------
 * proc_viewer::show_proc_list
 *
 */
void proc_viewer::show_proc_list(event &e, proc_viewer *viewer)
{
  vman.show_window(vman.find_window_class("Procedure list"));
}

/*--------------------------------------------------------------------
 * proc_viewer::view
 *
 * view a certain line
 */
void proc_viewer::view(int line)
{
  text->view(line, 0);
}

/*--------------------------------------------------------------------
 * proc_viewer::view
 *
 * view a procedure symbol
 */
void proc_viewer::view(proc_sym *psym)
{
  if (!psym->is_in_memory()) {
    psym->read_proc();
  }
  tree_proc *proc = psym->block();
  if (proc) {
    view(proc);
  }
}

/*--------------------------------------------------------------------
 * proc_viewer::view
 *
 * view a procedure
 */
void proc_viewer::view(tree_proc *proc)
{
  if (current_proc != proc) {

    current_proc = proc;

    /* print proc tree */
    text->clear();
    print_proc(proc);

    /* print annotations */
    FILE *fd = text->fd();
    text->tag_style(BOLD_BEGIN);
    fprintf(fd, "\nAnnotations:\n");
    text->tag_style(BOLD_END);
    proc->print_annotes(fd, 2);
    text->update();

    /* update */
    update_message();
    update_find_menu();
  }
}

/*--------------------------------------------------------------------
 * proc_viewer::view
 *
 * view a tree node
 */

tag_node *proc_viewer::show_node(tree_node *tn)
{
  if (!tn->is_proc()) {
    /* make sure its parent is expanded */
    tree_node *par = tn->parent()->parent();
    if (par) {
      tag_node *par_tag = show_node(par);
      if (!par_tag->is_expanded()) {
	text->expand_node(par_tag);
      }
    }
  }

  /* now, get the node */
  vnode *vn = vman.find_vnode(tn);
  tag_node *tag = text->find_tag(vn);
  return (tag);
}


void proc_viewer::view(tree_node *tn)
{
  view(tn->proc());

  /* make sure that the tn is visible */
  tag_node *tag = show_node(tn);
  text->view(tag);
}

/*--------------------------------------------------------------------
 * proc_viewer::view
 *
 * view a code fragment
 */
void proc_viewer::view(code_fragment *f)
{
  tree_node *tn = f->tn;
  view(tn);
}

/*--------------------------------------------------------------------
 * proc_viewer::select
 *
 * select a code fragment
 */
void proc_viewer::select(code_fragment *f)
{
  text->select_clear();

  vnode *vn;
  if (f->tn->is_instr()) {
    instruction_list_iter iter(f->instrs);
    while (!iter.is_empty()) {
      instruction *instr = iter.step();
      vnode *vn = vman.find_vnode(instr->owner());
      text->select_add(text->find_tag(vn));
    }
  } else {
    vn = vman.find_vnode(f->tn);
    text->select(vn);
  }
}

/*--------------------------------------------------------------------
 * proc_viewer::select
 *
 * select a tree node
 */
void proc_viewer::select(tree_node *tn)
{
  text->select_clear();

  vnode *vn = vman.find_vnode(tn);
  text->select(vn);
}

/*--------------------------------------------------------------------
 * proc_viewer::update_message
 *
 */
void proc_viewer::update_message(void)
{
  char *proc_name = current_proc ? current_proc->proc()->name() : "";

  char m[100];
  sprintf(m, "Procedure: %s", proc_name);
  message->set_message(m);
}

/*--------------------------------------------------------------------
 * proc_viewer::create_view_menu
 *
 */
void proc_viewer::create_view_menu(void)
{
  menu->clear("View");

  binding *b = new binding;
  bindings->append(b);
  b->set_function((bfun) &expand_all, this);
  menu->add_command(b, "View", "Expand all");

  b = new binding;
  bindings->append(b);
  b->set_function((bfun) &collapse_all, this);
  menu->add_command(b, "View", "Collapse all");

  menu->add_separator("View");

  b = new binding;
  bindings->append(b);
  b->set_function((bfun) &filter_mrks, this);
  menu->add_check(b, "View/Filter", "mrk");
  menu->invoke("View/Filter", "mrk");
}

void proc_viewer::filter_mrks(event &e, proc_viewer *viewer)
{
  if (viewer->proc_pr.get_filter_mrks()) {
    viewer->proc_pr.set_filter_mrks(FALSE);
  } else {
    viewer->proc_pr.set_filter_mrks(TRUE);
  }
  tree_proc *proc = viewer->current_proc;
  viewer->current_proc = NULL;	// to force a redraw
  viewer->view(proc);
}

/*--------------------------------------------------------------------
 * proc_viewer::expand_all
 *
 */

void proc_viewer::expand_all(event &e, proc_viewer *viewer)
{
  viewer->text->expand_all();
}

void proc_viewer::collapse_all(event &e, proc_viewer *viewer)
{
  viewer->text->collapse_all();
}

/*--------------------------------------------------------------------
 * proc_viewer::update_find_menu
 *
 */
void proc_viewer::update_find_menu(void)
{
  menu->clear("Find");
  delete_bindings(item_bindings);
  item_bindings->erase();

  /* find node */
  binding *b;
  
  b = new binding;
  item_bindings->append(b);
  b->set_function((bfun) &find_node, this);
  menu->add_radio(b, "Find/Item", "Tree node");

  b = new binding;
  item_bindings->append(b);
  b->set_function((bfun2) &find_block, this, (void *) TREE_BLOCK);
  menu->add_radio(b, "Find/Item", "Block");

  b = new binding;
  item_bindings->append(b);
  b->set_function((bfun2) &find_block, this, (void *) TREE_LOOP);
  menu->add_radio(b, "Find/Item", "Loop");

  b = new binding;
  item_bindings->append(b);
  b->set_function((bfun2) &find_block, this, (void *) TREE_FOR);
  menu->add_radio(b, "Find/Item", "For");

  b = new binding;
  item_bindings->append(b);
  b->set_function((bfun2) &find_block, this, (void *) TREE_IF);
  menu->add_radio(b, "Find/Item", "If");
  
  /* annotation items */
  string_list *names = get_annotation_names(current_proc);
  string_list_iter s_iter(names);
  while(!s_iter.is_empty()) {

    char *name = s_iter.step();
    b = new binding;
    item_bindings->append(b);

    b->set_function((bfun2) &find_annote, this, name);
    char m[200];
    sprintf(m, "Annote: [%s]", name);
    menu->add_radio(b, "Find/Item", m);
  }
  delete (names);

#if 0
  /* initialize find item */
  menu->invoke("Find/Item", "Tree node"); // activate first radio button
#endif

  /* find prev command */
  b = new binding;
  item_bindings->append(b);
  b->set_function((bfun2) &find_next, this, (void *) FALSE);
  menu->add_command(b, "Find", "Previous", "p");

  /* find next command */
  b = new binding;
  item_bindings->append(b);
  b->set_function((bfun2) &find_next, this, (void *) TRUE);
  menu->add_command(b, "Find", "Next", "n");
}

/*--------------------------------------------------------------------
 * select event handler (local)
 *
 */
void proc_viewer::local_select_event(event &e, proc_viewer *viewer)
{
}

/*--------------------------------------------------------------------
 * select event handler
 *
 * when a selection event occurs, this function is invoked
 */
void proc_viewer::global_select_event(event &e, proc_viewer *viewer)
{
  void *event_source = e.get_source();
  if (event_source == viewer ||
      event_source == viewer->text) return;	// ignode local event

  vnode *vn = e.get_object();
  char *tag = vn->get_tag();

  if (tag == tag_suif_object) {
    suif_object *obj = (suif_object *) vn->get_object();
    if (obj->object_kind() == TREE_OBJ) {
      tree_node *tn=  (tree_node *) obj;
      viewer->view(tn);
      if (!tn->is_proc()) {
	viewer->select(tn);
      }

    } else if (obj->object_kind() == SYM_OBJ &&
	       ((sym_node *) obj)->is_proc()) {

      /* proc_sym */
      viewer->view((proc_sym *) obj);
    }

  } else if (tag == tag_code_fragment) {

    /* code fragment */
    code_fragment *f = (code_fragment *) vn->get_object();
    viewer->view(f);
    viewer->select(f);
  }
}

/*--------------------------------------------------------------------
 * find block
 *
 */
void proc_viewer::find_block(event &e, proc_viewer *viewer, 
			     void *block_kind)
{
  viewer->find_info.type = FIND_BLOCK;
  viewer->find_info.block_kind = (int) block_kind;
  find_next(event(), viewer ,(void *) TRUE);
}

/*--------------------------------------------------------------------
 * find node
 *
 */
void proc_viewer::find_node(event &e, proc_viewer *viewer)
{
  viewer->find_info.type = FIND_NODE;
  find_next(event(), viewer ,(void *) TRUE);
}

/*--------------------------------------------------------------------
 * proc_viewer::find_annote
 *
 */
void proc_viewer::find_annote(event &e, proc_viewer *viewer, char *name)
{
  viewer->find_info.type = FIND_ANNOTE;
  viewer->find_info.annote_name = name;
  viewer->find_next(event(), viewer, (void *) TRUE);
}

/*--------------------------------------------------------------------
 * find next/prev
 *
 */
void proc_viewer::find_next(event &e, proc_viewer *viewer,
			    void *search_forward)
{
  match_fn fn;
  void *client_data;

  switch (viewer->find_info.type) {
  case FIND_BLOCK:
    fn = &match_block_kind;
    client_data = (void *) viewer->find_info.block_kind;
    break;
  case FIND_ANNOTE:
    fn = &match_annote_name;
    client_data = (void *)viewer->find_info.annote_name;
    break;
  case FIND_NODE:
    fn = &match_node;
    client_data = NULL;
    break;
  default:
    break;
  }
  viewer->find_next_helper(fn, client_data, (boolean) search_forward);
}

/*--------------------------------------------------------------------
 * find next helper
 *
 */
void proc_viewer::find_next_helper(match_fn fn, void *client_data,
				  boolean search_forward)
{
  vnode *vn = text->get_selection();

#if 1
  text->select_clear();
#endif

  tree_node *node;
  if (vn) {
    assert(is_suif_object(vn));
    suif_object *obj = (suif_object *) vn->get_object(); 
    switch (obj->object_kind()) {
    case TREE_OBJ:
      node = (tree_node *) obj;
      break;
    case INSTR_OBJ:
      node = ((instruction *) obj)->parent();
      break;
    default:
      node = current_proc;
    }
  } else {
    node = current_proc; // start from the top node
  }
  tree_node *next_node = node;
  while (1) {
    if (search_forward) {
      next_node = next_tree_node(next_node);
    } else {
      next_node = prev_tree_node(next_node);
    }
    if (!next_node) break;
    if (fn(next_node, client_data)) {
      /* skip filtered instructions */
      if (proc_pr.get_filter_mrks() &&
	  next_node->is_instr() &&
	  ((tree_instr *) next_node)->instr()->opcode() == io_mrk) {
	continue;
      }
      break;
    }
  }

  if (next_node) {
    view(next_node);
    select(next_node);
    vnode *vn = vman.find_vnode(next_node);
    post_event(event(vn, SELECTION, this));
  }
}

/*---------------------------------------------------------------------
 * match functions
 */
static boolean match_block_kind(tree_node *tn, void *client_data)
{
  return (tn->kind() == (int) client_data);
}

static boolean match_annote_name(tree_node *tn, void *client_data)
{
  if (tn->is_instr()) {
    instruction *instr = ((tree_instr *)tn)->instr();
    return (instr->peek_annote((char *) client_data) != NULL);
  } else {
    return (tn->peek_annote((char *) client_data) != NULL);
  }
}

static boolean match_node(tree_node *tn, void *client_data)
{
  return (TRUE);
}

/*---------------------------------------------------------------------
 * print the tree
 */

void proc_viewer::print_proc(tree_proc *proc)
{
  vnode *proc_vn = create_vnode(proc);
  text->tag_begin(proc_vn);
  proc_pr.print_tree_node(text, proc, 3, PRINT_FULL);
  text->tag_end(proc_vn);
}
