/*
 * allfiles.cc
 *
 * This file is only used for fast compilation.
 */

#include "vsuif.cc"
#include "suif_vnode.cc"
#include "suif_print.cc"
#include "suif_menu.cc"
#include "base_viewer.cc"
#include "suif_viewer.cc"
#include "info_viewer.cc"
#include "src_viewer.cc"
#include "prof_viewer.cc"
#include "text_viewer.cc"
#include "output_viewer.cc"
#include "code_tree.cc"
#include "suif_utils.cc"
#include "list_viewer.cc"
#include "main_window.cc"
#include "profile.cc"
#include "ann_form.cc"
#include "suif_types.cc"
#include "misc_viewers.cc"
#include "cg_viewer.cc"
