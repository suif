/*--------------------------------------------------------------------
 * src_viewer.h
 *
 * The "src_viewer" displays source code of the currently selected object.
 */

#ifndef SRC_VIEWER_H
#define SRC_VIEWER_H

#include "includes.h"
#include "base_viewer.h"
#include "code_tree.h"

class src_viewer : public text_base_viewer {
  typedef text_base_viewer inherited;

protected:
  file_set_entry *current_fse;
  const char *current_file;
  int line_offset;

  vmessage *infobar;
  column_id annote_column;

  code_tree *stree;
  class code_tree_list *stree_cache;

  void create_file_menu(void);
  void update_infobar(void);

  void clear(void);
  void refresh(void);

  void clear_cache(void);
  void build_stree(void);
  boolean print_source(file_set_entry *fse, const char *filename,
		       const char *pathname);
  void create_tags(code_fragment *f, tag_node *parent_tag);
  void show(vnode *vn);

  void annotate_src(void);
  void annotate_src_helper(code_fragment *f);

  static void create_src_links(tree_node *t, src_viewer *viewer);
  static code_range map_tree_node(tree_node *tn, src_viewer *viewer);
  static void do_close(event &e, src_viewer *viewer);

public:
  src_viewer(void);
  ~src_viewer(void);
  virtual void create_window(void);
  virtual char *class_name(void) { return "Source Viewer"; }
  virtual void handle_event(event &e);

  /* state of viewer */
  file_set_entry *file(void) { return current_fse; }
  code_tree *source_tree(void) { return stree; }

  /* display */
  void view(file_set_entry *fse, const char *filename = NULL);
  void view(tree_node *tn, boolean select = TRUE);

  static window *constructor(void) {
    return (new src_viewer);
  }

};

#endif
