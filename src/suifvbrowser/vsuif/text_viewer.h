/*--------------------------------------------------------------------
 * text_viewer.h
 *
 */

#ifndef TEXT_VIEWER_H
#define TEXT_VIEWER_H

#include "includes.h"
#include "base_viewer.h"

/*
 * text viewer
 */
class text_viewer : public text_base_viewer {
  typedef text_base_viewer inherited;

private:
  static void do_open(event &e, text_viewer *viewer);
  static void do_close(event &e, text_viewer *viewer);

public:
  text_viewer(void);
  ~text_viewer(void);
  virtual void create_window(void);
  virtual char *class_name(void) { return "Text Viewer"; }

  void open(char *filename);
  void clear(void);
  void insert_text(char *str);

  static window *constructor(void) {
    return (new text_viewer);
  }
};

#endif
