/*----------------------------------------------------------------------
 * suif_types.cc
 *
 */

#include "suif_types.h"
#include <string.h>
#include "suif_utils.h"

const char *t_int;
const char *t_string;
const char *t_extended_int;
const char *t_float;
const char *t_extended_float;
const char *t_symbol;
const char *t_type;
const char *t_operand;
const char *t_instruction;
const char *t_unknown;

static char *type_check_unknown(const char *string, char *&error_msg);
static char *type_check_int(const char *int_string, char *&error_msg);
static char *type_check_string(const char *string, char *&error_msg);
static char *type_check_float(const char *float_string, char *&error_msg);
static char *type_check_extended_int(const char *int_string, char *&error_msg);
static char *type_check_extended_float(const char *float_string, char *&error_msg);
static char *type_check_symbol(const char *symbol_string, char *&error_msg);
static char *type_check_type(const char *type_string, char *&error_msg);
static char *type_check_operand(const char *op_string, char *&error_msg);
static char *type_check_instruction(const char *instr_string, char *&error_msg);

static char **parse_string(char *str, int &litc);

/*-------------------------------------------------------------------
 * register suif types
 */
void register_suif_types(void)
{
  t_unknown = lexicon->enter("unknown")->sp;;
  t_int = lexicon->enter("int")->sp;
  t_string = lexicon->enter("string")->sp;;
  t_extended_int = lexicon->enter("extended_int")->sp;;
  t_float = lexicon->enter("float")->sp;;
  t_extended_float = lexicon->enter("extended_float")->sp;;
  t_symbol = lexicon->enter("symbol")->sp;;
  t_type = lexicon->enter("type")->sp;;
  t_operand = lexicon->enter("operand")->sp;;
  t_instruction = lexicon->enter("instruction")->sp;;
 
  typeman.register_type(t_unknown, &type_check_unknown);
  typeman.register_type(t_int, &type_check_int);
  typeman.register_type(t_string, &type_check_string);
  typeman.register_type(t_float, &type_check_float);
  typeman.register_type(t_extended_int, &type_check_extended_int);
  typeman.register_type(t_extended_float, &type_check_extended_float);
  typeman.register_type(t_symbol, &type_check_symbol);
  typeman.register_type(t_type, &type_check_type);
  typeman.register_type(t_operand, &type_check_operand);
  typeman.register_type(t_instruction, &type_check_instruction);
}

/*-------------------------------------------------------------------
 * type checking functions
 *
 */

static char *type_check_unknown(const char *string,
                                char *& /* error_msg */) /* unused */
{
  return (strdup(string));
}

static char *type_check_int(const char *int_string, char *&error_msg)
{
  error_msg = NULL;
  int val = atoi(int_string);

  char *new_val = new char[30];
  sprintf(new_val, "%d", val);
  return (new_val);
}

static char *type_check_string(const char *string, char *&error_msg)
{
  error_msg = NULL;
  return (strdup(string));
}

static char *type_check_float(const char *float_string, char *&error_msg)
{
  error_msg = NULL;
  float f = 0;
  sscanf(float_string, "%f", &f);

  char *new_val = new char[30];
  sprintf(new_val, "%f", f);
  return (new_val);
}

static char *type_check_extended_int(const char *int_string, char *&error_msg)
{
  error_msg = NULL;
  long val = atol(int_string);

  char *new_val = new char[30];
  sprintf(new_val, "%ld", val);
  return (new_val);
}

static char *type_check_extended_float(const char *float_string, char *&error_msg)
{
  error_msg = NULL;
  return (strdup(float_string));
}

static char *type_check_symbol(const char *symbol_string, char *&error_msg)
{
  error_msg = NULL;
  return (strdup(symbol_string));
}

static char *type_check_type(const char *type_string, char *&error_msg)
{
  error_msg = NULL;
  return (strdup(type_string));
}

static char *type_check_operand(const char *op_string, char *&error_msg)
{
  error_msg = NULL;
  return (strdup(op_string));
}

static char *type_check_instruction(const char *instr_string, char *&error_msg)
{
  error_msg = NULL;
  return (strdup(instr_string));
}

/*--------------------------------------------------------------------
 * Convert type node <-> string
 *
 */
void type_node_to_string(char *buffer, type_node *type)
{
  sprintf(buffer, "%d", type->type_id());
}

type_node *string_to_type_node(suif_object *obj, char *string, char *error_msg)
{
  int type_id;
  boolean match = FALSE;

  int litc;
  char **litv = parse_string(string, litc);
  if (litc == 1) {
    type_id = atoi(litv[2]);
    match = TRUE;
  }

  if (match) {

    base_symtab *symtab = get_enclosing_symtab(obj);
    if (!symtab) {
      error_msg = "Cannot find the symbol table.";
      return (NULL);
    }

    type_node *type = symtab->lookup_type_id(type_id);
    if (!type) {
      error_msg = "Cannot find specified type.";
    } else {
      error_msg = NULL;
    }
    return (type);

  } else {

    error_msg = "Parse error encountered, invalid type specified.\n"
      "Format: \"type_id\"";
    return (NULL);
  }
}

/*--------------------------------------------------------------------
 * Convert operand to string
 *
 */
void operand_to_string(char *buffer, operand op)
{
  switch (op.kind()) {
  case OPER_NULL: 
    {
      sprintf(buffer, "<nullop>");
    }
    break;

  case OPER_SYM:
    {
      sprintf(buffer, "<var_sym 0x%p>", op.symbol());
    }
    break;

  case OPER_INSTR: 
    {
      sprintf(buffer, "<instruction 0x%p>", op.instr());
    }
    break;

  default:
    {
      assert_msg(FALSE, ("Unknown operand kind %d", op.kind()));
    }
    break;
  }
}

operand string_to_operand(char *string, char *&error_msg)
{
  error_msg = NULL;

  if (strcmp(string, "<nullop>") == 0) {
    return operand();
  }

  var_sym *sym;
  if (sscanf(string, "<var_sym 0x%p>", &sym) == 1) {
    return operand(sym);
  }

  instruction *instr;
  if (sscanf(string, "<instruction 0x%p>", &instr) == 1) {
    return (operand(instr));
  }

  error_msg = "Unknown operand.";
  return operand();
}

/*--------------------------------------------------------------------
 * Convert instruction <-> string
 *
 */
void instruction_to_string(char *buffer, instruction *instr)
{
  sprintf(buffer, "0x%p", instr);
}

instruction *string_to_instruction(char *string, char *&error_msg)
{
  instruction *instr;
  if (sscanf(string, "0x%p", &instr) == 1) {
    error_msg = NULL;
    return (instr);
  } else {
    error_msg = "Invalid instruction specified.";
    return (NULL);
  }
}

/*--------------------------------------------------------------------
 * Convert sym_addr <-> string
 *
 */
void sym_addr_to_string(char *buffer, sym_addr addr)
{
  sym_node *sym = addr.symbol();
  sprintf(buffer, "%s,%d", sym->name(), addr.offset());
}

sym_addr string_to_sym_addr(suif_object *obj, char *string, char *&error_msg)
{
  char *sym_name;
  char *offset_str;
  boolean match = FALSE;

  int litc;
  char **litv = parse_string(string, litc);
  if (litc == 3 &&
      litv[1][0] == ',') {
    sym_name = litv[0];
    offset_str = litv[2];
    match = TRUE;
  }

  if (match) {
    base_symtab *symtab = get_enclosing_symtab(obj);
    if (!symtab) {
      error_msg = "Cannot find the symbol table.";
      return sym_addr((sym_node *) NULL, 0);
    }

    int k;
    sym_node *sym = NULL;
    for (k = 0; k < 3; k++) {
      sym = symtab->lookup_sym(sym_name, (sym_kinds) k, TRUE);
      if (sym) break;
    }
    if (!sym) {
      error_msg = "Cannot find the specified symbol.";
      return sym_addr((sym_node *) NULL, 0);
    }

    int offset = atoi(offset_str);
    return sym_addr(sym, offset);

  } else {

    error_msg = "Parse error encountered, invalid symbol specified.\n"
      "Format: \"symbol_name,offset\"";
    return sym_addr((sym_node *) NULL, 0);
  }
}

/*-------------------------------------------------------------------
 * Convert from string to int
 *
 */
int string_to_int(char *val, char *&error_msg) 
{
  error_msg = NULL;
  return atoi(val);
}

long string_to_extended_int(char *val, char *&error_msg) 
{
  error_msg = NULL;
  return atol(val);
}

/*-------------------------------------------------------------------
 * Convert from string to float
 *
 */
float string_to_float(char *val, char *&error_msg)
{
  error_msg = NULL;
  float f = 0;
  sscanf(val, "%f", &f);
  return f;
}


/*-------------------------------------------------------------------
 * Parse a string into literals
 *
 */
static char **parse_string(char *str, int &litc)
{
  char **litv = new char*[20];
  litc = 0;
  char *ptr = str;

  while (*ptr) {

    /* skip white spaces */
    while (*ptr == ' ') ptr++;

    /* match alphanumeric (+ some other symbols) string */
    char *old_ptr = ptr;
    while (*ptr) {
      char c = *ptr;
      if ((c >= 'a' && c <= 'z') || (c >='A' && c <= 'Z') ||
	  (c >= '0' && c <= '9') || (c == '_') || (c == '.') ||
	  (c == '/') || (c == '-')) {
	ptr++;
      } else {
	break;
      }
    }
    if (ptr != old_ptr) {
      int l = ptr - old_ptr;
      char *lit = new char[l + 1];
      strncpy(lit, old_ptr, l);
      lit[l] = 0;
      litv[litc] = lit;
      litc++;

    } else if (*ptr) {
      /* other char */
      char *lit = new char[2];
      lit[0] = *ptr;
      lit[1] = 0;
      litv[litc] = lit;
      litc++;
      ptr++;
    }
    if (litc == 19) {
      break;
    }
  }

  litv[litc] = NULL;
  return (litv);
}


#if 0
/*
 * helper functions no longer used
 */

static char *get_file_name(char *pathname)
{
  char *n = pathname;
  while (TRUE) {
    char *slash = strchr(n, '/');
    if (!slash) break;
    n = slash + 1;
  }

  return (n);
}

/*-------------------------------------------------------------------
 * Find a symtab from the name
 *
 */
static base_symtab *find_symtab(char *symtab_name)
{
  char *sname = strdup(symtab_name);
  char *p = strtok(sname, "/");
  if (p == sname) {
    p = strtok(NULL, "/");
  }
  base_symtab *symtab = fileset->globals();

  // follow the symtab chain
  while (symtab && p) {
    if (symtab->kind() == SYMTAB_GLOBAL) {

      for (base_symtab_list_e *le = symtab->children()->head();
	   le; le = le->next()) {
	if (strcmp(get_file_name(le->contents->name()), p) == 0) {
	  symtab = le->contents;
	  break;
	}
      }
      if (!le) {
	symtab = NULL;
      }

    } else {
      symtab = symtab->lookup_child(p);
    }
    p = strtok(NULL, "/");
  }
  delete (sname);
  return (symtab);
}

static void get_symtab_chain_name(char *s, base_symtab *symtab)
{
  if (symtab->parent() && symtab->parent()->kind() != SYMTAB_GLOBAL) {
    get_symtab_chain_name(s, symtab->parent());
  }
  strcat(s, "/");
  if (symtab->kind() == SYMTAB_FILE) {
    char *n = get_file_name(symtab->name());
    strcat(s, n);

  } else if (symtab->kind() != SYMTAB_GLOBAL) {
    strcat(s, symtab->name());
  }
}


#endif
