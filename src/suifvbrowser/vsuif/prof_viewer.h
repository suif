/*--------------------------------------------------------------------
 * prof_viewer.h
 *
 */

#ifndef PROF_VIEWER_H
#define PROF_VIEWER_H

#include "includes.h"
#include "profile.h"
#include "base_viewer.h"

/*
 * profile viewer
 */
class prof_viewer : public text_base_viewer {
  typedef text_base_viewer inherited;

private:
  profile_table *p_table;

  void init(void);
  void print_profile(void);
  void show(vnode *vn);
  void clear(void);

  static void do_open_runtime_file(event &e, prof_viewer *viewer);

public:
  prof_viewer(void);
  ~prof_viewer(void);
  virtual void create_window(void);
  virtual char *class_name(void) { return "Profile Viewer"; }
  virtual void handle_event(event &e);

  static window *constructor(void) {
    return (new prof_viewer);
  }
};

#endif
