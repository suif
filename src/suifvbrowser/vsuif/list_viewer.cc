/*-------------------------------------------------------------------
 * list_viewer
 *
 */

#include "list_viewer.h"
#include "suif_vnode.h"
#include "suif_event.h"
#include <stdlib.h>

/*--------------------------------------------------------------------
 * proc_list::create_window
 *
 */
void proc_list::create_window(void)
{
  inherited::create_window();
  add_close_button(button_bar);

  init();
}

/*--------------------------------------------------------------------
 * proc_list::init
 *
 */
void proc_list::init(void)
{
  sym_node_list procs;

  fileset->reset_iter();
  file_set_entry *fse;
  while ((fse = fileset->next_file())) {
    fse->reset_proc_iter();
    proc_sym *psym;
    while ((psym = fse->next_proc())) {
      if (psym->is_readable()) {
	procs.append(psym);
      }
    }
  }

  /* sort the procs and add them to the listbox */
  while (!procs.is_empty()) {
    sym_node_list_e *first_e = procs.head();
    for (sym_node_list_e *e = first_e->next(); e; e = e->next()) {
      if (strcmp(e->contents->name(), first_e->contents->name()) < 0) {
	first_e = e;
      }
    }
    vnode *vn = create_vnode(first_e->contents);
    listbox->add(NULL, vn, first_e->contents->name());
    delete procs.remove(first_e);
  }
}

/*--------------------------------------------------------------------
 * proc_list::handle_event
 *
 */
void proc_list::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case NEW_FILESET:
    {
      listbox->clear();
      init();
    }
    break;

  default:
    break;
  }
}
