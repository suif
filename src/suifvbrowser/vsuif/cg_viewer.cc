/*-------------------------------------------------------------------
 * cg_viewer
 *
 */

#include "cg_viewer.h"
#include "suif_vnode.h"
#include "suif_event.h"
#include "suif_menu.h"
#include "code_tree.h"
#include <cg.h>
#include <stdlib.h>

static char *init_msg = "Initializing call graph..";

/*--------------------------------------------------------------------
 * cg_viewer::cg_viewer
 */
cg_viewer::cg_viewer(void)
{
  nodes = NULL;
  current_cg = NULL;
  to_show_external_procs = TRUE;
}

/*--------------------------------------------------------------------
 * cg_viewer::~cg_viewer
 *
 */
cg_viewer::~cg_viewer(void)
{
  if (nodes) {
    delete (nodes);
  }
  if (current_cg) {
    delete (current_cg);
  }
}

/*--------------------------------------------------------------------
 * cg_viewer::create_window
 */
void cg_viewer::create_window(void)
{
  inherited::create_window();

  create_proc_menu();
  create_view_menu();
  init();

  vnode *last_sel = vman->get_selection();
  if (last_sel) {
    show(last_sel);
  }
}

/*--------------------------------------------------------------------
 * cg_viewer::clear
 */
void cg_viewer::clear(void)
{
  if (nodes) {
    delete (nodes);
    nodes = NULL;
  }
  graph_wdgt->clear();
}

/*--------------------------------------------------------------------
 * cg_viewer::init
 */
void cg_viewer::init(void)
{
  if (fileset->file_list()->is_empty()) {
    return;
  }

  post_progress(this, init_msg, 0);

  /* create parameters to pass to cg */
  file_set_entry_list *fse_list = fileset->file_list();
  int argc = fse_list->count() + 1;
  char **argv = new char*[argc];
  file_set_entry_list_e *e = fse_list->head();
  for (int i = 1; i < argc; i++) {
    argv[i] = strdup(e->contents->name());
    e = e->next();
  }

  /* initialize call graph */
  init_cg(argc, argv);
  current_cg = new cg(TRUE);

  post_progress(this, init_msg, 50);

  /* create visual call graph */
  show_cg(current_cg);
  exit_cg();
  for (int i = 1; i < argc; i++)
    free(argv[i]);
  delete argv;

  unpost_progress(this);
}

/*--------------------------------------------------------------------
 * cg_viewer::show_cg
 */
void cg_viewer::show_cg(cg *callgraph)
{
  int i;

  num_nodes = callgraph->num_nodes();
  nodes = new gnode*[num_nodes];

  /* add nodes */
  for (i = 0; i < num_nodes; i++) {
    cg_node *node = (*callgraph)[i];
    proc_sym *psym = node->suif_proc();

    if (!to_show_external_procs && !psym->is_readable()) {
      // don't show this node
      nodes[i] = NULL;

    } else {

      vnode *vn = create_vnode(psym);
      gnode *graph_node = graph_wdgt->add_node(psym->name(), vn);
      nodes[i] = graph_node;
    
      if (node == callgraph->main_node()) {
	graph_wdgt->set_root_node(graph_node);
      }
    }
  }

  /* add edges */
  for (i = 0; i < num_nodes; i++) {
    cg_node *caller, *callee;

    caller = (*callgraph)[i];
    cg_node_list *callees = caller->succs();
    cg_node_list_iter iter(callees);
    while (!iter.is_empty()) {
      callee = iter.step();
      int caller_num = caller->number();
      int callee_num = callee->number();

      arrow_dir arrow;
#if 0
      if (callee->succs()->contains((cg_node_list_e *)caller)) {
	if (callee_num < callee_num) {
	  continue;		// only create 1 edge, ignore this case
	}
	arrow = ARROW_BOTH;
      } else {
	arrow = ARROW_FORWARD;
      }
#endif

#if 1				// just create the edge!
      arrow = ARROW_FORWARD;
#endif

      if (nodes[caller_num] && nodes[callee_num]) {
	graph_wdgt->add_edge(nodes[caller_num], nodes[callee_num], arrow);
      }
    }

    visual_yield();
  }
  graph_wdgt->layout();
}

/*--------------------------------------------------------------------
 * cg_viewer::create_proc_menu
 */
void cg_viewer::create_proc_menu(void)
{
  menu->clear("Procedure");

  add_std_proc_menu(menu, "Procedure");
  menu->add_separator("Procedure");
  add_close_command(menu, "Procedure");
}

/*--------------------------------------------------------------------
 * cg_viewer::create_view_menu
 */
void cg_viewer::create_view_menu(void)
{
  menu->clear("View");

  binding *b = new binding((bfun) &do_show_external_procs, this);
  menu->add_check(b, "View/Options", "Show external procedures", TRUE);
}

/*--------------------------------------------------------------------
 */
void cg_viewer::set_binding(binding *b)
{
  graph_wdgt->set_binding(b);
}

/*--------------------------------------------------------------------
 */
void cg_viewer::select(proc_sym *psym)
{
  vnode *vn = vman->find_vnode(psym);
  graph_wdgt->select(vn);
}

void cg_viewer::select_add(proc_sym *psym)
{
  vnode *vn = vman->find_vnode(psym);
  graph_wdgt->select_add(vn);
}

/*--------------------------------------------------------------------
 */
void cg_viewer::do_show_external_procs(event &, cg_viewer *viewer)
{
  viewer->to_show_external_procs = !viewer->to_show_external_procs;
  if (viewer->current_cg) {
    viewer->clear();
    viewer->show_cg(viewer->current_cg);
  }
}

/*--------------------------------------------------------------------
 * event handler
 *
 * when a event occurs, this function is invoked
 */
void cg_viewer::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case SELECTION:
    {
      vnode *vn = e.get_object();
      void *event_source = e.get_source();
      if (event_source == graph_wdgt) return; // ignore local event

      show(vn);
    }
    break;

  case CLOSE_FILESET:
    {
      clear();
    }
    break;
    
  case NEW_FILESET:
    {
      create_proc_menu();
      create_view_menu();
      init();
    }
    break;

  default:
    break;
  }
}

/*--------------------------------------------------------------------
 * cg_viewer::show
 *
 */
void cg_viewer::show(vnode *vn)
{
  const char *tag = vn->get_tag();
  if (tag == tag_suif_object) {
    suif_object *obj = (suif_object *) vn->get_object();
    
    switch (obj->object_kind()) {
    case TREE_OBJ:
      {
	/* tree_proc */
	tree_node *tn = (tree_node *) obj;
	if (tn->is_proc()) {
	  select(tn->proc());
	}
      }
      break;

    case SYM_OBJ:
      {
	if (((sym_node *) obj)->is_proc()) {
	  /* proc_sym */
	  select((proc_sym *)obj);
	}
      }
      break;

    default:
      break;
    }

  } else if (tag == tag_code_fragment) {
    code_fragment *f = (code_fragment *) vn->get_object();
    if (f->node() && f->node()->is_proc()) {
      select(f->node()->proc());
    }
  }
}
