/*--------------------------------------------------------------------
 * list_viewer.h
 *
 */

#ifndef LIST_VIEWER_H
#define LIST_VIEWER_H

#include "includes.h"
#include "suif_print.h"
#include "base_viewer.h"

/*
 * Procedure list window
 */

class proc_list : public list_base_viewer {
  typedef list_base_viewer inherited;

protected:
  void init(void);

public:
  proc_list(char *text) : list_base_viewer() { set_title(text); }
  virtual void create_window(void);
  virtual char *class_name(void) { return "Procedure List"; }
  virtual void handle_event(event &e);

  static window *constructor(void) {
    return (new proc_list("Procedures:"));
  }
};

#endif
