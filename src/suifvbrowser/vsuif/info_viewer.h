/*--------------------------------------------------------------------
 * info_viewer.h
 *
 * The "info_viewer" class displays the currently selected object, and
 * various information about that object.
 *
 */

#ifndef INFO_VIEWER_H
#define INFO_VIEWER_H

#include "includes.h"
#include "suif_print.h"
#include "base_viewer.h"

class info_printer : public suif_printer {

protected:

  static void print_parallel_region(suif_printer *pr, vtty *text,
				    tree_node *node, int depth);

public:
  info_printer() {
    /* override */
    print_instruction_fn = &print_instruction_info;
    print_tree_node_fn = &print_tree_node_info;

    to_print_instr_annotes = FALSE;
  }

  static void print_instruction_info(suif_printer *pr, vtty *text,
				     instruction *instr, int depth,
				     int elab, int *en);
  static void print_tree_node_info(suif_printer *pr, vtty *text,
				   tree_node *tn, int depth,
				   int detail);
};

class info_viewer : public text_base_viewer {
  typedef text_base_viewer inherited;

private:
  info_printer info_pr;

  void create_obj_menu(void);
  void create_edit_menu(void);

  static void do_show_obj_cmd(event &e, info_viewer *viewer, vnode *vn);

public:
  info_viewer(void);
  ~info_viewer(void);
  virtual void create_window(void);
  virtual char *class_name(void) { return "Info Viewer"; }
  virtual void handle_event(event &e);

  void clear(void);
  void view(vnode *vn);
  void refresh(void);

  static window *constructor(void) {
    return (new info_viewer);
  }

};

#endif
