#ifndef SUIF_TYPES_H
#define SUIF_TYPES_H

#include "includes.h"

void register_suif_types(void);

/*
 * Immediate list dynamic types. See "dynatype.h"
 */

extern const char *t_int;
extern const char *t_string;
extern const char *t_extended_int;
extern const char *t_float;
extern const char *t_extended_float;
extern const char *t_symbol;
extern const char *t_type;
extern const char *t_operand;
extern const char *t_instruction;
extern const char *t_unknown;


/*
 * Conversion routines
 */

void type_node_to_string(char *buffer, type_node *type);
void operand_to_string(char *buffer, operand op);
void instruction_to_string(char *buffer, instruction *instr);
void sym_addr_to_string(char *buffer, sym_addr addr);

int string_to_int(char *val, char *&error_msg);
long string_to_extended_int(char *val, char *&error_msg);
float string_to_float(char *val, char *&error_msg);

type_node *string_to_type_node(suif_object *obj, char *string,
			       char *error_msg);
operand string_to_operand(char *string, char *&error_msg);
instruction *string_to_instruction(char *string, char *&error_msg);
sym_addr string_to_sym_addr(suif_object *obj, char *string, char *&error_msg);

#endif


