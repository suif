/*-------------------------------------------------------------------
 * suif_viewer
 *
 */

#include "suif_viewer.h"
#include "src_viewer.h"
#include "suif_utils.h"
#include "suif_vnode.h"
#include "suif_print.h"
#include "suif_event.h"
#include "suif_menu.h"
#include "list_viewer.h"
#include <stdlib.h>

enum { FIND_NODE = 0, FIND_BLOCK, FIND_ANNOTE}; 

static boolean match_block_kind(suif_object *obj, void *client_data);
static boolean match_annote_name(suif_object *obj, void *client_data);
static boolean match_tree_node(suif_object *obj, void *client_data);

static boolean _filter_mrk = FALSE;

/*--------------------------------------------------------------------
 * proc_printer::print_instruction
 *
 */
void proc_printer::print_instruction_filter(suif_printer *pr, vtty *text,
					    instruction *instr,
					    int depth, int elab, int *en)
{
  boolean filter = ((proc_printer *)pr)->filter_mrk;

  /* filter away the marks */
  if (filter && instr->opcode() == io_mrk) {
    return;
  }
  suif_pr.print_instruction(text, instr, depth, elab, en);
}

/*--------------------------------------------------------------------
 * suif_viewer::suif_viewer
 */
suif_viewer::suif_viewer(void)
{
  current_proc = NULL;
}

/*--------------------------------------------------------------------
 * suif_viewer::~suif_viewer
 *
 */
suif_viewer::~suif_viewer(void)
{
  delete (info_bar);
}

/*--------------------------------------------------------------------
 * suif_viewer::create_window
 */

void suif_viewer::create_window(void)
{
  inherited::create_window();
  info_bar = new vmessage(toplevel);

  /* menu */
  menu->remove(ROOT_MENU);
  create_proc_menu();
  create_edit_menu();
  create_view_menu();
  create_find_menu();
  add_std_go_menu(menu);

  /* info bar */
  update_info_bar();

  vnode *last_sel = vman->get_selection();
  if (last_sel) {
    select_node(last_sel);
  }
}

/*--------------------------------------------------------------------
 * suif_viewer::clear
 *
 */
void suif_viewer::clear(void)
{
  current_proc = NULL;
  text->clear();

  menu->remove(ROOT_MENU);
  create_proc_menu();
  create_edit_menu();
  create_view_menu();
  create_find_menu();
  add_std_go_menu(menu);

  update_info_bar();
}

/*--------------------------------------------------------------------
 * suif_viewer::refresh
 */
void suif_viewer::refresh(void)
{
  /* menu */
  menu->remove(ROOT_MENU);
  create_proc_menu();
  create_edit_menu();
  create_view_menu();
  create_find_menu();
  add_std_go_menu(menu);

  /* info bar */
  update_info_bar();

  proc_sym *proc = current_proc;
  current_proc = NULL;		// to force a redraw
  view(proc);

  vnode *last_sel = vman->get_selection();
  if (last_sel) {
    select_node(last_sel);
  }
}

/*--------------------------------------------------------------------
 * suif_viewer::create_proc_menu
 *
 */
void suif_viewer::create_proc_menu(void)
{
  menu->clear("Procedure");
  add_std_proc_menu(menu, "Procedure");
  menu->add_separator("Procedure");
  add_close_command(menu, "Procedure");
}

/*--------------------------------------------------------------------
 * suif_viewer::create_edit_menu
 *
 */
void suif_viewer::create_edit_menu(void)
{
  menu->clear("Edit");
  add_std_edit_menu(menu, "Edit");
}

/*--------------------------------------------------------------------
 * suif_viewer::view
 *
 * view a procedure
 */
void suif_viewer::view(proc_sym *psym)
{
  if (current_proc != psym) {

    current_proc = psym;

    if (!psym->is_in_memory()) {
      psym->read_proc();
    }

    text->clear();

    /* print procedure */
    tree_proc *proc = psym->block();
    if (proc) {
      print_proc(proc);
      text->update();
    }

    /* update menu and info bar */
    update_info_bar();
    create_find_menu();

    // Post the new event
    post_event(event(NULL, NEW_PROC_SUIF_DISPLAYED));
  }
}

/*--------------------------------------------------------------------
 * suif_viewer::show_node
 *
 * to make sure a node is expanded, visible
 */
void suif_viewer::show_node(tree_node *tn)
{
  if (!tn->is_proc()) {
    /* make sure its parent is expanded */
    tree_node *par = tn->parent()->parent();
    if (par) {
      show_node(par);
      vnode *vn = create_vnode(par);
      tag_node *par_tag = text->find_tag(vn);
      if (!par_tag->is_expanded()) {
	text->expand_node(par_tag);
      }
    }
  }
}

/*--------------------------------------------------------------------
 * suif_viewer::view
 *
 * view a tree node
 */
void suif_viewer::view(tree_node *tn)
{
  view(tn->proc());

  /* make sure that the tn is visible */
  show_node(tn);
  vnode *vn = create_vnode(tn);
  text->view(vn);
}

/*--------------------------------------------------------------------
 * suif_viewer::view
 *
 * view an instruction
 */

void suif_viewer::view(instruction *instr)
{
  tree_node *par = instr->parent();
  view(par->proc());

  /* make sure that the tn is visible */
  show_node(par);
  vnode *vn = create_vnode(instr);
  text->view(vn);
}

/*--------------------------------------------------------------------
 * suif_viewer::view
 *
 * view a code fragment
 */
void suif_viewer::view(code_fragment *f)
{
  tree_node *tn = f->node();
  view(tn);
}

/*--------------------------------------------------------------------
 * suif_viewer::select
 *
 * select a code fragment
 */
void suif_viewer::select(code_fragment *f)
{
  text->select_clear();

  vnode *vn;
  if (f->node()->is_instr()) {
    instruction_list_iter iter(f->instr_list());
    while (!iter.is_empty()) {
      instruction *instr = iter.step();
      vnode *vn = create_vnode(instr->owner());
      text->select(text->find_tag(vn), TRUE);
    }
  } else {
    vn = create_vnode(f->node());
    text->select(vn);
  }
}

/*--------------------------------------------------------------------
 * suif_viewer::select
 *
 * select a tree node
 */
void suif_viewer::select(tree_node *tn)
{
  text->select_clear();

  vnode *vn;

  if (tn->is_instr()) {
    vn = create_vnode(((tree_instr *) tn)->instr());
  } else {
    vn = create_vnode(tn);
  }

  if (vn) {
    text->select(vn);
  }
}

void suif_viewer::select(instruction *instr)
{
  text->select_clear();
  vnode *vn = create_vnode(instr);
  if (vn) {
    text->select(vn);
  }
}

/*--------------------------------------------------------------------
 * suif_viewer::update_info_bar
 *
 */
void suif_viewer::update_info_bar(void)
{
  const char *proc_name = current_proc ? current_proc->name() : "";

  char m[100];
  sprintf(m, "Procedure: %s", proc_name);
  info_bar->set_message(m);
}

/*--------------------------------------------------------------------
 * suif_viewer::create_view_menu
 *
 */
void suif_viewer::create_view_menu(void)
{
  menu->clear("View");

  binding *b = new binding((bfun) &expand_all_cmd, this);
  menu->add_command(b, "View", "Expand all");

  b = new binding((bfun) &collapse_all_cmd, this);
  menu->add_command(b, "View", "Collapse all");

  menu->add_separator("View");
  b = new binding((bfun) &filter_mrk_cmd, this);
  menu->add_check(b, "View/Filter", "mrk", proc_pr.filter_mrk);
}

void suif_viewer::filter_mrk_cmd(event &, suif_viewer *viewer)
{
  if (viewer->proc_pr.filter_mrk) {
    viewer->proc_pr.filter_mrk = FALSE;
  } else {
    viewer->proc_pr.filter_mrk = TRUE;
  }

  viewer->refresh();
}

/*--------------------------------------------------------------------
 * suif_viewer::expand_all
 *
 */

void suif_viewer::expand_all_cmd(event &, suif_viewer *viewer)
{
  viewer->text->expand_all();
}

void suif_viewer::collapse_all_cmd(event &, suif_viewer *viewer)
{
  viewer->text->collapse_all();
}

/*--------------------------------------------------------------------
 * suif_viewer::create_find_menu
 *
 */
void suif_viewer::create_find_menu(void)
{
  menu->clear("Find");

  if (!current_proc) {
    return;
  }

  /* find node */
  binding *b;
  
  b = new binding((bfun) &find_node_cmd, this);
  menu->add_radio(b, "Find/Item", "Tree node");

  b = new binding((bfun2) &find_block_cmd, this, (void *) TREE_BLOCK);
  menu->add_radio(b, "Find/Item", "Block node");

  b = new binding((bfun2) &find_block_cmd, this, (void *) TREE_LOOP);
  menu->add_radio(b, "Find/Item", "Loop node");

  b = new binding((bfun2) &find_block_cmd, this, (void *) TREE_FOR);
  menu->add_radio(b, "Find/Item", "For node");

  b = new binding((bfun2) &find_block_cmd, this, (void *) TREE_IF);
  menu->add_radio(b, "Find/Item", "If node");
  
  /* annotation items */
  if (current_proc) {
    string_list *names = get_annotation_names(current_proc->block());
    string_list_iter s_iter(names);
    while(!s_iter.is_empty()) {
      
      const char *name = s_iter.step();
      b = new binding((bfun2) &find_annote_cmd, this, name);
      char m[200];
      sprintf(m, "Annote: [%s]", name);
      menu->add_radio(b, "Find/Item", m);
    }
    delete (names);
  }

  menu->add_separator("Find");

  /* "find prev" command */
  b = new binding((bfun2) &find_next_cmd, this, (void *) FALSE);
  menu->add_command(b, "Find", "Previous", "p");
  
  /* "find next" command */
  b = new binding((bfun2) &find_next_cmd, this, (void *) TRUE);
  menu->add_command(b, "Find", "Next", "n");

  menu->add_separator("Find");

  /* find instruction command */
  b = new binding((bfun) & find_instr_cmd, this);
  menu->add_command(b, "Find", "Instruction..");
}

/*--------------------------------------------------------------------
 * handle_event
 *
 * when a event occurs, this function is invoked
 */
void suif_viewer::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case SELECTION:
    {
      void *event_source = e.get_source();
      if (event_source == text) return;	// ignore local event
      
      vnode *vn = e.get_object();
      select_node(vn);
    }
    break;

  case CLOSE_FILESET:
  case NEW_FILESET:
    {
      clear();
    }
    break;

  case REFRESH:
  case PROC_MODIFIED:
  case FSE_MODIFIED:
    {
      refresh();
    }
    break;

  default:
    break;
  }
}

/*--------------------------------------------------------------------
 * find instruction
 *
 */
void suif_viewer::find_instr_cmd(event &, suif_viewer *viewer)
{
  char buffer[200];
  display_query(viewer, "Enter instruction number:", buffer);

  int inum;
  if (sscanf(buffer, "%d", &inum) == 1 &&
      viewer->current_proc) {
    /* find the instruction */
    instruction *instr = find_instr_by_num(viewer->current_proc->block(),
					   inum);
    if (instr) {
      vnode *vn = create_vnode(instr);
      viewer->view(instr);
      viewer->select(instr);
      post_event(event(vn, SELECTION, viewer->text));
    } else {
      display_message(viewer, "Cannot find instruction numbered %d.", inum);
    }
  }
}

/*--------------------------------------------------------------------
 * find block
 *
 */
void suif_viewer::find_block_cmd(event &, suif_viewer *viewer, 
				 void *block_kind)
{
  viewer->find_info.type = FIND_BLOCK;
  viewer->find_info.block_kind = (int) block_kind;
  find_next_cmd(event(), viewer ,(void *) TRUE);
}

/*--------------------------------------------------------------------
 * find node
 *
 */
void suif_viewer::find_node_cmd(event &, suif_viewer *viewer)
{
  viewer->find_info.type = FIND_NODE;
  find_next_cmd(event(), viewer ,(void *) TRUE);
}

/*--------------------------------------------------------------------
 * suif_viewer::find_annote
 *
 */
void suif_viewer::find_annote_cmd(event &, suif_viewer *viewer, const char *name)
{
  viewer->find_info.type = FIND_ANNOTE;
  viewer->find_info.annote_name = name;
  viewer->find_next_cmd(event(), viewer, (void *) TRUE);
}

/*--------------------------------------------------------------------
 * find next/prev
 *
 */
void suif_viewer::find_next_cmd(const event &, suif_viewer *viewer,
				void *search_forward)
{
  match_fn fn;
  void *client_data;

  switch (viewer->find_info.type) {
  case FIND_BLOCK:
    fn = &match_block_kind;
    client_data = (void *) viewer->find_info.block_kind;
    break;
  case FIND_ANNOTE:
    fn = &match_annote_name;
    client_data = (void *)viewer->find_info.annote_name;
    break;
  case FIND_NODE:
    fn = &match_tree_node;
    client_data = NULL;
    break;
  default:
    break;
  }
  viewer->find_next_helper(fn, client_data, (boolean) search_forward);
}

/*--------------------------------------------------------------------
 * find next helper
 *
 */
void suif_viewer::find_next_helper(match_fn fn, void *client_data,
				  boolean search_forward)
{
  vnode *vn = text->get_selection();

#if 1
  text->select_clear();
#endif

  tree_node *node;
  if (vn && vn->get_tag() == tag_suif_object) {
    suif_object *obj = (suif_object *) vn->get_object(); 
    switch (obj->object_kind()) {
    case TREE_OBJ:
      node = (tree_node *) obj;
      break;
    case INSTR_OBJ:
      node = ((instruction *) obj)->parent();
      break;
    default:
      node = current_proc->block();
    }
  } else {
    if (search_forward) {
      node = current_proc->block(); // start from the top node
    } else {
      node = last_descendent(current_proc->block());
    }
  }

  _filter_mrk = proc_pr.filter_mrk;
  suif_object *found_obj = find_tree_node(node, fn, client_data,
					  search_forward);
  if (found_obj) {
    switch (found_obj->object_kind()) {
    case TREE_OBJ:
      {
	tree_node *tn = (tree_node *) found_obj;
	view(tn);
	select(tn);
	vnode *vn = create_vnode(tn);
	post_event(event(vn, SELECTION, text));
      }
      break;
      
    case INSTR_OBJ:
      {
	instruction *instr = (instruction *) found_obj;
	view(instr);
	select(instr);
	vnode *vn = create_vnode(instr);
	post_event(event(vn, SELECTION, text));
      }
      break;
      
    default:
      display_message(this, "Canont find item.");
      break;
    }
  } else {
    display_message(this, "Cannot find item.");
  }
}

/*---------------------------------------------------------------------
 * match functions
 */
static boolean match_block_kind(suif_object *obj, void *client_data)
{
  if (obj->object_kind() == TREE_OBJ) {
    tree_node *tn = (tree_node *) obj;
    if (_filter_mrk &&
	tn->is_instr() &&
	((tree_instr *) tn)->instr()->opcode() == io_mrk) {
      return (FALSE);
    }
    return (tn->kind() == (int) client_data);
  }
  return (FALSE);
}

static boolean match_annote_name(suif_object *obj, void *client_data)
{
  return (obj->peek_annote((char *) client_data) != NULL);
}

static boolean match_tree_node(suif_object *obj,
                               void * /* client_data */) /* unused */
{
  if (obj->object_kind() == TREE_OBJ) {
    tree_node *tn = (tree_node *) obj;
    if (_filter_mrk &&
	tn->is_instr() &&
	((tree_instr *) tn)->instr()->opcode() == io_mrk) {
      return (FALSE);
    }
    return (TRUE);
  }
  return (FALSE);
}

/*---------------------------------------------------------------------
 * print the tree
 */
void suif_viewer::print_proc(tree_proc *proc)
{
  vnode *proc_vn = create_vnode(proc);
  text->tag_begin(proc_vn);
  proc_pr.print_tree_node(text, proc, 3, PRINT_FULL);
  text->tag_end(proc_vn);
}

/*---------------------------------------------------------------------
 * show
 */
void suif_viewer::select_node(vnode *vn)
{
  const char *tag = vn->get_tag();
  if (tag == tag_suif_object) {

    suif_object *obj = (suif_object *) vn->get_object();

    switch (obj->object_kind()) {
    case TREE_OBJ:
      {
	tree_node *tn = (tree_node *) obj;
	view(tn);
	select(tn);
      } 
      break;

    case INSTR_OBJ:
      {
	instruction *instr = (instruction *) obj;
	view(instr);
	select(instr);
      }
      break;

    case SYM_OBJ:
      {
	if (((sym_node *) obj)->is_proc()) {
	  /* proc_sym */
	  proc_sym *psym = (proc_sym *) obj;
	  if (!psym->is_in_memory()) {
	    psym->read_proc();
	  }
	  tree_node *tn = psym->block();
	  if (tn) {
	    view(tn);
	    /*	    select(tn);AMER*/
	  }
	}
      }
      break;

    default:
      break;
    }
    
  } else if (tag == tag_code_fragment) {
    
    /* code fragment */
    code_fragment *f = (code_fragment *) vn->get_object();
    view(f);
    select(f);
  }
}
