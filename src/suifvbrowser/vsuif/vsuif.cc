/*--------------------------------------------------------------------
 * vsuif.cc
 */

#include "info_viewer.h"
#include "suif_viewer.h"
#include "src_viewer.h"
#include "prof_viewer.h"
#include "text_viewer.h"
#include "output_viewer.h"
#include "list_viewer.h"
#include "suif_print.h"
#include "suif_types.h"
#include "suif_vnode.h"
#include "main_window.h"
#include "cg_viewer.h"
#include <annotes.h>

/*--------------------------------------------------------------------
 * main initialization routine
 */

void init_vsuif(void)
{
  static boolean init_flag = FALSE;

  if (!init_flag) {
    init_flag = TRUE;

    init_vnode_tags();
    
    /* Pointer analysis initialization */
    wilbyr_init_annotes();

    /* Register dynamic types */
    register_suif_types();
    
    /* Register viewers */
    vman->register_window_class("Callgraph Viewer",
			       &cg_viewer::constructor);
    vman->register_window_class("Source Viewer", &src_viewer::constructor);
    vman->register_window_class("Suif Viewer", &suif_viewer::constructor);
    vman->register_window_class("Output Viewer", &output_viewer::constructor);
    vman->register_window_class("Info Viewer", &info_viewer::constructor);
    vman->register_window_class("Profile Viewer", &prof_viewer::constructor);
    vman->register_window_class("Text Viewer", &text_viewer::constructor);
    vman->register_window_class("Procedure List", &proc_list::constructor);
  }
}

/*----------------------------------------------------------------------
 * exit
 */

void exit_vsuif(void)
{
}

/*----------------------------------------------------------------------
 * start_vsuif
 */
void start_vsuif(int argc, char * argv[])
{
  main_window *mainwin = new main_window;
  mainwin->create_window();
  mainwin->browse_fileset(argc, argv);

#if 0
  /* prompt */
  visual_prompt();
#endif

#if 0
  /* main loop */
  visual_mainloop();
#endif

}
