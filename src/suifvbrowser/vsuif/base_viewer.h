/*--------------------------------------------------------------------
 * base_viewer.h
 *
 */

#ifndef BASE_VIEWER_H
#define BASE_VIEWER_H

#include "includes.h"

/*----------------------------------------------------------------------
 * base viewer
 */

class base_viewer : public window {
  typedef window inherited;

private:
  static void do_close_command(event &e, base_viewer *viewer);

protected:
  binding *event_binding;

public:
  base_viewer(void);
  virtual ~base_viewer(void);

  virtual char *class_name(void) { return "Base Viewer"; }
  virtual void handle_event(event &) {};

  void add_close_command(vmenu *menu, char *parent_menu);
  void add_close_button(vbuttonbar *button_bar);
};

/*----------------------------------------------------------------------
 * text viewer
 */
class text_base_viewer : public base_viewer {
  typedef base_viewer inherited;

protected:
  vframe *frame;
  vmenu *menu;
  vtext *text;

public:
  text_base_viewer(void);
  virtual ~text_base_viewer(void);
  virtual void handle_event(event &e);

  virtual void create_window(void);
  virtual char *class_name(void) { return "Text Viewer"; }
};

/*----------------------------------------------------------------------
 * graph base viewer
 */
class graph_base_viewer : public base_viewer {
  typedef base_viewer inherited;

protected:
  vframe *frame;
  vmenu *menu;
  vgraph *graph_wdgt;

public:
  graph_base_viewer(void);
  virtual ~graph_base_viewer(void);

  virtual void create_window(void);
  virtual char *class_name(void) { return "Graph Viewer"; }
};

/*----------------------------------------------------------------------
 * list base viewer
 */

class list_base_viewer : public base_viewer {
  typedef base_viewer inherited;

protected:  
  vframe *listbox_frame;
  vlistbox *listbox;
  vframe *button_frame;
  vbuttonbar *button_bar;

  char *list_title;

  void clear(void);

public:
  list_base_viewer(void);
  virtual ~list_base_viewer(void);

  void set_title(char *s);

  virtual void create_window(void);
  virtual char *class_name(void) { return "List Viewer"; }
};


/*----------------------------------------------------------------------
 * form base viewer
 */

class form_base_viewer : public base_viewer {
  typedef base_viewer inherited;

protected:  
  vmenu *menu;
  vframe *form_frame;
  vframe *button_frame;
  vform *form;
  vmessage *info_bar;
  vbuttonbar *button_bar;

public:
  form_base_viewer(void);
  virtual ~form_base_viewer(void);

  virtual void create_window(void);
  virtual char *class_name(void) { return "Form Viewer"; }

  void set_info_bar(char *msg);

  void add_immed_list(immed_list *immeds);
  void add_immed(immed imm, char *field_name);

  immed_list *get_immed_list(suif_object *obj, int first_field_num = 0);
  immed get_immed(suif_object *obj, int field_num, char *&error_msg);

};


#endif
