/*--------------------------------------------------------------------
 * misc_viewers.h
 *
 * Miscellaneous viewers
 *
 */

#ifndef MISC_VIEWERS_H
#define MISC_VIEWERS_H

#include "includes.h"
#include "suif_print.h"
#include "base_viewer.h"
#include "suif_utils.h"

/*
 * Proc Iter viewer
 */

class proc_iter_viewer : public text_base_viewer {
  typedef text_base_viewer inherited;

protected:

  file_set_entry *current_fse;

  void create_menu(void);
  static void do_iterate(event &e, proc_iter_viewer *viewer);

public:

  visual_prociter_f iter_fn;
  boolean writeback;
  boolean exp_trees;
  boolean use_fortran_form;

  proc_iter_viewer(void);
  ~proc_iter_viewer(void);

  virtual void create_window(void);
  virtual char *class_name(void) { return "Proc Iter Viewer"; }
  virtual void handle_event(event &e);

  void reset_iter(void);
  boolean next_proc(void);	/* return FALSE if there is no more proc */
  void iterate(void);

  static window *constructor(void) {
    return (new proc_iter_viewer);
  }

};

#endif
