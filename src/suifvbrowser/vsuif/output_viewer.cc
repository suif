/*-------------------------------------------------------------------
 * output_viewer
 *
 */

#include "output_viewer.h"
#include "suif_vnode.h"
#include "code_tree.h"
#include "suif_utils.h"
#include "suif_event.h"
#include <stdlib.h>
#include <string.h>

#define NORMAL_STATE 0
#define COMMENT_STATE 1

static char *preprocess_c(char *line, int &state, const char *&current_src_file,
			  int &current_src_line, boolean &found_line_num);

/*--------------------------------------------------------------------
 */
struct output_node {
  int src_line;
  const char *src_file;
  int out_line;
  tree_node *tn;

  boolean mapped;		// used by map_tree_node

  output_node(int s_line, const char *s_file, int o_line) {
    src_line = s_line;
    src_file = lexicon->enter(s_file)->sp;
    out_line = o_line;
    mapped = FALSE;
  }
};

DECLARE_LIST_CLASS(output_node_list, output_node *);

struct scope_node {
  proc_sym *proc;

  int first_line;
  int last_line;

  output_node_list nodes;
};

DECLARE_LIST_CLASS(scope_node_list, scope_node *);

/*--------------------------------------------------------------------
 * output_viewer::output_viewer
 */
output_viewer::output_viewer(void)
{
  current_file = NULL;
  current_fse = NULL;
  outtree = new code_tree;
  outtree->set_map_fn((map_tn_fn) &map_tree_node, this);

  proc_scopes = new scope_node_list;
}

/*--------------------------------------------------------------------
 * output_viewer::~output_viewer
 *
 */
output_viewer::~output_viewer(void)
{
  delete (outtree);
  delete (infobar);
  delete (proc_scopes);
}

/*--------------------------------------------------------------------
 * output_viewer::create_window
 */
void output_viewer::create_window(void)
{
  inherited::create_window();
  infobar = new vmessage(toplevel);

  /* menu */
  binding *b = new binding((bfun) &do_open_cmd, this);
  menu->add_command(b, "File", "Open Output File..");
  add_close_command(menu, "File");

  update_infobar();

  vnode *last_sel = vman->get_selection();
  if (last_sel) {
    show(last_sel);
  }

}

/*--------------------------------------------------------------------
 * output_viewer::erase_mappings()
 *
 */
void output_viewer::erase_mappings(void)
{
  while (!proc_scopes->is_empty()) {
    scope_node *scope = proc_scopes->pop();
    while (!scope->nodes.is_empty()) {
      delete scope->nodes.pop();
    }
    delete (scope);
  }
}

/*--------------------------------------------------------------------
 * output_viewer::do_open_cmd
 *
 */
void output_viewer::do_open_cmd(event &, output_viewer *viewer)
{
  char filename[200];
  select_file(viewer, filename, "Load output file:");
  if (filename[0] == 0) {
    return;
  }

  viewer->view(NULL, filename);
}

/*--------------------------------------------------------------------
 * output_viewer::clear
 *
 */
void output_viewer::clear(void)
{
  current_file = NULL;
  current_fse = NULL;
  outtree->clear();

  text->clear();
  update_infobar();
}

/*--------------------------------------------------------------------
 * output_viewer::update_infobar
 */
void output_viewer::update_infobar(void)
{
  char m[200];
  sprintf(m, 
	  "s2c Output file: `%s'",
	  current_file ? current_file : "none");
  infobar->set_message(m);
}

/*--------------------------------------------------------------------
 * output_viewer::print_output
 *
 */
boolean output_viewer::print_output(const char *filename)
{
  post_progress(this, "Loading output file..", 0);

  text->clear();

  FILE *fd = fopen(filename, "r");
  if (!fd) {
    fprintf(text->fd(), "Cannot open file `%s'.\n", filename);
    text->update();
    return (FALSE);
  }

  /* read the file */
  fseek(fd, 0, SEEK_END);
  long length = ftell(fd);
  fseek(fd, 0, SEEK_SET);
  char *buffer = new char[length];
  int l = fread(buffer, 1, length, fd);
  buffer[l] = 0;
  fclose(fd);

  int current_line = 1;
  int current_src_line = -1;
  boolean new_src_line = FALSE;
  boolean parse_error = FALSE;
  int scope = 0;
  scope_node *current_scope = NULL;
  int state = NORMAL_STATE;

  erase_mappings();		// erase previous mappings

  char *next_line;
  const char *current_src_file = NULL;
  for (char *line = buffer; line; line = next_line) {
    next_line = strchr(line, '\n');
    if (next_line) {
      *next_line++ = 0;
    }
 
    /*
     * Preprocess C
     */
    boolean found_line_num;
    char *ppc = preprocess_c(line, state, current_src_file,
			     current_src_line, found_line_num);
    /*
     * Scan for "{" and "}" to determine current scope.
     */
    for (char *p = ppc; *p; p++) {
      if (*p == '{') {
	if (scope == 0) {
	  current_scope = new scope_node;
	  current_scope->first_line = current_line;
	  current_scope->proc = NULL;
	  proc_scopes->append(current_scope);
	}
	scope++;

      } else if (*p == '}') {
	scope--;
	if (scope < 0) {
	  parse_error = TRUE;
	}
	if (scope == 0) {
	  current_scope->last_line = current_line;
	}
      }
    }
    delete (ppc);

    /* Check if current line has line number annotation */
    if (found_line_num) {
      new_src_line = TRUE;
      continue;			// don't print it out
    }

    /*
     * Record the line, if nec.
     */
    if (new_src_line) {
      /* enter this line as a node */
      output_node *node = new output_node(current_src_line,
					  current_src_file,
					  current_line);
      if (current_scope) {
	current_scope->nodes.append(node);
	
	/* match current scope to procedure */
	if (current_scope->proc == NULL) {
	  tree_node *tn = map_src_to_tree_node(current_src_file, 
					       current_src_line);
	  if (tn) {
	    current_scope->proc = tn->proc();
	  }
	}
      }
      
      new_src_line = FALSE;
    }

    /* print the line out */
    fprintf(text->fd(), "%s\n", line);
    current_line++;
  }

  delete (buffer);
  text->update();

  if (scope != 0 || parse_error) {
    display_message(this, "Warning: unable to parse the file `%s'.", filename);
  }

  if (proc_scopes->is_empty()) {
    display_message(this, "Warning: cannot find line number information in "
		    "the file `%s'.",
		    filename);
    return (FALSE);
  }


  /*
   * Get the file set entry of this file
   */
  if (!current_fse && current_src_file) {
    current_fse = find_fse(current_src_file);
    if (!current_fse) {
      display_message(this, 
		      "Warning: cannot find the corresponding file set entry.");
    }
  }
  
  /*
   * Create new output tree
   */
  outtree->clear();

  int num_procs = 0;
  current_fse->reset_proc_iter();
  while (current_fse->next_proc()) {
    num_procs++;
  }

  current_fse->reset_proc_iter();
  int i= 0;
  proc_sym *psym;
  while ((psym = current_fse->next_proc())) {
    if (!psym->is_in_memory()) {
      psym->read_proc();
    }
    tree_proc *proc = psym->block();
    if (proc) {
      /* build the output tree */
      outtree->build(proc);
    }

    i++;
    post_progress(this, "Loading output file..", ((float) i)/num_procs*100);
  }


  /* create tags */
  outtree->create_tags(text);

  unpost_progress(this);

  return (TRUE);
}

/*--------------------------------------------------------------------
 * preprocess_c
 *
 * This is a very naive C preprocessor that removes / * and * / comments,
 * # directives, escape codes beginning with '\', and strings are ignored.
 *
 * It is assumed that s2c doesn't generate complicated C codes.
 *
 */
static char *preprocess_c(char *line, int &state, const char *&current_src_file,
			  int &current_src_line, boolean &found_line_num)
{
  found_line_num = FALSE;
  char *buffer = new char[strlen(line) + 1];
  char *buffer_ptr = buffer;

  for (char *p = line; *p; p++) {
    if (state == COMMENT_STATE) {
      if (p[0] == '*' && p[1] == '/') {
	state = NORMAL_STATE;
	p++;
      }
    } else {
      if (p[0] == '/' && p[1] == '*') {
	state = COMMENT_STATE;

	/* check if this is a line number comment */
	int line_num;
	char src_file[200];
	if (sscanf(p, "/* line: %d \"%s\" */", 
		   &line_num, src_file) == 2) {
	  
	  /* this line contains the line number and file info */
	  int l = strlen(src_file);
	  if (src_file[l-1] == '\"') src_file[l-1] = 0;
	  current_src_file = lexicon->enter(src_file)->sp;
	  current_src_line = line_num;
	  found_line_num = TRUE;
	}
	p++;

      } else {

	*buffer_ptr++ = *p;
      }
    }
  }

  *buffer_ptr = 0;
  return (buffer);
}

/*--------------------------------------------------------------------
 * output_viewer::map_tree_node
 *
 * This is a mapping function from a tree-node to the corresponding
 * line number on the output file
 */
code_range output_viewer::map_tree_node(tree_node *tn, output_viewer *viewer)
{
  /* find the src file and src line */
  const char *file;
  int line = find_source_line(tn, &file);
  if (line) {

    int possible_line = 0;

    proc_sym *proc = tn->proc();
    
    /* iterate through the scopes */
    scope_node_list_iter s_iter(viewer->proc_scopes);
    while (!s_iter.is_empty()) {
      scope_node *scope = s_iter.step();
      if (scope->proc == proc) {
	
	/* this is the correct proc */
	if (tn->is_proc()) {
	  return (code_range(scope->first_line, scope->last_line));
	}
	
	output_node_list_iter iter(&scope->nodes);
	while (!iter.is_empty()) {
	  output_node *node = iter.step();
	  if (node->src_line == line &&
	      node->src_file == file) {

	    if (!node->mapped) {
	      node->mapped = TRUE;
	      return (code_range(node->out_line, node->out_line));
	    } else {
	      possible_line = node->out_line;
	    }
	  }
	}
      }
    }

    return (code_range(possible_line, possible_line));

  }
  return (code_range(0,0));
}

/*--------------------------------------------------------------------
 * output_viewer::find_fse
 *
 */
file_set_entry *output_viewer::find_fse(const char *src_file)
{
  file_set_entry *the_fse = NULL;

  /* find the file set entry */
  fileset->reset_iter();
  file_set_entry *fse;
  while ((fse = fileset->next_file())) {
    if (find_source_file(fse) == src_file) {
      the_fse = fse;
      break;
    }
  }
  return (the_fse);
}

/*--------------------------------------------------------------------
 * output_viewer::view
 *
 */
void output_viewer::view(file_set_entry *fse, char *out_file)
{
  if (!current_fse || current_fse != fse) {
    clear();

    char *out_filename = NULL;

    if (!out_file) {
      const char *src_filename = fse->name();
      out_filename = new char[strlen(src_filename) + 10];
      sprintf(out_filename, "%s.out.c", src_filename);
      out_file = out_filename;
    }

    const char *dir = fileset_dir();
    char *buffer = new char[strlen(dir) + strlen(out_file) + 1];
    construct_pathname(buffer, dir, out_file);

    current_file = lexicon->enter(buffer)->sp;
    current_fse = fse;

    delete (buffer);
    if (out_filename) delete (out_filename);

    print_output(current_file);
    update_infobar();
  }
}

/*--------------------------------------------------------------------
 * output_viewer::view
 *
 * view a tree node
 */
void output_viewer::view(tree_node *tn, boolean select)
{
  proc_sym *psym = tn->proc();
  if (psym) {
    file_set_entry *fse = psym->file();
    view(fse, NULL);
  }

  code_fragment *f = outtree->lookup(tn);
  if (f) {
    text->view(f->first_line(), 0);
    if (select) {
      vnode *vn = vman->find_vnode(f);
      text->select(vn);
    }
  }
}

/*--------------------------------------------------------------------
 * event handler
 *
 * when a event occurs, this function is invoked
 */
void output_viewer::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case SELECTION:
    {
      vnode *vn = e.get_object();
      void *event_source = e.get_source();
      if (event_source == text) return; // ignore local event

      show(vn);
    }
    break;

  case CLOSE_FILESET:
    {
      clear();
    }
    break;

  default:
    break;
  }
}

/*--------------------------------------------------------------------
 * output_viewer::show
 *
 */
void output_viewer::show(vnode *vn)
{
  const char *tag = vn->get_tag();

  tree_node *tn;
  if (tag == tag_suif_object) {
    
    suif_object *obj = (suif_object *) vn->get_object();
    
    switch (obj->object_kind()) {
    case FILE_OBJ:
      {
	view((file_set_entry *) obj, NULL);
      }
      break;

    case TREE_OBJ:
      {
	/* tree_proc */
	tn = (tree_node *) obj;
	view(tn, TRUE);
      }
      break;
    case SYM_OBJ:
      {
	if (((sym_node *) obj)->is_proc()) {
	  /* proc_sym */
	  proc_sym *psym = (proc_sym *) obj;
	  if (!psym->is_in_memory()) {
	    psym->read_proc();
	  }
	  tn = psym->block();
	  if (tn) view(tn, TRUE);
	}
      }
      break;
      
    case INSTR_OBJ:
      {
	tn = ((instruction *) obj)->parent();
	if (tn) view(tn, TRUE);
      }
      break;
    default:
      break;
    }
    
  } else if (tag == tag_code_fragment) {
    tn = ((code_fragment *) vn->get_object())->node();
    if (tn) view(tn, TRUE);
  }
}
