/*--------------------------------------------------------------------
 * suif_viewer.h
 *
 * The "suif_viewer" displays SUIF codes of a procedure.
 *
 */

#ifndef SUIF_VIEWER_H
#define SUIF_VIEWER_H

#include "includes.h"
#include "suif_print.h"
#include "base_viewer.h"
#include "suif_utils.h"

class code_fragment;

/*
 * proc_printer
 */

class proc_printer : public suif_printer {
  typedef suif_printer inherited;

public:
  proc_printer(void) {
    filter_mrk = TRUE;
    /* override */
    print_instruction_fn = &print_instruction_filter;
    to_print_instr_annotes = TRUE;
  }

  boolean filter_mrk;
  static void print_instruction_filter(suif_printer *pr, vtty *text,
				       instruction *instr, 
				       int depth, int elab, int *en);
};

/*----------------------------------------------------------------------
 * suif_viewer
 */

class suif_viewer : public text_base_viewer {
  typedef text_base_viewer inherited;

protected:
  vmessage *info_bar;

  proc_sym *current_proc;
  proc_printer proc_pr;

  struct {
    int type;
    const char *annote_name;
    int block_kind;
  } find_info;

  /* menu */
  virtual void create_proc_menu(void);
  virtual void create_edit_menu(void);
  virtual void create_view_menu(void);
  virtual void create_find_menu(void);

  /* display */
  void clear(void);
  void update_info_bar(void);
  void print_proc(tree_proc *proc);

  /* find */
  void find_next_helper(match_fn fn, void *client_data,
			boolean search_forward);

  /* view */
  void show_node(tree_node *tn);
  void select_node(vnode *vn);

  /* static functions */
  static void find_block_cmd(event &e, suif_viewer *viewer,
			     void *block_kind);
  static void find_annote_cmd(event &e, suif_viewer *viewer, const char *name);
  static void find_node_cmd(event &e, suif_viewer *viewer);
  static void find_instr_cmd(event &e, suif_viewer *viewer);
  
  static void find_next_cmd(const event &, suif_viewer *viewer,
			    void *search_forward);

  static void collapse_all_cmd(event &e, suif_viewer *viewer);
  static void expand_all_cmd(event &e, suif_viewer *viewer);
  static void filter_mrk_cmd(event &e, suif_viewer *viewer);

public:
  suif_viewer(void);
  ~suif_viewer(void);

  virtual void create_window(void);
  virtual char *class_name(void) { return "Suif Viewer"; }
  virtual void handle_event(event &e);

  /* selection */
  void select(code_fragment *f);
  void select(tree_node *tn);
  void select(instruction *instr);

  /* view */
  void view(proc_sym *psym);
  void view(code_fragment *f);
  void view(tree_node *tn);
  void view(instruction *instr);
  void refresh(void);

  static window *constructor(void) {
    return (new suif_viewer);
  }
};

#endif
