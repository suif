/*-------------------------------------------------------------------
 * src_tree.cc
 *
 */

#include "src_tree.h"
#include <stdlib.h>
#include <values.h>
#include "suif_utils.h"
#include "suif_vnode.h"

/*--------------------------------------------------------------------
 * src_tree::src_tree
 */
src_tree::src_tree(void)
{
  root = new code_fragment;
  map_fn = NULL;
}

/*--------------------------------------------------------------------
 * src_tree::~src_tree
 *
 */
src_tree::~src_tree(void)
{
  clear();
  delete (root);
}

/*--------------------------------------------------------------------
 * src_tree::clear
 *
 */
void free_fragment(code_fragment *f)
{
  code_fragment *next_child;
  for (code_fragment *child = f->son; child; child = next_child) {
    free_fragment(child);
    next_child = child->next;

    delete (child);
  }
  f->son = f->last_son = NULL;
}

void src_tree::clear(void)
{
  free_fragment(root);
}

/*--------------------------------------------------------------------
 * src_tree::build
 *
 */
void src_tree::build(tree_proc *proc)
{
  map_to_source(proc, NULL, root);
}

/*--------------------------------------------------------------------
 * src_tree::map_to_source
 *
 * Create the map between the source code and suif nodes
 *
 */
code_fragment *src_tree::map_to_source(tree_node *tn, code_fragment *current_f,
				       code_fragment *parent_f)
{
  if (tn->is_instr()) {

    /*
     * Instruction node
     *
     * Find the line number. If different from last line number,
     * create a new node.
     */

    /* instruction node */
    instruction *instr = ((tree_instr *) tn)->instr();

#if 1
    if (instr->opcode() == io_mrk) {
      /* ignore it */
      return (current_f);
    }
#endif

    src_range r = (*map_fn)(tn, client_data);
    if (!r.first_line) {
      /* no line number info */
      if (current_f) {
	current_f->instrs->append(instr);
      }
    } else {
      /* line number found */
      if (!current_f || current_f->last_line != r.first_line) {
	current_f = new code_fragment;
	current_f->first_line = r.first_line;
	current_f->last_line = r.last_line;
	current_f->tn = tn;
	parent_f->add_son(current_f);
      }
      current_f->instrs->append(instr);
    }

  } else {

    /*
     * Not a leaf node. Scan children also.
     */

    src_range r = (*map_fn)(tn, client_data);
  
    code_fragment *new_f = new code_fragment;
    new_f->tn = tn;
    if (r.first_line) {
      new_f->first_line = r.first_line;
      new_f->last_line = r.last_line;
    } else {
      new_f->first_line = MAXINT;
      new_f->last_line = 0;
    }

    /* 
     * Map children
     *
     * Determine the first and start line of current node by taking the
     * min and max lines of the children.
     *
     */
    code_fragment *child_f = NULL;
    code_fragment *prev_child_f = NULL;
    int n = tn->num_child_lists();
    for (int i = 0; i < n; i++) {
      tree_node_list *l = tn->child_list_num(i);

      for (tree_node_list_e *e = l->head(); e; e = e->next()) {
	tree_node *child = e->contents;

	child_f = map_to_source(child, child_f, new_f);
	if (child_f) {
	  if (prev_child_f && prev_child_f != child_f) {
	    /* adjust the last line of the previous child fragment
	     * to make sure no lines are skipped. */
	    prev_child_f->last_line = 
	      MAX(child_f->first_line - 1, prev_child_f->last_line);
	  }
	  new_f->first_line = MIN(new_f->first_line,
				  child_f->first_line);
	  new_f->last_line = MAX(new_f->last_line,
				 child_f->last_line);
	}
	prev_child_f = child_f;
      }
    }

    /*
     * Check code fragment. If it has no line info, then
     * discard this node.
     */
    if (new_f->first_line == MAXINT) {
      delete (new_f);
    } else {
      parent_f->add_son(new_f);
      current_f = new_f;
    }
  }

  return (current_f);
}


/*--------------------------------------------------------------------
 * src_tree::lookup
 *
 * lookup the code_fragment that corresponds with a tree node
 */

static code_fragment *find_fragment(code_fragment *f, tree_node *tn)
{
  if (f->tn == tn) return f;
  if (tn->is_instr()) {
    if (f->instrs->lookup(((tree_instr *) tn)->instr())) {
      return (f);
    }
  }

  for (code_fragment *child = f->son; child; child = child->next) {
    code_fragment *n = find_fragment(child, tn);
    if (n) return (n);
  }
  return (NULL);
}

code_fragment *src_tree::lookup(tree_node *tn)
{
  tree_proc *proc = tn->proc()->block();
  for (code_fragment *proc_f = root->son; proc_f; proc_f = proc_f->next) {
    if (proc_f->tn == proc) {
      /* this is the procedure, now, look for the code_fragment! */
      return (find_fragment(proc_f, tn));
    }
  }
  return (NULL);
}

/*--------------------------------------------------------------------
 * create_tags
 *
 */
void src_tree::create_tags(vtext *text)
{
  for (code_fragment *f = get_root()->son; f; f = f->next) {
    create_tags(f, text->root_tag());
  }
}

void src_tree::create_tags(code_fragment *f, tag_node *parent_tag)
{
  /* create tags */
  assert(f->tn); // this should not be NULL
  vnode *vn = new vnode(f, tag_code_fragment);
  vman.add_vnode(vn);

  tag_node *tag = new tag_node;
  tag->set_object(vn);
  tag->set_begin_coord(text_coord(f->first_line, 0));
  tag->set_end_coord(text_coord(f->last_line + 1, 0));
  parent_tag->add_son(tag);

  /* children */
  for (code_fragment *child = f->son; child; child = child->next) {
    create_tags(child, tag);
  }
}

/*--------------------------------------------------------------------
 * print
 *
 */
void src_tree::print(FILE *fd)
{
  for (code_fragment *f = get_root()->son; f; f = f->next) {
    fprintf(fd, "Proc '%s'\n", f->tn->proc()->name());
    print_helper(fd, f, 0);
  }
}

void src_tree::print_helper(FILE *fd, code_fragment *f, int depth)
{
  suif_indent(fd, depth);
  fprintf(fd, "- (first: %d, last: %d, tn = %lx)\n",
	  f->first_line, f->last_line, (long) f->tn);

  for (code_fragment *child = f->son; child; child = child->next) {
    print_helper(fd, child, depth + 1);
  }
}
