/*-------------------------------------------------------------------
 * misc_viewers.cc
 *
 */

#include "misc_viewers.h"
#include "suif_vnode.h"
#include "suif_print.h"
#include "suif_event.h"
#include "suif_utils.h"
#include <stdlib.h>

/*--------------------------------------------------------------------
 * proc_iter_viewer::proc_iter_viewer
 */
proc_iter_viewer::proc_iter_viewer(void)
{
  iter_fn = NULL;
  writeback = FALSE;
  exp_trees = TRUE;
  use_fortran_form = TRUE;
}

/*--------------------------------------------------------------------
 * proc_iter_viewer::~proc_iter_viewer
 *
 */
proc_iter_viewer::~proc_iter_viewer(void)
{
}

/*--------------------------------------------------------------------
 * proc_iter_viewer::create_window
 */
void proc_iter_viewer::create_window(void)
{
  inherited::create_window();
  create_menu();
}

/*--------------------------------------------------------------------
 * proc_iter_viewer::create_menu
 */
void proc_iter_viewer::create_menu(void)
{
  add_close_command(menu, "Procedure");
}

/*--------------------------------------------------------------------
 * proc_iter_viewer::do_iterate
 */
void proc_iter_viewer::do_iterate(event &, proc_iter_viewer *viewer)
{
  viewer->text->clear();
  viewer->reset_iter();
  while (viewer->next_proc());
  viewer->text->update();
}

/*--------------------------------------------------------------------
 * proc_iter_viewer::iterate
 */
void proc_iter_viewer::iterate(void)
{
  text->clear();
  reset_iter();
  while (next_proc());
  text->update();
}

/*--------------------------------------------------------------------
 * proc_iter_viewer::reset_iter
 */
void proc_iter_viewer::reset_iter(void)
{
  fileset->reset_iter();
  current_fse = fileset->next_file();
}

/*--------------------------------------------------------------------
 * proc_iter_viewer::next_proc
 */
boolean proc_iter_viewer::next_proc(void)
{
  if (!current_fse)
    return FALSE;

  proc_sym *ps = current_fse->next_proc();
  while (!ps) {
    current_fse = fileset->next_file();
    if (!current_fse) return FALSE;
    ps = current_fse->next_proc();
  }

  if (!ps->is_in_memory()) {
    ps->read_proc(exp_trees,
		  (ps->src_lang() == src_fortran) ?
		  use_fortran_form : FALSE);
  }

  (iter_fn)(text, ps->block());

  return TRUE;
}

/*--------------------------------------------------------------------
 * event handler
 *
 */
void proc_iter_viewer::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case CLOSE_FILESET:
    {
      text->clear();
      current_fse = NULL;
    }
    break;

  case NEW_FILESET:
    {
      iterate();
    }
    break;

  case SELECTION:
    {
      if (e.get_source() != text) {
	text->select(e.get_object());
	text->view(e.get_object());
      }
    }
    break;

  default:
    break;
  }
}

