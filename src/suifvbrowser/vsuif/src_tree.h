/*--------------------------------------------------------------------
 * src_tree.h
 *
 */

#ifndef SRC_TREE_H
#define SRC_TREE_H

#include "includes.h"

struct code_fragment {
  int first_line;
  int last_line;
  instruction_list *instrs;
  tree_node *tn;

  code_fragment *son;
  code_fragment *last_son;
  code_fragment *next;

  code_fragment(void) {
    instrs = new instruction_list;
    son = last_son = next = NULL;
    tn = NULL;
  }
  ~code_fragment(void) {
    delete instrs;
  }
  void add_son(code_fragment *f) {
    if (last_son) {
      last_son->next = f;
    } else {
      son = f;
    }
    last_son = f;
  }
};

DECLARE_LIST_CLASS(code_fragment_list, code_fragment *);

typedef int (*map_tn_fn)(tree_node *tn, void *client_data);

class src_tree {
private:
  code_fragment *root;

  code_fragment *map_to_source(tree_node *tn, code_fragment *current_f,
			       code_fragment *parent_f);
  static void create_tags(code_fragment *f, tag_node *parent_tag);

  map_tn_fn map_fn;
  void *client_data;

  static void print_helper(FILE *fd, code_fragment *f, int depth);

public:
  src_tree(void);
  virtual ~src_tree(void);

  code_fragment *get_root(void) { return root; }
  void set_map_fn(map_tn_fn fn, void *data) {
    map_fn = fn; client_data = data;
  }

  void clear(void);
  void build(tree_proc *proc);

  virtual code_fragment *lookup(tree_node *tn);
  void create_tags(vtext *text);

  void print(FILE *fd);

};

#endif
