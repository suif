/*--------------------------------------------------------------------
 * code_tree.h
 *
 */

#ifndef CODE_TREE_H
#define CODE_TREE_H

#include "includes.h"

class code_fragment {
  friend class code_tree;

protected:
  int f_line;
  int l_line;
  instruction_list *instrs;
  tree_node *tn;

  code_fragment *son;
  code_fragment *last_son;
  code_fragment *next;

  void free_subtree(void);

public:
  code_fragment(tree_node *node);
  ~code_fragment(void);

  code_fragment *child() { return son; }
  code_fragment *last_child() { return last_son; }
  code_fragment *next_sib() { return next; }

  instruction_list *instr_list() { return instrs; }
  tree_node *node() { return tn; }
  int first_line() { return f_line; }
  int last_line() { return l_line; }

  void add_son(code_fragment *f);
  code_fragment *find_fragment(tree_node *tn);
};

DECLARE_LIST_CLASS(code_fragment_list, code_fragment *);

struct code_range {
  int first_line;
  int last_line;

  code_range(int first, int last) {
    first_line = first;
    last_line = last;
  }
};

typedef code_range (*map_tn_fn)(tree_node *tn, void *client_data);

class code_tree {
private:
  code_fragment *root;

  code_fragment *map_to_source(tree_node *tn, code_fragment *current_f,
			       code_fragment *parent_f);
  static void create_tags(code_fragment *f, tag_node *parent_tag);

  map_tn_fn map_fn;
  void *client_data;

  static void print_helper(FILE *fd, code_fragment *f, int depth);

public:
  code_tree(void);
  virtual ~code_tree(void);

  void *id;			// for attaching additional identifying info

  code_fragment *get_root(void) { return root; }
  void set_map_fn(map_tn_fn fn, void *data) {
    map_fn = fn; client_data = data;
  }
  
  void clear(void);
  void build(tree_proc *proc);
  virtual code_fragment *lookup(tree_node *tn);

  void create_tags(vtext *text);
  void suif_to_code_prop(vprop *suif_prop, vprop *code_prop);

  void print(FILE *fd);

};

DECLARE_LIST_CLASS(code_tree_list, code_tree *);


#endif
