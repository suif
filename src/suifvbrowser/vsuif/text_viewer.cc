/*-------------------------------------------------------------------
 * text_viewer
 *
 */

#include "text_viewer.h"
#include "suif_vnode.h"
#include <stdlib.h>

/*--------------------------------------------------------------------
 * text_viewer::text_viewer
 */
text_viewer::text_viewer(void)
{
}

/*--------------------------------------------------------------------
 * text_viewer::~text_viewer
 *
 */
text_viewer::~text_viewer(void)
{
}

/*--------------------------------------------------------------------
 * text_viewer::create_window
 */
void text_viewer::create_window(void)
{
  inherited::create_window();

  binding *b = new binding((bfun) &do_open, this);
  menu->add_command(b, "File", "Open..");

  b = new binding((bfun) &do_close, this);
  menu->add_command(b, "File", "Close");
}

/*--------------------------------------------------------------------
 * text_viewer::do_open
 *
 */
void text_viewer::do_open(event &, text_viewer *viewer)
{
  char filename[200];
  select_file(viewer, filename);
  if (filename[0] == 0) {
    return;
  }
  viewer->open(filename);
}

/*--------------------------------------------------------------------
 * text_viewer::do_close
 *
 */
void text_viewer::do_close(event &, text_viewer *viewer)
{
  viewer->destroy();
}

/*--------------------------------------------------------------------
 * text_viewer::open
 *
 */
void text_viewer::open(char *filename)
{
  text->clear();
  if (text->insert_file(filename) == -1) {
    fprintf(text->fd(), "Cannot open file '%s'\n", filename);
    text->update();
  }
}

/*--------------------------------------------------------------------
 * text_viewer::clear
 *
 */
void text_viewer::clear(void)
{
  text->clear();
}

/*--------------------------------------------------------------------
 * text_viewer::insert_text
 *
 */
void text_viewer::insert_text(char *str)
{
  fputs(str, text->fd());
  text->update();
}
