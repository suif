/*--------------------------------------------------------------------
 * cg_viewer.h
 *
 * The "cg_viewer" class displays the call graph of the current file set.
 *
 */

#ifndef CG_VIEWER_H
#define CG_VIEWER_H

#include "includes.h"
#include "base_viewer.h"

class cg;

class cg_viewer : public graph_base_viewer {
  typedef graph_base_viewer inherited;

private:
  int num_nodes;
  gnode **nodes;
  boolean to_show_external_procs;
  cg *current_cg;

  void show_cg(cg *callgraph);

  void show(vnode *vn);
  void create_proc_menu(void);
  void create_view_menu(void);

  static void do_show_external_procs(event &e, cg_viewer *viewer);

public:
  cg_viewer(void);
  ~cg_viewer(void);
  virtual void create_window(void);
  virtual char *class_name(void) { return "Callgraph Viewer"; }
  virtual void handle_event(event &e);

  void clear(void);
  void init(void);

  void select(proc_sym *psym);
  void select_add(proc_sym *psym);

  void set_binding(binding *b);

  static window *constructor(void) {
    return (new cg_viewer);
  }

};

#endif
