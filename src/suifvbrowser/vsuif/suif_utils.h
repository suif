/*
 * suif_utils.h
 */

#ifndef SUIF_UTILS_H
#define SUIF_UTILS_H

#include "includes.h"

tree_node *next_tree_node(tree_node *tn);
tree_node *prev_tree_node(tree_node *tn);
tree_node *last_descendent(tree_node *tn);

DECLARE_LIST_CLASS(string_list, const char *);

/*
 * Mapping functions 
 */
const char *find_source_file(file_set_entry *fse);
const char *find_source_file(tree_node *tn);
int find_source_line(tree_node *tn, const char **filename = NULL);
tree_node *map_src_to_tree_node(const char *filename, int line);
tree_node *map_line_to_tree_node(tree_node *root, int line);

/*
 * Source language
 */
src_lang_type get_fse_src_lang(file_set_entry *fse);

/*
 * Annotations
 */
string_list *get_annotation_names(vnode_list *nodes);
string_list *get_annotation_names(tree_node *tn);
vnode_list *select_by_annote_name(vnode_list *nodes, char *name);

/*
 * Search functions
 */
typedef boolean (*match_fn)(suif_object *obj, void *client_data);

instruction *find_instr(instruction *instr, match_fn fn, 
			void *client_data);
suif_object *find_tree_node(tree_node *tn, match_fn fn, void *client_data,
			    boolean search_forward);
instruction *find_instr_by_num(tree_proc *proc, int inum);

/*
 * Misc
 */
const char *fileset_dir(void);
void construct_pathname(char *buffer, const char *dir, const char *filename);

proc_sym *get_parent_proc(vnode *vn);
proc_sym *get_parent_proc(suif_object *obj);
file_set_entry *get_parent_fse(vnode *vn);
file_set_entry *get_parent_fse(suif_object *obj);
base_symtab *get_enclosing_symtab(suif_object *obj);

/*
 * High level functions
 */

typedef void (*visual_prociter_f)(vtty *tty, tree_proc * p);

void visual_suif_proc_iter(int argc, char * argv[], visual_prociter_f fun, 
			   boolean writeback=FALSE,
			   boolean exp_trees=TRUE,
			   boolean use_fortran_form=TRUE);

#endif
