/*-------------------------------------------------------------------
 * suif_print.h
 *
 */

#ifndef SUIF_PRINT_H
#define SUIF_PRINT_H

#include "includes.h"

/*
 * This is the default suif printing class.
 */

class suif_printer {

protected:
  boolean to_print_instr_annotes;

  /* Default printing functions */
  static void def_print_fse(suif_printer *pr, vtty *text, file_set_entry *fse,
			    int depth, int detail);
  static void def_print_tree_node(suif_printer *pr, vtty *text, tree_node *tn,
				  int depth, int detail);
  static void def_print_tree_node_list(suif_printer *pr, vtty *text, 
				       tree_node_list *l, int depth);
  static void def_print_instruction(suif_printer *pr, vtty *text,
				    instruction *instr, int depth,
				    int elab, int *en);
  static void def_print_symtab(suif_printer *pr, vtty *text,
			       base_symtab *symtab, int depth, int detail);
  static void def_print_sym_node(suif_printer *pr, vtty *text, sym_node *sym,
				 int depth, int detail);
  static void def_print_var_def(suif_printer *pr, vtty *text, var_def *def,
				int depth, int detail);
  static void def_print_type_node(suif_printer *pr, vtty *text, 
				  type_node *type, int depth, int detail);
  static void def_print_annotations(suif_printer *pr, vtty *text,
				    suif_object *obj, int depth, int detail);
  static void def_print_annote(suif_printer *pr, vtty *text,
			       annote *ann, int depth, int detail);

public:
  suif_printer();

  /* print function pointers */
  void (*print_fse_fn)(suif_printer *pr, vtty *text, file_set_entry *fse,
		       int depth, int detail);
  void (*print_tree_node_fn)(suif_printer *pr, vtty *text, tree_node *tn, 
			     int depth, int detail);
  void (*print_tree_node_list_fn)(suif_printer *pr, vtty *text,
			       tree_node_list *tn, int depth);
  void (*print_instruction_fn)(suif_printer *pr, vtty *text,
			       instruction *instr,
			       int depth, int elab, int *en);
  void (*print_symtab_fn)(suif_printer *pr, vtty *text, base_symtab *symtab,
			  int depth, int detail);
  void (*print_sym_node_fn)(suif_printer *pr, vtty *text, sym_node *sym,
			    int depth, int detail);
  void (*print_var_def_fn)(suif_printer *pr, vtty *text, var_def *def,
			   int depth, int detail);
  void (*print_type_node_fn)(suif_printer *pr, vtty *text, type_node *type, 
			     int depth, int detail);
  void (*print_annotations_fn)(suif_printer *pr, vtty *text, suif_object *obj, 
			       int depth, int detail);
  void (*print_annote_fn)(suif_printer *pr, vtty *text, annote *ann,
			  int depth, int detail);

  /* methods */
  static void indent(FILE *fd, int depth);
  void print_suif_object(vtty *text, suif_object *obj,
			 int depth = 0, int detail = PRINT_FULL);
  char *get_object_name(suif_object *obj);
  char *get_object_description(suif_object *obj);

  /* print functions */
  void print_fse(vtty *text, file_set_entry *fse, int depth = 0,
		 int detail = PRINT_FULL) {
    print_fse_fn(this, text, fse, depth, detail);
  }
  void print_tree_node(vtty *text, tree_node *tn, int depth = 0,
		       int detail = PRINT_FULL) {
    print_tree_node_fn(this, text, tn, depth, detail);
  }
  void print_tree_node_list(vtty *text, tree_node_list *tn,
			    int depth = 0) {
    print_tree_node_list_fn(this, text, tn, depth);
  }
  void print_instruction(vtty *text, instruction *instr,
			 int depth = 0, int elab = 0, int *en = NULL) {
    print_instruction_fn(this, text, instr, depth, elab, en);
  }
  void print_symtab(vtty *text, base_symtab *symtab,
		    int depth = 0, int detail = PRINT_FULL) {
    print_symtab_fn(this, text, symtab, depth, detail);
  }
  void print_sym_node(vtty *text, sym_node *sym,
		      int depth = 0, int detail = PRINT_FULL) {
    print_sym_node_fn(this, text, sym, depth, detail);
  }
  void print_var_def(vtty *text, var_def *def, int depth = 0,
		     int detail = PRINT_FULL) {
    print_var_def_fn(this, text, def, depth, detail);
  }
  void print_type_node(vtty *text, type_node *type, 
		       int depth = 0, int detail = PRINT_FULL) {
    print_type_node_fn(this, text, type, depth, detail);
  }
  void print_annotations(vtty *text, suif_object *obj,
			 int depth = 0, int detail = PRINT_FULL) {
    print_annotations_fn(this, text, obj, depth, detail);
  }
  void print_annote(vtty *text, annote *ann, int depth = 0,
		    int detail = PRINT_FULL) {
    print_annote_fn(this, text, ann, depth, detail);
  }
};

extern suif_printer suif_pr;


#endif
