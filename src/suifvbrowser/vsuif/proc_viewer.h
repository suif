/*--------------------------------------------------------------------
 * proc_viewer.h
 *
 */

#ifndef PROC_VIEWER_H
#define PROC_VIEWER_H

#include "includes.h"
#include "suif_print.h"

class code_fragment;

typedef boolean (*match_fn)(tree_node *tn, void *client_data);

/*
 * proc_printer
 */

class proc_printer : public suif_printer {
private:
  boolean filter_mrk;

public:
  proc_printer(void) {
    filter_mrk = FALSE;
  }
  boolean get_filter_mrks(void) { return filter_mrk; }
  void set_filter_mrks(boolean b) { filter_mrk = b; }

  /* override */
  virtual void print_instruction(vtext *text, instruction *instr, int depth);
};

/*
 * proc_viewer
 */

class proc_viewer : public viewer {

 private:
  vframe *frame;
  vmenu *menu;
  vtext *text;
  vmessage *message;
  vprop *highlight;

  tree_proc *current_proc;
  proc_printer proc_pr;

  binding_list *item_bindings;
  struct {
    int type;
    char *annote_name;
    int block_kind;
  } find_info;

  /* menu */
  void create_procs_menu(void);
  void create_view_menu(void);

  /* update */
  void update_message(void);
  void update_find_menu(void);

  /* print */
  void print_proc(tree_proc *proc);

  /* find */
  void find_next_helper(match_fn fn, void *client_data,
			boolean search_forward);

  /* view */
  tag_node *show_node(tree_node *tn);

  /* static functions */
  static void local_select_event(event &e, proc_viewer *viewer);
  static void global_select_event(event &e, proc_viewer *viewer);

  static void find_block(event &e, proc_viewer *viewer,
			 void *block_kind);
  static void find_annote(event &e, proc_viewer *viewer, char *name);
  static void find_node(event &e, proc_viewer *viewer);

  static void find_next(event &e, proc_viewer *viewer,
			void *search_forward);
  static void show_proc(event &e, proc_viewer *viewer, proc_sym *psym);
  static void show_proc_list(event &e, proc_viewer *viewer);

  static void collapse_all(event &e, proc_viewer *viewer);
  static void expand_all(event &e, proc_viewer *viewer);
  static void filter_mrks(event &e, proc_viewer *viewer);

 public:
  proc_viewer(void);
  ~proc_viewer(void);

  virtual char *class_name(void) { return "Procedure viewer"; }

  void select(code_fragment *f);
  void select(tree_node *tn);

  void view(proc_sym *psym);
  void view(tree_proc *proc);
  void view(code_fragment *f);
  void view(int line);
  void view(tree_node *tn);

  static window *constructor(void) {
    return (new proc_viewer);
  }

};


#endif
