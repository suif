/*-------------------------------------------------------------------
 * suif_menu
 *
 */

#include "suif_menu.h"
#include "suif_vnode.h"
#include "ann_form.h"
#include "suif_event.h"
#include "suif_utils.h"
#include "suif_print.h"
#include "code_tree.h"
#include <stdlib.h>
#include <string.h>

static void do_show_proc_list(const event &, void *);
static void do_show_proc(const event &, proc_sym *psym);
static void do_show_fse(const event &, file_set_entry *fse);

static void do_edit_annote(const event &, void *);
static void do_remove_annote(const event &, void *);
static void do_add_annote(const event &, void *);

static void do_go_cmd(const event &e, void *);
static void do_go_to_node_cmd(const event &, vnode *vn);
static void do_go_back_cmd(const event &, void *);

/*--------------------------------------------------------------------
 * add_std_fse_menu
 *
 */
void add_std_fse_menu(vmenu *root_menu, char *parent_menu)
{
  size_t bl = 200;
  char *buffer = new char[200];

  /* file set entry submenu */
  fileset->reset_iter();
  file_set_entry *fse;
  while ((fse = fileset->next_file())) {
    binding *b = new binding((bfun) &do_show_fse, fse);

    if (strlen(fse->name()) + 10 > bl) {
      bl <<=1;
      delete (buffer);
      buffer = new char[bl];
    }
    sprintf(buffer, "`%s'", fse->name());
    root_menu->add_command(b, parent_menu, buffer);
  }
  delete (buffer);
}

/*--------------------------------------------------------------------
 * add_std_proc_menu
 *
 */
void add_std_proc_menu(vmenu *root_menu, char *parent_menu)
{
  size_t bl = 200;
  char *buffer = new char[200];

  /* procedure submenu */
  sym_node_list proc_symbols;

  int num_procs = 0;
  fileset->reset_iter();
  file_set_entry *fse;
  while ((fse = fileset->next_file())) {
    fse->reset_proc_iter();
    proc_sym *psym;
    while ((psym = fse->next_proc())) {
      if (psym->is_readable()) {
	proc_symbols.append(psym);
	num_procs++;
	if (num_procs > 10) break;
      }
    }
  }

  if (num_procs <= 10) {
    /* add the procedures to the menu */
    sym_node_list_iter iter(&proc_symbols);
    while (!iter.is_empty()) {
      proc_sym *psym = (proc_sym *) iter.step();
      binding *b = new binding((bfun) &do_show_proc, psym);

      if (strlen(psym->name())+10 > bl) {
	bl <<=1;
	delete (buffer);
	buffer = new char[bl];
      }
      sprintf(buffer, "`%s'", psym->name());
      root_menu->add_command(b, parent_menu, buffer);
    }
    root_menu->add_separator(parent_menu);
  }
  
  /* add procedure list command to the menu */
  binding *b = new binding((bfun) &do_show_proc_list, NULL);
  root_menu->add_command(b, parent_menu, "Procedure List..");

  delete (buffer);
}

/*--------------------------------------------------------------------
 * show_proc_list
 *
 */
static void do_show_proc_list(const event &, void *)
{
  vman->show_window(vman->find_window_class("Procedure List"));
}

/*--------------------------------------------------------------------
 * show_proc
 *
 */
static void do_show_proc(const event &, proc_sym *psym)
{
  vnode *vn = create_vnode(psym);
  post_event(event(vn, SELECTION, NULL));
}

/*--------------------------------------------------------------------
 * show_fse
 *
 */
static void do_show_fse(const event &, file_set_entry *fse)
{
  vnode *vn = create_vnode(fse);
  post_event(event(vn, SELECTION, NULL));
}

/*--------------------------------------------------------------------
 * add_std_edit_menu
 *
 */
void add_std_edit_menu(vmenu *root_menu, char *parent_menu)
{
  binding *b = new binding((bfun) &do_edit_annote, NULL);
  root_menu->add_command(b, parent_menu, "Modify Annote..");

  b = new binding((bfun) &do_add_annote, NULL);
  root_menu->add_command(b, parent_menu, "Add Annote..");

  b = new binding((bfun) &do_remove_annote, NULL);
  root_menu->add_command(b, parent_menu, "Remove Annote");
}

/*--------------------------------------------------------------------
 * edit_annote
 *
 */
static void do_edit_annote(const event &, void *)
{
  vnode *vn = vman->get_selection();
  if (!vn || vn->get_tag() != tag_annote) {
    display_message(NULL, 
		    "No annote selected. Please select an annote first.");
    return;
  }

  annote *ann = (annote *) vn->get_object();
  suif_object *obj = (suif_object *) vn->get_data();
  ann_form *form = new ann_form(ann, obj);
  form->create_window();
}

/*--------------------------------------------------------------------
 * remove_annote
 *
 */
static void do_remove_annote(const event &, void *)
{
  vnode *vn = vman->get_selection();
  if (!vn || vn->get_tag() != tag_annote) {
    display_message(NULL, 
		    "No annote selected. Please select an annote first.");
    return;
  }

  annote *ann = (annote *) vn->get_object();
  suif_object *obj = (suif_object *) vn->get_data();

  annote_list *annlist = obj->annotes();
  annote_list_e *le = annlist->lookup(ann);
  if (le) {
    delete annlist->remove(le);

    // clean up
    delete (ann);
    delete (vn);
  }

  /* post event, notifying that the object has been modified */
  proc_sym *psym = get_parent_proc(obj);
  if (psym) {
    vnode *psym_vn = create_vnode(psym);
    post_event(event(psym_vn, PROC_MODIFIED, NULL));
  } else {
    file_set_entry *fse = get_parent_fse(obj);
    if (fse) {
      vnode *fse_vn = create_vnode(fse);
      post_event(event(fse_vn, FSE_MODIFIED, NULL));
    } else {
      assert(FALSE);		// this should not happen..
    }
  }
}

/*--------------------------------------------------------------------
 * add_annote
 *
 */
static void do_add_annote(const event &, void *)
{
  vnode *vn = vman->get_selection();
  if (!vn || vn->get_tag() != tag_suif_object) {
    display_message(NULL, 
		    "Please select a suif object first.");
    return;
  }

  suif_object *obj = (suif_object *) vn->get_object();
  ann_form *form = new ann_form(NULL, obj);
  form->create_window();
}

/*--------------------------------------------------------------------
 * add_std_go_menu
 *
 * This is a dynamic menu. When the menu button is activated, the
 * menu items are then constructed.
 */

void add_std_go_menu(vmenu *root_menu)
{
  binding *b = new binding((bfun) &do_go_cmd, NULL);
  root_menu->add_menu(b, "Go");
}

static void do_go_cmd(const event &e, void *)
{
  binding *b;

  vmenu *menu = (vmenu *) e.get_source();
  menu->clear("Go");

  b = new binding((bfun) &do_go_back_cmd, NULL);
  menu->add_command(b, "Go", "Back");
  
  menu->add_separator("Go");

  /* construct list of previously selected objects */
  vnode_list *hist = vman->get_selection_history();
  int i = 0;

  vnode_list_e *le = hist->head();
  if (le) le = le->next();	// skip the current object
  for (; le; le = le->next()) {
    char node_name[100];
    const char *node_info;
    char node_info_buffer[100];

    vnode *vn = le->contents;
    const char *tag = vn->get_tag();
    if (tag == tag_suif_object) {

      suif_object *obj = (suif_object *) vn->get_object();
      node_info = suif_pr.get_object_description(obj);

    } else if (tag == tag_annote) {

      annote *ann = (annote *) vn->get_object();
      sprintf(node_info_buffer, "`%s'", ann->name());
      node_info = node_info_buffer;

    } else if (tag == tag_code_fragment) {

      code_fragment *f = (code_fragment *) vn->get_object();
      tree_node *tn = f->node();
      node_info =  tn ? suif_pr.get_object_description(tn) : "";

    } else {

      node_info = "";
    }

    sprintf(node_name, "[%s] (0x%p) %s", 
	    tag, vn->get_object(), node_info);

    b = new binding((bfun) &do_go_to_node_cmd, vn);
    menu->add_command(b, "Go", node_name);
    
    i++;
    if (i > 20) break;
  }

}

static void do_go_to_node_cmd(const event &, vnode *vn)
{
  post_event(event(vn, SELECTION, NULL));
}

static void do_go_back_cmd(const event &, void *)
{
  vman->go_back();
}
