/*----------------------------------------------------------------------
 * ann_form
 */

#include "ann_form.h"
#include "includes.h"
#include "base_viewer.h"
#include "suif_event.h"
#include "suif_utils.h"
#include "suif_vnode.h"
#include "suif_types.h"
#include "suif_print.h"

/*----------------------------------------------------------------------
 * ann_form::ann_form
 */
ann_form::ann_form(annote *ann, suif_object *obj)
{
  if (ann) {
    current_ann = ann;
    new_annote = FALSE;
  } else {
    current_ann = new annote("");
    new_annote = TRUE;
  }
  current_obj = obj;
}

/*----------------------------------------------------------------------
 * ann_form::~ann_form
 */
ann_form::~ann_form(void)
{
  if (new_annote) {
    delete current_ann;
  }
}

/*----------------------------------------------------------------------
 * ann_form::destroy
 */
void ann_form::destroy(void)
{
  inherited::destroy();
}

/*----------------------------------------------------------------------
 * ann_form::create_window
 */
void ann_form::create_window(void)
{
  inherited::create_window();

  /* set title */
  char string[200];
  sprintf(string, "Annote: (0x%p) Object: (0x%p) %s",
	  current_ann, current_obj, suif_pr.get_object_name(current_obj));
  set_info_bar(string);

  form->add_field("Annote name", t_string, current_ann->name());
  add_immed_list(current_ann->immeds());

  /* menu */
  binding *b;

  static const char *immed_types[] = {t_string, t_int, t_float,
				t_extended_int, t_extended_float, t_symbol,
				t_type, t_operand, t_instruction, NULL};

  for (const char **t = immed_types; *t; t++) {
    b = new binding((bfun2) &do_insert, this, *t);
    menu->add_command(b, "Edit/Insert Field", *t);
  }

  b = new binding((bfun) &do_delete, this);
  menu->add_command(b, "Edit", "Delete field");

  /* buttons */
  b = new binding((bfun) &do_update, this);
  button_bar->add_button(b, "Ok");

  b = new binding((bfun) &do_cancel, this);
  button_bar->add_button(b, "Cancel");
}

/*----------------------------------------------------------------------
 */
void ann_form::do_update(event &, ann_form *win)
{
  const char *annote_name = lexicon->enter(win->form->get_field_data(0))->sp;
  annote_def *adef = lookup_annote(annote_name);
  if (!adef) {
    char buffer[200];
    sprintf(buffer, "The annote name `%s' is not registered. "
	    "Do you want to register the annotation and continue?",
	    annote_name);
    int result = display_dialog(win, buffer, "Yes No", 0);
    if (result != 0) {
      return;
    }

    adef = new annote_def(annote_name, TRUE);
    register_annote(adef);
  }

  immed_list *immlist = win->get_immed_list(win->current_obj, 1);
  if (immlist) {

    win->current_ann->set_name(annote_name);
    if (win->current_ann->immeds()) 
      delete win->current_ann->immeds();
    win->current_ann->set_immeds(immlist, win->current_obj);

    if (win->new_annote) {
      /* add annote to the object's annote list */
      win->current_obj->annotes()->append(win->current_ann);
      win->new_annote = FALSE;
    }

    /* post event */
    proc_sym *psym = get_parent_proc(win->current_obj);
    if (psym) {

      vnode *psym_vn = create_vnode(psym);
      post_event(event(psym_vn, PROC_MODIFIED));

    } else {
      
      file_set_entry *fse = get_parent_fse(win->current_obj);
      if (fse) {
	vnode *fse_vn = create_vnode(fse);
	post_event(event(fse_vn, FSE_MODIFIED));

      } else {
	assert(FALSE);		// this should not happen..
      }
    }

    win->destroy();
  }
}

/*----------------------------------------------------------------------
 */
void ann_form::do_cancel(event &, ann_form *win)
{
  win->destroy();
}

/*----------------------------------------------------------------------
 * Insert a field
 */
void ann_form::do_insert(event &, ann_form *win, char *type)
{
  int f = win->form->get_current_field();

  int n = win->form->num_fields();
  char **field_data = new char*[n];
  char **field_type = new char*[n];
  int i;
  for (i = 0; i < n; i++) {
    field_data[i] = strdup(win->form->get_field_data(i));
    field_type[i] = strdup(win->form->get_field_type(i));
  }

  win->form->clear();
  win->form->add_field("Annote name", t_string, field_data[0]); 

  int j = 1;
  for (i = 1; i < n+1; i++) {
    char field_name[50];
    sprintf(field_name, "Field %d", i);

    if (i == f+1) {
      win->form->add_field(field_name, type, "");
    } else {
      win->form->add_field(field_name, field_type[j], field_data[j]);
      j++;
    }
  }

  win->form->focus_field(f+1);

  for (i = 0; i < n; i++) {
    delete field_data[i];
    delete field_type[i];
  }
  delete field_data;
  delete field_type;
}

/*----------------------------------------------------------------------
 * Delete a field
 */
void ann_form::do_delete(event &, ann_form *win)
{
  int f = win->form->get_current_field();

  if (f == 0) {
    display_message(win, "Cannot delete annote name.");
    return;
  }

  int n = win->form->num_fields();
  char **field_data = new char*[n];
  char **field_type = new char*[n];
  int i;
  for (i = 0; i < n; i++) {
    field_data[i] = strdup(win->form->get_field_data(i));
    field_type[i] = strdup(win->form->get_field_type(i));
  }

  win->form->clear();
  win->form->add_field("Annote name", t_string, field_data[0]); 

  int j = 1;
  for (i = 1; i < n; i++) {
    char field_name[50];
    sprintf(field_name, "Field %d", j);

    if (i != f) {
      win->form->add_field(field_name, field_type[i], field_data[i]);
      j++;
    }
  }

  win->form->focus_field(f);

  for (i = 0; i < n; i++) {
    delete field_data[i];
    delete field_type[i];
  }
  delete field_data;
  delete field_type;
}

/*----------------------------------------------------------------------
 * handle_event
 */
void ann_form::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case CLOSE_FILESET:
    {
      destroy();		// destroy this form
    }
    break;

  default:
    break;
  }

}
