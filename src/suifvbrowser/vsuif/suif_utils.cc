/*----------------------------------------------------------------------
 * suif_utils.cc
 *
 */

#include "suif_utils.h"
#include "suif_vnode.h"
#include "code_tree.h"
#include "includes.h"
#include "misc_viewers.h"

extern void init_vsuif(void);
extern void exit_vsuif(void);
void start_vsuif(int argc, char *argv[]);

/*----------------------------------------------------------------------
 * next_tree_node
 *
 */
tree_node *next_tree_node(tree_node *tn)
{
  tree_node *node;

  /* search downwards */
  unsigned i;
  for (i = 0; i < tn->num_child_lists(); i++) {
    tree_node_list_e *head = tn->child_list_num(i)->head();
    if (head) {
      return (head->contents);
    }
  }

  while (TRUE) {
    /* search siblings */
    tree_node_list_e *le = tn->list_e();
    if (le && le->next()) {
      node = le->next()->contents;
      return (node);
    }
    
    /* search upwards */
    tree_node_list *l = tn->parent();
    if (l == NULL) {
      break;
    }
    tree_node *parent = l->parent();
    
    /* find the child list number where node is */
    unsigned n = parent->num_child_lists();
    for (i = 0; i < n; i++) {
      if (parent->child_list_num(i) == l) {
	break;
      }
    }
    
    /* find other child lists */
    for (i++; i < n; i++) {
      tree_node_list_e *head = parent->child_list_num(i)->head();
      if (head) {
	return (head->contents);
      }
    }

    tn = parent;
  }

  return (NULL);
}

/*----------------------------------------------------------------------
 * prev_tree_node
 *
 */
tree_node *prev_tree_node(tree_node *tn) 
{
  /* previous node */
  tree_node_list_e *le = tn->list_e();
  if (le == NULL) {
    return (NULL);
  }
  if (le->prev()) {
    return (last_descendent(le->prev()->contents));
  }

  /* search other children list */
  tree_node_list *l = tn->parent();
  if (l == NULL) {
    return (NULL);
  }
  tree_node *parent = l->parent();
  
  /* find the child list number where node is */
  int n = parent->num_child_lists();
  int i;
  for (i = n - 1; i >= 0; i--) {
    if (parent->child_list_num(i) == l) {
      break;
    }
  }

  /* find other child lists */
  for (i--; i >= 0; i--) {
    tree_node_list *l = parent->child_list_num(i);
    if (!l->is_empty()) {
      return (last_descendent(l->tail()->contents));
    }
  }

  return (parent);
}

/*----------------------------------------------------------------------
 * last_descendent
 *
 */
tree_node *last_descendent(tree_node *tn) 
{
  /* search downwards */
  while (TRUE) {
    int n = tn->num_child_lists();
    int i;
    for (i = n - 1; i >= 0; i--) {
      tree_node_list_e *tail = tn->child_list_num(i)->tail();
      if (tail) {
	tn = tail->contents;
	break;
      }
    }
    if (i < 0) break;
  } 

  return (tn);
}

/*--------------------------------------------------------------------
 * get_annotation_names
 *
 * Returns the list of annotation names that are attached to objects
 * in a vnode list
 * Caller is responsible for deleting the list after use.
 */
string_list *get_annotation_names(vnode_list *nodes)
{
  string_list *names = new string_list;

  vnode_list_iter n_iter(nodes);
  while (!n_iter.is_empty()) {
    vnode *vn = n_iter.step();
    if (vn->get_tag() == tag_suif_object) {

      suif_object *obj = (suif_object *)vn->get_object();
      if (obj->are_annotations()) {
	annote_list_iter a_iter(obj->annotes());
	while (!a_iter.is_empty()) {
	  const char *name = a_iter.step()->name();
	  if (!names->lookup(name)) {
	    names->append(name);
	  }
	}
      }
    }
  }
  return (names);
}

/*--------------------------------------------------------------------
 * get_annotation_names
 *
 * Returns the list of annotation names that are attached to a proc
 * Caller is responsible for deleting the list after use.
 */
static void get_ann_names_of_instr(instruction *instr, string_list *names)
{
  if (instr->are_annotations()) {
    annote_list_iter a_iter(instr->annotes());
    while (!a_iter.is_empty()) {
      const char *name = a_iter.step()->name();
      if (!names->lookup(name)) {
	names->append(name);
      }
    }
  }

  int n = instr->num_srcs();
  for (int i = 0; i < n; i++) {
    operand src = instr->src_op(i);
    if (src.is_instr()) {
      get_ann_names_of_instr(src.instr(), names);
    }
  }
}

static void get_ann_names_of_tree_node(tree_node *tn, string_list *names)
{
  if (tn->is_instr()) {
    get_ann_names_of_instr(((tree_instr *)tn)->instr(), names);
  }
  if (tn->are_annotations()) {
    annote_list_iter a_iter(tn->annotes());
    while (!a_iter.is_empty()) {
      const char *name = a_iter.step()->name();
      if (!names->lookup(name)) {
	names->append(name);
      }
    }
  }
}

string_list *get_annotation_names(tree_node *root)
{
  string_list *names = new string_list;
  root->map((tree_map_f) &get_ann_names_of_tree_node, names);
  return (names);
}

/*--------------------------------------------------------------------
 * select_by_annote_name
 *
 */
vnode_list *select_by_annote_name(vnode_list *nodes, char *name)
{
  vnode_list *selected = new vnode_list;

  vnode_list_iter n_iter(nodes);
  while (!n_iter.is_empty()) {
    vnode *vn = n_iter.step();
    if (vn->get_tag() == tag_suif_object) {
      suif_object *obj = (suif_object *)vn->get_object();

      if (obj->are_annotations()) {
	annote_list_iter a_iter(obj->annotes());
	while (!a_iter.is_empty()) {
	  if (a_iter.step()->name() == name) {
	    selected->append(vn);
	    break;
	  }
	}
      }
    }
  }
  return (selected);
}

/*--------------------------------------------------------------------
 * find the source file given a file set entry
 *
 */
const char *find_source_file(file_set_entry *fse)
{
  fse->reset_proc_iter();
  proc_sym *psym;
  while ((psym = fse->next_proc())) {
    if (!psym->is_in_memory()) {
      psym->read_proc();
    }
    const char *src_file = find_source_file(psym->block());
    if (src_file) return (src_file);
  }
  return (NULL);
}

/*--------------------------------------------------------------------
 * find the source file given a tree node
 *
 */
const char *find_source_file(tree_node *tn)
{
  tree_node *node = tn->proc()->block();
  while (node) {
    immed_list *immeds;
    if (node->is_instr()) {
      instruction *instr = ((tree_instr *) node)->instr();
      immeds = (immed_list *) instr->peek_annote(k_line);
    } else {
      immeds = (immed_list *) node->peek_annote(k_line);
    }
    if (immeds) {
      immed val = (*immeds)[1];
      assert(val.is_string());
      return (val.string());
    }
    node = next_tree_node(node);
  }
  return (NULL);
}

/*--------------------------------------------------------------------
 * find_source_line
 */
int find_source_line(tree_node *node, const char **filename)
{
  tree_node *tn = node;
  boolean search_downwards = FALSE;
  while (tn) {
    immed_list *the_immeds = (immed_list *)(tn->peek_annote(k_line));
    if (the_immeds != NULL) {
      int line = (*the_immeds)[0].unsigned_int();
      if (filename) {
	*filename = (*the_immeds)[1].string();
      }
      return (line);
    }

    if (tn->kind() == TREE_INSTR) {
      instruction *instr = ((tree_instr *) tn)->instr();
      if (instr != NULL) {
	immed_list *the_immeds = (immed_list *)(instr->peek_annote(k_line));
	if (the_immeds != NULL) {
	  int line = (*the_immeds)[0].unsigned_int();
	  if (filename) {
	    *filename = (*the_immeds)[1].string();
	  }
	  return (line);
	}
      }
    } else if (tn->is_proc()) {
      search_downwards = TRUE;	// now, search downwards...
      tn = node;
    }

    if (search_downwards) {
      tn = next_tree_node(tn);
    } else {
      tn = prev_tree_node(tn);
    }
  }

  return(0);
}

/*----------------------------------------------------------------------
  * get_fse_src_lang
 */
src_lang_type get_fse_src_lang(file_set_entry *fse)
{
  /* check filename first */
  const char *filename = find_source_file(fse);
  if (filename) {

    int l = strlen(filename);
    if (l > 2) {
      if (strcmp(&filename[l-2], ".f") == 0) {
	return src_fortran;
      }
      if (strcmp(&filename[l-2], ".c") == 0) {
	return src_c;
      }
    }
    
    /* check psym info */
    fse->reset_proc_iter();
    proc_sym *psym;
    while ((psym = fse->next_proc())) {
      src_lang_type lang = psym->src_lang();
      if (lang != src_unknown)
	return (lang);
    }
  }

  return (src_unknown);
}

/*--------------------------------------------------------------------
 * map_line_to_tree_node
 *
 * given a src line and the root of a suif subtree,
 * map it to the suif tree node
 *
 */
tree_node *map_line_to_tree_node(tree_node *root, int line)
{
  unsigned i;
  for (i = 0; i < root->num_child_lists(); i++) {
    for (tree_node_list_e *node_e = root->child_list_num(i)->head();
	 node_e; node_e = node_e->next()) {

      tree_node *child = node_e->contents;

      /* search child */
      tree_node *tn = map_line_to_tree_node(child, line);
      if (tn) return (tn);

      /* look for line annotation */
      immed_list *immeds;
      if (child->is_instr()) {
	instruction *instr = ((tree_instr *) child)->instr();
	immeds = (immed_list *) instr->peek_annote(k_line);
      } else {
	immeds = (immed_list *) child->peek_annote(k_line);
      }
      if (immeds) {
	immed val = (*immeds)[0];
	assert(val.is_int_const());
	if (val.is_integer() && (val.integer() == line)) {
	  return (child);
	}
      }
    }
  }

  return (NULL);
}

/*--------------------------------------------------------------------
 * map_src_to_tree_node
 *
 * given a src file and line, map it to suif tree node
 *
 */
tree_node *map_src_to_tree_node(const char *filename, int line)
{
  fileset->reset_iter();
  file_set_entry *fse;
  while ((fse = fileset->next_file())) {
    if (find_source_file(fse) == filename) {

      fse->reset_proc_iter();
      proc_sym *psym;
      while ((psym = fse->next_proc())) {
	if (!psym->is_in_memory()) {
	  psym->read_proc();
	}
	if (psym->is_readable()) {
	  tree_proc *proc = psym->block();
	  tree_node *tn = map_line_to_tree_node(proc, line);
	  if (tn) return (tn);
	}
      }
    }
  }

  return (NULL);
}

/*----------------------------------------------------------------------
 * fileset_dir
 *
 */
const char *fileset_dir(void)
{
  fileset->reset_iter();
  file_set_entry *fse = fileset->next_file();
  if (!fse) {
    return "";
  }

  char *tmp = new char[strlen(fse->name())+1];
  strcpy(tmp, fse->name());
  char *last_slash = strrchr(tmp, '/');
  if (last_slash) {
    last_slash[0] = 0;
  } else {
    *tmp = 0;
  }
  const char *dir = lexicon->enter(tmp)->sp;
  delete (tmp);
  return (dir);
}

/*----------------------------------------------------------------------
 * construct pathname
 *
 */
void construct_pathname(char *buffer, const char *dir, const char *filename)
{
  if (filename[0] == '/' || dir[0] == 0) {
    strcpy(buffer, filename);
  } else {
    sprintf(buffer, "%s/%s", dir, filename);
  }
}

/*--------------------------------------------------------------------
 * find instr
 *
 */
instruction *find_instr(instruction *instr, match_fn fn, 
			void *client_data)
{
  if (fn(instr, client_data)) {
    return (instr);
  }

  instruction *found = NULL;
  int n = instr->num_srcs();
  for (int i = 0; i < n; i++) {
    operand src = instr->src_op(i);
    if (src.is_instr()) {
      found = find_instr(src.instr(), fn, client_data);
      if (found) break;
    }
  }
  return (found);
}

/*--------------------------------------------------------------------
 * find tree node
 *
 */
suif_object *find_tree_node(tree_node *tn, match_fn fn, void *client_data,
			    boolean search_forward)
{
  suif_object *found_obj = NULL;
  tree_node *next_node = tn;
  while (TRUE) {
    if (search_forward) {
      next_node = next_tree_node(next_node);
    } else {
      next_node = prev_tree_node(next_node);
    }
    if (!next_node) break;
    if (fn(next_node, client_data)) {
      found_obj = next_node;
      break;
    }
    if (next_node->is_instr()) {
      found_obj = find_instr(((tree_instr *) next_node)->instr(), fn,
			     client_data);
      if (found_obj) break;
    }
  }

  return (found_obj);
}

/*--------------------------------------------------------------------
 * find instruction by instruction number
 *
 */

static instruction *found_instr;

static void find_instr_by_num_helper(instruction *i, void *x)
{
  if (i->number() == (unsigned) x) found_instr = i;
}

instruction *find_instr_by_num(tree_proc *proc, int inum)
{
  found_instr = NULL;

  tree_node *tn;
  for (tn = proc; tn; tn = next_tree_node(tn)) {
    if (tn->is_instr()) {
      ((tree_instr *)tn)->instr_map(&find_instr_by_num_helper, (void *) inum);
      if (found_instr) break;
    }
  }
  return (found_instr);
}

/*--------------------------------------------------------------------
 * get procedure containing a specified vnode
 *
 */
proc_sym *get_parent_proc(vnode *vn)
{
  const char *tag = vn->get_tag();

  if (tag == tag_suif_object) {

    suif_object *obj = (suif_object *) vn->get_object();
    return get_parent_proc(obj);

  } else if ((tag == tag_annote) || (tag == tag_annote_list)) {

    suif_object *obj = (suif_object *) vn->get_data();
    return get_parent_proc(obj);

  } else if (tag == tag_code_fragment) {

    code_fragment *f = (code_fragment *) vn->get_object();
    if (f->node()) {
      return f->node()->proc();
    }
  }

  return (NULL);
}

proc_sym *get_parent_proc(suif_object *obj)
{
  proc_sym *proc = NULL;

  switch (obj->object_kind()) {
  case TREE_OBJ:
    {
      tree_node *tn = (tree_node *) obj;
      proc = tn->proc();
    }
    break;
    
  case INSTR_OBJ:
    {
      instruction *instr = (instruction *) obj;
      proc = instr->parent()->proc();
    }
    break;
    
  case SYM_OBJ:
    {
      sym_node *sym = (sym_node *) obj;
      if (sym->is_proc()) proc = (proc_sym *) sym;
    }
    break;
    
  default:
    break;
  }

  return proc;
}

/*--------------------------------------------------------------------
 * get file set entry containing a specified vnode
 *
 */
file_set_entry *get_parent_fse(vnode *vn)
{
  const char *tag = vn->get_tag();

  if (tag == tag_suif_object) {
    return get_parent_fse((suif_object *) vn->get_object());
  } else {
    return (NULL);
  }
}

file_set_entry *get_parent_fse(suif_object *obj)
{
  proc_sym *psym = get_parent_proc(obj);
  if (psym) {
    return (psym->file());
  }

  file_set_entry *fse = NULL;
  switch (obj->object_kind()) {
  case FILE_OBJ:
    {
      fse = (file_set_entry *) obj;
    }
    break;
    
  default:
    break;
  }
  
  return (fse);
}

/*--------------------------------------------------------------------
 * get_enclosing_symtab
 *
 */
base_symtab *get_enclosing_symtab(suif_object *obj)
{
  switch(obj->object_kind()) {
  case FILE_OBJ:
    return ((file_set_entry *) obj)->symtab();
  case TREE_OBJ:
    return ((tree_node *) obj)->scope();
  case INSTR_OBJ:
    return ((instruction *) obj)->parent()->scope();
  case SYMTAB_OBJ:
    return (base_symtab *) obj;
  case SYM_OBJ:
    return ((sym_node *) obj)->parent();
  case DEF_OBJ:
    return ((var_def *) obj)->parent();
  case TYPE_OBJ:
    return ((type_node *) obj)->parent();
  default:
    return (NULL);
  }
}

#if 1
/*--------------------------------------------------------------------
 * visual_suif_proc_iter
 *
 */

void visual_suif_proc_iter(int argc, char * argv[], visual_prociter_f fun, 
			   boolean writeback,
			   boolean exp_trees,
			   boolean use_fortran_form)
{
  init_visual1(argc, (const char**)argv);
  init_vsuif();
  start_vsuif(argc, argv);

  proc_iter_viewer *pi_viewer = new proc_iter_viewer;

  pi_viewer->writeback = writeback;
  pi_viewer->exp_trees = exp_trees;
  pi_viewer->use_fortran_form = use_fortran_form;
  pi_viewer->iter_fn = fun;

  pi_viewer->create_window();
  pi_viewer->iterate();

  /* main loop */
  visual_mainloop();
}

#endif
