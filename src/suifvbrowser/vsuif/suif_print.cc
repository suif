/*-------------------------------------------------------------------
 * suif_print.cc
 *
 */

#include "suif_print.h"
#include "suif_vnode.h"
#include <stdio.h>
#include <stdlib.h>

suif_printer suif_pr;

#define PRINT_INSTR_ANNOTATION

/*----------------------------------------------------------------------
 */
static void instr_header(suif_printer *pr, vtty *text, 
			 instruction *instr, int depth,
			 int elab = 0, int *en = NULL);

static void print_operand(vtty *text, operand op, instruction *instr);
static void print_immed(vtty *text, immed imm);
static void print_tree_node_helper(vtty *text, vnode *tn_vnode,
				   int depth, int detail,
				   suif_printer *pr);
static void print_symtab_helper(vtty *text, vnode *symtab_vnode,
				int depth, int detail, suif_printer *pr);

/*-------------------------------------------------------------------
 * suif_printer::suif_printer
 */
suif_printer::suif_printer(void) 
{
  print_fse_fn = &suif_printer::def_print_fse;
  print_tree_node_fn = &suif_printer::def_print_tree_node;
  print_tree_node_list_fn = &suif_printer::def_print_tree_node_list;
  print_instruction_fn = &suif_printer::def_print_instruction;
  print_symtab_fn = &suif_printer::def_print_symtab;
  print_sym_node_fn = &suif_printer::def_print_sym_node;
  print_var_def_fn = &suif_printer::def_print_var_def;
  print_type_node_fn = &suif_printer::def_print_type_node;
  print_annotations_fn = &suif_printer::def_print_annotations;
  print_annote_fn = &suif_printer::def_print_annote;
  
  to_print_instr_annotes = TRUE;
}

/*-------------------------------------------------------------------
 * indent
 */
void suif_printer::indent(FILE *fd, int depth)
{
  for (int i = 0; i < depth; i++) {
    fputs("  ", fd);
  }
}

/*-------------------------------------------------------------------
 * suif object dispatcher
 *
 */
void suif_printer::print_suif_object(vtty *text, suif_object *obj,
				     int depth, int detail)
{
  // FILE *fd = text->fd();

  switch (obj->object_kind()) {
  case FILE_OBJ:
    print_fse(text, (file_set_entry *) obj, depth, detail);
    break;

  case TREE_OBJ:
    print_tree_node(text, (tree_node *) obj, depth, detail);
    break;

  case INSTR_OBJ:
    print_instruction(text, (instruction *) obj, depth);
    break;

  case SYMTAB_OBJ:
    print_symtab(text, (base_symtab *) obj, depth, detail);
    break;

  case SYM_OBJ:
    print_sym_node(text, (sym_node *) obj, depth, detail);
    break;

  case DEF_OBJ:
    print_var_def(text, (var_def *) obj, depth, detail);
    break;
    
  case TYPE_OBJ:
    print_type_node(text, (type_node *) obj, depth, detail);
    break;

  default:
    break;
  }
}

/*-------------------------------------------------------------------
 * file_set_entry
 *
 */
void suif_printer::def_print_fse(suif_printer *pr, vtty *text,
				 file_set_entry *fse,
				 int depth, int /* detail */) /* unused */
{
  FILE *fd = text->fd();

  text->tag_style(BOLD_BEGIN);
  pr->indent(fd, depth);
  fprintf(fd, "File Set Entry: ");
  text->tag_style(BOLD_END);
  fprintf(fd, "%s\n", fse->name());

  /* symbol table */
  file_symtab *symtab = fse->symtab();
  vnode *vn = create_vnode(symtab);
  text->tag_begin(vn);

  text->tag_style(BOLD_BEGIN);
  pr->indent(fd, depth);
  fprintf(fd, "[Symbol table]\n");
  text->tag_style(BOLD_END);
  text->tag_end(vn);
  
  /* procedures */
  fputc('\n', fd);
  text->tag_style(BOLD_BEGIN);
  pr->indent(fd, depth);
  fprintf(fd, "Procedures:\n");
  text->tag_style(BOLD_END);

  fse->reset_proc_iter();
  proc_sym *psym;
  while ((psym = fse->next_proc())) {
    vn = create_vnode(psym);
    text->tag_begin(vn);
    pr->indent(fd, depth+1);
    fprintf(fd, "%s\n", psym->name());
    text->tag_end(vn);
  }
}

/*-------------------------------------------------------------------
 * tree_node
 *
 */
void suif_printer::def_print_tree_node(suif_printer *pr, vtty *text,
				       tree_node *tn, int depth, int detail)
{
  FILE *fd = text->fd();  

  switch (tn->kind()) {
  case TREE_INSTR:
    {
      tree_instr *ti = (tree_instr *)tn;
      instruction *inst = ti->instr();
      inst->print(fd, depth);
    }
    break;

  case TREE_BLOCK:
    {
      tree_block *tb = (tree_block *) tn;

      text->tag_style(BOLD_BEGIN);
      pr->indent(fd, depth);
      fputs("BLOCK\n", fd);
      text->tag_style(BOLD_END);

      if (detail == PRINT_FULL) {
	base_symtab *symtab = tb->symtab();
	vnode *symtab_vn = create_vnode(symtab);

	text->tag_begin(symtab_vn, (print_fn)&print_symtab_helper, depth, pr);
	pr->print_symtab(text, symtab, depth, PRINT_BRIEF);
	text->tag_end(symtab_vn);

	pr->print_tree_node_list(text, tb->body(), depth+1);

	pr->indent(fd, depth);
	text->tag_style(BOLD_BEGIN);
	fputs("BLOCK END\n", fd);
	text->tag_style(BOLD_END);
      }
    }
    break;

  case TREE_LOOP:
    {
      tree_loop *tl = (tree_loop *) tn;

      text->tag_style(BOLD_BEGIN);
      pr->indent(fd, depth);
      fputs("LOOP (Top=", fd);
      tl->toplab()->print(fd);
      fputs(" Cont=", fd);
      tl->contlab()->print(fd);
      fputs(" Brk=", fd);
      tl->brklab()->print(fd);
      fputs(")\n", fd);
      text->tag_style(BOLD_END);

      if (detail == PRINT_FULL) {
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("LOOP BODY\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, tl->body(), depth+1);
	
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("LOOP TEST\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, tl->test(), depth+1);
	
	pr->indent(fd, depth);
	text->tag_style(BOLD_BEGIN);
	fputs("LOOP END\n", fd);
	text->tag_style(BOLD_END);
      }
    }
    break;

  case TREE_FOR:
    {
      tree_for *tf = (tree_for *) tn;
      
      text->tag_style(BOLD_BEGIN);
      pr->indent(fd, depth);
      fputs("FOR (Index=", fd);
      tf->index()->print(fd);
      fputs(" Test=", fd);
      switch (tf->test()) {
      case FOR_EQ:	{ fputs("EQ", fd); break; }
      case FOR_NEQ:	{ fputs("NEQ", fd); break; }
      case FOR_SGELE:	{ fputs("SGELE", fd); break; }
      case FOR_SGT:	{ fputs("SGT", fd); break; }
      case FOR_SGTE:	{ fputs("SGTE", fd); break; }
      case FOR_SLT:	{ fputs("SLT", fd); break; }
      case FOR_SLTE:	{ fputs("SLTE", fd); break; }
      case FOR_UGELE:	{ fputs("UGELE", fd); break; }
      case FOR_UGT:	{ fputs("UGT", fd); break; }
      case FOR_UGTE:	{ fputs("UGTE", fd); break; }
      case FOR_ULT:	{ fputs("ULT", fd); break; }
      case FOR_ULTE:	{ fputs("ULTE", fd); break; }
      }
      fputs(" Cont=", fd);
      tf->contlab()->print(fd);
      fputs(" Brk=", fd);
      tf->brklab()->print(fd);
      fputs(")\n", fd);
      text->tag_style(BOLD_END);

      if (detail == PRINT_FULL) {
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("FOR LB\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, tf->lb_list(), depth+1);
	
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("FOR UB\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, tf->ub_list(), depth+1);
	
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("FOR STEP\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, tf->step_list(), depth+1);
      
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("FOR LANDING PAD\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, tf->landing_pad(), depth+1);
	
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("FOR BODY\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, tf->body(), depth+1);
	
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("FOR END\n", fd);
	text->tag_style(BOLD_END);
      }
    }
    break;

  case TREE_IF:
    {
      tree_if *ti = (tree_if *) tn;
      
      text->tag_style(BOLD_BEGIN);
      pr->indent(fd, depth);
      fputs("IF (Jumpto=", fd);
      ti->jumpto()->print(fd);
      fputs(")\n", fd);
      text->tag_style(BOLD_END);

      if (detail == PRINT_FULL) {
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("IF HEADER\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, ti->header(), depth+1);
	
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("IF THEN\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, ti->then_part(), depth+1);
	
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("IF ELSE\n", fd);
	text->tag_style(BOLD_END);
	pr->print_tree_node_list(text, ti->else_part(), depth+1);
	
	text->tag_style(BOLD_BEGIN);
	pr->indent(fd, depth);
	fputs("IF END\n", fd);
	text->tag_style(BOLD_END);
      }
    }
    break;

  default:
    break;
  }

  pr->print_annotations(text, tn, depth+1);
} 

/*-------------------------------------------------------------------
 * tree_node_list
 *
 */
void suif_printer::def_print_tree_node_list(suif_printer *pr, vtty *text, 
					    tree_node_list *l, int depth)
{
  // FILE *fd = text->fd();

  tree_node_list_iter iter(l);
  while (!iter.is_empty()) {
    tree_node *tn = iter.step();

    switch (tn->kind()) {
    case TREE_INSTR:
      {
	tree_instr *ti = (tree_instr *)tn;
	vnode *vn = create_vnode(tn);
	text->tag_begin(vn);
	pr->print_instruction(text, ti->instr(), depth);
	text->tag_end(vn);
      }
      break;

    case TREE_BLOCK:
    case TREE_LOOP:
    case TREE_FOR:
    case TREE_IF:
      {
	vnode *vn = create_vnode(tn);
	text->tag_begin(vn, (print_fn)&print_tree_node_helper, depth, pr);
	pr->print_tree_node(text, tn, depth, PRINT_BRIEF);
	text->tag_end(vn);
      }
      break;

    default:
      break;
    }
  }
}

/*-------------------------------------------------------------------
 * instruction
 *
 */
static void instr_header(suif_printer *pr, vtty *text, instruction *instr,
			 int depth, int elab, int * /* en */) /* unused */
{
  FILE *fd = text->fd();

  /* first print the instruction number */
  if (instr->number() != 0) {
    fprintf(fd, "%4d: ", instr->number());
    if (depth > 3) pr->indent(fd, depth - 3);
  } else {
    pr->indent(fd, depth);
  }
  
  /* print the expression label if this is a subexpression */
  if (elab) {
    fprintf(fd, "e%d: ", elab);
  }

  /* print the opcode */
  fprintf(fd, "%s ", if_ops_name(instr->opcode()));
}

void suif_printer::def_print_instruction(suif_printer *pr, vtty *text,
					 instruction *instr,
					 int depth, int elab, int *en)
{
  FILE *fd = text->fd();

  vnode *instr_vn = create_vnode(instr);

  int *ien;
  static int internal_en;
  if (en == NULL) {
    internal_en = 1;
    ien = &internal_en;
  } else {
    ien = en;
  }
  int starten = *ien;

  switch (which_format(instr->opcode())) {
  case inf_rrr:
    {
      text->tag_begin(instr_vn);

      in_rrr *rrr_instr = (in_rrr *) instr;
      instr_header(pr, text, rrr_instr, depth, elab, ien);

      switch (rrr_instr->opcode()) {
      case io_str:
      case io_memcpy:
	{
	  putc(' ', fd);
	  if (rrr_instr->src1_op().is_expr()) {
	    fprintf(fd, "e%d", (*ien)++);
	  } else {
	    print_operand(text, rrr_instr->src1_op(), rrr_instr);
	  }
	  fprintf(fd, " = ");
	  if (rrr_instr->src2_op().is_expr()) {
	    fprintf(fd, "e%d", (*ien)++);
	  } else {
	    print_operand(text, rrr_instr->src2_op(), rrr_instr);
	  }
	  putc('\n', fd);

          if (pr->to_print_instr_annotes) {
	    pr->print_annotations(text, rrr_instr, depth+1);
	  }
	  text->tag_end(instr_vn);

	  /* print the expression trees */
	  if (rrr_instr->src1_op().is_expr()) {
	    instruction *src1_instr = rrr_instr->src1_op().instr();
	    pr->print_instruction(text, src1_instr, depth+1,
				  starten++, ien);
	  }
	  if (rrr_instr->src2_op().is_expr()) {
	    instruction *src2_instr = rrr_instr->src2_op().instr();
	    pr->print_instruction(text, src2_instr, depth+1,
				  starten++, ien);
	  }
	}
	break;

      case io_ret:
	{
	  putc(' ', fd);
	  if (rrr_instr->src1_op().is_expr()) {
	    fprintf(fd, "e%d", (*ien)++);
	  } else {
	    print_operand(text, rrr_instr->src1_op(), rrr_instr);
	  }

	  putc('\n', fd);

          if (pr->to_print_instr_annotes) {
	    pr->print_annotations(text, rrr_instr, depth+1);
	  }

	  text->tag_end(instr_vn);

	  /* print the expression tree */
	  if (rrr_instr->src1_op().is_expr()) {
	    instruction *src1_instr = rrr_instr->src1_op().instr();
	    pr->print_instruction(text, src1_instr, depth+1,
				  starten++, ien);
	  }
	}
	break;
	
      default:

	if (!_suif_no_types) {
	  vnode *sym_vn = create_vnode(rrr_instr->result_type());
	  text->tag_begin(sym_vn);
	  pr->print_type_node(text, rrr_instr->result_type(), 0, PRINT_BRIEF);
	  text->tag_end(sym_vn);

	}
	putc(' ', fd);
	if (!rrr_instr->parent() ||
	    rrr_instr->parent()->instr() == rrr_instr) {
	  print_operand(text, rrr_instr->dst_op(), rrr_instr);
	  fprintf(fd, " = ");
	}
	if (rrr_instr->src1_op().is_expr()) {
	  fprintf(fd, "e%d", (*ien)++);
	} else {
	  print_operand(text, rrr_instr->src1_op(), rrr_instr);
	}

	/* check if the second source operand is used */
	if (!rrr_instr->is_unary() && (rrr_instr->opcode() != io_lod)) {
	  fprintf(fd, ", ");
	  if (rrr_instr->src2_op().is_expr()) {
	    fprintf(fd, "e%d", (*ien)++);
	  } else {
	    print_operand(text, rrr_instr->src2_op(), rrr_instr);
	  }
	}
	putc('\n', fd);

	if (pr->to_print_instr_annotes) {
	  pr->print_annotations(text, rrr_instr, depth+1);
	}
	text->tag_end(instr_vn);

	/* print the expression tree(s) */
	if (rrr_instr->src1_op().is_expr()) {
	  instruction *src1_instr = rrr_instr->src1_op().instr();
	  pr->print_instruction(text, src1_instr, depth+1,
				starten++, ien);
	}
	if (!rrr_instr->is_unary() && rrr_instr->src2_op().is_expr()) {
	  instruction *src2_instr = rrr_instr->src2_op().instr();
	  pr->print_instruction(text, src2_instr, depth+1,
				starten++, ien);
	}
	break;
      }
    }
    break;

  case inf_bj:
    {
      in_bj *bj_instr = (in_bj *) instr;

      text->tag_begin(instr_vn);

      instr_header(pr, text, bj_instr, depth, elab);

      if (bj_instr->opcode() == io_jmp) {
	vnode *sym_vn = create_vnode(bj_instr->target());
	text->tag_begin(sym_vn);
	pr->print_sym_node(text, bj_instr->target(), 0, PRINT_BRIEF);
	text->tag_end(sym_vn);

	putc('\n', fd);

	if (pr->to_print_instr_annotes) {
	  pr->print_annotations(text, instr, depth+1);
	}
	text->tag_end(instr_vn);

      } else {

	/* handle conditional branches */
	putc(' ', fd);
	if (bj_instr->src_op().is_expr()) {
	  fprintf(fd, "e%d", (*ien)++);
	} else {
	  print_operand(text, bj_instr->src_op(), bj_instr);
	}
	fprintf(fd, ", ");

	vnode *sym_vn = create_vnode(bj_instr->target());
	text->tag_begin(sym_vn);
	pr->print_sym_node(text, bj_instr->target(), 0, PRINT_BRIEF);
	text->tag_end(sym_vn);

	putc('\n', fd);

	if (pr->to_print_instr_annotes) {
	  pr->print_annotations(text, bj_instr, depth+1);
	}
	text->tag_end(instr_vn);

	/* print the expression tree */
	if (bj_instr->src_op().is_expr()) {
	  instruction *src_instr = bj_instr->src_op().instr();
	  pr->print_instruction(text, src_instr, depth+1,
				starten++, ien);
	}
      }
    }
    break;

  case inf_ldc:
    {
      text->tag_begin(instr_vn);

      instr_header(pr, text, instr, depth, elab, en);
      if (!_suif_no_types) {
	vnode *sym_vn = create_vnode(instr->result_type());
	text->tag_begin(sym_vn);
	pr->print_type_node(text, instr->result_type(), 0, PRINT_BRIEF);
	text->tag_end(sym_vn);
      }
      putc(' ', fd);
      
      if (!instr->parent() || instr->parent()->instr() == instr) {
	print_operand(text, instr->dst_op(), instr);
	fprintf(fd, " = ");
      }

      print_immed(text, ((in_ldc *) instr)->value());

      putc('\n', fd);

      if (pr->to_print_instr_annotes) {
	pr->print_annotations(text, instr, depth+1);
      }
      text->tag_end(instr_vn);
    }
    break;

  case inf_cal:
    {
      
      in_cal *cal_instr = (in_cal *) instr;

      text->tag_begin(instr_vn);
      instr_header(pr, text, cal_instr, depth, elab);

      if (!_suif_no_types) {
	vnode *sym_vn = create_vnode(cal_instr->result_type());
	text->tag_begin(sym_vn);
	pr->print_type_node(text, cal_instr->result_type(), 0, PRINT_BRIEF);
	text->tag_end(sym_vn);
      }
      putc(' ', fd);

      if (!cal_instr->parent() || cal_instr->parent()->instr() ==
	  cal_instr) {
	print_operand(text, cal_instr->dst_op(), cal_instr);
	fprintf(fd, " = ");
      }

      /* print the procedure address */
      if (cal_instr->addr_op().is_expr()) {
	fprintf(fd, "e%d", (*ien)++);
      } else {
	print_operand(text, cal_instr->addr_op(), cal_instr);
      }

      /* print the argument list */
      putc('(', fd);
      int i, n = cal_instr->num_args();
      for (i = 0; i < n; i++) {
	if (cal_instr->argument(i).is_expr()) {
	  fprintf(fd, "e%d", (*ien)++);
	} else {
	  print_operand(text, cal_instr->argument(i), cal_instr);
	}
	if (i < n-1) fprintf(fd, ", ");
      }
      putc(')', fd);
      putc('\n', fd);

      if (pr->to_print_instr_annotes) {
	pr->print_annotations(text, cal_instr, depth+1);
      }
      text->tag_end(instr_vn);

      /* print expression tree for the address */
      if (cal_instr->addr_op().is_expr()) {
	instruction *addr_instr = cal_instr->addr_op().instr();
	pr->print_instruction(text, addr_instr, depth+1,
			      starten++, ien);
      }

      /* print expression trees for the arguments */
      for (i = 0; i < n; i++) {
	if (cal_instr->argument(i).is_expr()) {
	  instruction *arg_instr = cal_instr->argument(i).instr();
	  pr->print_instruction(text, arg_instr, depth+1,
				starten++, ien);
	}
      }
    }
    break;

  case inf_lab:
    {
      text->tag_begin(instr_vn);

      in_lab *lab_instr = (in_lab *) instr;
      instr_header(pr, text, lab_instr, depth, elab, en);
      vnode *sym_vn = create_vnode(lab_instr->label());
      text->tag_begin(sym_vn);
      pr->print_sym_node(text, lab_instr->label(), 0, PRINT_BRIEF);
      text->tag_end(sym_vn);

      putc('\n', fd);

      if (pr->to_print_instr_annotes) {
	pr->print_annotations(text, lab_instr, depth+1);
      }
      text->tag_end(instr_vn);
    }
    break;

  case inf_array:
    {
      text->tag_begin(instr_vn);

      in_array *array_instr = (in_array *) instr;
      instr_header(pr, text, array_instr, depth, elab, ien);
      if (!_suif_no_types) {
	vnode *sym_vn = create_vnode(array_instr->result_type());
	text->tag_begin(sym_vn);
	pr->print_type_node(text, array_instr->result_type(), 0, PRINT_BRIEF);
	text->tag_end(sym_vn);
      }
      putc(' ', fd);
      
      if (!array_instr->parent() || 
	  array_instr->parent()->instr() == array_instr) {
	print_operand(text, array_instr->dst_op(), array_instr);
	fputs(" = ", fd);
      }

      /* print the base address plus the offset within the element */
      if (array_instr->base_op().is_expr()) {
	fprintf(fd, "e%d", (*ien)++);
      } else {
	print_operand(text, array_instr->base_op(), array_instr);
      }
      fprintf(fd, "+%u", array_instr->offset());

      /* print the FORTRAN array offset */
      if (!array_instr->offset_op().is_null()) {
	if (array_instr->offset_op().is_expr()) {
	  fprintf(fd, "-e%d", (*ien)++);
	} else {
	  fputs("-", fd);
	  print_operand(text, array_instr->offset_op(), array_instr);
	}
      }

      /* print the element size */
      fprintf(fd, ", size(%u", array_instr->elem_size());

      /* print the indexes */
      fputs("), index(", fd);
      unsigned i;
      for (i = 0; i < array_instr->dims(); i++) {
	if (array_instr->index(i).is_expr()) {
	  fprintf(fd, "e%d", (*ien)++);
	} else {
	  print_operand(text, array_instr->index(i), array_instr);
	}
	if (i < array_instr->dims() - 1) fputs(", ", fd);
      }

      /* print the bounds */
      fputs("), bound(", fd);
      for (i = 0; i < array_instr->dims(); i++) {
	if (array_instr->bound(i).is_expr()) {
	  fprintf(fd, "e%d", (*ien)++);
	} else {
	  print_operand(text, array_instr->bound(i), array_instr);
	}
	if (i < array_instr->dims() - 1) fputs(", ", fd);
      }
      putc(')', fd);
      putc('\n', fd);

      if (pr->to_print_instr_annotes) {
	pr->print_annotations(text, array_instr, depth+1);
      }
      text->tag_end(instr_vn);
      
      /* print the expression trees */
      if (array_instr->base_op().is_expr()) {
	instruction *base_instr = array_instr->base_op().instr();
	pr->print_instruction(text, base_instr, depth+1, starten++, ien);
      }
      if (array_instr->offset_op().is_expr()) {
	instruction *offset_instr = array_instr->offset_op().instr();
	pr->print_instruction(text, offset_instr, depth+1, starten++, ien);
      }
      for (i = 0; i < array_instr->dims(); i++) {
	if (array_instr->index(i).is_expr()) {
	  instruction *index_instr = array_instr->index(i).instr();
	  pr->print_instruction(text, index_instr, depth+1,
				starten++, ien);
	}
      }
      for (i = 0; i < array_instr->dims(); i++) {
	if (array_instr->bound(i).is_expr()) {
	  instruction *bound_instr = array_instr->bound(i).instr();
	  pr->print_instruction(text, bound_instr, depth+1,
				starten++, ien);
	}
      }
    }
    break;

  default:
    {
      text->tag_begin(instr_vn);
      instr->print(fd, depth, elab, en);
      text->tag_end(instr_vn);
    }
    break;
  }

}

/*-------------------------------------------------------------------
 * print_operand
 *
 */
static void print_operand(vtty *text, operand op, instruction *instr)
{
  vnode *vn;
  switch (op.kind()) {
  case OPER_SYM:
    vn = create_vnode(op.symbol());
    break;

  case OPER_INSTR:
    vn = create_vnode(op.instr());
    break;

  default:
    vn = NULL;
  }
  if (vn) text->tag_begin(vn);
  op.print(instr, text->fd());
  if (vn) text->tag_end(vn);
}     

/*-------------------------------------------------------------------
 * print_immed
 *
 */
static void print_immed(vtty *text, immed imm)
{
  vnode *vn;

  switch (imm.kind()) {
  case im_symbol:
    {
      sym_node *sym = imm.symbol();
      vn = create_vnode(sym);
    }
    break;
  case im_type:
    {
      type_node *type = imm.type();
      vn = create_vnode(type);
    }
    break;
  default:
    vn = NULL;
  }

  if (vn) text->tag_begin(vn);
  imm.print(text->fd());
  if (vn) text->tag_end(vn);
}

/*-------------------------------------------------------------------
 * base_symtab
 *
 */
void suif_printer::def_print_symtab(suif_printer *pr, vtty *text, 
				    base_symtab *symtab, 
				    int depth, int detail)
{
  FILE *fd = text->fd();

  text->tag_style(BOLD_BEGIN);
  pr->indent(fd, depth);
  fprintf(fd, "Symbol table: ");
  text->tag_style(BOLD_END);

  /* print the symtab name if available */
  if (symtab->name()) {
    fprintf(fd, "`%s'\n", symtab->name());
  } else {
    fprintf(fd, "<None>\n");
  }
  
  if (detail == PRINT_FULL) {

    /* print the children */
    base_symtab_list *children = symtab->children();
    if (children) {
      pr->indent(fd, depth+1);
      text->tag_style(BOLD_BEGIN);
      fprintf(fd, "Children:\n");
      text->tag_style(BOLD_END);

      if (children->is_empty()) {
	pr->indent(fd, depth+2);
	fprintf(fd, "<None>\n");
      } else {
	base_symtab_list_iter symtab_iter(children);
	while (!symtab_iter.is_empty()) {
	  base_symtab *s = symtab_iter.step();
	  vnode *vn = create_vnode(s);
	  text->tag_begin(vn);
	  pr->print_symtab(text, s, depth+2, PRINT_BRIEF);
	  text->tag_end(vn);
	}
      }
    }
    
    /* print the types */
    text->tag_style(BOLD_BEGIN);
    pr->indent(fd, depth+1);
    fprintf(fd, "Types:\n");
    text->tag_style(BOLD_END);
    
    if (symtab->types()->is_empty()) {
      pr->indent(fd, depth+2);
      fprintf(fd, "<None>\n");
    } else {
      type_node_list_iter tmi(symtab->types());
      while (!tmi.is_empty()) {
	type_node *t = tmi.step();
	vnode *vn = create_vnode(t);
	text->tag_begin(vn);
	pr->print_type_node(text, t, depth+2);
	text->tag_end(vn);
      }
    }
  
    /* print the symbols */
    text->tag_style(BOLD_BEGIN);
    pr->indent(fd, depth+1);
    fprintf(fd, "Symbols:\n");
    text->tag_style(BOLD_END);
    
    if (symtab->symbols()->is_empty()) {
      pr->indent(fd, depth+2);
      fprintf(fd, "<None>\n");
    } else {
      sym_node_list_iter smi(symtab->symbols());
      while (!smi.is_empty()) {
	sym_node *sym = smi.step();
	vnode *vn = create_vnode(sym);
	text->tag_begin(vn);
	pr->print_sym_node(text, sym, depth+2, PRINT_FULL);
	text->tag_end(vn);
      }
    }
    
    /* print the variable definitions */
    text->tag_style(BOLD_BEGIN);
    pr->indent(fd, depth+1);
    fprintf(fd, "Definitions:\n");
    text->tag_style(BOLD_END);
    
    if (symtab->var_defs()->is_empty()) {
      pr->indent(fd, depth+2);
      fprintf(fd, "<None>\n");
    } else {
      var_def_list_iter vdli(symtab->var_defs());
      while (!vdli.is_empty()) {
	var_def *def = vdli.step();
	vnode *vn = create_vnode(def);
	text->tag_begin(vn);
	def->print(fd, depth+2);
	text->tag_end(vn);
      }
    }
  }
}

/*-------------------------------------------------------------------
 * sym_node
 *
 */
void suif_printer::def_print_sym_node(suif_printer *pr, vtty *text,
				      sym_node *sym,
				      int depth, int detail)
{
  FILE *fd = text->fd();
  tree_proc *proc = NULL;

  if (sym->kind() == SYM_PROC) {
    proc_sym *psym = (proc_sym *) sym;
    if (!psym->is_in_memory()) {
      psym->read_proc();
    }
    proc = psym->block();
  }

  if (detail == PRINT_FULL) {
    if (proc) {
      vnode *vn = create_vnode(proc);
      text->tag_begin(vn);
      pr->indent(fd, depth);
      fprintf(fd, "[Proc: `%s']\n", sym->name());
      text->tag_end(vn);
    }
    sym->print_full(fd, depth);
    
  } else {
    pr->indent(fd, depth);
    if (proc) {
      vnode *vn = create_vnode(proc);
      text->tag_begin(vn);
      fprintf(fd, "[Proc: `%s'] ", sym->name());
      text->tag_end(vn);
    }
    sym->print(fd);
  }

}

/*-------------------------------------------------------------------
 * var_def
 *
 */
void suif_printer::def_print_var_def(suif_printer * /* pr */, /* unused */
				     vtty *text,
				     var_def *def,
				     int depth, int /* detail */) /* unused */
{
  FILE *fd = text->fd();
  def->print(fd, depth);
}

/*-------------------------------------------------------------------
 * type_node
 *
 */

void suif_printer::def_print_type_node(suif_printer *pr, vtty *text,
				       type_node *type,
				       int depth, int detail)
{
  FILE *fd = text->fd();
  if (detail == PRINT_FULL) {
    type->print_full(fd, depth);
  } else {
    pr->indent(fd, depth);
    type->print_abbrev(fd);
    // print_abbrev(fd);
  }
}

/*-------------------------------------------------------------------
 * anntotations
 *
 */
void suif_printer::def_print_annotations(suif_printer *pr, vtty *text,
					 suif_object *obj,
					 int depth,
					 int /* detail */) /* unused */
{
  FILE *fd = text->fd();
  annote_list *annotes;

  if (obj->object_kind() == TREE_OBJ &&
      ((tree_node *)obj)->is_instr()) {
    instruction *instr = ((tree_instr *)obj)->instr();
    if (!instr->are_annotations()) return;
    annotes = instr->annotes();
  } else {
    if (!obj->are_annotations()) return;
    annotes = obj->annotes();
  }

  annote_list_iter anli(annotes);
  while (!anli.is_empty()) {
    annote *ann = anli.step();
    vnode *vn = create_vnode(ann, obj);
    text->tag_begin(vn);
    pr->print_annote(text, ann, depth);
    putc('\n', fd);
    text->tag_end(vn);
  }
}

/*-------------------------------------------------------------------
 * anntote
 *
 */
void suif_printer::def_print_annote(suif_printer *pr, vtty *text,
				    annote *ann,
				    int depth, int /* detail */) /* unused */
{
  FILE *fd = text->fd();
  pr->indent(fd, depth);

  /* first try to use the print function */
  annote_def *ad = lookup_annote(ann->name());
  if (ad && ad->is_structured()) {
    ann_print_f print_func = ((struct_annote_def *)ad)->print();
    if (print_func && !_suif_flat_annotes) {
      text->tag_style(BOLD_BEGIN);
      print_func(fd, ann->name(), ann->data());
      text->tag_style(BOLD_END);
      return;
    }
  }

  /* next try to print the annotation as an immed_list */
  immed_list *iml = ann->immeds();
  if (iml) {
    fprintf(fd, "[\"");
    text->tag_style(BOLD_BEGIN);
    fprintf(fd, "%s", ann->name());
    text->tag_style(BOLD_END);
    fprintf(fd, "\":");
    immed_list_iter imli(iml);
    while (!imli.is_empty()) {
      putc(' ', fd);
      imli.step().print(fd);
    }
    fprintf(fd, "]");
    
    if (ad && ad->is_structured()) delete iml;
    return;
  }
  
  /* just print the data field directly */
  fprintf(fd, "[\"");
  text->tag_style(BOLD_BEGIN);
  fprintf(fd, "%s", ann->name());
  text->tag_style(BOLD_END);
  fprintf(fd, "\": %p]", ann->data());
}

/*-------------------------------------------------------------------
 * helper functions
 *
 */

static void print_tree_node_helper(vtty *text, vnode *tn_vnode,
			    int depth, int detail,
			    suif_printer *pr)
{
  tree_node *tn = (tree_node *) tn_vnode->get_object();
  pr->print_tree_node(text, tn, depth, detail);
}

static void print_symtab_helper(vtty *text, vnode *symtab_vnode,
			 int depth, int detail, suif_printer *pr)
{
  base_symtab *symtab = (base_symtab *) symtab_vnode->get_object();
  pr->print_symtab(text, symtab, depth, detail);
}

/*--------------------------------------------------------------------
 * get_object_name
 *
 */
char *suif_printer::get_object_name(suif_object *obj)
{
  switch (obj->object_kind()) {
  case FILE_OBJ:		/* file_set_entry */
    return "file_set_entry";
  case TREE_OBJ:		/* tree_node */
    return "tree_node";
  case INSTR_OBJ:		/* instruction */
    return "instruction";
  case SYMTAB_OBJ:		/* base_symtab */
    return "base_symtab";
  case SYM_OBJ:			/* sym_node */
    return "sym_node";
  case DEF_OBJ:			/* var_def */
    return "var_def";
  case TYPE_OBJ:		/* type_node */
    return "type_node";
  }
  return "unknown";
}

/*--------------------------------------------------------------------
 * get_object_description
 *
 */
char *suif_printer::get_object_description(suif_object *obj)
{
  static char desc[100];
  char str_buffer[100];
  const char *str;
  // char *obj_name = get_object_name(obj);

  switch (obj->object_kind()) {
  case FILE_OBJ:		/* file_set_entry */
    {
      str = ((file_set_entry *) obj)->name();
    }
    break;

  case TREE_OBJ:		/* tree_node */
    {
      switch (((tree_node *) obj)->kind()) {
      case TREE_INSTR:
	str = "tree_instr";
	break;
      case TREE_BLOCK:
	str = "tree_block";
	break;
      case TREE_LOOP:
	str = "tree_loop";
	break;
      case TREE_FOR:
	str = "tree_for";
	break;
      case TREE_IF:
	str = "tree_if";
	break;
      default:
	str = "tree_node";
	break;
      }
    }
    break;

  case INSTR_OBJ:		/* instruction */
    {
      str = if_ops_name(((instruction *) obj)->opcode());
    }
    break;

  case SYMTAB_OBJ:		/* base_symtab */
    {
      str = ((base_symtab *) obj)->name();
    }
    break;
  case SYM_OBJ:			/* sym_node */
    {
      str = ((sym_node *) obj)->name();
    }
    break;
  case DEF_OBJ:			/* var_def */
    {
      str = ((var_def *) obj)->variable()->name();
    }
    break;
  case TYPE_OBJ:		/* type_node */
    {
      sprintf(str_buffer, "id: %d",
	      ((type_node *) obj)->type_id());
      str = str_buffer;
    }
    break;
  default:
    str= "unknown type";
  }

  sprintf(desc, "[%s] %s", 
	  get_object_name(obj), str);

  return desc;
}
