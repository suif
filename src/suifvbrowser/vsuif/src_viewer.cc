/*-------------------------------------------------------------------
 * src_viewer
 *
 */

#include "src_viewer.h"
#include "code_tree.h"
#include "suif_vnode.h"
#include "suif_utils.h"
#include "suif_event.h"
#include "suif_menu.h"
#include <stdlib.h>
#include <string.h>

/*--------------------------------------------------------------------
 * src_viewer::src_viewer
 */
src_viewer::src_viewer(void)
{
  line_offset = 0;
  current_file = NULL;
  current_fse = NULL;
  stree = NULL;

  stree_cache = new code_tree_list;
}

/*--------------------------------------------------------------------
 * src_viewer::~src_viewer
 *
 */
src_viewer::~src_viewer(void)
{
  // Note: stree is in the stree_cache, so it will be deleted by
  // clear_cache() method.

  delete (infobar);

  clear_cache();
  delete (stree_cache);
}

/*--------------------------------------------------------------------
 * src_viewer::clear_cache
 *
 */
void src_viewer::clear_cache(void)
{
  while (!stree_cache->is_empty()) {
    delete stree_cache->pop();
  }
}

/*--------------------------------------------------------------------
 * src_viewer::create_window
 *
 */
void src_viewer::create_window(void)
{
  inherited::create_window();
  infobar = new vmessage(toplevel);
  annote_column = text->add_column(4);

  /* procs menu */
  create_file_menu();
  update_infobar();

  vnode *last_sel = vman->get_selection();
  if (last_sel) {
    show(last_sel);
  }
}

/*--------------------------------------------------------------------
 * src_viewer::clear
 *
 */
void src_viewer::clear(void)
{
  current_fse = NULL;
  current_file = NULL;
  text->clear();
  stree = NULL;

  menu->remove(ROOT_MENU);
  create_file_menu();
  add_std_go_menu(menu);

  update_infobar();
}

/*--------------------------------------------------------------------
 * src_viewer::refresh
 *
 */
void src_viewer::refresh(void)
{
  menu->remove(ROOT_MENU);
  create_file_menu();
  add_std_go_menu(menu);

  update_infobar();

  file_set_entry *fse = current_fse;
  current_fse = NULL;		// to force a redraw
  view(fse);
}

/*--------------------------------------------------------------------
 * src_viewer::create_file_menu
 *
 */
void src_viewer::create_file_menu(void)
{
  menu->clear("File");
  add_std_fse_menu(menu, "File/File Set Entry");
  add_std_proc_menu(menu, "File/Procedure");
  menu->add_separator("File");
  add_close_command(menu, "File");
}

/*--------------------------------------------------------------------
 * src_viewer::build_stree
 */
void src_viewer::build_stree(void)
{
  /* create a new src tree */
  stree = new code_tree;
  stree->set_map_fn((map_tn_fn) &map_tree_node, this);
  stree->id = current_fse;
  stree_cache->append(stree);

  post_progress(this, "Loading source file..", 0);
  
  /* create link to source */
  int num_procs = 0;
  current_fse->reset_proc_iter();
  while (current_fse->next_proc()) {
    num_procs++;
  }

  int i = 0;
  current_fse->reset_proc_iter();
  proc_sym *psym;
  while ((psym = current_fse->next_proc())) {
    if (!psym->is_in_memory()) {
      psym->read_proc();
    }
    tree_proc *proc = psym->block();
    if (proc) {
      stree->build(proc);
    }
    i++;
    post_progress(this, "Loading SUIF procedures..", 
		  ((float) i)/num_procs*100);
  }
}

/*--------------------------------------------------------------------
 * src_viewer::print_source
 */

boolean src_viewer::print_source(file_set_entry * /* fse */, /* unused */
                                 const char *filename, const char *pathname)
{
  text->clear();

  if (filename == NULL) {
    fprintf(text->fd(), "No source file\n");
    text->update();
    return FALSE;
  }

  /* read and insert file */
  if (text->insert_file(pathname, TRUE)) {
    fprintf(text->fd(), "Cannot find source file '%s'.\n", pathname);
    text->update();
    return FALSE;
  }

  current_file = lexicon->enter(filename)->sp;

  /* annotate the source codes */
  annotate_src();
  
  /* create tags */
  stree->create_tags(text);
  
  unpost_progress(this);

  return TRUE;
}

/*--------------------------------------------------------------------
 * src_viewer::map_tree_node
 *
 * Return 0 if the source line cannot be found
 */
code_range src_viewer::map_tree_node(tree_node *tn, src_viewer *viewer)
{
  const char *file;
  int line = find_source_line(tn, &file);
  if (line) {
    line += viewer->line_offset;
    
#if 0
    /* check file! */
    if (file == viewer->current_file) {
      return (code_range(line, line));
    }
#endif

#if 1
    return (code_range(line, line));
#endif
  }
  return (code_range(0, 0));
}

/*--------------------------------------------------------------------
 * src_viewer::annotate_src
 *
 */
void src_viewer::annotate_src_helper(code_fragment *code_f)
{
  const char *k_doall = lexicon->enter("doall")->sp;
  const char *k_memdep_there = lexicon->enter("speculate_dependence")->sp;
  const char *k_memdeps_run = lexicon->enter("loop_unique_num")->sp;

  for (code_fragment *f = code_f->child(); f; f = f->next_sib()) {
    annotate_src_helper(f);
    tree_node *tn = f->node();
    switch (tn->kind()) {
    case TREE_FOR:
      if (tn->peek_annote(k_doall)) {
	  text->set_column_text(annote_column,
				f->first_line(), "DA");
      } else if (tn->peek_annote(k_memdeps_run)
		 && !tn->peek_annote(k_memdep_there)) {
	  text->set_column_text(annote_column,
				f->first_line(), "DA?");
      }
      break;
    default:
      break;
    }
  }
}

void src_viewer::annotate_src(void)
{
  annotate_src_helper(stree->get_root());
}

/*--------------------------------------------------------------------
 * src_viewer::update_infobar
 */
void src_viewer::update_infobar(void)
{
  char m[200];
  sprintf(m, 
	  "Source File: `%s'",
	  current_file ? current_file : "none");
  infobar->set_message(m);
}

/*--------------------------------------------------------------------
 * src_viewer::view
 *
 */
void src_viewer::view(file_set_entry *fse, const char *filename)
{
  if (fse && fse != current_fse) {
    current_fse = fse;

    /* for fortran codes, offset by -1 line */
    if (get_fse_src_lang(fse) == src_fortran) {
      line_offset = -1;
    } else {
      line_offset = 0;
    }

    /* check cache */
    stree = NULL;
    code_tree_list_iter iter(stree_cache);
    while (!iter.is_empty()) {
      code_tree *ct = iter.step();
      if (ct->id == current_fse) {
	stree = ct;
      }
    }

    if (!stree) {
      build_stree();
    }

    if (!filename) {
      filename = find_source_file(current_fse);
    }

    /* figure out the path of the source code from the fse path */
    const char *dir = fileset_dir();
    char *buffer = new char[strlen(dir) + strlen(filename)+10];
    construct_pathname(buffer, dir, filename);
    
    print_source(current_fse, filename, buffer);
    delete (buffer);

    update_infobar();
  }
}

/*--------------------------------------------------------------------
 * src_viewer::view
 *
 * view a tree node
 */
void src_viewer::view(tree_node *tn, boolean select)
{
  proc_sym *psym = tn->proc();
  if (psym) {
    file_set_entry *fse = psym->file();
    if (fse != current_fse) {
      const char *filename = find_source_file(tn);
      view(fse, filename);
    }
  }

  /* look up tree node */    
  code_fragment *f = stree->lookup(tn);
  if (f) {
    vnode *vn = vman->find_vnode(f);
    text->view(f->first_line() + line_offset, 0);
    if (select) {
      text->select(vn);
    }
  } else {
    int line = find_source_line(tn);
    if (line) {
      text->view(line + line_offset, 0);
      if (select) {
	text->select_line(line + line_offset);
      }
    }
  }
}

/*--------------------------------------------------------------------
 * event handler
 *
 * when a event occurs, this function is invoked
 */
void src_viewer::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case SELECTION:
    {
      vnode *vn = e.get_object();
      void *event_source = e.get_source();
      if (event_source == text) return; // ignore local event

      show(vn);
    }
    break;

  case CLOSE_FILESET:
    {
      clear_cache();
      clear();
      create_file_menu();
    }
    break;

  case NEW_FILESET:
    {
      /* show the first fse */
      fileset->reset_iter();
      file_set_entry *fse = fileset->next_file();
      if (fse) {
	view(fse);
      }
    }
    break;

  case REFRESH:
  case PROC_MODIFIED:
  case FSE_MODIFIED:
    {
      refresh();
    }
    break;

  default:
    break;
  }
}

/*----------------------------------------------------------------------
 * show
 */
void src_viewer::show(vnode *vn)
{
  const char *tag = vn->get_tag();
  tree_node *tn = NULL;
  boolean showing_new_proc = FALSE;

  if (tag == tag_suif_object) {
    
    suif_object *obj = (suif_object *) vn->get_object();
    switch (obj->object_kind()) {
    case TREE_OBJ:
      {
	tn = (tree_node *) obj;
      }
      break;

    case SYM_OBJ:
      {
	if (((sym_node *) obj)->is_proc()) {
	  /* proc_sym */
	  proc_sym *psym = (proc_sym *) obj;
	  if (!psym->is_in_memory()) {
	    psym->read_proc();
	  }
	  showing_new_proc = TRUE;
	  tn = psym->block();
	}
      }
      break;

    case INSTR_OBJ:
      {
	tn = ((instruction *) obj)->parent();
      }
      break;

    case FILE_OBJ:
      {
	file_set_entry *fse = (file_set_entry *) obj;
	view(fse);
      }
      break;

    default:
      break;
    }    
  } else if (tag == tag_code_fragment) {
    tn = ((code_fragment *) vn->get_object())->node();
  }

  if (tn) {
    view(tn, FALSE);
    if (showing_new_proc) {
      post_event(event(NULL, NEW_PROC_SRC_DISPLAYED));
    }
  }
}

