/*
 * main_window.h
 */

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "includes.h"
#include "base_viewer.h"

extern char *application_help_text;

/*----------------------------------------------------------------------
 * main window
 *
 */

class main_window : public base_viewer {
  typedef base_viewer inherited;

private:
  vmenu *menu;
  vframe *text_frame;
  vtext *text;
  vframe *button_frame;
  vbuttonbar *button_bar;

  int current_argc;
  char **current_argv;

  boolean fileset_modified;

  static void do_open_cmd(event &e, main_window *win);
  static void do_save_cmd(event &e, main_window *win);
  static void do_reload_cmd(event &e, main_window *win);
  static void do_exit_cmd(event &e, main_window *win);
  static void do_show_window_cmd(event &e, main_window *win,
				 window_class *wclass);
  static void do_help_cmd(event &e, main_window *win);

public:
  main_window(void);
  ~main_window(void);
  virtual void create_window(void);
  virtual char *class_name(void) { return "Main Window"; }

  void close_fileset(void);
  void browse_fileset(int argc, const char * const argv[]);

  void save_fileset(void);
  void update_display(void);

  virtual void handle_event(event &e);
};

#endif

