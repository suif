/*-------------------------------------------------------------------
 * graph_layout.cc
 *
 * graph layout module
 */

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <tcl.h>
#include <suif1.h>
#include "graph_layout.h"


#define DPRIORITY 32		// a heuristic
#define FAN_SPACE_FACTOR 2

/*-------------------------------------------------------------------
 * layout_node
 */
layout_node::layout_node(void *node_id)
{
  client_id = node_id;
  succs = new layout_node_list;
  preds = new layout_node_list;
}

layout_node::~layout_node(void)
{
  delete succs;
  delete preds;
}

/*------------------------------------------------------------------
 * Find how connected this node is, this is a heuristic..
 *
 */
int layout_node::find_connectivity(layout_node_list *visited)
{
  if (visited->lookup(this)) return (0);
  visited->push(this);

  int num = 0;
  layout_node_list_iter iter(succs);

#if 1
  while (!iter.is_empty()) {
    layout_node *succ = iter.step();
    if (succ->parent == this) {
      num += succ->find_connectivity(visited);
    } else {
      num++;
    }
  }
#endif

  iter.reset(preds);
  while (!iter.is_empty()) {
    layout_node *pred = iter.step();
    if (pred != parent) {
      num++;
    }
  }
  return (num);
}

/*-------------------------------------------------------------------
 * graph_layout
 */
graph_layout::graph_layout(void)
{
  nodes = new layout_node_list;
  root = NULL;
  config.xspacing = 20;
  config.yspacing = 20;
  config.xoffset = 20;
  config.yoffset = 20;

  num_nodes = 0;
}

graph_layout::~graph_layout(void)
{
  layout_node_list_iter iter(nodes);
  while (!iter.is_empty()) {
    delete (iter.step());
  }
  delete (nodes);
}

/*-------------------------------------------------------------------
 * add node
 */
void graph_layout::add_node(void *node_id, layout_geometry &geom)
{
  layout_node *new_node = new layout_node(node_id);
  new_node->geom = geom;

  nodes->append(new_node);
  num_nodes++;
}

/*-------------------------------------------------------------------
 * find node given the client id
 */
layout_node *graph_layout::find_node(void *client_id)
{
  layout_node_list_iter iter(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    if (n->client_id == client_id) {
      return (n);
    }
  }
  return (NULL);
}

/*-------------------------------------------------------------------
 * add edge
 */
void graph_layout::add_edge(void *node_id1, void *node_id2)
{
  layout_node *node1 = find_node(node_id1);
  layout_node *node2 = find_node(node_id2);
  node1->succs->append(node2);
  node2->preds->append(node1);
}

/*-------------------------------------------------------------------
 * set root of the graph
 */
void graph_layout::set_root(void *node_id)
{
  root = find_node(node_id);
}

/*-------------------------------------------------------------------
 * get bounding box of node
 */
layout_geometry &graph_layout::get_node_bbox(void *node_id)
{
  layout_node *n = find_node(node_id);
  return (n->geom);
}

/*-------------------------------------------------------------------
 * layout in a tree structure
 *
 * Algorithm:
 * 1) Do a topological sort, determine the depth of each node
 * 2) set the y position of each node, do it in a post-order traversal
 * 3) set the x position of each node according to its depth
 *
 */

int graph_layout::layout_tree(void)
{
  /* topological sort */
  toposort();

  /* initialize layout priorities */
  layout_node_list_iter iter(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    layout_node_list visited;

    // this is just a heuristic
    n->priority = - n->find_connectivity(&visited);

#if 0
    gnode *gn = (gnode *) n->client_id;
    printf("%s : %d\n", gn->get_text(), n->priority);
#endif

  }

  /* layout in y direction */
  reset_visited();

  for (int i = 0; i < MAX_DEPTH; i++) {
    max_ypos[i] = config.yoffset;
  }

  if (root) {
    layout_tree_y(root, config.yoffset);
  }

  layout_node_list top_level_nodes;
  iter.reset(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    if (n->depth == 0) {
      top_level_nodes.append(n);
    }
  }

  while (1) {
    int max_priority = -10000;
    layout_node *next_node = NULL;
    
    iter.reset(&top_level_nodes);
    while (!iter.is_empty()) {
      layout_node *n = iter.step();
      
      if (!n->visited &&
	  n->priority > max_priority) {
	max_priority = n->priority;
	next_node = n;
      }
    }
    if (next_node == NULL) break;
    
    layout_tree_y(next_node, config.yoffset);
  }
  
  /* layout in x direction */
  layout_tree_x(config.xoffset);

  return (0);
}

void graph_layout::layout_tree_y(layout_node *n, int yoffset)
{
  layout_node *node;

  if (n->visited == FALSE) {
    n->visited = TRUE;

    int ymin = INT_MAX;		// keep track of min and max y pos of succs
    int ymax = 0;
    int depth = n->depth;

    /* set the lower bound of y coordinate for this subtree */
    int yspace = find_space(n);
    yspace = MAX(yspace, yoffset);

    /* layout the children, in order of priority */
    while (1) {
      int max_priority = -10000;
      node = NULL;

      layout_node_list_iter iter(n->succs);
      while (!iter.is_empty()) {
	layout_node *child = iter.step();
	
	if (!child->visited &&
	    child->parent == n &&
	    child->priority > max_priority) {
	  max_priority = child->priority;
	  node = child;
	}
      }
      if (node == NULL) break;

      layout_tree_y(node, yspace);
      ymin = MIN(ymin, node->geom.y);
      ymax = MAX(ymax, node->geom.y + node->geom.height);
    }

    int extra_y_spacing = FAN_SPACE_FACTOR *
      (n->preds->count() + n->succs->count());

    /* now layout the node, set y-coord of the node */
    int ycenter;
    if (ymin == INT_MAX) {
      ycenter = 0;
    } else {
      ycenter = (ymin + ymax - n->geom.height)/2;
    }
    int ypos = MAX(max_ypos[depth] + extra_y_spacing, ycenter);
    ypos = MAX(yspace, ypos);
    n->geom.y = ypos;
    ypos += n->geom.height;

    max_ypos[depth] = ypos + config.yspacing + extra_y_spacing;

    /* increase priority of nodes that are connected to this node */
    layout_node_list visited;
    increase_forest_priority(n, DPRIORITY, visited);
  }
}

void graph_layout::layout_tree_x(int xoffset)
{
  int max_width = 0;

  layout_node_list_iter iter(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    max_width = MAX(max_width, n->geom.width);
  }

#if 0
  iter.reset(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    n->geom.x = n->depth * (max_width + config.xspacing) + xoffset;
  }
#endif

#if 1
  int i;
  int max_depth = 0;

  iter.reset(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    max_depth = MAX(max_depth, n->depth);
  }

  layout_node_list **nodelists = new layout_node_list*[max_depth + 1];
  for (i = 0; i <= max_depth; i++) { 
    nodelists[i] = new layout_node_list;
  }
  int *xpos = new int[max_depth + 1];

  iter.reset(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    nodelists[n->depth]->append(n);
  }

  int delta_x = (max_width + config.xspacing);

  for (i = 0; i <= max_depth; i++) {

    int x;

    /* find a good x position for nodes at depth i */

    if (i == 0) {
      x = xoffset;
    } else {

      int x0 = xpos[i-1] + delta_x;

      float max_gradient = 3;
      int max_gradient_xdiff = delta_x/3;

      iter.reset(nodelists[i]);
      while (!iter.is_empty()) {
	layout_node *n = iter.step();
	
	layout_node_list_iter c_iter(n->preds);
	for (int j = 0; j < 2; j++) {
	  if (j == 1) {
	    c_iter.reset(n->succs);
	  }
	  while (!c_iter.is_empty()) {
	    layout_node *p = c_iter.step();
	    if (p->depth < i) {
	      int xdiff = x0 - xpos[p->depth];
	      float grad = (p->geom.y - n->geom.y)/xdiff;
	      if (grad < 0) grad = -grad;
	      if (grad > max_gradient) {
		max_gradient = grad;
		max_gradient_xdiff = xdiff;
	      }
	    }
	  }
	}
      }

      int dx = (int)(max_gradient * max_gradient_xdiff);
      int max_dx = max_width + 8 * config.xspacing;
      dx = MIN(dx, max_dx);
      x = xpos[i-1] + dx;
    }

    /* now set the x-coordinates of nodes at depth i */

    xpos[i] = x;

    iter.reset(nodelists[i]);
    while (!iter.is_empty()) {
      layout_node *n = iter.step();
      n->geom.x = x;
    }

  }

  for (i = 0; i <= max_depth; i++) {
    delete nodelists[i];
  }
  delete nodelists;
  delete xpos;

#endif
}

/*
 * find y space for a subtree at node n, i.e.
 * find the max y of already-allocated nodes
 *
 */
int graph_layout::find_space(layout_node *n)
{
  int maxy = max_ypos[n->depth];
  layout_node_list_iter iter(n->succs);
  while (!iter.is_empty()) {
    layout_node *child = iter.step();

    /* for the adjacent successor */
    if (child->parent == n) {
      int child_maxy = find_space(child);
      maxy = MAX(maxy, child_maxy);
    }
    /* for succs that is higher up in the tree (i.e. there's a cycle) */
    if (child->depth < n->depth) {
      for (int d = child->depth; d < n->depth; d++) {
	maxy = MAX(maxy, max_ypos[d]);
      }
    }
  }
  return (maxy);
}

/*
 * find max depth of tree
 *
 */
int graph_layout::find_max_depth(layout_node *n)
{
  int max_depth = n->depth;
  layout_node_list_iter iter(n->succs);
  while (!iter.is_empty()) {
    layout_node *child = iter.step();
    if (child->depth > n->depth) {
      int child_max_depth = find_max_depth(child);
      max_depth = MAX(max_depth, child_max_depth);
    }
  }
  return (max_depth);
}

#if 0
/*
 * find number of descendents that have been visited
 *
 */
int graph_layout::find_num_visited_des(layout_node *n)
{
  int num = 0;
  layout_node_list_iter iter(n->succs);
  while (!iter.is_empty()) {
    layout_node *child = iter.step();
    if (child->parent == n) {
      if (!child->visited) {
	num += num_visited_des(child) + 1;
      }
    }
  }
  return (num);
}
#endif

/*
 * increase priority of nodes that are reachable from node n
 *
 */
void graph_layout::increase_forest_priority(layout_node *n, int delta,
					    layout_node_list &visited)
{
  if (delta <= 0 || visited.lookup(n)) {
    return;
  }
  visited.push(n);

  layout_node_list_iter iter(n->succs);
  for (int j = 0; j < 2; j++) {
    if (j == 1) {
      iter.reset(n->preds);
    }
    while (!iter.is_empty()) {
      layout_node *adj = iter.step();
      if (!adj->visited && adj->parent != n) {
	adj->priority += delta;
	increase_forest_priority(adj, delta - 2, visited);
      }
    }
  }
}

/*-------------------------------------------------------------------
 * topological sort
 *
 */

void graph_layout::toposort(void)
{
  reset_visited();
  reset_depth();
  reset_priority();

  layout_node_list_iter iter(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();

    /* if this node has no predecessors */
    if (n->preds->is_empty()) {
      topo_node_sort(n, 0, 0);
    }
  }

  /* now, check those nodes that form a cycle and are not visited */

  iter.reset(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();

    if (n->visited == FALSE) {
      topo_node_sort(n, 0, 0);
    }
  }

}

void graph_layout::topo_node_sort(layout_node *n, layout_node *parent, 
				  int depth)
{
  if (n->visited == FALSE ||
      n->depth < depth) {

    n->visited = TRUE;
    n->depth = depth;
    n->parent = parent;

    /* visit successors */
    layout_node_list_iter iter(n->succs);
    while (!iter.is_empty()) {
      layout_node *child = iter.step();
      if (!cycle_exists(n, child)) {
	topo_node_sort(child, n, depth + 1);
      }
    }
  }
}

/*
 * check if a cycle exists
 * returns TRUE if "child" is an ancestor of "n"
 */

boolean graph_layout::cycle_exists(layout_node *n, layout_node *child)
{
  for (layout_node *node = n; node; node = node->parent) {
    if (node == child) {
      return (TRUE);
    }
  }
  return (FALSE);
}

/*-------------------------------------------------------------------
 * misc
 */

void graph_layout::reset_visited(void)
{
  layout_node_list_iter iter(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    n->visited = 0;
  }
}

void graph_layout::reset_depth(void)
{
  layout_node_list_iter iter(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    n->depth = 0;
  }
}

void graph_layout::reset_priority(void)
{
  layout_node_list_iter iter(nodes);
  while (!iter.is_empty()) {
    layout_node *n = iter.step();
    n->priority = 0;
  }
}
