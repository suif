/* vcommands */

#include "vcommands.h"
#include "vtcl.h"
#include "vgraph.h"
#include "vtext.h"
#include "vmenu.h"
#include "vframe.h"
#include "vtoplevel.h"
#include "vmessage.h"
#include "vlistbox.h"
#include "vform.h"
#include "vbuttonbar.h"
#include <tcl.h>
#include <stdarg.h>

/*--------------------------------------------------------------------*/
Tcl_CmdProc v_handle_error;
Tcl_CmdProc v_exit;

/*--------------------------------------------------------------------*/

command_entry v_commands[] = 
{
  { "v_handle_error", &v_handle_error, NULL, NULL},
  { "v_exit", &v_exit, NULL, NULL},
  { "vframe", &vframe::vframe_cmd, NULL, NULL},
  { "vmenu", &vmenu::vmenu_cmd, NULL, NULL},
  { "vtext", &vtext::vtext_cmd, NULL, NULL},
  { "vgraph", &vgraph::vgraph_cmd, NULL, NULL},
  { "vtoplevel", &vtoplevel::vtoplevel_cmd, NULL, NULL},
  { "vmessage", &vmessage::vmessage_cmd, NULL, NULL},
  { "vlistbox", &vlistbox::vlistbox_cmd, NULL, NULL},
  { "vform", &vform::vform_cmd, NULL, NULL},
  { "vbuttonbar", &vbuttonbar::vbuttonbar_cmd, NULL, NULL},
  { NULL, NULL, NULL, NULL}
};

/*--------------------------------------------------------------------*/
/*
 * create tcl/tk commands
 *
 */

void v_create_commands(command_entry commands[])
{

  for (int i = 0;; i++) {
    if (commands[i].name == NULL) {
      break;
    }
    tcl.create_command(commands[i].name,
		       commands[i].proc,
		       commands[i].clientData,
		       commands[i].delProc);
  }
}

/*--------------------------------------------------------------------*/
/*
 * parse first argument
 *
 */
int v_parse_firstarg(Tcl_Interp *interp, int argc, const char *argv[],
		      char *firstargs[])
{
  if (argc >= 2) {
    for (int i = 0; firstargs[i]; i++) {
      if (strcmp(argv[1], firstargs[i]) == 0) {
	return (i);
      }
    }
  }

  Tcl_AppendResult(interp, "Syntax error. First argument must be:", NULL);
  for (int i = 0; firstargs[i]; i++) {
    Tcl_AppendResult(interp, " ", firstargs[i], NULL);
  }
  return (-1);
}

/*--------------------------------------------------------------------*/
/*
 * v_wrong_argc
 *
 */

void v_wrong_argc(Tcl_Interp *interp)
{
  Tcl_SetResult(interp, "wrong # args", TCL_STATIC);
}

/*--------------------------------------------------------------------*/
/*
 * v_warning
 *
 */

void v_warning(Tcl_Interp *interp)
{
  fprintf(stderr, "Warning: %s\n", interp->result);
}

void v_warning(char *msg ...)
{
  va_list ap;
  va_start(ap, msg);

  vfprintf(stderr, msg, ap);
  va_end(ap);
}

/*--------------------------------------------------------------------*/
/*
 * v_error
 *
 */

void v_error(int return_code, char *msg)
{
  fprintf(stderr, "Error(%d): %s\n", return_code, msg);
  exit(1);
}

/*--------------------------------------------------------------------*/
/*
 * v_handle_error
 *
 */

int v_handle_error(ClientData, Tcl_Interp *interp, int /* argc */, /* unused */
		    const char * /* argv */ []) /* unused */
{
  char buffer[200];
  sprintf(buffer, "Tcl error: result was: %s\n", interp->result);
  v_error(1, buffer);
  return (TCL_OK);
}

/*--------------------------------------------------------------------*/
/*
 * v_exit
 *
 */

int v_exit(ClientData, Tcl_Interp * /* interp */ /* unused */, int argc,
	  const char *argv[])
{
  int exit_code = 0;
  if (argc > 1) {
    exit_code = atoi(argv[1]);
  }
  exit(exit_code);
  return (TCL_OK);
}

