/*--------------------------------------------------------------------
 * vbuttonbar.h
 *
 */

#ifndef VBUTTONBAR_H
#define VBUTTONBAR_H

#include "vwidget.h"

class binding;

class vbuttonbar : public vwidget {

protected:

  class button_info_list *button_list;

public:
 
  vbuttonbar(vwidget *par);
  ~vbuttonbar(void);
  void destroy(void);

  void add_button(binding *b, char *text);
  void clear(void);

  virtual int kind(void) { return WIDGET_BUTTONBAR; }

  /* interface with tcl/tk */
  static int vbuttonbar_cmd(ClientData, Tcl_Interp *interp, int argc, 
			    const char *argv[]);
};

#endif
