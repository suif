/*-------------------------------------------------------------------
 * dynatype.cc
 *
 */

#include "dynatype.h"
#include "vtcl.h"
#include <string.h>
#include <stdlib.h>

dynatype_manager typeman;

/*-------------------------------------------------------------------
 * dynatype_manager::dynatype_manager
 *
 */
dynatype_manager::dynatype_manager(void)
{
  type_list = new dynatype_list;
}

/*-------------------------------------------------------------------
 * dynatype_manager::~dynatype_manager
 *
 */
dynatype_manager::~dynatype_manager(void)
{
  delete (type_list);
}

/*-------------------------------------------------------------------
 * dynatype_manager::register_type
 *
 */
void dynatype_manager::register_type(const char *type_name, type_check_fn fn)
{
  dynatype *type = new dynatype(lexicon->enter(type_name)->sp, fn);
  type_list->append(type);
}

/*-------------------------------------------------------------------
 * dynatype_manager::lookup
 *
 */
dynatype *dynatype_manager::lookup(const char *type_name)
{
  dynatype_list_iter iter(type_list);
  while (!iter.is_empty()) {
    dynatype *type = iter.step();
    if (strcmp(type->type(), type_name) == 0) {
      return type;
    }
  }
  return (NULL);
}

/*-------------------------------------------------------------------
 * dynatype_manager::type_check
 *
 */
char *dynatype_manager::type_check(const char *type_name, const char *value,
				   char *&error_msg)
{
  dynatype *dyn = lookup(type_name);
  if (dyn) {
    return dyn->check_fn()(value, error_msg);
  } else {
    error_msg = "Cannot find specified type.";
    return NULL;
  }
}

/*-------------------------------------------------------------------
 * dynatype_manager::reset_type_iter
 */
void dynatype_manager::reset_type_iter(void)
{
  type_iter.reset(type_list);
}

/*-------------------------------------------------------------------
 * dynatype_manager::next_type
 */
dynatype *dynatype_manager::next_type(void)
{
  if (type_iter.is_empty()) {
    return (NULL);
  } else {
    return type_iter.step();
  }
}
