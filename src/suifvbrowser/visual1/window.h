/* window.h */

#ifndef WINDOW_H
#define WINDOW_H

/*
 * A "window" class is a basic top-level window in the visual system.
 * The "create_window" method is used to create the window; it is a
 * virtual function and subclasses should override this with any
 * additional code.
 * 
 * When the window is destroyed, the "destroyed" method is called by
 * tcl/tk, and the window object is then deleted.
 */

#include <suif1.h>
#include "vtoplevel.h"

class binding_list;
class event;


/*
 * window
 */

class window {
protected:
  binding_list *bindings;
  vtoplevel *toplevel;

public:
  window(void);
  virtual ~window(void);
  virtual void destroy(void);
  virtual void create_window(void);
  virtual void destroyed(void);

  /* must override this method to define the class name */
  virtual char *class_name(void) { return "no-name"; }

  /* misc */
  vtoplevel *toplevel_window(void) const { return toplevel; }

  /* display methods */
  void raise(void) { if (toplevel) toplevel->raise(); }
  void lower(void) { if (toplevel) toplevel->lower(); }
  void iconify(void) { if (toplevel) toplevel->iconify(); }
  void deiconify(void) { if (toplevel) toplevel->deiconify(); }
  void withdraw(void) { if (toplevel) toplevel->withdraw(); }

};

DECLARE_LIST_CLASS(window_list, window *);


/*
 * window_class
 */
 
typedef window *(*window_cons_fn)(void);

class window_class {
private:
  window_cons_fn cons_fn;	// constructor fn
  const char *nm;		// name

public:
  window_class(char *n, window_cons_fn fn) {
    nm = lexicon->enter(n)->sp;
    cons_fn = fn;
  }

  const char *name(void) { return nm; }
  window_cons_fn constructor(void) { return cons_fn; }
};

DECLARE_LIST_CLASS(window_class_list, window_class *);

#endif
