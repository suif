/*-------------------------------------------------------------------
 * event.cc
 *
 */

#include "event.h"
#include "binding.h"
#include <stdio.h>
#include <stdlib.h>

struct event_binding {
  binding *b;
  int event_k;
};

DECLARE_LIST_CLASS(event_binding_list, event_binding *);

static event_binding_list *Event_Bindings;

/*-------------------------------------------------------------------
 * init_eman
 */
void init_eman(void)
{
  Event_Bindings = new event_binding_list;
}

/*-------------------------------------------------------------------
 * exit_eman
 */
void exit_eman(void)
{
  delete Event_Bindings;
}

/*-------------------------------------------------------------------
 * post_event
 */
void post_event(const event &e)
{
  int k = e.kind();

  /* invoke the event bindings */
  for (event_binding_list_e *le = Event_Bindings->head(); le;
       le = le->next()) {
    if (k & le->contents->event_k) {
      le->contents->b->invoke(e);
    }
  }
}

/*-------------------------------------------------------------------
 * add_event_binding
 */
void add_event_binding(binding *b, int event_mask)
{
  event_binding *eb = new event_binding;;
  eb->b = b;
  eb->event_k = event_mask;
  Event_Bindings->append(eb);
}

/*-------------------------------------------------------------------
 * remove_event_binding
 */
void remove_event_binding(binding *b)
{
  for (event_binding_list_e *le = Event_Bindings->head(); le; 
       le = le->next()) {
    if (le->contents->b == b) {
      delete (le->contents);
      delete Event_Bindings->remove(le);
    }
  }
}

/*-------------------------------------------------------------------
 * set_event_mask
 */
void set_event_mask(binding *b, int event_mask)
{
  for (event_binding_list_e *le = Event_Bindings->head(); le;
       le = le->next()) {
    if (le->contents->b == b) {
      le->contents->event_k = event_mask;
    }
  }
}

