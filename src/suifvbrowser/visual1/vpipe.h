/* vpipe.h */

#ifndef VPIPE_H
#define VPIPE_H

#include <stdio.h>

class vpipe {

private:

  FILE *pipefd;
  int readpos;
  int writepos;

public:
  vpipe(void);
  ~vpipe(void);

  FILE *fd(void) { return (pipefd); }
  void clear(void);
  int size(void);

  int read(char *ptr, int size); // read from pipe, returns # of bytes
};

#endif
