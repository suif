/*-------------------------------------------------------------------
 * vlistbox
 *
 */

#include "vtcl.h"
#include "vlistbox.h"
#include "vcommands.h"
#include "binding.h"
#include "event.h"
#include <suif1.h>
#include <stdio.h>
#include <stdlib.h>

/*-------------------------------------------------------------------
 * vlistbox::vlistbox
 *
 */
vlistbox::vlistbox(vwidget *par, char *title) : vwidget(par)
{
  strcpy(wpath, par->path());
  tcl << "vlistbox_create" << wpath << title << this << tcl_end;

  entries = new x_array(10);
}

/*-------------------------------------------------------------------
 * vlistbox::~vlistbox
 *
 */
vlistbox::~vlistbox(void)
{
  int n = entries->ub();
  for (int i = 0; i < n; i++) {
    delete ((*entries)[i]);
  }
  delete (entries);
}

 /*-------------------------------------------------------------------
 * vlistbox::destroy
 *
 */
void vlistbox::destroy(void)
{
  tcl << "vlistbox_destroy" << wpath << tcl_end;
}

/*-------------------------------------------------------------------
 * vlistbox::add
 *
 */
void vlistbox::add(binding *b, vnode *object, const char *text)
{
  tcl << "vlistbox_add" << wpath << text << tcl_end;

  listbox_entry *entry = new listbox_entry(object, b);
  entries->extend((void *) entry);
}

/*-------------------------------------------------------------------
 * vlistbox::clear
 *
 */
void vlistbox::clear(void)
{
  int n = entries->ub();
  for (int i = 0; i < n; i++) {
    delete (*entries)[i];
  }
  delete (entries);
  entries = new x_array(10);

  tcl << "vlistbox_clear" << wpath << tcl_end;
}

/*--------------------------------------------------------------------*/
/* tk command
 */
int vlistbox::vlistbox_cmd(ClientData, Tcl_Interp *interp,
			   int argc, const char *argv[])
{
  static char *firstargs[] = {"select", "destroy", NULL};
  enum { SELECT = 0, DESTROY};

  int a = v_parse_firstarg(interp, argc, argv, firstargs);
  if (a < 0) {
    return (TCL_ERROR);
  }
  
  if (argc < 3) {
    v_wrong_argc(interp);
    return (TCL_ERROR);
  }
      
  vlistbox *listbox;
  sscanf(argv[2], "%p", &listbox);

  switch (a) {
  case SELECT:
    {
      if (argc < 4) {
	v_wrong_argc(interp);
	return (TCL_ERROR);
      }
      int index;
      sscanf(argv[3], "%d", &index);

      if (index < listbox->entries->ub()) {

	listbox_entry *entry = (listbox_entry *) (*listbox->entries)[index];
	vnode *vn = entry->object;
	binding *b = entry->bd;
	event e = event(vn, SELECTION, listbox);
	if (b) {
	  b->invoke(e);
	}
	post_event(e);
      }
    }
    break;

  case DESTROY:
    {
      listbox->destroy();
    }
    break;

  default:
    break;
  }
  return (TCL_OK);
}
