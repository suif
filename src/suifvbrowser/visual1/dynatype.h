/*----------------------------------------------------------------------
 * dynatype.h
 *
 *
 * A "dynatype" is a dynamic type class. This is used for having dynamic
 * typed objects, and allow dynamic type-checking in forms.
 *
 * The "dynatype_manager" class manages all dynamic types. Dynamic types
 * are registered to the manager. There should only be one instance of
 * the manager, called "typeman".
 *
 */

#ifndef DYNATYPE_H
#define DYNATYPE_H

#include <suif1.h>

typedef char *(*type_check_fn)(const char *value, char *&error_msg);

/*----------------------------------------------------------------------
 * Dynamic type
 */

class dynatype {
  const char *type_name;
  type_check_fn tc_fn;

public:
  dynatype(const char *type, type_check_fn fn) {
    type_name = type;
    tc_fn = fn;
  }

  const char *type(void) { return type_name; }
  type_check_fn check_fn(void) { return tc_fn; }
};

DECLARE_LIST_CLASS(dynatype_list, dynatype *);


/*----------------------------------------------------------------------
 * Dynamic type manager
 */

class dynatype_manager {

private:
  class dynatype_list *type_list;
  class dynatype *lookup(const char *type_name);

  dynatype_list_iter type_iter;

public:
  dynatype_manager(void);
  ~dynatype_manager(void);

  void register_type(const char *type_name, type_check_fn check_fn);
  char *type_check(const char *type_name, const char *value, char *&error_msg);

  void reset_type_iter(void);
  dynatype *next_type(void);

};

extern dynatype_manager typeman;

#endif

