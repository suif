#include <suif.h>

proc_sym *proc_for_call(in_cal *the_call)
  {
    operand function_address = the_call->addr_op();
    if (!function_address.is_expr())
        return NULL;
    instruction *addr_instr = function_address.instr();

    if (addr_instr->opcode() != io_ldc)
        return NULL;
    in_ldc *addr_ldc = (in_ldc *)addr_instr;

    immed addr_value = addr_ldc->value();
    if (!addr_value.is_symbol())
        return NULL;
    sym_node *addr_sym = addr_value.symbol();

    if (!addr_sym->is_proc())
        return NULL;
    return (proc_sym *)addr_sym;
  }
