/*
 * vbrowser.cc
 *
 *
 */

#include "includes.h"
#include "vbrowser.h"
#include "pointer_module.h"
#include "ipadeps_module.h"
#include "memdeps_module.h"
#include <stdlib.h>
#include <stdio.h>
#include <suif1.h>

static char *help_mesg =
"Visual Suif Browser\n"
"\n"
"Syntax: vbrowser [options] [suif file(s)]\n"
"\n"
"options:\n"
"	-help		   Show this help message.\n"
"	-sync		   Use synchronous mode for display server.\n"
"\n";


extern char *vbrowser_help_text;

/*----------------------------------------------------------------------
 * main
 *
 */

int main(int argc, char * argv[])
{
  /* parse command line */
  for (int i = 1; i < argc; i++) {
    char *s = argv[i];
    if (s[0] == '-') {
      if (strcmp(&s[1], "help") == 0) {
	fprintf(stderr, help_mesg);
	return 1;
      }
    }  
  }

  start_suif(argc, argv);
  init_visual1(argc, (const char**)argv);
  init_vsuif();

  /* set application help text */
  application_help_text = vbrowser_help_text;

  /* register modules */
  REGISTER_MODULE("Pointer Analysis", &pointer_module::constructor);
  REGISTER_MODULE("Static Dependence Analysis", &ipadeps_module::constructor);
  REGISTER_MODULE("Runtime Dependence Analysis", &memdeps_module::constructor);

  /* start visual suif */
  start_vsuif(argc, argv);

  visual_mainloop();

  return (0);
}

