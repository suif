/*--------------------------------------------------------------------
 * ipadeps_module.h
 *
 */

#ifndef IPADEPS_MODULE_H
#define IPADEPS_MODULE_H

#include "includes.h"

/* ipa stuff */
#define WHY_IO "i/o"
#define WHY_EXIT "exit"
#define WHY_ARRAY_NOTPRIV "array notpriv"
#define WHY_ARRAY_NOTFIN "array notfin"
#define WHY_ARRAY_BAD_PRIV "array bad priv"
#define WHY_SCALAR_NOTPRIV "scalar notpriv"
#define WHY_SCALAR_NOTFIN "scalar notfin"
#define WHY_BAD_REDUCE "reduce bad"
#define WHY_PARALLEL_INNER "inner"
#define WHY_INTERPROC "interproc"

extern const char *k_notpar;

/*----------------------------------------------------------------------
 * ipadeps_module
 */

class ipadeps_module : public module {
  typedef module inherited;

protected:
  boolean to_show_ipaall;

  vprop *loop_ipadep_prop; // primary ipa highlighting property;
  vprop_list *loop_ipadep_proplist; // list so we can do each var individually;

  // corresponding properties for source window;
  vprop *src_ipadep_prop; 
  vprop_list *src_ipadep_proplist;

  // corresponding properties for output window;
  vprop *output_ipadep_prop;
  vprop_list *output_ipadep_proplist;

  tree_proc *current_proc;

  /* static functions */
  static void do_show_ipaall(event &e, ipadeps_module *m);
  static void do_clear_ipaall(event &e, ipadeps_module *m);
  static void do_about(event &e, ipadeps_module *m);

  /* ipadeps */
  void show_dep_instrs(var_sym *, tree_for *, vprop *);
  void show_var_ipadeps(var_sym *sym, vprop *vp, tree_for *tf);
  void show_some_deps(annote *an, tree_for *tf,
		      vprop_list_e *&, vprop_list_e *&, vprop_list_e *&,
		      boolean &unchanged);
  void show_tf_ipadeps(tree_for *tf, boolean &unchanged);
  void show_annote_ipadeps(annote *an, tree_for *tf,
			   boolean &unchanged);
  void show_annotes_ipadeps(annote_list *ans, tree_for *tf,
			    boolean &unchanged);

public:
  ipadeps_module(void);
  ~ipadeps_module(void);

  virtual char *class_name(void) { 
      return "Static Memory Dependence Analysis"; } 
  virtual void handle_event(event &e);

  /* menu */
  virtual void create_menu(void);

  /* display */
  void clear(void);
  void update_ipadeps(vnode *selected_vn);

  static module *constructor(void) {
    return (new ipadeps_module);
  }

};


#endif
