/*--------------------------------------------------------------------
 * memdeps_module.h
 *
 */

#ifndef MEMDEPS_MODULE_H
#define MEMDEPS_MODULE_H

#include "includes.h"

/*----------------------------------------------------------------------
 * memdeps_module
 */

class memdeps_module : public module {
  typedef module inherited;

  friend void show_deps_i_help(instruction *, void *);

protected:
  boolean to_show_memdeps;

  vprop *memdep_prop;
  vprop_list *memdep_proplist;

  vprop *src_memdep_prop;
  vprop_list *src_memdep_proplist;

  vprop *output_memdep_prop;
  vprop_list *output_memdep_proplist;

  tree_proc *current_proc;

  /* static functions */
  static void do_show_memdeps(event &e, memdeps_module *m);
  static void do_clear_memdeps(event &e, memdeps_module *m);
  static void do_about(event &e, memdeps_module *m);

  /* memdeps */
  void get_annote_memdeps(annote *an, immed_list *im);
  void show_a_memdep(suif_object *object, vprop *vprop);
  void show_a_memdep(suif_object *obj,
		     vprop_list_e *&prop_e,
		     vprop_list_e *&sprop_e,
		     vprop_list_e *&oprop_e,
		     boolean &need_to_clear);

  void show_some_deps(immed_list *ldsts,
		      vprop_list_e *&, vprop_list_e *&, vprop_list_e *&,
		      boolean &unchanged,
		      tree_node *parent);
      
  void show_inst_memdeps(instruction *i, boolean &unchanged);
  void show_tf_memdeps(tree_for *tf, boolean &unchanged);
  void show_annote_memdeps(annote *an, boolean &unchanged);
  void show_annotes_memdeps(annote_list *ans, boolean &unchanged);

public:
  memdeps_module(void);
  ~memdeps_module(void);

  virtual char *class_name(void) { 
      return "Runtime Memory Dependence Analysis"; } 
  virtual void handle_event(event &e);

  /* menu */
  virtual void create_menu(void);

  /* display */
  void clear(void);
  void update_memdeps(vnode *selected_vn);

  static module *constructor(void) {
    return (new memdeps_module);
  }

};


#endif
