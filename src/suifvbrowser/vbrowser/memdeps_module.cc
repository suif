/*-------------------------------------------------------------------
 * memdeps_module
 *
 */

class instruction;
static void show_deps_i_help(instruction *i, void *x);

#include "memdeps_module.h"
#include "includes.h"
#include <annotes.h>
#include <stdlib.h>

const char *k_specdep = 0;
const char *k_specprof = 0;
const char *k_ldst_num = 0;

/*--------------------------------------------------------------------
 * memdeps_module::memdeps_module
 */
memdeps_module::memdeps_module(void)
{
  to_show_memdeps = FALSE;
  current_proc = NULL;

  if (!k_specdep) {
      ANNOTE(k_specdep, "speculate_dependence", TRUE);
      ANNOTE(k_specprof, "speculate_profitable", TRUE);
      ANNOTE(k_ldst_num, "ldst_num", TRUE);
  }

  memdep_prop = new vprop("memdeps");
  memdep_prop->set_background("springgreen2");
  memdep_proplist = new vprop_list;

  src_memdep_prop = new vprop("memdeps");
  src_memdep_prop->set_background("springgreen2");
  src_memdep_proplist = new vprop_list;

  output_memdep_prop = new vprop("memdeps");
  output_memdep_prop->set_background("springgreen2");
  output_memdep_proplist = new vprop_list;
}

/*--------------------------------------------------------------------
 * memdeps_module::~memdeps_module
 *
 */
memdeps_module::~memdeps_module(void)
{
  delete memdep_prop;
  vprop_list_iter vli(memdep_proplist);
  while (!vli.is_empty()) {
      delete vli.step();
  }
  delete memdep_proplist;

  delete src_memdep_prop;
  vprop_list_iter src_vli(src_memdep_proplist);
  while (!src_vli.is_empty()) {
      delete src_vli.step();
  }
  delete src_memdep_proplist;

  delete output_memdep_prop;
  vprop_list_iter out_vli(output_memdep_proplist);
  while (!out_vli.is_empty()) {
      delete out_vli.step();
  }
  delete output_memdep_proplist;
}

/*--------------------------------------------------------------------
 * memdeps_module::clear
 *
 */
void memdeps_module::clear(void)
{
    //     fprintf(stderr,"erasing vprop %x\n", memdep_prop);
    memdep_prop->erase();
    memdep_prop->update();
    
    vprop_list_iter vli(memdep_proplist);
    while (!vli.is_empty()) {
	vprop *vp = vli.step();
	// fprintf(stderr,"erasing vprop %x\n", vp);
	vp->erase();
	vp->update();
    }
    
    src_memdep_prop->erase();
    src_memdep_prop->update();
    
    vprop_list_iter src_vli(src_memdep_proplist);
    while (!src_vli.is_empty()) {
	vprop *vp = src_vli.step();
	// fprintf(stderr,"erasing vprop %x\n", vp);
	vp->erase();
	vp->update();
    }
    
    output_memdep_prop->erase();
    output_memdep_prop->update();
    
    vprop_list_iter out_vli(output_memdep_proplist);
    while (!out_vli.is_empty()) {
	vprop *vp = out_vli.step();
	// fprintf(stderr,"erasing vprop %x\n", vp);
	vp->erase();
	vp->update();
    }
    current_proc = NULL;
}

/*--------------------------------------------------------------------
 * memdeps_module::create_menu
 *
 */
void memdeps_module::create_menu(void)
{
  binding *b = new binding((bfun) &do_show_memdeps, this);
  menu->add_check(b, "", "Show runtime dependences",
		  to_show_memdeps);
  b = new binding((bfun) &do_clear_memdeps, this);
  menu->add_check(b, "", "Hide runtime dependences",
		  to_show_memdeps);

  menu->add_separator("");

  b = new binding((bfun) &do_about, this);
  menu->add_command(b, "", "About..");
}

/*--------------------------------------------------------------------
 * memdeps_module::do_about
 *
 */
void memdeps_module::do_about(event &, memdeps_module * /* m */) /* unused */
{
  display_message(NULL, 
		  "Run-time dependence Module:\n"
		  "\n"
		  "To see run-time memory dependences, you have to turn on the"
		  "`Show runtime dependences' "
		  "option. When you select a loop in "
		  "the Suif/Source/Output viewer, the dependent"
		  "loads and stores are "
		  "highlighted."
		  "  Alternately, select a "
		  "`speculate_dependence' annotation to see the relevant"
		  " load and store.  The highlights remain until another "
		  "loop is selected or until the `Hide runtime dependences' "
		  "option is selected.\n"
		);
}

/*--------------------------------------------------------------------
 * memdeps_module::do_show_memdeps
 *
 */
void memdeps_module::do_show_memdeps(event &, memdeps_module *m)
{
    m->to_show_memdeps = !m->to_show_memdeps;
    if (m->to_show_memdeps)
	fprintf(stderr,"turned on static deps\n");
    else {
	fprintf(stderr,"turned off static deps\n");
	// m->clear();
    }
}

void memdeps_module::do_clear_memdeps(event &, memdeps_module *m)
{
    m->clear();
}

/*--------------------------------------------------------------------
 * memdeps_module::show_a_memdep
 *
 */
void memdeps_module::show_a_memdep(suif_object *obj, vprop *obj_vprop)
{
    // fprintf(stderr,"show_a_memdep1\n");
    /* highlight the memdep */
    vnode *obj_vnode = create_vnode(obj);
    // fprintf(stderr,"created vnode %x for object %x\n", obj_vnode, obj);
    obj_vprop->add_node(obj_vnode);
    // fprintf(stderr,"added vnode %x to prop %x\n", obj_vnode, obj_vprop);
}

/*  actually highlight the thing;
    might want to highlight in different colors eventually, but for
    now, just use the base memdep_prop */

void memdeps_module::show_a_memdep(suif_object *obj,
				    vprop_list_e *&prop_e,
				    vprop_list_e *&sprop_e,
				    vprop_list_e *&oprop_e,
				    boolean &need_to_clear)
{
    // fprintf(stderr,"show_a_memdep2\n");
    if (need_to_clear) {
	clear();
	need_to_clear = FALSE;
    }

    // forget about prop lists for now and just use base;
    show_a_memdep(obj, memdep_prop);
}


/*--------------------------------------------------------------------
 * memdeps_module::show_some_deps
 *
 */

/* wrapper for needed info for mapping routine */
struct dep_box {
    immed_list *immeds;
    vprop *a_prop;
    vprop_list_e **prop_e;
    vprop_list_e **sprop_e;
    vprop_list_e **oprop_e;
    boolean *need_to_clear;
    memdeps_module *mdm;

    dep_box(immed_list *i0, vprop *prop0, vprop_list_e **pp1, 
	    vprop_list_e **pp2, 
	    vprop_list_e **pp3, boolean *n0, memdeps_module *mdm0) :
	immeds(i0), a_prop(prop0), prop_e(pp1), sprop_e(pp2), oprop_e(pp3),
	need_to_clear(n0), mdm(mdm0) {};
};


/* if this instruction is dependent, highlight it */
static void show_deps_i_help(instruction *i, void *x)
{
    if_ops op=i->opcode();
    if (op == io_str || op == io_memcpy || op == io_lod) {
	annote_list *anl = i->annotes();
	annote *an = anl ? anl->peek_annote(k_ldst_num) : 0;
	immed_list *il = an ? an->immeds() : 0;
	if (il) {
	    immed num = (*il)[0];
	    dep_box *data = (dep_box *) x;
	    immed_list_e *ile = data->immeds->lookup(num);
	    if (ile) {
		data->mdm->show_a_memdep(i,
					 *data->prop_e,
					 *data->sprop_e,
					 *data->oprop_e,
					 *data->need_to_clear);
	    }
	}
    }
}

static void show_deps_helper(tree_node *t, void *x)
{
    if (t->kind() == TREE_INSTR) {
	tree_instr *ti = (tree_instr *) t;
	ti->instr_map(show_deps_i_help, x);
    }
}

void memdeps_module::show_some_deps(immed_list *ldsts,
				    vprop_list_e *&prop_e,
				    vprop_list_e *&sprop_e,
				    vprop_list_e *&oprop_e,
				    boolean &need_to_clear,
				    tree_node *parent)
{
    // fprintf(stderr,"show_some_deps\n");
    if (!ldsts || ldsts->is_empty())
	return;

    dep_box d_data(ldsts, memdep_prop,
		   &prop_e, &sprop_e, &oprop_e, &need_to_clear, this);

    parent->map(show_deps_helper, (void *) &d_data);
}

void memdeps_module::get_annote_memdeps(annote *an, immed_list *immeds)
{
    if ((an->name() == k_specdep) || (an->name() == k_ldst_num)) {
	immed_list *iml = an->immeds();

	immed_list_iter ili(iml);
	while (!ili.is_empty()) {
	    immed im = ili.step();
	    if (!immeds->lookup(im))
		immeds->push(im);
	}
    }
}

void memdeps_module::show_tf_memdeps(tree_for *tf, boolean &unchanged)
{
    // fprintf(stderr,"show_tf_memdeps\n");
    // this is it;
    if (tf->are_annotations()) {
	annote_list *tfa = tf->annotes();
	annote_list_iter tfai(tfa);

	vprop_list_e *prop_e = 0;
	vprop_list_e *sprop_e = 0;
	vprop_list_e *oprop_e = 0;
	immed_list immeds;

	while (!tfai.is_empty()) {
	    annote *an = tfai.step();

	    get_annote_memdeps(an, &immeds);
	    
	    if (!immeds.is_empty()) {
		show_some_deps(&immeds, prop_e, sprop_e, oprop_e, unchanged,
			       tf);
		if (unchanged) {
		    clear();
		    unchanged = FALSE;
		}
	    }
	}
    }
}

void memdeps_module::show_annote_memdeps(annote *an, boolean &unchanged)
{
    // fprintf(stderr,"show_annote_memdeps\n");
    // this is it;

    vprop_list_e *prop_e = 0;
    vprop_list_e *sprop_e = 0;
    vprop_list_e *oprop_e = 0;
    immed_list immeds;

    get_annote_memdeps(an, &immeds);

    if (!immeds.is_empty()) {
	show_some_deps(&immeds, prop_e, sprop_e, oprop_e, unchanged,
		       current_proc);
	if (unchanged) {
	    clear();
	    unchanged = FALSE;
	}
    }
}

void memdeps_module::show_annotes_memdeps(annote_list *tfa, boolean &unchanged)
{
    // fprintf(stderr,"show_annotes_memdeps\n");
    // this is it;

    annote_list_iter tfai(tfa);
    vprop_list_e *prop_e = 0;
    vprop_list_e *sprop_e = 0;
    vprop_list_e *oprop_e = 0;
    immed_list immeds;
    
    while (!tfai.is_empty()) {
	annote *an = tfai.step();
	get_annote_memdeps(an, &immeds);
    }

    if (!immeds.is_empty())
	show_some_deps(&immeds, prop_e, sprop_e, oprop_e, unchanged,
		       current_proc);
}

struct memdep_info {
    var_sym *vs;
    vnode_list vnl;
};

/*--------------------------------------------------------------------
 * handle_event
 *
 * when a event occurs, this function is invoked
 */
void memdeps_module::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case SELECTION:
    {
      vnode *vn = e.get_object();
      update_memdeps(vn);
    }
    break;
    
  case NEW_FILESET:
    {
      clear();
    }
    break;
    
  default:
    break;
  }
}

/*--------------------------------------------------------------------
 * update_memdeps 
 *
 * update memdeps, with respect to a newly selected vnode
 */
void memdeps_module::update_memdeps(vnode *selected_vn)
{
    // fprintf(stderr,"update_memdeps:1\n");
    if (!to_show_memdeps) {
	return;
    }
    
    // fprintf(stderr,"update_memdeps:2\n");
    proc_sym *psym = get_parent_proc(selected_vn);
    tree_proc *newproc;
    if (psym) {
	if (!psym->is_in_memory()) {
	    psym->read_proc();
	}
	tree_proc *proc = psym->block();
	if (proc != current_proc) {
	    newproc = proc;
	}
    }
    
    // fprintf(stderr,"update_memdeps:3\n");
    if (!newproc) return;
    // tree_proc *oldproc = current_proc;
    current_proc = newproc;
    boolean unchanged = TRUE; // don't clear until necessary;
    
    // fprintf(stderr,"update_memdeps:4\n");
    
    const char *tag = selected_vn->get_tag();
    if (tag == tag_suif_object) {
	
	suif_object *obj = (suif_object *) selected_vn->get_object();
	
	// fprintf(stderr,"update_memdeps:5\n");
	switch (obj->object_kind()) {
	case TREE_OBJ:
	    {
		tree_node *tn = (tree_node *) obj;
		if (tn->is_for()) {
		    show_tf_memdeps((tree_for *)tn, unchanged);
		    if (unchanged) {
			clear();
			unchanged = FALSE;
		    }
		    break;
		}
	    }
	break;
	case TREE_INSTR:
	    {
		// find the aliased loads/stores;
		// not anymore
		// instruction *i = (instruction *) obj;
		// show_annotes_memdeps(i->annotes(), unchanged);
	    }
	break;
	default:
	    break;
	}
	
    } else if (tag == tag_annote) {
	annote *an = (annote *) selected_vn->get_object();
	suif_object *parent = (suif_object *) selected_vn->get_data();
	if (parent->object_kind() == TREE_OBJ) {
	    tree_node *tn = (tree_node *) parent;
	    if (tn->is_for()) {
		show_annote_memdeps(an, unchanged);
	    }
	}
	
    } else if (tag == tag_annote_list) {
	annote_list *ans = (annote_list *) selected_vn->get_object();
	suif_object *parent = (suif_object *) selected_vn->get_data();
	
	if (parent->object_kind() == TREE_OBJ) {
	    tree_node *tn = (tree_node *) parent;
	    if (tn->is_for()) {
		show_annotes_memdeps(ans, unchanged);
	    }
	}
	
    } else if (tag == tag_code_fragment) {
	
	code_fragment *f = (code_fragment *) selected_vn->get_object();
	if (f->node()) {
	    if (f->node()->is_for()) {
		show_tf_memdeps((tree_for *) f->node(), unchanged);
		if (unchanged) {
		    clear();
		    unchanged = FALSE;
		}
	    }
	}
    }
    
    // fprintf(stderr,"memdeps is unchanged\n");
    
    if (unchanged) return;
    
    memdep_prop->update();
    // fprintf(stderr,"updating vprop %x\n", memdep_prop);
    vprop_list_iter vli1(memdep_proplist);
    while (!vli1.is_empty()) {
	vprop *vp = vli1.step();
	vp->update();
	// fprintf(stderr,"updating vprop %x\n", vp);
    }
    
    /* source viewer */
    window_class *wclass = vman->find_window_class("Source Viewer");
    if (wclass) {
	src_viewer *sviewer = (src_viewer *) vman->find_window_instance(wclass);
	if (sviewer) {
	    
	    sviewer->source_tree()->suif_to_code_prop(memdep_prop, 
						      src_memdep_prop);
	    src_memdep_prop->update();
	    
	    assert(memdep_proplist->count() ==  src_memdep_proplist->count());
	    vprop_list_iter vli(memdep_proplist), svli(src_memdep_proplist);
	    while (!vli.is_empty()) {
		vprop *vp = vli.step();
		vprop *svp = svli.step();
		
		if (!vp->get_node_list()->is_empty()) {
		    sviewer->source_tree()->suif_to_code_prop(vp, svp);
		    svp->update();
		}
	    }
	}
    }
    
    /* output viewer */
    wclass = vman->find_window_class("Output Viewer");
    if (wclass) {
	output_viewer *oviewer = (output_viewer *) 
	    vman->find_window_instance(wclass);
	if (oviewer) {
	    oviewer->output_tree()->suif_to_code_prop(memdep_prop,
						      output_memdep_prop);
	    output_memdep_prop->update();
	    
	    assert(memdep_proplist->count() == 
		   output_memdep_proplist->count());
	    vprop_list_iter vli(memdep_proplist), 
		ovli(output_memdep_proplist);
	    while (!vli.is_empty()) {
		vprop *vp = vli.step();
		vprop *ovp = ovli.step();
		
		if (!vp->get_node_list()->is_empty()) {
		    oviewer->output_tree()->suif_to_code_prop(vp, ovp);
		    ovp->update();
		}
	    }
	}
    }
}
