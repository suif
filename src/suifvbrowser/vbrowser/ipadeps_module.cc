/*-------------------------------------------------------------------
 * ipadeps_module
 *
 */

#include "ipadeps_module.h"
#include "includes.h"
#include <annotes.h>
#include <stdlib.h>

const char *k_notpar = 0;
const char *k_vs_vprop = 0;
#define why_array_size 7
char *why_array[why_array_size] = { 
		 WHY_ARRAY_NOTPRIV,
		 WHY_ARRAY_NOTFIN,
		 WHY_ARRAY_BAD_PRIV,
		 WHY_BAD_REDUCE,
		 WHY_SCALAR_NOTPRIV,
		 WHY_SCALAR_NOTFIN,
		 WHY_BAD_REDUCE
};
immed why_immed_array[why_array_size];

/*--------------------------------------------------------------------
 * ipadeps_module::ipadeps_module
 */
ipadeps_module::ipadeps_module(void)
{
    to_show_ipaall = FALSE;
    current_proc = NULL;
    
    int i;
    if (!k_notpar) {
	ANNOTE(k_notpar, "no doall", TRUE);
	k_vs_vprop = lexicon->enter("ipadeps vprop")->sp;
	
	for (i=0; i<why_array_size; i++) {
	    why_immed_array[i] = immed(why_array[i]);
	}
    }
    
    loop_ipadep_prop = new vprop("ipadeps");
    loop_ipadep_prop->set_background("#d0c0e0");
    loop_ipadep_proplist = new vprop_list;
    
    src_ipadep_prop = new vprop("ipadeps");
    src_ipadep_prop->set_background("#d0c0e0");
    src_ipadep_proplist = new vprop_list;
    
    output_ipadep_prop = new vprop("ipadeps");
    output_ipadep_prop->set_background("#d0c0e0");
    output_ipadep_proplist = new vprop_list;
}

/*--------------------------------------------------------------------
 * ipadeps_module::~ipadeps_module
 *
 */
ipadeps_module::~ipadeps_module(void)
{
    delete loop_ipadep_prop;
    vprop_list_iter vli(loop_ipadep_proplist);
    while (!vli.is_empty()) {
	delete vli.step();
    }
    delete loop_ipadep_proplist;
    
    delete src_ipadep_prop;
    vprop_list_iter src_vli(src_ipadep_proplist);
    while (!src_vli.is_empty()) {
	delete src_vli.step();
    }
    delete src_ipadep_proplist;
    
    delete output_ipadep_prop;
    vprop_list_iter out_vli(output_ipadep_proplist);
    while (!out_vli.is_empty()) {
	delete out_vli.step();
    }
    delete output_ipadep_proplist;
}

/*--------------------------------------------------------------------
 * ipadeps_module::clear
 *
 */
void ipadeps_module::clear(void)
{
    // fprintf(stderr,"erasing vprop %x\n", loop_ipadep_prop);
    loop_ipadep_prop->erase();
    loop_ipadep_prop->update();
    
    vprop_list_iter vli(loop_ipadep_proplist);
    while (!vli.is_empty()) {
	vprop *vp = vli.step();
	void *v_vs = vp->get_client_data();
	if (v_vs) {
	    var_sym *vs = (var_sym *) v_vs;
	    annote *an = vs->annotes()->get_annote(k_vs_vprop);
	    if (an) delete an;
	}
	// fprintf(stderr,"erasing vprop %x\n", vp);
	vp->erase();
	vp->update();
    }
    vprop_list_iter src_vli(src_ipadep_proplist);
    while (!src_vli.is_empty()) {
	vprop *vp = src_vli.step();
	void *v_vs = vp->get_client_data();
	if (v_vs) {
	    var_sym *vs = (var_sym *) v_vs;
	    annote *an = vs->annotes()->get_annote(k_vs_vprop);
	    if (an) delete an;
	}
	// fprintf(stderr,"erasing vprop %x\n", vp);
	vp->erase();
	vp->update();
    }
    vprop_list_iter out_vli(output_ipadep_proplist);
    while (!out_vli.is_empty()) {
	vprop *vp = out_vli.step();
	void *v_vs = vp->get_client_data();
	if (v_vs) {
	    var_sym *vs = (var_sym *) v_vs;
	    annote *an = vs->annotes()->get_annote(k_vs_vprop);
	    if (an) delete an;
	}
	// fprintf(stderr,"erasing vprop %x\n", vp);
	vp->erase();
	vp->update();
    }
    current_proc = NULL;
}

/*--------------------------------------------------------------------
 * ipadeps_module::create_menu
 *
 */
void ipadeps_module::create_menu(void)
{
  binding *b = new binding((bfun) &do_show_ipaall, this);
  menu->add_check(b, "", "Show static dependences",
		  to_show_ipaall);
  b = new binding((bfun) &do_clear_ipaall, this);
  menu->add_check(b, "", "Hide static dependences",
		  to_show_ipaall);

  menu->add_separator("");

  b = new binding((bfun) &do_about, this);
  menu->add_command(b, "", "About..");
}

/*--------------------------------------------------------------------
 * ipadeps_module::do_about
 *
 */
void ipadeps_module::do_about(event &, ipadeps_module * /* m */) /* unused */
{
  display_message(NULL, 
		  "Static Dependence Module:\n"
		  "\n"
		  "To see memory dependences, you have to turn on the"
		  "`Show static dependences' "
		  "option. When you select a loop in "
		  "the Suif/Source/Output viewer, the dependent "
		  "loads and stores are "
		  "highlighted.  They remain until another loop is "
		  "selected or until the `Hide Static Dependences' option "
		  "is selected.\n"
		);
}

/*--------------------------------------------------------------------
 * ipadeps_module::do_show_ipaall
 *
 */
void ipadeps_module::do_show_ipaall(event &, ipadeps_module *m)
{
    m->to_show_ipaall = !m->to_show_ipaall;
    if (m->to_show_ipaall) {
	fprintf(stderr,"turned on static deps\n");
	m->clear();
    } else
	fprintf(stderr,"turned off static deps\n");
}

void ipadeps_module::do_clear_ipaall(event &, ipadeps_module *m)
{
    m->clear();
}

#if 0
static int colors[] = {
    139,  58,  98,        //     hotpink4
    139,  34,  82,        //     violetred4
    //    255, 245, 238,        //     seashell
    173, 216, 230,        //     lightblue
    175, 238, 238,        //     pale turquoise
    60 , 179, 113,         //    medium sea green
    255, 255,   0,        //     yellow
    222, 184, 135,        //     burlywood
    244, 164,  96,        //     sandybrown
    255, 165,   0,        //     orange
    160,  32, 240,        //     purple
    // 238, 223, 204,        //     antiquewhite2
    // 139, 139, 131,        //     ivory4
    67 , 205, 128,   //  
    46 , 139,  87,   //          seagreen4
    0  , 255,   0,    //         green1
    0  , 139,   0,    //         green4
    127, 255,   0,  //           chartreuse1
    238, 201,   0,  //           gold2
    238, 106,  80,  //           coral2
    255,  69,   0,  //           orangered1
    139,  99, 108,  //           pink4
    238, 174, 238,  //           plum2
    144, 238, 144,  //           light green
    113, 198, 113,    //         sgi chartreuse
    173, 216, 230,    //         light blue
    72 , 209, 204,     //        mediumturquoise
    143, 188, 143,    //         dark sea green
    219, 112, 147,    //         palevioletred
    153,  50, 204,    //         darkorchid
    138,  43, 226,    //         blue violet
    220, 220, 220,    //         gainsboro
    240, 255, 240,    //         honeydew
    255, 228, 225,    //         mistyrose
    112, 128, 144, //  
    135, 206, 235, //             skyblue
    176, 196, 222, //             light steel blue
    224, 255, 255, //             lightcyan
    50 , 205,  50,  //            limegreen
    154, 205,  50, //             yellowgreen
    218, 165,  32, //             goldenrod
    205, 133,  63, //             peru
    // 255, 160, 122, //             light salmon
    199,  21, 133, //             medium violet red
    // 139, 137, 137, //             snow4
    238, 213, 183, //             bisque2
    205, 183, 158, //             bisque3
    240, 255, 255, //             azure1
    131, 111, 255, //            slateblue1
    71 , 60 , 139,    //         slateblue4
    135, 206, 255,     //        skyblue1
    // 141, 182, 205,    //         lightskyblue3
    102, 139, 139,    //         paleturquoise4
    142, 229, 238,    //         cadetblue2
    69 , 139, 116,     //        aquamarine4
    0  , 238, 118,      //       springgreen2
    192, 255,  62,    //         olivedrab1
    238, 238,   0,    //         yellow2
    205, 155,  29,    //         goldenrod3
    139, 105,  20,    //         goldenrod4
    255, 211, 155,    //         burlywood1
    205, 133,   0,    //         orange3
    205,  79,  57,    //         tomato3
    178,  58, 238,    //   darkorchid2
    0  , 0  ,   0
};
static int colornum = 0;
static char colorbuffer[256];
#endif

static char *getnewcolor()
{
    // currently, don't distinguish them by color;
    return ("yellow");
#if 0
    int r, g, b;
    if (!(colorbuffer[colornum] || colorbuffer[colornum+1] ||
	  colorbuffer[colornum+2]))
	colornum = 0;
    sprintf(&colorbuffer[0], "#%2x%2x%2x", colors[colornum++],
	    colors[colornum++], colors[colornum++]);
    return (lexicon->enter(&(colorbuffer[0]))->sp);
#endif
}

/*--------------------------------------------------------------------
 * ipadeps_module::show_tf_ipadeps
 *   
 */

/* wrapper for needed info for mapping routine */
struct sdep_box {
    var_sym *vs;
    vprop *a_prop;
    ipadeps_module *ipm;
    boolean found;

    sdep_box(var_sym *vs0, vprop *vp0, ipadeps_module *m) :
	vs(vs0), a_prop(vp0), ipm(m), found(FALSE) {};
};

/* check whether any operand of this instruction contains the symbol */
static boolean sdep_check_op(instruction *i, operand *r, void *x)
{
    sdep_box *sdb = (sdep_box *) x;
    if (r->is_symbol() && (r->symbol() == sdb->vs)) {
	sdb->found = TRUE;
    }
    return FALSE;
}

/* if this instruction contains the symbol, highlight it */
static void show_sdeps_inst(instruction *i, void *x)
{
    sdep_box *sdb = (sdep_box *) x;

    sdb->found = FALSE;
    operand dst = i->dst_op();
    sdep_check_op(i, &dst, x);
    if (!sdb->found) {
	if (i->opcode() == io_ldc) {
	    immed v = ((in_ldc *) i)->value();
	    if (v.is_symbol() && v.symbol() == sdb->vs)
		sdb->found = TRUE;
	} else
	    i->src_map(sdep_check_op, x);
    }
    if (sdb->found) {
	// add instruction;
	vnode *ivn = create_vnode(i);
	// fprintf(stderr,"created vnode %x for instr ",ivn);
	// i->print(stderr); fprintf(stderr,"\n");
	sdb->a_prop->add_node(ivn);
	// fprintf(stderr,"added vnode %x to prop %x\n", ivn, sdb->a_prop);
    }
}

/* map over each tree, showing each instruction containing the symbol */
static void show_sdeps_helper(tree_node *t, void *x)
{
    if (t->kind() == TREE_INSTR) {
	tree_instr *ti = (tree_instr *) t;
	ti->instr_map(show_sdeps_inst, x);
    }
}

/* show all instructions which use vs, in property prop */
void ipadeps_module::show_dep_instrs(var_sym *vs, tree_for *tf,
				     vprop *prop)
{
    // fprintf(stderr,"show_dep_instrs\n");
    sdep_box i_data(vs, prop, this);

    tf->map(show_sdeps_helper, (void *) &i_data);
}

/* given an annotation, if it's a notpar annotation,
   find the referenced symbols and mark them;
   add a property to the lists for each;
   if need_to_clear, then clear properties in the lists first if
   we do find a valid symbol to highlight */
void ipadeps_module::show_some_deps(annote *an, tree_for *tf,
				    vprop_list_e *&prop_e,
				    vprop_list_e *&sprop_e,
				    vprop_list_e *&oprop_e,
				    boolean &need_to_clear)
{
    // check for a notpar annotation;
    if (an->name() != k_notpar) 
	return;
    
    // fprintf(stderr,"found a no_doall annot\n");

    int whyi;
    immed_list *immeds = an->immeds();
    
    if (immeds->is_empty())
	return;
    
    immed head = (*immeds)[0];
    for (whyi=0; whyi < why_array_size; whyi++) {
	if (head == why_immed_array[whyi])
	    break;
    }
    if (whyi >= why_array_size) 
	return;
    
    // fprintf(stderr,"found a no_doall of interest\n");
    
    // it's something of interest;
    immed_list_iter ili(immeds);
    (void) ili.step();
    while (!ili.is_empty()) {
	immed immed1 = ili.step();
	if (immed1.is_symbol()) {
	    sym_node *thesym = immed1.symbol();
	    assert(thesym->is_var());
	    var_sym *thevs = (var_sym *) thesym;
	    
	    // fprintf(stderr,"looking at var_sym ");
	    // thevs->print(stderr);
	    // fprintf(stderr,"\n");
	    
	    if (!to_show_ipaall)
		continue;
	    
	    // fprintf(stderr,"it's of interest\n");
	    
	    if (need_to_clear) {
		clear();
		need_to_clear = FALSE;
		prop_e = loop_ipadep_proplist->head();
		sprop_e = src_ipadep_proplist->head();
		oprop_e = output_ipadep_proplist->head();
	    }

	    void *oldprop = thevs->peek_annote(k_vs_vprop);
	    if (oldprop) continue;
	    
	    // fprintf(stderr,"not already done\n");
	    
	    // set up a property for it;
	    if (!prop_e || !prop_e->next()) {
		vprop *vn = new vprop("ipadeps");
		char *newcolor = getnewcolor();
		vprop *svn = new vprop("ipadeps");
		vprop *ovn = new vprop("ipadeps");
		vn->set_background(newcolor);
		svn->set_background(newcolor);
		ovn->set_background(newcolor);
		prop_e = new vprop_list_e(vn);
		sprop_e = new vprop_list_e(svn);
		oprop_e = new vprop_list_e(ovn);
		
		// fprintf(stderr,"created prop %x with color %s\n",
		// vn, newcolor);
		
		loop_ipadep_proplist->append(prop_e);
		src_ipadep_proplist->append(sprop_e);
		output_ipadep_proplist->append(oprop_e);
	    } else {
		prop_e = prop_e->next();
		sprop_e = sprop_e->next();
		oprop_e = oprop_e->next();
	    }
	    
	    // record linking between thevs and vprop;
	    vprop *vp = prop_e->contents;
	    vp->set_client_data((void *)thevs);
	    thevs->append_annote(k_vs_vprop, (void *)vp);
	    
	    // now, add the vnodes;
	    
	    // fprintf(stderr,"adding var "); thevs->print(stderr);
	    // fprintf(stderr," to prop %x\n", vp);
	    show_var_ipadeps(thevs, vp, tf);
	}
    }
}

/* show the ipa dependences for a given tree_for */
void ipadeps_module::show_tf_ipadeps(tree_for *tf, boolean &unchanged)
{
    // fprintf(stderr,"show_tf_ipadeps\n");
    // this is it;
    if (tf->are_annotations()) {
	annote_list *tfa = tf->annotes();
	annote_list_iter tfai(tfa);
	vprop_list_e *prop_e = 0;
	vprop_list_e *sprop_e = 0;
	vprop_list_e *oprop_e = 0;

	while (!tfai.is_empty()) {
	    annote *an = tfai.step();

	    show_some_deps(an, tf, prop_e, sprop_e, oprop_e, unchanged);
	}
    }
}

/* show the ipa dependences for a given annote */
void ipadeps_module::show_annote_ipadeps(annote *an, tree_for *tf,
					 boolean &unchanged)
{
    // fprintf(stderr,"show_annote_ipadeps\n");
    // this is it;

    vprop_list_e *prop_e = 0;
    vprop_list_e *sprop_e = 0;
    vprop_list_e *oprop_e = 0;
    
    show_some_deps(an, tf, prop_e, sprop_e, oprop_e, unchanged);
}

/* show the ipa dependences for a given annote_list */
void ipadeps_module::show_annotes_ipadeps(annote_list *tfa, tree_for *tf,
					  boolean &unchanged)
{
    // fprintf(stderr,"show_annotes_ipadeps\n");
    // this is it;

    annote_list_iter tfai(tfa);
    vprop_list_e *prop_e = 0;
    vprop_list_e *sprop_e = 0;
    vprop_list_e *oprop_e = 0;
    
    while (!tfai.is_empty()) {
	annote *an = tfai.step();
	
	show_some_deps(an, tf, prop_e, sprop_e, oprop_e, unchanged);
    }
}

struct ipadep_info {
    var_sym *vs;
    vnode_list vnl;
};

/*--------------------------------------------------------------------
 * ipadeps_module::show_var_ipadeps
 *
 */
/* show the ipa dependences for the given var in tf in property var_vprop */
void ipadeps_module::show_var_ipadeps(var_sym *var, vprop *var_vprop,
				      tree_for *tf)
{
    ipadep_info ip_info;
    
    // fprintf(stderr, "show_var_ipadeps ");
    // var->print(stderr); 
    // fprintf(stderr, "\n");

    ip_info.vs = var;

    /* find ipadeps */
    /*    tf->map((tree_map_f) &find_tf_var, &ip_info); */

    /* highlight the ipadeps */
    vnode *var_vnode = create_vnode(var);
    // fprintf(stderr,"created vnode %x for var ",var_vnode);
    // var->print(stderr); fprintf(stderr,"\n");
    var_vprop->add_node(var_vnode);
    // fprintf(stderr,"added vnode %x to prop %x\n", var_vnode, var_vprop);

    // add instructions;
    show_dep_instrs(var, tf, var_vprop);
}

/*--------------------------------------------------------------------
 * handle_event
 *
 * when a event occurs, this function is invoked
 */
void ipadeps_module::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case SELECTION:
    {
      vnode *vn = e.get_object();
      update_ipadeps(vn);
    }
    break;
    
  case NEW_FILESET:
    {
      clear();
    }
    break;
    
  default:
    break;
  }
}

/*--------------------------------------------------------------------
 * update_ipadeps 
 *
 * update ipadeps, with respect to a newly selected vnode
 */
void ipadeps_module::update_ipadeps(vnode *selected_vn)
{
    // fprintf(stderr,"update_ipadeps:1\n");
    if (!to_show_ipaall) {
	return;
    }

    // fprintf(stderr,"update_ipadeps:2\n");
    proc_sym *psym = get_parent_proc(selected_vn);
    tree_proc *newproc;
    if (psym) {
	if (!psym->is_in_memory()) {
	    psym->read_proc();
	}
	tree_proc *proc = psym->block();
	if (proc != current_proc) {
	    newproc = proc;
	}
    }
    
    // fprintf(stderr,"update_ipadeps:3\n");
    if (!newproc) return;
    // tree_proc *oldproc = current_proc;
    current_proc = newproc;
    boolean unchanged = TRUE; // don't clear until necessary;
    
    // fprintf(stderr,"update_ipadeps:4\n");
    
    const char *tag = selected_vn->get_tag();
    if (tag == tag_suif_object) {
	
	suif_object *obj = (suif_object *) selected_vn->get_object();
	
	// fprintf(stderr,"update_ipadeps:5\n");
	switch (obj->object_kind()) {
	case TREE_OBJ:
	    {
		tree_node *tn = (tree_node *) obj;
		if (tn->is_for()) {
		    show_tf_ipadeps((tree_for *)tn, unchanged);
		    if (unchanged) {
			clear();
			unchanged = FALSE;
		    }
		    break;
		}
	    }
	break;
	
	default:
	    break;
	}
	
    } else if (tag == tag_annote) {
	annote *an = (annote *) selected_vn->get_object();
	suif_object *parent = (suif_object *) selected_vn->get_data();
	if (parent->object_kind() == TREE_OBJ) {
	    tree_node *tn = (tree_node *) parent;
	    if (tn->is_for()) {
		show_annote_ipadeps(an, (tree_for *)tn, unchanged);
	    }
	}
	
    } else if (tag == tag_annote_list) {
	annote_list *ans = (annote_list *) selected_vn->get_object();
	suif_object *parent = (suif_object *) selected_vn->get_data();
	
	if (parent->object_kind() == TREE_OBJ) {
	    tree_node *tn = (tree_node *) parent;
	    if (tn->is_for()) {
		show_annotes_ipadeps(ans, (tree_for *)tn, unchanged);
	    }
	}
	
    } else if (tag == tag_code_fragment) {
	
	code_fragment *f = (code_fragment *) selected_vn->get_object();
	if (f->node()) {
	    if (f->node()->is_for()) {
		show_tf_ipadeps((tree_for *) f->node(), unchanged);
		if (unchanged) {
		    clear();
		    unchanged = FALSE;
		}
	    }
	}
    }
    
    if (unchanged) return;
    
    loop_ipadep_prop->update();
    // fprintf(stderr,"updating vprop %x\n", loop_ipadep_prop);
    vprop_list_iter vli1(loop_ipadep_proplist);
    while (!vli1.is_empty()) {
	vprop *vp = vli1.step();
	vp->update();
	// fprintf(stderr,"updating vprop %x\n", vp);
    }
    
    /* source viewer */
    window_class *wclass = vman->find_window_class("Source Viewer");
    if (wclass) {
	src_viewer *sviewer = 
	    (src_viewer *) vman->find_window_instance(wclass);
	if (sviewer) {
	    
	    sviewer->source_tree()->suif_to_code_prop(loop_ipadep_prop, 
						      src_ipadep_prop);
	    src_ipadep_prop->update();
	    
	    assert(loop_ipadep_proplist->count()
		   ==src_ipadep_proplist->count());
	    vprop_list_iter vli(loop_ipadep_proplist), 
		svli(src_ipadep_proplist);
	    while (!vli.is_empty()) {
		vprop *vp = vli.step();
		vprop *svp = svli.step();
		
		if (!vp->get_node_list()->is_empty()) {
		    sviewer->source_tree()->suif_to_code_prop(vp, svp);
		    svp->update();
		}
	    }
	}
    }
    
    /* output viewer */
    wclass = vman->find_window_class("Output Viewer");
    if (wclass) {
	output_viewer *oviewer = (output_viewer *) 
	    vman->find_window_instance(wclass);
	if (oviewer) {
	    oviewer->output_tree()->suif_to_code_prop(loop_ipadep_prop,
						      output_ipadep_prop);
	    output_ipadep_prop->update();
	    
	    assert(loop_ipadep_proplist->count() == 
		   output_ipadep_proplist->count());
	    vprop_list_iter vli(loop_ipadep_proplist), 
		ovli(output_ipadep_proplist);
	    while (!vli.is_empty()) {
		vprop *vp = vli.step();
		vprop *ovp = ovli.step();
		
		if (!vp->get_node_list()->is_empty()) {
		    oviewer->output_tree()->suif_to_code_prop(vp, ovp);
		    ovp->update();
		}
	    }
	}
    }
}
