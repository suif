/*--------------------------------------------------------------------
 * pointer_module.h
 *
 */

#ifndef POINTER_MODULE_H
#define POINTER_MODULE_H

#include "includes.h"

/*----------------------------------------------------------------------
 * pointer_module
 */

class pointer_module : public module {
  typedef module inherited;

protected:
  boolean to_show_aliases;

  vprop *alias_prop;
  vprop *output_alias_prop;
  vprop *src_alias_prop;

  tree_proc *current_proc;

  /* static functions */
  static void do_show_aliases(event &e, pointer_module *m);
  static void do_clear_aliases(event &e, pointer_module *m);
  static void do_about(event &e, pointer_module *m);

  /* aliases */
  void show_var_aliases(var_sym *sym);
  void show_instr_aliases(instruction *instr);
  void show_ti_aliases(tree_instr *ti);

  static void show_ti_aliases_helper(instruction *instr,
				     pointer_module *m);

public:
  pointer_module(void);
  ~pointer_module(void);

  virtual char *class_name(void) { return "Pointer Analysis"; }
  virtual void handle_event(event &e);

  /* menu */
  virtual void create_menu(void);

  /* display */
  void clear(void);
  void update_aliases(vnode *selected_vn);

  static module *constructor(void) {
    return (new pointer_module);
  }

};


#endif
