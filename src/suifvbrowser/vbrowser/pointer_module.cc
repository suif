/*-------------------------------------------------------------------
 * pointer_module
 *
 */

#include "pointer_module.h"
#include "includes.h"
#include <annotes.h>
#include <stdlib.h>


struct alias_info {
  /* aliased with an instruction */
  w_pval_list *pval;
  int clone_num;
  base_symtab *scope;

  /* aliased with a variable */
  var_sym *var;

  vnode_list aliases;
};

/*--------------------------------------------------------------------
 * pointer_module::pointer_module
 */
pointer_module::pointer_module(void)
{
  to_show_aliases = FALSE;
  current_proc = NULL;

  alias_prop = new vprop("aliases");
  alias_prop->set_background("#d0c0e0");
  src_alias_prop = new vprop("aliases");
  src_alias_prop->set_background("#d0c0e0");
  output_alias_prop = new vprop("aliases");
  output_alias_prop->set_background("#d0c0e0");
}

/*--------------------------------------------------------------------
 * pointer_module::~pointer_module
 *
 */
pointer_module::~pointer_module(void)
{
  delete alias_prop;
  delete src_alias_prop;
  delete output_alias_prop;
}

/*--------------------------------------------------------------------
 * pointer_module::clear
 *
 */
void pointer_module::clear(void)
{
  alias_prop->erase();
  alias_prop->update();
  src_alias_prop->erase();
  src_alias_prop->update();
  output_alias_prop->erase();
  output_alias_prop->update();

  current_proc = NULL;
}

/*--------------------------------------------------------------------
 * pointer_module::create_menu
 *
 */
void pointer_module::create_menu(void)
{
  binding *b = new binding((bfun) &do_show_aliases, this);
  menu->add_check(b, "", "Show aliases", to_show_aliases);
  b = new binding((bfun) &do_clear_aliases, this);
  menu->add_check(b, "", "Hide aliases", to_show_aliases);

  menu->add_separator("");

  b = new binding((bfun) &do_about, this);
  menu->add_command(b, "", "About..");
}

/*--------------------------------------------------------------------
 * pointer_module::do_about
 *
 */
void pointer_module::do_about(event &, pointer_module * /* m */) /* unused */
{
  display_message(NULL, 
		  "Pointer Analysis Module:\n"
		  "\n"
		  "To see aliases, you have to turn on the `Show aliases' "
		  "option. When you select an instruction or symbol in "
		  "the Suif/Source/Output viewer, the aliases are "
		  "highlighted in purple color.  The aliases remain "
		  "highlighted until another instruction or symbol is "
		  "selected, or the `Hide aliases' option is selected\n"
		);
}

/*--------------------------------------------------------------------
 * pointer_module::do_show_aliases
 *
 */
void pointer_module::do_show_aliases(event &, pointer_module *m)
{
  if (m->to_show_aliases) {

    m->to_show_aliases = FALSE;

    /*
    m->alias_prop->erase();
    m->alias_prop->update();
    m->src_alias_prop->erase();
    m->src_alias_prop->update();
    m->output_alias_prop->erase();
    m->output_alias_prop->update();
    */

  } else {
    m->to_show_aliases = TRUE;
  }
}

void pointer_module::do_clear_aliases(event &, pointer_module *m)
{
    m->alias_prop->erase();
    m->alias_prop->update();
    m->src_alias_prop->erase();
    m->src_alias_prop->update();
    m->output_alias_prop->erase();
    m->output_alias_prop->update();
}

/*--------------------------------------------------------------------
 * find_aliases
 */
static void find_instr_aliases_with_instr(instruction *instr,
					  alias_info *a_info)
{
  if_ops op = instr->opcode();
  boolean is_aliased = FALSE;

  if (op == io_str || op == io_memcpy) {
    if (instr->are_annotations()) {
      annote_list_iter iter(instr->annotes());
      while (!iter.is_empty()) {
	annote *ann = iter.step();
	if (ann->name() == k_wilbyr_store_to) {
	  w_points_to *wpt = WILBYR_STORE_TO(ann);
	  if (wpt &&
	      wpt->clone_num() == a_info->clone_num &&
	      a_info->pval->aliased(wpt->points_to())) {
	    is_aliased = TRUE;
	  }
	}
      }
    }
  }
  
  if (op == io_lod || op == io_memcpy) {
    if (instr->are_annotations()) {
      annote_list_iter iter(instr->annotes());
      while (!iter.is_empty()) {
	annote *ann = iter.step();
	if (ann->name() == k_wilbyr_load_from) {
	  w_points_to *wpt = WILBYR_LOAD_FROM(ann);
	  if (wpt &&
	      wpt->clone_num() == a_info->clone_num &&
	      a_info->pval->aliased(wpt->points_to())) {
	    is_aliased = TRUE;
	  }
	}
      }
    }
  }
  
  if (is_aliased) {
    vnode *vn = create_vnode(instr);
    if (!a_info->aliases.lookup(vn)) {
      a_info->aliases.append(vn);
    }
  }

  /* examine expression tree */
  int n = instr->num_srcs();
  for (int i = 0; i < n; i++) {
    operand src = instr->src_op(i);
    if (src.is_symbol()) {
      /* symbol alias */
      if (a_info->pval->aliased(src.symbol(), a_info->scope,
				a_info->clone_num)) {
	vnode *vn = create_vnode(src.symbol());
	if (!a_info->aliases.lookup(vn)) {
	  a_info->aliases.append(vn);
	}
      }
    } else if (src.is_instr()) {
      find_instr_aliases_with_instr(src.instr(), a_info);
    }
  }
}

static void find_tn_aliases_with_instr(tree_node *tn, alias_info *a_info)
{
  if (tn->is_instr()) {
    ((tree_instr *)tn)->instr_map((instr_map_f) &find_instr_aliases_with_instr,
				  a_info);
  }
}

static void find_instr_aliases_with_var(instruction *instr,
					alias_info *a_info)
{
  if_ops op = instr->opcode();
  boolean is_aliased = FALSE;

  if (op == io_str || op == io_memcpy) {
    annote *ann = instr->annotes()->peek_annote(k_wilbyr_store_to);
    if (ann) {
      w_points_to *wpt = WILBYR_STORE_TO(ann);
      if (wpt &&
	  wpt->points_to()->aliased(a_info->var, instr->parent()->scope(),
				    wpt->clone_num())) {
	is_aliased = TRUE;
      }
    }
  }

  if (op == io_lod || op == io_memcpy) {
    annote *ann = instr->annotes()->peek_annote(k_wilbyr_load_from);
    if (ann) {
      w_points_to *wpt = WILBYR_LOAD_FROM(ann);
      if (wpt &&
	  wpt->points_to()->aliased(a_info->var, instr->parent()->scope(),
				    wpt->clone_num())) {
	is_aliased = TRUE;
      }
    }
  }

  if (is_aliased) {
    vnode *vn = create_vnode(instr);
    if (!a_info->aliases.lookup(vn)) {
      a_info->aliases.append(vn);
    }
  }

  /* examine expression tree */
  int n = instr->num_srcs();
  for (int i = 0; i < n; i++) {
    operand src = instr->src_op(i);
    if (src.is_instr()) {
      find_instr_aliases_with_var(src.instr(), a_info);
    }
  }
}

static void find_tn_aliases_with_var(tree_node *tn, alias_info *a_info)
{
  if (tn->is_instr()) {
    find_instr_aliases_with_var(((tree_instr *)tn)->instr(), a_info);
  }
}

/*--------------------------------------------------------------------
 * pointer_module::show_instr_aliases
 *
 */
void pointer_module::show_instr_aliases(instruction *instr)
{
  alias_info a_info;
  if_ops op = instr->opcode();

  if (op == io_str || op == io_memcpy) {
    if (instr->are_annotations()) {
      annote_list_iter iter(instr->annotes());
      while (!iter.is_empty()) {
	annote *ann = iter.step();
	if (ann->name() == k_wilbyr_store_to) {

	  w_points_to *wpt = WILBYR_STORE_TO(ann);
	  a_info.pval = wpt->points_to();
	  a_info.clone_num = wpt->clone_num();
	  a_info.scope= instr->parent()->scope();
	  
	  /* find aliases */
	  current_proc->map((tree_map_f) &find_tn_aliases_with_instr, &a_info);
	}
      }
    }
  }

  if (op == io_lod || op == io_memcpy) {
    if (instr->are_annotations()) {
      annote_list_iter iter(instr->annotes());
      while (!iter.is_empty()) {
	annote *ann = iter.step();
	if (ann->name() == k_wilbyr_load_from) {
	  w_points_to *wpt = WILBYR_LOAD_FROM(ann);
	  a_info.pval = wpt->points_to();
	  a_info.clone_num = wpt->clone_num();
	  a_info.scope= instr->parent()->scope();
	  
	  /* find aliases */
	  current_proc->map((tree_map_f) &find_tn_aliases_with_instr, &a_info);
	}
      }
    }
  }

  /* highlight the aliases */
  for (vnode_list_e *e = a_info.aliases.head(); e; e = e->next()) {
    alias_prop->add_node(e->contents);
  }
}

/*--------------------------------------------------------------------
 * pointer_module::show_tree_instr_aliases
 *
 */
void pointer_module::show_ti_aliases_helper(instruction *instr,
					  pointer_module *m)
{
  m->show_instr_aliases(instr);
}

void pointer_module::show_ti_aliases(tree_instr *ti)
{
  ti->instr_map((instr_map_f) &show_ti_aliases_helper, this);
}

/*--------------------------------------------------------------------
 * pointer_module::show_var_aliases
 *
 */
void pointer_module::show_var_aliases(var_sym *var)
{
  alias_info a_info;
  a_info.var = var;
  
  /* find aliases */
  if (current_proc) {
    current_proc->map((tree_map_f) &find_tn_aliases_with_var, &a_info);
  }

  /* highlight the aliases */
  vnode *var_vnode = create_vnode(var);
  alias_prop->add_node(var_vnode);

  for (vnode_list_e *e = a_info.aliases.head(); e; e = e->next()) {
      alias_prop->add_node(e->contents);
  }
}

/*--------------------------------------------------------------------
 * handle_event
 *
 * when a event occurs, this function is invoked
 */
void pointer_module::handle_event(event &e)
{
  inherited::handle_event(e);

  switch (e.kind()) {
  case SELECTION:
    {
      vnode *vn = e.get_object();
      update_aliases(vn);
    }
    break;
    
  case NEW_FILESET:
    {
      clear();
    }
    break;
    
  default:
    break;
  }
}

/*--------------------------------------------------------------------
 * update_aliases 
 *
 * update aliases, with respect to a newly selected vnode
 */
void pointer_module::update_aliases(vnode *selected_vn)
{
  if (!to_show_aliases) {
    return;
  }

  proc_sym *psym = get_parent_proc(selected_vn);
  if (psym) {
    if (!psym->is_in_memory()) {
      psym->read_proc();
    }
    tree_proc *proc = psym->block();
    if (proc != current_proc) {
      current_proc = proc;
    }
  }

  if (!current_proc) return;

  const char *tag = selected_vn->get_tag();
  if (tag == tag_suif_object) {
    
    suif_object *obj = (suif_object *) selected_vn->get_object();
    
    switch (obj->object_kind()) {
    case TREE_OBJ:
      {
	tree_node *tn = (tree_node *) obj;
	if (tn->is_instr()) {
	    alias_prop->erase();
	  show_ti_aliases((tree_instr *)tn);
	  break;
	}
      }
      break;
      
    case INSTR_OBJ:
      {
	  alias_prop->erase();
	show_instr_aliases((instruction *) obj);
      }
      break;
      
    case SYM_OBJ:
      {
	sym_node *sym = (sym_node *) obj;
	if (sym->is_var()) {
	    alias_prop->erase();
	  show_var_aliases((var_sym *) sym);
	}
      }
      break;
      
    default:
      break;
    }
    
  } else if (tag == tag_code_fragment) {
      alias_prop->erase();
    
    code_fragment *f = (code_fragment *) selected_vn->get_object();
    instruction_list_iter iter(f->instr_list());
    while (!iter.is_empty()) {
      instruction *instr = iter.step();
      show_instr_aliases(instr);
    }
    if (f->node() && f->node()->is_instr()) {
      show_ti_aliases((tree_instr *) f->node());
    }
  }

  alias_prop->update();
  
  /* source viewer */
  window_class *wclass = vman->find_window_class("Source Viewer");
  if (wclass) {
    src_viewer *sviewer = (src_viewer *) vman->find_window_instance(wclass);
    if (sviewer) {
      sviewer->source_tree()->suif_to_code_prop(alias_prop, src_alias_prop);
      src_alias_prop->update();
    }
  }
  
  /* output viewer */
  wclass = vman->find_window_class("Output Viewer");
  if (wclass) {
    output_viewer *oviewer = (output_viewer *) 
      vman->find_window_instance(wclass);
    if (oviewer) {
      oviewer->output_tree()->suif_to_code_prop(alias_prop,
						output_alias_prop);
      output_alias_prop->update();
    }
  }
}
