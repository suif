/*--------------------------------------------------------------------
 * vform.h
 *
 */

#ifndef VFORM_H
#define VFORM_H

#include "vwidget.h"

class vform : public vwidget {

protected:

  class binding *dialog_binding;
  class field_info_list *fields;

  int filter(char *title, char *variable);

public:
 
  vform(vwidget *par);
  ~vform(void);
  void destroy(void);

  void add_field(char *field_name, char *type, char*val);
  void insert_field(int field_num, char *field_name, char *type,
		    char *val);
  void remove_field(int field_num);
  void clear(void);

  char *get_field_data(int field_num);
  const char *get_field_type(int field_num);
  char *get_field_name(int field_num);
  int num_fields(void);

  int get_current_field(void);
  void set_current_field(int field_num);
  void focus_field(int field_num);

  void set_dialog_binding(binding *b) {
    dialog_binding = b;
  }

  virtual int kind(void) { return WIDGET_FORM; }

  /* interface with tcl/tk */
  static int vform_cmd(ClientData, Tcl_Interp *interp, int argc, 
		       char *argv[]);

};

#endif
