/* vpipe */

#include "vpipe.h"
#include "vcommands.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifndef MIN
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#endif

/*-------------------------------------------------------------------
 *
 */ 
vpipe::vpipe(void)
{
  /* create pipe */
  pipefd = tmpfile();
  if (pipefd == NULL) {
    v_error(1, "Cannot open tmp file");
  }

  readpos = writepos = 0;
}

vpipe::~vpipe(void) 
{
  fclose(pipefd);
}

/*-------------------------------------------------------------------
 * read from pipe
 *
 */
int vpipe::read(char *ptr, int size)
{
  writepos = ftell(pipefd);

  if (readpos == writepos) {
    return (0);
  }

  fseek(pipefd, readpos, SEEK_SET);
  
  int s = MIN(size - 1, writepos - readpos);
  int result = fread(ptr, 1, s, pipefd);
  
  readpos += result;
  if (writepos == readpos) {
    readpos = writepos = 0;
  }
  fseek(pipefd, writepos, SEEK_SET);
  
  ptr[result] = 0;		// mark end of string
  
  return (result);
}

/*-------------------------------------------------------------------
 * vpipe::clear
 */
void vpipe::clear(void)
{
  readpos = writepos = 0;
  fseek(pipefd, 0, SEEK_SET);
}

/*-------------------------------------------------------------------
 * size of pipe data
 */
int vpipe::size(void)
{
  int orig_pos = ftell(pipefd);
  fseek(pipefd, 0, SEEK_END);
  int size = ftell(pipefd);
  fseek(pipefd, orig_pos, SEEK_SET);
  return (size);
}
