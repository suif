/*-------------------------------------------------------------------
 * vhtml
 *
 */

#include "vhtml.h"
#include "vtcl.h"
#include "vcommands.h"
#include "binding.h"
#include "vnode.h"
#include "event.h"
#include <tcl.h>
#include <stdio.h>
#include <stdlib.h>

/*-------------------------------------------------------------------
 * vhtml::vhtml
 *
 */
vhtml::vhtml(vwidget *par) : vtty(par)
{
  tcl << "vhtml_create" << wpath << this << tcl_end;
}

/*-------------------------------------------------------------------
 * vhtml::~vhtml
 *
 */
vhtml::~vhtml(void)
{
}

/*-------------------------------------------------------------------
 * vhtml::destroy
 *
 */
void vhtml::destroy(void)
{
  if (state != W_DESTROYED) {
    state = W_DESTROYED;
    tcl << "vhtml_destroy" << wpath << tcl_end;
  }
}

/*-------------------------------------------------------------------
 * vhtml::clear
 */
void vhtml::clear(void)
{
  tcl << "vhtml_clear" << wpath << tcl_end;
  text_pipe->clear();
}

/*-------------------------------------------------------------------
 * vhtml::update
 *
 */
#define BUFFERSIZE 1024

void vhtml::update(void)
{
  int size = text_pipe->size();
  char *b = new char[size + 1];
  char *tmp = b;

  /* link buffer variable to tcl/tk variable */
  char tcl_command[] = "catch {global v_tmp; unset v_tmp}";
  tcl.eval(tcl_command);
  if (tcl.link_var("v_tmp", (char *) &tmp,
		   TCL_LINK_STRING | TCL_LINK_READ_ONLY) != TCL_OK) {
    char mesg[100];
    sprintf(mesg, "Cannot link variable (%s)", tcl.result());
    v_warning(mesg);
    return;
  }

  /*
   * Read from text pipe, 
   * and insert into text window
   */
  int result = text_pipe->read(b, size+1);
  char tcl_com[100];
  sprintf(tcl_com, "global v_tmp; vhtml_insert_text %s $v_tmp",
	  wpath);
  tcl.eval(tcl_com);
  
  /* clean up */
  tcl.unlink_var("v_tmp");
  tcl.eval(tcl_command);
  delete (b);
}

/*-------------------------------------------------------------------
 * vhtml::view
 *
 */
void vhtml::view(int row, int col)
{
  tcl << "vhtml_view" << wpath << row << col << 1 << tcl_end;
}

/*-------------------------------------------------------------------
 * vhtml::tag_begin
 *
 */
void *vhtml::tag_begin(vnode *obj)
{
  fprintf(text_pipe->fd(), "<vnode:%p>", obj);
  return (NULL);
}

/*-------------------------------------------------------------------
 * vhtml::tag_end
 *
 */
void vhtml::tag_end(vnode *)
{
  fprintf(text_pipe->fd(), "</vnode>");
}

/*-------------------------------------------------------------------
 * vhtml::tag_style
 *
 */
void vhtml::tag_style(text_style style)
{
  FILE *fd = text_pipe->fd();
  fputc('\03', fd);
  fputc((style >> 8) & 0xFF, fd);
  fputc(style & 0xFF, fd);
}

/*--------------------------------------------------------------------*/
/* tk command
 *
 */
int vhtml::vhtml_cmd(ClientData, Tcl_Interp *interp, int argc, 
			 char *argv[])
{
  static char *firstargs[] = {"invoke", "destroy", NULL};
  enum { INVOKE = 0, DESTROY, };

  int a = v_parse_firstarg(interp, argc, argv, firstargs);
  if (a < 0) {
    return (TCL_ERROR);
  }

  switch (a) {
  case INVOKE:
    {
      if (argc != 3) {
	v_wrong_argc(interp);
	return (TCL_ERROR);
      }
      vhtml *text;
      sscanf(argv[2], "%p", &text);
      vnode *sel = text->get_selection();
      if (sel) {
	post_event(event(sel, INVOCATION, text));
      }
    }
    break;

  case DESTROY:
    {
      if (argc != 3) {
	v_wrong_argc(interp);
	return (TCL_ERROR);
      }
      vhtml *text;
      sscanf(argv[2], "%p", &text);
      text->destroy();
    }
    break;

  default:
    return (TCL_ERROR);
  }

  return (TCL_OK);
}
