/*--------------------------------------------------------------------
 * vframe.h
 *
 */

#ifndef VFRAME_H
#define VFRAME_H

#include "vwidget.h"
#include "vtcl.h"

class vframe : public vwidget {
  
 public:
  vframe(vwidget *par);
  ~vframe(void);
  void destroy(void);
  virtual int kind(void) { return WIDGET_FRAME; }

  /* interface with tcl/tk */
  static int vframe_cmd(ClientData clientData, Tcl_Interp *interp,
			  int argc, char *argv[]);
};

#endif
