/*
 * a heap class
 */

#ifndef HEAP_H
#define HEAP_H

#include <suif1.h>

template <class T> struct heap_e  {
  T elem;
  int val;
};

template <class T> class heap {

private:
  int max_elems;
  int num_elems;
  int ascending_order;
  heap_e<T> *elems;

  void heapify_up(int e) {
    if (e ==  0) return;
    int p = e >> 1;		// parent
    if (elems[e].val > elems[p].val) {
      heap_e<T> tmp = elems[e]; elems[e] = elems[p]; elems[p] = tmp;
      heapify_up(p);
    }
  }

  void heapify_down(int e) {
    while (1) {
      int s1 = e + e + 1;		// siblings
      if (s1 >= num_elems) return;
      int s2 = s1 + 1;
      
      if (s2 >= num_elems || elems[s1].val >= elems[s2].val) {
	if (elems[s1].val > elems[e].val) {
	  // swap s1 and e
	  heap_e<T> tmp = elems[e]; elems[e] = elems[s1]; elems[s1] = tmp;
	  e = s1;
	} else {
	  break;
	}
      } else {
	if (elems[s2].val > elems[e].val) {
	  // swap s2 and e
	    heap_e<T> tmp = elems[e]; elems[e] = elems[s2]; elems[s2] = tmp;
	  e = s2;
	} else {
	  break;
	}
      }
    }
  }

  void expand(void) {
    int new_max_elems = max_elems * 2;
    heap_e<T> *new_elems = new heap_e<T>[new_max_elems];
    for (int i = 0; i < num_elems; i++) {
      new_elems[i] = elems[i];
    }
    delete elems;
    elems = new_elems;
    max_elems = new_max_elems;
  }

public:
  heap(int n = 32) {
    max_elems = n;
    num_elems = 0;
    elems = new heap_e<T> [n];
    ascending_order = 0;
  }
  ~heap(void) {
    delete elems;
  }
  void reset(void) { num_elems = 0; }

  void set_ascending_order(boolean b = TRUE) { ascending_order = b; }

  /* add an element */
  void add(T b, int val)  {
    if (num_elems >= max_elems) {
      expand();
    }
    elems[num_elems].elem = b;
    elems[num_elems].val = (ascending_order ? -val: val);
    heapify_up(num_elems);
    num_elems++;
  }

  /* remove the largest element from the heap */
  T remove(void) {
    assert(num_elems > 0);
    num_elems--;
    T b = elems[0].elem;
    elems[0] = elems[num_elems];
    heapify_down(0);
    return b;
  }

  /* sort the elements */
  void sort(void) {
    heap_e<T> *ordered_elems = new heap_e<T>[max_elems];
    int n = num_elems;
    for (int i = 0; i < n; i++) {
      ordered_elems[i] = elems[0];
      num_elems--;
      elems[0] = elems[num_elems];
      heapify_down(0);
    }
    delete elems;
    elems = ordered_elems;
    num_elems = n;
  }

  /* get the elements */
  T &operator[](int i) {
    assert(i < num_elems);
    return (elems[i].elem);
  }
  int get_num_elems(void) { return num_elems; }
  boolean is_empty(void) { return (num_elems == 0); }

};

/* heap iteration class */

template <class T> class heap_iter {
  heap<T> *the_heap;
  int next;

 public:
  heap_iter(heap<T> *h) {
    reset(h);
  }

  void reset(heap<T> *h) {
    the_heap = h;
    next = 0;
    the_heap->sort();
  }

  boolean is_empty(void) { return (next >= the_heap->get_num_elems()); }
  T step(void) { return ((*the_heap)[next++]); }
  T peek(void) { return (*the_heap)[next]; }
};

#endif
