/*--------------------------------------------------------------------
 * vhtml.h
 *
 * Note: This html interface is not implemented yet.
 *
 */

#ifndef VHTML_H
#define VHTML_H

#include "vwidget.h"
#include "event.h"
#include "vtcl.h"
#include "vpipe.h"
#include "vtagman.h"
#include "vtty.h"
#include <suif.h>

class vnode;

/*----------------------------------------------------------------------
 * html widget
 */

class vhtml : public vtty {
public:
  vhtml(vwidget *par);
  ~vhtml(void);
  virtual void destroy(void);
  virtual int kind(void) { return WIDGET_HTML; }
  
  /* text I/O */
  virtual void update(void);
  virtual void clear(void);

  /* attributes */
  virtual void tag_style(text_style style);
  virtual void *tag_begin(vnode *obj);
  virtual void tag_end(vnode *obj);

  /* viewing */
  virtual void view(int row, int col);

  /* interface with tcl/tk */
  static int vhtml_cmd(ClientData clientData, Tcl_Interp *interp, int argc, 
		       char *argv[]);

};

#endif
