/*-------------------------------------------------------------------
 * vprop.cc
 *
 */

#include "vprop.h"
#include "vtcl.h"
#include "vnode.h"
#include "event.h"
#include <stdio.h>

/*-------------------------------------------------------------------
 * vprop::vprop
 */
vprop::vprop(char *n, void *data)
{
  client_data = data;
  nodes = new vnode_list;
  nm = lexicon->enter(n)->sp;
  desc = NULL;

  foreground = DEFAULT_FOREGROUND;
  background = DEFAULT_BACKGROUND;

  post_event(event(NULL, PROP_CREATE, NULL, this));
}

/*-------------------------------------------------------------------
 * vprop::~vprop
 */
vprop::~vprop(void)
{
  delete nodes;
  post_event(event(NULL, PROP_DESTROY, NULL, this));
}

/*-------------------------------------------------------------------
 * vprop::add_node
 */
void vprop::add_node(vnode *vn)
{
  nodes->append(vn);
  vn->add_prop(this);
}

/*-------------------------------------------------------------------
 * vprop::remove_node
 */
void vprop::remove_node(vnode *vn)
{
  vnode_list_e *e = nodes->lookup(vn);
  if (e) {
    delete nodes->remove(e);
    vn->remove_prop(this);
  }
}

/*-------------------------------------------------------------------
 * vprop::erase
 */
void vprop::erase(void)
{
  for (vnode_list_e *e = nodes->head(); e; e = e->next()) {
    e->contents->remove_prop(this);
  }
  nodes->erase();
}

/*-------------------------------------------------------------------
 * vprop::update
 */
void vprop::update(void)
{
  post_event(event(NULL, PROP_CHANGE, NULL, this));
}
