#ifndef VTCL_H
#define VTCL_H

#include <suif1.h>
#include "vcommands.h"
#include <tcl.h>
#include <tk.h>
#include <stdlib.h>
#include <string.h>

/* globals */

extern Tcl_Interp *v_interp;	// Visual interpreter

/*
 * tcl command interface class
 *
 */
#define MAX_COMMAND_SIZE 500

class tcl_terminator {
};

extern tcl_terminator *tcl_end;	// end of tcl command, will call tcl to
				// interpret the command
extern char *tcl_null;

class tcl_command {
private:
  char *command;
  char *ptr;
  Tcl_Interp *interp;

public:
  tcl_command(void) {
    command = new char[MAX_COMMAND_SIZE];
    ptr = command;
    interp = NULL;
  }
  ~tcl_command(void) {
    delete (command);
  }

  void set_interp(Tcl_Interp *tcl_interp) { interp = tcl_interp; }
  Tcl_Interp *get_interp(void) { return interp; }

  /*
   * create command
   */
  void create_command(char *cmdName, Tcl_CmdProc *proc,
		      ClientData clientData,
		      Tcl_CmdDeleteProc *deleteProc) {
    Tcl_CreateCommand(interp, cmdName, proc, clientData, deleteProc);
  }

  int link_var(char *varName, char *addr, int type) {
    return (Tcl_LinkVar(interp, varName, addr, type));
  }
  void unlink_var(char *varName) {
    Tcl_UnlinkVar(interp, varName);
  }

  /*
   * Evaluating tcl command
   *
   */

  int operator << (const tcl_terminator *) {
    char *current_command = command;

    /* allocate space for next command */
    ptr = new char[MAX_COMMAND_SIZE];
    command = ptr;

    int result = Tcl_Eval(interp, current_command);
    if (result != TCL_OK) {
      v_warning("Tcl command `%s': (%d) %s",
	      current_command, result, interp->result);
    }
    delete current_command;
    return result;
  }
  tcl_command &operator << (const char *string) {
    sprintf(ptr, "{%s} ", string);
    ptr += strlen(ptr);
    return *this;
  }
  tcl_command &operator << (const void *data) {
    sprintf(ptr, "%p ", data);
    ptr += strlen(ptr);
    return *this;
  }
  tcl_command &operator << (int data) {
    sprintf(ptr, "%d ", data);
    ptr += strlen(ptr);
    return *this;
  }
  tcl_command &operator << (double data) {
    sprintf(ptr, "%f ", data);
    ptr += strlen(ptr);
    return *this;
  }
  int eval(char *string) {
    int result = Tcl_Eval(interp, string);
    if (result != TCL_OK) {
      v_warning("Tcl command `%s': (%d) %s",
		string, result, interp->result);
    }
    return (result);
  }
  int eval_file(char *filename) {
    return (Tcl_EvalFile(interp, filename));
  }

  /*
   * Get results from tcl
   */
  
  int operator >> (int &data) {
    data = atoi(interp->result);
    return TCL_OK;
  }
  int operator >> (char *&data) {
    strcpy(data, interp->result);
    return TCL_OK;
  }
  int operator >> (void *&data) {
    if (sscanf(interp->result, "%p", &data) != 1) {
      return TCL_ERROR;
    } else {
      return TCL_OK;
    }
  }
  char *result(void) {
    return interp->result;
  }
  
  /*
   * Misc
   */
  void print(FILE *fd = stdout) {
    fprintf(fd, "%s\n", command);
  }
};

extern tcl_command tcl;

#endif
