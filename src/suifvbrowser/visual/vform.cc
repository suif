/*-------------------------------------------------------------------
 * vform
 *
 */

#include "vtcl.h"
#include "vform.h"
#include "vcommands.h"
#include "dynatype.h"
#include "vmisc.h"
#include <tcl.h>
#include <stdio.h>
#include <stdlib.h>

struct field_info {
  char *name;
  const char *type;

  field_info() {};
  field_info(char *nm, char *t) {
    name = strdup(nm);
    type = lexicon->enter(t)->sp;
  }
  ~field_info() { 
    delete (name);
  }
  boolean operator==(const field_info &f) {
    return (strcmp(f.name, name) == 0 && f.type == type);
  }
};

DECLARE_LIST_CLASS(field_info_list, field_info *);

/*-------------------------------------------------------------------
 * vform::vform
 *
 */
vform::vform(vwidget *par) : vwidget(par)
{
  strcpy(wpath, par->path());
  dialog_binding = NULL;
  fields = new field_info_list;

  tcl << "vform_create" << wpath << this << tcl_end;
}

/*-------------------------------------------------------------------
 * vform::~vform
 *
 */
vform::~vform(void)
{
  for (field_info_list_e *le = fields->head(); le; le = le->next()) {
    delete le->contents;
  }
  delete (fields);
}

/*-------------------------------------------------------------------
 * vform::destroy
 *
 */
void vform::destroy(void)
{
  if (state != W_DESTROYED) {
    state = W_DESTROYED;
    tcl << "vform_destroy" << wpath << tcl_end;
  }
}

/*--------------------------------------------------------------------
 * Clear the form
 */
void vform::clear(void)
{
  tcl << "vform_clear" << wpath << tcl_end;

  for (field_info_list_e *le = fields->head(); le; le = le->next()) {
    delete le->contents;
  }
  fields->erase();
}

/*--------------------------------------------------------------------
 * Add a field to the form
 */
void vform::add_field(char *field_name, char *type, char *val)
{
  tcl << "vform_add_entry" << wpath << this << field_name << type <<
    val << tcl_end;

  field_info *f = new field_info(field_name, type);
  fields->append(f);
}

/*--------------------------------------------------------------------
 * Insert a field to the form
 */
void vform::insert_field(int field_num, char *field_name, char *type,
		    char *val)
{
  int n = fields->count();
  if (field_num <= n) {

    tcl << "vform_insert_entry" << wpath << this << field_name << type <<
      val << field_num << tcl_end;

    field_info *f = new field_info(field_name, type);
    if (field_num == 0) {
      fields->push(f);
    } else {
      int i = 0;
      for (field_info_list_e *le = fields->head(); le; le = le->next()) {
	if (++i == field_num) {
	  fields->insert_after(f, le);
	  break;
	}
      }
    }
  }
}

/*--------------------------------------------------------------------
 * Remove a field
 */
void vform::remove_field(int field_num)
{
  int n = fields->count();
  if (field_num < n) {
    tcl << "vform_remove_entry" << wpath << this << field_num << tcl_end;

    int i = 0;
    for (field_info_list_e *le = fields->head(); le; le = le->next()) {
      if (i == field_num) {
	delete fields->remove(le);
	break;
      }
    }
  }
}

/*--------------------------------------------------------------------
 * Set current field, focus on the field entry
 *
 */
void vform::set_current_field(int field_num)
{
  tcl << "vform_set_current_field" << wpath << this << field_num << tcl_end;
}

/*--------------------------------------------------------------------
 * Get current field
 *
 */
int vform::get_current_field(void)
{
  tcl << "vform_get_current_field" << wpath << this << tcl_end;
  int field_num;
  tcl >> field_num;
  return field_num;
}

/*--------------------------------------------------------------------
 * Get number of fields
 *
 */
int vform::num_fields(void)
{
  return (fields->count());
}

/*--------------------------------------------------------------------
 * Filter and check a field
 */

int vform::filter(char *type_name, char *variable)
{
  char *val = Tcl_GetVar(v_interp, variable, TCL_GLOBAL_ONLY);
  if (!val) {
    display_message(win(), "Warning: Cannot read tcl/tk variable");
    return (-1);		// error!
  }

  char *error_msg = NULL;
  char *newval = typeman.type_check(type_name, val, error_msg);

  if (!newval) {
    display_message(win(), error_msg ? error_msg : "Unknown type error");
    return (-1);
  }

  char *result = Tcl_SetVar(v_interp, variable, newval, TCL_GLOBAL_ONLY);
  if (!result) {
    v_warning("Cannot set variable.");
  }
  delete (newval);

  return (0);
}

/*--------------------------------------------------------------------
 * Get a field data
 */
char *vform::get_field_data(int field_num)
{
  int result = tcl << "vform_get_data" << wpath << field_num << tcl_end;
  if (result != TCL_OK) {
    return (NULL);
  }

  return tcl.result();
}

/*--------------------------------------------------------------------
 * Get field type
 */
const char *vform::get_field_type(int field_num)
{
  if (field_num >= fields->count()) {
    return NULL;
  }
  return (*fields)[field_num]->type;
}

/*--------------------------------------------------------------------
 * Get field name
 */
char *vform::get_field_name(int field_num)
{
  if (field_num >= fields->count()) {
    return NULL;
  }
  return (*fields)[field_num]->name;
}

/*--------------------------------------------------------------------
 * Set focus on a field
 */
void vform::focus_field(int field_num)
{
  tcl << "vform_set_focus" << wpath << this << field_num << tcl_end;
}

/*--------------------------------------------------------------------*/
/* tk command: vform 
 *
 */
int vform::vform_cmd(ClientData, Tcl_Interp *interp, int argc, 
		     char *argv[])
{
  static char *firstargs[] = {"select", "destroy", "filter", "ok",
			      "cancel", NULL};
  enum { SELECT = 0, DESTROY, FILTER, OK, CANCEL };

  int a = v_parse_firstarg(interp, argc, argv, firstargs);
  if (a < 0) {
    return (TCL_ERROR);
  }

  if (argc < 3) {
    v_wrong_argc(interp);
    return (TCL_ERROR);
  }
  vform *form;
  sscanf(argv[2], "%p", &form);

  switch (a) {
  case SELECT:
    {
    }
    break;

  case DESTROY:
    {
      if (argc != 3) {
	v_wrong_argc(interp);
	return (TCL_ERROR);
      }
      form->destroy();
    }
    break;

  case FILTER:
    {
      if (argc != 5) {
	v_wrong_argc(interp);
	return (TCL_ERROR);
      }
      int result = form->filter(argv[3], argv[4]);
      if (result == -1) {
	return (TCL_ERROR);
      }
    }
    break;

  default:
    return (TCL_ERROR);
  }

  return (TCL_OK);
}
