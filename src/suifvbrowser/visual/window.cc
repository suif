/*-------------------------------------------------------------------
 * window.cc
 * 
 */

#include "window.h"
#include "binding.h"
#include "event.h"
#include "vtoplevel.h"
#include "vman.h"

/*-------------------------------------------------------------------
 * window::window
 *
 */
window::window(void)
{
  bindings = new binding_list;
  toplevel = NULL;
}

/*-------------------------------------------------------------------
 * window::~window
 *
 */
window::~window(void)
{
  delete (toplevel);
  delete_bindings(bindings);
  delete (bindings);
}

/*-------------------------------------------------------------------
 * window::create_window
 *
 */
void window::create_window(void)
{
  post_event(event(NULL, WINDOW_CREATE, NULL, this));
  toplevel = new vtoplevel(class_name(), this);
}

/*-------------------------------------------------------------------
 * window::destroy
 *
 */
void window::destroy(void)
{
  if (toplevel) {
    toplevel->destroy();
  }
}

/*-------------------------------------------------------------------
 * window::destroyed
 *
 */
void window::destroyed(void)
{
  post_event(event(NULL, WINDOW_CLOSE, NULL, this));
  delete (this);
}

