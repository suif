/*--------------------------------------------------------------------
 * vlistbox.h
 *
 */

#ifndef VLISTBOX_H
#define VLISTBOX_H

#include "vwidget.h"

class vnode;
class binding;

struct listbox_entry {
  vnode *object;
  binding *bd;

  listbox_entry(vnode *vn, binding *b) { object = vn; bd = b; }
};

class vlistbox : public vwidget {
  
private:
  
  x_array *entries;

public:
  vlistbox(vwidget *par, char *title);
  ~vlistbox(void);
  void destroy(void);
  virtual int kind(void) { return WIDGET_LISTBOX; }

  void add(binding *b, vnode *object, char *text);
  void clear(void);

  /* interface with tcl/tk */
  static int vlistbox_cmd(ClientData clientData, Tcl_Interp *interp, int argc,
			 char *argv[]);

};

#endif
