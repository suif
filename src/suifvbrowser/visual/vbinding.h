/*--------------------------------------------------------------------
 * vbinding.h
 *
 */

#ifndef VBINDING_H
#define VBINDING_H

#include <suif.h>
#include "event.h"

class vnode;

typedef void (*bfun)(event &e, const void *client_data);
typedef void (*bfun2)(event &e, void *client_data1, const void *client_data2);

class vbinding {
 private:
  char *tcl_command;
  bfun function;

  int num_client_data;
  void *client_data1;
  void *client_data2;

  boolean attached;

 public:
  vbinding(void);
  ~vbinding(void);

  void set_tcl_command(char *s, void *data = NULL) {
    tcl_command = s;
    client_data1 = data;
    num_client_data = 1;
  }
  void set_tcl_command(char *s, void *data1, void *data2) {
    tcl_command = s;
    client_data1 = data1;
    client_data2 = data2;
    num_client_data = 2;
  }
  void set_function(bfun f, void *data = NULL) { 
    function = f;
    client_data1 = data;
    num_client_data = 1;
  };
  void set_function(bfun2 f, void *data1, void *data2) { 
    function = (bfun) f;
    client_data1 = data1;
    client_data2 = data2;
    num_client_data = 2;
  };

  /* invoke */
  void invoke(event &e);

  /* attach to global events */
  void attach(event_kind e);
  void dettach(event_kind e);

};

DECLARE_LIST_CLASS(vbinding_list, vbinding *);

void delete_bindings(vbinding_list *bindings);

#endif
