/*-------------------------------------------------------------------
 * vman.cc
 *
 */

#include "vman.h"
#include "vmisc.h"
#include "binding.h"
#include "event.h"
#include <stdio.h>
#include <stdlib.h>

visual_manager *vman;

#define MAX_HISTORY_LENGTH 50

/*-------------------------------------------------------------------*/
inline int hashfn(const void *ptr) {
  int hash = (((int)ptr) >> 2) ^ (((int)ptr) >> 10);
  return (hash & VN_HASH_MASK);
}

/*-------------------------------------------------------------------
 * Init and clean up functions 
 */
void init_vman(void)
{
  vman = new visual_manager;
  vman->init();
}

void exit_vman(void)
{
  delete vman;
}

/*-------------------------------------------------------------------
 * visual_manager
 */
visual_manager::visual_manager(void)
{
  toplevel_num = 0;		// window number
  wclasses = new window_class_list;
  current_windows = new window_instance_list;
  event_binding = NULL;
  props = new vprop_list;
  modules = new module_list;
  vn_history = new vnode_list;
}

/*-------------------------------------------------------------------
 * visual_manager
 */
visual_manager::~visual_manager(void)
{
  if (event_binding) {
    remove_event_binding(event_binding);
    delete (event_binding);
  }

  while (!wclasses->is_empty()) {
    delete wclasses->pop();
  }
  delete wclasses;

  while (!current_windows->is_empty()) {
    delete current_windows->pop();
  }
  delete (current_windows);
  delete (props);
  delete (modules);
}

/*-------------------------------------------------------------------
 * visual_manager::init
 */
void visual_manager::init(void)
{
  if (!event_binding) {
    event_binding = new binding((bfun) &event_handler, this);
    add_event_binding(event_binding, ALL_EVENTS);
  }
}

/*-------------------------------------------------------------------
 * visual_manager::find_vnode
 */
vnode *visual_manager::find_vnode(const void *obj)
{
  vnode_list *vn_list = &hashed_list[hashfn(obj)];
  for (vnode_list_e *e = vn_list->head(); e; e = e->next()) {
    if (e->contents->get_object() == obj) {
      return (e->contents);
    }
  }
  return (NULL);
}

/*-------------------------------------------------------------------
 * visual_manager::add_vnode
 */
void visual_manager::add_vnode(vnode *vn)
{
  vnode_list *vn_list = &hashed_list[hashfn(vn->get_object())];
  vn_list->push(vn);
}

/*-------------------------------------------------------------------
 * remove_vnode
 */
void visual_manager::remove_vnode(vnode *vn)
{
  vnode_list *vn_list = &hashed_list[hashfn(vn->get_object())];
  vnode_list_e *e = vn_list->lookup(vn);
  if (e) {
    delete vn_list->remove(e);
  }
}

/*-------------------------------------------------------------------
 * visual_manager::remove_all_vnodes
 */
void visual_manager::remove_all_vnodes(void)
{
  vn_history->erase();

  for (int i = 0; i < VN_HASH_SIZE; i++) {
    vnode_list *vlist = &hashed_list[i];
    while (!vlist->is_empty()) {
      delete vlist->head()->contents;
      // side effect: vnode destructor will post VNODE_DESTROY event,
      // and the corresponding vn_list entry will be removed.
    }
  }
}

/*-------------------------------------------------------------------
 * visual_manager::get_selection
 */
vnode *visual_manager::get_selection(void) const
{
  if (vn_history->is_empty()) {
    return (NULL);
  } else {
    return (vn_history->head()->contents);
  }
}
/*-------------------------------------------------------------------
 * visual_manager::new_toplevel_path
 *
 * assign a path to a new top level window
 */
void visual_manager::new_toplevel_path(char *path)
{
  sprintf(path, ".vwin%d", toplevel_num);
  toplevel_num++;
  assert (!widget_exists(path));
}

/*-------------------------------------------------------------------
 * visual_manager::register_window_class
 *
 */
void visual_manager::register_window_class(char *class_name, window_cons_fn fn)
{
  wclasses->append(new window_class(class_name, fn));
}

/*-------------------------------------------------------------------
 * visual_manager::find_window_class
 *
 */
window_class *visual_manager::find_window_class(char *name)
{
  window_class_list_iter iter(wclasses);
  while (!iter.is_empty()) {
    window_class *wclass = iter.step();
    if (strcmp(wclass->name(), name) == 0) {
      return (wclass);
    }
  }
  return (NULL);
}

/*----------------------------------------------------------------------
 * get_current_windows
 */
void visual_manager::get_current_windows(window_list &win_list)
{
  win_list.erase();
  window_instance_list_iter iter(current_windows);
  while (!iter.is_empty()) {
    win_list.append(iter.step()->instance);
  }
}

/*----------------------------------------------------------------------
 * show_window
 */
window *visual_manager::show_window(window_class *wclass)
{
  window *instance = find_window_instance(wclass);
 
  if (instance) {
    instance->raise();
  } else {      
    /* create new instance */
    instance = create_window_instance(wclass);
    instance->create_window();
  }
  return (instance);
}

window *visual_manager::show_window(char *class_name)
{
  window_class *wclass = find_window_class(class_name);
  if (wclass) {
    return show_window(wclass);
  } else {
    return NULL;
  }
}

/*----------------------------------------------------------------------
 * craete_window_instance
 */
window *visual_manager::create_window_instance(window_class *wclass)
{
  window *win = wclass->constructor()();
  return (win);
}

/*----------------------------------------------------------------------
 * find_window_instance
 */
window *visual_manager::find_window_instance(window_class *wclass)
{
  window_instance_list_iter iter(current_windows);
  while (!iter.is_empty()) {
    window_instance *win = iter.step();
    if (win->wclass == wclass) {
      return (win->instance);
    }
  }
  return (NULL);
}

boolean visual_manager::find_window_instance(window *win)
{
  window_instance_list_iter iter(current_windows);
  while (!iter.is_empty()) {
    window_instance *inst = iter.step();
    if (inst->instance == win) {
      return (TRUE);
    }
  }
  return (FALSE);
}

/*----------------------------------------------------------------------
 * new_window_instance
 */
void visual_manager::new_window_instance(window *win, char *class_name)
{
  window_class *wclass = find_window_class(class_name);

  window_instance *inst = new window_instance;
  inst->wclass = wclass;
  inst->instance = win;
  current_windows->append(inst);
}

/*----------------------------------------------------------------------
 * remove_window_instance
 *
 */
void visual_manager::remove_window_instance(window *the_win)
{
  window_instance_list_iter iter(current_windows);
  while (!iter.is_empty()) {
    window_instance *win = iter.step();
    if (win->instance == the_win) {
      delete (win);
      delete current_windows->remove(iter.cur_elem());
      break;
    }
  }
}

/*----------------------------------------------------------------------
 * visual_manager::register_module
 *
 */
void visual_manager::register_module(char * /* name */, /* unused */
				     module * (*constructor_fn)(void))
{
  module *m = constructor_fn();
  add_module(m);
}

/*----------------------------------------------------------------------
 * visual_manager::add_module
 *
 */
void visual_manager::add_module(module *m)
{
  modules->append(m);
}

/*----------------------------------------------------------------------
 * visual_manager::go_back
 *
 */
void visual_manager::go_back(void)
{
  vnode_list_e *le = vn_history->head();
  if (le && le->next()) {	// make sure there are two objects
    vn_history->pop();
    vnode *vn = vn_history->pop();

    /* post selection event */
    post_event(event(vn, SELECTION, NULL));
  }
}

/*----------------------------------------------------------------------
 * event handler
 *
 */
void visual_manager::event_handler(event &e, visual_manager *visual_man)
{
  switch (e.kind()) {
  case VNODE_CREATE:
    {

#if 0
      assert_msg(visual_man->find_vnode(e.get_object()) == NULL,
		 ("Internal error. Vnode duplicated."));
#endif

      visual_man->add_vnode(e.get_object());
    }
    break;

  case VNODE_DESTROY:
    {
      vnode *vn = e.get_object();

      // remove vnode from list
      visual_man->remove_vnode(e.get_object());

      // remove vnode from the history list
      vnode_list_e *top_le = visual_man->vn_history->head();
      vnode_list_e *le = visual_man->vn_history->head(), *next_le;
      for (;le; le = next_le) {
	next_le = le->next();
	if (le->contents == vn) {
	  delete visual_man->vn_history->remove(le);
	}
      }
    }
    break;

  case WINDOW_CREATE:
    {
      window *win = (window *) e.get_param();
      visual_man->new_window_instance(win, win->class_name());
    }
    break;

  case WINDOW_CLOSE:
    {
      window *win = (window *) e.get_param();
      visual_man->remove_window_instance(win);
    }
    break;

  case SELECTION:
    {
      vnode *sel = e.get_object();
      int n = visual_man->vn_history->count();
      if (n == 0 ||
	  visual_man->vn_history->head()->contents != sel) {

	// add to history list
	visual_man->vn_history->push(sel);
	
	if (n > MAX_HISTORY_LENGTH) {
	  delete visual_man->vn_history->remove(visual_man->vn_history->
						tail());
	}
      }
    }
    break;

  case PROP_CREATE:
    {
      vprop *p = (vprop *) e.get_param();
      if (!visual_man->props->lookup(p)) {
	visual_man->props->append(p);
      }
    }
    break;

  case PROP_DESTROY:
    {
      vprop *p = (vprop *) e.get_param();
      vprop_list_e *pe = visual_man->props->lookup(p);
      if (pe) delete visual_man->props->remove(pe);
    }
    break;

  default:
    break;
  }
}

