/*-------------------------------------------------------------------
 * vnode
 *
 */

#include "vnode.h"
#include <stdio.h>
#include <stdlib.h>
#include "vman.h"
#include "event.h"

/*-------------------------------------------------------------------
 * vnode::vnode
 */
vnode::vnode(void)
{
  object = NULL;
  tag = lexicon->enter("")->sp;
  props = NULL;
  aux_data = NULL;

  post_event(event(this, VNODE_CREATE));
}

vnode::vnode(void *const obj, char *const obj_tag)
{
  object = obj;
  assert(obj_tag && obj);	// this must not be NULL
  tag = lexicon->enter(obj_tag)->sp;
  props = NULL;
  aux_data = NULL;

  post_event(event(this, VNODE_CREATE));
}

vnode::vnode(void *const obj, char *const obj_tag, void *data)
{
  object = obj;
  assert(obj_tag && obj);	// this must not be NULL
  tag = lexicon->enter(obj_tag)->sp;
  props = NULL;
  aux_data = data;

  post_event(event(this, VNODE_CREATE));
}

/*-------------------------------------------------------------------
 * vnode::~vnode
 */
vnode::~vnode(void)
{
  if (props) {
    delete (props);
  }
  post_event(event(this, VNODE_DESTROY));
}

/*-------------------------------------------------------------------
 * vnode::add_prop
 */
void vnode::add_prop(vprop *p)
{
  if (!props) {
    props = new vprop_list;
  }
  if (!props->lookup(p)) {
    props->push(p);
  }
}

/*-------------------------------------------------------------------
 * vnode::get_prop
 */
vprop *vnode::get_prop(char *name)
{
  if (!props) return NULL;

  for (vprop_list_e *e = props->head(); e; e = e->next()) {
    vprop *p = e->contents;
    if (p->name() == name) {
      return (p);
    }
  }
  return (NULL);
}

/*-------------------------------------------------------------------
 * vnode::remove_prop
 */
void vnode::remove_prop(char *name)
{
  if (!props) return;

  for (vprop_list_e *cur = props->head(); cur; cur = cur->next()) {
    if (cur->contents->name() == name) {
      delete props->remove(cur);
      break;
    }
  }
}

/*-------------------------------------------------------------------
 * vnode::remove_prop
 */
void vnode::remove_prop(vprop *p)
{
  if (!props) return;

  vprop_list_e *e = props->lookup(p);
  if (e) delete props->remove(e);
}
