/* file "main.cc" of the recinfo program for SUIF */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for the recinfo program for
 *  SUIF.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:53 brm Exp $")

INCLUDE_SUIF_COPYRIGHT


DECLARE_DLIST_CLASS(proc_sym_list, proc_sym *);

static const char *k_recinfo_seen;


static void usage(void);
static void do_proc(proc_sym *the_proc);
static void check_object(suif_object *the_object, so_walker *the_walker);


extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    ANNOTE(k_recinfo_seen, "recinfo seen", FALSE);

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = fse->next_proc();
            if (this_proc == NULL)
                break;
            if (!this_proc->is_written())
              {
                do_proc(this_proc);
              }
          }
      }

    exit_suif();
    return 0;
  }


static void usage(void)
  {
    fprintf(stderr,
            "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void do_proc(proc_sym *the_proc)
  {
    the_proc->read_proc(TRUE, FALSE);
    if ((the_proc->peek_annote(k_no_recursion) != NULL) ||
        (the_proc->peek_annote(k_pure_function) != NULL) ||
        (the_proc->peek_annote(k_recinfo_seen) != NULL))
      {
        the_proc->write_proc(the_proc->file());
        the_proc->flush_proc();
        return;
      }
    the_proc->append_annote(k_recinfo_seen);
    proc_sym_list questionable_calls;
    so_walker the_walker;
    the_walker.set_data(0, &questionable_calls);
    boolean has_bad_calls =
            the_walker.walk(the_proc->block(), &check_object, (int)FALSE).si;
    the_proc->write_proc(the_proc->file());
    the_proc->flush_proc();
    if (has_bad_calls)
        return;
    while (!questionable_calls.is_empty())
      {
        proc_sym *callee = questionable_calls.pop();
        if (!callee->is_written())
            do_proc(callee);
        if (the_proc->peek_annote(k_no_recursion) == NULL)
            return;
      }
    the_proc->append_annote(k_no_recursion);
  }

static void check_object(suif_object *the_object, so_walker *the_walker)
  {
    if (the_walker->in_annotation())
        return;
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_cal)
        return;
    in_cal *the_call = (in_cal *)the_instr;
    proc_sym *the_proc = proc_for_call(the_call);
    if (the_proc != NULL)
      {
        if ((the_proc->peek_annote(k_no_recursion) != NULL) ||
            (the_proc->peek_annote(k_pure_function) != NULL))
          {
            return;
          }
        if ((the_proc->peek_annote(k_recinfo_seen) == NULL) &&
            the_proc->is_readable())
          {
            proc_sym_list *questionable_calls =
                    (proc_sym_list *)(the_walker->get_data(0).ptr);
            questionable_calls->append(the_proc);
            return;
          }
      }
    the_walker->set_result((int)TRUE);
    the_walker->set_break();
  }
