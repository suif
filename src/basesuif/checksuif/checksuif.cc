/*  SUIF Checking Program */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#include <suif1.h>
#include <check.h>
#include <string.h>


static boolean verbose = FALSE;


int
main(int argc, char *argv[])
{
    static boolean flag_warn = FALSE;
    static boolean flag_fail = FALSE;
    static cmd_line_option option_table[] =
      {
        {CLO_NOARG, "-warn",    NULL, &flag_warn},
        {CLO_NOARG, "-fail",    NULL, &flag_fail},
        {CLO_NOARG, "-v",       NULL, &verbose},
        {CLO_NOARG, "-V",       NULL, &verbose},
        {CLO_NOARG, "-verbose", NULL, &verbose}
      };

    start_suif(argc, argv);

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    if (flag_fail)
        checkfail = TRUE;
    else if (flag_warn)
        checkfail = FALSE;

    if (argc == 1)
        error_line(-1, NULL, "No files given");

    for (int arg_num = 1; arg_num < argc; arg_num++) {
        if (verbose) {
            fprintf(stderr,
                    "    reading file and global symbol table information for "
                    "file \"%s\"\n", argv[arg_num]);
            fflush(stderr);
        }
	fileset->add_file(argv[arg_num], NULL);
    }

    fileset->reset_iter();
    file_set_entry *fse;

    while (fse = fileset->next_file()) {
        fse->reset_proc_iter();

        proc_sym *ps;
        while (ps = fse->next_proc()) {
            if (verbose) {
                fprintf(stderr, "    procedure `%s': reading\n", ps->name());
                fflush(stderr);
            }

            assert(!ps->is_in_memory());
	    /* do not automatically convert to expression tree format;
	       the conversion would automatically try to fix up invalid
	       expression trees */
	    ps->read_proc(FALSE);

            if (verbose) {
                fprintf(stderr, "        checking\n");
                fflush(stderr);
            }

	    /* check types */
            tree_block *the_block = ps->block();
            if (the_block == NULL)
              {
                error_line(1, NULL, "procedure %s() has no body", ps->name());
              }
            the_block->map(check_type, NULL);
            check_proc(ps->block());

            if (verbose) {
                fprintf(stderr, "        deallocating\n");
                fflush(stderr);
            }

            ps->flush_proc();
	}

        if (verbose) {
            fprintf(stderr, "    checking file symbol table for file \"%s\"\n",
                    fse->name());
            fflush(stderr);
        }
        check_file_symtab(fse->symtab());
    }

    if (verbose) {
        fprintf(stderr, "    checking global symbol table\n");
        fflush(stderr);
    }
    check_global_symtab(fileset->globals());

    if (verbose) {
        fprintf(stderr, "    cleaning up global datastructures\n");
        fflush(stderr);
    }
    exit_suif();

    return 0;
}
