/* file "main.cc" of the private_prop program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for private_prop.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:51 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        The private_prop program propagates information about which
        globals are privatized onto all the procedures in which the
        private copies might be used.  It reads the ``privatized'' and
        ``reduced'' annotes on tree_for nodes to find privatized
        globals and writes out its results as lists of symbols in
        ``possible global privatizable'' annotations on the proc_syms.

        This pass assumes that if a callsite has either of the
        annotations ``call mod syms'' or ``call ref syms'', then there
        is at most one of each of these annotations at the callsite
        and that if a global variable is read or written by the callee
        or anything it calls at any level, that variable is listed in
        at least one of these two annotations.  If a callsite has
        neither of these annotations, this pass assumes that any
        global might be read or written on the other side of the
        callsite.

        This pass also assumes that if there are any indirect calls
        (calls through pointers) or calls to external procedures (any
        procedures that are not defined in the files given to this
        pass), no privatized globals are used on the other side of the
        call.  It's up to the pass that finds parallelization and
        marks privatizable variables to insure this is the case.

        Since this pass sometimes needs to move variables from file
        scope to inter-file scope, it needs to use the rescope pass.
        So this pass needs to be run with the ``-pre-pass'' option,
        then rescope can be run, then this pass can be run again to do
        the actual work.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_privatized;
static const char *k_reduced;
static const char *k_call_ref_syms;
static const char *k_call_mod_syms;
static const char *k_possible_global_privatizable;

static proc_sym *current_proc;
static boolean is_prepass = FALSE;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void start_prop_on_proc(tree_proc *the_proc);
static void start_prop_on_node(tree_node *the_node,
                               var_sym_list *private_globals);
static void start_prop_on_list(tree_node_list *node_list,
                               var_sym_list *private_globals);
static void start_prop_on_for(tree_for *the_for,
                              var_sym_list *private_globals);
static void prop_on_instr(instruction *the_instr,
                          var_sym_list *private_globals);
static void prop_on_op(operand the_op, var_sym_list *private_globals);
static void prop_into_proc(proc_sym *the_proc, var_sym_list *private_globals);
static void prop_on_callsite_node(tree_node *the_node, void *data);
static void make_interfile(var_sym *the_var);
static void make_interfile(type_node *the_type);
static void make_interfile_annotes(suif_object *the_object);
static void cleanup_global_symtab(global_symtab *the_symtab);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    static cmd_line_option option_table[] =
      {
        {CLO_NOARG, "-pre-pass", NULL, &is_prepass}
      };

    start_suif(argc, argv);

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    ANNOTE(k_privatized,    "privatized",    TRUE);
    ANNOTE(k_reduced,       "reduced",       TRUE);
    ANNOTE(k_call_ref_syms, "call ref syms", TRUE);
    ANNOTE(k_call_mod_syms, "call mod syms", TRUE);
    ANNOTE(k_possible_global_privatizable, "possible global privatizable",
           TRUE);

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            current_proc = this_proc_sym;
            start_prop_on_proc(this_proc_sym->block());
            this_proc_sym->flush_proc();
          }
      }

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
        cleanup_global_symtab(fse->symtab());
      }

    cleanup_global_symtab(fileset->globals());

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr, "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void start_prop_on_proc(tree_proc *the_proc)
  {
    var_sym_list private_globals;
    start_prop_on_node(the_proc, &private_globals);
    assert(private_globals.is_empty());
  }

static void start_prop_on_node(tree_node *the_node,
                               var_sym_list *private_globals)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            prop_on_instr(the_tree_instr->instr(), private_globals);
            break;
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            start_prop_on_list(the_loop->body(), private_globals);
            start_prop_on_list(the_loop->test(), private_globals);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            start_prop_on_for(the_for, private_globals);
            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;
            start_prop_on_list(the_if->header(), private_globals);
            start_prop_on_list(the_if->then_part(), private_globals);
            start_prop_on_list(the_if->else_part(), private_globals);
            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            start_prop_on_list(the_block->body(), private_globals);
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void start_prop_on_list(tree_node_list *node_list,
                               var_sym_list *private_globals)
  {
    tree_node_list_iter node_iter(node_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        start_prop_on_node(this_node, private_globals);
      }
  }

static void start_prop_on_for(tree_for *the_for,
                              var_sym_list *private_globals)
  {
    start_prop_on_list(the_for->landing_pad(), private_globals);
    prop_on_op(the_for->lb_op(), private_globals);
    prop_on_op(the_for->ub_op(), private_globals);
    prop_on_op(the_for->step_op(), private_globals);

    unsigned num_privates_added = 0;
    annote_list_iter annote_iter(the_for->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();
        if (this_annote->name() == k_privatized)
          {
            immed_list_iter immed_iter(this_annote->immeds());
            while (!immed_iter.is_empty())
              {
                immed this_immed = immed_iter.step();
                if (!this_immed.is_symbol())
                  {
                    error_line(1, the_for, "non-symbol in \"%s\" annote",
                               k_privatized);
                  }
                sym_node *this_sym = this_immed.symbol();
                if (!this_sym->is_var())
                  {
                    error_line(1, the_for,
                               "non-variable symbol in \"%s\" annote",
                               k_privatized);
                  }
                var_sym *this_var = (var_sym *)this_sym;
                if (this_var->is_global())
                  {
                    private_globals->push(this_var);
                    ++num_privates_added;
                  }
              }
          }
        else if (this_annote->name() == k_reduced)
          {
            if (this_annote->immeds()->count() < 2)
              {
                error_line(1, the_for,
                           "fewer than two arguments in in \"%s\" annote",
                           k_reduced);
              }
            immed reduced_immed = (*(this_annote->immeds()))[1];
            if (!reduced_immed.is_symbol())
              {
                error_line(1, the_for,
                           "non-symbol as reduction variable in \"%s\" annote",
                           k_reduced);
              }
            sym_node *reduced_sym = reduced_immed.symbol();
            if (!reduced_sym->is_var())
              {
                error_line(1, the_for,
                           "non-variable symbol as reduction variable "
                           "in \"%s\" annote", k_reduced);
              }
            var_sym *reduced_var = (var_sym *)reduced_sym;
            if (reduced_var->is_global())
              {
                private_globals->push(reduced_var);
                ++num_privates_added;
              }
          }
      }

    start_prop_on_list(the_for->body(), private_globals);

    while (num_privates_added > 0)
      {
        (void)(private_globals->pop());
        --num_privates_added;
      }
  }

static void prop_on_instr(instruction *the_instr,
                          var_sym_list *private_globals)
  {
    if (private_globals->is_empty())
        return;

    if (the_instr->opcode() == io_cal)
      {
        in_cal *the_call = (in_cal *)the_instr;
        proc_sym *the_proc = proc_for_call(the_call);
        if (the_proc != NULL)
          {
            annote *refs_annote =
                    the_call->annotes()->peek_annote(k_call_ref_syms);
            annote *mods_annote =
                    the_call->annotes()->peek_annote(k_call_mod_syms);
            if ((refs_annote != NULL) || (mods_annote != NULL))
              {
                var_sym_list privates_through_call;
                var_sym_list_iter privates_iter(private_globals);
                while (!privates_iter.is_empty())
                  {
                    var_sym *this_private = privates_iter.step();
                    if (((refs_annote != NULL) &&
                         (refs_annote->immeds()->lookup(immed(this_private))
                          != NULL)) ||
                        ((mods_annote != NULL) &&
                         (mods_annote->immeds()->lookup(immed(this_private))
                          != NULL)))
                      {
                        privates_through_call.append(this_private);
                      }
                  }
                prop_into_proc(the_proc, &privates_through_call);
              }
            else
              {
                prop_into_proc(the_proc, private_globals);
              }
          }
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
        prop_on_op(the_instr->src_op(src_num), private_globals);
  }

static void prop_on_op(operand the_op, var_sym_list *private_globals)
  {
    if (the_op.is_expr())
        prop_on_instr(the_op.instr(), private_globals);
  }

static void prop_into_proc(proc_sym *the_proc, var_sym_list *private_globals)
  {
    if (!the_proc->is_readable())
        return;

    annote *possible_privatizable_annote =
            the_proc->annotes()->peek_annote(k_possible_global_privatizable);
    if (possible_privatizable_annote == NULL)
      {
        possible_privatizable_annote =
                new annote(k_possible_global_privatizable, new immed_list);
        the_proc->annotes()->append(possible_privatizable_annote);
      }

    immed_list *annote_immeds = possible_privatizable_annote->immeds();

    var_sym_list further_prop_globals;

    var_sym_list_iter old_iter(private_globals);
    while (!old_iter.is_empty())
      {
        var_sym *the_private = old_iter.step();
        if (annote_immeds->lookup(immed(the_private)) == NULL)
          {
            if (the_private->parent()->is_file() &&
                (the_proc->parent() != the_private->parent()))
              {
                make_interfile(the_private);
              }
            annote_immeds->append(immed(the_private));
            further_prop_globals.append(the_private);
          }
      }

    if (further_prop_globals.is_empty())
        return;

    if (!the_proc->is_in_memory())
        the_proc->read_proc(TRUE, FALSE);

    the_proc->block()->map(&prop_on_callsite_node, private_globals);

    if (the_proc != current_proc)
        the_proc->flush_proc();
  }

static void prop_on_callsite_node(tree_node *the_node, void *data)
  {
    var_sym_list *private_globals = (var_sym_list *)data;
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        prop_on_instr(the_tree_instr->instr(), private_globals);
      }
  }

static void make_interfile(var_sym *the_var)
  {
    if (!is_prepass)
      {
        error_line(1, the_var,
                   "variable needs to be moved into file scope; run this pass"
                   " with the ``-pre-pass'' option, then run the ``rescope''"
                   " pass, then run this pass again without the ``-pre-pass''"
                   " option");
      }

    if (!the_var->parent()->is_file())
        return;

    if (the_var->annotes()->peek_annote(k_globalize) != NULL)
        return;

    the_var->append_annote(k_globalize);

    if (the_var->parent_var() != NULL)
        make_interfile(the_var->parent_var());

    unsigned num_children = the_var->num_children();
    for (unsigned child_num = 0; child_num < num_children; ++child_num)
        make_interfile(the_var->child_var(child_num));

    make_interfile(the_var->type());

    make_interfile_annotes(the_var);
  }

static void make_interfile(type_node *the_type)
  {
    if (!the_type->parent()->is_file())
        return;

    if (the_type->annotes()->peek_annote(k_globalize) != NULL)
        return;

    the_type->append_annote(k_globalize);

    unsigned num_ref_types = the_type->num_ref_types();
    for (unsigned ref_num = 0; ref_num < num_ref_types; ++ref_num)
        make_interfile(the_type->ref_type(ref_num));

    if (the_type->op() == TYPE_ARRAY)
      {
        array_type *this_array_type = (array_type *)the_type;
        array_bound the_bound = this_array_type->lower_bound();
        if (the_bound.is_variable())
            make_interfile(the_bound.variable());
        the_bound = this_array_type->upper_bound();
        if (the_bound.is_variable())
            make_interfile(the_bound.variable());
      }

    make_interfile_annotes(the_type);
  }

static void make_interfile_annotes(suif_object *the_object)
  {
    /* Throw away any annotations that might be pointing to anything
     * not visible in the inter-file scope. */
    annote_list_e *list_e = the_object->annotes()->head();
    while (list_e != NULL)
      {
        annote_list_e *next_e = list_e->next();
        annote *this_annote = list_e->contents;
        immed_list_iter immed_iter(this_annote->immeds());
        while (!immed_iter.is_empty())
          {
            immed this_immed = immed_iter.step();
            boolean throw_away = FALSE;
            if (this_immed.is_symbol())
              {
                if (this_immed.symbol()->parent() != fileset->globals())
                    throw_away = TRUE;
              }
            else if (this_immed.is_type())
              {
                if (this_immed.type()->parent() != fileset->globals())
                    throw_away = TRUE;
              }
            else if (this_immed.is_op())
              {
                throw_away = TRUE;
              }
            else if (this_immed.is_instr())
              {
                throw_away = TRUE;
              }

            if (throw_away)
              {
                the_object->annotes()->remove(list_e);
                delete list_e;
                delete this_annote;
                break;
              }
          }
        list_e = next_e;
      }
  }

static void cleanup_global_symtab(global_symtab *the_symtab)
  {
    if (!is_prepass)
        return;

    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->annotes()->peek_annote(k_possible_global_privatizable) !=
            NULL)
          {
            annote *this_annote =
                    this_sym->annotes()->get_annote(
                            k_possible_global_privatizable);
            delete this_annote;
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
