/* file "argh.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 * This program is a front-end for passes taking multiple arguments.
 */

#define RCS_BASE_FILE argh_cc

#include <suif1.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

RCS_BASE(
    "$Id: argh.cc,v 1.1.1.1 1998/06/16 15:17:01 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage_error(void);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

int main(int argc, char *argv[])
  {
    int arg_num;

    start_suif(argc, argv);

    if (argc < 3)
        usage_error();

    if (argc == 3)
      {
        fprintf(stdout, "no input file specifications given\n");
        exit(1);
      }

    char *suffix = argv[2];
    if ((suffix[0] != '-') || suffix[1] != '.')
        usage_error();
    suffix += 2;

    char **new_args = new char *[(argc - 3) * 2 + 2];
    new_args[0] = argv[1];
    int new_arg_num = 1;
    boolean flags_finished = FALSE;
    for (arg_num = 0; arg_num < argc - 3; ++arg_num)
      {
        char *input_name = argv[3 + arg_num];
        new_args[new_arg_num] = input_name;
        ++new_arg_num;
        if ((!flags_finished) && input_name[0] == '-')
          {
            if ((input_name[1] == '-') && (input_name[2] == 0))
                flags_finished = TRUE;
          }
        else
          {
            char *output_name =
                    new char[strlen(input_name) + strlen(suffix) + 2];
            strcpy(output_name, input_name);
            char *place = strrchr(output_name, '.');
            if (place == NULL)
              {
                strcat(output_name, ".");
                place = strrchr(output_name, '.');
              }
            ++place;
            strcpy(place, suffix);
            new_args[new_arg_num] = output_name;
            ++new_arg_num;
          }
      }
    new_args[new_arg_num] = NULL;

    execvp(argv[1], new_args);

    for (arg_num = 0; arg_num < argc - 3; ++arg_num)
        delete new_args[arg_num * 2 + 2];
    delete new_args;
    exit_suif();
    exit(0);
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage_error(void)
  {
    fprintf(stderr, "usage: %s <progname> -.<out-suffix> "
            "<flags> <input-filenames>\n", _suif_program_name);
    exit(1);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
