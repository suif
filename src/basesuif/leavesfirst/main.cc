/* file "main.cc" of the leavesfirst program for SUIF */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for the leavesfirst program
 *  for SUIF.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <limits.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:38 brm Exp $")

INCLUDE_SUIF_COPYRIGHT


DECLARE_DLIST_CLASS(proc_sym_list, proc_sym *);


static const char *k_leavesfirst_callees;
static const char *k_leavesfirst_placed;


static void usage(void);
static void find_callees_on_proc(proc_sym *the_proc);
static void find_callees_on_object(suif_object *the_object,
                                   so_walker *the_walker);
static void place_proc(proc_sym *the_proc);


extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    ANNOTE(k_leavesfirst_callees, "leavesfirst callees", FALSE);
    ANNOTE(k_leavesfirst_placed,  "leavesfirst placed",  FALSE);

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = fse->next_proc();
            if (this_proc == NULL)
                break;
            this_proc->read_proc(TRUE, FALSE);
            find_callees_on_proc(this_proc);
            this_proc->write_proc(fse);
            this_proc->flush_proc();
          }
      }

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        proc_sym_list proc_list;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = fse->next_proc();
            if (this_proc == NULL)
                break;
            proc_list.append(this_proc);
          }
        while (!proc_list.is_empty())
          {
            proc_sym *this_proc = proc_list.pop();
            place_proc(this_proc);
          }
      }

    exit_suif();
    return 0;
  }


static void usage(void)
  {
    fprintf(stderr,
            "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void find_callees_on_proc(proc_sym *the_proc)
  {
    walk(the_proc->block(), &find_callees_on_object);
  }

static void find_callees_on_object(suif_object *the_object,
                                   so_walker *the_walker)
  {
    if (the_walker->in_annotation())
        return;
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_cal)
        return;
    in_cal *this_call = (in_cal *)the_instr;
    proc_sym *callee = proc_for_call(this_call);
    if (callee == NULL)
        return;
    proc_sym *caller = the_instr->owner()->proc();
    annote *callees_annote =
            caller->annotes()->peek_annote(k_leavesfirst_callees);
    if (callees_annote == NULL)
      {
        callees_annote = new annote(k_leavesfirst_callees);
        caller->annotes()->append(callees_annote);
      }
    callees_annote->immeds()->append(callee);
  }

static void place_proc(proc_sym *the_proc)
  {
    if (the_proc->peek_annote(k_leavesfirst_placed) != NULL)
        return;
    the_proc->append_annote(k_leavesfirst_placed);
    immed_list *callees =
            (immed_list *)(the_proc->peek_annote(k_leavesfirst_callees));
    if (callees != NULL)
      {
        immed_list_iter call_iter(callees);
        while (!call_iter.is_empty())
          {
            immed this_immed = call_iter.step();
            assert(this_immed.is_symbol());
            sym_node *this_sym = this_immed.symbol();
            assert(this_sym->is_proc());
            proc_sym *this_callee = (proc_sym *)this_sym;
            place_proc(this_callee);
          }
      }

    base_symtab *parent = the_proc->parent();
    sym_node_list *sym_list = parent->symbols();
    sym_node_list_e *list_e = sym_list->lookup(the_proc);
    assert(list_e != NULL);
    sym_list->remove(list_e);
    delete list_e;
    sym_list->append(the_proc);
  }
