/* file "main.cc" of the localize program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for localize.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:44 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        The localize program turns global variables that are used in
        only a single procedure into static local variables for the
        procedure.  Note that variables with external linkage might be
        used outside all the files this pass can see, so they cannot
        be turned into locals; only variables in the file symbol table
        are eligible for localization.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static proc_sym *current_proc = NULL;
static const char *k_localize_status;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void mark_on_symtab(base_symtab *the_symtab);
static void mark_for_node(tree_node *the_node, void *);
static void mark_for_instr(instruction *the_instr);
static void mark_for_op(operand the_op);
static void mark_for_annotes(suif_object *the_object);
static void mark_for_immed(immed the_immed);
static void mark_sym_ref(sym_node *the_symbol);
static void mark_type_ref(type_node *the_type);
static void mark_bound_ref(array_bound the_bound);
static boolean mark_object(suif_object *the_object);
static void mark_global_uses(tree_proc *the_proc);
static void localize_on_proc(tree_proc *the_proc);
static boolean object_unique_here(suif_object *this_object);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    ANNOTE(k_localize_status, "localize status", FALSE);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;

        mark_on_symtab(fse->symtab());

        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc();
            mark_global_uses(this_proc_sym->block());
            this_proc_sym->flush_proc();
          }

        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc();
            localize_on_proc(this_proc_sym->block());
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr, "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void mark_on_symtab(base_symtab *the_symtab)
  {
    mark_for_annotes(the_symtab);

    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_symbol = sym_iter.step();
        if (this_symbol->is_proc())
            mark_sym_ref(this_symbol);
      }

    var_def_list_iter def_iter(the_symtab->var_defs());
    while (!def_iter.is_empty())
      {
        var_def *this_def = def_iter.step();
        if (!this_def->variable()->parent()->is_file())
            mark_for_annotes(this_def);
      }
  }

static void mark_for_node(tree_node *the_node, void *)
  {
    mark_for_annotes(the_node);

    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        mark_for_instr(the_tree_instr->instr());
      }
    else if (the_node->is_block())
      {
        tree_block *the_block = (tree_block *)the_node;
        mark_on_symtab(the_block->symtab());
      }
    the_node->map(&mark_for_node, NULL, TRUE, FALSE);
  }

static void mark_for_instr(instruction *the_instr)
  {
    mark_for_annotes(the_instr);

    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        mark_for_immed(the_ldc->value());
      }

    if (the_instr->dst_op().is_symbol())
        mark_sym_ref(the_instr->dst_op().symbol());

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
        mark_for_op(the_instr->src_op(src_num));
  }

static void mark_for_op(operand the_op)
  {
    if (the_op.is_symbol())
        mark_sym_ref(the_op.symbol());
    else if (the_op.is_expr())
        mark_for_instr(the_op.instr());
  }

static void mark_for_annotes(suif_object *the_object)
  {
    annote_list_iter annote_iter(the_object->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();
        immed_list_iter immed_iter(this_annote->immeds());
        while (!immed_iter.is_empty())
          {
            immed this_immed = immed_iter.step();
            mark_for_immed(this_immed);
          }
      }
  }

static void mark_for_immed(immed the_immed)
  {
    switch (the_immed.kind())
      {
        case im_symbol:
            mark_sym_ref(the_immed.symbol());
            break;
        case im_type:
            mark_type_ref(the_immed.type());
            break;
        case im_op:
            mark_for_op(the_immed.op());
            break;
        case im_instr:
            mark_for_instr(the_immed.instr());
            break;
        default:
            break;
      }
  }

static void mark_sym_ref(sym_node *the_symbol)
  {
    if (!the_symbol->parent()->is_file())
        return;

    boolean marked = mark_object(the_symbol);
    if (!marked)
        return;

    mark_for_annotes(the_symbol);

    switch (the_symbol->kind())
      {
        case SYM_PROC:
          {
            proc_sym *the_proc = (proc_sym *)the_symbol;
            mark_type_ref(the_proc->type());
            break;
          }
        case SYM_LABEL:
            assert(FALSE);
        case SYM_VAR:
          {
            var_sym *the_var = (var_sym *)the_symbol;
            mark_type_ref(the_var->type());

            if (the_var->has_var_def())
                mark_for_annotes(the_var->definition());

            if (the_var->parent_var() != NULL)
                mark_sym_ref(the_var->parent_var());

            if (the_var->num_children() != 0)
              {
                unsigned num_children = the_var->num_children();
                for (unsigned child_num = 0; child_num < num_children;
                     ++child_num)
                  {
                    mark_sym_ref(the_var->child_var(child_num));
                  }
              }

            break;
          }
        default:
            assert(FALSE);
      }
  }

static void mark_type_ref(type_node *the_type)
  {
    if (!the_type->parent()->is_file())
        return;

    boolean marked = mark_object(the_type);
    if (!marked)
        return;

    mark_for_annotes(the_type);

    switch (the_type->op())
      {
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
        case TYPE_ENUM:
            break;
        case TYPE_PTR:
          {
            ptr_type *the_ptr = (ptr_type *)the_type;
            mark_type_ref(the_ptr->ref_type());
            break;
          }
        case TYPE_ARRAY:
          {
            array_type *the_array = (array_type *)the_type;
            mark_type_ref(the_array->elem_type());
            mark_bound_ref(the_array->lower_bound());
            mark_bound_ref(the_array->upper_bound());
            break;
          }
        case TYPE_FUNC:
          {
            func_type *the_func = (func_type *)the_type;
            mark_type_ref(the_func->return_type());
            unsigned num_args = the_func->num_args();
            for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                mark_type_ref(the_func->arg_type(arg_num));
            break;
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *the_struct = (struct_type *)the_type;
            unsigned num_fields = the_struct->num_fields();
            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
                mark_type_ref(the_struct->field_type(field_num));
            break;
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *the_modifier = (modifier_type *)the_type;
            mark_type_ref(the_modifier->base());
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void mark_bound_ref(array_bound the_bound)
  {
    if (the_bound.is_variable())
        mark_sym_ref(the_bound.variable());
  }

static boolean mark_object(suif_object *the_object)
  {
    annote *status_annote =
            the_object->annotes()->peek_annote(k_localize_status);
    if (status_annote == NULL)
      {
        status_annote = new annote(k_localize_status);
        immed status_immed;
        if (current_proc != NULL)
            status_immed = immed(current_proc);
        status_annote->immeds()->append(status_immed);
        the_object->annotes()->append(status_annote);
        return TRUE;
      }
    immed old_status_immed = (*(status_annote->immeds()))[0];
    if (!old_status_immed.is_symbol())
        return FALSE;
    sym_node *old_symbol = old_status_immed.symbol();
    if (old_symbol == current_proc)
        return FALSE;

    status_annote->immeds()->head()->contents = immed();
    return TRUE;
  }

static void mark_global_uses(tree_proc *the_proc)
  {
    assert(the_proc != NULL);
    current_proc = the_proc->proc();
    mark_for_node(the_proc, NULL);
    current_proc = NULL;
  }

static void localize_on_proc(tree_proc *the_proc)
  {
    assert(the_proc != NULL);
    current_proc = the_proc->proc();
    proc_symtab *proc_syms = the_proc->proc_syms();
    base_symtab *file_syms = proc_syms->parent();

    sym_node_list_iter sym_iter(file_syms->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_symbol = sym_iter.step();
        if (this_symbol->is_var() && object_unique_here(this_symbol))
          {
            var_sym *this_var = (var_sym *)this_symbol;
            assert(this_var->has_var_def());
            var_def *this_def = this_var->definition();
            file_syms->remove_sym(this_var);
            proc_syms->add_sym(this_var);
            file_syms->remove_def(this_def);
            proc_syms->add_def(this_def);
          }
      }

    type_node_list_iter type_iter(file_syms->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (object_unique_here(this_type))
          {
            file_syms->remove_type(this_type);
            proc_syms->add_type(this_type);
          }
      }

    current_proc = NULL;
  }

static boolean object_unique_here(suif_object *this_object)
  {
    annote *status_annote =
            this_object->annotes()->peek_annote(k_localize_status);
    if (status_annote == NULL)
        return FALSE;
    immed status_immed = (*(status_annote->immeds()))[0];
    if (!status_immed.is_symbol())
        return FALSE;
    sym_node *old_symbol = status_immed.symbol();
    return (old_symbol == current_proc);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
