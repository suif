/* file "main.cc" of the c_lib_info program for SUIF */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for c_lib_info.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:08 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        The c_lib_info program adds information to the inter-file
        symbol table of SUIF programs about the standard C libraries.
        Currently, it only attaches ``pure function'' annotations to
        some proc_syms for library functions, but it may eventually
        provide more information about the functions and variables
        used by the standard C libraries.

        Note that it is only legal to run this pass if the resulting
        program is to be linked to the standard C libraries.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void mark_pure_functions(global_symtab *the_symtab);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    mark_pure_functions(fileset->globals());

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr, "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void mark_pure_functions(global_symtab *the_symtab)
  {
    static char *pure_function_names[] =
      {
        /*
         * These are all from the standard math library, as described
         * in ANSI/ISO 9899-1990, section 7.5.
         */
        "sin",
        "cos",
        "tan",
        "asin",
        "acos",
        "atan2",
        "atan",
        "sinh",
        "cosh",
        "tanh",
        "log",
        "sqrt",
        "exp",
        "ldexp",
        "log10",
        "pow",
        "ceil",
        "fabs",
        "floor",
        "fmod",
        NULL
      };

    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_symbol = sym_iter.step();
        if (this_symbol->is_proc())
          {
            char **follow = &(pure_function_names[0]);
            while (*follow != NULL)
              {
                if (strcmp(this_symbol->name(), *follow) == 0)
                  {
                    this_symbol->append_annote(k_pure_function);
                    break;
                  }
                ++follow;
              }
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
