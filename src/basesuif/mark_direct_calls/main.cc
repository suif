/* file "main.cc" of the mark_direct_calls program for SUIF */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for mark_direct_calls.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:47 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void do_proc(tree_proc *the_proc);
static void do_object(suif_object *the_object, so_walker *the_walker);
static boolean is_callsite_addr(instruction *the_instr);
static void begin_symtab(global_symtab *the_symtab);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    begin_symtab(fileset->globals());

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        begin_symtab(fse->symtab());
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            do_proc(this_proc_sym->block());
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr,
            "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void do_proc(tree_proc *the_proc)
  {
    walk(the_proc, &do_object);
  }

static void do_object(suif_object *the_object, so_walker *the_walker)
  {
    if (the_walker->in_annotation())
        return;
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_ldc)
        return;
    in_ldc *the_ldc = (in_ldc *)the_instr;
    immed value = the_ldc->value();
    if (!value.is_symbol())
        return;
    sym_node *the_sym = value.symbol();
    if (!the_sym->is_proc())
        return;
    if ((value.offset() != 0) || (!is_callsite_addr(the_instr)))
      {
        annote *old_annote =
                the_sym->annotes()->get_annote(k_direct_calls_only);
        if (old_annote != NULL)
            delete old_annote;
      }
  }

static boolean is_callsite_addr(instruction *the_instr)
  {
    operand dest_op = the_instr->dst_op();
    if (!dest_op.is_instr())
        return FALSE;
    instruction *dest_instr = dest_op.instr();
    if (dest_instr->opcode() == io_cvt)
      {
        if (!dest_instr->result_type()->is_ptr())
            return FALSE;
        return is_callsite_addr(dest_instr);
      }
    if (dest_instr->opcode() != io_cal)
        return FALSE;
    in_cal *the_call = (in_cal *)dest_instr;
    return (the_call->addr_op() == operand(the_instr));
  }

static void begin_symtab(global_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->is_proc() && unreferenced_outside_fileset(this_sym))
            this_sym->append_annote(k_direct_calls_only);
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
