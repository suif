/* file "main.cc" of the rescope program for SUIF */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for rescope.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:55 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        The rescope program changes the scopes of global variable or
        procedure symbols or global types, either from the inter-file
        global symbol table to a file symbol table or from a file
        symbol table to the inter-file symbol table.

        The reason there is a separate program for these two
        operations is that they make unusual requirements on the way
        procedures are read and written.  Symbols and types in the
        inter-file and file symbol tables use disjoint ranges for
        their id numbers, which is how symbol and type references are
        read and written.  All procedures that might reference a
        symbol or type must be read in from a file before that object
        may safely be moved to a different symbol table with a
        different range of id numbers.  But no procedure that
        references the object may be written out until it gets its new
        id number.  This is not a problem for objects moving to or
        from a local scope -- the symbol can only be referenced in a
        single procedure at the time of the move.  But for moves
        between file and inter-file symbol tables, directly moving the
        object would in general require all procedures from a file to
        be read before any are written.

        This program avoids having more than one procedure in memory
        by using two copies of the object to be moved.  The original
        is left in the original table while the copy is put in the
        destination.  Then as procedures are read in, the original is
        used and this program goes over the procedure and changes all
        references to the original to the copy, then writes the
        procedure out.  It does this simultaneously for all symbols
        and types that are being moved.

        Other passes communicate which objects to move and where to
        move them through ``globalize'' and ``filize'' annotations on
        global objects.  The ``globalize'' annotation takes no
        arguments and should only appear on objects in a file symbol
        table that should be moved to the inter-file table.  The
        ``filize'' annotation should have exactly one integer immed as
        data; this annotation should appear only on objects in the
        inter-file symbol table that should be moved to a file symbol
        table specified by the integer immed.  The integer immed
        refers to the file id number; one of the file_set_entries for
        the input files given to this program must have a file_id()
        that matches the immed.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void prep_file_symtab(file_symtab *the_symtab);
static void prep_interfile_symtab(global_symtab *the_symtab);
static void cleanup_symtab(global_symtab *the_symtab);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    prep_interfile_symtab(fileset->globals());

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        prep_file_symtab(fse->symtab());
        do_replacement(fse);
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            if (this_proc_sym->is_readable())
              {
                this_proc_sym->read_proc(TRUE, FALSE);
                tree_proc *this_tree_proc = this_proc_sym->block();
                do_replacement(this_tree_proc);
                /* Careful: Now this_tree_proc may have a proc_sym
                 * different from this_proc_sym. */
                this_tree_proc->proc()->write_proc(fse);
                this_tree_proc->proc()->flush_proc();
              }
          }
      }

    do_replacement(fileset->globals());

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        cleanup_symtab(fse->symtab());
      }

    cleanup_symtab(fileset->globals());

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr, "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void prep_file_symtab(file_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->annotes()->peek_annote(k_replacement) != NULL)
          {
            error_line(1, this_sym, "symbol already has \"%s\" annotation",
                       k_replacement);
          }
        if (this_sym->annotes()->peek_annote(k_globalize) == NULL)
            continue;
        sym_node *new_sym = this_sym->copy();
        this_sym->copy_annotes(new_sym);
        annote *glob_annote = new_sym->annotes()->get_annote(k_globalize);
        assert(glob_annote != NULL);
        delete glob_annote;
        fileset->globals()->add_sym(new_sym);
        this_sym->append_annote(k_replacement, new immed_list(immed(new_sym)));
      }

    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (this_type->annotes()->peek_annote(k_replacement) != NULL)
          {
            error_line(1, this_type, "type already has \"%s\" annotation",
                       k_replacement);
          }
        if (this_type->annotes()->peek_annote(k_globalize) == NULL)
            continue;
        type_node *new_type = this_type->copy();
        this_type->copy_annotes(new_type);
        annote *glob_annote = new_type->annotes()->get_annote(k_globalize);
        assert(glob_annote != NULL);
        delete glob_annote;
        fileset->globals()->add_type(new_type);
        this_type->append_annote(k_replacement,
                                 new immed_list(immed(new_type)));
      }
  }

static void prep_interfile_symtab(global_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->annotes()->peek_annote(k_replacement) != NULL)
          {
            error_line(1, this_sym, "symbol already has \"%s\" annotation",
                       k_replacement);
          }
        if (this_sym->annotes()->peek_annote(k_filize) == NULL)
            continue;
        sym_node *new_sym = this_sym->copy();
        this_sym->copy_annotes(new_sym);
        annote *the_annote = new_sym->annotes()->get_annote(k_filize);
        assert(the_annote != NULL);
        immed_list *the_immeds = the_annote->immeds();
        assert(the_immeds != NULL);
        if (the_immeds->is_empty())
            error_line(1, this_sym, "no data for \"%s\" annotation", k_filize);
        immed this_immed = the_immeds->head()->contents;
        delete the_annote;
        if (!this_immed.is_int_const())
          {
            error_line(1, this_sym, "non-integer data for \"%s\" annotation",
                       k_filize);
          }
        i_integer file_ii = immed_to_ii(this_immed);
        if (!file_ii.is_c_int())
          {
            error_line(1, this_sym,
                       "data for \"%s\" annotation is out of range for file"
                       " number", k_filize);
          }
        file_set_entry *the_fse = fileset->find_by_num(file_ii.c_int());
        if (the_fse == NULL)
          {
            error_line(1, this_sym,
                       "no such file number: %d", file_ii.c_int());
          }
        the_fse->symtab()->add_sym(new_sym);
        this_sym->append_annote(k_replacement, new immed_list(immed(new_sym)));
      }

    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (this_type->annotes()->peek_annote(k_replacement) != NULL)
          {
            error_line(1, this_type, "type already has \"%s\" annotation",
                       k_replacement);
          }
        if (this_type->annotes()->peek_annote(k_filize) == NULL)
            continue;
        type_node *new_type = this_type->copy();
        this_type->copy_annotes(new_type);
        annote *the_annote = new_type->annotes()->get_annote(k_filize);
        assert(the_annote != NULL);
        immed_list *the_immeds = the_annote->immeds();
        assert(the_immeds != NULL);
        if (the_immeds->is_empty())
          {
            error_line(1, this_type,
                       "no data for \"%s\" annotation", k_filize);
          }
        immed this_immed = the_immeds->head()->contents;
        delete the_annote;
        if (!this_immed.is_int_const())
          {
            error_line(1, this_type,
                       "non-integer data for \"%s\" annotation", k_filize);
          }
        i_integer file_ii = immed_to_ii(this_immed);
        if (!file_ii.is_c_int())
          {
            error_line(1, this_type,
                       "data for \"%s\" annotation is out of range for file"
                       " number", k_filize);
          }
        file_set_entry *the_fse = fileset->find_by_num(file_ii.c_int());
        if (the_fse == NULL)
          {
            error_line(1, this_type,
                       "no such file number: %d", file_ii.c_int());
          }
        the_fse->symtab()->add_type(new_type);
        this_type->append_annote(k_replacement,
                                 new immed_list(immed(new_type)));
      }
  }

static void cleanup_symtab(global_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->annotes()->peek_annote(k_replacement) != NULL)
          {
            the_symtab->remove_sym(this_sym);
            delete this_sym;
          }
      }

    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (this_type->annotes()->peek_annote(k_replacement) != NULL)
          {
            the_symtab->remove_type(this_type);
            delete this_type;
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
