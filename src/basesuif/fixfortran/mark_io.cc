/* file "mark_io.cc" of the fixfortran program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains code to mark call instructions that do Fortran
 *  I/O as either input or output.
 */

#define RCS_BASE_FILE mark_io_cc

#include "fixfortran.h"
#include <string.h>

RCS_BASE(
    "$Id: mark_io.cc,v 1.1.1.1 1998/06/16 15:17:25 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void mark_io_on_list(tree_node_list *node_list)
  {
    static boolean is_read = FALSE;
    static boolean is_write = FALSE;

    tree_node_list_iter node_iter(node_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        switch (this_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)this_node;
                instruction *the_instr = the_tree_instr->instr();
                if (the_instr->opcode() != io_cal)
                    break;
                in_cal *the_call = (in_cal *)the_instr;
                proc_sym *the_proc = proc_for_call(the_call);
                if (the_proc == NULL)
                    break;
                const char *pname = the_proc->name();
                if ((strcmp(pname, "do_fio") == 0) ||
                    (strcmp(pname, "do_lio") == 0) ||
                    (strcmp(pname, "do_uio") == 0))
                  {
                    assert(is_read || is_write);
                    if (is_read)
                        the_call->append_annote(k_io_read);
                    else
                        the_call->append_annote(k_io_write);
                  }
                else if ((strlen(pname) == 6) && (pname[1] == '_'))
                  {
                    if (pname[0] == 's')
                      {
                        boolean closed = FALSE;
                        if (strcmp(&(pname[3]), "sne") == 0)
                            closed = TRUE;
                        if (pname[2] == 'r')
                          {
                            assert(!is_read);
                            assert(!is_write);
                            if (!closed)
                                is_read = TRUE;
                            the_call->append_annote(k_io_read);
                          }
                        else if (pname[2] == 'w')
                          {
                            assert(!is_read);
                            assert(!is_write);
                            if (!closed)
                                is_write = TRUE;
                            the_call->append_annote(k_io_write);
                          }
                      }
                    else if (pname[0] == 'e')
                      {
                        if (pname[2] == 'r')
                          {
                            assert(is_read);
                            the_call->append_annote(k_io_read);
                            is_read = FALSE;
                          }
                        else if (pname[2] == 'w')
                          {
                            assert(is_write);
                            the_call->append_annote(k_io_write);
                            is_write = FALSE;
                          }
                      }
                  }
                break;
              }
            case TREE_LOOP:
              {
                tree_loop *the_loop = (tree_loop *)this_node;
                mark_io_on_list(the_loop->body());
                mark_io_on_list(the_loop->test());
                break;
              }
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)this_node;
                mark_io_on_list(the_for->body());
                mark_io_on_list(the_for->landing_pad());
                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)this_node;
                mark_io_on_list(the_if->header());
                mark_io_on_list(the_if->then_part());
                mark_io_on_list(the_if->else_part());
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)this_node;
                mark_io_on_list(the_block->body());
                break;
              }
            default:
                assert(FALSE);
          }
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
