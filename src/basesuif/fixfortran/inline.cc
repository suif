/* file "inline.cc" of the fixfortran program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains code to inline some calls to Fortran
 *  intrinsics.
 */

#define RCS_BASE_FILE inline_cc

#include "fixfortran.h"
#include <string.h>

RCS_BASE(
    "$Id: inline.cc,v 1.1.1.1 1998/06/16 15:17:24 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void inline_complex_part_call(in_cal *the_call, int part, int size);
static void inline_complex_exp(in_cal *the_call, type_node *elem_type);
static void inline_complex_conjugate(in_cal *the_call);
static proc_sym *find_math_sym(char *fast_name, char *base_name);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

/*
 *  Inlines fortran MAX, MIN, and ABS calls.
 */
extern void inline_intrinsics_on_instr(instruction *the_instr, void *)
  {
    if (the_instr->format() != inf_cal)
        return;

    in_cal *the_call = (in_cal *)the_instr;

    sym_node *address_symbol = proc_for_call(the_call);
    if (address_symbol == NULL)
        return;

    const char *name = address_symbol->name();

    if ((strcmp(name, "pow_ii") == 0) || (strcmp(name, "pow_dd") == 0))
      {
        if (the_call->num_args() != 2)
            return;
        char *new_name;
        if (strcmp(name, "pow_ii") == 0)
            new_name = "fr_power_i_i";
        else
            new_name = "fr_power_d_d";
        operand arg1 = the_call->argument(0);
        operand arg2 = the_call->argument(1);
        type_node *arg_type1 = arg1.type();
        type_node *arg_type2 = arg2.type();
        if ((!arg_type1->is_ptr()) || (!arg_type2->is_ptr()))
            return;
        ptr_type *ptr1 = (ptr_type *)arg_type1;
        ptr_type *ptr2 = (ptr_type *)arg_type2;
        type_node *base1 = ptr1->ref_type();
        type_node *base2 = ptr2->ref_type();
        if (((base1->op() != TYPE_INT) && (base1->op() != TYPE_FLOAT)) ||
            ((base2->op() != TYPE_INT) && (base2->op() != TYPE_FLOAT)))
          {
            return;
          }
        proc_sym *new_proc = fileset->globals()->lookup_proc(new_name);
        if (new_proc == NULL)
          {
            if ((base1->parent() != fileset->globals()) ||
                (base2->parent() != fileset->globals()) ||
                (the_call->result_type()->parent() != fileset->globals()))
              {
                return;
              }
            func_type *new_func_type =
                    new func_type(the_call->result_type(), base1, base2);
            new_func_type =
                    (func_type *)(fileset->globals()->install_type(
                                          new_func_type));
            new_proc =
                    fileset->globals()->new_proc(new_func_type, src_unknown,
                                                 new_name);
            new_proc->append_annote(k_fortran_power_op);
            new_proc->append_annote(k_pure_function);
          }
        operand old_addr_op = the_call->addr_op();
        old_addr_op.remove();
        kill_op(old_addr_op);
        the_call->set_addr_op(addr_op(new_proc));
        arg1.remove();
        arg2.remove();
        arg1 = fold_load(arg1);
        arg2 = fold_load(arg2);
        the_call->set_argument(0, arg1);
        the_call->set_argument(1, arg2);
        return;
      }

    if_ops opcode;

    if ((strcmp(name, "h_max") == 0) || (strcmp(name, "i_max") == 0) ||
        (strcmp(name, "r_max") == 0) || (strcmp(name, "d_max") == 0) ||
        (strcmp(name, "ri_max") == 0) || (strcmp(name, "di_max") == 0) ||
        (strcmp(name, "ir_max") == 0) || (strcmp(name, "id_max") == 0))
      {
        opcode = io_max;
      }
    else if ((strcmp(name, "h_min") == 0) || (strcmp(name, "i_min") == 0) ||
             (strcmp(name, "r_min") == 0) || (strcmp(name, "d_min") == 0) ||
             (strcmp(name, "ri_min") == 0) || (strcmp(name, "di_min") == 0) ||
             (strcmp(name, "ir_min") == 0) || (strcmp(name, "id_min") == 0))
      {
        opcode = io_min;
      }
    else if ((strcmp(name, "sd_abs") == 0) || (strcmp(name, "sr_abs") == 0) ||
             (strcmp(name, "si_abs") == 0) || (strcmp(name, "sh_abs") == 0))
      {
        opcode = io_abs;
      }
    else
      {
        if (strcmp(name, "d_imag") == 0)
            inline_complex_part_call(the_call, 1, target.size[C_double]);
        else if (strcmp(name, "r_imag") == 0)
            inline_complex_part_call(the_call, 1, target.size[C_float]);
        else if (strcmp(name, "c_exp") == 0)
            inline_complex_exp(the_call, type_float);
        else if (strcmp(name, "z_exp") == 0)
            inline_complex_exp(the_call, type_double);
        else if (strcmp(name, "r_cnjg") == 0)
            inline_complex_conjugate(the_call);
        else if (strcmp(name, "d_cnjg") == 0)
            inline_complex_conjugate(the_call);
        return;
      }

    type_node *the_type = the_call->result_type();
    unsigned num_operands = the_call->num_args();

    instruction *new_instr;

    if (opcode == io_abs)
      {
        if (num_operands != 1)
          {
            warning_line(the_call->parent(),
                         "call to abs() with %d arguments can't be inlined,"
                         " ignoring", num_operands);
            return;
          }
        operand source = the_call->argument(0);
        source.remove();
        new_instr = new in_rrr(opcode, the_type, operand(), source);
      }
    else
      {
        char *function_name;
        if (opcode == io_max)
            function_name = "max";
        else
            function_name = "min";
        if (num_operands < 3)
          {
            warning_line(the_call->parent(),
                         "call to %s() with %d arguments can't be inlined,"
                         " ignoring", function_name, num_operands);
            return;
          }

        operand count_operand = the_call->argument(0);
        boolean wrong_format = FALSE;
        instruction *count_instr;
        in_ldc *count_ldc;
        immed count_value;
        if (count_operand.kind() != OPER_INSTR)
            wrong_format = TRUE;
        if (!wrong_format)
          {
            count_instr = count_operand.instr();
            if (count_instr->format() != inf_ldc)
                wrong_format = TRUE;
          }
        if (!wrong_format)
          {
            count_ldc = (in_ldc *)count_instr;
            count_value = count_ldc->value();
            if (!count_value.is_unsigned_int())
                wrong_format = TRUE;
          }
        if (!wrong_format)
          {
            if (count_value.unsigned_int() != num_operands - 1)
                wrong_format = TRUE;
          }
        if (wrong_format)
          {
            warning_line(the_call->parent(),
                         "the arguments to a call to %s() don't have the "
                         "proper form\n(an argument count followed by the "
                         "actual arguments), so it will not be\ninlined.",
                         function_name);
            return;
          }

        operand source_1 = the_call->argument(1);
        source_1.remove();

        operand source_2 = the_call->argument(2);
        source_2.remove();

        type_node *computation_type = the_type;
        type_node *operand_type = source_1.type();
        if ((the_type->op() == TYPE_INT) && (operand_type->op() == TYPE_FLOAT))
            computation_type = operand_type;

        source_1 = cast_op(source_1, computation_type);
        source_2 = cast_op(source_2, computation_type);

        new_instr = new in_rrr(opcode, computation_type, operand(), source_1,
                               source_2);
        unsigned arg_num = 3;
        while (arg_num < num_operands)
          {
            source_1.set_instr(new_instr);
            source_2 = the_call->argument(arg_num);
            source_2.remove();
            source_2 = cast_op(source_2, computation_type);
            new_instr = new in_rrr(opcode, computation_type, operand(),
                                   source_1, source_2);
            ++arg_num;
          }

        operand new_op = cast_op(operand(new_instr), the_type);
        assert(new_op.is_expr());
        new_instr = new_op.instr();
      }

    replace_instruction(the_call, new_instr);

    delete the_call;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void inline_complex_part_call(in_cal *the_call, int part, int size)
  {
    if (the_call->num_args() != 1)
      {
        error_line(1, NULL,
                   "call to complex selector function with %d arguments",
                   the_call->num_args());
      }

    operand arg_op = the_call->argument(0);
    arg_op.remove();

    type_node *arg_type = arg_op.type()->unqual();
    if (!arg_type->is_ptr())
      {
        error_line(1, NULL,
                   "call to complex selector function with non-pointer "
                   "argument", the_call->num_args());
      }
    ptr_type *arg_ptr = (ptr_type *)arg_type;
    type_node *arg_base = arg_ptr->ref_type()->unqual();

    type_node *result_type = new base_type(TYPE_FLOAT, size);
    result_type = fileset->globals()->install_type(result_type);

    boolean correct_type = FALSE;
    if (arg_base->is_array())
      {
        array_type *arg_array = (array_type *)arg_base;
        if ((arg_array->elem_type() == result_type) &&
            (arg_array->lower_bound() == array_bound(0)) &&
            (arg_array->upper_bound() == array_bound(1)))
          {
            correct_type = TRUE;
          }
      }

    if (!correct_type)
      {
        array_type *new_array =
                new array_type(result_type, array_bound(0), array_bound(1));
        ptr_type *new_ptr =
                fileset->globals()->install_type(new_array)->ptr_to();
        arg_op = operand(new in_rrr(io_cvt, new_ptr, operand(), arg_op));
      }

    in_array *new_array = add_const_aref(arg_op, part);
    instruction *new_instr =
            new in_rrr(io_lod, result_type, operand(), operand(new_array));

    if (the_call->result_type() != result_type)
      {
        new_instr =
                new in_rrr(io_cvt, the_call->result_type(), operand(),
                           operand(new_instr));
      }

    replace_instruction(the_call, new_instr);

    delete the_call;
  }

static void inline_complex_exp(in_cal *the_call, type_node *elem_type)
  {
    if (!the_call->dst_op().is_null())
        error_line(1, the_call, "bad destination op for complex exp call");
    if (the_call->num_args() != 2)
        error_line(1, the_call, "bad arg count for complex exp call");
    operand result_pointer = the_call->argument(0);
    operand arg_pointer = the_call->argument(1);
    result_pointer.remove();
    arg_pointer.remove();
    operand arg_real_part =
            fold_load(operand(add_const_aref(arg_pointer.clone(), 0)));
    operand arg_imag_part = fold_load(operand(add_const_aref(arg_pointer, 1)));

    operand result_real_pointer =
            operand(add_const_aref(result_pointer.clone(), 0));
    operand result_imag_pointer = operand(add_const_aref(result_pointer, 1));

    tree_instr *parent = the_call->parent();
    var_sym *temp_var = parent->scope()->new_unique_var(type_double);

    proc_sym *exp_sym = find_math_sym("F_exp", "exp");
    in_cal *exp_call =
            new in_cal(type_double, operand(temp_var), addr_op(exp_sym),
                       cast_op(arg_real_part, type_double));
    parent->parent()->insert_before(new tree_instr(exp_call),
                                    parent->list_e());

    proc_sym *cos_sym = find_math_sym("F_cos", "cos");
    in_cal *cos_call =
            new in_cal(type_double, operand(), addr_op(cos_sym),
                       cast_op(arg_imag_part.clone(), type_double));
    in_rrr *cos_str =
            new in_rrr(io_str, type_void, operand(), result_real_pointer,
                       cast_op(operand(temp_var) * operand(cos_call),
                               elem_type));
    parent->parent()->insert_before(new tree_instr(cos_str), parent->list_e());

    proc_sym *sin_sym = find_math_sym("F_sin", "sin");
    in_cal *sin_call =
            new in_cal(type_double, operand(), addr_op(sin_sym),
                       cast_op(arg_imag_part, type_double));
    in_rrr *sin_str =
            new in_rrr(io_str, type_void, operand(), result_imag_pointer,
                       cast_op(operand(temp_var) * operand(sin_call),
                               elem_type));
    parent->remove_instr(the_call);
    parent->set_instr(sin_str);
    delete the_call;
  }

static void inline_complex_conjugate(in_cal *the_call)
  {
    if (!the_call->dst_op().is_null())
        error_line(1, the_call, "bad destination op for complex exp call");
    if (the_call->num_args() != 2)
        error_line(1, the_call, "bad arg count for complex exp call");
    operand result_pointer = the_call->argument(0);
    operand arg_pointer = the_call->argument(1);
    result_pointer.remove();
    arg_pointer.remove();
    operand arg_real_part =
            fold_load(operand(add_const_aref(arg_pointer.clone(), 0)));
    operand arg_imag_part = fold_load(operand(add_const_aref(arg_pointer, 1)));

    operand result_real_pointer =
            operand(add_const_aref(result_pointer.clone(), 0));
    operand result_imag_pointer = operand(add_const_aref(result_pointer, 1));

    tree_instr *parent = the_call->parent();

    in_rrr *copy_str =
            new in_rrr(io_str, type_void, operand(), result_real_pointer,
                       arg_real_part);
    parent->parent()->insert_before(new tree_instr(copy_str),
                                    parent->list_e());

    in_rrr *neg_str =
            new in_rrr(io_str, type_void, operand(), result_imag_pointer,
                       -arg_imag_part);
    parent->remove_instr(the_call);
    parent->set_instr(neg_str);
    delete the_call;
  }

static proc_sym *find_math_sym(char *fast_name, char *base_name)
  {
    proc_sym *fast_sym = fileset->globals()->lookup_proc(fast_name, FALSE);
    if (fast_sym != NULL)
        return fast_sym;
    proc_sym *base_sym = fileset->globals()->lookup_proc(base_name, FALSE);
    if (base_sym != NULL)
        return base_sym;
    func_type *the_type = new func_type(type_double);
    the_type = (func_type *)(fileset->globals()->install_type(the_type));
    return fileset->globals()->new_proc(the_type, src_unknown, base_name);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
