/* file "main.cc" of the fixfortran program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for fixfortran.  Many of the
 *  details of fixfortran are implemented here.
 */

#define RCS_BASE_FILE main_cc

#include "fixfortran.h"
#include <limits.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:24 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        The fixfortran program is, along with sf2c and snoot, part of
        the SUIF Fortran front end.  It tries to undo some of the
        changes sf2c makes in the code to write it in C.  When
        possible, fixfortran de-linearizes the array linearizations
        sf2c does, inlines calls to max, min, absolute value, and
        imaginary part functions, finds and marks common block
        variables, and changes structures representing complex numbers
        into arrays.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

boolean errors = FALSE;
const char *k_io_read;
const char *k_io_write;

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static size_t max_int_str_len;
static alist *aux_var_values;
static alist *array_types;
static const char *k_fixfortran_needed_aux;
static const char *k_fixfortran_fixed_array_type;
static const char *k_fixfortran_original_type;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void mark_intrinsics(global_symtab *the_symtab);
static void do_proc(tree_proc *the_tree_proc);
static void de_linearize(tree_proc *the_proc);
void fix_array_type_for_base_expr(instruction *the_instr);
void fix_array_type_for_str_base_expr(instruction *the_instr);
static void aux_sub_on_instr(instruction *the_instr, void *);
static void fix_arrays_on_node(tree_node *the_node, void *);
static void fix_arrays_on_instr(instruction *the_instr);
static void fix_arrays_on_operand(operand the_operand);
static array_type *array_type_from_aref(in_array *the_aref);
static array_type *register_type_for_base_name(const char *base_name,
                                               type_node *element_type,
                                               base_symtab *the_symtab);
static array_type *array_type_for_base_name(const char *base_name,
                                            type_node *base_type,
                                            unsigned *num_dimensions);
static type_node *string_type_for_base_name(const char *base_name,
                                            base_symtab *the_symtab);
static array_bound bound_from_aux(var_sym *the_var);
static void fix_addresses(instruction *the_instr, void *);
static void deallocate_operand(operand to_go);
static operand build_offset_operand(array_type *the_array_type,
                                    int num_dimensions);
static const char *last_field(immed_list *field_immeds);
static char *guess_base_var_name(in_array *the_array);
static void linear_form(operand *op_a, operand *op_b, operand original,
                        var_sym *the_var);
static boolean is_zero(operand the_operand);
static void nullify_operand(operand *the_operand);
static boolean is_subtracted(operand the_operand, var_sym *the_variable,
                             boolean negated);
static operand remove_subtracted_variable(operand the_operand,
                                          var_sym *the_variable,
                                          boolean negated);
static boolean operand_is_var(operand the_operand, var_sym *the_variable);
static void mark_params_call_by_ref(tree_proc *the_tree_proc);
static void fix_symtabs(base_symtab *the_symtab);
static void fix_symtab(base_symtab *the_symtab);
static boolean is_complex(type_node *the_type);
static type_node *complex_replacement(type_node *complex_type);
static void replace_complex_in_type(type_node *the_type);
static void fix_complex_refs(tree_proc *the_proc);
static void fix_complex_on_tree_node(tree_node *the_node, void *);
static void fix_complex_on_instr(instruction *the_instr, void *);
static void drop_last_field_name(instruction *the_instr);
static void fix_complex_store(in_rrr *the_store);
static operand make_re_evalable(operand the_op, tree_node *place);
static operand simplify_address(operand old_address, type_node *new_type,
                                const char *name);
static boolean is_simple_var_addr(operand the_op, var_sym **the_var);
static const char *new_field_name(const char *desired_name, 
				  struct_type *the_struct);
static int biggest_char_array_at_offset(struct_type *the_struct, int offset);
static type_node *original_op_type(operand the_op);
static void mark_common_blocks(base_symtab *the_symtab);
static void fix_defs(suif_object *the_object);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    int remainder = INT_MAX;
    max_int_str_len = 1;
    while (remainder >= 10)
      {
        remainder /= 10;
        ++max_int_str_len;
      }
    start_suif(argc, argv);

    ANNOTE(k_fixfortran_needed_aux,       "fixfortran needed aux",
           FALSE);
    ANNOTE(k_fixfortran_fixed_array_type, "fixfortran fixed array type",
           FALSE);
    k_fixfortran_original_type =
            lexicon->enter("fixfortran original type")->sp;
    ANNOTE(k_io_read,  "io read",  TRUE);
    ANNOTE(k_io_write, "io write", TRUE);

    if (argc < 2)
        error_line(1, NULL, "no file specifications given");
    else if (argc == 2)
        error_line(1, NULL, "no output file specification given");
    else if (argc > 3)
        error_line(1, NULL, "too many file specifications given");

    fileset->add_file(argv[1], argv[2]);
    mark_intrinsics(fileset->globals());
    fix_symtab(fileset->globals());

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        mark_constants(fse->symtab());
        fix_symtab(fse->symtab());
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            do_proc(this_proc_sym->block());
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
        mark_common_blocks(fse->symtab());
      }

    mark_common_blocks(fileset->globals());

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        walk(fse->symtab(), &fix_defs);
      }
    walk(fileset->globals(), &fix_defs);

    delete fileset;

    if (errors)
        return 1;
    else
        return 0;
  }

/*
 *  This takes an operand of the form
 *
 *    <expr>
 *
 *  and turns it into the form
 *
 *    &(<expr>[const])
 *
 *  The result is the same memory address plus const times the size, but the
 *  type changes from a pointer to an array to a pointer to the element type
 *  of the array.  If the original expression is an array reference itself
 *  and has no "fields" annotation or iteger offset, the constant array
 *  reference is tacked onto the end of the existing reference.
 *
 *  Restrictions: The type of the original expression must be a pointer to
 *  an array with lower bound of zero (i.e. a C array).
 */
extern in_array *add_const_aref(operand array_pointer, int constant)
  {
    assert(array_pointer.type()->unqual()->is_ptr());
    ptr_type *the_ptr_type = (ptr_type *)(array_pointer.type()->unqual());
    assert(the_ptr_type->ref_type()->unqual()->is_array());
    array_type *the_array_type =
            (array_type *)(the_ptr_type->ref_type()->unqual());

    type_node *new_type = the_array_type->elem_type()->ptr_to();

    if (array_pointer.is_expr())
      {
        instruction *the_instr = array_pointer.instr();
        if ((the_instr->opcode() == io_array) &&
            (the_instr->peek_annote(k_fields) == NULL))
          {
            in_array *the_array = (in_array *)the_instr;
            if (the_array->offset() == 0)
              {
                int num_elems = the_array->elem_size() /
                                the_array_type->elem_type()->size();
                unsigned num_dims = the_array->dims();
                ++num_dims;
                the_array->set_dims(num_dims);
                the_array->set_index(num_dims - 1,
                                     const_op(immed(constant), type_ptr_diff));
                the_array->set_bound(num_dims - 1,
                                     const_op(immed(num_elems),
                                              type_ptr_diff));
                the_array->set_elem_size(the_array_type->elem_type()->size());
                operand offset_operand = the_array->offset_op();
                if (!offset_operand.is_null())
                  {
                    offset_operand.remove();
                    the_array->set_offset_op(offset_operand * num_elems);
                  }
                the_array->set_result_type(new_type);
                return the_array;
              }
          }
      }

    in_array *new_array =
            new in_array(new_type, operand(), array_pointer,
                          the_array_type->elem_type()->size(), 1, 0,
                          operand());
    new_array->set_index(0, const_op(immed(constant), type_ptr_diff));
    new_array->set_bound(0, operand());
    return new_array;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void mark_intrinsics(global_symtab *the_symtab)
  {
    static char *pure_intrinsic_names[] =
      {
        "sin",
        "F_sin",
        "cos",
        "F_cos",
        "tan",
        "F_tan",
        "asin",
        "F_asin",
        "acos",
        "F_acos",
        "atan2",
        "F_atan2",
        "atan",
        "F_atan",
        "sinh",
        "cosh",
        "tanh",
        "log",
        "F_log",
        "sqrt",
        "F_sqrt",
        "exp",
        "F_exp",
        "r_lg10",
        "d_lg10",
        "i_sign",
        "r_sign",
        "d_sign",
        "h_mod",
        "i_mod",
        "r_mod",
        "d_mod",
        "i_nint",
        "i_dnnt",
        "r_nint",
        "d_nint",
        "r_int",
        "d_int",
        "pow_ii",
        "pow_ri",
        "pow_di",
        "pow_ci",
        "pow_zi",
        "pow_hh",
        "pow_dd",
        "pow_zz",
        "c_div",
        "z_div",
        "s_copy",
        "s_cmp",
        "s_cat",
        "r_imag",
        "d_imag",
        "r_dim",
        "sc_abs",
        "sz_abs",
        NULL
      };

    static char *intrinsic_fortran_names[] =
      {
        "DSIN", /* sin */
        "DSIN", /* F_sin */
        "DCOS", /* cos */
        "DCOS", /* F_cos */
        "DTAN", /* tan */
        "DTAN", /* F_tan */
        "DASIN", /* asin */
        "DASIN", /* F_asin */
        "DACOS", /* acos */
        "DACOS", /* F_acos */
        "DATAN2", /* atan2 */
        "DATAN2", /* F_atan2 */
        "DATAN", /* atan */
        "DATAN", /* F_atan */
        "DSINH", /* sinh */
        "DCOSH", /* cosh */
        "DTANH", /* tanh */
        "DLOG", /* log */
        "DLOG", /* F_log */
        "DSQRT", /* sqrt */
        "DSQRT", /* F_sqrt */
        "DEXP", /* exp */
        "DEXP", /* F_exp */
        "ALOG10", /* r_lg10 */
        "DLOG10", /* d_lg10 */
        "ISIGN", /* i_sign */
        "SIGN", /* r_sign */
        "DSIGN", /* d_sign */
        NULL, /* h_mod */
        "MOD", /* i_mod */
        "AMOD", /* r_mod */
        "DMOD", /* d_mod */
        "NINT", /* i_nint */
        "IDNINT", /* i_dnnt */
        "ANINT", /* r_nint */
        "DNINT", /* d_nint */
        "AINT", /* r_int */
        "DINT", /* d_int */
        "**", /* pow_ii */
        "**", /* pow_ri */
        "**", /* pow_di */
        "**", /* pow_ci */
        "**", /* pow_zi */
        "**", /* pow_hh */
        "**", /* pow_dd */
        "**", /* pow_zz */
        NULL, /* c_div */
        NULL, /* z_div */
        NULL, /* s_copy */
        NULL, /* s_cmp */
        NULL, /* s_cat */
        "AIMAG", /* r_imag */
        NULL, /* d_imag */
        "DIM", /* r_dim */
        NULL, /* sc_abs */
        NULL, /* sz_abs */
        NULL
      };

    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_symbol = sym_iter.step();
        if (this_symbol->is_proc())
          {
            char **follow = &(pure_intrinsic_names[0]);
            unsigned long counter = 0;
            while (*follow != NULL)
              {
                if (strcmp(this_symbol->name(), *follow) == 0)
                  {
                    this_symbol->append_annote(k_pure_function);
                    char *fortran_name = intrinsic_fortran_names[counter];
                    if (fortran_name != NULL)
                      {
                        if (strcmp(fortran_name, "**") == 0)
                          {
                            this_symbol->append_annote(k_fortran_power_op);
                          }
                        else
                          {
                            immed_list *data =
                                    new immed_list(immed(fortran_name));
                            this_symbol->append_annote(k_fortran_intrinsic,
                                                       data);
                          }
                      }
                    break;
                  }
                ++follow;
                ++counter;
              }
          }
      }
  }

static void do_proc(tree_proc *the_tree_proc)
  {
    if (the_tree_proc->proc()->src_lang() == src_fortran)
      {
        error_line(0, the_tree_proc,
                   "function %s has already been converted to fortran",
                   the_tree_proc->proc()->name());
        errors = TRUE;
        return;
      }

    de_linearize(the_tree_proc);
    fix_symtabs(the_tree_proc->proc_syms());
    mark_params_call_by_ref(the_tree_proc);
    fix_complex_refs(the_tree_proc);
    mark_io_on_list(the_tree_proc->body());
    limit_complex_temp_scopes(the_tree_proc->body());
    walk(the_tree_proc, &fix_defs);

    the_tree_proc->proc()->append_annote(k_no_recursion);
    the_tree_proc->proc()->set_src_lang(src_fortran);
  }

static void de_linearize(tree_proc *the_proc)
  {
    assert(the_proc != NULL);

    if (the_proc->body() == NULL)
        return;

    aux_var_values = new alist;
    array_types = new alist;

    tree_node_list_iter body_iter(the_proc->body());
    while (!body_iter.is_empty())
      {
        tree_node *this_node = body_iter.step();
        assert(this_node != NULL);

        if (this_node->is_instr())
          {
            tree_instr *this_tree_instr = (tree_instr *)this_node;
            instruction *this_instr = this_tree_instr->instr();
            assert(this_instr != NULL);
            operand destination = this_instr->dst_op();
            if (destination.is_symbol())
              {
                var_sym *this_var = destination.symbol();
                assert(this_var != NULL);
                if ((strstr(this_var->name(), "_offset") != NULL)  ||
                    (strstr(this_var->name(), "_dim") != NULL)  ||
                    (strstr(this_var->name(), "_lb") != NULL)  ||
                    (strstr(this_var->name(), "_ub") != NULL) ||
                    (strstr(this_var->name(), "_strlen") != NULL))
                  {
                    if (aux_var_values->exists(this_var))
                      {
                        /*
                         * It can happen that one of these array linearization
                         * auxiliary variables is initialized more than once:
                         * f2c writes a ``prolog'' that initializes these
                         * things and it writes one prolog for every entry
                         * point.  It would only need to do the part of the
                         * prolog concerning parameters for each entry point,
                         * but it doesn't, it simply produces an entire prolog
                         * for each.  That means auxiliaries for things common
                         * to all entry points (most locals) are repeated.
                         * This still produces correct code, it's just
                         * redundant.  Since f2c writes all the prologs before
                         * the switch to branch to the label for the particular
                         * entry point, all the prologs are executed for every
                         * entry point.  Hence all the auxiliary variables that
                         * are written multiple times must be given the same
                         * values or else it would be wrong for some entry
                         * point.  So when we can just ignore the redundant
                         * assignment at this point.
                         */
                        continue;
                      }

                    aux_var_values->enter(this_var, this_tree_instr);
                  }
              }
          }
      }

    alist vars_to_delete;

    tree_node_list_iter base_body_iter(the_proc->body());
    while (!base_body_iter.is_empty())
      {
        tree_node *this_node = base_body_iter.step();
        assert(this_node != NULL);

        if (this_node->is_instr())
          {
            tree_instr *this_tree_instr = (tree_instr *)this_node;
            instruction *this_instr = this_tree_instr->instr();
            assert(this_instr != NULL);
            operand destination = this_instr->dst_op();
            if (destination.is_symbol())
              {
                var_sym *this_var = destination.symbol();
                assert(this_var != NULL);
                boolean is_a_base = FALSE;
                if (strstr(this_var->name(), "_base") != NULL)
                  {
                    fix_array_type_for_base_expr(this_instr);
                    is_a_base = TRUE;
                  }
                else if (strstr(this_var->name(), "_strbase") != NULL)
                  {
                    fix_array_type_for_str_base_expr(this_instr);
                    is_a_base = TRUE;
                  }

                if (is_a_base)
                  {
                    tree_node_list_e *this_elem = this_tree_instr->list_e();
                    the_proc->body()->remove(this_elem);
                    delete this_elem;
                    delete this_tree_instr;

                    if (!vars_to_delete.exists(this_var))
                        vars_to_delete.enter(this_var, this_var);
                  }
              }
          }
      }

    the_proc->map(&fix_arrays_on_node, NULL);

    alist_iter value_iter(aux_var_values);
    while (!value_iter.is_empty())
      {
        alist_e *this_alist_e = value_iter.step();
        assert(this_alist_e != NULL);
        aux_var_values->remove(this_alist_e);
        delete this_alist_e;
      }
    delete aux_var_values;
    aux_var_values = NULL;

    tree_node_list_iter del_body_iter(the_proc->body());
    while (!del_body_iter.is_empty())
      {
        tree_node *this_node = del_body_iter.step();
        assert(this_node != NULL);

        if (this_node->is_instr())
          {
            tree_instr *this_tree_instr = (tree_instr *)this_node;
            instruction *this_instr = this_tree_instr->instr();
            assert(this_instr != NULL);
            operand destination = this_instr->dst_op();
            if (destination.is_symbol())
              {
                var_sym *this_var = destination.symbol();
                assert(this_var != NULL);
                if ((strstr(this_var->name(), "_offset") != NULL) ||
                    (strstr(this_var->name(), "_dim") != NULL) ||
                    (strstr(this_var->name(), "_lb") != NULL) ||
                    (strstr(this_var->name(), "_ub") != NULL) ||
                    (strstr(this_var->name(), "_strlen") != NULL))
                  {
                    if (this_var->peek_annote(k_fixfortran_needed_aux) == NULL)
                      {
                        tree_node_list_e *this_elem =
                                this_tree_instr->list_e();
                        the_proc->body()->remove(this_elem);
                        delete this_elem;
                        delete this_tree_instr;

                        if (!vars_to_delete.exists(this_var))
                            vars_to_delete.enter(this_var, this_var);
                      }
                  }
              }
          }
      }

    alist_iter to_delete_iter(&vars_to_delete);
    while (!to_delete_iter.is_empty())
      {
        alist_e *this_alist_e = to_delete_iter.step();
        assert(this_alist_e != NULL);

        var_sym *this_var = (var_sym *)this_alist_e->key;
        assert(this_var != NULL);

        this_var->parent()->remove_sym(this_var);
        delete this_var;

        vars_to_delete.remove(this_alist_e);
        delete this_alist_e;
      }

    alist_iter type_iter(array_types);
    while (!type_iter.is_empty())
      {
        alist_e *this_alist_e = type_iter.step();
        assert(this_alist_e != NULL);
        array_types->remove(this_alist_e);
        delete this_alist_e;
      }
    delete array_types;
  }

/*
 * This function takes as an argument an instruction of the form
 *
 *     <str>_base = (int) &(<base_expr>[0])
 *
 * where <str> is a string (the name of the variable in Fortran) and
 * <base_expr> is the C expression for the base of the array.  This
 * function uses <str> to find the auxiliary variables for the array
 * type and sets the type of <base_expr> to that type if <base_expr>'s
 * type comes from something like a variable or structure member that
 * can be changed so that other occurances of <base_expr> will have
 * that type.
 *
 * This has the effect of restoring the types of arrays that are local
 * variables, parameters, or elements of common blocks.
 */
void fix_array_type_for_base_expr(instruction *the_instr)
  {
    assert(the_instr != NULL);

    assert(the_instr->dst_op().is_symbol());
    var_sym *base_aux_var_sym = the_instr->dst_op().symbol();
    const char *base_aux_var_name = base_aux_var_sym->name();

    char *suffix = strstr(base_aux_var_name, "_base");
    assert(suffix != NULL);

    if (strcmp(suffix, "_base") != 0)
      {
        error_line(0, the_instr,
                   "variable name contains ``_base'' but not as a suffix;");
        error_line(0, the_instr,
                   "reconstruction of array types from auxiliary variables "
                   "failed");
        errors = TRUE;
        return;
      }

    char *old_storage = new char[suffix - base_aux_var_name + 1];
    strncpy(old_storage, base_aux_var_name, suffix - base_aux_var_name);
    old_storage[suffix - base_aux_var_name] = 0;
    const char *aux_base_name = old_storage;
    aux_base_name = lexicon->enter(aux_base_name)->sp;
    delete[] old_storage;

    operand aref_base_op(the_instr);
    while (aref_base_op.is_instr() &&
           ((aref_base_op.instr()->opcode() == io_cvt) ||
            (aref_base_op.instr()->opcode() == io_cpy)))
      {
        in_rrr *the_rrr = (in_rrr *)(aref_base_op.instr());
        aref_base_op = the_rrr->src_op();
      }

    if (aref_base_op.is_instr() &&
        (aref_base_op.instr()->opcode() == io_array))
      {
        in_array *the_aref = (in_array *)(aref_base_op.instr());
        aref_base_op = the_aref->base_op();
      }

    type_node *base_type = aref_base_op.type()->unqual();
    if (!base_type->is_ptr())
      {
        error_line(0, the_instr->parent(),
                   "bad format for `%s_base' auxiliary expression;",
                   aux_base_name);
        error_line(0, the_instr->parent(),
                   "reconstruction of array types from auxiliary "
                   "variables failed");
        errors = TRUE;
        return;
      }
    ptr_type *the_pointer = (ptr_type *)base_type;

    type_node *element_type = the_pointer->ref_type()->unqual();

    boolean is_param = FALSE;
    var_sym *param_var = NULL;

    while (aref_base_op.is_instr() &&
           (aref_base_op.instr()->opcode() == io_cvt))
      {
        in_rrr *the_cvt = (in_rrr *)(aref_base_op.instr());
        aref_base_op = the_cvt->src_op();
      }

    if (aref_base_op.is_symbol())
      {
        var_sym *the_var = aref_base_op.symbol();
        if (the_var->is_param())
          {
            is_param = TRUE;
            param_var = the_var;
          }
      }

    if (aref_base_op.is_instr() && (aref_base_op.instr()->opcode() == io_ldc))
      {
        in_ldc *the_ldc = (in_ldc *)(aref_base_op.instr());
        immed value = the_ldc->value();
        if (value.is_symbol() && value.symbol()->is_var() &&
            (value.offset() == 0))
          {
            var_sym *the_var = (var_sym *)(value.symbol());
            if (the_var->is_param())
              {
                is_param = TRUE;
                param_var = the_var;
                assert(element_type->is_ptr());
                ptr_type *element_ptr = (ptr_type *)element_type;
                element_type = element_ptr->ref_type()->unqual();
              }
          }
      }

    if (element_type->is_array())
      {
        array_type *the_array_type = (array_type *)element_type;
        element_type = the_array_type->elem_type()->unqual();
      }

    if ((element_type->op() == TYPE_INT) &&
        (element_type->size() == target.size[C_char]))
      {
        element_type = string_type_for_base_name(aux_base_name,
                                                 base_aux_var_sym->parent());
      }

    type_node *new_type =
            register_type_for_base_name(aux_base_name, element_type,
                                        base_aux_var_sym->parent());

    if (is_param)
      {
        param_var->set_type(new_type->ptr_to());
        return;
      }

    operand new_op =
            simplify_address(aref_base_op.clone(), new_type, aux_base_name);
    if (new_op.is_expr())
        delete new_op.instr();
  }

/*
 * This function takes as an argument an instruction of the form
 *
 *     <str>_strbase = (int) <base_expr>
 *
 * where <str> is a string (the name of the variable in Fortran) and
 * <base_expr> is the C expression for the location of the string or
 * array of strings.  This function first checks for a <str>_base
 * auxiliary variable, and if it finds one, it returns without doing
 * anything because this means it is really an array of strings and it
 * will be handled by the array handling code in
 * fix_array_type_for_base_expr().  Otherwise, it uses <str> to find
 * the auxiliary variable for the length and sets the type of
 * *<base_expr> to a character array from one to that length if
 * *<base_expr>'s type comes from something like a variable or
 * structure member that can be changed so that other occurances of
 * *<base_expr> will have that type.
 *
 * This has the effect of restoring the types of strings that are
 * local variables, parameters, or elements of common blocks.
 */
void fix_array_type_for_str_base_expr(instruction *the_instr)
  {
    assert(the_instr != NULL);

    assert(the_instr->dst_op().is_symbol());
    var_sym *base_aux_var_sym = the_instr->dst_op().symbol();
    const char *base_aux_var_name = base_aux_var_sym->name();

    char *suffix = strstr(base_aux_var_name, "_strbase");
    assert(suffix != NULL);

    if (strcmp(suffix, "_strbase") != 0)
      {
        error_line(0, the_instr,
                   "variable name contains ``_strbase'' but not as a suffix;");
        error_line(0, the_instr,
                   "reconstruction of string types from auxiliary variables "
                   "failed");
        errors = TRUE;
        return;
      }

    char *old_storage = new char[suffix - base_aux_var_name + 1];
    strncpy(old_storage, base_aux_var_name, suffix - base_aux_var_name);
    old_storage[suffix - base_aux_var_name] = 0;
    const char *aux_base_name = old_storage;
    aux_base_name = lexicon->enter(aux_base_name)->sp;
    delete[] old_storage;

    char *aux_array_base_name = new char[strlen(aux_base_name) + 8];
    strcpy(aux_array_base_name, aux_base_name);
    strcat(aux_array_base_name, "_base");
    var_sym *aux_array_base_var =
            base_aux_var_sym->parent()->lookup_var(aux_array_base_name);
    delete[] aux_array_base_name;

    if (aux_array_base_var != NULL)
        return;

    operand aref_base_op(the_instr);
    while (aref_base_op.is_instr() &&
           ((aref_base_op.instr()->opcode() == io_cvt) ||
            (aref_base_op.instr()->opcode() == io_cpy)))
      {
        in_rrr *the_rrr = (in_rrr *)(aref_base_op.instr());
        aref_base_op = the_rrr->src_op();
      }

    if (aref_base_op.is_instr() &&
        (aref_base_op.instr()->opcode() == io_array))
      {
        in_array *the_aref = (in_array *)(aref_base_op.instr());
        aref_base_op = the_aref->base_op();
      }

    boolean is_param = FALSE;
    var_sym *param_var = NULL;

    while (aref_base_op.is_instr() &&
           (aref_base_op.instr()->opcode() == io_cvt))
      {
        in_rrr *the_cvt = (in_rrr *)(aref_base_op.instr());
        aref_base_op = the_cvt->src_op();
      }

    if (aref_base_op.is_symbol())
      {
        var_sym *the_var = aref_base_op.symbol();
        if (the_var->is_param())
          {
            is_param = TRUE;
            param_var = the_var;
          }
      }

    if (aref_base_op.is_instr() && (aref_base_op.instr()->opcode() == io_ldc))
      {
        in_ldc *the_ldc = (in_ldc *)(aref_base_op.instr());
        immed value = the_ldc->value();
        if (value.is_symbol() && value.symbol()->is_var() &&
            (value.offset() == 0))
          {
            var_sym *the_var = (var_sym *)(value.symbol());
            if (the_var->is_param())
              {
                is_param = TRUE;
                param_var = the_var;
              }
          }
      }

    type_node *new_type =
            string_type_for_base_name(aux_base_name,
                                      base_aux_var_sym->parent());

    if (is_param)
      {
        param_var->append_annote(k_fixfortran_original_type,
                                 param_var->type());
        param_var->set_type(new_type->ptr_to());
        return;
      }

    operand new_op =
            simplify_address(aref_base_op.clone(), new_type, aux_base_name);
    if (new_op.is_expr())
        delete new_op.instr();
  }

static void aux_sub_on_instr(instruction *the_instr, void *)
  {
    assert(the_instr != NULL);

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_operand = the_instr->src_op(src_num);
        if (this_operand.is_symbol())
          {
            var_sym *this_var = this_operand.symbol();
            alist_e *the_alist_e = aux_var_values->search(this_var);
            if (the_alist_e != NULL)
              {
                tree_instr *old_tree_instr = (tree_instr *)(the_alist_e->info);
                instruction *old_instr = old_tree_instr->instr();
                assert(old_instr != NULL);

                instruction *new_instr =
                        old_instr->clone(the_instr->parent()->scope());
                new_instr->set_dst(operand());
                the_instr->set_src_op(src_num, operand(new_instr));
              }
          }
      }

    /*
     * sf2c sometimes passes the location of one of these, such as &x_dim1, to
     * an I/O routine.  So if it's location is needed, we'd better not delete
     * it.
     */
    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        immed value = the_ldc->value();
        if (value.is_symbol())
          {
            sym_node *the_symbol = value.symbol();
            if (aux_var_values->search(the_symbol) != NULL)
              {
                the_symbol->append_annote(k_fixfortran_needed_aux,
                                          new immed_list);
              }
          }
      }
  }

static void fix_arrays_on_node(tree_node *the_node, void *)
  {
    assert(the_node != NULL);

    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    fix_arrays_on_instr(the_tree_instr->instr());
    the_tree_instr->instr_map(&fix_addresses, NULL, FALSE);
    the_tree_instr->instr_map(&inline_intrinsics_on_instr, NULL, FALSE);
    the_tree_instr->instr_map(&aux_sub_on_instr, NULL);
  }

static void fix_arrays_on_instr(instruction *the_instr)
  {
    assert(the_instr != NULL);

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
        fix_arrays_on_operand(the_instr->src_op(src_num));

    if (the_instr->opcode() != io_array)
        return;

    in_array *old_array = (in_array *)the_instr;
    if (old_array->dims() != 1)
      {
        error_line(0, the_instr->parent(),
                   "array reference with %d dimensions found",
                   old_array->dims());
        errors = TRUE;
        return;
      }

    char *base_var_name = guess_base_var_name(old_array);
    if (base_var_name == NULL)
      {
        /*
         *  If we can't find the base name, then this is not in the form of
         *  an array reference translated from Fortran by sf2c.  So assume
         *  that it is a C array reference to an sf2c generated array.  Such
         *  C arrays are generated to translate complicated Fortran internals
         *  such as string operations and I/O.
         */

        operand base_operand = old_array->base_op();
        base_operand.remove();
        assert(base_operand.type()->is_ptr());
        ptr_type *base_ptr = (ptr_type *)(base_operand.type());
        base_operand =
                simplify_address(base_operand, base_ptr->ref_type()->unqual(),
                                 NULL);
        old_array->set_base_op(base_operand);

        return;
      }

    type_node *base_type = array_type_from_aref(old_array);
    if (base_type == NULL)
        return;

    if (base_type->is_array())
      {
        array_type *the_array = (array_type *)base_type;
        base_type = the_array->elem_type();
      }

    base_symtab *the_symtab = the_instr->owner()->scope();

    unsigned num_dimensions;
    array_type *new_type =
            array_type_for_base_name(base_var_name, base_type,
                                     &num_dimensions);

    char *auxiliary_name =
            new char[strlen(base_var_name) + max_int_str_len + 5];
    strcpy(auxiliary_name, base_var_name);
    strcat(auxiliary_name, "_dim");
    char *number_place = auxiliary_name + strlen(base_var_name) + 4;

    operand offset_operand = old_array->offset_op();
    offset_operand.remove();
    if (!is_zero(offset_operand))
      {
        error_line(0, the_instr->parent(),
                   "Fortran array already contains a non-zero offset");
        errors = TRUE;
      }
    deallocate_operand(offset_operand);

    char *offset_var_name = new char[strlen(base_var_name) + 8];
    strcpy(offset_var_name, base_var_name);
    strcat(offset_var_name, "_offset");
    var_sym *offset_var = the_symtab->lookup_var(offset_var_name);

    if (offset_var == NULL)
      {
        error_line(0, the_instr->parent(),
                     "unable to find offset variable %s", offset_var_name);
        errors = TRUE;
      }
    delete[] offset_var_name;

    delete[] base_var_name;

    operand old_bound = old_array->bound(0);
    old_bound.remove();
    deallocate_operand(old_bound);

    operand base_operand = old_array->base_op();
    base_operand.remove();

    base_operand = simplify_address(base_operand, new_type, NULL);

    operand remaining_index = old_array->index(0);
    remaining_index.remove();

    offset_operand = build_offset_operand(new_type, num_dimensions);

    in_array *new_array = new in_array(old_array->result_type(), operand(),
                                       base_operand, old_array->elem_size(),
                                       num_dimensions, old_array->offset(),
                                       operand());
    new_array->annotes()->append(old_array->annotes());

    while (remaining_index.is_instr() &&
           (remaining_index.instr()->opcode() == io_cvt))
      {
        in_rrr *the_cvt = (in_rrr *)(remaining_index.instr());
        remaining_index = the_cvt->src_op();
      }

    /* skip multiplication done for character strings */
    if (remaining_index.is_instr() &&
        (remaining_index.instr()->opcode() == io_mul))
      {
        in_rrr *the_div = (in_rrr *)(remaining_index.instr());
        remaining_index = the_div->src1_op();
      }

    if (!is_subtracted(remaining_index, offset_var, FALSE))
      {
        error_line(0, the_instr->parent(),
                   "offset for Fortran array not subtracted");
        errors = TRUE;
      }
    remaining_index = remove_subtracted_variable(remaining_index, offset_var,
                                                 FALSE);
    unsigned dimension_num;
    for (dimension_num = 0; dimension_num + 1 < num_dimensions;
         ++dimension_num)
      {
        sprintf(number_place, "%u", dimension_num + 1);
        var_sym *bound_var = the_symtab->lookup_var(auxiliary_name);

        if (bound_var == NULL)
          {
            error_line(0, the_instr->parent(),
                       "unable to find bound variable %s", auxiliary_name);
            errors = TRUE;
          }

        operand op_a, op_b;
        linear_form(&op_a, &op_b, remaining_index, bound_var);
        remaining_index = op_b;
        new_array->set_index(num_dimensions - (dimension_num + 1), op_a);
      }

    new_array->set_index(0, remaining_index);

    delete[] auxiliary_name;

    type_node *follow_type = new_type;
    dimension_num = 0;
    while (dimension_num < num_dimensions)
      {
        assert(follow_type != NULL);
        assert(follow_type->is_array());
        array_type *follow_array = (array_type *)follow_type;

        array_bound upper_bound = follow_array->upper_bound();
        array_bound lower_bound = follow_array->lower_bound();
        operand new_bound;
        if (upper_bound.is_unknown() || lower_bound.is_unknown())
          {
            new_bound = operand();
          }
        else
          {
            new_bound = (operand_from_array_bound(upper_bound) -
                         operand_from_array_bound(lower_bound));
            new_bound = (new_bound + const_op(immed(1), new_bound.type()));
          }
        new_array->set_bound(dimension_num, new_bound);

        follow_type = follow_array->elem_type()->unqual();
        ++dimension_num;
      }

    /* handle the case of arrays of character strings */
    if (follow_type->is_array())
      {
        array_type *string_array = (array_type *)follow_type;
        if (string_array->elem_type()->unqual()->is_same(type_char))
          {
            new_array->set_dims(num_dimensions + 1);
            new_array->set_elem_size(type_char->size());

            operand upper_op =
                    operand_from_array_bound(string_array->upper_bound());
            operand lower_op =
                    operand_from_array_bound(string_array->lower_bound());
            operand difference = ((upper_op - lower_op.clone()) + 1);

            offset_operand *= difference.clone();
            offset_operand += lower_op.clone();

            new_array->set_index(num_dimensions, lower_op);
            new_array->set_bound(num_dimensions, difference);
          }
      }

    new_array->set_offset_op(offset_operand);

    replace_instruction(old_array, new_array);
    delete old_array;
  }

static void fix_arrays_on_operand(operand the_operand)
  {
    if (!the_operand.is_expr())
        return;

    fix_arrays_on_instr(the_operand.instr());
  }

static array_type *array_type_from_aref(in_array *the_aref)
  {
    type_node *base_type = the_aref->base_op().type()->unqual();
    if (base_type->is_ptr())
      {
        ptr_type *the_pointer = (ptr_type *)base_type;
        base_type = the_pointer->ref_type()->unqual();
        if (!base_type->is_array())
          {
            error_line(0, the_aref->parent(),
                       "base of array reference instruction is not a pointer"
                       " to an array");
            errors = TRUE;
            return NULL;
          }
        return (array_type *)base_type;
      }
    else
      {
        error_line(0, the_aref->parent(),
                   "base of array reference instruction is not a pointer");
        errors = TRUE;
        return NULL;
      }
  }

static array_type *register_type_for_base_name(const char *base_name,
                                               type_node *element_type,
                                               base_symtab *the_symtab)
  {
    assert(base_name != NULL);
    assert(element_type != NULL);

    alist_iter type_iter(array_types);
    while (!type_iter.is_empty())
      {
        alist_e *this_alist_e = type_iter.step();
        assert(this_alist_e != NULL);
        if (strcmp((char *)(this_alist_e->key), base_name) == 0)
          {
            /*
             *  This happens whenever there are multiple entry points
             *  for a function -- sf2c repeats all the declarations
             *  once for each entry point.
             */
            unsigned dummy;
            return array_type_for_base_name(base_name, element_type, &dummy);
          }
      }

    base_symtab *new_symtab = element_type->parent();

    char *aux_lb_name = new char[strlen(base_name) + max_int_str_len + 4];
    strcpy(aux_lb_name, base_name);
    strcat(aux_lb_name, "_lb");
    char *lb_num_place = aux_lb_name + strlen(base_name) + 3;

    char *aux_ub_name = new char[strlen(base_name) + max_int_str_len + 4];
    strcpy(aux_ub_name, base_name);
    strcat(aux_ub_name, "_ub");
    char *ub_num_place = aux_ub_name + strlen(base_name) + 3;

    type_node *result = element_type;
    int dim_count = 0;
    while (TRUE)
      {
        sprintf(lb_num_place, "%d", dim_count + 1);
        var_sym *lb_var = the_symtab->lookup_var(aux_lb_name);
        if (lb_var == NULL)
            break;

        ++dim_count;

        array_type *new_type = new array_type(result);
        result = new_type;

        array_bound new_lower = bound_from_aux(lb_var);
        new_type->set_lower_bound(new_lower);
        if (new_lower.is_variable())
          {
            new_symtab = joint_symtab(new_symtab,
                                      new_lower.variable()->parent());
          }

        sprintf(ub_num_place, "%d", dim_count);
        var_sym *ub_var = the_symtab->lookup_var(aux_ub_name);
        if (ub_var == NULL)
          {
            new_type->set_upper_bound(array_bound());
            break;
          }

        array_bound new_upper = bound_from_aux(ub_var);
        new_type->set_upper_bound(new_upper);
        if (new_upper.is_variable())
          {
            new_symtab = joint_symtab(new_symtab,
                                      new_upper.variable()->parent());
          }
      }

    if (dim_count == 0)
      {
        error_line(0, NULL, "cannot find dimension variable %s", aux_lb_name);
        errors = TRUE;
        result = new array_type(element_type);
      }

    delete[] aux_lb_name;
    delete[] aux_ub_name;

    assert(new_symtab != NULL);
    array_type *installed_result =
            (array_type *)(new_symtab->install_type(result));
    array_types->enter(lexicon->enter(base_name)->sp, installed_result);
    return installed_result;
  }

static array_type *array_type_for_base_name(const char *base_name,
                                            type_node *base_type,
                                            unsigned *num_dimensions)
  {
    assert(base_name != NULL);
    assert(base_type != NULL);
    assert(num_dimensions != NULL);

    alist_iter type_iter(array_types);
    while (!type_iter.is_empty())
      {
        alist_e *this_alist_e = type_iter.step();
        assert(this_alist_e != NULL);
        if (strcmp((char *)(this_alist_e->key), base_name) == 0)
          {
            array_type *result = (array_type *)(this_alist_e->info);
            int dim_count = 0;
            type_node *follow_type = result;
            while (!follow_type->is_same(base_type))
              {
                ++dim_count;
                assert(follow_type != NULL);
                if (!follow_type->is_array())
                  {
                    error_line(0, NULL,
                               "array with base name \"%s\" used with "
                               "conflicting element types", base_name);
                    errors = TRUE;
                    break;
                  }
                array_type *follow_array = (array_type *)follow_type;
                follow_type = follow_array->elem_type();
              }
            if (base_type->unqual()->is_same(type_char))
                --dim_count;
            *num_dimensions = dim_count;
            return result;
          }
      }

    error_line(0, NULL, "cannot find declaration statement for array `%s'",
               base_name);
    errors = TRUE;

    *num_dimensions = 1;
    array_type *result = new array_type(base_type);

    return (array_type *)(base_type->parent()->install_type(result));
  }

static type_node *string_type_for_base_name(const char *base_name,
                                            base_symtab *the_symtab)
  {
    char *aux_name = new char[strlen(base_name) + 8];
    strcpy(aux_name, base_name);
    strcat(aux_name, "_strlen");
    var_sym *aux_var = the_symtab->lookup_var(aux_name);
    delete[] aux_name;

    if (aux_var == NULL)
        return type_char;

    array_bound new_upper = bound_from_aux(aux_var);
    type_node *result =
            new array_type(type_char, array_bound(1), new_upper);

    base_symtab *result_symtab;
    if (new_upper.is_variable())
        result_symtab = new_upper.variable()->parent();
    else
        result_symtab = fileset->globals();
    return result_symtab->install_type(result);
  }

static array_bound bound_from_aux(var_sym *the_var)
  {
    alist_e *the_alist_e = aux_var_values->search(the_var);
    if (the_alist_e == NULL)
      {
        error_line(0, NULL, "cannot find value of auxiliary variable %s",
                   the_var->name());
        errors = TRUE;
        return array_bound();
      }

    tree_instr *the_tree_instr = (tree_instr *)(the_alist_e->info);
    instruction *the_instr = the_tree_instr->instr();
    assert(the_instr != NULL);

    array_bound result;
    immed value;
    eval_status return_code = evaluate_const_instr(the_instr, &value);
    if ((return_code == EVAL_OK) && value.is_integer())
        result = array_bound(value.integer());

    if (result.is_unknown())
      {
        the_var->append_annote(k_fixfortran_needed_aux, new immed_list);
        result = array_bound(the_var);
      }

    return result;
  }

static void fix_addresses(instruction *the_instr, void *)
  {
    unsigned first_src, last_src;
    boolean is_fio = FALSE;
    boolean is_memop = FALSE;
    switch (the_instr->opcode())
      {
        case io_lod:
            first_src = 0;
            last_src = 0;
            is_memop = TRUE;
            break;
        case io_str:
        case io_memcpy:
            first_src = 0;
            last_src = 1;
            is_memop = TRUE;
            break;
        case io_cal:
          {
            first_src = 1;
            last_src = the_instr->num_srcs() - 1;
            proc_sym *call_sym = proc_for_call((in_cal *)the_instr);
            if ((call_sym != NULL) &&
                ((strcmp(call_sym->name(), "do_fio") == 0) ||
                 (strcmp(call_sym->name(), "do_lio") == 0) ||
                 (strcmp(call_sym->name(), "do_uio") == 0)))
              {
                is_fio = TRUE;
              }
            break;
          }
        default:
            return;
      }

    for (unsigned src_num = first_src; src_num <= last_src; ++src_num)
      {
        operand this_src = the_instr->src_op(src_num);
        if (!this_src.type()->unqual()->is_ptr())
            continue;

        type_node *original_type = original_op_type(this_src);
        ptr_type *src_ptr = (ptr_type *)(this_src.type()->unqual());
        in_rrr *old_cvt = NULL;
        if (is_fio && this_src.is_expr() &&
            (this_src.instr()->opcode() == io_cvt) &&
            (src_ptr->ref_type()->op() == TYPE_INT) &&
            (src_ptr->ref_type()->size() == target.size[C_char]))
          {
            old_cvt = (in_rrr *)(this_src.instr());
            this_src = old_cvt->src_op();
            src_ptr = (ptr_type *)(this_src.type()->unqual());
          }
        this_src.remove();
        this_src = simplify_address(this_src, src_ptr->ref_type(), NULL);
        if (is_memop && (this_src.type() != src_ptr))
            this_src = fold_real_1op_rrr(io_cvt, original_type, this_src);
        if (old_cvt != NULL)
            old_cvt->set_src(this_src);
        else
            the_instr->set_src_op(src_num, this_src);
      }

    switch (the_instr->opcode())
      {
        case io_lod:
          {
            in_rrr *the_load = (in_rrr *)the_instr;
            var_sym *loaded_var;
            boolean is_var =
                    is_simple_var_addr(the_load->src_addr_op(), &loaded_var);
            if (is_var)
              {
                if (the_load->dst_op().is_instr())
                  {
                    instruction *parent_instr = the_load->dst_op().instr();
                    unsigned num_srcs = parent_instr->num_srcs();
                    unsigned src_num;
                    for (src_num = 0; src_num < num_srcs; ++src_num)
                      {
                        if (parent_instr->src_op(src_num) == operand(the_load))
                            break;
                      }
                    the_load->remove();
                    parent_instr->set_src_op(src_num, operand(loaded_var));
                  }
                else
                  {
                    in_rrr *new_copy =
                            new in_rrr(io_cpy, loaded_var->type(), operand(),
                                       operand(loaded_var));
                    replace_instruction(the_load, new_copy);
                  }
                delete the_load;
              }
            break;
          }
        case io_str:
          {
            in_rrr *the_store = (in_rrr *)the_instr;
            var_sym *stored_var;
            boolean is_var =
                    is_simple_var_addr(the_store->dst_addr_op(), &stored_var);
            if (is_var)
              {
                operand data_op = the_store->src2_op();
                data_op.remove();
                instruction *new_instr;
                if (data_op.is_expr())
                  {
                    new_instr = data_op.instr();
                  }
                else
                  {
                    new_instr =
                            new in_rrr(io_cpy, stored_var->type(), operand(),
                                       data_op);
                  }

                replace_instruction(the_store, new_instr);
                new_instr->set_dst(operand(stored_var));
                delete the_store;
              }
            break;
          }
        case io_memcpy:
          {
            in_rrr *the_memcopy = (in_rrr *)the_instr;
            var_sym *loaded_var;
            boolean load_is_var =
                    is_simple_var_addr(the_memcopy->src_addr_op(),
                                       &loaded_var);
            var_sym *stored_var;
            boolean store_is_var =
                    is_simple_var_addr(the_memcopy->dst_addr_op(),
                                       &stored_var);
            if (load_is_var && store_is_var)
              {
                in_rrr *new_copy =
                        new in_rrr(io_cpy, loaded_var->type(), operand(),
                                   operand(loaded_var));
                replace_instruction(the_memcopy, new_copy);
                new_copy->set_dst(operand(stored_var));
                delete the_memcopy;
              }
            else if (load_is_var)
              {
                operand dst_addr = the_memcopy->dst_addr_op();
                dst_addr.remove();
                in_rrr *new_store =
                        new in_rrr(io_str, type_void, operand(), dst_addr,
                                   operand(loaded_var));
                replace_instruction(the_memcopy, new_store);
                delete the_memcopy;
              }
            else if (store_is_var)
              {
                operand src_addr = the_memcopy->src_addr_op();
                src_addr.remove();
                in_rrr *new_load =
                        new in_rrr(io_lod, stored_var->type(), operand(),
                                   src_addr);
                replace_instruction(the_memcopy, new_load);
                new_load->set_dst(operand(stored_var));
                delete the_memcopy;
              }
            break;
          }
        default:
            break;
      }
  }

static void deallocate_operand(operand to_go)
  {
    if (!to_go.is_expr())
        return;

    instruction *the_instr = to_go.instr();
    assert(the_instr != NULL);
    delete the_instr;
  }

static operand build_offset_operand(array_type *the_array_type,
                                    int num_dimensions)
  {
    array_type *follow_array = the_array_type;
    operand result = const_op(immed(0), type_ptr_diff);
    int dim_num = 0;
    while (dim_num < num_dimensions)
      {
        operand upper_op =
                operand_from_array_bound(follow_array->upper_bound());
        operand lower_op =
                operand_from_array_bound(follow_array->lower_bound());
        result *= ((upper_op - lower_op.clone()) + 1);
        result += lower_op;

        type_node *next_type = follow_array->elem_type();
        assert(next_type != NULL);
        if (!next_type->is_array())
            return result;
        follow_array = (array_type *)next_type;
        ++dim_num;
      }

    return result;
  }

static const char *last_field(immed_list *field_immeds)
  {
    const char *result = NULL;
    immed_list_iter the_iter(field_immeds);
    while (!the_iter.is_empty())
      {
        immed value = the_iter.step();
        if (!value.is_string())
            return NULL;
        result = value.string();
      }
    return result;
  }

static char *guess_base_var_name(in_array *the_array)
  {
    if (the_array == NULL)
        return NULL;

    operand index = the_array->index(0);
    if (!index.is_instr())
        return NULL;

    instruction *index_instr = index.instr();
    assert(index_instr != NULL);
    while (index_instr->opcode() == io_cvt)
      {
        in_rrr *the_cvt = (in_rrr *)index_instr;
        index = the_cvt->src_op();
        if (!index.is_instr())
            return NULL;

        index_instr = index.instr();
        assert(index_instr != NULL);
      }

    /* skip multiplication done for character strings */
    if (index_instr->opcode() == io_mul)
      {
        in_rrr *the_div = (in_rrr *)index_instr;
        index = the_div->src1_op();
        if (!index.is_instr())
            return NULL;

        index_instr = index.instr();
        assert(index_instr != NULL);
      }

    while (index_instr->opcode() == io_cvt)
      {
        in_rrr *the_cvt = (in_rrr *)index_instr;
        index = the_cvt->src_op();
        if (!index.is_instr())
            return NULL;

        index_instr = index.instr();
        assert(index_instr != NULL);
      }

    if (index_instr->opcode() != io_sub)
        return NULL;

    in_rrr *the_sub = (in_rrr *)index_instr;
    operand subtracted = the_sub->src2_op();

    while (subtracted.is_expr() && (subtracted.instr()->opcode() == io_cvt))
      {
        in_rrr *the_cvt = (in_rrr *)(subtracted.instr());
        subtracted = the_cvt->src_op();
      }

    if (!subtracted.is_symbol())
        return NULL;

    var_sym *the_symbol = subtracted.symbol();
    if (the_symbol == NULL)
        return NULL;

    const char *sym_name = the_symbol->name();
    if (sym_name == NULL)
        return NULL;

    char *suffix = strstr(sym_name, "_offset");
    if (suffix == NULL)
        return NULL;

    if (strcmp(suffix, "_offset") != 0)
        return NULL;

    char *result = new char[suffix - sym_name + 1];
    strncpy(result, sym_name, suffix - sym_name);
    result[suffix - sym_name] = 0;
    return result;
  }

/*
 *  This function takes the operand ``original'' and breaks it up into
 *  *op_a and *op_b such that
 *
 *      *op_a + (the_var * (*op_b)) = original
 *
 *  with as much of the operand as possible in *op_b.
 */
static void linear_form(operand *op_a, operand *op_b, operand original,
                        var_sym *the_var)
  {
    type_node *the_type = original.type();

    switch (original.kind())
      {
        case OPER_NULL:
          {
            *op_a = operand();
            *op_b = operand();
            break;
          }
        case OPER_SYM:
          {
            if (original.symbol() == the_var)
              {
                *op_a = const_op(immed(0), the_type);
                *op_b = const_op(immed(1), the_type);
              }
            else
              {
                *op_a = original;
                *op_b = const_op(immed(0), the_type);
              }
            break;
          }
        case OPER_INSTR:
          {
            instruction *the_instr = original.instr();
            assert(the_instr != NULL);

            switch(the_instr->opcode())
              {
                case io_cpy:
                case io_cvt:
                  {
                    in_rrr *the_rrr = (in_rrr *)the_instr;
                    assert(the_rrr->src2_op().is_null());
                    operand source1 = the_rrr->src1_op();
                    source1.remove();

                    delete the_rrr;

                    linear_form(op_a, op_b, source1, the_var);
                    break;
                  }
                case io_add:
                  {
                    in_rrr *the_rrr = (in_rrr *)the_instr;
                    operand source1 = the_rrr->src1_op();
                    source1.remove();
                    operand source2 = the_rrr->src2_op();
                    source2.remove();

                    delete the_rrr;

                    operand op_a1, op_a2, op_b1, op_b2;
                    linear_form(&op_a1, &op_b1, source1, the_var);
                    linear_form(&op_a2, &op_b2, source2, the_var);
                    *op_a = (op_a1 + op_a2);
                    *op_b = (op_b1 + op_b2);
                    break;
                  }
                case io_sub:
                  {
                    in_rrr *the_rrr = (in_rrr *)the_instr;
                    operand source1 = the_rrr->src1_op();
                    source1.remove();
                    operand source2 = the_rrr->src2_op();
                    source2.remove();

                    delete the_rrr;

                    operand op_a1, op_a2, op_b1, op_b2;
                    linear_form(&op_a1, &op_b1, source1, the_var);
                    linear_form(&op_a2, &op_b2, source2, the_var);
                    *op_a = (op_a1 - op_a2);
                    *op_b = (op_b1 - op_b2);
                    break;
                  }
                case io_neg:
                  {
                    in_rrr *the_rrr = (in_rrr *)the_instr;
                    assert(the_rrr->src2_op().is_null());
                    operand source1 = the_rrr->src1_op();
                    source1.remove();

                    delete the_rrr;

                    operand op_a1, op_b1;
                    linear_form(&op_a1, &op_b1, source1, the_var);
                    *op_a = -op_a1;
                    *op_b = -op_b1;
                    break;
                  }
                case io_mul:
                  {
                    in_rrr *the_rrr = (in_rrr *)the_instr;
                    operand source1 = the_rrr->src1_op();
                    source1.remove();
                    operand source2 = the_rrr->src2_op();
                    source2.remove();

                    delete the_rrr;

                    operand op_a1, op_a2, op_b1, op_b2;
                    linear_form(&op_a1, &op_b1, source1, the_var);
                    linear_form(&op_a2, &op_b2, source2, the_var);

                    if (is_zero(op_b1))
                      {
                        nullify_operand(&op_b1);
                        *op_a = (op_a1.clone() * op_a2);
                        *op_b = (op_b2 * op_a1);
                      }
                    else if (is_zero(op_b2))
                      {
                        nullify_operand(&op_b2);
                        *op_a = (op_a2.clone() * op_a1);
                        *op_b = (op_b1 * op_a2);
                      }
                    else
                      {
                        operand new_a1 = op_a1.clone();
                        operand new_a2 = op_a2.clone();
                        operand new_b1 = op_b1.clone();
                        operand new_b2 = op_b2.clone();
                        *op_a = (op_a1 * op_a2);
                        *op_b = ((new_b1 * new_a2) + (new_a1 * new_b2) +
                                 (operand(the_var) * op_b1 * op_b2));
                      }
                    break;
                  }
                case io_lsl:
                  {
                    in_rrr *the_rrr = (in_rrr *)the_instr;
                    operand source2 = the_rrr->src2_op();
                    if (source2.is_expr())
                      {
                        instruction *source_2_instr = source2.instr();
                        assert(source_2_instr != NULL);
                        if (source_2_instr->opcode() == io_ldc)
                          {
                            in_ldc *the_ldc = (in_ldc *)source_2_instr;
                            immed value = the_ldc->value();
                            if (value.is_integer())
                              {
                                int shift_amount = value.integer();
                                if ((shift_amount >= 0) &&
                                    (shift_amount < (int)sizeof(int)))
                                  {
                                    operand source1 = the_rrr->src1_op();
                                    source1.remove();
                                    delete the_rrr;
                                    linear_form(op_a, op_b, source1, the_var);
                                    if (shift_amount != 0)
                                      {
                                        i_integer i_const = 1 << shift_amount;
                                        *op_a *= const_op(i_const, the_type);
                                        *op_b *= const_op(i_const, the_type);
                                      }
                                    break;
                                  }
                              }
                          }
                      }
                    /* fall through */
                  }
                default:
                  {
                    *op_a = original;
                    *op_b = const_op(immed(0), the_type);
                    break;
                  }
              }
            break;
          }
        default:
            assert(FALSE);
      }
  }

static boolean is_zero(operand the_operand)
  {
    if (the_operand.kind() == OPER_NULL)
        return TRUE;

    if (the_operand.kind() != OPER_INSTR)
        return FALSE;

    instruction *the_instr = the_operand.instr();
    assert(the_instr != NULL);
    if (the_instr->opcode() != io_ldc)
        return FALSE;

    in_ldc *the_ldc = (in_ldc *)the_instr;
    immed value = the_ldc->value();

    if (value.kind() != im_int)
        return FALSE;

    if (value.integer() == 0)
        return TRUE;

    return FALSE;
  }

static void nullify_operand(operand *the_operand)
  {
    if (the_operand->kind() == OPER_INSTR)
      {
        instruction *the_instr = the_operand->instr();
        if (the_instr != NULL)
            delete the_instr;
      }

    the_operand->set_null();
  }

static boolean is_subtracted(operand the_operand, var_sym *the_variable,
                             boolean negated)
  {
    if (negated && (the_operand.kind() == OPER_SYM))
      {
        var_sym *the_symbol = the_operand.symbol();
        return (the_symbol == the_variable);
      }

    if (the_operand.kind() != OPER_INSTR)
        return FALSE;

    instruction *the_instr = the_operand.instr();
    if (the_instr == NULL)
        return FALSE;

    switch (the_instr->opcode())
      {
        case io_cpy:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            return is_subtracted(the_rrr->src_op(), the_variable, negated);
          }
        case io_cvt:
          {
            in_rrr *the_cvt = (in_rrr *)the_instr;
            operand source_op = the_cvt->src_op();
            return is_subtracted(source_op, the_variable, negated);
          }
        case io_add:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            return (is_subtracted(the_rrr->src1_op(), the_variable, negated) ||
                    is_subtracted(the_rrr->src2_op(), the_variable, negated));
          }
        case io_sub:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            return (is_subtracted(the_rrr->src1_op(), the_variable, negated) ||
                    is_subtracted(the_rrr->src2_op(), the_variable, !negated));
          }
        case io_neg:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            assert(the_rrr->src2_op().is_null());
            return is_subtracted(the_rrr->src1_op(), the_variable, !negated);
          }
        default:
            return FALSE;
      }
  }

static operand remove_subtracted_variable(operand the_operand,
                                          var_sym *the_variable,
                                          boolean negated)
  {
    assert(the_operand.kind() == OPER_INSTR);

    instruction *the_instr = the_operand.instr();
    assert(the_instr != NULL);

    type_node *the_type = the_instr->result_type();

    switch (the_instr->opcode())
      {
        case io_cpy:
        case io_cvt:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            operand source = the_rrr->src_op();
            source.remove();

            delete the_rrr;

            return remove_subtracted_variable(source, the_variable, negated);
          }
        case io_add:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            operand source1 = the_rrr->src1_op();
            operand source2 = the_rrr->src2_op();
            source1.remove();
            source2.remove();

            delete the_rrr;

            if (negated)
              {
                if (operand_is_var(source1, the_variable))
                    return source2;
                if (operand_is_var(source2, the_variable))
                    return source1;
              }

            if (is_subtracted(source1, the_variable, negated))
              {
                return new in_rrr(io_add, the_type, operand(),
                                  remove_subtracted_variable(source1,
                                                             the_variable,
                                                             negated),
                                  source2);
              }

            assert(is_subtracted(source2, the_variable, negated));
            return new in_rrr(io_add, the_type, operand(), source1,
                              remove_subtracted_variable(source2,
                                                         the_variable,
                                                         negated));
          }
        case io_sub:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            operand source1 = the_rrr->src1_op();
            operand source2 = the_rrr->src2_op();
            source1.remove();
            source2.remove();

            delete the_rrr;

            if (negated && operand_is_var(source1, the_variable))
                return source2;
            if ((!negated) && operand_is_var(source2, the_variable))
                return source1;

            if (is_subtracted(source1, the_variable, negated))
              {
                return new in_rrr(io_sub, the_type, operand(),
                                  remove_subtracted_variable(source1,
                                                             the_variable,
                                                             negated),
                                  source2);
              }

            assert(is_subtracted(source2, the_variable, !negated));
            return new in_rrr(io_sub, the_type, operand(), source1,
                              remove_subtracted_variable(source2,
                                                         the_variable,
                                                         !negated));
          }
        case io_neg:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            assert(the_rrr->src2_op().is_null());
            operand source1 = the_rrr->src1_op();
            source1.remove();

            delete the_rrr;

            return new in_rrr(io_neg, the_type, operand(),
                              remove_subtracted_variable(source1,
                                                         the_variable,
                                                         !negated), operand());
          }
        default:
            assert(FALSE);
      }

    assert(FALSE);
    return operand();
  }

static boolean operand_is_var(operand the_operand, var_sym *the_variable)
  {
    operand follow_op = the_operand;

    while (follow_op.is_expr() &&
           ((follow_op.instr()->opcode() == io_cvt) ||
            (follow_op.instr()->opcode() == io_cpy)))
      {
        in_rrr *the_rrr = (in_rrr *)(follow_op.instr());
        follow_op = the_rrr->src1_op();
      }

    if (follow_op.kind() != OPER_SYM)
        return FALSE;

    return (the_variable == follow_op.symbol());
  }

static void mark_params_call_by_ref(tree_proc *the_tree_proc)
  {
    proc_symtab *the_proc_symtab = the_tree_proc->proc_syms();
    assert(the_proc_symtab != NULL);

    sym_node_list_iter the_iter(the_proc_symtab->params());
    while (!the_iter.is_empty())
      {
        sym_node *the_symbol = the_iter.step();
        assert(the_symbol != NULL);
        assert(the_symbol->is_var());
        var_sym *the_var = (var_sym *)the_symbol;
        type_node *the_type = the_var->type();
        assert(the_type != NULL);
        if (the_type->is_ptr())
          {
            ptr_type *the_ptr = (ptr_type *)the_type;
            if (!the_ptr->ref_type()->unqual()->is_func())
              {
                type_node *new_type = the_type->copy();
                new_type->append_annote(k_call_by_ref, NULL);
                the_var->set_type(the_type->parent()->install_type(new_type));
              }
          }
      }
  }

static void fix_symtabs(base_symtab *the_symtab)
  {
    assert(the_symtab != NULL);
    fix_symtab(the_symtab);
    base_symtab_list_iter the_iter(the_symtab->children());
    while (!the_iter.is_empty())
      {
        base_symtab *this_symtab = the_iter.step();
        fix_symtabs(this_symtab);
      }
  }

static void fix_symtab(base_symtab *the_symtab)
  {
    sym_node_list_iter the_iter(the_symtab->symbols());

    while (!the_iter.is_empty())
      {
        sym_node *the_symbol = the_iter.step();
        if (the_symbol->is_var())
          {
            if (strchr(the_symbol->name(), '_') != NULL)
                the_symbol->reset_userdef();

            var_sym *the_var = (var_sym *)the_symbol;
            type_node *var_type = the_var->type();
            if (is_complex(var_type))
                the_var->set_type(complex_replacement(var_type));
          }
      }

    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        replace_complex_in_type(this_type);
      }
  }

static boolean is_complex(type_node *the_type)
  {
    assert(the_type != NULL);
    if (the_type->op() != TYPE_STRUCT)
        return FALSE;
    struct_type *the_struct = (struct_type *)the_type;
    if (the_struct->num_fields() != 2)
        return FALSE;
    if (strcmp(the_struct->field_name(0), "_r") != 0)
        return FALSE;
    if (strcmp(the_struct->field_name(1), "_i") != 0)
        return FALSE;
    if (the_struct->field_type(0) != the_struct->field_type(1))
        return FALSE;
    if (the_struct->field_type(0)->op() != TYPE_FLOAT)
        return FALSE;
    return TRUE;
  }

static type_node *complex_replacement(type_node *complex_type)
  {
    assert(complex_type != NULL);
    assert(complex_type->op() == TYPE_STRUCT);
    struct_type *the_struct = (struct_type *)complex_type;
    assert(the_struct->num_fields() == 2);
    type_node *new_type =
            new array_type(the_struct->field_type(0), array_bound(0),
                           array_bound(1));
    return complex_type->parent()->install_type(new_type);
  }

static void replace_complex_in_type(type_node *the_type)
  {
    assert(the_type != NULL);
    switch(the_type->op())
      {
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
            break;
        case TYPE_PTR:
          {
            ptr_type *the_ptr = (ptr_type *)the_type;

            type_node *base_type = the_ptr->ref_type();
            if (is_complex(base_type))
                the_ptr->set_ref_type(complex_replacement(base_type));
            else
                replace_complex_in_type(base_type);

            break;
          }
        case TYPE_ARRAY:
          {
            array_type *the_array = (array_type *)the_type;

            type_node *base_type = the_array->elem_type();
            if (is_complex(base_type))
                the_array->set_elem_type(complex_replacement(base_type));
            else
                replace_complex_in_type(base_type);

            break;
          }
        case TYPE_FUNC:
          {
            func_type *the_function = (func_type *)the_type;

            type_node *return_type = the_function->return_type();
            if (is_complex(return_type))
              {
                the_function->set_return_type(
                        complex_replacement(return_type));
              }
            else
              {
                replace_complex_in_type(return_type);
              }

            unsigned num_args = the_function->num_args();
            for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
              {
                type_node *arg_type = the_function->arg_type(arg_num);
                if (is_complex(arg_type))
                  {
                    the_function->set_arg_type(arg_num,
                                               complex_replacement(arg_type));
                  }
                else
                  {
                    replace_complex_in_type(arg_type);
                  }
              }

            break;
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *the_struct = (struct_type *)the_type;

            unsigned num_fields = the_struct->num_fields();
            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                type_node *field_type = the_struct->field_type(field_num);
                if (is_complex(field_type))
                  {
                    type_node *replacement = complex_replacement(field_type);
                    assert(the_struct->parent()->is_ancestor(
                            replacement->parent()));
                    the_struct->set_field_type(field_num, replacement);
                  }
                else
                  {
                    replace_complex_in_type(field_type);
                  }
              }

            break;
          }
        case TYPE_ENUM:
            break;
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *the_modifier = (modifier_type *)the_type;

            type_node *base_type = the_modifier->base();
            if (is_complex(base_type))
                the_modifier->set_base(complex_replacement(base_type));
            else
                replace_complex_in_type(base_type);

            break;
          }
        default:
            assert(FALSE);
      }
  }

static void fix_complex_refs(tree_proc *the_proc)
  {
    assert(the_proc != NULL);
    the_proc->body()->map(&fix_complex_on_tree_node, NULL);
  }

static void fix_complex_on_tree_node(tree_node *the_node, void *)
  {
    assert(the_node != NULL);
    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    the_tree_instr->instr_map(&fix_complex_on_instr, NULL, FALSE);
  }

static void fix_complex_on_instr(instruction *the_instr, void *)
  {
    assert(the_instr != NULL);
    type_node *result_type = the_instr->result_type();
    if (is_complex(result_type))
      {
        the_instr->set_result_type(complex_replacement(result_type));

        if (the_instr->dst_op().is_symbol())
          {
            var_sym *dest_var = the_instr->dst_op().symbol();
            dest_var->set_addr_taken();
            in_rrr *the_store =
                    new in_rrr(io_str, type_void, operand(), addr_op(dest_var),
                               operand());
            the_instr->set_dst(operand());
            replace_instruction(the_instr, the_store);
            the_store->set_src2(operand(the_instr));
            fix_complex_store(the_store);
            return;
          }
      }

    if ((the_instr->opcode() == io_str) || (the_instr->opcode() == io_memcpy))
      {
        in_rrr *the_store = (in_rrr *)the_instr;
        type_node *dest_type = the_store->dst_addr_op().type()->unqual();

        assert(dest_type->is_ptr());
        ptr_type *dest_ptr = (ptr_type *)dest_type;
        type_node *object_type = dest_ptr->ref_type()->unqual();
        if (object_type->is_array())
          {
            array_type *the_array = (array_type *)object_type;
            if ((the_array->lower_bound() == array_bound(0)) &&
                (the_array->upper_bound() == array_bound(1)))
              {
                fix_complex_store(the_store);
                return;
              }
          }
      }

    immed_list *field_immeds =
            (immed_list *)(the_instr->peek_annote(k_fields));
    if (field_immeds == NULL)
        return;
    const char *last_string = last_field(field_immeds);
    if (last_string == NULL)
        return;

    boolean is_first;
    if (strcmp(last_string, "_r") == 0)
        is_first = TRUE;
    else if (strcmp(last_string, "_i") == 0)
        is_first = FALSE;
    else
        return;

    type_node *original_type = the_instr->result_type();

    if (original_type->unqual()->op() != TYPE_PTR)
      {
        warning_line(the_instr->parent(),
                     "field annotation on instruction of non-pointer type");
        return;
      }

    ptr_type *the_ptr = (ptr_type *)(original_type->unqual());
    type_node *base_type = the_ptr->ref_type();
    assert(base_type != NULL);
    unsigned elem_size = base_type->size();

    switch (the_instr->opcode())
      {
        case io_add:
        case io_sub:
            if (!is_first)
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                operand the_operand = the_rrr->src2_op();
                if ((the_operand.type()->unqual()->op() != TYPE_INT) &&
                    (the_instr->opcode() == io_add))
                  {
                    the_operand = the_rrr->src1_op();
                  }

                if (!the_operand.is_expr())
                  {
                    warning_line(the_instr->parent(),
                                 "offset for field annotation is not "
                                 "constant");
                    return;
                  }
                instruction *offset_instr = the_operand.instr();

                if (offset_instr->opcode() != io_ldc)
                  {
                    warning_line(the_instr->parent(),
                                 "offset for field annotation is not "
                                 "constant");
                    return;
                  }
                in_ldc *offset_ldc = (in_ldc *)offset_instr;

                immed offset_value = offset_ldc->value();
                if (!offset_value.is_integer())
                  {
                    warning_line(the_instr->parent(),
                                 "offset for field annotation is not "
                                 "an integer constant");
                    return;
                  }

                int offset = offset_value.integer();
                if (the_instr->opcode() == io_add)
                    offset -= elem_size / target.addressable_size;
                else
                    offset += elem_size / target.addressable_size;
                offset_ldc->set_value(immed(offset));
              }
            break;
        case io_ldc:
            if (!is_first)
              {
                in_ldc *the_ldc = (in_ldc *)the_instr;
                immed value = the_ldc->value();
                if (value.is_symbol())
                  {
                    sym_node *the_symbol = value.symbol();
                    int offset = value.offset();
                    offset -= elem_size;
                    value = immed(the_symbol, offset);
                  }
                else if (value.is_integer())
                  {
                    int offset = value.integer();
                    offset -= elem_size / target.addressable_size;
                    value = immed(offset);
                  }
                else
                  {
                    warning_line(the_instr->parent(),
                                 "field annotation of bad ldc");
                    return;
                  }
                the_ldc->set_value(value);
              }
            break;
        case io_cvt:
            if (!is_first)
              {
                warning_line(the_instr->parent(),
                             "field annotation of bad cvt");
                return;
              }
            break;
        case io_array:
          {
            in_array *the_array = (in_array *)the_instr;
            if (!is_first)
              {
                unsigned offset = the_array->offset();
                assert(offset >= elem_size);
                offset -= elem_size;
                the_array->set_offset(offset);
              }
            break;
          }
        default:
            warning_line(the_instr->parent(),
                         "field annotation on bad instruction type");
            return;
      }

    drop_last_field_name(the_instr);

    type_node *new_type =
            new array_type(base_type, array_bound(0), array_bound(1));
    new_type = base_type->parent()->install_type(new_type);
    the_instr->set_result_type(new_type->ptr_to());

    tree_instr *the_parent = NULL;
    instruction *instr_above = NULL;
    unsigned src_num;
    operand destination = the_instr->dst_op();
    if (destination.is_instr())
      {
        instr_above = destination.instr();
        assert(instr_above != NULL);
        unsigned num_srcs = instr_above->num_srcs();
        for (src_num = 0; src_num < num_srcs; ++src_num)
          {
            operand this_source = instr_above->src_op(src_num);
            if (this_source.is_expr())
              {
                if (this_source.instr() == the_instr)
                    break;
              }
          }
        assert(src_num < num_srcs);
        the_instr->remove();
      }
    else
      {
        tree_instr *the_parent = the_instr->parent();
        assert(the_parent != NULL);
        the_parent->remove_instr(the_instr);
        the_instr->set_dst(operand());
      }

    in_array *new_array = add_const_aref(operand(the_instr), is_first ? 0 : 1);

    if (the_parent == NULL)
      {
        assert(instr_above != NULL);
        instr_above->set_src_op(src_num, operand(new_array));
      }
    else
      {
        new_array->set_dst(destination);
        the_parent->set_instr(new_array);
      }
  }

static void drop_last_field_name(instruction *the_instr)
  {
    immed_list *old_immeds = (immed_list *)(the_instr->get_annote(k_fields));
    assert(old_immeds != NULL);
    immed_list_iter the_iter(old_immeds);
    if (the_iter.is_empty())
        return;
    immed_list *new_immeds = new immed_list;
    while (TRUE)
      {
        immed this_immed = the_iter.step();
        if (the_iter.is_empty())
            break;
        new_immeds->append(this_immed);
      }

    if (new_immeds->is_empty())
        delete new_immeds;
    else
        the_instr->append_annote(k_fields, new_immeds);
    return;
  }

static void fix_complex_store(in_rrr *the_store)
  {
    assert((the_store->opcode() == io_str) ||
           (the_store->opcode() == io_memcpy));

    operand dest_addr = the_store->dst_addr_op();

    operand src_addr;
    if (the_store->opcode() == io_memcpy)
      {
        src_addr = the_store->src_addr_op();
        src_addr.remove();
      }
    else
      {
        operand src_op = the_store->src2_op();
        while (src_op.is_expr() && (src_op.instr()->opcode() == io_cpy))
          {
            in_rrr *the_copy = (in_rrr *)(src_op.instr());
            src_op = the_copy->src_op();
          }

        if (src_op.is_expr())
          {
            instruction *src_instr = src_op.instr();
            if (src_instr->opcode() != io_lod)
                return;
            in_rrr *src_lod = (in_rrr *)src_instr;
            src_addr = src_lod->src_addr_op();
            src_addr.remove();
          }
        else if (src_op.is_symbol())
          {
            var_sym *src_var = src_op.symbol();
            src_var->set_addr_taken();
            src_addr = const_op(immed(src_var), dest_addr.type());
          }
        else
          {
            return;
          }
      }

    dest_addr.remove();

    tree_node *place = the_store->owner();

    dest_addr = make_re_evalable(dest_addr, place);
    src_addr = make_re_evalable(src_addr, place);

    in_array *first_dest = add_const_aref(dest_addr.clone(), 0);
    in_array *first_src = add_const_aref(src_addr.clone(), 0);
    in_rrr *first_cpy =
            new in_rrr(io_memcpy, type_void, operand(), operand(first_dest),
                       operand(first_src));
    place->parent()->insert_before(new tree_instr(first_cpy), place->list_e());

    in_array *second_dest = add_const_aref(dest_addr, 1);
    in_array *second_src = add_const_aref(src_addr, 1);
    in_rrr *second_cpy =
            new in_rrr(io_memcpy, type_void, operand(), operand(second_dest),
                       operand(second_src));
    replace_instruction(the_store, second_cpy);

    delete the_store;
  }

/*
 *  Here we only have to deal with Fortran 77 code, so the only way
 *  there can be a side effect is through a function call.  So we only
 *  have to put the results of function calls into temporaries.
 */
static operand make_re_evalable(operand the_op, tree_node *place)
  {
    if (!the_op.is_expr())
        return the_op;

    instruction *the_instr = the_op.instr();

    if (the_instr->opcode() == io_cal)
      {
        var_sym *temp_var =
                place->scope()->new_unique_var(the_instr->result_type());
        temp_var->reset_userdef();

        the_instr->set_dst(operand(temp_var));
        tree_instr *new_tree_instr = new tree_instr(the_instr);
        place->parent()->insert_before(new_tree_instr, place->list_e());

        return operand(temp_var);
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        this_op.remove();
        this_op = make_re_evalable(this_op, place);
        the_instr->set_src_op(src_num, this_op);
      }
    return the_op;
  }

/*
 *  Return an address which is simply the location of a symbol with
 *  the proper type.  The appropriate symbol is created as a
 *  sub-variable and/or the type of the variable is set, as
 *  appropriate.  The exception is addresses calculated by array
 *  refernce instructions -- these are returned unchanged, because the
 *  base operand of the array will be handled elsewhere.
 */
static operand simplify_address(operand old_address, type_node *new_type,
                                const char *name)
  {
    const char *new_name = name;
    if ((new_name == NULL) && old_address.is_expr())
      {
        annote *field_annote =
                old_address.instr()->annotes()->peek_annote(k_fields);
        if (field_annote != NULL)
          {
            immed_list *field_immeds = field_annote->immeds();
            if ((field_immeds != NULL) && (!field_immeds->is_empty()))
              {
                immed last_immed = field_immeds->tail()->contents;
                if (last_immed.is_string())
                    new_name = last_immed.string();
              }
          }
      }

    if (old_address.is_expr() && (old_address.instr()->opcode() == io_array))
        return fold_real_1op_rrr(io_cvt, new_type->ptr_to(), old_address);

    if (old_address.is_symbol())
      {
        var_sym *old_symbol = old_address.symbol();
        void *data = old_symbol->peek_annote(k_fixfortran_original_type);
        if (data != NULL)
          {
            type_node *original_type = (type_node *)data;
            return fold_real_1op_rrr(io_cvt, original_type, old_address);
          }
      }

    immed immed_address;
    eval_status status = evaluate_const_expr(old_address, &immed_address);
    if ((status != EVAL_OK) || (!immed_address.is_symbol()) ||
        (!immed_address.symbol()->is_var()))
      {
        /*
         *  This happens in the case of weird pointer arithmetic that
         *  sf2c puts in to handle arrays of character strings.  In
         *  that case, we just give up on trying to put a type back on
         *  an object.
         */
        return fold_real_1op_rrr(io_cvt, new_type->ptr_to(), old_address);
      }

    var_sym *the_var = (var_sym *)(immed_address.symbol());

    /*
     * Make sure it's not a temporary put in by snoot to hold a string
     * literal.
     */
    if (strstr(the_var->name(), "__tmp_string") != NULL)
        return fold_real_1op_rrr(io_cvt, new_type->ptr_to(), old_address);

    if (old_address.is_expr())
        delete old_address.instr();

    type_node *var_type = the_var->type()->unqual();
    if (is_complex(var_type))
      {
        in_ldc *new_ldc =
                new in_ldc(new_type->ptr_to(), operand(), immed_address);
        if ((new_type != complex_replacement(var_type)) ||
            (immed_address.offset() != 0))
          {
            immed_list *field_immeds = new immed_list;
            if (immed_address.offset() == 0)
                field_immeds->append(immed("_r"));
            else
                field_immeds->append(immed("_i"));
            new_ldc->append_annote(k_fields, field_immeds);
          }
        return operand(new_ldc);
      }
    else if (var_type->is_struct() || var_type->is_array())
      {
        if (the_var->parent_var() != NULL)
          {
            assert(the_var->offset() == 0);
            the_var = the_var->parent_var();
            var_type = the_var->type()->unqual();
            assert(var_type->is_struct());
          }

        if (var_type->is_array() && new_type->is_array() &&
            (the_var->parent_var() == NULL) &&
            (var_type->size() == new_type->size()) &&
            (the_var->annotes()->peek_annote(k_fixfortran_fixed_array_type) ==
             NULL))
          {
            the_var->set_type(new_type);
            the_var->append_annote(k_fixfortran_fixed_array_type,
                                   new immed_list());
          }
        else if (var_type->is_array() && (the_var->type() == new_type))
          {
            /* empty */;
          }
        else if (var_type->is_array() && new_type->is_same(type_char))
          {
            /* empty */;
          }
        else
          {
            struct_type *var_struct;

            if (var_type->op() == TYPE_GROUP)
              {
                var_struct = (struct_type *)var_type;
              }
            else if (var_type->is_array() &&
                (the_var->annotes()->peek_annote(k_fixfortran_fixed_array_type)
                 != NULL))
              {
                if (the_var->parent_var() != NULL)
                  {
                    the_var = the_var->parent_var();
                    var_type = the_var->type();
                    assert(var_type->op() == TYPE_GROUP);
                    var_struct = (struct_type *)var_type;
                  }
                else
                  {
                    var_struct = new struct_type(TYPE_GROUP, var_type->size(),
                                                 the_var->name(), 1);
                    var_struct->set_field_name(0, the_var->name());
                    var_struct->set_offset(0, 0);
                    var_struct->set_field_type(0, var_type);
                    var_type = var_type->parent()->install_type(var_struct);
                    var_struct = (struct_type *)var_type;

                    var_sym *new_var =
                            the_var->parent()->new_var(var_struct,
                                                       the_var->name());

                    if (the_var->has_var_def())
                      {
                        var_def *old_def = the_var->definition();
                        var_def *new_def =
                                old_def->parent()->define_var(new_var,
                                        old_def->alignment());
                        old_def->parent()->remove_def(old_def);
                        while (!old_def->annotes()->is_empty())
                          {
                            annote *this_annote = old_def->annotes()->pop();
                            new_def->annotes()->append(this_annote);
                          }
                        delete old_def;
                      }

                    if (the_var->annotes()->peek_annote(k_common_block) !=
                        NULL)
                      {
                        delete the_var->annotes()->get_annote(k_common_block);
                      }

                    if (new_var->is_global())
                        new_var->append_annote(k_common_block);

                    new_var->add_child(the_var, 0);
                    the_var = new_var;
                  }
              }
            else
              {
                const char *struct_name;
                if (var_type->is_struct())
                    struct_name = ((struct_type *)var_type)->name();
                else
                    struct_name = "0";
                var_struct =
                        new struct_type(TYPE_GROUP, var_type->size(),
                                        struct_name, 0);
                var_type = var_type->parent()->install_type(var_struct);
                var_struct = (struct_type *)var_type;
                the_var->set_type(var_struct);
                if (the_var->is_global())
                    the_var->append_annote(k_common_block, NULL);
              }

            new_name = new_field_name(new_name, var_struct);

            /*
             *  There's no such thing as a single ``char'' variable in
             *  Fortran -- strings are multiple characters and have
             *  lengths, and the string of length one is just
             *  considered a special case of that.  So if we are
             *  addressing something of type char, either it's
             *  something internal created by sf2c, not the user; or
             *  it's a character string, in which case the type will
             *  already have been put on the object at some point with
             *  a call to this function with an array type; or it is
             *  something cast to ``char*'' for I/O or pointer
             *  arithmetic, and it's hopeless to try to restore the
             *  type at this point anyway.  So we look at all
             *  character arrays in the object at the right offset and
             *  take the longest one and assume this is what is being
             *  passed.  This will be alright for all of the cases
             *  just mentioned: if it's internal or weird pointer
             *  arithmetic, it doesn't matter, and if it really is a
             *  character string, we'll be passing a string at least
             *  as large as what should be passed, which is always ok
             *  in Fortran.
             */
            if (new_type->is_same(type_char))
              {
                int new_size =
                        biggest_char_array_at_offset(var_struct,
                                                     immed_address.offset());
                if (new_size != 0)
                  {
                    array_type *new_array =
                            new array_type(new_type, array_bound(1),
                                           array_bound(new_size));
                    new_type = new_type->parent()->install_type(new_array);
                  }
              }

            unsigned num_fields = var_struct->num_fields();
            unsigned field_num;
            for (field_num = 0; field_num < num_fields; ++field_num)
              {
                if ((var_struct->offset(field_num) == immed_address.offset())
                    && (var_struct->field_type(field_num) == new_type))
                  {
                    break;
                  }
              }
            if (field_num == num_fields)
              {
                var_struct->set_num_fields(num_fields + 1);
                var_struct->set_field_name(field_num, new_name);
                var_struct->set_offset(field_num, immed_address.offset());
              }
            assert(var_struct->parent()->is_ancestor(new_type->parent()));
            var_struct->set_field_type(field_num, new_type);

            var_sym *new_var =
                    the_var->find_child(immed_address.offset(), new_type);
            if (new_var == NULL)
              {
                new_var = the_var->build_child(immed_address.offset(),
                                  new_type, var_struct->field_name(field_num));
              }
            the_var = new_var;
          }
      }
    else
      {
        /*
         *  Because sf2c casts everything to character pointers for
         *  some kinds of I/O, we can get the address of something
         *  cast to another type.
         */
        return cast_op(const_op(immed_address, var_type->ptr_to()),
                       new_type->ptr_to());
      }

    the_var->set_addr_taken();
    return const_op(immed(the_var), new_type->ptr_to());
  }

static boolean is_simple_var_addr(operand the_op, var_sym **the_var)
  {
    if (!the_op.is_expr())
        return FALSE;
    instruction *the_instr = the_op.instr();
    if (the_instr->opcode() != io_ldc)
        return FALSE;
    in_ldc *the_ldc = (in_ldc *)the_instr;
    immed value = the_ldc->value();
    if (!value.is_symbol())
        return FALSE;
    if (value.offset() != 0)
        return FALSE;
    sym_node *the_sym = value.symbol();
    if (!the_sym->is_var())
        return FALSE;
    var_sym *this_var = (var_sym *)the_sym;
    type_node *ldc_type = the_ldc->result_type()->unqual();
    if (!ldc_type->is_ptr())
        return FALSE;
    ptr_type *ldc_ptr = (ptr_type *)ldc_type;
    if (ldc_ptr->ref_type() != this_var->type())
        return FALSE;
    *the_var = this_var;
    return TRUE;
  }

static const char *new_field_name(const char *desired_name, 
				  struct_type *the_struct)
  {
    unsigned num_fields = the_struct->num_fields();

    if ((desired_name != NULL) &&
        (the_struct->find_field_by_name(desired_name) >= num_fields))
      {
        return desired_name;
      }

    const char *base = ((desired_name != NULL) ? desired_name : "");

    char *tester = new char [strlen(base) + 100];
    sprintf(tester, "%s_", base);
    char *num_place = tester + strlen(base) + 1;
    unsigned test_num = 0;
    while (TRUE)
      {
        sprintf(num_place, "%u", test_num);
        if (the_struct->find_field_by_name(tester) >= num_fields)
          {
            const char *new_name = lexicon->enter(tester)->sp;
            delete tester;
            return new_name;
          }
        assert(test_num < UINT_MAX);
        ++test_num;
      }
  }

static int biggest_char_array_at_offset(struct_type *the_struct, int offset)
  {
    int result = 0;
    unsigned num_fields = the_struct->num_fields();
    for (unsigned field_num = 0; field_num < num_fields; ++field_num)
      {
        if (the_struct->offset(field_num) == offset)
          {
            type_node *field_type =
                    the_struct->field_type(field_num)->unqual();
            if (field_type->is_array())
              {
                array_type *field_array = (array_type *)field_type;
                if (field_array->elem_type()->is_same(type_char))
                  {
                    assert(field_array->lower_bound().is_constant());
                    assert(field_array->upper_bound().is_constant());
                    int this_size =
                            field_array->upper_bound().constant() -
                            field_array->lower_bound().constant() + 1;
                    if (this_size > result)
                        result = this_size;
                  }
              }
          }
      }
    return result;
  }

static type_node *original_op_type(operand the_op)
  {
    if (the_op.is_symbol())
      {
        var_sym *the_var = the_op.symbol();
        void *data = the_var->peek_annote(k_fixfortran_original_type);
        if (data != NULL)
            return (type_node *)data;
        else
            return the_var->type()->unqual();
      }
    else
      {
        return the_op.type();
      }
  }

static void mark_common_blocks(base_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->is_var())
          {
            var_sym *this_var = (var_sym *)this_sym;
            if (this_var->type()->is_struct())
              {
                if (this_var->annotes()->peek_annote(k_common_block) == NULL)
                  {
                    struct_type *old_struct =
                            (struct_type *)(this_var->type());
                    if (old_struct->op() != TYPE_GROUP)
                      {
                        struct_type *new_group =
                                new struct_type(TYPE_GROUP, old_struct->size(),
                                                old_struct->name(), 0);
                        type_node *new_type =
                                old_struct->parent()->install_type(new_group);
                        this_var->set_type(new_type);
                      }
                    this_var->append_annote(k_common_block, NULL);
                  }
              }
          }
      }
  }

static void fix_defs(suif_object *the_object)
  {
    /*
     * Because of all the type changes we have done, some variables
     * might now have the wrong alignment set, so we reset all
     * alignments now.
     */

    if (!the_object->is_def_obj())
        return;
    var_def *this_def = (var_def *)the_object;
    type_node *this_type = this_def->variable()->type();
    this_def->set_alignment(get_alignment(this_type));
    type_node *this_unqual = this_type->unqual();
    if (this_unqual->op() == TYPE_GROUP)
      {
        fix_group_type_for_initializations((struct_type *)this_unqual,
                                           this_def);
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
