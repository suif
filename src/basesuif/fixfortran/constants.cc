/* file "constants.cc" of the fixfortran program for SUIF */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains code to mark globals that have been created by
 *  sf2c exclusively to store constant integer or floating-point
 *  values with ``constant'' annotations, so other passes know the
 *  values of these variables cannot change, so the constant
 *  initialization data can be used anywhere the variable is used.
 */

#define RCS_BASE_FILE constants_cc

#include "fixfortran.h"
#include <ctype.h>

RCS_BASE(
    "$Id: constants.cc,v 1.1.1.1 1998/06/16 15:17:22 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void mark_constants(file_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (!this_sym->is_var())
            continue;
        var_sym *this_var = (var_sym *)this_sym;
        type_node *this_type = this_var->type()->unqual();
        type_ops this_type_op = this_type->op();
        if ((this_type_op != TYPE_INT) && (this_type_op != TYPE_FLOAT))
            continue;
        const char *this_name = this_var->name();
        if ((this_name[0] != 'c') || (this_name[1] != '_') ||
            (this_name[2] != '_'))
          {
            continue;
          }
        const char *follow = &(this_name[3]);
        if (!isdigit(*follow))
           continue;
        do
          {
            ++follow;
          } while (isdigit(*follow));
        if (*follow != 0)
            continue;
        this_var->append_annote(k_is_constant);
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
