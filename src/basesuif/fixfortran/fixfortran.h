/* file "fixfortran.h" of the fixfortran program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the public declarations for fixfortran.
 */

#ifndef FIXFORTRAN_H
#define FIXFORTRAN_H

#include <suif1.h>
#include <useful.h>

RCS_HEADER(fixfortran_h,
     "$Id: fixfortran.h,v 1.1.1.1 1998/06/16 15:17:23 brm Exp $")


/*----------------------------------------------------------------------*
    Begin Global Variables
 *----------------------------------------------------------------------*/

extern const char *k_io_read;
extern const char *k_io_write;

/*----------------------------------------------------------------------*
    End Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    in file "main.cc":
 *----------------------------------------------------------------------*/

extern in_array *add_const_aref(operand array_pointer, int constant);

/*----------------------------------------------------------------------*
    in file "inline.cc":
 *----------------------------------------------------------------------*/

extern void inline_intrinsics_on_instr(instruction *the_instr, void *);

/*----------------------------------------------------------------------*
    in file "mark_io.cc":
 *----------------------------------------------------------------------*/

extern void mark_io_on_list(tree_node_list *node_list);

/*----------------------------------------------------------------------*
    in file "temp_scope.cc":
 *----------------------------------------------------------------------*/

extern void limit_complex_temp_scopes(tree_node_list *node_list);

/*----------------------------------------------------------------------*
    in file "constants.cc":
 *----------------------------------------------------------------------*/

extern void mark_constants(file_symtab *the_symtab);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/

#endif
