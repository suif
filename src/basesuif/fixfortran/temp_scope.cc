/* file "temp_scope.cc" of the fixfortran program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains code to limit the scope of temporaries of
 *  complex type that sf2c creates.  We know that sf2c will only
 *  create such variables to hold values within an expression tree in
 *  the original Fortran, so the live range of such a variable never
 *  exceeds a basic block.  But if the address of the complex
 *  temporary is passed to an intrinsic, it is a pain to figure out
 *  the scope of the temporary in the resulting code.  Since it's a
 *  complex number, the temporary has array type, so scalar analysis
 *  will not tell you anything about the scope.
 *
 *  We handle this by limiting the scope of any such temporaries to
 *  the body of the nearest enclosing loop, because what we usually
 *  care about is limiting scopes to within a loop body.  So here we
 *  create a local block to contain the loop body, if it doesn't exist
 *  already, and put the complex temporary in that block's symbol
 *  table.
 */

#define RCS_BASE_FILE temp_scope_cc

#include "fixfortran.h"
#include <string.h>

RCS_BASE(
    "$Id: temp_scope.cc,v 1.1.1.1 1998/06/16 15:17:25 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void limit_scopes_on_op(operand the_op, tree_node *the_node);
static void limit_scopes_on_instr(instruction *the_instr, tree_node *the_node);
static void limit_scopes_on_var(var_sym *the_var, tree_node *the_node);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void limit_complex_temp_scopes(tree_node_list *node_list)
  {
    tree_node_list_iter node_iter(node_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        switch (this_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)this_node;
                instruction *the_instr = the_tree_instr->instr();
                limit_scopes_on_instr(the_instr, the_tree_instr);
                break;
              }
            case TREE_LOOP:
              {
                tree_loop *the_loop = (tree_loop *)this_node;
                limit_complex_temp_scopes(the_loop->body());
                limit_complex_temp_scopes(the_loop->test());
                break;
              }
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)this_node;
                limit_scopes_on_op(the_for->lb_op(), the_for);
                limit_scopes_on_op(the_for->ub_op(), the_for);
                limit_scopes_on_op(the_for->step_op(), the_for);
                limit_complex_temp_scopes(the_for->body());
                limit_complex_temp_scopes(the_for->landing_pad());
                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)this_node;
                limit_complex_temp_scopes(the_if->header());
                limit_complex_temp_scopes(the_if->then_part());
                limit_complex_temp_scopes(the_if->else_part());
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)this_node;
                limit_complex_temp_scopes(the_block->body());
                break;
              }
            default:
                assert(FALSE);
          }
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void limit_scopes_on_op(operand the_op, tree_node *the_node)
  {
    if (the_op.is_expr())
        limit_scopes_on_instr(the_op.instr(), the_node);
    else if (the_op.is_symbol())
        limit_scopes_on_var(the_op.symbol(), the_node);
  }

static void limit_scopes_on_instr(instruction *the_instr, tree_node *the_node)
  {
    if (the_instr->dst_op().is_symbol())
        limit_scopes_on_var(the_instr->dst_op().symbol(), the_node);

    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        immed value = the_ldc->value();
        if (value.is_symbol())
          {
            sym_node *the_sym = value.symbol();
            if (the_sym->is_var())
                limit_scopes_on_var((var_sym *)the_sym, the_node);
          }
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
        limit_scopes_on_op(the_instr->src_op(src_num), the_node);
  }

static void limit_scopes_on_var(var_sym *the_var, tree_node *the_node)
  {
    const char *var_name = the_var->name();
    if ((strlen(var_name) < 3) || (var_name[1] != '_') ||
        ((var_name[0] != 'q') && (var_name[0] != 'z')) ||
        the_var->is_global() || (!the_var->parent()->is_proc()) ||
        (!the_var->is_auto()))
      {
        return;
      }

    for (int var_place = 2; var_name[var_place] != 0; ++var_place)
      {
        if ((var_name[var_place] < '0') || (var_name[var_place] > '9'))
            return;
      }

    tree_block *new_block;
    tree_node_list *follow_list = the_node->parent();
    while (TRUE)
      {
        assert(follow_list != NULL);
        tree_node *next_node = follow_list->parent();
        assert(next_node != NULL);
        if (next_node->is_for())
          {
            tree_for *the_for = (tree_for *)next_node;
            if (the_for->body() == follow_list)
              {
                block_symtab *new_symtab = new block_symtab("_");
                new_block = new tree_block(follow_list, new_symtab);
                tree_node_list *new_body = new tree_node_list;
                new_body->append(new_block);
                the_for->set_body(new_body);

                base_symtab *scope = the_for->scope();
                base_symtab_list_iter table_iter(scope->children());
                while (!table_iter.is_empty())
                  {
                    base_symtab *child = table_iter.step();
                    assert(child->is_block());
                    block_symtab *block_tab = (block_symtab *)child;
                    base_symtab *child_scope = block_tab->block()->scope();
                    if (child_scope == new_symtab)
                      {
                        scope->remove_child(child);
                        new_symtab->add_child(child);
                      }
                  }
                scope->add_child(new_symtab);
                break;
              }
          }
        if (next_node->is_block())
          {
            new_block = (tree_block *)next_node;
            break;
          }
        follow_list = next_node->parent();
      }

    the_var->parent()->remove_sym(the_var);
    new_block->symtab()->add_sym(the_var);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
