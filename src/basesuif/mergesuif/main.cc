/* file "main.cc" of the mergesuif program for SUIF */ 

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for mergesuif.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: main.cc,v 1.2 1999/08/25 03:27:22 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void prepare_transfer(file_set_entry *in_fse, file_set_entry *out_fse);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if (argc < 3)
        usage();

    for (int arg_num = 1; arg_num < argc - 1; ++arg_num)
        fileset->add_file(argv[arg_num], NULL);

    file_set_entry *out_fse = fileset->add_file(NULL, argv[argc - 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *in_fse = fileset->next_file();
        if (in_fse == NULL)
            break;
        if (in_fse == out_fse)
            continue;
        prepare_transfer(in_fse, out_fse);
        in_fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = in_fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            tree_proc *this_tree_proc = this_proc_sym->block();
            do_replacement(this_tree_proc);
            in_fse->symtab()->remove_child(this_tree_proc->proc_syms());
            out_fse->symtab()->add_child(this_tree_proc->proc_syms());
            this_tree_proc->proc()->write_proc(out_fse);
            this_tree_proc->proc()->flush_proc();
          }
      }

    do_replacement(out_fse);
    do_replacement(fileset->globals());
    enable_automatic_renaming();

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr, "usage: %s <infile> { <infile> }* <outfile>\n",
            _suif_prog_base_name);
    exit(1);
  }

static void prepare_transfer(file_set_entry *in_fse, file_set_entry *out_fse)
  {
    while (!in_fse->annotes()->is_empty())
      {
	annote *this_annote = in_fse->annotes()->pop();
	if (this_annote->name() == k_history
	 || this_annote->name() == k_version_history)
	    delete this_annote;
	else
            out_fse->annotes()->append(this_annote);
      }

    file_symtab *in_symtab = in_fse->symtab();
    file_symtab *out_symtab = out_fse->symtab();

    while (!in_symtab->annotes()->is_empty())
      {
        annote *this_annote = in_symtab->annotes()->pop();
        out_symtab->annotes()->append(this_annote);
      }

    sym_node_list_iter sym_iter(in_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *in_sym = sym_iter.step();
        if (in_sym->annotes()->peek_annote(k_replacement) != NULL)
          {
            error_line(1, in_sym, "symbol already has \"%s\" annotation",
                       k_replacement);
          }
        sym_node *out_sym = in_sym->copy();
        in_sym->copy_annotes(out_sym);
        out_symtab->add_sym(out_sym);
        in_sym->append_annote(k_replacement, new immed_list(immed(out_sym)));
      }

    type_node_list_iter type_iter(in_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *in_type = type_iter.step();
        if (in_type->annotes()->peek_annote(k_replacement) != NULL)
          {
            error_line(1, in_type, "type already has \"%s\" annotation",
                       k_replacement);
          }
        type_node *out_type = in_type->copy();
        in_type->copy_annotes(out_type);
        out_symtab->add_type(out_type);
        in_type->append_annote(k_replacement, new immed_list(immed(out_type)));
      }

    while (!in_symtab->var_defs()->is_empty())
      {
        var_def *the_def = in_symtab->var_defs()->head()->contents;
        in_symtab->remove_def(the_def);
        do_replacement(the_def);
        out_symtab->add_def(the_def);
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
