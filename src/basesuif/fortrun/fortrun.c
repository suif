/* file "fortrun.c" */


/*
       Copyright (c) 1996 Stanford University

       All rights reserved.

       This software is provided under the terms described in
       the "suif_copyright.h" include file.
*/

#include <suif_copyright.h>


#include "fortrun.h"
#include <math.h>
#include <assert.h>



Integer fr_int_i(Integer arg)
  {
    return arg;
  }

Integer fr_int_r(Real arg)
  {
    return (Integer)arg;
  }

Integer fr_ifix(Real arg)
  {
    return (Integer)arg;
  }

Integer fr_idint(Double arg)
  {
    return (Integer)arg;
  }

Integer fr_int_cx(Complex arg)
  {
    return (Integer)(arg._r);
  }


Integer fr_int_i_cbr(arg)
    Integer *arg;
  {
    return *arg;
  }

Integer fr_int_r_cbr(arg)
    Real *arg;
  {
    return (Integer)(*arg);
  }

Integer fr_ifix_cbr(arg)
    Real *arg;
  {
    return (Integer)(*arg);
  }

Integer fr_idint_cbr(arg)
    Double *arg;
  {
    return (Integer)(*arg);
  }

Integer fr_int_cx_cbr(arg)
    Complex *arg;
  {
    return (Integer)(arg->_r);
  }



Real fr_real_i(Integer arg)
  {
    return (Real)arg;
  }

Real fr_float(Integer arg)
  {
    return (Real)arg;
  }

Real fr_real_r(Real arg)
  {
    return arg;
  }

Real fr_sngl(Double arg)
  {
    return (Real)arg;
  }

Real fr_real_cx(Complex arg)
  {
    return (Real)(arg._r);
  }


Real fr_real_i_cbr(arg)
    Integer *arg;
  {
    return (Real)(*arg);
  }

Real fr_float_cbr(arg)
    Integer *arg;
  {
    return (Real)(*arg);
  }

Real fr_real_r_cbr(arg)
    Real *arg;
  {
    return *arg;
  }

Real fr_sngl_cbr(arg)
    Double *arg;
  {
    return (Real)(*arg);
  }

Real fr_real_cx_cbr(arg)
    Complex *arg;
  {
    return (Real)(arg->_r);
  }



Double fr_dble_i(Integer arg)
  {
    return (Double)arg;
  }

Double fr_dble_r(Real arg)
  {
    return (Double)arg;
  }

Double fr_dble_d(Double arg)
  {
    return arg;
  }

Double fr_dble_cx(Complex arg)
  {
    return (Double)(arg._r);
  }


Double fr_dble_i_cbr(arg)
    Integer *arg;
  {
    return (Double)(*arg);
  }

Double fr_dble_r_cbr(arg)
    Real *arg;
  {
    return (Double)(*arg);
  }

Double fr_dble_d_cbr(arg)
    Double *arg;
  {
    return *arg;
  }

Double fr_dble_cx_cbr(arg)
    Complex *arg;
  {
    return (Double)(arg->_r);
  }



Complex fr_cmplx_i(Integer arg)
  {
    Complex result;

    result._r = (Real)arg;
    result._i = 0.0;
    return result;
  }

Complex fr_cmplx_i_i(Integer arg1, Integer arg2)
  {
    Complex result;

    result._r = (Real)arg1;
    result._i = (Real)arg2;
    return result;
  }

Complex fr_cmplx_r(Real arg)
  {
    Complex result;

    result._r = arg;
    result._i = 0.0;
    return result;
  }

Complex fr_cmplx_r_r(Real arg1, Real arg2)
  {
    Complex result;

    result._r = arg1;
    result._i = arg2;
    return result;
  }

Complex fr_cmplx_d(Double arg)
  {
    Complex result;

    result._r = (Real)arg;
    result._i = 0.0;
    return result;
  }

Complex fr_cmplx_d_d(Double arg1, Double arg2)
  {
    Complex result;

    result._r = (Real)arg1;
    result._i = (Real)arg2;
    return result;
  }

Complex fr_cmplx_cx(Complex arg)
  {
    return arg;
  }


Complex fr_cmplx_i_cbr(arg)
    Integer *arg;
  {
    Complex result;

    result._r = (Real)(*arg);
    result._i = 0.0;
    return result;
  }

Complex fr_cmplx_i_i_cbr(arg1, arg2)
    Integer *arg1;
    Integer *arg2;
  {
    Complex result;

    result._r = (Real)(*arg1);
    result._i = (Real)(*arg2);
    return result;
  }

Complex fr_cmplx_r_cbr(arg)
    Real *arg;
  {
    Complex result;

    result._r = *arg;
    result._i = 0.0;
    return result;
  }

Complex fr_cmplx_r_r_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    Complex result;

    result._r = *arg1;
    result._i = *arg2;
    return result;
  }

Complex fr_cmplx_d_cbr(arg)
    Double *arg;
  {
    Complex result;

    result._r = (Real)(*arg);
    result._i = 0.0;
    return result;
  }

Complex fr_cmplx_d_d_cbr(arg1, arg2)
    Double *arg1;
    Double *arg2;
  {
    Complex result;

    result._r = (Real)(*arg1);
    result._i = (Real)(*arg2);
    return result;
  }

Complex fr_cmplx_cx_cbr(arg)
    Complex *arg;
  {
    return *arg;
  }



Integer fr_ichar(char *base, charcount_t length)
  {
    assert(length == 1);
    return (Integer)(base[0]);
  }


Integer fr_ichar_cbr(base, length)
    char *base;
    charcount_t length;
  {
    assert(length == 1);
    return (Integer)(base[0]);
  }



void fr_char(char *result_base, charcount_t result_length, Integer integer_arg)
  {
    assert(result_length == 1);
    result_base[0] = (char)integer_arg;
  }


void fr_char_cbr(result_base, result_length, integer_arg)
    char *result_base;
    charcount_t result_length;
    Integer *integer_arg;
  {
    assert(result_length == 1);
    result_base[0] = (char)(*integer_arg);
  }



Real fr_aint(Real arg)
  {
    return ((arg >= 0.0) ? (Real)(floor((double)arg)) :
                           (Real)(ceil((double)arg)));
  }

Double fr_dint(Double arg)
  {
    return ((arg >= 0.0) ? (Double)(floor((double)arg)) :
                           (Double)(ceil((double)arg)));
  }


Real fr_aint_cbr(arg)
    Real *arg;
  {
    return ((*arg >= 0.0) ? (Real)(floor((double)(*arg))) :
                            (Real)(ceil((double)(*arg))));
  }

Double fr_dint_cbr(arg)
    Double *arg;
  {
    return ((*arg >= 0.0) ? (Double)(floor((double)(*arg))) :
                            (Double)(ceil((double)(*arg))));
  }



Real fr_anint(Real arg)
  {
    if (arg >= 0.0)
        return (Real)(floor((double)arg + 0.5));
    else
        return (Real)(ceil((double)arg - 0.5));
  }

Double fr_dnint(Double arg)
  {
    if (arg >= 0.0)
        return (Double)(floor((double)arg + 0.5));
    else
        return (Double)(ceil((double)arg - 0.5));
  }


Real fr_anint_cbr(arg)
    Real *arg;
  {
    if (*arg >= 0.0)
        return (Real)(floor((double)(*arg) + 0.5));
    else
        return (Real)(ceil((double)(*arg) - 0.5));
  }

Double fr_dnint_cbr(arg)
    Double *arg;
  {
    if (*arg >= 0.0)
        return (Double)(floor((double)(*arg) + 0.5));
    else
        return (Double)(ceil((double)(*arg) - 0.5));
  }



Integer fr_nint(Real arg)
  {
    if (arg >= 0.0)
        return (Integer)(arg + 0.5);
    else
        return (Integer)(arg - 0.5);
  }

Integer fr_idnint(Double arg)
  {
    if (arg >= 0.0)
        return (Integer)(arg + 0.5);
    else
        return (Integer)(arg - 0.5);
  }


Integer fr_nint_cbr(arg)
    Real *arg;
  {
    if (*arg >= 0.0)
        return (Integer)((*arg) + 0.5);
    else
        return (Integer)((*arg) - 0.5);
  }

Integer fr_idnint_cbr(arg)
    Double *arg;
  {
    if (*arg >= 0.0)
        return (Integer)((*arg) + 0.5);
    else
        return (Integer)((*arg) - 0.5);
  }



Integer fr_iabs(Integer arg)
  {
    return ((arg >= 0) ? arg : -arg);
  }

Real fr_abs(Real arg)
  {
    return ((arg >= 0) ? arg : -arg);
  }

Double fr_dabs(Double arg)
  {
    return ((arg >= 0) ? arg : -arg);
  }

Real fr_cabs(Complex arg)
  {
    double part1;
    double part2;

    /*
     *  Note: this function returns the square root of the sum of the
     *  squares of the real and imaginary parts.  But to avoid
     *  overflow, we rearrange the computation.  We want to avoid
     *  having the square root term blow the magnitude up too far.  We
     *  do so by pulling a factor out of the square root.  We choose
     *  as that factor the magnitude of either the real or imaginary
     *  part, whichever is larger.
     */
/*
    return (Real)(sqrt((((double)(arg._r)) * ((double)(arg._r))) +
                       (((double)(arg._i)) * ((double)(arg._i)))));
*/
    part1 = ((arg._r >= 0.0) ? arg._r : -arg._r);
    part2 = ((arg._i >= 0.0) ? arg._i : -arg._i);
    if (part1 < part2)
      {
        double temp_part;

        temp_part = part1;
        part1 = part2;
        part2 = temp_part;
      }
    if (part1 == 0.0)
        return (Real)0.0;
    part2 = part2/part1;
    return (Real)(part1 * sqrt(1.0 + (part2 * part2)));
  }


Integer fr_iabs_cbr(arg)
    Integer *arg;
  {
    return ((*arg >= 0) ? *arg : -(*arg));
  }

Real fr_abs_cbr(arg)
    Real *arg;
  {
    return ((*arg >= 0) ? *arg : -(*arg));
  }

Double fr_dabs_cbr(arg)
    Double *arg;
  {
    return ((*arg >= 0) ? *arg : -(*arg));
  }

Real fr_cabs_cbr(arg)
    Complex *arg;
  {
    return fr_cabs(*arg);
  }



Integer fr_mod(Integer arg1, Integer arg2)
  {
    return ((arg1 >= 0) ?
               ((arg2 >= 0) ?
                   (arg1 % arg2) :
                   (arg1 % (-arg2))) :
               ((arg2 >= 0) ?
                   -((-arg1) % arg2) :
                   -((-arg1) % (-arg2))));
  }

Real fr_amod(Real arg1, Real arg2)
  {
    Real div;

    div = arg1/arg2;
    return ((div >= 0.0) ? (arg1 - ((Real)(floor((double)div))) * arg2) :
                           (arg1 - ((Real)(ceil((double)div))) * arg2));
  }

Double fr_dmod(Double arg1, Double arg2)
  {
    Double div;

    div = arg1/arg2;
    return ((div >= 0.0) ? (arg1 - ((Double)(floor((double)div))) * arg2) :
                           (arg1 - ((Double)(ceil((double)div))) * arg2));
  }


Integer fr_mod_cbr(arg1, arg2)
    Integer *arg1;
    Integer *arg2;
  {
    return ((*arg1 >= 0) ?
               ((*arg2 >= 0) ?
                   (*arg1 % *arg2) :
                   (*arg1 % (-*arg2))) :
               ((*arg2 >= 0) ?
                   -((-*arg1) % *arg2) :
                   -((-*arg1) % (-*arg2))));
  }

Real fr_amod_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    Real div;

    div = (*arg1)/(*arg2);
    return ((div >= 0.0) ? ((*arg1) - ((Real)(floor((double)div))) * (*arg2)) :
                           ((*arg1) - ((Real)(ceil((double)div))) * (*arg2)));
  }

Double fr_dmod_cbr(arg1, arg2)
    Double *arg1;
    Double *arg2;
  {
    Double div;

    div = (*arg1)/(*arg2);
    return ((div >= 0.0) ?
            ((*arg1) - ((Double)(floor((double)div))) * (*arg2)) :
            ((*arg1) - ((Double)(ceil((double)div))) * (*arg2)));
  }



Integer fr_isign(Integer arg1, Integer arg2)
  {
    return ((arg2 >= 0) ? ((arg1 >= 0) ? arg1 : -arg1) :
                          ((arg1 <= 0) ? arg1 : -arg1));
  }

Real fr_sign(Real arg1, Real arg2)
  {
    return ((arg2 >= 0) ? ((arg1 >= 0) ? arg1 : -arg1) :
                          ((arg1 <= 0) ? arg1 : -arg1));
  }

Double fr_dsign(Double arg1, Double arg2)
  {
    return ((arg2 >= 0) ? ((arg1 >= 0) ? arg1 : -arg1) :
                          ((arg1 <= 0) ? arg1 : -arg1));
  }


Integer fr_isign_cbr(arg1, arg2)
    Integer *arg1;
    Integer *arg2;
  {
    return ((*arg2 >= 0) ? ((*arg1 >= 0) ? *arg1 : -(*arg1)) :
                           ((*arg1 <= 0) ? *arg1 : -(*arg1)));
  }

Real fr_sign_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    return ((*arg2 >= 0) ? ((*arg1 >= 0) ? *arg1 : -(*arg1)) :
                           ((*arg1 <= 0) ? *arg1 : -(*arg1)));
  }

Double fr_dsign_cbr(arg1, arg2)
    Double *arg1;
    Double *arg2;
  {
    return ((*arg2 >= 0) ? ((*arg1 >= 0) ? *arg1 : -(*arg1)) :
                           ((*arg1 <= 0) ? *arg1 : -(*arg1)));
  }



Integer fr_idim(Integer arg1, Integer arg2)
  {
    return ((arg1 > arg2) ? (arg1 - arg2) : 0);
  }

Real fr_dim(Real arg1, Real arg2)
  {
    return ((arg1 > arg2) ? (arg1 - arg2) : 0.0);
  }

Double fr_ddim(Double arg1, Double arg2)
  {
    return ((arg1 > arg2) ? (arg1 - arg2) : 0.0);
  }


Integer fr_idim_cbr(arg1, arg2)
    Integer *arg1;
    Integer *arg2;
  {
    return ((*arg1 > *arg2) ? (*arg1 - *arg2) : 0);
  }

Real fr_dim_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    return ((*arg1 > *arg2) ? (*arg1 - *arg2) : 0.0);
  }

Double fr_ddim_cbr(arg1, arg2)
    Double *arg1;
    Double *arg2;
  {
    return ((*arg1 > *arg2) ? (*arg1 - *arg2) : 0.0);
  }



Double fr_dprod(Real arg1, Real arg2)
  {
    return ((Double)arg1) * ((Double)arg2);
  }


Double fr_dprod_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    return ((Double)(*arg1)) * ((Double)(*arg2));
  }



Integer fr_max0(Integer arg1, Integer arg2)
  {
    return ((arg1 >= arg2) ? arg1 : arg2);
  }

Real fr_amax1(Real arg1, Real arg2)
  {
    return ((arg1 >= arg2) ? arg1 : arg2);
  }

Double fr_dmax1(Double arg1, Double arg2)
  {
    return ((arg1 >= arg2) ? arg1 : arg2);
  }

Real fr_amax0(Integer arg1, Integer arg2)
  {
    return (Real)((arg1 >= arg2) ? arg1 : arg2);
  }

Integer fr_max1(Real arg1, Real arg2)
  {
    return (Integer)((arg1 >= arg2) ? arg1 : arg2);
  }


Integer fr_max0_cbr(arg1, arg2)
    Integer *arg1;
    Integer *arg2;
  {
    return ((*arg1 >= *arg2) ? *arg1 : *arg2);
  }

Real fr_amax1_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    return ((*arg1 >= *arg2) ? *arg1 : *arg2);
  }

Double fr_dmax1_cbr(arg1, arg2)
    Double *arg1;
    Double *arg2;
  {
    return ((*arg1 >= *arg2) ? *arg1 : *arg2);
  }

Real fr_amax0_cbr(arg1, arg2)
    Integer *arg1;
    Integer *arg2;
  {
    return (Real)((*arg1 >= *arg2) ? *arg1 : *arg2);
  }

Integer fr_max1_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    return (Integer)((*arg1 >= *arg2) ? *arg1 : *arg2);
  }



Integer fr_min0(Integer arg1, Integer arg2)
  {
    return ((arg1 <= arg2) ? arg1 : arg2);
  }

Real fr_amin1(Real arg1, Real arg2)
  {
    return ((arg1 <= arg2) ? arg1 : arg2);
  }

Double fr_dmin1(Double arg1, Double arg2)
  {
    return ((arg1 <= arg2) ? arg1 : arg2);
  }

Real fr_amin0(Integer arg1, Integer arg2)
  {
    return (Real)((arg1 <= arg2) ? arg1 : arg2);
  }

Integer fr_min1(Real arg1, Real arg2)
  {
    return (Integer)((arg1 <= arg2) ? arg1 : arg2);
  }


Integer fr_min0_cbr(arg1, arg2)
    Integer *arg1;
    Integer *arg2;
  {
    return ((*arg1 <= *arg2) ? *arg1 : *arg2);
  }

Real fr_amin1_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    return ((*arg1 <= *arg2) ? *arg1 : *arg2);
  }

Double fr_dmin1_cbr(arg1, arg2)
    Double *arg1;
    Double *arg2;
  {
    return ((*arg1 <= *arg2) ? *arg1 : *arg2);
  }

Real fr_amin0_cbr(arg1, arg2)
    Integer *arg1;
    Integer *arg2;
  {
    return (Real)((*arg1 <= *arg2) ? *arg1 : *arg2);
  }

Integer fr_min1_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    return (Integer)((*arg1 <= *arg2) ? *arg1 : *arg2);
  }



Integer fr_len(char *base, charcount_t length)
  {
    return (Integer)length;
  }


Integer fr_len_cbr(base, length)
    char *base;
    charcount_t length;
  {
    return (Integer)length;
  }



Integer fr_index(char *arg1_base, charcount_t arg1_length,
                 char *arg2_base, charcount_t arg2_length)
  {
    charcount_t position1;

    for (position1 = 0; position1 + arg2_length <= arg1_length; ++position1)
      {
        charcount_t position2;

        position2 = 0;
        while (arg1_base[position1 + position2] == arg2_base[position2])
          {
            if (position2 + 1 == arg2_length)
                return (Integer)(position1 + 1);
            ++position2;
          }
      }
    return (Integer)0;
  }


Integer fr_index_cbr(arg1_base, arg1_length, arg2_base, arg2_length)
    char *arg1_base;
    charcount_t arg1_length;
    char *arg2_base;
    charcount_t arg2_length;
  {
    charcount_t position1;

    for (position1 = 0; position1 + arg2_length <= arg1_length; ++position1)
      {
        charcount_t position2;

        position2 = 0;
        while (arg1_base[position1 + position2] == arg2_base[position2])
          {
            if (position2 + 1 == arg2_length)
                return (Integer)(position1 + 1);
            ++position2;
          }
      }
    return (Integer)0;
  }



Real fr_aimag(Complex arg)
  {
    return arg._i;
  }


Real fr_aimag_cbr(arg)
    Complex *arg;
  {
    return arg->_i;
  }



Complex fr_conjg(Complex arg)
  {
    Complex result;

    result._r = arg._r;
    result._i = -(arg._i);
    return result;
  }


Complex fr_conjg_cbr(arg)
    Complex *arg;
  {
    Complex result;

    result._r = arg->_r;
    result._i = -(arg->_i);
    return result;
  }



Real fr_sqrt(Real arg)
  {
    return (Real)(sqrt((double)arg));
  }

Double fr_dsqrt(Double arg)
  {
    return (Double)(sqrt((double)arg));
  }

Complex fr_csqrt(Complex arg)
  {
    Complex result;

    result._i = sqrt((-arg._r + (double)(fr_cabs(arg)))/2);
    result._r = ((result._i > 0.0) ? (arg._i/(2 * result._i)) : 0.0);
    if (result._r < 0.0)
      {
        result._r = -result._r;
        result._i = -result._i;
      }
    else if ((result._r == 0.0) && (result._i < 0.0))
      {
        result._i = -result._i;
      }
    return result;
  }


Real fr_sqrt_cbr(arg)
    Real *arg;
  {
    return (Real)(sqrt((double)(*arg)));
  }

Double fr_dsqrt_cbr(arg)
    Double *arg;
  {
    return (Double)(sqrt((double)(*arg)));
  }

Complex fr_csqrt_cbr(arg)
    Complex *arg;
  {
    Complex result;

    result._i = sqrt((-(arg->_r) + (double)(fr_cabs(*arg)))/2);
    result._r = ((result._i > 0.0) ? (arg->_i/(2 * result._i)) : 0.0);
    if (result._r < 0.0)
      {
        result._r = -result._r;
        result._i = -result._i;
      }
    else if ((result._r == 0.0) && (result._i < 0.0))
      {
        result._i = -result._i;
      }
    return result;
  }



Real fr_exp(Real arg)
  {
    return (Real)(exp((double)arg));
  }

Double fr_dexp(Double arg)
  {
    return (Double)(exp((double)arg));
  }

Complex fr_cexp(Complex arg)
  {
    Complex result;
    double result_magnitude;

    result_magnitude = exp((double)arg._r);
    result._r = (Real)(result_magnitude * cos((double)arg._i));
    result._i = (Real)(result_magnitude * sin((double)arg._i));
    return result;
  }


Real fr_exp_cbr(arg)
    Real *arg;
  {
    return (Real)(exp((double)(*arg)));
  }

Double fr_dexp_cbr(arg)
    Double *arg;
  {
    return (Double)(exp((double)(*arg)));
  }

Complex fr_cexp_cbr(arg)
    Complex *arg;
  {
    Complex result;
    double result_magnitude;

    result_magnitude = exp((double)arg->_r);
    result._r = (Real)(result_magnitude * cos((double)arg->_i));
    result._i = (Real)(result_magnitude * sin((double)arg->_i));
    return result;
  }



Real fr_alog(Real arg)
  {
    return (Real)(log((double)arg));
  }

Double fr_dlog(Double arg)
  {
    return (Double)(log((double)arg));
  }

Complex fr_clog(Complex arg)
  {
    Complex result;

    result._r = (Real)(log((double)(fr_cabs(arg))));
    result._i = (Real)(atan2((double)arg._i, (double)arg._r));
    return result;
  }


Real fr_alog_cbr(arg)
    Real *arg;
  {
    return (Real)(log((double)(*arg)));
  }

Double fr_dlog_cbr(arg)
    Double *arg;
  {
    return (Double)(log((double)(*arg)));
  }

Complex fr_clog_cbr(arg)
    Complex *arg;
  {
    Complex result;

    result._r = (Real)(log((double)(fr_cabs(*arg))));
    result._i = (Real)(atan2((double)arg->_i, (double)arg->_r));
    return result;
  }



Real fr_alog10(Real arg)
  {
    return (Real)(log10((double)arg));
  }

Double fr_dlog10(Double arg)
  {
    return (Real)(log10((double)arg));
  }


Real fr_alog10_cbr(arg)
    Real *arg;
  {
    return (Real)(log10((double)(*arg)));
  }

Double fr_dlog10_cbr(arg)
    Double *arg;
  {
    return (Double)(log10((double)(*arg)));
  }



Real fr_sin(Real arg)
  {
    return (Real)(sin((double)arg));
  }

Double fr_dsin(Double arg)
  {
    return (Double)(sin((double)arg));
  }

Complex fr_csin(Complex arg)
  {
    Complex result;

    result._r = (Real)(sin((double)arg._r) * cosh((double)arg._i));
    result._i = (Real)(cos((double)arg._r) * sinh((double)arg._i));
    return result;
  }


Real fr_sin_cbr(arg)
    Real *arg;
  {
    return (Real)(sin((double)(*arg)));
  }

Double fr_dsin_cbr(arg)
    Double *arg;
  {
    return (Double)(sin((double)(*arg)));
  }

Complex fr_csin_cbr(arg)
    Complex *arg;
  {
    Complex result;

    result._r = (Real)(sin((double)arg->_r) * cosh((double)arg->_i));
    result._i = (Real)(cos((double)arg->_r) * sinh((double)arg->_i));
    return result;
  }



Real fr_cos(Real arg)
  {
    return (Real)(cos((double)arg));
  }

Double fr_dcos(Double arg)
  {
    return (Double)(cos((double)arg));
  }

Complex fr_ccos(Complex arg)
  {
    Complex result;

    result._r = (Real)(cos((double)arg._r) * cosh((double)arg._i));
    result._i = (Real)(-(sin((double)arg._r) * sinh((double)arg._i)));
    return result;
  }


Real fr_cos_cbr(arg)
    Real *arg;
  {
    return (Real)(cos((double)(*arg)));
  }

Double fr_dcos_cbr(arg)
    Double *arg;
  {
    return (Double)(cos((double)(*arg)));
  }

Complex fr_ccos_cbr(arg)
    Complex *arg;
  {
    Complex result;

    result._r = (Real)(cos((double)arg->_r) * cosh((double)arg->_i));
    result._i = (Real)(-(sin((double)arg->_r) * sinh((double)arg->_i)));
    return result;
  }



Real fr_tan(Real arg)
  {
    return (Real)(tan((double)arg));
  }

Double fr_dtan(Double arg)
  {
    return (Double)(tan((double)arg));
  }


Real fr_tan_cbr(arg)
    Real *arg;
  {
    return (Real)(tan((double)(*arg)));
  }

Double fr_dtan_cbr(arg)
    Double *arg;
  {
    return (Double)(tan((double)(*arg)));
  }



Real fr_asin(Real arg)
  {
    return (Real)(asin((double)arg));
  }

Double fr_dasin(Double arg)
  {
    return (Double)(asin((double)arg));
  }


Real fr_asin_cbr(arg)
    Real *arg;
  {
    return (Real)(asin((double)(*arg)));
  }

Double fr_dasin_cbr(arg)
    Double *arg;
  {
    return (Double)(asin((double)(*arg)));
  }



Real fr_acos(Real arg)
  {
    return (Real)(acos((double)arg));
  }

Double fr_dacos(Double arg)
  {
    return (Double)(acos((double)arg));
  }


Real fr_acos_cbr(arg)
    Real *arg;
  {
    return (Real)(acos((double)(*arg)));
  }

Double fr_dacos_cbr(arg)
    Double *arg;
  {
    return (Double)(acos((double)(*arg)));
  }



Real fr_atan(Real arg)
  {
    return (Real)(atan((double)arg));
  }

Double fr_datan(Double arg)
  {
    return (Double)(atan((double)arg));
  }


Real fr_atan_cbr(arg)
    Real *arg;
  {
    return (Real)(atan((double)(*arg)));
  }

Double fr_datan_cbr(arg)
    Double *arg;
  {
    return (Double)(atan((double)(*arg)));
  }



Real fr_atan2(Real arg1, Real arg2)
  {
    return (Real)(atan2((double)arg1, (double)arg2));
  }

Double fr_datan2(Double arg1, Double arg2)
  {
    return (Double)(atan2((double)arg1, (double)arg2));
  }


Real fr_atan2_cbr(arg1, arg2)
    Real *arg1;
    Real *arg2;
  {
    return (Real)(atan2((double)(*arg1), (double)(*arg2)));
  }

Double fr_datan2_cbr(arg1, arg2)
    Double *arg1;
    Double *arg2;
  {
    return (Double)(atan2((double)(*arg1), (double)(*arg2)));
  }



Real fr_sinh(Real arg)
  {
    return (Real)(sinh((double)arg));
  }

Double fr_dsinh(Double arg)
  {
    return (Double)(sinh((double)arg));
  }


Real fr_sinh_cbr(arg)
    Real *arg;
  {
    return (Real)(sinh((double)(*arg)));
  }

Double fr_dsinh_cbr(arg)
    Double *arg;
  {
    return (Double)(sinh((double)(*arg)));
  }



Real fr_cosh(Real arg)
  {
    return (Real)(cosh((double)arg));
  }

Double fr_dcosh(Double arg)
  {
    return (Double)(cosh((double)arg));
  }


Real fr_cosh_cbr(arg)
    Real *arg;
  {
    return (Real)(cosh((double)(*arg)));
  }

Double fr_dcosh_cbr(arg)
    Double *arg;
  {
    return (Double)(cosh((double)(*arg)));
  }



Real fr_tanh(Real arg)
  {
    return (Real)(tanh((double)arg));
  }

Double fr_dtanh(Double arg)
  {
    return (Double)(tanh((double)arg));
  }


Real fr_tanh_cbr(arg)
    Real *arg;
  {
    return (Real)(tanh((double)(*arg)));
  }

Double fr_dtanh_cbr(arg)
    Double *arg;
  {
    return (Double)(tanh((double)(*arg)));
  }



Logical fr_lge(char *arg1_base, charcount_t arg1_length,
               char *arg2_base, charcount_t arg2_length)
  {
    charcount_t min_length;
    charcount_t position;

    min_length = ((arg1_length <= arg2_length) ? arg1_length : arg2_length);
    for (position = 0; position < min_length; ++position)
      {
        if (arg1_base[position] > arg2_base[position])
            return True;
        if (arg1_base[position] < arg2_base[position])
            return False;
      }
    if (position < arg1_length)
      {
        while (position < arg1_length)
          {
            if (arg1_base[position] > ' ')
                return True;
            if (arg1_base[position] < ' ')
                return False;
            ++position;
          }
      }
    else if (position < arg2_length)
      {
        while (position < arg2_length)
          {
            if (' ' > arg2_base[position])
                return True;
            if (' ' < arg2_base[position])
                return False;
            ++position;
          }
      }
    return True;
  }


Logical fr_lge_cbr(arg1_base, arg1_length, arg2_base, arg2_length)
    char *arg1_base;
    charcount_t arg1_length;
    char *arg2_base;
    charcount_t arg2_length;
  {
    charcount_t min_length;
    charcount_t position;

    min_length = ((arg1_length <= arg2_length) ? arg1_length : arg2_length);
    for (position = 0; position < min_length; ++position)
      {
        if (arg1_base[position] > arg2_base[position])
            return True;
        if (arg1_base[position] < arg2_base[position])
            return False;
      }
    if (position < arg1_length)
      {
        while (position < arg1_length)
          {
            if (arg1_base[position] > ' ')
                return True;
            if (arg1_base[position] < ' ')
                return False;
            ++position;
          }
      }
    else if (position < arg2_length)
      {
        while (position < arg2_length)
          {
            if (' ' > arg2_base[position])
                return True;
            if (' ' < arg2_base[position])
                return False;
            ++position;
          }
      }
    return True;
  }



Logical fr_lgt(char *arg1_base, charcount_t arg1_length,
               char *arg2_base, charcount_t arg2_length)
  {
    charcount_t min_length;
    charcount_t position;

    min_length = ((arg1_length <= arg2_length) ? arg1_length : arg2_length);
    for (position = 0; position < min_length; ++position)
      {
        if (arg1_base[position] > arg2_base[position])
            return True;
        if (arg1_base[position] < arg2_base[position])
            return False;
      }
    if (position < arg1_length)
      {
        while (position < arg1_length)
          {
            if (arg1_base[position] > ' ')
                return True;
            if (arg1_base[position] < ' ')
                return False;
            ++position;
          }
      }
    else if (position < arg2_length)
      {
        while (position < arg2_length)
          {
            if (' ' > arg2_base[position])
                return True;
            if (' ' < arg2_base[position])
                return False;
            ++position;
          }
      }
    return False;
  }


Logical fr_lgt_cbr(arg1_base, arg1_length, arg2_base, arg2_length)
    char *arg1_base;
    charcount_t arg1_length;
    char *arg2_base;
    charcount_t arg2_length;
  {
    charcount_t min_length;
    charcount_t position;

    min_length = ((arg1_length <= arg2_length) ? arg1_length : arg2_length);
    for (position = 0; position < min_length; ++position)
      {
        if (arg1_base[position] > arg2_base[position])
            return True;
        if (arg1_base[position] < arg2_base[position])
            return False;
      }
    if (position < arg1_length)
      {
        while (position < arg1_length)
          {
            if (arg1_base[position] > ' ')
                return True;
            if (arg1_base[position] < ' ')
                return False;
            ++position;
          }
      }
    else if (position < arg2_length)
      {
        while (position < arg2_length)
          {
            if (' ' > arg2_base[position])
                return True;
            if (' ' < arg2_base[position])
                return False;
            ++position;
          }
      }
    return False;
  }



Logical fr_lle(char *arg1_base, charcount_t arg1_length,
               char *arg2_base, charcount_t arg2_length)
  {
    charcount_t min_length;
    charcount_t position;

    min_length = ((arg1_length <= arg2_length) ? arg1_length : arg2_length);
    for (position = 0; position < min_length; ++position)
      {
        if (arg1_base[position] > arg2_base[position])
            return False;
        if (arg1_base[position] < arg2_base[position])
            return True;
      }
    if (position < arg1_length)
      {
        while (position < arg1_length)
          {
            if (arg1_base[position] > ' ')
                return False;
            if (arg1_base[position] < ' ')
                return True;
            ++position;
          }
      }
    else if (position < arg2_length)
      {
        while (position < arg2_length)
          {
            if (' ' > arg2_base[position])
                return False;
            if (' ' < arg2_base[position])
                return True;
            ++position;
          }
      }
    return True;
  }


Logical fr_lle_cbr(arg1_base, arg1_length, arg2_base, arg2_length)
    char *arg1_base;
    charcount_t arg1_length;
    char *arg2_base;
    charcount_t arg2_length;
  {
    charcount_t min_length;
    charcount_t position;

    min_length = ((arg1_length <= arg2_length) ? arg1_length : arg2_length);
    for (position = 0; position < min_length; ++position)
      {
        if (arg1_base[position] > arg2_base[position])
            return False;
        if (arg1_base[position] < arg2_base[position])
            return True;
      }
    if (position < arg1_length)
      {
        while (position < arg1_length)
          {
            if (arg1_base[position] > ' ')
                return False;
            if (arg1_base[position] < ' ')
                return True;
            ++position;
          }
      }
    else if (position < arg2_length)
      {
        while (position < arg2_length)
          {
            if (' ' > arg2_base[position])
                return False;
            if (' ' < arg2_base[position])
                return True;
            ++position;
          }
      }
    return True;
  }



Logical fr_llt(char *arg1_base, charcount_t arg1_length,
               char *arg2_base, charcount_t arg2_length)
  {
    charcount_t min_length;
    charcount_t position;

    min_length = ((arg1_length <= arg2_length) ? arg1_length : arg2_length);
    for (position = 0; position < min_length; ++position)
      {
        if (arg1_base[position] > arg2_base[position])
            return False;
        if (arg1_base[position] < arg2_base[position])
            return True;
      }
    if (position < arg1_length)
      {
        while (position < arg1_length)
          {
            if (arg1_base[position] > ' ')
                return False;
            if (arg1_base[position] < ' ')
                return True;
            ++position;
          }
      }
    else if (position < arg2_length)
      {
        while (position < arg2_length)
          {
            if (' ' > arg2_base[position])
                return False;
            if (' ' < arg2_base[position])
                return True;
            ++position;
          }
      }
    return False;
  }


Logical fr_llt_cbr(arg1_base, arg1_length, arg2_base, arg2_length)
    char *arg1_base;
    charcount_t arg1_length;
    char *arg2_base;
    charcount_t arg2_length;
  {
    charcount_t min_length;
    charcount_t position;

    min_length = ((arg1_length <= arg2_length) ? arg1_length : arg2_length);
    for (position = 0; position < min_length; ++position)
      {
        if (arg1_base[position] > arg2_base[position])
            return False;
        if (arg1_base[position] < arg2_base[position])
            return True;
      }
    if (position < arg1_length)
      {
        while (position < arg1_length)
          {
            if (arg1_base[position] > ' ')
                return False;
            if (arg1_base[position] < ' ')
                return True;
            ++position;
          }
      }
    else if (position < arg2_length)
      {
        while (position < arg2_length)
          {
            if (' ' > arg2_base[position])
                return False;
            if (' ' < arg2_base[position])
                return True;
            ++position;
          }
      }
    return False;
  }



Integer fr_power_i_i(Integer arg1, Integer arg2)
  {
    Integer result;
    Integer factor;
    Integer exponent;

    if (arg1 == 0)
        return (Integer)0;
    if (arg2 < 0)
        return ((arg1 == 1) ? (Integer)1 : (Integer)0);
    result = 1;
    factor = arg1;
    exponent = arg2;
    while (1)
      {
        if (exponent % 2 == 1)
            result *= factor;
        exponent /= 2;
        if (exponent == 0)
            break;
        factor *= factor;
      }
    return result;
  }

Real fr_power_r_i(Real arg1, Integer arg2)
  {
    Real result;
    Real factor;
    Integer exponent;

    if (arg1 == 0.0)
        return (Real)0.0;
    result = 1;
    factor = arg1;
    exponent = arg2;
    if (exponent < 0)
      {
        exponent = -exponent;
        factor = 1/factor;
      }
    while (1)
      {
        if (exponent % 2 == 1)
            result *= factor;
        exponent /= 2;
        if (exponent == 0)
            break;
        factor *= factor;
      }
    return result;
  }

Double fr_power_d_i(Double arg1, Integer arg2)
  {
    Double result;
    Double factor;
    Integer exponent;

    if (arg1 == 0.0)
        return (Double)0.0;
    result = 1;
    factor = arg1;
    exponent = arg2;
    if (exponent < 0)
      {
        exponent = -exponent;
        factor = 1/factor;
      }
    while (1)
      {
        if (exponent % 2 == 1)
            result *= factor;
        exponent /= 2;
        if (exponent == 0)
            break;
        factor *= factor;
      }
    return result;
  }

Complex fr_power_cx_i(Complex arg1, Integer arg2)
  {
    Complex result;

    /* @@@ */
    return result;
  }


Real fr_power_i_r(Integer arg1, Real arg2)
  {
    Real result;

    /* @@@ */
    return result;
  }

Real fr_power_r_r(Real arg1, Real arg2)
  {
    return (Real)(pow((double)arg1, (double)arg2));
  }

Double fr_power_d_r(Double arg1, Real arg2)
  {
    return (Double)(pow((double)arg1, (double)arg2));
  }

Complex fr_power_cx_r(Complex arg1, Real arg2)
  {
    Complex result;

    /* @@@ */
    return result;
  }


Double fr_power_i_d(Integer arg1, Double arg2)
  {
    Double result;

    /* @@@ */
    return result;
  }

Double fr_power_r_d(Real arg1, Double arg2)
  {
    return (Double)(pow((double)arg1, (double)arg2));
  }

Double fr_power_d_d(Double arg1, Double arg2)
  {
    return (Double)(pow((double)arg1, (double)arg2));
  }


Complex fr_power_i_cx(Integer arg1, Complex arg2)
  {
    Complex result;

    /* @@@ */
    return result;
  }

Complex fr_power_r_cx(Real arg1, Complex arg2)
  {
    Complex result;

    /* @@@ */
    return result;
  }

Complex fr_power_cx_cx(Complex arg1, Complex arg2)
  {
    Complex result;

    /* @@@ */
    return result;
  }
