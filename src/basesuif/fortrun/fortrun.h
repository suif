/* file "fortrun.h" */


/*
       Copyright (c) 1996 Stanford University

       All rights reserved.

       This software is provided under the terms described in
       the "suif_copyright.h" include file.
*/

#include <suif_copyright.h>


#ifndef FORTRUN_H
#define FORTRUN_H


#ifndef Integer
#define Integer int
#endif

#ifndef Real
#define Real float
#endif

#ifndef Double
#define Double double
#endif

#ifndef Complex
typedef struct { Real _r, _i; } complex_struct;
#define Complex complex_struct
#endif

#ifndef Logical
#define Logical int
#define True 1
#define False 0
#endif

#ifndef charcount_t
#define charcount_t Integer
#endif


/*
 *  There are two functions provided for each Fortran intrinsic, a
 *  call-by-reference version and a call-by-value version.  The call
 *  by value version is preferable in most cases, both because it is
 *  usually more efficient to remove the level of indirection in the
 *  generated code and because it makes it easier for the compiler
 *  when it doesn't have to deal with that extra level of indirection.
 *  But the call-by-reference version is necessary because Fortran
 *  intrinsics may legally be passed as parameters to other Fortran
 *  functions, and the code that eventually calls the intrinsic does
 *  not in general know whether it is using an intrinsic or a
 *  user-defined function which depends on the call-by-reference
 *  calling convention.  The call-by-reference version of each
 *  function has the same name but with a ``_cbr'' appended to the end
 *  of the name.
 *
 *  Note that some of these functions can trivially be inlined away,
 *  but the call-by-ref version is still needed for procedure
 *  arguments, and the call-by-value version of each is provided for
 *  completeness.  A few of the conversin routines also cannot
 *  actually be used for actual parameters because there are no
 *  specific names for them, only generic names, but again the
 *  call-by-reference versions are provided for completeness.
 *
 *  The call-by-ref versions purposely use old-style C argument
 *  passing (i.e., no argument type information externally visible) so
 *  that they can be passed as parameters.
 *
 *  The ``fr_'' prefix is short for ``fortrun'', which is short for
 *  ``Fortran Runtime Library''.  It is used on all functions to help
 *  avoid potential namespace conflicts with other runtime libraries.
 *  The name of the function is the lower case version of the name of
 *  the intrinsic plus the ``fr_'' prefix.  If that does not generate
 *  a unique name, a further suffix is added to indicate the argument
 *  types: the suffix is the concatenation of the suffixes for each
 *  argument type, where the suffix for each type is as shown below:
 *
 *      _i  Integer
 *      _r  Real
 *      _d  Double
 *      _cx Complex
 *      _l  Logical
 *      _ch Character
 *
 *  Note also that it is illegal to pass minimum or maximum intrinsics
 *  as arguments because they take variable numbers of arguments.  So
 *  the compiler always knows when a minimum or maximum intrinsic is
 *  being used, so it can use calls to 2-argument versions of minimum
 *  and maximum functions.  So only these 2-argument versions are
 *  provided here.
 */

/*
 *        F77 INTRINSIC FUNCTIONS
 *        -----------------------
 *
 *  These are intrinsic functions required by ANSI X3.9-1978
 *  Fortran 77.
 */

/*
 *  INT
 *    INT
 *    INT
 *    IFIX
 *    IDINT
 *    INT
 */

Integer fr_int_i(Integer);
Integer fr_int_r(Real);
Integer fr_ifix(Real);
Integer fr_idint(Double);
Integer fr_int_cx(Complex);

Integer fr_int_i_cbr(/* Integer * */);
Integer fr_int_r_cbr(/* Real * */);
Integer fr_ifix_cbr(/* Real * */);
Integer fr_idint_cbr(/* Double * */);
Integer fr_int_cx_cbr(/* Complex * */);


/*
 *  REAL
 *    REAL
 *    FLOAT
 *    REAL
 *    SNGL
 *    REAL
 */

Real fr_real_i(Integer);
Real fr_float(Integer);
Real fr_real_r(Real);
Real fr_sngl(Double);
Real fr_real_cx(Complex);

Real fr_real_i_cbr(/* Integer * */);
Real fr_float_cbr(/* Integer * */);
Real fr_real_r_cbr(/* Real * */);
Real fr_sngl_cbr(/* Double * */);
Real fr_real_cx_cbr(/* Complex * */);


/*
 *  DBLE
 *    DBLE
 *    DBLE
 *    DBLE
 *    DBLE
 */

Double fr_dble_i(Integer);
Double fr_dble_r(Real);
Double fr_dble_d(Double);
Double fr_dble_cx(Complex);

Double fr_dble_i_cbr(/* Integer * */);
Double fr_dble_r_cbr(/* Real * */);
Double fr_dble_d_cbr(/* Double * */);
Double fr_dble_cx_cbr(/* Complex * */);


/*
 *  CMPLX
 *    CMPLX
 *    CMPLX
 *    CMPLX
 *    CMPLX
 *    CMPLX
 *    CMPLX
 *    CMPLX
 */

Complex fr_cmplx_i(Integer);
Complex fr_cmplx_i_i(Integer, Integer);
Complex fr_cmplx_r(Real);
Complex fr_cmplx_r_r(Real, Real);
Complex fr_cmplx_d(Double);
Complex fr_cmplx_d_d(Double, Double);
Complex fr_cmplx_cx(Complex);

Complex fr_cmplx_i_cbr(/* Integer * */);
Complex fr_cmplx_i_i_cbr(/* Integer *, Integer * */);
Complex fr_cmplx_r_cbr(/* Real * */);
Complex fr_cmplx_r_r_cbr(/* Real *, Real * */);
Complex fr_cmplx_d_cbr(/* Double * */);
Complex fr_cmplx_d_d_cbr(/* Double *, Double * */);
Complex fr_cmplx_cx_cbr(/* Complex * */);


/*
 *  ICHAR
 */

Integer fr_ichar(char *base, charcount_t length);

Integer fr_ichar_cbr(/* char *base, charcount_t length */);


/*
 *  CHAR
 */

void fr_char(char *result_base, charcount_t result_length, Integer);

void fr_char_cbr(/* char *result_base, charcount_t result_length,
                 Integer * */);


/*
 *  AINT
 *    AINT
 *    DINT
 */

Real fr_aint(Real);
Double fr_dint(Double);

Real fr_aint_cbr(/* Real * */);
Double fr_dint_cbr(/* Double * */);


/*
 *  ANINT
 *    ANINT
 *    DNINT
 */

Real fr_anint(Real);
Double fr_dnint(Double);

Real fr_anint_cbr(/* Real * */);
Double fr_dnint_cbr(/* Double * */);


/*
 *  NINT
 *    NINT
 *    IDNINT
 */

Integer fr_nint(Real);
Integer fr_idnint(Double);

Integer fr_nint_cbr(/* Real * */);
Integer fr_idnint_cbr(/* Double * */);


/*
 *  ABS
 *    IABS
 *    ABS
 *    DABS
 *    CABS
 */

Integer fr_iabs(Integer);
Real fr_abs(Real);
Double fr_dabs(Double);
Real fr_cabs(Complex);

Integer fr_iabs_cbr(/* Integer * */);
Real fr_abs_cbr(/* Real * */);
Double fr_dabs_cbr(/* Double * */);
Real fr_cabs_cbr(/* Complex * */);


/*
 *  MOD
 *    MOD
 *    AMOD
 *    DMOD
 */

Integer fr_mod(Integer, Integer);
Real fr_amod(Real, Real);
Double fr_dmod(Double, Double);

Integer fr_mod_cbr(/* Integer *, Integer * */);
Real fr_amod_cbr(/* Real *, Real * */);
Double fr_dmod_cbr(/* Double *, Double * */);


/*
 *  SIGN
 *    ISIGN
 *    SIGN
 *    DSIGN
 */

Integer fr_isign(Integer, Integer);
Real fr_sign(Real, Real);
Double fr_dsign(Double, Double);

Integer fr_isign_cbr(/* Integer *, Integer * */);
Real fr_sign_cbr(/* Real *, Real * */);
Double fr_dsign_cbr(/* Double *, Double * */);


/*
 *  DIM
 *    IDIM
 *    DIM
 *    DDIM
 */

Integer fr_idim(Integer, Integer);
Real fr_dim(Real, Real);
Double fr_ddim(Double, Double);

Integer fr_idim_cbr(/* Integer *, Integer * */);
Real fr_dim_cbr(/* Real *, Real * */);
Double fr_ddim_cbr(/* Double *, Double * */);


/*
 *  DPROD
 */

Double fr_dprod(Real, Real);

Double fr_dprod_cbr(/* Real *, Real * */);


/*
 *  MAX
 *    MAX0
 *    AMAX1
 *    DMAX1
 *    AMAX0
 *    MAX1
 */

Integer fr_max0(Integer, Integer);
Real fr_amax1(Real, Real);
Double fr_dmax1(Double, Double);
Real fr_amax0(Integer, Integer);
Integer fr_max1(Real, Real);

Integer fr_max0_cbr(/* Integer *, Integer * */);
Real fr_amax1_cbr(/* Real *, Real * */);
Double fr_dmax1_cbr(/* Double *, Double * */);
Real fr_amax0_cbr(/* Integer *, Integer * */);
Integer fr_max1_cbr(/* Real *, Real * */);


/*
 *  MIN
 *    MIN0
 *    AMIN1
 *    DMIN1
 *    AMIN0
 *    MIN1
 */

Integer fr_min0(Integer, Integer);
Real fr_amin1(Real, Real);
Double fr_dmin1(Double, Double);
Real fr_amin0(Integer, Integer);
Integer fr_min1(Real, Real);

Integer fr_min0_cbr(/* Integer *, Integer * */);
Real fr_amin1_cbr(/* Real *, Real * */);
Double fr_dmin1_cbr(/* Double *, Double * */);
Real fr_amin0_cbr(/* Integer *, Integer * */);
Integer fr_min1_cbr(/* Real *, Real * */);


/*
 *  LEN
 */

Integer fr_len(char *base, charcount_t length);

Integer fr_len_cbr(/* char *base, charcount_t length */);


/*
 *  INDEX
 */

Integer fr_index(char *arg1_base, charcount_t arg1_length,
                 char *arg2_base, charcount_t arg2_length);

Integer fr_index_cbr(/* char *arg1_base, charcount_t arg1_length,
                        char *arg2_base, charcount_t arg2_length */);


/*
 *  AIMAG
 */

Real fr_aimag(Complex);

Real fr_aimag_cbr(/* Complex * */);


/*
 *  CONJG
 */

Complex fr_conjg(Complex);

Complex fr_conjg_cbr(/* Complex * */);


/*
 *  SQRT
 *    SQRT
 *    DSQRT
 *    CSQRT
 */

Real fr_sqrt(Real);
Double fr_dsqrt(Double);
Complex fr_csqrt(Complex);

Real fr_sqrt_cbr(/* Real * */);
Double fr_dsqrt_cbr(/* Double * */);
Complex fr_csqrt_cbr(/* Complex * */);


/*
 *  EXP
 *    EXP
 *    DEXP
 *    CEXP
 */

Real fr_exp(Real);
Double fr_dexp(Double);
Complex fr_cexp(Complex);

Real fr_exp_cbr(/* Real * */);
Double fr_dexp_cbr(/* Double * */);
Complex fr_cexp_cbr(/* Complex * */);


/*
 *  LOG
 *    ALOG
 *    DLOG
 *    CLOG
 */

Real fr_alog(Real);
Double fr_dlog(Double);
Complex fr_clog(Complex);

Real fr_alog_cbr(/* Real * */);
Double fr_dlog_cbr(/* Double * */);
Complex fr_clog_cbr(/* Complex * */);


/*
 *  LOG10
 *    ALOG10
 *    DLOG10
 */

Real fr_alog10(Real);
Double fr_dlog10(Double);

Real fr_alog10_cbr(/* Real * */);
Double fr_dlog10_cbr(/* Double * */);


/*
 *  SIN
 *    SIN
 *    DSIN
 *    CSIN
 */

Real fr_sin(Real);
Double fr_dsin(Double);
Complex fr_csin(Complex);

Real fr_sin_cbr(/* Real * */);
Double fr_dsin_cbr(/* Double * */);
Complex fr_csin_cbr(/* Complex * */);


/*
 *  COS
 *    COS
 *    DCOS
 *    CCOS
 */

Real fr_cos(Real);
Double fr_dcos(Double);
Complex fr_ccos(Complex);

Real fr_cos_cbr(/* Real * */);
Double fr_dcos_cbr(/* Double * */);
Complex fr_ccos_cbr(/* Complex * */);


/*
 *  TAN
 *    TAN
 *    DTAN
 */

Real fr_tan(Real);
Double fr_dtan(Double);

Real fr_tan_cbr(/* Real * */);
Double fr_dtan_cbr(/* Double * */);


/*
 *  ASIN
 *    ASIN
 *    DASIN
 */

Real fr_asin(Real);
Double fr_dasin(Double);

Real fr_asin_cbr(/* Real * */);
Double fr_dasin_cbr(/* Double * */);


/*
 *  ACOS
 *    ACOS
 *    DACOS
 */

Real fr_acos(Real);
Double fr_dacos(Double);

Real fr_acos_cbr(/* Real * */);
Double fr_dacos_cbr(/* Double * */);


/*
 *  ATAN
 *    ATAN
 *    DATAN
 */

Real fr_atan(Real);
Double fr_datan(Double);

Real fr_atan_cbr(/* Real * */);
Double fr_datan_cbr(/* Double * */);


/*
 *  ATAN2
 *    ATAN2
 *    DATAN2
 */

Real fr_atan2(Real, Real);
Double fr_datan2(Double, Double);

Real fr_atan2_cbr(/* Real *, Real * */);
Double fr_datan2_cbr(/* Double *, Double * */);


/*
 *  SINH
 *    SINH
 *    DSINH
 */

Real fr_sinh(Real);
Double fr_dsinh(Double);

Real fr_sinh_cbr(/* Real * */);
Double fr_dsinh_cbr(/* Double * */);


/*
 *  COSH
 *    COSH
 *    DCOSH
 */

Real fr_cosh(Real);
Double fr_dcosh(Double);

Real fr_cosh_cbr(/* Real * */);
Double fr_dcosh_cbr(/* Double * */);


/*
 *  TANH
 *    TANH
 *    DTANH
 */

Real fr_tanh(Real);
Double fr_dtanh(Double);

Real fr_tanh_cbr(/* Real * */);
Double fr_dtanh_cbr(/* Double * */);


/*
 *    LGE
 */

Logical fr_lge(char *arg1_base, charcount_t arg1_length,
               char *arg2_base, charcount_t arg2_length);

Logical fr_lge_cbr(/* char *arg1_base, charcount_t arg1_length,
                      char *arg2_base, charcount_t arg2_length */);


/*
 *    LGT
 */

Logical fr_lgt(char *arg1_base, charcount_t arg1_length,
               char *arg2_base, charcount_t arg2_length);

Logical fr_lgt_cbr(/* char *arg1_base, charcount_t arg1_length,
                      char *arg2_base, charcount_t arg2_length */);


/*
 *    LLE
 */

Logical fr_lle(char *arg1_base, charcount_t arg1_length,
               char *arg2_base, charcount_t arg2_length);

Logical fr_lle_cbr(/* char *arg1_base, charcount_t arg1_length,
                      char *arg2_base, charcount_t arg2_length */);


/*
 *    LLT
 */

Logical fr_llt(char *arg1_base, charcount_t arg1_length,
               char *arg2_base, charcount_t arg2_length);

Logical fr_llt_cbr(/* char *arg1_base, charcount_t arg1_length,
                      char *arg2_base, charcount_t arg2_length */);


/*
 *        F77 POWER FUNCTIONS
 *        -------------------
 *
 *  These are functions that implement the versions of the power
 *  operator (``**'') required by ANSI X3.9-1978 Fortran 77.
 */

Integer fr_power_i_i(Integer, Integer);
Real fr_power_r_i(Real, Integer);
Double fr_power_d_i(Double, Integer);
Complex fr_power_cx_i(Complex, Integer);

Real fr_power_i_r(Integer, Real);
Real fr_power_r_r(Real, Real);
Double fr_power_d_r(Double, Real);
Complex fr_power_cx_r(Complex, Real);

Double fr_power_i_d(Integer, Double);
Double fr_power_r_d(Real, Double);
Double fr_power_d_d(Double, Double);

Complex fr_power_i_cx(Integer, Complex);
Complex fr_power_r_cx(Real, Complex);
Complex fr_power_cx_cx(Complex, Complex);


#endif /* FORTRUN_H */
