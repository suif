/* file "main.cc" of the entry_types program for SUIF */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for entry_types.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:20 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        The entry_types program propagates the original parameter
        array type information to the dummy procedures produced by
        sf2c for entry points into subroutines.  It must be run right
        after fixfortran if it will be necesary to have the correct
        type information in these dummy procedures.  For
        intraprocedural analysis, it ususally does not matter because
        the dummy procedures simply call the procedures containing the
        body and pass parameters directly through, so there is no
        place within the dummy procedure where the type information
        will matter.  For interprocedural analysis, however, this
        information does matter, so if any interprocedural passes are
        to be run, the entry_types pass should be run first.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

DECLARE_LIST_CLASS(proc_sym_list, proc_sym *);

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

proc_sym_list *entry_bodies;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void record_bodies_with_entries(tree_proc *the_proc);
static void fix_possible_entry_proc(tree_proc *the_proc, boolean *write);
static in_cal *node_in_entry(tree_node *the_node);
static in_cal *list_in_entry(tree_node_list *the_list);
static in_cal *expr_in_entry(instruction *the_instr);
static in_cal *op_in_entry(operand the_op);
static type_node *type_outside_call(type_node *the_type, in_cal *the_call);
static array_bound bound_outside_call(array_bound old_bound, in_cal *the_call);
static var_sym *aux_var_outside_call(var_sym *old_var, in_cal *the_call);
static instruction *aux_expr_outside_call(instruction *old_expr,
                                          in_cal *the_call);
static var_sym *var_for_arg_num(in_cal *the_call, unsigned arg_num);
static instruction *auxiliary_assignment(tree_node_list *the_list,
                                         var_sym *the_aux);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    entry_bodies = new proc_sym_list;

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            record_bodies_with_entries(this_proc_sym->block());
            this_proc_sym->flush_proc();
          }
      }

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            boolean write = FALSE;
            fix_possible_entry_proc(this_proc_sym->block(), &write);
            if (write)
                this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
      }

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            if (this_proc_sym->is_readable())
              {
                this_proc_sym->read_proc(TRUE, FALSE);
                this_proc_sym->write_proc(fse);
                this_proc_sym->flush_proc();
              }
          }
      }

    delete entry_bodies;

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr, "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

/*
 *  If a subroutine has multiple entry points, sf2c builds a procedure
 *  for its body that has a new first argument named ``n__'', so we
 *  recognize such situations by looking for a first parameter of that
 *  name (this is the only situation under which the name ``n__'' can
 *  be used as a parameter by sf2c).
 */
static void record_bodies_with_entries(tree_proc *the_proc)
  {
    sym_node_list_iter param_iter(the_proc->proc_syms()->params());
    if (param_iter.is_empty())
        return;

    sym_node *first_param = param_iter.step();
    if (strcmp(first_param->name(), "n__") == 0)
        entry_bodies->append(the_proc->proc());
  }

static void fix_possible_entry_proc(tree_proc *the_proc, boolean *write)
  {
    in_cal *body_call = node_in_entry(the_proc);
    if (body_call == NULL)
        return;

    *write = TRUE;

    unsigned num_args = body_call->num_args();
    if (num_args < ((unsigned)(the_proc->proc_syms()->params()->count())) + 1)
        error_line(1, body_call, "badly formed call from entry point to body");

    if (num_args == 1)
        return;

    proc_sym *body_sym = proc_for_call(body_call);
    assert(body_sym != NULL);
    if (body_sym->is_in_memory() || !body_sym->is_readable())
        error_line(1, body_call, "bad call graph for entry points and bodies");
    body_sym->read_proc(TRUE, FALSE);

    proc_symtab *body_proc_syms = body_sym->block()->proc_syms();
    sym_node_list_iter body_param_iter(body_proc_syms->params());

    /* Argument zero will be an integer specifying which entry this
     * is, so skip it. */
    (void)(body_param_iter.step());
    for (unsigned arg_num = 1; arg_num < num_args; ++arg_num)
      {
        sym_node *this_sym_param = body_param_iter.step();
        assert(this_sym_param->is_var());
        var_sym *this_body_param = (var_sym *)this_sym_param;

        var_sym *this_var = var_for_arg_num(body_call, arg_num);
        if (this_var == NULL)
            continue;

        if (!this_var->is_param())
          {
            error_line(1, body_call,
                       "unexpected non-parameter variable in call from "
                       "entry point to body");
          }

        this_var->set_type(type_outside_call(this_body_param->type(),
                                             body_call));
      }

    body_sym->flush_proc();
  }

/*
 *  If the node contains a call to a body with entries, indicating it
 *  is in a dummy procedure for an entry for that procedure, return a
 *  pointer to the call to the body.  Otherwise, return NULL.
 */
static in_cal *node_in_entry(tree_node *the_node)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            return expr_in_entry(the_tree_instr->instr());
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;

            in_cal *body_call = list_in_entry(the_loop->body());
            if (body_call != NULL)
                return body_call;

            return list_in_entry(the_loop->test());
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;

            in_cal *body_call = op_in_entry(the_for->lb_op());
            if (body_call != NULL)
                return body_call;

            body_call = op_in_entry(the_for->ub_op());
            if (body_call != NULL)
                return body_call;

            body_call = op_in_entry(the_for->step_op());
            if (body_call != NULL)
                return body_call;

            body_call = list_in_entry(the_for->body());
            if (body_call != NULL)
                return body_call;

            return list_in_entry(the_for->landing_pad());
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;

            in_cal *body_call = list_in_entry(the_if->header());
            if (body_call != NULL)
                return body_call;

            body_call = list_in_entry(the_if->then_part());
            if (body_call != NULL)
                return body_call;

            return list_in_entry(the_if->else_part());
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            return list_in_entry(the_block->body());
          }
        default:
            assert(FALSE);
            return NULL;
      }
  }

/*
 *  If the list contains a call to a body with entries, indicating it
 *  is in a dummy procedure for an entry for that procedure, return a
 *  pointer to the call to the body.  Otherwise, return NULL.
 */
static in_cal *list_in_entry(tree_node_list *the_list)
  {
    tree_node_list_iter node_iter(the_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        in_cal *body_call = node_in_entry(this_node);
        if (body_call != NULL)
            return body_call;
      }

    return NULL;
  }

static in_cal *expr_in_entry(instruction *the_instr)
  {
    if (the_instr->opcode() == io_cal)
      {
        in_cal *this_call = (in_cal *)the_instr;
        proc_sym *this_proc = proc_for_call(this_call);
        if ((this_proc != NULL) && (entry_bodies->lookup(this_proc) != NULL))
            return this_call;
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        in_cal *body_call = op_in_entry(this_op);
        if (body_call != NULL)
            return body_call;
      }

    return NULL;
  }

static in_cal *op_in_entry(operand the_op)
  {
    if (the_op.is_expr())
        return expr_in_entry(the_op.instr());
    else
        return NULL;
  }

static type_node *type_outside_call(type_node *the_type, in_cal *the_call)
  {
    switch (the_type->op())
      {
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
          {
            base_type *old_base = (base_type *)the_type;
            base_type *new_base =
                    new base_type(old_base->op(), old_base->size(),
                                  old_base->is_signed());
            old_base->copy_annotes(new_base);
            return fileset->globals()->install_type(new_base);
          }
        case TYPE_PTR:
          {
            ptr_type *old_ptr = (ptr_type *)the_type;
            type_node *new_ref =
                    type_outside_call(old_ptr->ref_type(), the_call);
            ptr_type *new_ptr = new ptr_type(new_ref);
            old_ptr->copy_annotes(new_ptr);
            return new_ref->parent()->install_type(new_ptr);
          }
        case TYPE_ARRAY:
          {
            array_type *old_array = (array_type *)the_type;
            type_node *new_elem =
                    type_outside_call(old_array->elem_type(), the_call);
            array_bound new_lower =
                    bound_outside_call(old_array->lower_bound(), the_call);
            array_bound new_upper =
                    bound_outside_call(old_array->upper_bound(), the_call);
            array_type *new_array =
                    new array_type(new_elem, new_lower, new_upper);
            base_symtab *new_symtab = new_elem->parent();
            if (new_lower.is_variable())
              {
                new_symtab =
                        joint_symtab(new_lower.variable()->parent(),
                                     new_symtab);
              }
            if (new_upper.is_variable())
              {
                new_symtab =
                        joint_symtab(new_upper.variable()->parent(),
                                     new_symtab);
              }
            old_array->copy_annotes(new_array);
            return new_symtab->install_type(new_array);
          }
        case TYPE_FUNC:
          {
            func_type *old_func = (func_type *)the_type;
            type_node *new_return_type =
                    type_outside_call(old_func->return_type(), the_call);
            unsigned num_args = old_func->num_args();
            func_type *new_func =
                    new func_type(new_return_type, num_args,
                                  old_func->has_varargs(),
                                  old_func->args_known());
            base_symtab *new_symtab = new_return_type->parent();
            for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
              {
                type_node *new_arg_type =
                        type_outside_call(old_func->arg_type(arg_num),
                                          the_call);
                new_func->set_arg_type(arg_num, new_arg_type);
                new_symtab =
                        joint_symtab(new_arg_type->parent(), new_symtab);
                assert(new_symtab != NULL);
              }
            old_func->copy_annotes(new_func);
            return new_symtab->install_type(new_func);
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *old_struct = (struct_type *)the_type;
            unsigned num_fields = old_struct->num_fields();
            struct_type *new_struct =
                    new struct_type(old_struct->op(), old_struct->size(),
                                    old_struct->name(), num_fields);
            base_symtab *new_symtab = fileset->globals();
            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                type_node *new_field_type =
                        type_outside_call(old_struct->field_type(field_num),
                                          the_call);
                new_struct->set_field_name(field_num,
                                           old_struct->field_name(field_num));
                new_struct->set_field_type(field_num, new_field_type);
                new_struct->set_offset(field_num,
                                       old_struct->offset(field_num));
                new_symtab =
                        joint_symtab(new_field_type->parent(), new_symtab);
                assert(new_symtab != NULL);
              }
            old_struct->copy_annotes(new_struct);
            return new_symtab->install_type(new_struct);
          }
        case TYPE_ENUM:
          {
            enum_type *old_enum = (enum_type *)the_type;
            enum_type *new_enum = old_enum->clone();
            return fileset->globals()->install_type(new_enum);
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *old_modifier = (modifier_type *)the_type;
            type_node *new_base =
                    type_outside_call(old_modifier->base(), the_call);
            modifier_type *new_modifier =
                    new modifier_type(old_modifier->op(), new_base);
            old_modifier->copy_annotes(new_modifier);
            return new_base->parent()->install_type(new_modifier);
          }
        default:
            assert(FALSE);
            return NULL;
      }
  }

static array_bound bound_outside_call(array_bound old_bound, in_cal *the_call)
  {
    if (old_bound.is_variable())
      {
        return array_bound(aux_var_outside_call(old_bound.variable(),
                                                the_call));
      }
    else
      {
        return old_bound;
      }
  }

static var_sym *aux_var_outside_call(var_sym *old_var, in_cal *the_call)
  {
    if (old_var->is_param())
      {
        base_symtab *old_symtab = old_var->parent();
        assert(old_symtab->is_proc());
        proc_symtab *old_proc_symtab = (proc_symtab *)old_symtab;

        sym_node_list_iter param_iter(old_proc_symtab->params());
        unsigned param_num = 0;
        while (TRUE)
          {
            assert(!param_iter.is_empty());
            sym_node *this_param = param_iter.step();
            if (this_param == old_var)
                break;
            ++param_num;
          }

        if (param_num >= the_call->num_args())
          {
            error_line(1, the_call,
                       "not enough arguments for function called");
          }
        var_sym *outer_var = var_for_arg_num(the_call, param_num);

        if (outer_var == NULL)
          {
            error_line(1, the_call,
                       "argument %u: variable expected in call from entry "
                       "point to body", param_num);
          }

        if (!outer_var->type()->is_same(old_var->type()))
          {
            error_line(1, the_call,
                       "type mismatch between caller `%s' and callee `%s'",
                       outer_var->name(), old_var->name());
          }

        return outer_var;
      }
    else
      {
        base_symtab *old_symtab = old_var->parent();
        if (!old_symtab->is_block())
          {
            assert(the_call->owner()->scope()->is_ancestor(old_symtab));
            return old_var;
          }
        block_symtab *old_block_symtab = (block_symtab *)old_symtab;
        instruction *old_assignment =
                auxiliary_assignment(old_block_symtab->block()->body(),
                                     old_var);
        if (old_assignment == NULL)
          {
            error_line(1, the_call,
                       "cannot find assignment for auxiliary variable `%s'",
                       old_var->name());
          }

        in_rrr *placeholder = new in_rrr(io_mrk);
        tree_instr *assignment_node = new tree_instr(placeholder);
        tree_proc *callee_proc = the_call->owner()->proc()->block();
        callee_proc->body()->push(assignment_node);

        if (!the_call->owner()->scope()->is_ancestor(
                    old_var->type()->parent()))
          {
            error_line(1, the_call, "type of auxiliary var is local");
          }

        var_sym *new_var =
                callee_proc->proc_syms()->new_var(old_var->type(),
                                                  old_var->name());

        instruction *new_expr =
                aux_expr_outside_call(old_assignment, the_call);
        new_expr->set_dst(operand(new_var));
        assignment_node->remove_instr(placeholder);
        assignment_node->set_instr(new_expr);
        delete placeholder;

        return new_var;
      }
  }

static instruction *aux_expr_outside_call(instruction *old_expr,
                                          in_cal *the_call)
  {
    if (!the_call->owner()->scope()->is_ancestor(
                old_expr->result_type()->parent()))
      {
        error_line(1, the_call, "type in auxiliary var expression is local");
      }

    instruction *new_expr;
    switch (old_expr->format())
      {
        case inf_rrr:
          {
            in_rrr *old_rrr = (in_rrr *)old_expr;
            new_expr = new in_rrr(old_rrr->opcode(), old_rrr->result_type());
            break;
          }
        case inf_ldc:
          {
            in_ldc *old_ldc = (in_ldc *)old_expr;
            new_expr =
                    new in_ldc(old_ldc->result_type(), operand(),
                               old_ldc->value());
            break;
          }
        case inf_cal:
          {
            in_cal *old_call = (in_cal *)old_expr;
            new_expr =
                    new in_cal(old_call->result_type(), operand(), operand(),
                               old_call->num_args());
            break;
          }
        case inf_array:
          {
            in_array *old_array = (in_array *)old_expr;
            new_expr =
                    new in_array(old_array->result_type(), operand(),
                                 operand(), old_array->elem_size(),
                                 old_array->dims(), old_array->offset());
            break;
          }
        case inf_gen:
          {
            in_gen *old_gen = (in_gen *)old_expr;
            new_expr =
                    new in_gen(old_gen->name(), old_gen->result_type(),
                               operand(), old_gen->num_srcs());
            break;
          }
        default:
            assert(FALSE);
      }

    unsigned num_srcs = old_expr->num_srcs();
    assert(num_srcs == new_expr->num_srcs());
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_src = old_expr->src_op(src_num);
        if (this_src.is_symbol())
          {
            this_src =
                    operand(aux_var_outside_call(this_src.symbol(), the_call));
          }
        else if (this_src.is_expr())
          {
            this_src =
                    operand(aux_expr_outside_call(this_src.instr(), the_call));
          }
        else
          {
            assert(this_src.is_null());
          }

        new_expr->set_src_op(src_num, this_src);
      }

    return new_expr;
  }

static var_sym *var_for_arg_num(in_cal *the_call, unsigned arg_num)
  {
    assert(the_call->num_args() > arg_num);
    operand this_arg = the_call->argument(arg_num);

    while (this_arg.is_expr() && this_arg.type()->is_ptr() &&
           ((this_arg.instr()->opcode() == io_cvt) ||
            (this_arg.instr()->opcode() == io_cpy)))
      {
        in_rrr *the_cvt = (in_rrr *)(this_arg.instr());
        operand new_arg = the_cvt->src_op();
        if (!new_arg.type()->is_ptr())
            break;
        this_arg = new_arg;
      }

    if (!this_arg.is_symbol())
        return NULL;
    return this_arg.symbol();
  }

static instruction *auxiliary_assignment(tree_node_list *the_list,
                                         var_sym *the_aux)
  {
    tree_node_list_iter list_iter(the_list);
    while (!list_iter.is_empty())
      {
        tree_node *this_node = list_iter.step();
        switch (this_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)this_node;
                if (the_tree_instr->instr()->dst_op() == operand(the_aux))
                    return the_tree_instr->instr();
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)this_node;
                instruction *this_assignment =
                        auxiliary_assignment(the_block->body(), the_aux);
                if (this_assignment != NULL)
                    return this_assignment;
                break;
              }
            default:
                return NULL;
          }
      }

    return NULL;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
