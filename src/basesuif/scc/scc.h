/* file "scc.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  SCC Definitions */

#define STRINGIFY(S) #S

#define TMPDIR "/tmp"
#define MVCMD "/bin/mv"

#if defined(__sgi) || defined(__linux)
#define ASLD_DIR "/usr/bin"
#else
#define ASLD_DIR "/bin"
#endif

#if defined(__linux)
#define TIMECMD "/usr/bin/time"
#else
#define TIMECMD "/bin/time"
#endif

#define MAX_FLAGS 4


enum Suffix {
    s_F,
    s_f,
    s_c,
    s_i,
    s_snt,
    s_spd,
    s_sln,
    s_spf,
    s_sfm,
    s_sf1,
    s_sce,
    s_spr,
    s_skw,
    s_sfp,
    s_sf2,
    s_out_c,
    s_spx,
    s_sfl,
    s_sfe,
    s_sr,
    s_sfo,
    s_sf,
    s_s,
    s_o,
    s_out,
    s_a,
    s_tmp
};

const char *suffixes[] = {
    "F",
    "f",
    "c",
    "i",
    "snt",
    "spd",
    "sln",
    "spf",
    "sf-",
    "sf1",
    "sce",
    "spr",
    "skw",
    "sfp",
    "sf2",
    "out.c",
    "spx",
    "sfl",
    "sfe",
    "sr",
    "sfo",
    "sf",
    "s",
    "o",
    "out",
    "a",
    0
};



enum pass_file_counts
{
    ONE_IN_ONE_OUT,
    MANY_IN_ONE_OUT,
    MANY_IN_MANY_OUT
};


enum output_file_format
{
    OUTPUT_SUIF,
    OUTPUT_NOT_SUIF
};


/* the following are linked in automatically by the standard Makefiles */
extern char *prog_ver_string;
extern char *prog_who_string;

const char *scc_machine = SCC_SYSTEM_NAME;


const char *suif_top;			/* top-level SUIF directory */
const char *suif_path;			/* SUIFPATH environment variable */
const char *suif_bin = NULL;		/* binary directory */
const char *suif_include;		/* include directory for scc */
const char *suif_lib;			/* lib directory for f2c */
String **default_path;			/* array of directory names */

const char *cmdnam;			/* name of this program (scc) */
char *tmpdir;				/* directory for temporary files */
char *tmpbase;				/* base name for temporary files */
const char *target_machine;		/* target machine */
long Gnum = -1;
int s_target;				/* suffix of the target files */
String *outfilename = NULL;		/* final output file name */


/* flags */
boolean automatic_flag = TRUE;
int keepflag = 0;			/* 1 to keep .sf* files;
					   2 to keep all files */
boolean parallel_passes = FALSE;        /* parallel passes executing now? */
boolean nullflag = FALSE;		/* don't actually execute */
boolean opt_level = 0;
boolean option_linksuif = FALSE;
boolean reassociate_arrays = UNKNOWN;
boolean sf2c_flag = FALSE;		/* fortran file, link in libs, etc */
boolean g_flag = FALSE;			/* -g specified, debugging info
                                           (this should be passed to s2c, mgen,
                                            and any other back end) */
boolean no_warn_flag = FALSE;		/* -w specified, surpress warnings
                                           (this should be passed to all
                                            passes to surpress warnings) */
boolean timing = FALSE;			/* time each pass */
boolean verbose = FALSE;		/* echo commands */
boolean version_only = FALSE;		/* print version and exit */
boolean checksuif = FALSE;		/* run checksuif on every SUIF1 file
                                         */
boolean checkfail = FALSE;		/* in checksuif, fail, instead of */
					/* just warning, on a problem */
boolean out_to_stdout = FALSE;		/* output to stdout, not file */
boolean pp_deps = FALSE;		/* pre-processor output is dependences
                                           for a makefile */
boolean use_c_backend = FALSE;		/* use s2c followed by the native C
                                           compiler as a back-end */
boolean alternate_cc = FALSE;		/* the C compiler used for a C backend
                                           is not the native one, so look in
                                           SUIFPATH, not the hardcoded
                                           directory for it */

/* This routine is for use in "commands.def", to allow parts of the
 * configuration to be read in at run time.  If the file exists, its
 * contents are appended to the_string with newlines replaced by
 * spaces.  If the file doesn't exist or can't be read, the_string
 * remains unchanged. */
extern void string_from_file(const char *file_name, String *the_string);



/*  The information for the individual passes of the compiler is maintained
    in the following table structure.  The table is initialized with the data
    in the commands.def file.  Since some of the fields in the table are
    dependent on run-time options, these fields are specified by code
    for functions (or expessions) that are not evaluated until after the
    command-line arguments have been parsed.  The rest of the fields are
    either filled directly from commands.def or initialized at run-time.  */

/*  functions to set up the pass table */
class pass;
typedef const char * (*dir_handler)();
typedef boolean (*exec_handler)();
typedef void (*flag_handler)(pass *p);

class pass {
public:
    /* these first fields are initialized with info from commands.def */
    const char *passname;			/* name of the pass */
    pass_file_counts file_counts;       /* one file in, one out, or
                                           many in, one out, or
                                           many in, many out */
    Suffix suffix;			/* suffix of output files */
    const char *cmdname;		/* name of the executable file */
    const char *cmdfmt;			/* format of the command line */
    dir_handler set_dir;		/* function to set command directory */
    exec_handler set_exec;		/* function to determine if this runs*/
    flag_handler set_flags;		/* function to set command line flags*/
    output_file_format out_file_format;
    /* end of fields that are initialized with info from commands.def */

    const char *dir;			/* directory containing the pass */
    boolean exec;			/* does this pass run? */
    String **flags;			/* array of flags */
    String *pass_options;		/* extra flags from command-line */
};


/*  Now we define the PASS macro in various ways to access the information
    in commands.def.  */

extern pass passtbl[];

/*  create an enumeration for all of the pass names */
#define PASS(TAG, COUNTS, SFX, OUTFORM, DIR, NAME, FMT, EXEC, FLAGS) TAG ,

enum passes {
#include "commands.def"
    last_pass
};


/*  define the functions to set the directory for each pass */
#undef PASS
#define PASS(TAG, COUNTS, SFX, OUTFORM, DIR, NAME, FMT, EXEC, FLAGS) \
    const char * set_dir_ ## TAG () { return (DIR); }

#include "commands.def"


/*  define the functions to set the exec flag for each pass */
#undef PASS
#define PASS(TAG, COUNTS, SFX, OUTFORM, DIR, NAME, FMT, EXEC, FLAGS) \
    boolean set_exec_ ## TAG () { return (EXEC); }

#include "commands.def"


/*  define the functions to set the flags for each pass */
#undef PASS
#define PASS(TAG, COUNTS, SFX, OUTFORM, DIR, NAME, FMT, EXEC, FLAGS) \
    void set_flags_ ## TAG (pass *p) { FLAGS }

#include "commands.def"


/*  now initialize the pass table itself */
#undef PASS
#define PASS(TAG, COUNTS, SFX, OUTFORM, DIR, NAME, FMT, EXEC, FLAGS) \
    { #TAG, COUNTS, SFX, NAME, FMT, set_dir_ ## TAG, set_exec_ ## TAG, \
	set_flags_ ## TAG, OUTFORM },

pass passtbl[] = {
#include "commands.def"
};



/*  The input and output filenames for the various passes are stored in the
    following structure.  Since these names are usually contained in lists,
    the class inherits the basic list element structure from glist_e.  Besides,
    the name, the structure also records the starting pass for the file (based
    on the filename extension) and whether the file should be removed after
    it has been processed.  It is helpful to keep this extra information with
    the filenames, because the filenames in a list may come from several
    different sources (command line argument, output of previous pass, etc.) */

class filename : public glist_e {
public:
    String name;			/* name of the file */
    String basename;                    /* base name of the last non-temporary
                                           precursor of this file */
    enum passes start_pass;		/* first pass to run on this file */
    boolean erase;			/* delete file after processing it? */

    filename(String the_name, String the_basename, enum passes the_start_pass,
             boolean will_erase);

    filename(String n)			{ name = n; }
    filename(const char *n)		{ name = String(n); }
    filename(filename *f) : name(f->name) { start_pass = f->start_pass;
					  erase = f->erase; }
};

typedef glist filelist;
typedef glist_iter filelistiter;

filelist *infiles;
filelist *outfiles;
filelist *tmpfiles;
const char *moving_file = NULL;


/* function prototypes */
char ** read_flag(char **argv);
enum passes find_pass(const char *name);
enum passes find_start_pass(const char *infile);
void process_files();
boolean contains_multiple_files(filelist *the_list);
void run_passes(filename *orig_infile, enum passes stop_pass);
String list_names(filelist *files);
filename *choose_output_file(enum passes the_pass, String base,
                             boolean has_base);
String base_from_name(String the_name);
String suffix_to_string(Suffix the_suffix);
enum passes next_pass(enum passes the_pass);
Suffix choose_suffix(enum passes the_pass);
boolean is_last_pass(enum passes the_pass);
void run_many_in_pass(enum passes this_pass);
boolean uses_specified_output(enum passes p);
void run_pass_one_in_one_out(enum passes p, const char *infiles, 
			     const char *outfile);
void run_pass_basic(enum passes p, const char *infile_names, 
		    const char *outfiles, const char *in_out_files);
const char * select_path(const char *cmd, const char *dir);
int execute_cmd(const char *cmd);
String new_temp_base();
Suffix getsfx(const char *s);
void move_file(const char *f1, const char *f2);
void interrupt(int);
void bad_option(const char *s);
void usage();
void error(int rc, const char *msg, ...);
void cleanup();
void erase_files(filelist *flist);
void syscallerr(const char *s);
void transfer_filename(filename *to_transfer, filelist *from, filelist *to);
