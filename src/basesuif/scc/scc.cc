/* file "scc.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  SCC Compiler Driver */

#include <suif1.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#include <sys/wait.h>
#else
#include <process.h>
#include <io.h>
#define pid_t int
// for access() - since Win32 does not have UNIX-style execute permissions
// just check for the existence of the executable
#define X_OK 0
#endif

#include "String.h"

#ifndef NULL
#define NULL 0
#endif

#include "scc.h"

#ifdef RLIMIT_STACK
#ifndef WIN32
#define SUIF_NEED_RLIMIT
#endif

#include <machine_dependent.h>
#undef SUIF_NEED_RLIMIT
#endif /* RLIMIT_STACK */

#ifdef __MINGW32__
#define PATHSEP			';'
#define SLASH			"\\"
#else
#define PATHSEP			':'
#define SLASH			"/"
#endif

int
main(int argc, char **argv)
{
    start_suif(argc, argv);

    filename *infile;
    int interesting_files = 0;
    int p;
    pass *pp;

    /* Try to get environment variables from the environment.  If they
       don't exist, just use the defaults.  */
    if ((suif_top = getenv("SUIFHOME")) == 0)
	suif_top = STRINGIFY(SUIF_TOP);
    if ((suif_path = getenv("SUIFPATH")) == 0)
	suif_path = STRINGIFY(SUIFPATH);
    if ((tmpdir = getenv("TMPDIR")) == 0)
	tmpdir = TMPDIR;

#ifdef RLIMIT_STACK
    /* unlimit the stack */
    rlimit rl;
    assert(getrlimit(RLIMIT_STACK, &rl) == 0);
    rl.rlim_cur = rl.rlim_max;
    setrlimit(RLIMIT_STACK, &rl);
#endif

    /* initialize the command table */
    for (p = 0; p < last_pass; p++) {
	pp = &passtbl[p];
	pp->flags = new String*[MAX_FLAGS];
	for (int flag_index = 0; flag_index < MAX_FLAGS; ++flag_index)
	    pp->flags[flag_index] = NULL;
	pp->pass_options = new String;
	pp->exec = UNKNOWN;
    }
    
    /* create the lists of input and output filenames */
    infiles = new filelist;
    outfiles = new filelist;
    tmpfiles = new filelist;

    /* default: compile to an executable (a.out) file */
    s_target = s_out;

    /* default target machine: unknown */
    target_machine = NULL;

    /* save the name of this program (usually "scc") */
    cmdnam = argv[0];

    /* generate a base for temporary file names */
    tmpbase = new char[10];
    sprintf(tmpbase, "scc%05lu", (unsigned long)getpid());
    
    /* trap signals and cleanup when we get them */
    signal(SIGINT, interrupt);
    signal(SIGTERM, interrupt);
#ifndef WIN32
    /* catch SIGHUP if it was not previously ignored */
    if ((void *)signal(SIGHUP, (void (*)(int))SIG_IGN) != (void *)SIG_IGN) {
	signal(SIGHUP, interrupt);
    }
#endif

    /* read command line options */
    argv++;
    while (*argv) {

	if (argv[0][0] == '-') {

	    argv = read_flag(argv);

	} else {

	    /* if not a flag, this must be an input file */
	    interesting_files++;
	    Suffix s = getsfx(argv[0]);
	    if (s == s_a) {
		/* special case: pass .a files to the linker */
		*passtbl[LD].pass_options += argv[0];
		*passtbl[LD].pass_options += " ";
	    } else {
		if (s == s_f || s == s_F) sf2c_flag = TRUE;
                String the_name(argv[0]);
                infile = new filename(the_name, base_from_name(the_name),
                                      find_start_pass(argv[0]), FALSE);
		outfiles->append(infile);
	    }
	}
	argv++;
    }

    if (version_only)
      {
        const char *base_cmd_name;
        if (cmdnam == NULL)
          {
            base_cmd_name = "suif";
          }
        else
          {
            base_cmd_name = strrchr(cmdnam, '/');
            if (base_cmd_name == NULL)
                base_cmd_name = cmdnam;
            else
                ++base_cmd_name;
          }
        fprintf(stderr, "%s %s%s\n", base_cmd_name, prog_ver_string,
                prog_who_string);
        exit(0);
      }

    if (interesting_files == 0) usage();

    if (reassociate_arrays == UNKNOWN) reassociate_arrays = (opt_level > 0);
    if (target_machine == NULL) target_machine = scc_machine;

    if (s_target == s_s)
	*passtbl[BACKEND_CC].pass_options += " -S";

    if (!alternate_cc)
	passtbl[LD].cmdname = "cc";

    if (suif_bin) {
	default_path = new String*[2];
	default_path[0] = new String(suif_bin);
	default_path[1] = NULL;

    } else if (suif_path) {

	/* count the colons in the path */
	int num_paths = 2;
	const char *c = suif_path;
	while (*c != '\0') {
	    if (*c++ == PATHSEP) num_paths++;
	}
	default_path = new String*[num_paths];

	/* copy the paths into the default path array */
	c = suif_path;
	int path_num = 0;
	do {

	    /* copy the string */
	    default_path[path_num] = new String;
	    while ((*c != '\0') && (*c != PATHSEP)) {
		*default_path[path_num] += *c++;
	    }
	    *default_path[path_num] += '\0';

	    /* go to the next element of the path */
	    path_num++;
	} while (*c++ != '\0');

	default_path[path_num] = NULL;
	
    } else {
	default_path = new String*[2];
	default_path[0] = new String(suif_top);
	*default_path[0] += SLASH;
	*default_path[0] += scc_machine;
	*default_path[0] += SLASH"bin";
	default_path[1] = NULL;
    }

    /* set the include directory name */
    String *incl_tmp = new String(suif_top);
    *incl_tmp += SLASH;
    *incl_tmp += target_machine;
    *incl_tmp += SLASH"include";
    suif_include = incl_tmp->string();

    /* set the lib directory name */
    String *lib_tmp = new String(suif_top);
    *lib_tmp += SLASH;
    *lib_tmp += target_machine;
    *lib_tmp += SLASH"lib";
    suif_lib = lib_tmp->string();

    /* set up the pass table */
    for (p = 0; p < last_pass; p++) {
	pp = &passtbl[p];
	pp->dir = (*pp->set_dir)();
	(*pp->set_flags)(pp);
	/* allow -yes and -no options to override defaults */
	if (pp->exec == UNKNOWN) pp->exec = (*pp->set_exec)();
    }

    /* run the passes.... */
    process_files();
    return 0;
}



extern void string_from_file(const char *file_name, String *the_string)
  {
    FILE *fp = fopen(file_name, "r");
    if (fp == NULL)
        return;
    int inchar = fgetc(fp);
    while (inchar != EOF)
      {
        if ((inchar == '\n') || (inchar == 0))
            *the_string += ' ';
        else
            *the_string += (char)inchar;
        inchar = fgetc(fp);
      }
    fclose(fp);
  }

char ** read_flag(char **argv)
{
    passes p;

    switch (argv[0][1]) {

	case 'a':
	    if (!strcmp(argv[0], "-automatic")) {
		automatic_flag = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'c':
	    if (!strcmp(argv[0], "-checkwarn")) {
		checksuif = TRUE;
		break;
	    }
	    if (!strcmp(argv[0], "-checkfail")) {
		checksuif = TRUE;
		checkfail = TRUE;
		break;
	    }
	    if (!strcmp(argv[0], "-cc")) {
		if (argv[1] == 0)
		    error(1, "usage: -cc <back-end C compiler name>");
		passtbl[BACKEND_CC].cmdname = argv[1];
		passtbl[LD].cmdname = argv[1];
		alternate_cc = TRUE;
		argv++;
		break;
	    }
	    if (!strcmp(argv[0], "-c")) {
		s_target = s_o;
		break;
	    }
	    bad_option(argv[0]);

	case 'd':
	    bad_option(argv[0]);

	case 'f':
	    if (!strcmp(argv[0], "-f2c")) {
		sf2c_flag = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'g':
	    if (!strcmp(argv[0], "-g")) {
		g_flag = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'k':
	    if (!strcmp(argv[0], "-keep")) {
		keepflag++;
		if (keepflag > 1) tmpdir = ".";
		break;
	    }
	    if (!strcmp(argv[0], "-k")) {
		nullflag = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'l': 
	    *passtbl[LD].pass_options += argv[0];
	    *passtbl[LD].pass_options += " ";
	    break;

	case 'm':
	    bad_option(argv[0]);

	case 'n':
	    if (!strcmp(argv[0], "-no")) {
		if (argv[1] == 0) error(1, "usage: -no <pass>");
		if ((p = find_pass(argv[1])) == last_pass)
		    error(1, "-no: unknown pass \"%s\"", argv[1]);
		passtbl[p].exec = FALSE;
		argv++;
		break;
	    }
	    if (!strcmp(argv[0], "-noreassoc")) {
		reassociate_arrays = FALSE;
		break;
	    }
	    bad_option(argv[0]);

	case 'o':
	    if (!strcmp(argv[0],"-option")) {
		if (argv[1] == 0 || argv[2] == 0)
		    error(1, "usage: -option <pass> <option>");
		if ((p = find_pass(argv[1])) == last_pass)
		    error(1, "-option: unknown pass \"%s\"", argv[1]);
		*passtbl[p].pass_options += argv[2];
		*passtbl[p].pass_options += " ";
		argv += 2;
		break;
	    }
	    if (!strcmp(argv[0], "-o")) {
		if (argv[1] == 0)
		    error(1, "-o must have file argument");
                if (outfilename != NULL)
                    error(1, "multiple -o files specified");
		outfilename = new String(argv[1]);
		argv++;
		break;
	    }
	    bad_option(argv[0]);

	case 'p':
	    bad_option(argv[0]);

	case 'r':
	    if (!strcmp(argv[0], "-reassoc")) {
		reassociate_arrays = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 's':
	    if (!strcmp(argv[0], "-s2c")) {
		break;
	    }
	    if (!strcmp(argv[0], "-sf2c")) {
		sf2c_flag = TRUE;
		break;
	    }
	    if (!strcmp(argv[0], "-static")) {
		automatic_flag = FALSE;
		break;
	    }
	    if (!strcmp(argv[0], "-show")) {
		verbose = TRUE;
		break;
	    }
	    if (!strcmp(argv[0], "-suif-link")) {
		option_linksuif = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 't':
	    tmpdir = &argv[0][2];
	    if (argv[0][2] == 0) tmpdir = ".";
	    break;

	case 'v':
	    if (!strcmp(argv[0], "-version")) {
		version_only = TRUE;
		break;
	    }
	    if (!strcmp(argv[0], "-v")) {
		verbose = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'w':
	    if (!strcmp(argv[0], "-w")) {
		no_warn_flag = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'y':
	    if (!strcmp(argv[0], "-yes")) {
		if (argv[1] == 0)
		    error(1, "usage: -yes <pass>");
		if ((p = find_pass(argv[1])) == last_pass)
		    error(1, "-yes: unknown pass \"%s\"", argv[1]);
		passtbl[p].exec = TRUE;
		argv++;
		break;
	    }
	    bad_option(argv[0]);

	case 'B':
	    suif_bin = &argv[0][2];
	    if (argv[0][2] == '\0')
		error(1, "usage: -Bprefix");
	    break;

	case 'E':
	    if (!strcmp(argv[0],"-E")) {
		s_target = s_i;
		out_to_stdout = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'G':
	    if (!strcmp(argv[0], "-G")) {
		if (argv[1] == 0) error(1, "-G must have argument");
		char *p;
		Gnum = strtol(argv[1], &p, 10);
		if (*p) error(1, "-G must have number argument");
		if (Gnum < 0)
		    error(1, "-G must have non-negative argument");
		argv++;
		break;
	    }
	    bad_option(argv[0]);

	case 'L':
	    *passtbl[LD].pass_options += argv[0];
	    *passtbl[LD].pass_options += " ";
	    break;

	case 'M':
	    if (!strcmp(argv[0], "-M")) {
		s_target = s_i;
		out_to_stdout = TRUE;
		pp_deps = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'N':
	    *passtbl[SF2C].pass_options += argv[0];
	    *passtbl[SF2C].pass_options += " ";
	    break;

	case 'D':	/* defines for cpp */
	case 'I':	/* include directory for cpp */
	case 'U':	/* undefines for cpp */
	    *passtbl[CPP].pass_options += argv[0];
	    *passtbl[CPP].pass_options += " ";
	    break;

	case 'O':	/* run scalar opts */
	    if (!strncmp(argv[0], "-O", 2)) {
		if (argv[0][2] == 0) {
		    opt_level = 1;
		} else if ((argv[0][2] >= '0') && (argv[0][2] <= '9') &&
			   (argv[0][3] == 0)) {
		    opt_level = argv[0][2] - '0';
		} else {
		    bad_option(argv[0]);
		}
	    } else {
		bad_option(argv[0]);
	    }
	    break;

	case 'S':
	    if (!strcmp(argv[0], "-S")) {
		s_target = s_s;
	    } else {
		bad_option(argv[0]);
	    }
	    break;

	case 'T':
	    if (!strcmp(argv[0], "-T")) {
		timing = TRUE;
	    } else if (!strcmp(argv[0], "-Target")) {
		if (argv[1] == 0)
		    error(1, "usage: -Target <target machine name>");
		target_machine = argv[1];
		argv++;
		break;
	    } else {
		bad_option(argv[0]);
	    }
	    break;

	case 'V':
	    if (!strcmp(argv[0], "-V")) {
		verbose = TRUE;
		break;
	    }
	    bad_option(argv[0]);

	case 'W':
	    if (!strcmp(argv[0], "-Wall")) {
		no_warn_flag = FALSE;
		break;
	    }
	    bad_option(argv[0]);

	case '.':
	    for (s_target = s_c; s_target <= s_o; s_target++) {
		if (!strcmp(&argv[0][2], suffixes[s_target]))
		    break;
	    }
	    if (s_target > s_o) {
		char buf[1024], buf2[10];
		sprintf(buf, "bad -. option: %s\nUse one of: ",
		    argv[0]);
		for (s_target = s_c; s_target <= s_o; s_target++) {
		    sprintf(buf2, "-.%s ", suffixes[s_target]);
		    strcat(buf, buf2);
		}
		error(1, buf);
	    }
	    break;

	default:
	    bad_option(argv[0]);
    }

    return argv;
}



passes find_pass(const char *name)
{
    int p;

    for (p = 0; p < last_pass; p++) {
	if (!strcmp(name, passtbl[p].passname)) break;
    }

    return (passes)p;
}



/*  Find the first pass to run over the input file, based on the suffix of
    the filename.  */

passes find_start_pass(const char *infile)
{
    Suffix infile_sfx, pass_sfx;
    int p;

    infile_sfx = getsfx(infile);
    if (infile_sfx == s_tmp) {
	error(1, "Unknown suffix (%s)", infile);
    }

    /* search the command table for the first pass which produces files with
     * a suffix that is greater than or equal to the infile suffix */
    for (p = 0; p < last_pass; p++) {
	pass_sfx = passtbl[p].suffix;
	if ((pass_sfx != s_tmp) && (pass_sfx >= infile_sfx)) break;
    }

    /* skip over passes that produce the same suffix as the infile suffix */
    while (pass_sfx == infile_sfx) {
	p += 1;
	pass_sfx = passtbl[p].suffix;
    }

    return (passes)p;
}



void process_files()
{
    passes p, last_real_sfx;
    int next_many_in_pass;
    pass *pp;
    boolean reached_target = FALSE;

    p = (passes)0;
    while (p <= last_pass) {

	/* Search through the pass table from the current pass until finding
	 * a pass that requires all of the source files at once (as indicated
	 * by the file_counts flag) or that generates the desired target.  */

	for (next_many_in_pass = p; next_many_in_pass < last_pass;
             next_many_in_pass++) {

	    if (reached_target) break;
	    pp = &passtbl[next_many_in_pass];

	    if (pp->suffix != s_tmp) {
		if (pp->suffix > s_target) {
		    reached_target = TRUE;
		    next_many_in_pass = last_real_sfx + 1;
		    break;
		}
		last_real_sfx = (passes)next_many_in_pass;
	    }

	    if (!pp->exec) continue;
	    if ((pp->file_counts == MANY_IN_ONE_OUT) ||
                (pp->file_counts == MANY_IN_MANY_OUT)) {
                break;
            }
	    if (pp->suffix == s_target) {
		reached_target = TRUE;
	    }
	}

	/* Compile the individual files up to the next requiring all input
         * files at once.  Note: here and throughout the remaining steps of
         * the compilation, we make sure that each filename is kept on one
         * of the filelists (infiles, * outfiles, or tmpfiles) so that we
         * can clean things up correctly if we're interrupted.  */

        filelistiter fiter(outfiles);
        while (!fiter.is_empty()) {
            filename *test_file = (filename *)(fiter.step());
            if (test_file->start_pass < next_many_in_pass) {
                transfer_filename(test_file, outfiles, tmpfiles);
            }
        }

        parallel_passes = contains_multiple_files(tmpfiles);

        fiter.reset(tmpfiles);
	while (!fiter.is_empty()) {
            filename *infile = (filename *)(fiter.step());
	    run_passes(infile, (passes)next_many_in_pass);
	}

        parallel_passes = FALSE;

	if (reached_target || (next_many_in_pass >= last_pass)) break;

	run_many_in_pass((passes)next_many_in_pass);
	if (passtbl[next_many_in_pass].suffix == s_target)
            reached_target = TRUE;

	p = (passes)(next_many_in_pass + 1);
    }
}



/*  This returns true if and only if the_list contains at least two files. */

boolean contains_multiple_files(filelist *the_list)
{
    filelistiter fiter(the_list);
    if (fiter.is_empty())
        return FALSE;
    (void)(fiter.step());
    return !(fiter.is_empty());
}



/*  Run a series of passes over the input file, beginning with the pass
    specified in the filename structure and stopping BEFORE running the
    stop_pass.  The name of the final output file is returned.  */

void run_passes(filename *orig_infile, passes stop_pass)
{
    filename *infile, *outfile;
    int p;

    /* ignore files that are not used until after stop_pass */
    passes start_pass = orig_infile->start_pass;
    if (start_pass >= stop_pass)
        return;

    /* the original infile is kept around to record the filename base */
    infile = orig_infile;
    transfer_filename(infile, tmpfiles, outfiles);

    for (p = start_pass; p < stop_pass; p++) {

	/* skip over passes that don't execute */
	if (!passtbl[p].exec) continue;

        transfer_filename(infile, outfiles, infiles);
        outfile = choose_output_file((passes)p, infile->basename, TRUE);
	outfiles->append(outfile);

	run_pass_one_in_one_out((passes)p, infile->name.string(),
                                outfile->name.string());

	/* the output of this pass becomes the input for the next */
	infile = outfile;
    }
}



/*  List the names of all the files in the given filelist in a single string,
    separated by single space characters */

String list_names(filelist *files)
{
    String result;
    boolean first = TRUE;

    filelistiter fiter(files);
    while (!fiter.is_empty()) {
        filename *infile = (filename *)(fiter.step());
        if (first)
            first = FALSE;
        else
            result += " ";
        result += infile->name;
    }

    return result;
}



filename *choose_output_file(passes the_pass, String base,
                             boolean has_base)
{
    /* find the appropriate suffix for the output of this pass */
    Suffix outfile_sfx = passtbl[the_pass].suffix;

    Suffix next_sfx = s_tmp;
    Suffix save_sfx = s_tmp;
    passes the_next_pass = (passes)(the_pass + 1);

    /* check through the passes that won't be executed */
    while (!passtbl[the_next_pass].exec) {
	Suffix pass_sfx = passtbl[the_next_pass].suffix;
	if ((pass_sfx != s_tmp) && (pass_sfx <= s_target)) {

	    /* save the previous value of next_sfx */
	    if (pass_sfx != next_sfx) save_sfx = next_sfx;

	    next_sfx = pass_sfx;
	}
	the_next_pass = (passes)(the_next_pass + 1);
	if (the_next_pass >= last_pass) break;
    }

    /* go back to saved suffix if found a pass that will generate next_sfx */
    if ((the_next_pass < last_pass) &&
	(passtbl[the_next_pass].suffix == next_sfx)) {
	next_sfx = save_sfx;
    }

    /* replace the suffix if this pass produces a file with a temporary suffix
       or if this is the last pass before the target suffix is reached */
    if ((outfile_sfx == s_tmp) || (next_sfx == s_target)) {
	outfile_sfx = next_sfx;
    }

    String suffix_name = suffix_to_string(outfile_sfx);

    String base_used;
    if (has_base) {
        base_used = base;
    } else {
        if (outfilename != NULL) {
            base_used = base_from_name(*outfilename);
	} else {
            /* default name is "a"; this causes the linker to produce
               "a.out" */
            base_used += "a";
	}
    }

    String the_name;
    boolean will_erase;
    if (outfile_sfx == s_target) {
        will_erase = FALSE;
	if (!out_to_stdout) {
            if (parallel_passes) {
                if (outfilename != NULL) {
                    error(0, "Multiple output files: \"-o %s\" ignored.\n",
		          outfilename->string());
                }
                the_name = base_used;
                the_name += '.';
                the_name += suffix_name;
            } else {
                if (outfilename != NULL) {
                    the_name = *outfilename;
                } else {
                    the_name = base_used;
                    the_name += '.';
                    the_name += suffix_name;
                }
            }
        }
    } else {
        will_erase = ((keepflag == 0) ||
                      ((keepflag == 1) && (outfile_sfx == s_tmp)));
        if (will_erase) {
            the_name = new_temp_base();
        } else {
            the_name = base_used;
	}
        the_name += '.';
        the_name += suffix_name;
    }

    return new filename(the_name, base_used, the_next_pass, will_erase);
}



/*  This returns everything before the extension of a file name. */

String base_from_name(String the_name)
{
    const char *name_string = the_name.string();
    const char *extension = NULL;
    char *buffer;

    const char *follow = name_string;
    while (*follow != 0) {
        if (*follow == '/')
            extension = NULL;
        else if (*follow == '.')
            extension = follow;
        ++follow;
    }
    if (extension == NULL)
        extension = follow;

    buffer = new char[(extension - name_string) + 1];
    strncpy(buffer, name_string, (extension - name_string));
    buffer[extension - name_string] = 0;
    String return_value(buffer);
    delete buffer;
    return return_value;
}



/*  If the suffix is a temporary, this returns a new unique suffix.
    Otherwise, it returns the string for that suffix.  */

String suffix_to_string(Suffix the_suffix)
{
    static long int suffix_number = 0;
    String return_value;

    if (the_suffix == s_tmp) {
        char buf[32];

        sprintf(buf, "%ld", suffix_number++);
        return_value += buf;
    } else {
	return_value += suffixes[the_suffix];
    }
    return return_value;
}



/*  Run a pass that requires all of the input files at once.  The input
    files are removed from the infiles list, and the output file (as well as
    any of the input files which will be used in future passes) is added to
    the outlist.  */

void run_many_in_pass(passes this_pass)
{
    String outfile_string;
    String in_out_string;

    /*  Search through the output files and add any files that should be
        processed by this pass to the input files list.  */

    filelistiter fiter(outfiles);
    while (!fiter.is_empty()) {
	filename *test_file = (filename *)(fiter.step());
	if (test_file->start_pass <= this_pass) {
            transfer_filename(test_file, outfiles, infiles);
	}
    }

    if (passtbl[this_pass].file_counts == MANY_IN_ONE_OUT) {
        String dummy;
        filename *outfile = choose_output_file(this_pass, dummy, FALSE);
        outfiles->append(outfile);
        outfile_string += outfile->name;
        in_out_string += list_names(infiles);
        in_out_string += ' ';
        in_out_string += outfile->name;
    } else {
        parallel_passes = contains_multiple_files(infiles);
        filelistiter fiter(infiles);
        while (!fiter.is_empty()) {
            filename *one_file = (filename *)(fiter.step());
            filename *outfile =
                    choose_output_file(this_pass, one_file->basename, TRUE);
            outfiles->append(outfile);
            outfile_string += outfile->name;
            in_out_string += one_file->name;
            in_out_string += ' ';
            in_out_string += outfile->name;
            if (!fiter.is_empty()) {
                outfile_string += ' ';
                in_out_string += ' ';
            }
        }
        parallel_passes = FALSE;
    }

    if (!uses_specified_output(this_pass)) {
        error(0, "Pass %s cannot have multiple input files.\n"
                 "(the output file is not specified)",
              passtbl[this_pass].passname);
        cleanup();
    }

    String infiles_string = list_names(infiles);
    run_pass_basic(this_pass, infiles_string.string(),
                   outfile_string.string(), in_out_string.string());
}



/*  This function returns whether or not a particular pass uses the output
    name scc gives it or not.  It bases this decision on whether or not the
    %o flag is included in that pass's format string.  */

boolean uses_specified_output(passes p)
{
    pass *pp = &passtbl[p];

    const char *fmt_index = pp->cmdfmt;
    do {
	if (*fmt_index == '%') {
	    fmt_index++;
	    if ((*fmt_index == 'o') || (*fmt_index == 'a')) {
		return TRUE;
	    }
	}
    } while (*fmt_index++ != '\0');

    return FALSE;
}



void run_pass_one_in_one_out(passes p, const char *infile, const char *outfile)
{
    /* if the command format string doesn't include a %o field, then the
     * output file name cannot be specified; this flag is used to detect
     * the presence of the %o flag in the format string.  */
    boolean used_outfile = uses_specified_output(p);

    String in_out_string;
    in_out_string += infile;
    in_out_string += ' ';
    in_out_string += outfile;
    run_pass_basic(p, infile, outfile, in_out_string.string());

    if (!used_outfile) {

	/* Find the name of the output file.  This assumes that if the output
	 * filename cannot be specified that it defaults to just changing the
	 * suffix of the input file.  We just replace the suffix of the input
         * file.  */

	/* the pass must have a named suffix */
        Suffix pass_sfx = passtbl[p].suffix;
	if (pass_sfx == s_tmp) {
	    error(0, "Pass %s must be given a named suffix.\n"
		"(the output file is not specified)", passtbl[p].passname);
	    cleanup();
	}

	/* if the command didn't put the output in the right file,
	 * we have to move it there.  */
        String infile_name(infile);
        String actual_outfile = base_from_name(infile_name);
        actual_outfile += '.';
        actual_outfile += suffix_to_string(pass_sfx);
	move_file(actual_outfile.string(), outfile);
    }
}



/*  Run a pass over the specified input file(s) and put the output in the
    specified file.  The format for the command line, as well as the flags
    and pass_options, are read from the global pass table.  */

void run_pass_basic(passes p, const char *infile_names, const char *outfiles,
                    const char *in_out_files)
{
    String command_line;
    int flag_index = 0;
    pass *pp = &passtbl[p];

    if (timing) {
	command_line += TIMECMD;
	command_line += " ";
    }

    command_line += select_path(pp->cmdname, pp->dir);
    command_line += SLASH;
    command_line += pp->cmdname;
    command_line += " ";

    /*  Now we have to parse the command format string.... */
    const char *fmt_index = pp->cmdfmt;
    do {
	if (*fmt_index != '%') {
	    command_line += *fmt_index;
	} else {
	    fmt_index++;
	    switch (*fmt_index) {
		case 'i':
		    command_line += infile_names;
		    break;

		case 'o':
		    command_line += outfiles;
		    break;

		case 'a':
		    command_line += in_out_files;
		    break;

		case 'f':
		    if (pp->flags[flag_index]) {
			command_line += *pp->flags[flag_index];
		    }
		    if (++flag_index > MAX_FLAGS) {
			error(0, "Too many flags in command format for %s\n",
			    pp->passname);
			cleanup();
		    }
		    break;

		case 'p':
		    command_line += *pp->pass_options;
		    break;

		default:
		    error(0,"Unrecognized flag %%%c in command format for %s\n",
			*fmt_index, pp->passname);
		    cleanup();
	    }
	}
    } while (*fmt_index++ != '\0');

    if (verbose)
	fprintf(stderr, "%s: %s\n", pp->passname, command_line.string());

    if (!nullflag) {
	int stat = execute_cmd(command_line.string());
	if (stat)
	    cleanup();

	if (checksuif && (pp->out_file_format == OUTPUT_SUIF)) {
	    String check_command_line;

	    if (timing) {
		check_command_line += TIMECMD;
		check_command_line += " ";
	    }

	    check_command_line += select_path("checksuif", NULL);
	    check_command_line += SLASH;
	    check_command_line += "checksuif ";
	    if (checkfail)
		check_command_line += "-fail ";
	    else
		check_command_line += "-warn ";
	    check_command_line += outfiles;
	    stat = execute_cmd(check_command_line.string());
	    if (stat)
		cleanup();
	}
    }

    /* delete any temporary files */
    erase_files(infiles);
}



/*  Find a directory containing the executable cmd.  The optional directory
    argument is checked first if it is provided.  Otherwise, the directories
    in the default_path array are checked.  If none of the directories
    contain the executable, then an error is signaled and the program exits.  */

const char * select_path(const char *cmd, const char *dir)
{
    String path, *p;

#ifdef WIN32
	char *exeName = new char[strlen(cmd) + 10];
	sprintf(exeName, "%s.exe", cmd);
	cmd = exeName;
#endif

    /* first try the specified directory */
    if (dir) {
	path += dir;
	path += SLASH;
	path += cmd;
	if (!access(path.string(), X_OK)) return dir;
    }

    /* otherwise, try the default directories */
    int path_num = 0;
    while ((p = default_path[path_num++])) {
	path.clear();
	path += p->string();
	path += SLASH;
	path += cmd;
	if (!access(path.string(), X_OK)) return p->string();
    }

    /* none of the paths worked */
    error(0, "Cannot find an executable file for \"%s\".\n"
	"Check the value of your SUIFPATH variable.", cmd);
    cleanup();

#ifdef WIN32
	delete exeName;
#endif

    return NULL;
}



/*  Execute a command.  First, we have to break the command string into
    words and store the individual words into an array.  Next, we fork off
    a new process and exec the specified command, and finally, we check
    the return status of the child process.  Note: I tried using the system()
    call to execute commands via the shell.  This avoids having to split
    the command into words and allows use of wildcards and other shell
    features.  Unfortunately, the return status did not indicate whether
    the child process had been killed, so I went back to directly executing
    the commands.  */

/* make it all look like system V */
#ifndef WIFSTOPPED
#define WIFSTOPPED WIFSIGNALED
#define WSTOPSIG WIFTEMSIG
#endif

#define SCC_INIT_CMD_ITEM_BUFFER_SIZE 20

int execute_cmd(const char *cmd)
{
    pid_t pid, w;
    static char **cmd_items = NULL;
    static unsigned long cmd_item_space = 0;

    /* break the command into words */
    const char *start = cmd;
    const char *end = cmd;
    int i = 0;

    if (cmd_items == NULL)
      {
        cmd_items = new char *[SCC_INIT_CMD_ITEM_BUFFER_SIZE];
        cmd_item_space = SCC_INIT_CMD_ITEM_BUFFER_SIZE;
      }

    while (TRUE) {

	/* skip any leading blanks */
	while (*start == ' ') start++;
	if (*start == '\0') break;

	/* find the end of the word */
	boolean inside_quote = FALSE;
	boolean inside_dblquote = FALSE;
	end = start;
	while (TRUE) {
	    if (*end == '\0') break;
	    if (*end == '\'') {
		if (inside_quote) {
		    inside_quote = FALSE;
		} else if (!inside_dblquote) {
		    inside_quote = TRUE;
		}
	    }
	    if (*end == '"') {
		if (inside_dblquote) {
		    inside_dblquote = FALSE;
		} else if (!inside_quote) {
		    inside_dblquote = TRUE;
		}
	    }
	    if (!inside_quote && !inside_dblquote && (*end == ' ')) break;
	    end++;
	}

	/* store the word into the array */
	cmd_items[i] = new char[(end - start) + 1];
	strncpy(cmd_items[i], start, (end - start));
	cmd_items[i][(end - start)] = '\0';
        ++i;
        if ((unsigned)i == cmd_item_space)
          {
            char **new_cmd_items = new char *[cmd_item_space * 2];
            memcpy(new_cmd_items, cmd_items, cmd_item_space * sizeof(char *));
            delete[] cmd_items;
            cmd_items = new_cmd_items;
            cmd_item_space *= 2;
          }

	if (*end == '\0') break;
	start = end + 1;
    }
    cmd_items[i] = NULL;

#ifndef WIN32
    if ((pid = fork()) == -1) syscallerr("fork");

    /* check if this is now the child process */
    if (pid == 0) {
	/* execute the command */
	execv(cmd_items[0], cmd_items);
	/* should never reach this point */
	syscallerr("execv");
    }
    /* otherwise, this must be the parent process */

#else // WIN32

	char *exeName = new char[strlen(cmd_items[0]) + 10];
	sprintf(exeName, "%s.exe", cmd_items[0]);
	delete cmd_items[0];
	cmd_items[0] = exeName;

	int rc = _spawnv(_P_WAIT, cmd_items[0], (const char *const *) cmd_items);
	if (rc) {
	    fprintf(stderr, "%s\nFAILED (exit status 0x%x)\n", cmd, rc);
	    return rc;
	}

	return rc;
#endif



    /* delete the command items */
    for (i = i - 1; i >= 0; i--) {
	delete[] cmd_items[i];
    }

#ifndef WIN32
    /* wait for the child to finish */
    int retcode = 0;
    while ((w = wait(&retcode)) != pid) {
	if (w == -1) syscallerr("wait");
	/* I suppose w == 0 and I'm about to be interrupted.  Not sure. */
    }

    if (WIFEXITED(retcode)) {
	/* normal exit */
	int rc = WEXITSTATUS(retcode);
	if (rc) {
	    fprintf(stderr, "%s\nFAILED (exit status 0x%x)\n", cmd, rc);
	    return rc;
	}

    } else if (WIFSTOPPED(retcode)) {
	/* child process caught a signal */
	int sig = WSTOPSIG(retcode);
	fprintf(stderr, "%s\nFAILED (caught signal 0x%x)\n", cmd, sig);
	return -1;

    } else {
	/* I have no idea, time to panic */
 	fprintf(stderr, "%s\nFAILED (wait code 0x%x)\n", cmd, retcode);
	return -2;
    }

    return 0;
#endif
}



/*  Return a new unique temporary file base name. */

String new_temp_base()
{
    char buf[32];
    static long int base_number = 0;

    String f(tmpdir);
    f += "/";
    f += tmpbase;
    f += "_";
    sprintf(buf, "%ld", base_number++);
    f += buf;
    return f;
}



/*  Return the suffix of the given filename */

Suffix getsfx(const char *s)
{
    int c;
    const char *sfx = NULL;
    const char *fs = s;

    /* scan through the string and record the position of the last dot */
    while ((c = *s++)) {
	if (c == '.') sfx = s;
    }
    if (!sfx) {
	error(0, "missing suffix, file \"%s\"", fs);
	cleanup();
    }

    /* compare the suffix to all of those in the suffixes[] array */
    int r;
    for (r = s_F; r < s_tmp; r++) {
	if (!strcmp(suffixes[r], sfx)) break;
    }

    return (Suffix)r;
}



/*  If the filenames are not identical, move the first file to the second.  */

void move_file(const char *f1, const char *f2)
{
    if (nullflag)
	return;

    if (strcmp(f1, f2)) {
	String mvcmd(MVCMD);
	mvcmd += " ";
	mvcmd += f1;
	mvcmd += " ";
	mvcmd += f2;
	int stat = execute_cmd(mvcmd.string());
	if (stat) {
	    error(0, "Unable to move temporary file \"%s\" to \"%s\".", f1, f2);
	    cleanup();
	}
    }
}



/*  The interrupt() function is called when we catch a signal.  This gives
    us a chance to remove any temporary output files before exiting.  */

void interrupt(int)
{
    cleanup();
}



void bad_option(const char *s)
{
    error(1, "unrecognized flag: %s", s);
}



void usage()
{
    fprintf(stderr, "Usage: %s [flags] file ...\n", cmdnam);
    exit(-1);
}



void error(int rc, const char *msg, ...)
{
    va_list ap;
    va_start(ap, msg);

    fprintf(stderr, "%s: ", cmdnam);
    vfprintf(stderr, msg, ap);

    putc('\n', stderr);
    if (rc != 0) exit(rc);
}



/*  Get rid of temporary files after detecting an error of some sort.  */

void cleanup()
{
    if (keepflag > 1) exit(-1);

    erase_files(infiles);
    erase_files(outfiles);
    erase_files(tmpfiles);
    if (moving_file != NULL)
        unlink(moving_file);

    exit(-1);
}



/*  Remove any temporary files on the specified list (also removes all of
    the filenames from the list and deletes them).  */

void erase_files(filelist *flist)
{
    filename *f;

    /* delete any temporary files */
    while (!flist->is_empty()) {
	f = (filename *)(flist->pop());
	/* try to remove the file -- don't worry if it fails */
	if (f->erase) unlink(f->name.string());
	delete f;
    }
}



void syscallerr(const char *s)
{
    fprintf(stderr, "system call error: %s\n", s);
    exit(-1);
}



/*  Safely transfer a file from one list to another, so it is always on at
    least one list or in the variable moving_file.  That way whenever we catch
    a signal, every temporary file will be recorded in at least one place, so
    it will be properly removed.  */

void transfer_filename(filename *to_transfer, filelist *from, filelist *to)
{
    moving_file = to_transfer->name.string();
    from->remove(to_transfer);
    to->append(to_transfer);
    moving_file = NULL;
}



filename::filename(String the_name, String the_basename,
                   passes the_start_pass, boolean will_erase)
{
    name = the_name;
    basename = the_basename;
    start_pass = the_start_pass;
    erase = will_erase;
}
