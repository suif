/* file "c.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* main snoot header file */

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <suif1.h>
#include <useful.h>
#include "target_info.h"

/* default sizes */
#define MAXLINE   512           /* maximum output line length */
#define MAXTOKEN   32           /* maximum token length */
#define BUFSIZE  4096           /* input buffer size */
#define HASHSIZE  128           /* default hash table size */
#define MEMINCR    10           /* blocks (1kb) allocated per arena */

#include "ops.h"

#include <stdarg.h>

enum tokencode
  {
#define xx(a,b,c,d,e,f,g) a=b,
#define yy(a,b,c,d,e,f,g)
#include "token.h"
    NTOKENS
  };

typedef void *Generic;

typedef struct list                     /* lists: */
  {
    Generic x;                              /* element */
    struct list *link;                      /* next node */
  } *List;

typedef struct symbol *Symbol;          /* symbol table entries */
typedef struct table *Table;            /* symbol tables */
typedef struct field *Field;            /* struct/union fields */
typedef struct bit_field_data bit_field_data;
typedef struct swtch *Swtch;            /* switch data */

typedef union value             /* constant values: */
  {
    int i;                          /* signed */
    char *p;                        /* pointer to anything */
  } Value;

typedef struct coord            /* source coordinates: */
  {
    const char *file;               /* file name */
    unsigned short x, y;            /* x,y position in file */
  } Coordinate;


    /* The following are defined in "gen.cc". */

extern void address(Symbol q, Symbol p, int n, char *field_name);
extern void defconst(var_def *the_def, type_node *the_type, immed value);
extern void defstring(var_def *the_def, int len, char *s, type_node *the_type);
extern void defsymbol(Symbol s);
extern void function(void);
extern var_sym *gen_static_global(type_node *ty);
extern var_sym *gen_static_local(type_node *ty, char *name);
extern var_def *global(Symbol s);
extern var_def *begin_initialization(var_sym *to_initialize);
extern void space(var_def *the_def, int size);
extern void stabblock(int enter_or_exit);
extern void stabline(Coordinate *coord);
extern base_symtab *get_current_symtab(void);
extern void current_list_append_node(tree_node *the_node);
extern void current_list_append_list(tree_node_list *the_list);

/* SUIF additions */
extern void start_function(Symbol f, Symbol callee[],
                           type_node *caller_types[], int num_params,
                           Coordinate *block_location);

extern tree_node_list *curr_list;
extern file_set_entry *outf;
extern proc_sym *curr_proc;

/* limits */
#include <limits.h>
#include <float.h>

/* data structures */

struct symbol           /* symbol structures: */
  {
    sym_node *suif_symbol;  /* SUIF symbol */
    const char *name;       /* name */
    unsigned short scope;   /* scope level */
    unsigned char sclass;   /* storage class */
    unsigned defined:1;     /* 1 if defined */
    unsigned temporary:1;   /* 1 if a temporary */
    unsigned computed:1;    /* 1 if an address computation identifier */
    unsigned initialized:1; /* 1 if local is initialized */
    int ref;                /* weighted # of references */
    type_node *type;        /* data type */
    Coordinate src;         /* definition coordinate */
    Coordinate **uses;      /* array of Coordinate *'s for uses */
    Symbol up;              /* next symbol in this or outer scope */
    union
      {
        int enum_value; /* enumeration identifiers: value */
        struct          /* constants: */
          {
            Value v;        /* value */
            var_sym *loc;   /* out-of-line location */
          } c;
        struct          /* functions: */
          {
            Coordinate pt[3];/* source code coordinates */
            var_sym *null_return; /* null value to return if no return value
                                     is given */
          } f;
      } u;
  };

enum
  {
    CONSTANTS=1, LABELS, GLOBAL, PARAM, LOCAL
  };

/* misc. macros */

#ifdef roundup
#undef roundup
#endif
#define roundup(x,n) (((x)+(n)-1)-(((x)+(n)-1)%(n)))

#ifndef assert
#ifdef NDEBUG
#define assert(c)
#else
#define assert(c) ((void)((c) || fatal(__FILE__,\
    "assertion failure at line %d\n",__LINE__)))
#endif
#endif

#define islabel(p) ((p) && (p)->op == LABEL+V && (p)->syms[0])

struct bit_field_data
  {
    short from, to;         /* bit fields: bits from..to */
    short ref_type_size;    /* size of full-blown version of this bit field's
                               type */
    boolean ref_type_signed;/* TRUE iff full-blown version of this bit field's
                               type is signed */
  };

class genop             /* Generalized SUIF operands */
  {
private:
    operand the_suif_operand;
    tree_node_list *the_precomputation;

public:
    genop(void)
      {
        the_suif_operand = operand();
        the_precomputation = NULL;
      }
    genop(operand initial_suif_operand,
          tree_node_list *initial_precomputation)
      {
        assert(initial_precomputation != NULL);
        the_suif_operand = initial_suif_operand;
        the_precomputation = initial_precomputation;
      }

    genop& operator=(const genop &source)
      {
        the_suif_operand = source.suif_operand();
        the_precomputation = source.precomputation();
        return *this;
      }

    operand suif_operand(void) const
      {
        return the_suif_operand;
      }
    tree_node_list *precomputation(void) const
      {
        return the_precomputation;
      }

    boolean is_addressable(void);
    type_node *type(void);
    tree_node_list *side_effects_only(void);
    void make_temporary(void);
    boolean is_int_const(void);
    boolean is_boolean_type(void);
    boolean is_null_pointer_constant(void);
    boolean is_integral_constant_expression(void);
    void make_bit_field_ref(unsigned short from, unsigned short to,
                            type_node *result_type);
    boolean is_bit_field_ref(void);
    void undo_bit_field_ref(unsigned short *from, unsigned short *to,
                            type_node **result_type);
    void double_bit_field_ref(genop *op1, genop *op2);

    void deallocate(void);
    void clean_up_bit_field_refs(void);
    eval_status evaluate_as_const(immed *result);
  };

typedef struct arena *Arena;

struct arena                    /* storage allocation arena: */
  {
    int m;                          /* size of current allocation request */
    char *avail;                    /* next available location */
    char *limit;                    /* address 1 past end of arena */
    Arena first;                    /* pointer to first arena */
    Arena next;                     /* link to next arena */
  };

#define yyalloc(n,ap) (ap->m = roundup(n,sizeof(double)), \
    ap->avail + ap->m >= ap->limit ? allocate(ap->m, &ap) : \
    (ap->avail += ap->m, ap->avail - ap->m))
#define alloc(n)  yyalloc(n, permanent)
#define talloc(n) yyalloc(n, transient)

struct field            /* struct/union fields: */
  {
    const char *name;       /* field name */
    type_node *type;        /* data type */
    int offset;             /* field offset */
    short from, to;         /* bit fields: bits from..to */
    const char *block_name; /* for bit fields, the name of the SUIF field
                               containing this bit field; otherwise this
                               is the same as ``name'' */
    type_node *block_type;  /* for bit fields, the type of the SUIF field
                               containing this bit field; otherwise this
                               is the same as ``type'' */
    Field link;             /* next field in this type */
  };

#define fieldsize(p) ((p).to - (p).from)
#define fieldright(p) (target.is_big_endian ? \
                       ((p).ref_type_size - (p).to) : (p).from)
#define field_right   (target.is_big_endian ? \
                       (result_type->size() - bit_field_to) : bit_field_from)
#define fieldmask(p) (~(i_integer(-1) << fieldsize(p)))
#define fieldleft(p) ((p).ref_type_size - fieldsize(p) - fieldright(p))

/*
 * type-checking macros.
 * the operator codes are defined in token.h
 * to permit the range tests below; don't change them.
 */
#define isqual(t)       ((t)->op >= CONST)
#define isvolatile(t)   ((t)->is_volatile())
#define isconst(t)      ((t)->is_const())
#define isarray(t)      ((t)->unqual()->op() == TYPE_ARRAY)
#define isstruct_or_union(t) \
                        (((t)->unqual()->op() == TYPE_STRUCT) || \
                         ((t)->unqual()->op() == TYPE_UNION))
#define isunion(t)      ((t)->unqual()->op() == TYPE_UNION)
#define isfunc(t)       ((t)->unqual()->is_func())
#define isptr(t)        ((t)->unqual()->op() == TYPE_PTR)
#define ischar(t)       (((t)->unqual()->op() == TYPE_INT) && \
                         ((t)->unqual()->size() == target.size[C_char]))
#define isenum(t)       ((t)->unqual()->is_enum())
#define isint(t)        ((isenum(t)) || ((t)->unqual()->op() == TYPE_INT))
#define isfloat(t)      ((t)->unqual()->op() == TYPE_FLOAT)
#define isarith(t)      (((t)->unqual()->op() == TYPE_INT) || \
                         ((t)->unqual()->op() == TYPE_FLOAT) || \
                         ((t)->unqual()->is_enum()))
#define isunsigned(t)   (((t)->unqual()->op() == TYPE_INT) && \
                         (!((base_type *)((t)->unqual()))->is_signed()))
#define isdouble(t)     ((t)->unqual() == doubletype)
#define isscalar(t)     (((t)->unqual()->op() == TYPE_INT) || \
                         ((t)->unqual()->op() == TYPE_PTR) || \
                         ((t)->unqual()->op() == TYPE_FLOAT) || \
                         ((t)->unqual()->op() == TYPE_ENUM))

typedef void (*Apply)(Generic, Generic, Generic);

    /* The following are defined in "decl.cc". */

extern void compound(label_sym *continue_lab, label_sym *break_lab,
                     struct swtch *swp, int lev);
extern void finalize(void);
extern void program(void);
extern type_node *get_typename(void);

extern Symbol cfunc;
extern char *fname;

    /* The following are defined in "init.cc". */

extern int genconst(var_def *to_init, genop e, boolean def);
extern var_def *defglobal(Symbol p);
extern void initglobal(Symbol p, boolean flag);
extern type_node *initializer(var_def *to_init, type_node *ty, int lev);

    /* The following are defined in "tree.cc". */

extern char *allocate(int n, Arena *p);
extern void deallocate(Arena *p);

extern Arena permanent;
extern Arena transient;

    /* The following are defined in "expr.cc". */

extern genop addrof(genop p);
extern type_node *assign(type_node *xty, genop p);
extern genop cast(genop p, type_node *new_type);
extern genop cond(genop p);
extern genop conditional(int tok);
extern genop constexpr(int tok);
extern tree_node_list *expr0(int tok);
extern genop expr(int tok);
extern genop expr1(int tok);
extern genop field(genop p, char *name);
extern char *funcname(genop f);
extern genop idnode(Symbol p);
extern genop sym_node_to_genop(sym_node *the_symbol);
extern genop incr(tokencode op, genop e1, genop e2);
extern i_integer intexpr(int tok, int n);
extern genop lvalue(genop p);
extern genop pointer(genop p);
extern type_node *promote(type_node *ty);
extern genop rvalue(genop p);
extern genop sequence_point(genop the_genop);
extern tree_node *sequence_point_mark(Coordinate *where);

    /* The following are defined in "input.cc". */

extern void inputInit(FILE *fp);
extern void inputstring(char *str);
extern void fillbuf(void);
extern void nextline(void);

extern unsigned char *cp;
extern char *file;
extern char *firstfile;
extern unsigned char *limit;
extern char *line;
extern int lineno;

    /* The following are defined in "lex.cc". */

extern int getchr(void);
extern enum tokencode gettok(void);

extern char kind[];
extern Coordinate src;
extern enum tokencode t;
extern char *token;
extern Symbol tsym;
extern boolean concat_strings;        /* If TRUE (the default) adjacent
                                         string literals are concatenated */

    /* The following are defined in "main.cc". */

extern int main(int argc, char *argv[]);

extern int Aflag;
extern boolean Pflag;
extern boolean xref;
extern proc_sym *YYnull_proc;
extern boolean YYnull_used;

    /* The following are defined in "output.cc". */

extern void fprint(FILE *fp, const char *fmt, ...);
extern void print(char *fmt, ...);
extern void gprint(void (*s_printer)(const char *to_print, void *data),
                   void (*c_printer)(int to_print, void *data), void *data,
                   char *fmt, ...);
extern char *stringf(const char *fmt, ...);
extern void outflush(void);
extern void vfprint(FILE *fp, const char *fmt, va_list ap);

    /* The following are defined in "error.cc". */

extern void error(const char *fmt, ...);
extern int fatal(const char *name, const char *fmt, int n);
extern void warning(const char *fmt, ...);
extern int expect(int tok);
extern void skipto(int tok, char set[]);
extern void test(int tok, char set[]);

extern int errcnt;
extern int errlimit;
extern boolean wflag;

    /* The following are defined in "profio.cc". */

/* nothing */

    /* The following are defined in "enode.cc". */

extern genop asgnnode(int op, genop l, genop r);
extern genop bitnode(int op, genop l, genop r);
extern genop condnode(genop e, genop l, genop r);
extern genop constnode(i_integer n, type_node *ty);
extern genop eqnode(int op, genop l, genop r);
extern genop shnode(int op, genop l, genop r);
extern void typeerror(int op, genop l, genop r);

extern genop (*opnode[])(int, genop, genop);

    /* The following are defined in "simp.cc". */

extern genop simplify_convert(type_node *new_type, genop to_convert);

extern boolean needconst;

    /* The following are defined in "stmt.cc". */

extern Coordinate *definept(Coordinate *p);
extern void return_nothing(void);
extern void statement(label_sym *continue_lab, label_sym *break_lab,
                      struct swtch *swp, int lev);

extern i_rational density;
extern int refinc;

    /* The following are defined in "string.cc". */

extern List append(Generic x, List list);
extern int length(List list);
extern Generic *ltoa(List list, Generic a[]);
extern char *string(char *str);
extern char *stringd(int n);
extern char *stringn(char *str, int n);

    /* The following are defined in "sym.cc". */

extern Symbol symbol_for_string_const(type_node *ty, char *value);
extern void enterscope(void);
extern void exitscope(void);
extern void fielduses(Symbol p, Generic);
extern Symbol findtype(type_node *ty);
extern void foreach(Table tp, int lev, void (*apply)(Symbol, Generic),
                    Generic cl);
extern label_sym *genlabel(void);
extern label_sym *gen_user_label(char *name);
extern const char *gen_internal_name(void);
extern Symbol install(const char *name, Table *tpp, int perm);
extern Symbol lookup(const char *name, Table tp);
extern void setuses(Table tp);
extern Table table(Table tp, int lev);
extern void use(Symbol id_info, Coordinate src);
extern void annotate_with_refs(Symbol the_sym);

extern Table constants;
extern Table externals;
extern Table identifiers;
extern Table globals;
extern Table labels[2];
extern Table types;
extern int bnumber;
extern int level;

    /* The following are defined in "stypes.cc". */

extern void typeInit(void);
extern type_node *build_array(type_node *ty, int n);
extern type_node *atop(type_node *ty);
extern type_node *composite(type_node *ty1, type_node *ty2);
extern Symbol deftype(char *name, type_node *ty, Coordinate *pos);
extern type_node *deref(type_node *ty);
extern boolean eqtype(type_node *ty1, type_node *ty2, boolean ret);
extern Field fieldref(char *name, type_node *ty);
extern type_node *freturn(type_node *ty);
extern func_type *func(type_node *ty);
extern boolean hasproto(type_node *ty);
extern Field newfield(const char *name, struct field **field_list, 
		      type_node *fty, struct_type *to_fill);
extern Symbol newstruct(int op, const char *tag);
extern void outtype(void (*s_printer)(const char *to_print, void *data),
                    void (*c_printer)(int to_print, void *data), void *data,
                    type_node *ty);
extern void printdecl(Symbol p, type_node *ty);
extern void printproto(Symbol p, Symbol callee[]);
extern type_node *qual(int op, type_node *ty);
extern const char *typestring(type_node *ty, const char *str);
extern type_node *base_from_enum(type_node *the_type);
extern type_node *base_from_pointer(type_node *the_type);
extern type_node *base_from_array(type_node *the_type);
extern boolean compatible_types(type_node *type1, type_node *type2);
extern array_type *make_array(type_node *element_type);
extern boolean has_constant_in_fields(type_node *the_type);
extern type_node *create_function_type(type_node *return_type,
                                       type_node **arguments);
extern boolean field_is_bit_fields(struct_type *the_struct,
                                   unsigned field_num);
extern struct field *bit_field_list(struct_type *the_struct,
                                    unsigned field_num);
extern Table get_field_table(struct_type *the_struct);
extern void set_field_table(struct_type *the_struct, Table new_table);
extern type_node *unsigned_int_version(type_node *original_type);

extern type_node *chartype;
extern type_node *doubletype;
extern type_node *floattype;
extern type_node *inttype;
extern type_node *longdouble;
extern type_node *longtype;
extern type_node *longlong;
extern type_node *shorttype;
extern type_node *signedchar;
extern type_node *unsignedchar;
extern type_node *unsignedlong;
extern type_node *unsignedlonglong;
extern type_node *unsignedshort;
extern type_node *unsignedtype;
extern type_node *voidtype;
extern type_node *voidptype;
extern type_node *booleantype;
extern type_node *size_t_type;
extern Symbol chartype_sym;
extern Symbol doubletype_sym;
extern Symbol floattype_sym;
extern Symbol inttype_sym;
extern Symbol longdouble_sym;
extern Symbol longtype_sym;
extern Symbol longlong_sym;
extern Symbol shorttype_sym;
extern Symbol signedchar_sym;
extern Symbol unsignedchar_sym;
extern Symbol unsignedlong_sym;
extern Symbol unsignedlonglong_sym;
extern Symbol unsignedshort_sym;
extern Symbol unsignedtype_sym;
extern Symbol voidtype_sym;
extern replacements *global_replacements;

    /* The following are defined in "genop.cc". */

extern void init_annotes(void);
extern void init_bit_ref(void);
extern tree_node *last_action_node(tree_node_list *the_node_list);
extern tree_node *last_action_before(tree_node_list *the_node_list,
                                     tree_node *the_node);
extern boolean operand_may_have_side_effects(operand the_operand);
extern boolean is_same_operand(operand op1, operand op2);
extern void comment_text(char *comment_string);
extern immed_list *coordinate_to_immeds(Coordinate the_coordinate);
extern Coordinate *immeds_to_coordinate(immed_list *the_immeds);
extern void remove_unused_temps(tree_node_list *the_list);
extern immed immed_and_type_for_C_intconst(char *C_intconst,
                                           type_node **the_type);
extern immed immed_and_type_for_C_floatconst(char *C_floatconst,
                                             type_node **the_type);

extern const char *k_sequence_point;
extern const char *k_bit_field_info;
extern const char *k_type_name;
extern const char *k_type_source_coordinates;
extern const char *k_field_table;
extern const char *k_type_alignment;
extern const char *k_C_comment;
extern const char *k_source_coordinates;
extern const char *k_is_declared_reg;
extern const char *k_source_references;
extern const char *k_builtin_args;
extern const char *k_typedef_name;

extern boolean option_keep_comments;
extern boolean option_mark_execution_points;
extern boolean option_null_check;
extern boolean option_keep_typedef_info;
extern boolean option_force_enum_is_int;

    /* The following are defined in "builtin.cc". */

extern boolean is_builtin(char *id);
extern void type_builtin(in_gen *the_gen);
extern void register_builtin_info(char *this_info);
extern void init_builtins(void);
extern void register_when_builtins_initialized(const char *this_info);

    /* The following are defined in "config.cc". */

extern void initialize_targets(void);

extern char *target_names[];
extern target_info_block target_table[];
extern target_info_block native_target;
extern target_info_block *this_target;

    /* The following are defined in "find_params.c". */

extern "C" void get_info(target_info_block *info_block);
