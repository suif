/* file "gen.cc" of the snoot program for SUIF */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  The back end of the front end.
 *
 *  For help in understanding this code, read the documentation contained
 *  in this file after reading "A Code Generation Interface for ANSI C"
 *  by Fraser and Hanson from the lcc documentation.
 *
 *  History:
 *
 *        ? - 12/1992  Originally written and maintained for Old SUIF
 *                         by Rob French <rfrench@cs.stanford.edu>.
 *  12/1992 -  8/1993  Maintained and rewritten for New SUIF by
 *                         Todd Smith <tsmith@cs.stanford.edu>.
 *   8/1993 -  ?       Maintained and rewritten for even newer SUIF by
 *                        Chris Wilson <cwilson@cs.stanford.edu>
 *
 */

#define RCS_BASE_FILE gen_cc

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <suif1.h>
#include "c.h"

RCS_BASE(
    "$Id: gen.cc,v 1.2 1999/08/25 03:28:20 brm Exp $")



/*
 * Some symbol names will have to be generated.  The following
 * prefixes are used in naming various types of symbols.  If the
 * "passing type" of a parameter is not the same as the type inside
 * the called function, then it sends us the "passed parameter" and
 * the "real parameter"; the passed parameter gets a prefix and is
 * converted to the real one at the beginning of the function.
 * Temporary globals are given numbers in defsymbol(), to which the
 * prefix is prepended.  The last number given is kept in
 * curr_tmp_global.
 */
static char *tmp_string_prefix = "__tmp_string_";
static char *tmp_param_prefix  = "__tmp_param_";

int curr_tmp_global = -1;




file_set_entry *outf;              // this is where all the output goes
proc_sym *curr_proc = NULL;        // pointer to object for current procedure

/*
 *  The Stacks
 *
 *  We need three stacks to keep track of necessary information in a
 *  procedure:
 *    1) list_stack
 *        This is the stack of nested instruction lists which have
 *        currently been suspended.  The one in current use is stored
 *        in curr_list.  New lists are created for new blocks and for
 *        the various parts of SUIF macros (if's, loop's, and for's,
 *        via domacrocmd()).  curr_list is used for appending
 *        instructions as they are created.
 *    2) symtab_stack
 *        This is the stack of nested symbol tables which have
 *        currently been suspended, which corresponds exactly to the
 *        stack of currently suspended blocks.  The one in current use
 *        is stored in curr_symtab.  New symbol tables are created
 *        only for new blocks.  curr_symtab and symbol tables in the
 *        stack are used for entering local variables.
 *    3) block_num_stack
 *        This stack is a stack of block numbers that coincides with
 *        symtab_stack.  A sequence of blocks with the same parent
 *        block is numbered 0...? in the position in this stack
 *        corresponding to that nesting depth; thus, the numbers on
 *        the stack up to the current nesting depth give each block a
 *        unique hierarchical name.  symtab_stack functions handle
 *        this stuff and always keep the current number for use in
 *        curr_block_num.
 */

const int MAX_NESTING = 1000;          // Maximum number of nested lists or
                                       //  symbol tables

tree_node_list *curr_list = NULL;
tree_node_list *list_stack[MAX_NESTING];
int list_stack_ptr = -1;

block_symtab *curr_symtab = NULL;
block_symtab *symtab_stack[MAX_NESTING];
int symtab_stack_ptr = -1;
int block_num_stack[MAX_NESTING+2];
int curr_block_num = -1;


/*
 * The top block of a function is created in start_function(), so
 * when stabblock() is called the first time, we don't want to create
 * it again.  This boolean tells if stabblock() has skipped it yet.
 * It is also used at the end of functions when blocks are ended.
 */
int skipped_top_block = 0;

int curr_nesting_level = -1;       // 0 == top block of a function




#define MAXARGS 1000               // Maximum number of procedure arguments
int num_args = 0;
operand args[MAXARGS];             // store up the arguments before a call


void append_instr(instruction *instr);

static void finalize_suif_proc(proc_sym *the_proc);
static void finalize_suif_node(tree_node *the_node, void *);
static void finalize_suif_instr(instruction *the_instr, void *);
static annote *make_line_annote(Coordinate *location);



/********************************************************************
 * FUNCTIONS FOR HANDLING STACKS                                    *
 ********************************************************************/

void symtab_stack_init(void)  { block_num_stack[1] = -1; }
int symtab_stack_empty(void)  { return (symtab_stack_ptr == -1); }

void symtab_stack_push(block_symtab *b)
  {
    assert(symtab_stack_ptr < MAX_NESTING-1);
    symtab_stack[++symtab_stack_ptr] = b;
    curr_block_num = ++(block_num_stack[symtab_stack_ptr+1]);
    block_num_stack[symtab_stack_ptr+2] = -1;
  }
block_symtab *symtab_stack_pop(void)
  {
    assert(symtab_stack_ptr > -1);
    curr_block_num = block_num_stack[symtab_stack_ptr];
    return symtab_stack[symtab_stack_ptr--];
  }


void list_stack_push(tree_node_list *tnl)
  {
    assert(list_stack_ptr < MAX_NESTING-1);
    list_stack[++list_stack_ptr] = tnl;
  }
tree_node_list *list_stack_pop(void)
  {
    assert(list_stack_ptr > -1);
    return list_stack[list_stack_ptr--];
  }



/********************************************************************
 * MISCELLANEOUS USEFUL FUNCTIONS                                   *
 * 1. functions dealing with scopes and symbol tables               *
 ********************************************************************/

// returns currently active symbol table
extern base_symtab *get_current_symtab(void)
  {
    return ((curr_symtab == NULL) ? ((base_symtab *)(outf->symtab())) :
                                    ((base_symtab *)curr_symtab));
  }







/********************************************************************
 * LCC BACK-END FUNCTIONS FOR FUNCTION DEFINITION                   *
 ********************************************************************/


/*
 * addition to lcc interface; called a little while before function()
 * in order to set up the current procedure before defsymbol() is
 * called with some important information
 */
extern void start_function(Symbol f, Symbol callee[],
                           type_node *caller_types[], int num_params,
                           Coordinate *block_location)
  {
    assert(f->suif_symbol != NULL);

    /*
     *  function symbol should have already been sent to defsymbol();
     *  set up all its characteristics, and make sure it isn't marked as
     *  extern
     */
    curr_proc = (proc_sym *) f->suif_symbol;
    curr_proc->set_src_lang(src_c);

    assert(symtab_stack_empty());
    assert(curr_nesting_level == -1);

    // set up function to receive symbols, types, code
    tree_block *b = curr_proc->block();
    curr_list = b->body();
    curr_symtab = b->symtab();
    symtab_stack_init();
    curr_nesting_level = 0;
    skipped_top_block = 0;

    b->annotes()->append(make_line_annote(block_location));

    /* parameters are in reverse order */
    for (int param_num = num_params; param_num > 0; --param_num)
      {
        Symbol s = callee[param_num - 1];

        var_sym *the_var = curr_symtab->new_var(s->type, s->name);
        the_var->set_userdef();
        s->suif_symbol = the_var;

        if (s->sclass == REGISTER)
            the_var->append_annote(k_is_declared_reg, new immed_list);

        var_sym *the_param = the_var;
        if (s->type->unqual() != caller_types[param_num - 1]->unqual())
          {
            char *temp_name =
                    new char[strlen(tmp_param_prefix) + strlen(s->name) + 1];
            strcpy(temp_name, tmp_param_prefix);
            strcat(temp_name, s->name);
            the_param =
                    curr_symtab->new_var(caller_types[param_num - 1],
                                         temp_name);
            operand param_op =
                    fold_real_1op_rrr(io_cvt, the_var->type()->unqual(),
                                      operand(the_param));
            curr_list->append(create_assignment(the_var, param_op));
            delete[] temp_name;
          }

        the_param->set_param();
        curr_proc->block()->proc_syms()->params()->append(the_param);
      }

    annote *a = new annote(k_enable_exceptions);
    a->immeds()->append("int_divide-by-0");
    curr_proc->block()->annotes()->append(a);
  }


void function(void)
  {
    // clean up after the function
    curr_symtab = NULL;
    curr_list = NULL;
    assert(symtab_stack_empty());
    assert(curr_nesting_level == 0);

    curr_nesting_level = -1;

    finalize_suif_proc(curr_proc);
    curr_proc->write_proc(outf);
    curr_proc->flush_proc();
  }



/********************************************************************
 * BEGINNING/END OF BLOCK FUNCTIONS                                 *
 ********************************************************************/


// enter/exit a block in the emit phase, with its user-defined locals
void stabblock(int enter_or_exit)
  {
    // block entry code
    if (enter_or_exit == '{')
      {
        // if we just started a procedure and created its top block,
        //   then skip it and don't do anything here
        if (curr_nesting_level == 0 && !skipped_top_block)
            skipped_top_block = 1;
        // otherwise, push current list/symtab and create a child block
        else
          {
            list_stack_push(curr_list);
            symtab_stack_push(curr_symtab);

            tree_node_list *new_list = new tree_node_list;
            block_symtab *new_symtab =
                    new block_symtab(stringf("%d", curr_block_num));

            tree_block *b = new tree_block(new_list, new_symtab);
            curr_list->append(b);

            curr_list = new_list;
            if (curr_symtab != NULL)
                curr_symtab->add_child(new_symtab);
            curr_symtab = new_symtab;

            curr_nesting_level++;
          }
      }
    // block exit code
    else if (enter_or_exit == '}')
      {
        // if we are ending a procedure, then reset skipped_top_block
        //   and don't do anything yet
        if (curr_nesting_level == 0 && skipped_top_block)
          {
            skipped_top_block = 0;
          }
        else
          {
            // otherwise, pop parent block from stack
            tree_node_list *next_list = list_stack_pop();
            curr_list = next_list;
            curr_symtab = symtab_stack_pop();
            curr_nesting_level--;
          }
      }
    else
        assert(0);
  }



/********************************************************************
 * LCC BACK-END FUNCTIONS FOR DEFINING SYMBOLS, ETC.                *
 ********************************************************************/

/*
 * This function is called by lcc to define constants, globals, and statics.
 */
void defsymbol(Symbol s)
  {
    assert(s->name != NULL);

    // if already processed, ignore it, except for special cases
    if (s->suif_symbol != NULL)
      {
        if (s->type != NULL &&
	    s->type->unqual()->is_func() &&
	    s->scope == GLOBAL &&
	    s->suif_symbol != NULL)
	  {
	    // check if the type has been completed
            if (s->suif_symbol->is_var())
              {
                var_sym *v = (var_sym *)s->suif_symbol;
                type_node *curr_type = s->type;
                if (curr_type->size() > v->type()->size())
                  {
                    boolean vis = v->parent()->make_type_visible(curr_type);
                    assert(vis);
                    v->set_type(curr_type);
                  }
              }
	  }

	return;
      }

    /*
     * all functions declared are entered in global symbol table; if this
     * function is actually defined later, it will be looked up and
     * filled in
     */
    if (s->type != NULL && isfunc(s->type))
      {
	/*
	 * lcc seems to have a bug that causes it to generate multiple symbols
	 * for the same procedure in certain circumstances so we have to check
	 * for duplicates here
	 */
	proc_sym *p = outf->symtab()->lookup_proc(s->name);
	if (!p)
	  {
	    func_type *ft = (func_type *)s->type;
            fflush(stdout);
            global_symtab *the_symtab;
            if (s->sclass == STATIC)
                the_symtab = outf->symtab();
            else
                the_symtab = fileset->globals();
            boolean vis = the_symtab->make_type_visible(ft);
            assert(vis);
	    p = the_symtab->new_proc(ft, src_unknown, s->name);
	    proc_symtab *st = new proc_symtab(s->name);
	    p->set_block(new tree_proc(new tree_node_list, st));
	    outf->symtab()->add_child(st);

	    p->set_userdef();
	  }
	s->suif_symbol = p;
      }

    // constants get no symbol table entry
    else if (s->scope == CONSTANTS)
        ;

    /*
     * global non-function symbols get same name (or generated name),
     *  put in global symbol table
     */
    else if (s->scope == GLOBAL)
      {
        global_symtab *the_symtab;
        if (s->sclass == STATIC)
            the_symtab = outf->symtab();
        else
            the_symtab = fileset->globals();

        boolean vis = the_symtab->make_type_visible(s->type);
        assert(vis);
        var_sym *v = the_symtab->new_var(s->type, s->name);

        v->set_userdef();
        if (s->sclass != STATIC)
            v->set_addr_taken();
        else
            v->reset_addr_taken();
        if (s->sclass != EXTERN)
            outf->symtab()->define_var(v, get_alignment(v->type()));
        s->suif_symbol = v;
      }

    /*
     * look up local externs to see if they are already declared at
     * global level;  if not, declare them at global level
     */
    else if (s->scope >= LOCAL && s->sclass == EXTERN)
      {
        var_sym *v = fileset->globals()->lookup_var(s->name);
        if (v == NULL)
          {
            boolean vis = fileset->globals()->make_type_visible(s->type);
            assert(vis);
            v = fileset->globals()->new_var(s->type, s->name);
            v->set_userdef();
	    v->set_addr_taken();
          }
        s->suif_symbol = v;
      }

    /*
     * local statics get a symbol but are not entered in symbol table
     * yet, and get put in locals_without_a_home list until their home
     * block is determined
     */
    else if (s->scope >= LOCAL && s->sclass == STATIC)
      {
        var_sym *v = curr_symtab->new_var(s->type, s->name);
        v->set_userdef();
        curr_symtab->define_var(v, get_alignment(v->type()));
        s->suif_symbol = v;
      }

    // there better not be any other kinds of symbols
    else
        assert(0);
  }



/*
 *  Return a system defined static global variable.
 */
extern var_sym *gen_static_global(type_node *ty)
  {
    char *name = stringf("%s%d", tmp_string_prefix, ++curr_tmp_global);

    boolean vis = outf->symtab()->make_type_visible(ty);
    assert(vis);
    var_sym *the_var = outf->symtab()->new_var(ty, name);
    the_var->reset_userdef();
    the_var->reset_addr_taken();
    outf->symtab()->define_var(the_var, get_alignment(ty));
    return the_var;
  }



/*
 *  Return a system defined static local in the top level symbol table of the
 *  current function.
 */
extern var_sym *gen_static_local(type_node *ty, char *name)
  {
    assert(curr_proc != NULL);

    boolean vis = curr_proc->block()->proc_syms()->make_type_visible(ty);
    assert(vis);
    var_sym *the_var = curr_proc->block()->proc_syms()->new_var(ty, name);
    the_var->reset_userdef();
    the_var->reset_addr_taken();
    curr_proc->block()->proc_syms()->define_var(the_var, get_alignment(ty));
    return the_var;
  }



extern var_def *global(Symbol s)
  {
    /*
     * all this is used for is data initialization, so we only want
     * non-extern globals and local statics
     */
    assert((s->scope == GLOBAL) || (s->scope >= LOCAL && s->sclass == STATIC));


    /*
     * should have already been handled by defsymbol; make sure and
     * set up initialization stuff
     */
    assert(s->suif_symbol != NULL);
    assert(s->suif_symbol->is_var());
    return begin_initialization((var_sym *)(s->suif_symbol));
  }



extern var_def *begin_initialization(var_sym *to_initialize)
  {
    /*
     *  If we have the case of a declaration that uses the ``extern''
     *  keyword, but which also has an initializer, we won't think it
     *  needs a definition in the code that would otherwise define it
     *  because at that point we haven't seen the initialization yet.
     *  So here we need to explicityly check for and add a definition
     *  if necessary.
     */
    if (!to_initialize->has_var_def())
        outf->symtab()->define_var(to_initialize, get_alignment(to_initialize->type()));

    var_def *the_def = to_initialize->definition();
    assert(the_def != NULL);
    the_def->set_alignment(get_alignment(to_initialize->type()));
    return the_def;
  }



/********************************************************************
 * DATA INITIALIZATION FUNCTIONS                                    *
 *    attach initialization annotations to symbol that global() was *
 *      just called on                                              *
 ********************************************************************/

// initialize current global symbol with constant of type ty and value v
void defconst(var_def *the_def, type_node *the_type, immed value)
  {
    if (the_def == NULL)
        return;

    if (value.is_symbol())
      {
        sym_node *symbol = value.symbol();
        assert(symbol != NULL);
        if (symbol->is_var())
          {
            var_sym *the_var = (var_sym *)symbol;
            the_var->set_addr_taken();
          }
      }

    annote_list *current_annotes = the_def->annotes();
    annote_list_e *tail_e = current_annotes->tail();
    if (tail_e != NULL)
      {
        annote *last_annote = tail_e->contents;
        assert(last_annote != NULL);
        if (strcmp(last_annote->name(), k_repeat_init) == 0)
          {
            immed_list *immeds = last_annote->immeds();
            immed_list_iter the_iter(immeds);
            if (!the_iter.is_empty())
              {
                immed count_value = the_iter.step();
                if (count_value.is_int_const() && !the_iter.is_empty())
                  {
                    immed size_value = the_iter.step();
                    if (size_value.is_int_const() && !the_iter.is_empty())
                      {
                        immed old_value = the_iter.step();
                        i_integer count = immed_to_ii(count_value);
                        if ((old_value == value) &&
                            (size_value == immed(the_type->size())) &&
                            the_iter.is_empty())
                          {
                            ++count;
                            immed_list *new_list = new immed_list;
                            new_list->append(ii_to_immed(count));
                            new_list->append(immed(the_type->size()));
                            new_list->append(value);
                            last_annote->set_data(new_list);
                            return;
                          }
                      }
                  }
              }
          }
      }

    annote *a = new annote(k_repeat_init);
    a->immeds()->append(immed(1));

    a->immeds()->append(immed(the_type->size()));
    a->immeds()->append(value);

    the_def->annotes()->append(a);
  }


// initialize current global symbol with string of character values
void defstring(var_def *the_def, int len, char *s, type_node *the_type)
  {
    if (the_def == NULL)
        return;

    int n;
    annote *a = new annote(k_multi_init);
    a->immeds()->append(immed(the_type->size()));
    assert(the_type->unqual()->op() == TYPE_INT);
    base_type *the_base = (base_type *)(the_type->unqual());
    for (n=0; n<len; n++)
      {
        a->immeds()->append(immed(the_base->is_signed() ? (signed char)s[n] :
                                  (unsigned char)s[n]));
      }
    the_def->annotes()->append(a);
  }


// initialize current global symbol with ``size'' bits of empty space
void space(var_def *the_def, int size)
  {
    if (the_def == NULL)
        return;

    annote *a = new annote(k_fill);
    a->immeds()->append(immed(size));
    a->immeds()->append(immed(0));
    the_def->annotes()->append(a);
  }




/********************************************************************
 * MISCELLANOUS OTHER LCC BACK-END FUNCTIONS                        *
 ********************************************************************/


// stabline - file and line number marker
void stabline(Coordinate *coord)
  {
    if (curr_list != NULL)
      {
        instruction *mrk = new in_rrr(io_mrk);
        mrk->annotes()->append(make_line_annote(coord));
        append_instr(mrk);
      }
  }



/********************************************************************
 * ACCESSORY FUNCTIONS FOR CODE GENERATION                          *
 ********************************************************************/

void append_instr(instruction *instr)
  {
    curr_list->append(new tree_instr(instr));
  }







/* New stuff */

extern void current_list_append_node(tree_node *the_node)
  {
    assert(curr_list != NULL);
    curr_list->append(the_node);
  }

extern void current_list_append_list(tree_node_list *the_list)
  {
    assert(curr_list != NULL);
    curr_list->append(the_list);
    delete the_list;
  }

/*
 *  This procedure is called on the suif procedure just before it is written
 *  out.  It acts as a final pass to figure out symbols that have their
 *  addresses taken; replace load-store combinations with memcpy's; change one
 *  step conversions between integers that change both the the size and sign to
 *  two different conversions; and replace a series of array reference
 *  instructions into a single multi-dimensional array reference instruction.
 *  Then it makes all the substitutions specified by global_replacements to
 *  merge types that are really the same in SUIF.
 */
static void finalize_suif_proc(proc_sym *the_proc)
  {
    assert(the_proc != NULL);
    tree_block *the_block = the_proc->block();
    assert(the_block != NULL);
    the_block->map(&finalize_suif_node, NULL);
    (void)(the_block->clone_helper(global_replacements, TRUE));
  }

static void finalize_suif_node(tree_node *the_node, void *)
  {
    assert(the_node != NULL);
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        the_tree_instr->instr_map(&finalize_suif_instr, NULL, FALSE);
      }
  }

static void finalize_suif_instr(instruction *the_instr, void *)
  {
    assert(the_instr != NULL);
    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        immed value = the_ldc->value();
        if (value.is_symbol())
          {
            sym_node *symbol = value.symbol();
            assert(symbol != NULL);
            if (symbol->is_var())
              {
                var_sym *the_var = (var_sym *)symbol;
                the_var->set_addr_taken();
              }
          }
      }
    else if (the_instr->opcode() == io_str)
      {
        in_rrr *the_store = (in_rrr *)the_instr;
        operand data_op = the_store->src2_op();
        if (data_op.is_expr() && (data_op.instr()->opcode() == io_lod))
          {
            in_rrr *the_load = (in_rrr *)(data_op.instr());
            operand source_address = the_load->src_addr_op();
            source_address.remove();
            data_op.remove();
            the_store->set_opcode(io_memcpy);
            the_store->set_src2(source_address);
            delete the_load;
          }
      }
    else if (the_instr->opcode() == io_cvt)
      {
        in_rrr *the_cvt = (in_rrr *)the_instr;
        type_node *old_type = the_cvt->src_op().type()->unqual();
        type_node *new_type = the_cvt->result_type()->unqual();

        /*
         *  If there is a convert from an int or pointer type to an int or
         *  pointer type of a different size and one is signed but the other
         *  isn't (pointers are treated as unsigned), SUIF needs two separate
         *  convert instructions, one to change the size and one to change
         *  between signed and unsigned.  It matters which order this
         *  conversion is done:
         *
         *    - Going from an unsigned to a larger signed, everything
         *      representable in the original type is representable in the new
         *      type, so we just have to make sure it's representable in the
         *      intermediate type.  It is representable in the larger
         *      unsigned, so we can do that conversion first.  The other order
         *      would not work; for example, converting 255 from an unsigned
         *      char to a signed char would give -1, and converting that to a
         *      signed int would give -1, not the correct 255.  So in this
         *      case we must do the size conversion first.
         *
         *    - Going from a signed to a larger unsigned, 6.2.1.2, paragraph 2
         *      of ANSI/ISO 9899-1990 clearly states that the value is first
         *      promoted to the signed type of the larger size.  So we need to
         *      do the size conversion first in this case, too.
         *
         *    - Going from a signed to a smaller unsigned, 6.2.1.2,
         *      paragraph 3, sentence 1 or ANSI/ISO 9899-1990 defines the
         *      result: it must be the remainder on division of the number of
         *      values representable by the target type.  Converting first to
         *      an unsigned type of the larger size and then to the smaller
         *      size guarantees this will happen: it is the remainder on
         *      division by the number of values representable by the smaller
         *      unsigned of the remainder on division by the number of values
         *      representable by the larger unsigned of the original value.
         *      Since both numbers of values are powers of two, the larger is
         *      a multiple of the smaller, so the result is what we needed.
         *      So no matter how conversion to a smaller integer is defined
         *      (the second sentence of paragraph 3 of 6.2.1.2 of
         *      ANSI/ISO 9899-1990 makes it implementation defined), the result
         *      is correct if we first do the sign conversion in this case.
         *
         *    - Going from an unsigned to a smaller signed is a bit more
         *      problematic because the standard leaves this implementation
         *      defined.  Anything representable in both types would also be
         *      representable in the intermediate type either way the
         *      conversion is done.  And most of the time we're in 2's
         *      complement and changing between unsigned and signed leaves the
         *      same bits and changing to a smaller size just throws away the
         *      higher order bits, whether we are seeing it as a signed or
         *      unsigned.  So it really doesn't matter much which way we
         *      define it.
         *      A reasonable assumption to make would be that any conversion
         *      from a type A to a type B followed by a conversion from B to A
         *      always gives back the original value if B has at least as many
         *      bits as B.  Lets assume this is true of the conversion between
         *      a signed and an unsigned with the same number of bits.  Then
         *      we can guarantee this holds all the time if and only if we
         *      choose to do the sign conversion first for the case of an
         *      unsigned to a smaller signed.  So we'll do the sign conversion
         *      first in this case.
         *
         *  The net result is that we want to do the size conversion first
         *  when going to a larger size and the sign conversion first when
         *  going to a smaller size.
         */
        if (((old_type->op() == TYPE_INT) || old_type->is_enum() ||
             old_type->is_ptr()) &&
            ((new_type->op() == TYPE_INT) || new_type->is_enum() ||
             new_type->is_ptr()))
          {
            boolean old_signed = !non_negative(old_type);
            boolean new_signed = !non_negative(new_type);

            if ((new_signed != old_signed) &&
                (new_type->size() != old_type->size()))
              {
                type_node *intermediate_type;
                if (new_type->size() > old_type->size())
                  {
                    intermediate_type =
                            new base_type(TYPE_INT, new_type->size(),
                                          old_signed);
                  }
                else
                  {
                    intermediate_type =
                            new base_type(TYPE_INT, old_type->size(),
                                          new_signed);
                  }
                intermediate_type =
                        fileset->globals()->install_type(intermediate_type);
                operand old_op = the_cvt->src_op();
                old_op.remove();
                instruction *new_instr =
                        new in_rrr(io_cvt, intermediate_type, operand(),
                                   old_op);
                the_cvt->set_src(operand(new_instr));
              }
          }
        /*
         *  Mgen also can't handle direct converts between floating point and
         *  integers of any size other than word size.  So we need to go
         *  through that size first.
         */
        else if (((old_type->op() == TYPE_FLOAT) &&
                  ((new_type->op() == TYPE_INT) || new_type->is_enum()) &&
                  (new_type->size() != type_signed->size())) ||
                 ((new_type->op() == TYPE_FLOAT) &&
                  ((old_type->op() == TYPE_INT) || old_type->is_enum()) &&
                  (old_type->size() != type_signed->size())))
          {
            type_node *intermediate_type;
            if ((old_type->op() == TYPE_INT) || old_type->is_enum())
              {
                if (non_negative(old_type))
                    intermediate_type = type_unsigned;
                else
                    intermediate_type = type_signed;
              }
            else
              {
                if (non_negative(new_type))
                    intermediate_type = type_unsigned;
                else
                    intermediate_type = type_signed;
              }
            operand old_op = the_cvt->src_op();
            old_op.remove();
            instruction *new_instr =
                    new in_rrr(io_cvt, intermediate_type, operand(), old_op);
            the_cvt->set_src(operand(new_instr));
          }
      }
    else if (the_instr->opcode() == io_array)
      {
        in_array *the_array = (in_array *)the_instr;
        operand base_operand = the_array->base_op();
        if (base_operand.is_expr() &&
            (base_operand.instr()->opcode() == io_array) &&
            (base_operand.instr()->peek_annote(k_fields) == NULL))
          {
            /*
             *  Collapse adjacent array references into a single array
             *  reference instruction with multiple dimensions.
             */
            in_array *second_array = (in_array *)(base_operand.instr());
            second_array->remove();
            assert(the_array->dims() == 1);
            unsigned new_num_dims = 1 + second_array->dims();
            the_array->set_dims(new_num_dims);
            assert(second_array->offset_op() == operand());
            assert(second_array->offset() == 0);

            operand last_index = the_array->index(0);
            operand last_bound = the_array->bound(0);
            last_index.remove();
            last_bound.remove();
            the_array->set_index(new_num_dims - 1, last_index);
            the_array->set_bound(new_num_dims - 1, last_bound);

            for (unsigned dim_num = 0; dim_num < new_num_dims - 1; ++dim_num)
              {
                operand this_index = second_array->index(dim_num);
                operand this_bound = second_array->bound(dim_num);
                this_index.remove();
                this_bound.remove();
                the_array->set_index(dim_num, this_index);
                the_array->set_bound(dim_num, this_bound);
              }

            operand second_base = second_array->base_op();
            second_base.remove();
            the_array->set_base_op(second_base);

            delete second_array;
          }
      }
  }

static annote *make_line_annote(Coordinate *location)
  {
    annote *result = new annote(k_line);
    result->immeds()->append(immed((int)location->y));
    result->immeds()->append(immed(location->file ? location->file : ""));
    return result;
  }
