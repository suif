/* file "expr.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot expression parsing */

#include "c.h"

static char prec[] =
  {
#define xx(a,b,c,d,e,f,g) c,
#include "token.h"
  };

static int oper[] =
  {
#undef xx
#define xx(a,b,c,d,e,f,g) d,
#include "token.h"
  };

static boolean get_true_address(genop *result, genop p);
static genop call(genop f, type_node *fty);
static genop expr2(void);
static genop expr3(int k);
static genop nullcheck(genop p);
static genop postfix(genop p);
static genop prefix(void);
static genop primary(void);
static genop value(genop p);

/* addrof - address of p */
extern genop addrof(genop p)
  {
    genop result;
    boolean ok = get_true_address(&result, p);
    if (ok)
      {
        return result;
      }
    else
      {
        error("addressable object required\n");
        return value(p);
      }
  }

static boolean get_true_address(genop *result, genop p)
  {
    switch (p.suif_operand().kind())
      {
        case OPER_SYM:
          {
            sym_node *the_symbol = p.suif_operand().symbol();
            assert(the_symbol != NULL);
            type_node *new_type = p.type()->ptr_to();
            immed value(the_symbol);
            instruction *new_instr = new in_ldc(new_type, operand(), value);
            *result = genop(operand(new_instr), p.precomputation());
            return TRUE;
          }
        case OPER_INSTR:
          {
            instruction *the_instr = p.suif_operand().instr();
            assert(the_instr != NULL);
            if (the_instr->opcode() != io_lod)
                break;

            in_rrr *the_rrr = (in_rrr *)the_instr;
            operand new_op = the_rrr->src_addr_op();
            new_op.remove();
            delete the_rrr;

            *result = genop(new_op, p.precomputation());
            return TRUE;
          }
        default:
            break;
      }

    return FALSE;
  }



/*
 *  See ANSI/ISO 9899-1990, section 6.3.16.1, ``Constraints''.
 */

/* assign - perform type-checking of assignment of p to variable of type xty */
extern type_node *assign(type_node *xty, genop p)
  {
    type_node *yty = p.type()->unqual();

    if (yty->size() == 0)
        return NULL;
    xty = xty->unqual();
    type_node *xty_test = xty;
    if (isenum(xty_test))
        xty_test = base_from_enum(xty_test);

    if (isarith(xty_test) && isarith(yty))
        return xty;
    if (isstruct_or_union(xty_test) && compatible_types(xty_test, yty))
        return xty;
    if (isptr(xty_test) && isptr(yty))
      {
        type_node *lty = base_from_pointer(xty_test);
        type_node *rty = base_from_pointer(yty);
        if ((compatible_types(lty->unqual(), rty->unqual()) ||
             (lty->unqual()->op() == TYPE_VOID) ||
             (rty->unqual()->op() == TYPE_VOID)) &&
            (!isconst(rty) || isconst(lty)) &&
            (!isvolatile(rty) || isvolatile(lty)))
          {
            return xty;
          }

        if ((isint(lty->unqual()) || isptr(lty->unqual())) &&
            (isint(rty->unqual()) || isptr(rty->unqual())) &&
            (rty->size() == lty->size()))
          {
            warning("non-ANSI implicit conversion from `%t' to `%t'\n", yty,
                    xty);
            return xty;
          }
      }
    if (isptr(xty_test) && p.is_null_pointer_constant())
        return xty;
    return NULL;
  }

/* call - parse function call to f, type fty */
static genop call(genop f, type_node *fty)
  {
    assert(fty != NULL);
    type_node *ftype_unqual = fty->unqual();
    assert(ftype_unqual != NULL);
    if (ftype_unqual->op() != TYPE_FUNC)
        error("type error: function expected\n");
    func_type *the_func_type = (func_type *)ftype_unqual;

    int num_args;
    if (the_func_type->args_known())
        num_args = the_func_type->num_args();
    else
        num_args = 0;
    if (option_mark_execution_points)
        f.make_temporary();
    in_cal *the_call = new in_cal(the_func_type->return_type(), operand(),
                                  f.suif_operand(), num_args);
    tree_node_list *precomputation = f.precomputation();

    unsigned arg_num = 0;

    if (t != ')')
      {
        do
          {
            genop q = pointer(expr1(0));
            if (the_func_type->args_known() &&
                (the_func_type->num_args() > arg_num))
              {
                q = value(q);
                type_node *arg_type =
                        assign(the_func_type->arg_type(arg_num), q);
                if (arg_type != NULL)
                  {
                    q = cast(q, arg_type);
                  }
                else
                  {
                    error("type error in argument %u to %s; found `%t' "
                          "expected `%t'\n", arg_num + 1, funcname(f),
                          q.type(), the_func_type->arg_type(arg_num));
                  }
              }
            else
              {
                if (the_func_type->args_known() &&
                    !the_func_type->has_varargs() &&
                    (the_func_type->num_args() == arg_num))
                  {
                    error("too many arguments to %s\n", funcname(f));
                  }
                q = value(q);
                if (q.type() == floattype)
                  {
                    q = cast(q, doubletype);
                  }
                else if ((isint(q.type()) || isenum(q.type())) &&
                         (q.type()->size() != inttype->size()))
                  {
                    q = cast(q, promote(q.type()));
                  }
                else if (isarray(q.type()) || (q.type()->size() == 0))
                  {
                    error("type error in argument %u to %s; `%t' is illegal\n",
                          arg_num + 1, funcname(f), q.type());
                  }
              }
            if (arg_num >= the_call->num_args())
                the_call->set_num_args(arg_num + 1);
            if (option_mark_execution_points)
                q.make_temporary();
            the_call->set_argument(arg_num, q.suif_operand());
            precomputation->append(q.precomputation());
            delete q.precomputation();
            arg_num++;
            if ((Aflag >= 2) && (arg_num == 32))
              {
                warning("more than 31 arguments in a call to %s\n",
                        funcname(f));
              }
          } while ((t == ',') && ((t = gettok()) != 0));
      }
    expect(')');
    if (the_func_type->args_known() && (!the_func_type->has_varargs()) &&
        (the_func_type->num_args() > arg_num))
      {
        error("insufficient number of arguments to %s\n", funcname(f));
      }
#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
    genop result(operand(), precomputation);
#else
//
//  gcc version 2.6.3 gets a parse error on the code above.  The code
//  looks like it should be perfectly legal, and both gcc 2.5.8 and
//  the IRIX 5.3 C++ compiler have no problem with it, so I'm inclined
//  to think the problem is caused by brain damage in the g++
//  front-end.  A work-around follows.
//
    operand op1;
    genop result(op1, precomputation);
#endif
    result = sequence_point(result);
    return genop(operand(the_call), result.precomputation());
  }

/* cast - cast expression p to type */
genop cast(genop p, type_node *type)
  {
    type_node *pty = p.type()->unqual();
    type_node *ty = type->unqual();

    genop result = value(p);

    if (isptr(pty) && isptr(ty))
      {
        if (isfunc(base_from_pointer(pty)) != isfunc(base_from_pointer(ty)))
          {
            warning("conversion from `%t' to `%t' is compiler-dependent\n",
                    result.type(), ty);
          }
      }
    if (isptr(ty) && isint(pty) && (pty->size() < ty->size()) &&
        (!p.is_int_const()))
      {
        warning("conversion to pointer from a smaller integer type\n");
      }
    return simplify_convert(ty, result);
  }

/* cond - check for conditional operator, add comparison if necessary */
genop cond(genop p)
  {
    if (p.is_boolean_type())
        return p;

    p = pointer(p);
    p = cast(p, promote(p.type()));
    return (*opnode[NEQ])(NE, p, constnode(0, inttype));
  }

/* conditional - parse expression and cast to conditional */
genop conditional(int tok)
  {
    genop p = expr(tok);

    if ((Aflag > 1) && isfunc(p.type()))
        warning("%s used in a conditional expression\n", funcname(p));
    return cond(p);
  }

/* constexpr - parse a constant expression */
genop constexpr(int tok)
  {
    boolean old_needconst = needconst;

    needconst = TRUE;
    genop p = expr1(tok);
    needconst = old_needconst;
    return p;
  }

/* expr0 - parse an expression for side effect */
tree_node_list *expr0(int tok)
  {
    genop the_expression = expr(tok);
    the_expression.clean_up_bit_field_refs();
    tree_node_list *side_effects = the_expression.side_effects_only();
    remove_unused_temps(side_effects);
    return side_effects;
  }

/* expr - parse an expression */
genop expr(int tok)
  {
    genop p = expr1(0);

    while (t == ',')
      {
        t = gettok();
        p = sequence_point(p);
        genop q = pointer(expr1(0));
        tree_node_list *side_effects = p.side_effects_only();
        remove_unused_temps(side_effects);
        side_effects->append(q.precomputation());
        delete q.precomputation();
        p = genop(q.suif_operand(), side_effects);
      }
    if ((tok != 0) && (t == tok))
      {
        t = gettok();
      }
    else if (tok != 0)
      {
        static char follow[] = { IF, ID, '}', 0 };
        test(tok, follow);
      }
    return p;
  }

/* expr1 - parse assignments */
genop expr1(int tok)
  {
    genop p = expr2();

    while ((t == '=') || ((prec[t] >= 6) && (prec[t] <= 8)) ||
           ((prec[t] >= 11) && (prec[t] <= 13)))
      {
        tokencode op = t;
        t = gettok();
        if (oper[op] == ASGN)
          {
            p = asgnnode(ASGN, p, value(expr1(0)));
          }
        else
          {
            expect('=');
            p = incr(op, p, expr1(0));
          }
      }
    if ((tok != 0) && (t == tok))
      {
        t = gettok();
      }
    else if (tok != 0)
      {
        static char follow[] = { IF, ID, 0 };
        test(tok, follow);
      }
    return p;
  }

/* expr2 - parse conditional expressions */
static genop expr2(void)
  {
    genop p = expr3(4);

    if (t == '?')
      {
        if ((Aflag > 1) && isfunc(p.type()))
            warning("%s used in a conditional expression\n", funcname(p));
        p = pointer(p);
        p = sequence_point(p);
        t = gettok();
        genop l = pointer(expr(':'));
        genop r = pointer(expr2());
        p = condnode(p, l, r);
      }
    return p;
  }

/* expr3 - parse expressions at precedence level k */
static genop expr3(int k)
  {
    int k1;
    genop p = prefix();

    for (k1 = prec[t]; k1 >= k; k1--)
      {
        while ((prec[t] == k1) && (*cp != '='))
          {
            tokencode op = t;
            t = gettok();
            p = pointer(p);
            if ((op == ANDAND) || (op == OROR))
                p = sequence_point(p);
            genop r = pointer(expr3(k1 + (k1 > 5)));
            p = (*opnode[op])(oper[op], p, r);
          }
      }
    return p;
  }

/* field - construct tree for reference to field name via p */
genop field(genop p, char *name)
  {
    type_node *ty = p.type();

    if (isptr(ty))
        ty = deref(ty);
    type_node *ty1 = ty;
    ty = ty->unqual();
    Field q = fieldref(name, ty);
    if (q != NULL)
      {
        ty = q->block_type;
        if (isconst(ty1) && !isconst(ty))
            ty = qual(CONST, ty);
        if (isvolatile(ty1) && !isvolatile(ty))
            ty = qual(VOLATILE, ty);
        ty = ty->ptr_to();

        boolean folded = FALSE;
        instruction *the_instr;
        if (p.suif_operand().is_expr())
          {
            the_instr = p.suif_operand().instr();
            assert(the_instr != NULL);
            if (the_instr->opcode() == io_ldc)
              {
                in_ldc *the_ldc = (in_ldc *)the_instr;
                immed value = the_ldc->value();
                if (value.is_symbol())
                  {
                    sym_node *the_symbol = value.symbol();
                    int offset = value.offset();
                    assert((offset >= 0) && (q->offset >= 0));
                    offset = offset + q->offset;
                    if (offset >= 0)
                      {
                        folded = TRUE;
                        the_ldc->set_value(immed(the_symbol, offset));
                        the_ldc->set_result_type(ty);
                      }
                    else
                      {
                        warning("overflow in cumulative field offset "
                                "constant\n");
                      }
                  }
              }
            else if (the_instr->opcode() == io_array)
              {
                in_array *the_array = (in_array *)the_instr;
                unsigned old_offset = the_array->offset();
                assert(q->offset >= 0);
                unsigned new_offset = old_offset + q->offset;
                if (new_offset >= old_offset)
                  {
                    folded = TRUE;
                    the_array->set_offset(new_offset);
                    the_array->set_result_type(ty);
                  }
                else
                  {
                    warning("overflow in cumulative field offset constant\n");
                  }
              }
            else if (the_instr->opcode() == io_add)
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                void *field_annote = the_rrr->peek_annote(k_fields);
                if (field_annote != NULL)
                  {
                    operand source2 = the_rrr->src2_op();
                    if (source2.is_expr())
                      {
                        instruction *instr2 = source2.instr();
                        assert(instr2 != NULL);
                        if (instr2->opcode() == io_ldc)
                          {
                            in_ldc *ldc2 = (in_ldc *)instr2;
                            immed value = ldc2->value();
                            if (value.is_int_const())
                              {
                                i_integer offset = immed_to_ii(value);
                                assert((offset >= 0) && (q->offset >= 0));
                                assert((q->offset % target.addressable_size) ==
                                       0);
                                offset += q->offset / target.addressable_size;
                                folded = TRUE;
                                ldc2->set_value(ii_to_immed(offset));
                                the_instr->set_result_type(ty);
                              }
                          }
                      }
                  }
              }
          }

        if (!folded)
          {
            assert(q->offset % target.addressable_size == 0);
            operand offset_operand =
                    operand(new in_ldc(type_ptr_diff, operand(),
                                       immed(q->offset /
                                             target.addressable_size)));

            the_instr = new in_rrr(io_add, ty, operand(), p.suif_operand(),
                                   offset_operand);
            p = genop(operand(the_instr), p.precomputation());
          }

        immed_list *new_annote =
                (immed_list *)(the_instr->get_annote(k_fields));
        if (new_annote == NULL)
            new_annote = new immed_list;
        new_annote->append(immed(q->block_name));
        the_instr->append_annote(k_fields, new_annote);

        if (q->to != 0)
            p.make_bit_field_ref(q->from, q->to, q->type);
        else
            p = rvalue(p);
      }
    else
      {
        error("unknown field `%s' of `%t'\n", name, ty);
        p = rvalue(cast(p, inttype->ptr_to()));
      }
    return p;
  }

/* funcname - return name of function f or `a function' */
char *funcname(genop f)
  {
    if (f.suif_operand().is_expr())
      {
        instruction *the_instr = f.suif_operand().instr();
        assert(the_instr != NULL);
        if (the_instr->opcode() == io_ldc)
          {
            in_ldc *the_ldc = (in_ldc *)the_instr;
            immed value = the_ldc->value();
            if (value.is_symbol())
              {
                sym_node *the_symbol = value.symbol();
                assert(the_symbol != NULL);
                return stringf("`%s'", the_symbol->name());
              }
          }
      }
    return "a function";
  }

/* idnode - construct tree for reference to r-value of identifier p */
genop idnode(Symbol p)
  {
    p->ref += refinc;
    p->initialized = 1;    /* in case p->ref overflows */
    assert(p->suif_symbol != NULL);
    return sym_node_to_genop(p->suif_symbol);
  }

extern genop sym_node_to_genop(sym_node *the_symbol)
  {
    type_node *ty;
    if (the_symbol->is_var())
      {
        var_sym *the_var = (var_sym *)the_symbol;
        ty = the_var->type()->unqual();
        return genop(operand(the_var), new tree_node_list);
      }
    else
      {
        assert(the_symbol->is_proc());
        proc_sym *the_proc = (proc_sym *)the_symbol;
        ty = the_proc->type()->unqual();
      }

    genop result =
            genop(operand(new in_ldc(ty->ptr_to(), operand(),
                                     immed(the_symbol))), new tree_node_list);
    if (the_symbol->is_proc())
        result = cast(result, ty);
    return result;
  }

/* incr - construct tree for e1 op= e2 */
genop incr(tokencode op, genop e1, genop e2)
  {
    genop address;
    genop op1;
    if (e1.is_bit_field_ref())
      {
        e1.double_bit_field_ref(&op1, &address);
      }
    else
      {
        genop lval = lvalue(e1);
        lval.make_temporary();
        address =
                rvalue(genop(lval.suif_operand().clone(), new tree_node_list));
        op1 = rvalue(lval);
      }
    return asgnnode(ASGN, address, (*opnode[op])(oper[op], op1, e2));
  }

/* intexpr - parse a constant expression and return int value, default n */
i_integer intexpr(int tok, int n)
  {
    genop p = constexpr(tok);

    immed the_constant;
    eval_status status = p.evaluate_as_const(&the_constant);
    p.deallocate();

    switch (status)
      {
        case EVAL_OVERFLOW:
            warning("overflow in integer constant expression\n");
            /* fall through */
        case EVAL_OK:
            if (the_constant.is_int_const())
                return immed_to_ii(the_constant);
            else
                error("integer expression must be constant\n");
            break;
        case EVAL_NOT_CONST:
            error("integer expression must be constant\n");
            break;
        case EVAL_DIV_BY_ZERO:
            error("division by zero in integer constant\n");
            break;
        case EVAL_UNKNOWN_AT_LINK:
            error("constant expression cannot be computed at link time\n");
            break;
        default:
            assert(FALSE);
      }
    return n;
  }

/* lvalue - check for lvalue, return pointer to lvalue tree */
genop lvalue(genop p)
  {
    if (p.suif_operand().is_symbol())
      {
        sym_node *the_symbol = p.suif_operand().symbol();
        if (the_symbol->is_userdef())
          {
            type_node *the_type = p.type();
            if (the_type->op() == TYPE_VOID)
                warning("`%t' used as an lvalue\n", the_type);
            the_type = the_type->ptr_to();
            return genop(operand(new in_ldc(the_type, operand(),
                                            immed(the_symbol))),
                         p.precomputation());
          }
      }
    else if (p.suif_operand().is_expr())
      {
        instruction *the_instr = p.suif_operand().instr();
        assert(the_instr != NULL);
        if (the_instr->opcode() == io_lod)
          {
            in_rrr *the_lod = (in_rrr *)the_instr;
            operand address = the_lod->src_addr_op();
            if (address.is_expr() && (address.instr()->opcode() == io_ldc))
              {
                in_ldc *the_ldc = (in_ldc *)address.instr();
                if (the_ldc->value().is_symbol() &&
                    !the_ldc->value().symbol()->is_userdef())
                  {
                    error("lvalue required\n");
                    return value(p);
                  }
              }
            if (address.type()->op() == TYPE_VOID)
                warning("`%t' used as an lvalue\n", p.type());
            address.remove();
            delete the_lod;
            return genop(address, p.precomputation());
          }
        else if (the_instr->opcode() == io_gen)
          {
            /*
             *  In this case we have a builtin function or variable
             *  that we know nothing about.  So assume it's ok to take
             *  the address and just pass that through the SUIF code.
             */
            in_gen *addr_instr =
                    new in_gen("&", the_instr->result_type()->ptr_to(),
                               operand(), 1);
            addr_instr->set_src_op(0, operand(the_instr));
            return genop(operand(addr_instr), p.precomputation());
          }
      }
    error("lvalue required\n");
    return value(p);
  }

/* nullcheck - test if p null; build ((!p ? _YYnull(lineno) : ), p) */
static genop nullcheck(genop p)
  {
    if (needconst)
        return p;

    YYnull_used = TRUE;

    p.make_temporary();
    operand result = p.suif_operand().clone();
    genop test = (*opnode[EQL])(EQ, p, constnode(0, voidptype));

    operand proc_address(new in_ldc(YYnull_proc->type()->ptr_to(), operand(),
                                    immed(YYnull_proc)));
    in_cal *the_call = new in_cal(voidtype, operand(), proc_address, 1);
    in_ldc *argument = new in_ldc(inttype, operand(), immed(lineno));
    the_call->set_argument(0, operand(argument));
#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
    genop if_part(operand(the_call), new tree_node_list);

    genop else_part(operand(), new tree_node_list);
#else
//
//  gcc version 2.6.3 gets a parse error on the code above.  The code
//  looks like it should be perfectly legal, and both gcc 2.5.8 and
//  the IRIX 5.3 C++ compiler have no problem with it, so I'm inclined
//  to think the problem is caused by brain damage in the g++
//  front-end.  A work-around follows.
//
    operand op1(the_call);
    genop if_part(op1, new tree_node_list);

    operand op2;
    genop else_part(op2, new tree_node_list);
#endif
    genop conditional = condnode(test, if_part, else_part);
    return genop(result, conditional.side_effects_only());
  }

/* pointer - re-type `array of T', `T function' to `ptr to T', `ptr to T
   function', resp. */
genop pointer(genop p)
  {
    if (isarray(p.type()))
      {
        type_node *original_type = p.type();
        return cast(addrof(p), atop(original_type));
      }
    else if (isfunc(p.type()))
      {
        assert(p.suif_operand().is_expr() &&
               (p.suif_operand().instr()->opcode() == io_cvt));
        in_rrr *the_cvt = (in_rrr *)(p.suif_operand().instr());
        operand original_operand = the_cvt->src_op();
        assert(original_operand.type()->unqual()->op() == TYPE_PTR);
        ptr_type *the_pointer =
                (ptr_type *)(original_operand.type()->unqual());
        assert(the_pointer->ref_type() == p.type());
        original_operand.remove();
        delete the_cvt;
        return genop(original_operand, p.precomputation());
      }
    else
      {
        return p;
      }
  }

/* postfix - parse a postfix expresssion; p is the primary dag */
static genop postfix(genop p)
  {
    type_node *ty = NULL;

    for (;;)
      {
        switch (t)
          {
            case INCR:
            case DECR:
              {
                tree_node_list *precomp;
                genop address;
                if (p.is_bit_field_ref())
                  {
                    genop op1;
                    p.double_bit_field_ref(&op1, &address);
                    precomp = op1.precomputation();
                    p = genop(op1.suif_operand(), new tree_node_list);
                  }
                else
                  {
                    p = lvalue(p);
                    p.make_temporary();
                    precomp = p.precomputation();
                    address = genop(p.suif_operand().clone(),
                                    new tree_node_list);
                    p = genop(p.suif_operand(), new tree_node_list);
                    address = rvalue(address);
                    p = rvalue(p);
                  }
                p.make_temporary();
                operand result = p.suif_operand().clone();
                p = asgnnode(ASGN, address,
                             (*opnode[t])(oper[t], p, constnode(1, inttype)));
                precomp->append(p.side_effects_only());
                p = genop(result, precomp);

                t = gettok();
                continue;
              }
            case '[':
              {
                t = gettok();
                genop q = expr(']');
                if ((!isptr(p.type())) && (!isarray(p.type())) &&
                    (isptr(q.type()) || isarray(q.type())))
                  {
                    genop temp = p;
                    p = q;
                    q = temp;
                  }

                if (option_null_check)
                  {
                    if (isptr(p.type()))
                        p = nullcheck(p);
                  }
                if (isptr(p.type()))
                  {
                    type_node *new_base = base_from_pointer(p.type());
                    p = cast(p, make_array(new_base)->ptr_to());
                  }
                else if (isarray(p.type()))
                  {
                    type_node *new_type = p.type()->ptr_to();
                    p = cast(addrof(p), new_type);
                  }
                else
                  {
                    error("array subscript on non-pointer\n");
                    p = cast(p, make_array(chartype)->ptr_to());
                  }

                type_node *p_type = p.type()->unqual();
                assert(p_type->is_ptr());
                ptr_type *the_pointer = (ptr_type *)p_type;
                type_node *base_type = the_pointer->ref_type()->unqual();
                assert(base_type->is_array());
                array_type *the_array_type = (array_type *)base_type;

                if (!isarith(q.type()))
                    error("array subscript is not of integral type\n");
                q = cast(q, type_ptr_diff);
                p.precomputation()->append(q.precomputation());
                delete q.precomputation();
                in_array *the_array =
                        new in_array(the_array_type->elem_type()->ptr_to(),
                                     operand(), p.suif_operand(),
                                     the_array_type->elem_type()->size(),
                                     1);
                the_array->set_index(0, q.suif_operand());
                if (the_array_type->upper_bound().is_constant())
                  {
                    int bound_int = the_array_type->upper_bound().constant();
                    in_ldc *new_ldc = new in_ldc(type_ptr_diff, operand(),
                                                 immed(bound_int + 1));
                    the_array->set_bound(0, operand(new_ldc));
                  }
                p = genop(operand(the_array), p.precomputation());
                p = rvalue(p);
                continue;
              }
            case '(':
              {
                Coordinate pt;
                p = pointer(p);
                if (isptr(p.type()) && isfunc(base_from_pointer(p.type())))
                  {
                    ty = base_from_pointer(p.type());
                  }
                else
                  {
                    error("found `%t' expected a function\n", p.type());
                    ty = func(voidtype);
                  }
                pt = src;
                t = gettok();
                p = call(p, ty);
                continue;
              }
            case '.':
                t = gettok();
                if (t == ID)
                  {
                    if (isstruct_or_union(p.type()))
                      {
                        genop q;
                        boolean ok = get_true_address(&q, p);
                        if (!ok)
                          {
                            /*
                             * We have to be able to handle things such as
                             * f().x where it would be illegal for the user
                             * to take an address or assign but an address
                             * is needed for reading a structure member.
                             */
                            p.make_temporary();
                            ok = get_true_address(&q, p);
                            if (!ok)
                              {
                                /*
                                 *  In this case we have some constant or
                                 *  symbol's address cast as a whole
                                 *  structure or union, which is illegal
                                 *  anyway and we have no idea how to access
                                 *  a field of the thing.
                                 */
                                 error("addressable object required\n");
                                 q = value(p);
                              }
                          }
                        p = field(q, token);
                      }
                    else
                      {
                        error("left operand of . has incompatible type `%t'\n",
                              p.type());
                      }
                    t = gettok();
                  }
                else
                  {
                    error("field name expected\n");
                  }
                continue;
            case DEREF:
                t = gettok();
                p = pointer(p);
                if (t == ID)
                  {
                    if (isptr(p.type()))
                        ty = deref(p.type());
                    if (isptr(p.type()) && isstruct_or_union(ty))
                      {
                        if (option_null_check)
                            p = nullcheck(p);
                        p = field(p, token);
                      }
                    else
                      {
                        error("left operand of -> has incompatible "
                              "type `%t'\n", p.type());
                      }
                    t = gettok();
                  }
                else
                  {
                    error("field name expected\n");
                  }
                continue;
            default:
                return p;
          }
      }
  }

/* prefix - parse a prefix expression */
static genop prefix(void)
  {
    switch (t)
      {
        case '*':
          {
            t = gettok();
            genop p = pointer(prefix());
            if (isptr(p.type()) &&
                isfunc(base_from_pointer(p.type())))
              {
                p = cast(p, base_from_pointer(p.type()));
                return p;
              }
            if (option_null_check)
                p = nullcheck(p);
            p = rvalue(p);
            return p;
          }
        case '&':
          {
            t = gettok();
            genop p = prefix();
            if (isfunc(p.type()))
                p = pointer(p);
            else
                p = lvalue(p);
            if (p.suif_operand().is_expr())
              {
                instruction *the_instr = p.suif_operand().instr();
                assert(the_instr != NULL);
                if (the_instr->opcode() == io_ldc)
                  {
                    in_ldc *the_ldc = (in_ldc *)the_instr;
                    immed value = the_ldc->value();
                    if (value.is_symbol())
                      {
                        sym_node *the_symbol = value.symbol();
                        assert(the_symbol != NULL);
                        if (the_symbol->is_var())
                          {
                            var_sym *the_var = (var_sym *)the_symbol;
                            if (the_var->peek_annote(k_is_declared_reg) !=
                                NULL)
                              {
                                error("invalid operand of unary &; `%s' is "
                                      "declared register\n", the_var->name());
                              }
                            else
                              {
                                the_var->set_addr_taken();
                              }
                          }
                      }
                  }
              }
            return p;
          }
        case '+':
          {
            t = gettok();
            genop p = pointer(prefix());
            if (isarith(p.type()))
                p = cast(p, promote(p.type()));
            else
                typeerror(ADD, p, p);
            return p;
          }
        case '-':
          {
            t = gettok();
            genop p = pointer(prefix());
            if (isarith(p.type()))
              {
                p = cast(p, promote(p.type()));
                type_node *original_type = p.type();
                if (isunsigned(original_type))
                  {
                    warning("unsigned operand of unary -\n");
                    type_node *temp_type;
                    if (original_type->size() > longtype->size())
                        temp_type = longlong;
                    else if (original_type->size() > inttype->size())
                        temp_type = longtype;
                    else
                        temp_type = inttype;
                    p = cast(p, temp_type);
                  }
                in_rrr *new_neg = new in_rrr(io_neg, p.type(), operand(),
                                             p.suif_operand());
                p = genop(operand(new_neg), p.precomputation());
                if (isunsigned(original_type))
                    p = cast(p, original_type);
              }
            else
              {
                typeerror(SUB, p, p);
              }
            return p;
          }
        case '~':
          {
            t = gettok();
            genop p = pointer(prefix());
            if (isint(p.type()))
              {
                type_node *result_type = promote(p.type());
                p = cast(p, result_type);
                type_node *unsigned_type = unsigned_int_version(result_type);
                p = cast(p, unsigned_type);
                in_rrr *the_rrr =
                        new in_rrr(io_not, unsigned_type, operand(),
                                   p.suif_operand());
                p = genop(operand(the_rrr), p.precomputation());
                p = cast(p, result_type);
              }
            else
              {
                typeerror(BCOM, p, p);
              }
            return p;
          }
        case '!':
          {
            t = gettok();
            genop p = pointer(prefix());
            if (isscalar(p.type()))
              {
                p = cond(p);
                genop the_const = constnode(0, p.type());
                delete the_const.precomputation();
                in_rrr *the_rrr =
                        new in_rrr(io_seq, booleantype, operand(),
                                   p.suif_operand(), the_const.suif_operand());
                p = genop(operand(the_rrr), p.precomputation());
              }
            else
              {
                typeerror(NOT, p, p);
              }
            return p;
          }
        case INCR:
        case DECR:
          {
            tokencode op = t;
            t = gettok();
            return incr(op, pointer(prefix()), constnode(1, inttype));
          }
        case SIZEOF:
          {
            type_node *ty;
            t = gettok();
            if (t == '(')
              {
                t = gettok();
                if ((kind[t] == CHAR) ||
                    ((t == ID) && (tsym != NULL) &&
                     (tsym->sclass == TYPEDEF)))
                  {
                    ty = get_typename();
                    expect(')');
                  }
                else if (t == SCON)
                  {
                    ty = tsym->type;
                    t = gettok();
                    expect(')');
                  }
                else
                  {
                    genop p = postfix(expr(')'));
                    ty = p.type();
                    if (p.is_bit_field_ref())
                        error("`sizeof' applied to a bit field\n");
                    p.deallocate();
                  }
              }
            else if (t == SCON)
              {
                ty = tsym->type;
                t = gettok();
              }
            else
              {
                genop p = prefix();
                ty = p.type();
                if (p.is_bit_field_ref())
                    error("`sizeof' applied to a bit field\n");
                p.deallocate();
              }
            if (isfunc(ty) || (ty->size() == 0))
                error("invalid type argument `%t' to `sizeof'\n", ty);
            return constnode(ty->size() / target.addressable_size,
                             size_t_type);
          }
        case '(':
            t = gettok();
            if ((kind[t] == CHAR) ||
                ((t == ID) && (tsym != NULL) && (tsym->sclass == TYPEDEF)))
              {
                type_node *ty1 = get_typename();
                type_node *ty = ty1->unqual();
                if (isenum(ty))
                  {
                    type_node *ty2 = base_from_enum(ty);
                    if (isconst(ty1))
                        ty2 = qual(CONST, ty2);
                    if (isvolatile(ty1))
                        ty2 = qual(VOLATILE, ty2);
                    ty1 = ty2;
                    ty = base_from_enum(ty);
                  }
                expect(')');
                genop p = pointer(prefix());
                if (((isarith(p.type()) || isenum(p.type())) && isarith(ty)) ||
                    (isptr(ty) && (isptr(p.type()) || isint(p.type()))))
                  {
                    p = cast(p, ty);
                  }
                else if (isint(ty) && isptr(p.type()))
                  {
                    if ((Aflag >= 1) && (ty->size() < p.type()->size()))
                      {
                        warning("conversion from `%t' to `%t' is "
                                "compiler-dependent\n", p.type(), ty);
                      }
                    p = cast(p, ty);
                  }
                else if (ty->unqual()->op() == TYPE_VOID)
                  {
                    p = cast(p, ty);
                  }
                else
                  {
                    error("cast from `%t' to `%t' is illegal\n",
                        p.type(), ty1);
                  }
                return p;
              }
            else
              {
                return postfix(expr(')'));
              }
        default:
            return postfix(primary());
      }
  }

/* primary - parse a primary expression */
static genop primary(void)
  {

    switch (t)
      {
        case ID:
            if (is_builtin(token))
              {
                /* handle ``built-in'' identifiers */
                t = gettok();
                in_gen *the_gen = new in_gen(token, type_void, operand(), 0);
                tree_node_list *precomputation = new tree_node_list;
                if (t == '(')
                  {
                    t = gettok();
                    if (t != ')')
                      {
                        unsigned num_args = 0;
                        immed_list *builtin_immeds = new immed_list;
                        do
                          {
                            type_node *this_type = NULL;
                            if ((kind[t] == CHAR) ||
                                ((t == ID) && (tsym != NULL) &&
                                 (tsym->sclass == TYPEDEF)))
                              {
                                this_type = get_typename();
                              }
                            else if (t == SCON)
                              {
                                builtin_immeds->append(immed(tsym->u.c.v.p));
                                t = gettok();
                              }
                            else
                              {
                                genop arg_op = pointer(expr1(0));
                                the_gen->set_num_srcs(num_args + 1);
                                the_gen->set_src_op(num_args,
                                                    arg_op.suif_operand());
                                precomputation->append(
                                        arg_op.precomputation());
                                delete arg_op.precomputation();
                                builtin_immeds->append(immed(num_args));
                                ++num_args;
                              }

                            if (this_type != NULL)
                                builtin_immeds->append(immed(this_type));
                          } while ((t == ',') && ((t = gettok()) != 0));

                        the_gen->append_annote(k_builtin_args, builtin_immeds);
                      }
                    expect(')');
                  }
                type_builtin(the_gen);
                return genop(operand(the_gen), precomputation);
              }

            if (tsym == NULL)
              {
                Symbol q = install(token, &identifiers, level < LOCAL);
                q->src = src;
                t = gettok();
                if (t == '(')
                  {
                    q->sclass = EXTERN;
                    q->type = func(inttype);
                    if (Aflag >= 1)
                        warning("missing prototype\n");
                    defsymbol(q);
                    Symbol r = lookup(q->name, externals);
                    if (r != NULL)
                      {
                        q->defined = r->defined;
                        q->temporary = r->temporary;
                        q->computed = r->computed;
                        q->initialized = r->initialized;
                      }
                    else
                      {
                        r = install(q->name, &externals, 1);
                        r->src = q->src;
                        r->type = q->type;
                        r->sclass = EXTERN;
                      }
                  }
                else
                  {
                    error("undeclared identifier `%s'\n", q->name);
                    q->sclass = AUTO;
                    q->type = inttype;
                    q->suif_symbol =
                            get_current_symtab()->new_var(q->type, q->name);
                    q->suif_symbol->set_userdef();
                  }
                if (xref)
                    use(q, src);
                return idnode(q);
              }
            if (xref)
                use(tsym, src);
            if (tsym->sclass == ENUM)
              {
                t = gettok();
                return cast(constnode(tsym->u.enum_value, tsym->type),
                            inttype);
              }
            else
              {
                genop result;
                if (tsym->sclass == TYPEDEF)
                  {
                    error("illegal use of type name `%s'\n", tsym->name);
                    result =
                            genop(operand(new in_ldc(tsym->type, operand(),
                                                     immed(0))),
                                  new tree_node_list);
                  }
                else
                  {
                    result = idnode(tsym);
                  }
                t = gettok();
                return result;
              }
        case ICON:
          {
            immed the_immed;
            type_node *result_type;
            if (*token == '\'')
              {
                the_immed = immed(tsym->u.c.v.i);
                result_type = tsym->type;
              }
            else
              {
                the_immed = immed_and_type_for_C_intconst(token, &result_type);
              }
            t = gettok();
            return genop(operand(new in_ldc(result_type, operand(),
                                            the_immed)),
                         new tree_node_list);
          }
        case FCON:
          {
            type_node *result_type;
            immed the_immed =
                    immed_and_type_for_C_floatconst(token, &result_type);
            t = gettok();
            return genop(operand(new in_ldc(result_type, operand(),
                                            the_immed)),
                         new tree_node_list);
          }
        case SCON:
          {
            tsym->u.c.v.p = stringn(tsym->u.c.v.p, tsym->type->size());
            tsym = symbol_for_string_const(tsym->type, tsym->u.c.v.p);
            if (tsym->u.c.loc == NULL)
              {
                tsym->u.c.loc = gen_static_global(tsym->type);
                var_def *the_def = begin_initialization(tsym->u.c.loc);
                defstring(the_def,
                          tsym->type->size() / target.size[C_char],
                          tsym->u.c.v.p, base_from_array(tsym->type));
              }

            genop result = sym_node_to_genop(tsym->u.c.loc);
            t = gettok();
            return result;
          }
        case '(':
            t = gettok();
            return expr(')');
        default:
            error("illegal expression\n");
            t = gettok();
            return constnode(0, inttype);
      }
  }

/* promote - the usual integral promotions */
type_node *promote(type_node *ty)
  {
    ty = ty->unqual();
    if ((ty == chartype) || (ty == shorttype) || (ty == signedchar) ||
        (ty == unsignedchar) || (ty == unsignedshort) || isenum(ty))
      {
        if (ty->size() < inttype->size())
            return inttype;
        if (ty->size() > inttype->size())
            return unsignedtype;
        if (isenum(ty))
            ty = base_from_enum(ty);
        if (non_negative(ty))
            return unsignedtype;
        else
            return inttype;
      }
    return ty;
  }

/* rvalue - convert p to an rvalue */
genop rvalue(genop p)
  {
    type_node *ty = deref(p.type());

    if (p.suif_operand().is_expr())
      {
        instruction *the_instr = p.suif_operand().instr();
        assert(the_instr != NULL);
        if (the_instr->opcode() == io_ldc)
          {
            in_ldc *the_ldc = (in_ldc *)the_instr;
            immed value = the_ldc->value();
            if (value.is_symbol())
              {
                sym_node *the_symbol = value.symbol();
                assert(the_symbol != NULL);
                if (the_symbol->is_var() && (value.offset() == 0))
                  {
                    var_sym *the_var = (var_sym *)the_symbol;
                    if (the_var->type() == ty)
                      {
                        delete the_instr;
                        return genop(operand(the_var), p.precomputation());
                      }
                  }
              }
          }
      }

    ty = ty->unqual();
    return genop(operand(new in_rrr(io_lod, ty, operand(), p.suif_operand())),
                 p.precomputation());
  }

/* value - convert p from a conditional to a value, if necessary (it's not
   necessary in SUIF) */
static genop value(genop p)
  {
    return p;
  }


/*
 *  This function is called when there is a sequence point after the
 *  calculation of this operand and before its use.
 */

extern genop sequence_point(genop the_genop)
  {
    if (option_mark_execution_points)
      {
        genop result = the_genop;
        result.make_temporary();
        result.precomputation()->append(sequence_point_mark(&src));
        return result;
      }
    else
      {
        return the_genop;
      }
  }

extern tree_node *sequence_point_mark(Coordinate *where)
  {
    in_rrr *the_mark = new in_rrr(io_mrk);
    immed_list *the_immeds = new immed_list;
    if (where != NULL)
      {
        the_immeds->append(where->file);
        the_immeds->append((int)where->x);
        the_immeds->append((int)where->y);
      }
    the_mark->append_annote(k_sequence_point, the_immeds);
    return new tree_instr(the_mark);
  }
