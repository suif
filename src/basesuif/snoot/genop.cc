/* file "genop.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the implementation of the generalized operand class
 *  genop.
 */

#include <limits.h>
#include "c.h"

typedef struct
  {
    sym_node_list *read_once;
    sym_node_list *read_multiple;
  } read_once_data;

const char *k_sequence_point;
const char *k_bit_field_info;
const char *k_type_name;
const char *k_type_source_coordinates;
const char *k_field_table;
const char *k_type_alignment;
const char *k_C_comment;
const char *k_source_coordinates;
const char *k_is_declared_reg;
const char *k_source_references;
const char *k_builtin_args;
const char *k_typedef_name;

boolean option_keep_comments = FALSE;
boolean option_mark_execution_points = FALSE;
boolean option_null_check = FALSE;
boolean option_keep_typedef_info = FALSE;
boolean option_force_enum_is_int = FALSE;

static proc_sym *bit_ref_symbol;

static void side_effect_list(tree_node_list *the_list,
                             tree_node_list_e *position, operand the_operand);
static void written_and_not_read(sym_node_list *sym_list,
                                 tree_node_list *node_list);
static void find_temps_read_once(sym_node_list *sym_list,
                                 tree_node_list *node_list);
static void prune_once_read_on_node_list(sym_node_list *read_once,
                                         sym_node_list *read_multiple,
                                         tree_node_list *node_list);
static void prune_once_read_on_node(tree_node *the_node, void *data);
static void prune_once_read_on_instr(instruction *the_instr, void *data);
static void remove_sym_from_list(sym_node_list *the_list, sym_node *symbol);
static void attempt_combination(sym_node_list *read_once,
                                tree_node_list *node_list);
static boolean instr_uses_var(instruction *the_instr, var_sym *the_var);
static void replace_operand(instruction *the_instr, operand old_operand,
                            operand new_operand);
static void prune_out_reads(sym_node_list *sym_list, instruction *the_instr);
static void remove_writes(sym_node_list *sym_list, tree_node_list *node_list);
static boolean symbol_in_list(sym_node_list *sym_list, sym_node *the_symbol);
static void eliminate_write(tree_instr *writer);
static void remove_symbols(sym_node_list *sym_list);
static void clean_field_refs_on_tree_node(tree_node *the_node, void *);
static operand clean_field_refs_on_operand(operand to_clean);
static instruction *clean_field_refs_on_instr(instruction *to_clean);
static operand expand_bit_field_ref(instruction *to_undo);
static operand undo_bit_field_on_instr(instruction *to_undo,
                                       unsigned short *from,
                                       unsigned short *to,
                                       type_node **result_type);
static boolean instr_is_bit_field_ref(instruction *to_test);

boolean genop::is_addressable(void)
  {
    if (suif_operand().is_expr())
      {
        instruction *the_instr = suif_operand().instr();
        assert(the_instr != NULL);
        return (the_instr->opcode() == io_lod);
      }
    else if (suif_operand().is_symbol())
      {
        sym_node *symbol = suif_operand().symbol();
        assert(symbol != NULL);
        return symbol->is_userdef();
      }
    else
      {
        return FALSE;
      }
  }

type_node *genop::type(void)
  {
    return suif_operand().type();
  }

tree_node_list *genop::side_effects_only(void)
  {
    tree_node_list *the_list = precomputation();
    side_effect_list(the_list, NULL, suif_operand());
    return the_list;
  }

void genop::make_temporary(void)
  {
    type_node *this_type = type();

    if (suif_operand().is_symbol())
      {
        sym_node *symbol = suif_operand().symbol();
        assert(symbol != NULL);
        if (!symbol->is_userdef())
            return;
      }
    else if (suif_operand().is_expr())
      {
        instruction *the_instr = suif_operand().instr();
        assert(the_instr != NULL);
        if (the_instr->opcode() == io_ldc)
            return;
      }
    else
      {
        return;
      }

    if (needconst)
      {
        immed const_value;
        eval_status status = evaluate_as_const(&const_value);
        boolean need_default_value = FALSE;
        switch (status)
          {
            case EVAL_OVERFLOW:
                warning("overflow in constant expression\n");
                break;
            case EVAL_OK:
                break;
            case EVAL_NOT_CONST:
                error("expression must be constant\n");
                need_default_value = TRUE;
                break;
            case EVAL_DIV_BY_ZERO:
                error("division by zero in constant expression\n");
                need_default_value = TRUE;
                break;
            case EVAL_UNKNOWN_AT_LINK:
                error("constant expression cannot be computed at link time\n");
                need_default_value = TRUE;
                break;
            default:
                assert(FALSE);
          }

        if (need_default_value)
          {
            if (type()->unqual()->op() == TYPE_FLOAT)
                const_value = immed(0.0);
            else
                const_value = immed(0);
          }

        if (suif_operand().is_expr())
            delete suif_operand().instr();

        in_ldc *new_ldc = new in_ldc(type(), operand(), const_value);
        the_suif_operand = operand(new_ldc);
        return;
      }

    if (is_boolean_type())
        this_type = booleantype;

    var_sym *temporary = get_current_symtab()->new_unique_var(this_type);
    temporary->reset_userdef();

    precomputation()->append(create_assignment(temporary, suif_operand()));
    the_suif_operand = operand(temporary);
  }

boolean genop::is_int_const(void)
  {
    if (!suif_operand().is_expr())
        return FALSE;
    instruction *the_instr = suif_operand().instr();
    assert(the_instr != NULL);
    if (the_instr->opcode() != io_ldc)
        return FALSE;
    in_ldc *the_ldc = (in_ldc *)the_instr;
    return the_ldc->value().is_int_const();
  }

boolean genop::is_boolean_type(void)
  {
    if (suif_operand().is_expr())
      {
        instruction *the_instr = suif_operand().instr();
        switch(the_instr->opcode())
          {
            case io_seq:
            case io_sne:
            case io_sl:
            case io_sle:
                return TRUE;
            default:
                return FALSE;
          }
      }
    else if (suif_operand().is_symbol())
      {
        sym_node *symbol = suif_operand().symbol();
        assert(symbol != NULL);
        if (symbol->is_userdef())
            return FALSE;

        assert(symbol->is_var());
        var_sym *the_var = (var_sym *)symbol;
        return (the_var->type() == booleantype);
      }
    else
      {
        return FALSE;
      }
  }

/* Checks for a ``null pointer constant'' as defined by ANSI/ISO 9899,
 * Section 6.2.2.3, paragraph 3. */
boolean genop::is_null_pointer_constant(void)
  {
    if (!precomputation()->is_empty())
        return FALSE;
    if (!suif_operand().is_expr())
        return FALSE;
    instruction *the_instr = suif_operand().instr();
    assert(the_instr != NULL);
    while (the_instr->opcode() == io_cvt)
      {
        in_rrr *the_cvt = (in_rrr *)the_instr;
        type_node *cvt_result = the_cvt->result_type();
        if (!cvt_result->is_ptr())
            break;
        ptr_type *result_ptr = (ptr_type *)cvt_result;
        type_node *result_ref = result_ptr->ref_type();
        if (result_ref->op() != TYPE_VOID)
            break;
        operand new_op = the_cvt->src_op();
        if (!new_op.is_expr())
            break;
        the_instr = new_op.instr();
      }
    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        if (the_ldc->value() == immed(0))
            return TRUE;
      }

#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
    genop test_genop(operand(the_instr), precomputation());
#else
//
//  gcc version 2.7.2 gets a parse error on the code above.  It looks
//  like the same parsing bug in gcc as noted other places where the
//  DEAL_WITH_GCC_BRAIN_DAMAGE flag is used.  A work-around folows.
//
    operand instr_op(the_instr);
    genop test_genop(instr_op, precomputation());
#endif
    if (!test_genop.is_integral_constant_expression())
        return FALSE;
    immed immed_value;
    eval_status result = evaluate_as_const(&immed_value);
    return ((result == EVAL_OK) && (immed_value == immed(0)));
  }

/* Checks for an ``integral constant expression'' as defined by
 * ANSI/ISO 9899, Section 6.4, sub-section ``Semantics'', paragraph
 * 2. */
boolean genop::is_integral_constant_expression(void)
  {
    if (!precomputation()->is_empty())
        return FALSE;
    if (!suif_operand().is_expr())
        return FALSE;
    instruction *the_instr = suif_operand().instr();
    assert(the_instr != NULL);
    while (the_instr->opcode() == io_cvt)
      {
        in_rrr *the_cvt = (in_rrr *)the_instr;
        if (the_cvt->result_type()->op() != TYPE_INT)
            return FALSE;
        operand new_op = the_cvt->src_op();
        if (!new_op.is_expr())
            return FALSE;
        the_instr = new_op.instr();
        if (new_op.type()->op() == TYPE_FLOAT)
          {
            if (the_instr->opcode() != io_ldc)
                return FALSE;
            in_ldc *the_ldc = (in_ldc *)the_instr;
            return the_ldc->value().is_float_const();
          }
        if (new_op.type()->op() != TYPE_INT)
            return FALSE;
      }
    if (the_instr->opcode() != io_ldc)
        return FALSE;
    in_ldc *the_ldc = (in_ldc *)the_instr;
    return the_ldc->value().is_int_const();
  }

void genop::make_bit_field_ref(unsigned short from, unsigned short to,
                               type_node *result_type)
  {
    instruction *function =
            new in_ldc(voidtype->ptr_to(), operand(), immed(bit_ref_symbol));
    in_cal *the_call =
            new in_cal(result_type, operand(), operand(function), 3);
    the_call->set_argument(0, suif_operand());
    the_call->set_argument(1, operand(new in_ldc(inttype, operand(),
                                                 immed((int)from))));
    the_call->set_argument(2, operand(new in_ldc(inttype, operand(),
                                                 immed((int)to))));
    the_suif_operand = operand(the_call);
  }

boolean genop::is_bit_field_ref(void)
  {
    if (!suif_operand().is_expr())
        return FALSE;
    instruction *the_instr = suif_operand().instr();
    assert(the_instr != NULL);

    return instr_is_bit_field_ref(the_instr);
  }

void genop::undo_bit_field_ref(unsigned short *from, unsigned short *to,
                               type_node **result_type)
  {
    assert(suif_operand().is_expr());
    instruction *the_instr = suif_operand().instr();
    assert(the_instr != NULL);

    the_suif_operand =
            undo_bit_field_on_instr(the_instr, from, to, result_type);
  }

void genop::double_bit_field_ref(genop *op1, genop *op2)
  {
    assert(is_bit_field_ref());

    assert(suif_operand().is_expr());
    instruction *the_instr = suif_operand().instr();
    assert(the_instr != NULL);
    assert(the_instr->opcode() == io_cal);
    in_cal *old_call = (in_cal *)the_instr;

    assert(old_call->num_args() == 3);
    operand base_pointer = old_call->argument(0);
    base_pointer.remove();
    genop base_genop(base_pointer, precomputation());
    base_genop.make_temporary();

    in_cal *new_call =
            new in_cal(old_call->result_type(), operand(),
                       old_call->addr_op().clone(), 3);
    new_call->set_argument(0, base_genop.suif_operand().clone());
    new_call->set_argument(1, old_call->argument(1).clone());
    new_call->set_argument(2, old_call->argument(2).clone());

    old_call->set_argument(0, base_genop.suif_operand());

    *op1 = genop(operand(old_call), base_genop.precomputation());
    *op2 = genop(operand(new_call), new tree_node_list);
  }

void genop::deallocate(void)
  {
    delete precomputation();
    if (suif_operand().is_expr())
        delete suif_operand().instr();
  }

void genop::clean_up_bit_field_refs(void)
  {
    precomputation()->map(&clean_field_refs_on_tree_node, NULL);
    the_suif_operand = clean_field_refs_on_operand(suif_operand());
  }

eval_status genop::evaluate_as_const(immed *result)
  {
    if (!precomputation()->is_empty())
        return EVAL_NOT_CONST;

    suppress_array_folding = FALSE;
    eval_status status = evaluate_const_expr(suif_operand(), result);
    suppress_array_folding = TRUE;
    return status;
  }

extern void init_annotes(void)
  {
    k_type_alignment = lexicon->enter("type alignment")->sp;

    ANNOTE(k_sequence_point,          "sequence point",          TRUE);
    ANNOTE(k_bit_field_info,          "bit field info",          FALSE);
    ANNOTE(k_type_name,               "type name",               FALSE);
    ANNOTE(k_type_source_coordinates, "type source coordinates", FALSE);
    ANNOTE(k_field_table,             "field table",             FALSE);
    ANNOTE(k_C_comment,               "C comment",               TRUE);
    ANNOTE(k_source_coordinates,      "source coordinates",      TRUE);
    ANNOTE(k_is_declared_reg,         "is declared reg",         FALSE);
    ANNOTE(k_source_references,       "source references",       TRUE);
    ANNOTE(k_builtin_args,            "builtin args",            TRUE);
    ANNOTE(k_typedef_name,            "typedef name",            TRUE);
  }

extern void init_bit_ref(void)
  {
    /* Note that we don't install this since it will never be written out --
       it's just a placeholder */
    bit_ref_symbol =
            new proc_sym(func(inttype), src_unknown, "00bit_field_ref");
  }

extern tree_node *last_action_node(tree_node_list *the_node_list)
  {
    return last_action_before(the_node_list, NULL);
  }

extern tree_node *last_action_before(tree_node_list *the_node_list,
                                     tree_node *the_node)
  {
    if (the_node_list == NULL)
        return NULL;

    if (the_node_list->is_empty())
        return NULL;

    tree_node_list_e *current_element = the_node_list->tail();

    if (the_node != NULL)
      {
        while (current_element != NULL)
          {
            tree_node_list_e *old_element = current_element;
            current_element = current_element->prev();
            if (old_element->contents == the_node)
                break;
          }
      }

    while (current_element != NULL)
      {
        tree_node *this_node = current_element->contents;
        switch (this_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)this_node;
                instruction *the_instr = the_tree_instr->instr();
                assert(the_instr != NULL);
                if ((the_instr->opcode() != io_mrk) &&
                    (the_instr->opcode() != io_nop))
                  {
                    return this_node;
                  }
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)this_node;
                tree_node *last_in_block = last_action_node(the_block->body());
                if (last_in_block != NULL)
                    return last_in_block;
              }
            default:
                return this_node;
          }
        current_element = current_element->prev();
      }
    return NULL;
  }

extern boolean operand_may_have_side_effects(operand the_operand)
  {
    if (the_operand.is_symbol())
      {
        sym_node *the_symbol = the_operand.symbol();
        assert(the_symbol != NULL);
        if (!the_symbol->is_var())
            return TRUE;
        var_sym *the_var = (var_sym *)the_symbol;
        return any_part_volatile(the_var->type());
      }
    else if (the_operand.is_expr())
      {
        instruction *the_instr = the_operand.instr();
        if (the_instr->opcode() == io_cal)
          {
            return TRUE;
          }
        else if (the_instr->opcode() == io_lod)
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            operand address = the_rrr->src_addr_op();
            type_node *addr_type = address.type();
            addr_type = addr_type->unqual();
            if (!addr_type->is_ptr())
                return TRUE;
            ptr_type *the_ptr_type = (ptr_type *)addr_type;
            if (any_part_volatile(the_ptr_type->ref_type()))
                return TRUE;
          }
        else if (the_instr->opcode() == io_str)
          {
            return TRUE;
          }
        else if (the_instr->opcode() == io_memcpy)
          {
            return TRUE;
          }

        int num_srcs = the_instr->num_srcs();
        for (int src_num = 0; src_num < num_srcs; ++src_num)
          {
            if (operand_may_have_side_effects(the_instr->src_op(src_num)))
                return TRUE;
          }
        return FALSE;
      }
    else if (the_operand.is_null())
      {
        return FALSE;
      }
    else
      {
        return TRUE;
      }
  }

extern boolean is_same_operand(operand op1, operand op2)
  {
    if (op1.is_symbol())
      {
        if (!op2.is_symbol())
            return FALSE;
        return (op1.symbol() == op2.symbol());
      }
    else if (op1.is_expr())
      {
        if (!op2.is_expr())
            return FALSE;
        instruction *instr1 = op1.instr();
        instruction *instr2 = op2.instr();
        assert(instr1 != NULL);
        assert(instr2 != NULL);
        if (instr1->opcode() != instr2->opcode())
            return FALSE;
        if (!instr1->result_type()->is_same(instr2->result_type()))
            return FALSE;
        if (instr1->opcode() == io_ldc)
          {
            in_ldc *ldc1 = (in_ldc *)instr1;
            in_ldc *ldc2 = (in_ldc *)instr2;
            return (ldc1->value() == ldc2->value());
          }
        else
          {
            unsigned num_srcs = instr1->num_srcs();
            if (num_srcs != instr2->num_srcs())
                return FALSE;
            for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
              {
                if (!is_same_operand(instr1->src_op(src_num),
                                     instr2->src_op(src_num)))
                  {
                    return FALSE;
                  }
              }
            return TRUE;
          }
      }
    else
      {
        return FALSE;
      }
  }

extern void comment_text(char *comment_string)
  {
    if (!option_keep_comments)
        return;

    suif_object *mark_object;
    if (curr_list == NULL)
      {
        mark_object = outf;
      }
    else
      {
        in_rrr *new_mark = new in_rrr(io_mrk);
        curr_list->append(new tree_instr(new_mark));
        mark_object = new_mark;
      }

    immed_list *new_annote = new immed_list;
    new_annote->append(immed(comment_string));
    mark_object->append_annote(k_C_comment, new_annote);
  }

extern immed_list *coordinate_to_immeds(Coordinate the_coordinate)
  {
    immed_list *result = new immed_list;
    result->append(immed((the_coordinate.file == NULL) ?
                         "" : the_coordinate.file));
    result->append(immed((int)the_coordinate.x));
    result->append(immed((int)the_coordinate.y));
    return result;
  }

extern Coordinate *immeds_to_coordinate(immed_list *the_immeds)
  {
    static Coordinate result;

    if (the_immeds == NULL)
        return NULL;

    immed_list_iter the_iter(the_immeds);

    if (the_iter.is_empty())
        return NULL;
    immed file_immed = the_iter.step();
    if (!file_immed.is_string())
        return NULL;
    result.file = file_immed.string();

    if (the_iter.is_empty())
        return NULL;
    immed x_immed = the_iter.step();
    if (!x_immed.is_integer())
        return NULL;
    result.x = x_immed.integer();

    if (the_iter.is_empty())
        return NULL;
    immed y_immed = the_iter.step();
    if (!y_immed.is_integer())
        return NULL;
    result.y = y_immed.integer();

    if (!the_iter.is_empty())
        return NULL;
    return &result;
  }

extern void remove_unused_temps(tree_node_list *the_list)
  {
    assert(the_list != NULL);

    sym_node_list unused;
    written_and_not_read(&unused, the_list);
    remove_writes(&unused, the_list);
    remove_symbols(&unused);

    sym_node_list read_once;
    find_temps_read_once(&read_once, the_list);
    attempt_combination(&read_once, the_list);

    while (!read_once.is_empty())
      {
        sym_node_list_e *this_element = read_once.head();
        read_once.remove(this_element);
        delete this_element;
      }
  }

extern immed immed_and_type_for_C_intconst(char *C_intconst,
                                           type_node **the_type)
  {
    char *digit_start = C_intconst;
    int radix;
    if (*digit_start == '0')
      {
        ++digit_start;
        if ((*digit_start == 'X') || (*digit_start == 'x'))
          {
            ++digit_start;
            radix = 16;
          }
        else
          {
	    /* read "0" as "0" rather than skipping to the next token */
	    if (isspace(*digit_start))
		--digit_start;
            radix = 8;
          }
      }
    else
      {
        radix = 10;
      }

    i_integer const_value(digit_start, radix);
    immed result_value = ii_to_immed(const_value);

    if (the_type != NULL)
      {
        char *follow = digit_start;
        while (TRUE)
          {
            if (((*follow >= '0') && (*follow <= '9')) ||
                ((*follow >= 'a') && (*follow <= 'f')) ||
                ((*follow >= 'A') && (*follow <= 'F')))
              {
                ++follow;
              }
            else
              {
                break;
              }
          }

        int l_count = 0;
        boolean u_suffix = FALSE;

        while (TRUE)
          {
            if ((!u_suffix) && ((*follow == 'u') || (*follow == 'U')))
                u_suffix = TRUE;
            else if ((l_count < 2) && ((*follow == 'l') || (*follow == 'L')))
                ++l_count;
            else
                break;
            ++follow;
          }

        /*
         * See ANSI/ISO 9899-1990, section 6.1.3.2, second paragraph
         * of ``Semantics'' section.
         */
        if ((l_count == 2) && u_suffix)
          {
            *the_type = unsignedlonglong;
          }
        else if (l_count == 2)
          {
            if (immed_fits(result_value, longlong))
                *the_type = longlong;
            else
                *the_type = unsignedlonglong;
          }
        else if ((l_count >= 1) && u_suffix)
          {
            if (immed_fits(result_value, unsignedlong))
                *the_type = unsignedlong;
            else
                *the_type = unsignedlonglong;
          }
        else if (l_count >= 1)
          {
            if (immed_fits(result_value, longtype))
                *the_type = longtype;
            else if (immed_fits(result_value, unsignedlong))
                *the_type = unsignedlong;
            else if (immed_fits(result_value, longlong))
                *the_type = longlong;
            else
                *the_type = unsignedlonglong;
          }
        else if (u_suffix)
          {
            if (immed_fits(result_value, unsignedtype))
                *the_type = unsignedtype;
            else if (immed_fits(result_value, unsignedlong))
                *the_type = unsignedlong;
            else
                *the_type = unsignedlonglong;
          }
        else
          {
            if (radix == 10)
              {
                if (immed_fits(result_value, inttype))
                    *the_type = inttype;
                else if (immed_fits(result_value, longtype))
                    *the_type = longtype;
                else if (immed_fits(result_value, unsignedlong))
                    *the_type = unsignedlong;
                else if (immed_fits(result_value, longlong))
                    *the_type = longlong;
                else
                    *the_type = unsignedlonglong;
              }
            else
              {
                if (immed_fits(result_value, inttype))
                    *the_type = inttype;
                else if (immed_fits(result_value, unsignedtype))
                    *the_type = unsignedtype;
                else if (immed_fits(result_value, longtype))
                    *the_type = longtype;
                else if (immed_fits(result_value, unsignedlong))
                    *the_type = unsignedlong;
                else if (immed_fits(result_value, longlong))
                    *the_type = longlong;
                else
                    *the_type = unsignedlonglong;
              }
          }

        if (!immed_fits(result_value, *the_type))
          {
            warning("overflow in constant `%s'\n", C_intconst);
            eval_status status = fit_immed(&result_value, *the_type);
            assert(status == EVAL_OVERFLOW);
          }
      }

    return result_value;
  }

extern immed immed_and_type_for_C_floatconst(char *C_floatconst,
                                             type_node **the_type)
  {
    char *follow = (char *)cp;
    assert(follow > C_floatconst);
    --follow;
    type_node *result_type;
    if ((*follow == 'f') || (*follow == 'F'))
        result_type = floattype;
    else if ((*follow == 'l') || (*follow == 'L'))
        result_type = longdouble;
    else
        result_type = doubletype;

    if (the_type != NULL)
        *the_type = result_type;

    assert(result_type->op() == TYPE_FLOAT);
    if (native_floating_arithmetic_ok((base_type *)result_type))
      {
        double double_value = 0;
        follow = C_floatconst;

	double_value = strtod(C_floatconst, NULL);
        return immed(double_value);
      }
    else
      {
        char *mantissa = new char[((char *)cp - C_floatconst) + 1];
        i_integer additional_exponent(0);

        follow = C_floatconst;
        char *follow_mantissa = mantissa;

        while ((*follow >= '0') && (*follow <= '9'))
          {
            *follow_mantissa = *follow;
            ++follow_mantissa;
            ++additional_exponent;
            ++follow;
          }

        if (*follow == '.')
          {
            ++follow;
            while ((*follow >= '0') && (*follow <= '9'))
              {
                *follow_mantissa = *follow;
                ++follow_mantissa;
                ++follow;
              }
          }

        while ((follow_mantissa > mantissa) && (*(follow_mantissa - 1) == '0'))
            --follow_mantissa;

        *follow_mantissa = 0;

        follow_mantissa = mantissa;
        while (*follow_mantissa == '0')
          {
            ++follow_mantissa;
            --additional_exponent;
          }

        i_integer exponent(0);

        if ((*follow == 'e') || (*follow == 'E'))
          {
            ++follow;
            exponent.read(follow);
          }

        exponent += (additional_exponent - 1);

        unsigned long exponent_length =
                exponent.written_length().c_unsigned_long();
        char *result_string =
                new char[strlen(follow_mantissa) + exponent_length + 8];

        char *follow_result = result_string;
        if (*follow_mantissa == 0)
          {
            *follow_result = '0';
          }
        else
          {
            *follow_result = *follow_mantissa;
            ++follow_mantissa;
          }
        ++follow_result;

        *follow_result = '.';
        ++follow_result;

        if (*follow_mantissa == 0)
          {
            *follow_result = '0';
            ++follow_result;
          }
        else
          {
            while (*follow_mantissa != 0)
              {
                *follow_result = *follow_mantissa;
                ++follow_result;
                ++follow_mantissa;
              }
          }

        *follow_result = 'e';
        ++follow_result;

        if (!exponent.is_negative())
          {
            *follow_result = '+';
            ++follow_result;
          }

        exponent.write(follow_result);

        immed result_value = immed(im_extended_float, result_string);

        delete[] result_string;
        delete[] mantissa;

        return result_value;
      }
  }

static void side_effect_list(tree_node_list *the_list,
                             tree_node_list_e *position, operand the_operand)
  {
    assert(the_list != NULL);

    tree_node_list *side_effects = reduce_to_side_effects(the_operand);
    if (side_effects != NULL)
      {
        the_list->insert_before(side_effects, position);
        delete side_effects;
      }
  }

static void written_and_not_read(sym_node_list *sym_list,
                                 tree_node_list *node_list)
  {
    assert(sym_list != NULL);
    if (node_list == NULL)
        return;

    tree_node_list_iter the_iter(node_list);
    while (!the_iter.is_empty())
      {
        tree_node *this_node = the_iter.step();
        switch (this_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *this_tree_instr = (tree_instr *)this_node;
                instruction *this_instr = this_tree_instr->instr();
                assert(this_instr != NULL);
                prune_out_reads(sym_list, this_instr);

                operand destination = this_instr->dst_op();
                if (!destination.is_symbol())
                    break;

                sym_node *this_symbol = destination.symbol();
                assert(this_symbol != NULL);
                if (this_symbol->is_userdef())
                    break;

                if (sym_list->lookup(this_symbol) == NULL)
                    sym_list->append(this_symbol);

                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)this_node;
                written_and_not_read(sym_list, the_if->header());
                written_and_not_read(sym_list, the_if->then_part());
                written_and_not_read(sym_list, the_if->else_part());
                break;
              }
            default:
                /*
                 * This should never happen since this function should only be
                 * called on lists of instructions and tree_if's.
                 */
                assert(FALSE);
          }
      }
  }

static void find_temps_read_once(sym_node_list *sym_list,
                                 tree_node_list *node_list)
  {
    sym_node_list read_multiple;
    prune_once_read_on_node_list(sym_list, &read_multiple, node_list);

    while (!read_multiple.is_empty())
      {
        sym_node_list_e *this_element = read_multiple.head();
        read_multiple.remove(this_element);
        delete this_element;
      }
  }

static void prune_once_read_on_node_list(sym_node_list *read_once,
                                         sym_node_list *read_multiple,
                                         tree_node_list *node_list)
  {
    read_once_data read_data;
    read_data.read_once = read_once;
    read_data.read_multiple = read_multiple;

    node_list->map(&prune_once_read_on_node, &read_data);
  }

static void prune_once_read_on_node(tree_node *the_node, void *data)
  {
    assert(the_node != NULL);
    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    the_tree_instr->instr_map(&prune_once_read_on_instr, data);
  }

static void prune_once_read_on_instr(instruction *the_instr, void *data)
  {
    assert(the_instr != NULL);
    assert(data != NULL);

    read_once_data *read_data = (read_once_data *)data;

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand source = the_instr->src_op(src_num);
        if (source.is_symbol())
          {
            var_sym *the_var = source.symbol();
            if (!the_var->is_userdef())
              {
                sym_node_list_e *the_element =
                        read_data->read_multiple->lookup(the_var);
                if (the_element == NULL)
                  {
                    the_element = read_data->read_once->lookup(the_var);
                    if (the_element == NULL)
                      {
                        read_data->read_once->append(the_var);
                      }
                    else
                      {
                        read_data->read_once->remove(the_element);
                        delete the_element;
                        read_data->read_multiple->append(the_var);
                      }
                  }
              }
          }
      }

    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        immed value = the_ldc->value();
        if (value.is_symbol())
          {
            sym_node *the_symbol = value.symbol();
            if (!the_symbol->is_userdef())
              {
                remove_sym_from_list(read_data->read_once, the_symbol);
                read_data->read_multiple->append(the_symbol);
              }
          }
      }
  }

static void remove_sym_from_list(sym_node_list *the_list, sym_node *symbol)
  {
    assert(symbol != NULL);
    sym_node_list_e *the_element = the_list->lookup(symbol);
    if (the_element != NULL)
      {
        the_list->remove(the_element);
        delete the_element;
        return;
      }
  }

static void attempt_combination(sym_node_list *read_once,
                                tree_node_list *node_list)
  {
    assert(read_once != NULL);
    assert(node_list != NULL);

    tree_node_list_iter node_iter(node_list);
    while (!node_iter.is_empty())
      {
        tree_node *first_node = node_iter.step();
        assert(first_node != NULL);
        if (!first_node->is_instr())
            continue;
        tree_instr *first_instr_node = (tree_instr *)first_node;
        instruction *first_instr = first_instr_node->instr();
        assert(first_instr != NULL);

        if (node_iter.is_empty())
            break;
        tree_node *second_node = node_iter.peek();
        assert(second_node != NULL);
        if (!second_node->is_instr())
            continue;
        tree_instr *second_instr_node = (tree_instr *)second_node;
        instruction *second_instr = second_instr_node->instr();
        assert(second_instr != NULL);

        operand first_destination = first_instr->dst_op();
        if (!first_destination.is_symbol())
            continue;
        var_sym *the_var = first_destination.symbol();
        assert(the_var != NULL);
        sym_node_list_e *the_list_e = read_once->lookup(the_var);
        if (the_list_e == NULL)
            continue;

        if (!instr_uses_var(second_instr, the_var))
            continue;

        first_instr_node->remove_instr(first_instr);
        node_list->remove(first_instr_node->list_e());
        delete first_instr_node->list_e();
        delete first_instr_node;

        first_instr->set_dst(operand());
        operand new_operand(first_instr);
        if (first_instr->opcode() == io_cpy)
          {
            in_rrr *first_rrr = (in_rrr *)first_instr;
            new_operand = first_rrr->src_op();
            new_operand.remove();
            delete first_rrr;
          }

        replace_operand(second_instr, operand(the_var), new_operand);

        read_once->remove(the_list_e);
        delete the_list_e;
        the_var->parent()->remove_sym(the_var);
        delete the_var;
      }
  }

static boolean instr_uses_var(instruction *the_instr, var_sym *the_var)
  {
    assert(the_instr != NULL);

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_source = the_instr->src_op(src_num);
        if (this_source == operand(the_var))
            return TRUE;
        if (this_source.is_expr())
          {
            if (instr_uses_var(this_source.instr(), the_var))
                return TRUE;
          }
      }
    return FALSE;
  }

static void replace_operand(instruction *the_instr, operand old_operand,
                            operand new_operand)
  {
    assert(the_instr != NULL);

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_source = the_instr->src_op(src_num);
        if (this_source == old_operand)
          {
            the_instr->set_src_op(src_num, new_operand);
            return;
          }
        if (this_source.is_expr())
            replace_operand(this_source.instr(), old_operand, new_operand);
      }
  }

static void prune_out_reads(sym_node_list *sym_list, instruction *the_instr)
  {
    assert(sym_list != NULL);
    assert(the_instr != NULL);

    if (sym_list->is_empty())
        return;

    int num_srcs = the_instr->num_srcs();
    for (int src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_symbol())
          {
            sym_node *this_symbol = this_op.symbol();
            assert(this_symbol != NULL);
            sym_node_list_e *this_element;
            sym_node_list_e *next_element = sym_list->head();
            while (next_element != NULL)
              {
                this_element = next_element;
                next_element = this_element->next();
                sym_node *test_sym = this_element->contents;
                if (test_sym == this_symbol)
                  {
                    sym_list->remove(this_element);
                    delete this_element;
                  }
              }
            if (sym_list->is_empty())
                return;
          }
        else if (this_op.is_expr())
          {
            instruction *this_instr = this_op.instr();
            assert(this_instr != NULL);
            prune_out_reads(sym_list, this_instr);

            if (this_instr->opcode() == io_ldc)
              {
                in_ldc *this_ldc = (in_ldc *)this_instr;
                immed value = this_ldc->value();
                if (value.is_symbol())
                  {
                    sym_node_list_e *this_element =
                            sym_list->lookup(value.symbol());
                    if (this_element != NULL)
                      {
                        sym_list->remove(this_element);
                        delete this_element;
                      }
                  }
              }
          }
      }
  }

static void remove_writes(sym_node_list *sym_list, tree_node_list *node_list)
  {
    assert(sym_list != NULL);
    if (node_list == NULL)
        return;

    if (sym_list->is_empty())
        return;

    tree_node_list_iter the_iter(node_list);
    while (!the_iter.is_empty())
      {
        tree_node *this_node = the_iter.step();
        switch (this_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *this_tree_instr = (tree_instr *)this_node;
                instruction *this_instr = this_tree_instr->instr();
                assert(this_instr != NULL);

                operand destination = this_instr->dst_op();
                if (!destination.is_symbol())
                    break;

                sym_node *this_symbol = destination.symbol();
                assert(this_symbol != NULL);
                if (!symbol_in_list(sym_list, this_symbol))
                    break;

                eliminate_write(this_tree_instr);
                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)this_node;
                remove_writes(sym_list, the_if->header());
                remove_writes(sym_list, the_if->then_part());
                remove_writes(sym_list, the_if->else_part());
                break;
              }
            default:
                /*
                 * This should never happen since this function should only be
                 * called on lists of instructions and tree_if's.
                 */
                assert(FALSE);
          }
      }
  }

static boolean symbol_in_list(sym_node_list *sym_list, sym_node *the_symbol)
  {
    return (sym_list->lookup(the_symbol) != NULL);
  }

static void eliminate_write(tree_instr *writer)
  {
    tree_node_list_e *the_element = writer->list_e();
    assert(the_element != NULL);
    tree_node_list *the_list = writer->parent();
    assert(the_list != NULL);
    instruction *the_instr = writer->instr();
    assert(the_instr != NULL);

    writer->remove_instr(the_instr);
    the_instr->set_dst(operand());
    side_effect_list(the_list, the_element, operand(the_instr));
    the_list->remove(the_element);
    delete writer;
    delete the_element;
  }

static void remove_symbols(sym_node_list *sym_list)
  {
    assert(sym_list != NULL);

    sym_node_list_e *this_element;
    sym_node_list_e *next_element = sym_list->head();
    while (next_element != NULL)
      {
        this_element = next_element;
        next_element = this_element->next();
        sym_node *the_symbol = this_element->contents;
        assert(the_symbol != NULL);
        the_symbol->parent()->remove_sym(the_symbol);
        delete the_symbol;
        sym_list->remove(this_element);
        delete this_element;
      }
    assert(sym_list->is_empty());
  }

static void clean_field_refs_on_tree_node(tree_node *the_node, void *)
  {
    assert(the_node != NULL);
    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *old_instr = the_tree_instr->instr();
    assert(old_instr != NULL);
    the_tree_instr->remove_instr(old_instr);
    instruction *new_instr = clean_field_refs_on_instr(old_instr);
    the_tree_instr->set_instr(new_instr);
  }

static operand clean_field_refs_on_operand(operand to_clean)
  {
    if (!to_clean.is_expr())
        return to_clean;
    return operand(clean_field_refs_on_instr(to_clean.instr()));
  }

static instruction *clean_field_refs_on_instr(instruction *to_clean)
  {
    operand destination = to_clean->dst_op();

    unsigned num_srcs = to_clean->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand old_operand = to_clean->src_op(src_num);
        old_operand.remove();
        to_clean->set_src_op(src_num,
                             clean_field_refs_on_operand(old_operand));
      }

    if (instr_is_bit_field_ref(to_clean))
      {
        operand new_operand = expand_bit_field_ref(to_clean);
        instruction *new_instr;
        if (new_operand.is_expr())
          {
            new_instr = new_operand.instr();
          }
        else
          {
            new_instr = new in_rrr(io_cpy, new_operand.type(), operand(),
                                   new_operand);
          }
        if (destination.is_symbol())
          {
            new_instr->set_dst(destination);
          }
        return new_instr;
      }
    else
      {
        return to_clean;
      }
  }

static operand expand_bit_field_ref(instruction *to_undo)
  {
    unsigned short bit_field_from;
    unsigned short bit_field_to;
    type_node *result_type;
    operand address =
            undo_bit_field_on_instr(to_undo, &bit_field_from, &bit_field_to,
                                    &result_type);
    int right_amount = result_type->size() - (bit_field_to - bit_field_from);
    int left_amount = right_amount - field_right;

    in_rrr *the_load =
            new in_rrr(io_lod, unsignedtype, operand(), address);
    in_rrr *the_lsl =
            new in_rrr(io_lsl, unsignedtype, operand(), operand(the_load),
                       operand(new in_ldc(unsignedtype, operand(),
                                          immed(left_amount))));
    in_rrr *pre_asr;
    if_ops right_shift_op;
    if (result_type == unsignedtype)
      {
        pre_asr = the_lsl;
        right_shift_op = io_lsr;
      }
    else
      {
        pre_asr = new in_rrr(io_cvt, result_type, operand(), operand(the_lsl));
        right_shift_op = io_asr;
      }
    in_rrr *the_asr =
            new in_rrr(right_shift_op, result_type, operand(),
                       operand(pre_asr),
                       operand(new in_ldc(unsignedtype, operand(),
                                          immed(right_amount))));
    return operand(the_asr);
  }

static operand undo_bit_field_on_instr(instruction *to_undo,
                                       unsigned short *from,
                                       unsigned short *to,
                                       type_node **result_type)
  {
    assert(to_undo != NULL);
    assert(to_undo->opcode() == io_cal);
    in_cal *the_call = (in_cal *)to_undo;

    operand result = the_call->argument(0);
    result.remove();

    operand from_operand = the_call->argument(1);
    assert(from_operand.is_expr());
    instruction *from_instr = from_operand.instr();
    assert(from_instr != NULL);
    assert(from_instr->opcode() == io_ldc);
    in_ldc *from_ldc = (in_ldc *)from_instr;
    immed from_value = from_ldc->value();
    assert(from_value.is_integer());
    *from = from_value.integer();

    operand to_operand = the_call->argument(2);
    assert(to_operand.is_expr());
    instruction *to_instr = to_operand.instr();
    assert(to_instr != NULL);
    assert(to_instr->opcode() == io_ldc);
    in_ldc *to_ldc = (in_ldc *)to_instr;
    immed to_value = to_ldc->value();
    assert(to_value.is_integer());
    *to = to_value.integer();

    *result_type = the_call->result_type();

    delete to_undo;
    return result;
  }

static boolean instr_is_bit_field_ref(instruction *to_test)
  {
    assert(to_test != NULL);

    if (to_test->opcode() != io_cal)
        return FALSE;
    in_cal *the_call = (in_cal *)to_test;
    operand address = the_call->addr_op();

    if (!address.is_expr())
        return FALSE;
    instruction *addr_instr = address.instr();
    assert(addr_instr != NULL);

    if (addr_instr->opcode() != io_ldc)
        return FALSE;
    in_ldc *the_ldc = (in_ldc *)addr_instr;
    immed value = the_ldc->value();

    if (!value.is_symbol())
        return FALSE;

    return (value.symbol() == bit_ref_symbol);
  }
