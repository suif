/* file "output.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot output functions */

#include "c.h"

static char *current_buf_start = NULL;
static char *current_buf_end = NULL;
static char *current_buf_position = NULL;

static void string_file_print(const char *to_print, void *fp_void);
static void char_file_print(int to_print, void *fp_void);
static void string_buffer_print(const char *to_print, void *);
static void char_buffer_print(int to_print, void *);
static void vgprint(void (*s_printer)(const char *to_print, void *data),
                    void (*c_printer)(int to_print, void *data), void *data,
                    const char *fmt, va_list ap);

/* fprint - formatted output to file pointer fp */
void fprint(FILE *fp, const char *fmt, ...)
  {
    va_list ap;

    va_start(ap, fmt);
    vfprint(fp, fmt, ap);
    va_end(ap);
  }

/* outflush - flush output buffer; writes last buffer first */
void outflush(void)
  {
    fflush(stderr);
    fflush(stdout);
  }

/* print - formatted output to standard output */
void print(char *fmt, ...)
  {
    va_list ap;

    va_start(ap, fmt);
    vfprint(stdout, fmt, ap);
    va_end(ap);
  }

/* gprint - formatted output using the given low-level routines */
extern void gprint(void (*s_printer)(const char *to_print, void *data),
                   void (*c_printer)(int to_print, void *data), void *data,
                   char *fmt, ...)
  {
    va_list ap;

    va_start(ap, fmt);
    vgprint(s_printer, c_printer, data, fmt, ap);
    va_end(ap);
  }

/* stringf - formatted output to a saved string */
char *stringf(const char *fmt, ...)
  {
    va_list ap;

    va_start(ap, fmt);

    if (current_buf_start == NULL)
      {
        current_buf_start = new char[MAXLINE];
        current_buf_end = &(current_buf_start[MAXLINE - 1]);
      }
    current_buf_position = current_buf_start;
    *current_buf_position = 0;
    vgprint(&string_buffer_print, &char_buffer_print, NULL, fmt, ap);

    va_end(ap);
    return string(current_buf_start);
  }

/* vfprint - formatted output to file pointer fp */
void vfprint(FILE *fp, const char *fmt, va_list ap)
  {
    vgprint(&string_file_print, &char_file_print, fp, fmt, ap);
  }

static void string_file_print(const char *to_print, void *fp_void)
  {
    FILE *fp = (FILE *)fp_void;
    fputs(to_print, fp);
  }

static void char_file_print(int to_print, void *fp_void)
  {
    FILE *fp = (FILE *)fp_void;
    fputc(to_print, fp);
  }

static void string_buffer_print(const char *to_print, void *)
  {
    const char *remainder = to_print;
    while (*remainder != 0)
      {
        *current_buf_position = *remainder;
        if (current_buf_position == current_buf_end)
          {
            size_t old_size = current_buf_end - current_buf_start + 1;
            char *new_buffer = new char[old_size * 2];
            memcpy(new_buffer, current_buf_start, old_size);
            delete[] current_buf_start;
            current_buf_start = new_buffer;
            current_buf_end = &(new_buffer[old_size * 2 - 1]);
            current_buf_position = &(new_buffer[old_size - 1]);
          }
        ++current_buf_position;
        ++remainder;
      }
    *current_buf_position = 0;
  }

static void char_buffer_print(int to_print, void *)
  {
    *current_buf_position = to_print;
    if (current_buf_position == current_buf_end)
      {
        size_t old_size = current_buf_end - current_buf_start + 1;
        char *new_buffer = new char[old_size * 2];
        memcpy(new_buffer, current_buf_start, old_size);
        delete[] current_buf_start;
        current_buf_start = new_buffer;
        current_buf_end = &(new_buffer[old_size * 2 - 1]);
        current_buf_position = &(new_buffer[old_size - 1]);
      }
    ++current_buf_position;
    *current_buf_position = 0;
  }

static void vgprint(void (*s_printer)(const char *to_print, void *data),
                    void (*c_printer)(int to_print, void *data), void *data,
                    const char *fmt, va_list ap)
  {
    char buf[40], *s;

    for (; *fmt != 0; fmt++)
      {
        if (*fmt == '%')
          {
            switch (*++fmt)
              {
                case 'c':
                    (*c_printer)(va_arg(ap, int), data);
                    break;
                case 'd':
                  {
                    int n = va_arg(ap, int);
                    unsigned m;
                    s = buf + sizeof buf;
                    *--s = 0;
                    if (n == INT_MIN)
                        m = (unsigned)INT_MAX + 1;
                    else if (n < 0)
                        m = -n;
                    else
                        m = n;
                    do
                        *--s = m%10 + '0';
                      while (m /= 10);
                    if (n < 0)
                        *--s = '-';
                    (*s_printer)(s, data);
                    break;
                  }
                case 'D':
                  {
                    i_integer *the_ii = va_arg(ap, i_integer *);
                    immed the_immed = ii_to_immed(*the_ii);
                    if (the_immed.kind() == im_int)
                      {
                        int n = the_immed.integer();
                        unsigned m;
                        s = buf + sizeof buf;
                        *--s = 0;
                        if (n == INT_MIN)
                            m = (unsigned)INT_MAX + 1;
                        else if (n < 0)
                            m = -n;
                        else
                            m = n;
                        do
                            *--s = m%10 + '0';
                          while (m /= 10);
                        if (n < 0)
                            *--s = '-';
                        (*s_printer)(s, data);
                      }
                    else if (the_immed.kind() == im_extended_int)
                      {
                        (*s_printer)(the_immed.ext_integer(), data);
                      }
                    else
                      {
                        assert(FALSE);
                      }
                    break;
                  }
                case 'k':      /* print a token */
                  {
                    int t = va_arg(ap, int);
                    static char *tokens[] =
                      {
#define xx(a,b,c,d,e,f,g) g,
#define yy xx
#include "token.h"
                      };
                    assert(tokens[t & 0177]);
                    (*s_printer)(tokens[t & 0177], data);
                    break;
                  }
                case 'o':
                  {
                    unsigned n = va_arg(ap, unsigned);
                    s = buf + sizeof buf;
                    *--s = 0;
                    do
                        *--s = (n&7) + '0';
                      while (n >>= 3);
                    (*s_printer)(s, data);
                    break;
                  }
                case 'x':
                  {
                    unsigned n = va_arg(ap, unsigned);
                    s = buf + sizeof buf;
                    *--s = 0;
                    do
                        *--s = "0123456789abcdef"[n & 0xf];
                      while (n >>= 4);
                    (*s_printer)(s, data);
                    break;
                  }
                case 's':
                    s = va_arg(ap, char *);
                    (*s_printer)(s ? s : "", data);
                    break;
                case 't':      /* print a type */
                  {
                    type_node *ty = va_arg(ap, type_node *);
                    outtype(s_printer, c_printer, data,
                            (ty != NULL) ? ty : voidtype);
                    break;
                  }
                case 'u':
                  {
                    unsigned n = va_arg(ap, int);
                    s = buf + sizeof buf;
                    *--s = 0;
                    do
                        *--s = n%10 + '0';
                      while (n /= 10);
                    (*s_printer)(s, data);
                    break;
                  }
                case 'w':      /* print a source coordinate */
                  {
                    Coordinate *p = va_arg(ap, Coordinate *);
                    if (p->file && *p->file)
                      {
                        (*s_printer)(p->file, data);
                        (*c_printer)(':', data);
                      }
                    gprint(s_printer, c_printer, data, "%d", p->y);
                    break;
                  }
                default:
                    (*c_printer)(*fmt, data);
                    break;
              }
          }
        else
          {
            (*c_printer)(*fmt, data);
          }
      }
  }
