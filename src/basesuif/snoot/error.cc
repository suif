/* file "error.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot error reporting */

#include "c.h"

int errcnt;             /* number of compilation errors */
int errlimit = 20;      /* compilation error limit */
boolean wflag;          /* TRUE to suppress warning messages */

static void printtoken(void);

/* error - issue error message */
void error(const char *fmt, ...)
  {
    va_list ap;

    va_start(ap, fmt);
    if ((firstfile != file) && (firstfile != NULL) && (*firstfile != 0))
        fprint(stderr, "%s: ", firstfile);
    fprint(stderr, "%w: ", &src);
    vfprint(stderr, fmt, ap);
    if (++errcnt >= errlimit)
      {
        errcnt = -1;
        error("too many errors\n");
        exit(1);
      }
    va_end(ap);
  }

/* expect - advance if t is tok, otherwise issue message */
int expect(int tok)
  {
    if (t == tok)
      {
        t = gettok();
        return t;
      }
    errcnt--;
    error("syntax error; found");
    printtoken();
    fprint(stderr, " expecting `%k'\n", tok);
    errcnt++;
    return 0;
  }

/* fatal - issue fatal error message and exit */
int fatal(const char *name, const char *fmt, int n)
  {
    fputc('\n', stdout);
    outflush();
    error("compiler error in %s--", name);
    fprint(stderr, fmt, n);
    exit(1);
    return 0;
  }

/* printtoken - print current token preceeded by a space */
static void printtoken(void)
  {
    switch (t)
      {
        case ID:
            fprint(stderr, " `%s'", token); break;
        case ICON:
            if (*token == '\'')
              {
                char *s;
        case SCON:
                fprint(stderr, " ");
                for (s = token; (*s != 0) && (s - token < 20); s++)
                  {
                    if (*s < ' ' || *s >= 0177)
                        fprint(stderr, "\\%o", *s);
                    else
                        fprint(stderr, "%c", *s);
                  }
                if (*s != 0)
                    fprint(stderr, " ...");
                else
                    fprint(stderr, "%c", *token);
                break;
              } /* else fall through */
        case FCON:
          {
            char c = *cp;
            *cp = 0;
            fprint(stderr, " `%s'", token);
            *cp = c;
            break;
          }
        case '`':
        case '\'':
            fprint(stderr, " \"%k\"", t);
            break;
        default:
            fprint(stderr, " `%k'", t);
      }
  }

/* skipto - skip input up to tok U set, for a token where kind[t] is in set */
void skipto(int tok, char set[])
  {
    int n;
    char *s;

    for (n = 0; (t != EOI) && (t != tok); t = gettok())
      {
        if (set)
          {
            for (s = set; (*s != 0) && (kind[t] != *s); s++)
                ;
            if (kind[t] == *s)
                break;
          }
        if (n++ == 0)
          {
            errcnt--;
            error("skipping", 0, 0, 0, 0);
          }
        if (n <= 8)
            printtoken();
        else if (n == 9)
            fprint(stderr, " ...\n");
      }
    if (n > 8)
      {
        errcnt--;
        error("up to", 0, 0, 0, 0);
        printtoken();
      }
    if (n > 0)
        fprint(stderr, "\n");
  }

/* test - check for token tok, skip to tok U set, if necessary */
void test(int tok, char set[])
  {
    if (t == tok)
      {
        t = gettok();
      }
    else
      {
        expect(tok);
        skipto(tok, set);
        if (t == tok)
            t = gettok();
      }
  }

/* warning - issue warning error message */
void warning(const char *fmt, ...)
  {
    va_list ap;

    va_start(ap, fmt);
    if (!wflag)
      {
        errcnt--;    /* compensate for increment in error */
        error("warning: ");
        vfprint(stderr, fmt, ap);
      }
    va_end(ap);
  }
