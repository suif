/* file "sym.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot symbol table management */

#include "c.h"

struct entry            /* symbol table entry: */
  {
    struct symbol sym;      /* the symbol */
    List refs;              /* list form of sym.uses */
    struct entry *link;     /* next entry on hash chain */
  };

struct table            /* symbol tables: */
  {
    int level;              /* scope level for this table */
    struct table *previous; /* table for previous scope */
    Symbol list;            /* list of entries via up fields */
    struct entry *buckets[HASHSIZE];
  };

static struct table
    tconstants =   { CONSTANTS },
    texternals =   { GLOBAL },
    tidentifiers = { GLOBAL },
    ttypes =       { GLOBAL };

Table constants   = &tconstants;        /* constants */
Table externals   = &texternals;        /* externals */
Table identifiers = &tidentifiers;      /* identifiers */
Table globals     = &tidentifiers;      /* globals */
Table labels[2];                        /* labels */
Table types       = &ttypes;            /* types */

int bnumber;                            /* current block number */
int level;                              /* current block level */

/* constant - install and return constant value of type ty */
extern Symbol symbol_for_string_const(type_node *ty, char *value)
  {
    struct entry *p;
    unsigned h = ((unsigned long)value) & (HASHSIZE - 1);

    ty = ty->unqual();
    for (p = constants->buckets[h]; p != NULL; p = p->link)
      {
        if (eqtype(ty, p->sym.type, TRUE))
          {
            if (value == p->sym.u.c.v.p)
                return &p->sym;
          }
      }
    p = (struct entry *)alloc(sizeof *p);
    p->sym.name = value;
    p->sym.scope = CONSTANTS;
    p->sym.type = ty;
    p->sym.sclass = STATIC;
    p->sym.u.c.v.p = value;
    p->sym.u.c.loc = NULL;
    p->sym.suif_symbol = NULL;
    p->sym.defined = 0;
    p->sym.temporary = 0;
    p->sym.computed = 0;
    p->sym.initialized = 0;
    p->sym.ref = 0;
    p->sym.src.file = NULL;
    p->sym.src.x = 0;
    p->sym.src.y = 0;
    p->sym.uses = NULL;
    p->link = constants->buckets[h];
    p->sym.up = constants->list;
    constants->list = &p->sym;
    constants->buckets[h] = p;
    p->refs = 0;
    defsymbol(&p->sym);
    return &p->sym;
  }

/* enterscope - enter a scope */
void enterscope(void)
  {
    if (++level >= USHRT_MAX)
        error("compound statements nested too deeply\n");
  }

/* exitscope - exit a scope */
void exitscope(void)
  {
    if (identifiers->level == level)
      {
        if (Aflag >= 2)
          {
            int n = 0;
            Symbol p;
            for (p = identifiers->list; (p != NULL) && (p->scope == level);
                 p = p->up)
              {
                if (++n > 127)
                  {
                    warning("more than 127 identifiers declared in a block\n");
                    break;
                  }
              }
          }
        if (xref)
            setuses(identifiers);
        identifiers = identifiers->previous;
      }
    if (types->level == level)
      {
        if (xref)
          {
            foreach(types, level, fielduses, NULL);
            setuses(types);
          }
        types = types->previous;
      }
    assert(level >= GLOBAL);
    --level;
  }

/* fielduses - convert use lists for fields in type p */
void fielduses(Symbol p, Generic)
  {
    if ((p->type != NULL) && isstruct_or_union(p->type))
      {
        struct_type *the_struct = (struct_type *)(p->type->unqual());
        Table field_table = get_field_table(the_struct);
        if (field_table != NULL)
            setuses(field_table);
      }
  }

/* findtype - find type ty in identifiers */
Symbol findtype(type_node *ty)
  {
    Table tp = identifiers;
    int i;
    struct entry *p;

    assert(tp != NULL);
    do
      {
        for (i = 0; i < HASHSIZE; i++)
          {
            for (p = tp->buckets[i]; p != NULL; p = p->link)
              {
                if (p->sym.type == ty && p->sym.sclass == TYPEDEF)
                    return &p->sym;
              }
          }
      } while ((tp = tp->previous) != NULL);
    return NULL;
  }

/* foreach - call f(p) for each entry p in table tp */
void foreach(Table tp, int lev, void (*apply)(Symbol, Generic), Generic cl)
  {
    assert(tp != NULL);
    while ((tp != NULL) && (tp->level > lev))
        tp = tp->previous;
    if ((tp != NULL) && (tp->level == lev))
      {
        Symbol p;
        Coordinate sav;
        sav = src;
        for (p = tp->list; (p != NULL) && (p->scope == lev); p = p->up)
          {
            src = p->src;
            (*apply)(p, cl);
          }
        src = sav;
      }
  }

/* genlabel - generate a system generated label */
label_sym *genlabel(void)
  {
    label_sym *result = curr_proc->block()->proc_syms()->new_unique_label("L");
    result->reset_userdef();
    return result;
  }

/* gen_user_label - generate a user generated label */
label_sym *gen_user_label(char *name)
  {
    proc_symtab *the_proc_symtab = curr_proc->block()->proc_syms();

    /* if the label already exists and isn't user-defined, rename the
       existing label */
    label_sym *old_label = the_proc_symtab->lookup_label(name, FALSE);
    if ((old_label != NULL) && (!old_label->is_userdef()))
      {
        label_sym *temp_label = the_proc_symtab->new_unique_label("L");
        old_label->set_name(temp_label->name());
        the_proc_symtab->remove_sym(temp_label);
        delete temp_label;
      }

    label_sym *result = the_proc_symtab->new_label(name);
    result->set_userdef();
    return result;
  }

const char *gen_internal_name(void)
  {
    char num_buffer[20];
    static int name_counter = 1;

    sprintf(num_buffer, "%d", name_counter);
    ++name_counter;
    return lexicon->enter(num_buffer)->sp;
  }

/* install - install name in table *tp; permanently allocate entry iff
   perm != 0 */
Symbol install(const char *name, Table *tpp, int perm)
  {
    struct entry *p;
    unsigned h = (unsigned long)name & (HASHSIZE - 1);

    if (((tpp == &identifiers) || (tpp == &types)) && ((*tpp)->level < level))
        *tpp = table(*tpp, level);
    if (perm)
        p = (struct entry *)alloc(sizeof *p);
    else
        p = (struct entry *)talloc(sizeof *p);
    p->sym.name = name;
    p->sym.scope = (*tpp)->level;
    p->sym.up = (*tpp)->list;
    p->sym.suif_symbol = NULL;
    p->sym.sclass = 0;
    p->sym.defined = 0;
    p->sym.temporary = 0;
    p->sym.computed = 0;
    p->sym.initialized = 0;
    p->sym.ref = 0;
    p->sym.type = NULL;
    p->sym.src.file = NULL;
    p->sym.src.x = 0;
    p->sym.src.y = 0;
    p->sym.uses = NULL;
    (*tpp)->list = &p->sym;
    p->link = (*tpp)->buckets[h];
    (*tpp)->buckets[h] = p;
    p->refs = 0;
    return &p->sym;
  }

/* lookup - lookup name in table tp, return pointer to entry */
Symbol lookup(const char *name, Table tp)
  {
    struct entry *p;
    unsigned h = (unsigned long)name & (HASHSIZE - 1);

    assert(tp);
    do
      {
        for (p = tp->buckets[h]; p != NULL; p = p->link)
          {
            if (name == p->sym.name)
                return &p->sym;
          }
      } while ((tp = tp->previous));
    return NULL;
  }

/* setuses - convert p->refs to p->uses for all p at the current level in *tp
 */
void setuses(Table tp)
  {
    if (xref)
      {
        int i;
        struct entry *p;
        for (i = 0; i < HASHSIZE; i++)
          {
            for (p = tp->buckets[i]; p != NULL; p = p->link)
              {
                if (p->refs)
                    p->sym.uses = (Coordinate **)ltoa(p->refs, 0);
                p->refs = 0;
              }
          }
      }
  }

/* table - create a new table with predecessor tp, scope lev */
Table table(Table tp, int lev)
  {
    int i;
    Table new_table = (Table)talloc(sizeof *new_table);

    assert((lev > GLOBAL) || (lev == LABELS));
    new_table->previous = tp;
    new_table->level = lev;
    new_table->list = ((tp != NULL) ? tp->list : NULL);
    for (i = 0; i < HASHSIZE; i++)
        new_table->buckets[i] = 0;
    return new_table;
  }

/* use - add src to the list of uses for p */
void use(Symbol p, Coordinate src)
  {
    if (xref)
      {
        Coordinate *cp = (Coordinate *)alloc(sizeof *cp);
        *cp = src;
        ((struct entry *)p)->refs =
                append((Generic)cp, ((struct entry *)p)->refs);
      }
  }

extern void annotate_with_refs(Symbol the_sym)
  {
    assert(xref);
    assert(the_sym->suif_symbol != NULL);

    struct entry *the_entry = (struct entry *)the_sym;
    List ref_list = the_entry->refs;
    if (ref_list == NULL)
        return;

    immed_list *the_immeds = new immed_list;
    while (ref_list != NULL)
      {
        Coordinate *this_coordinate = (Coordinate *)ref_list->x;
        the_immeds->append(immed(this_coordinate->file));
        the_immeds->append(immed((int)this_coordinate->x));
        the_immeds->append(immed((int)this_coordinate->y));
        ref_list = ref_list->link;
      }

    the_sym->suif_symbol->append_annote(k_source_references, the_immeds);
  }
