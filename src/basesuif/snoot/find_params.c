/* file "find_params.c" of the snoot program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

/* #include <suif_copyright.h> */

/*
 *  This file contains a routine to figure out some parameters of the
 *  compiler on which it was compiled.  This is compiled and linked
 *  with snoot so that snoot can figure out the parameters to use by
 *  default.
 *
 *  This file can also be used to compile a stand-alone program that
 *  will print out the parameters of the compiler on which it was
 *  compiled.  The information is written out in the form needed in
 *  snoot's "config.h" file.  Then this output can be included in
 *  "config.h" to create a new cross-compilation target.
 *
 *  To create this stand-alone program, compile with
 *  ``cc -DSTAND_ALONE'' on the target machine.  If you want to use
 *  the non-standard extension to ANSI-C that supports the
 *  ``long long'' integral type, and the target machine's compiler
 *  supports this type, compile with the flag ``-DENABLE_LONGLONG''.
 */

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>

#ifdef TRUE
#undef TRUE
#endif

#ifdef FALSE
#undef FALSE
#endif

typedef enum {FALSE = 0, TRUE = 1} boolean;

#include "target_info.h"

#define get_int_size(type) \
      { \
        unsigned type tester; \
        tester = 0; \
        tester = ~tester; \
        counter = 1; \
        while (tester != 0 && counter != 0) \
          { \
            ++counter; \
            tester >>= 1; \
          } \
        if (counter == 0) \
          { \
            fprintf(stderr, "ERROR: overflow in counter\n"); \
            exit(1); \
          } \
        --counter; \
        if (addressable_size != 0 && \
            counter != addressable_size * sizeof(type)) \
          { \
            fprintf(stderr, \
                    "ERROR: size of %s from shift (%lu) mismatches size " \
                    "from sizeof(%s) (%lu)\n", #type, counter, #type, \
                    addressable_size * sizeof(type)); \
            exit(1); \
          } \
      }

#define set_int_size(type, index) \
    get_int_size(type); \
    info_block->size[index] = counter; \
    if (info_block->size[index] < 0 || info_block->size[index] != counter) \
      { \
        fprintf(stderr, "ERROR: size of %s too large to fit in an int\n", \
                #type); \
        exit(1); \
      }

#define set_other_size(type, index) \
    counter = addressable_size * sizeof(type); \
    info_block->size[index] = counter; \
    if (info_block->size[index] < 0 || info_block->size[index] != \
        counter || counter / addressable_size != sizeof(type)) \
      { \
        fprintf(stderr, "ERROR: size of %s too large to fit in an int\n", \
                #type); \
        exit(1); \
      }

#define get_align(type_b, type_name) \
      { \
        struct \
          { \
            char a; \
            type_b; \
          } test_struct; \
        counter = (unsigned long)(((char *)&(test_struct.b)) - \
                                  ((char *)&test_struct)); \
        int_counter = counter * addressable_size; \
        if (int_counter < 0 || int_counter / addressable_size != counter) \
          { \
            fprintf(stderr, \
                    "ERROR: alignment of %s too large to fit in an int\n", \
                    #type_name); \
            exit(1); \
          } \
      }

#define set_align(type, index) \
    get_align(type b, type); \
    info_block->align[index] = int_counter;

static union
  {
    int a;
    struct
      {
        int c : 1;
        int d : 1;
      } b;
  } test_bits;

extern void get_info(target_info_block *info_block)
  {
    unsigned long counter;
    unsigned long sizeof_long;
    unsigned long addressable_size;
    int int_counter;
    char a_char;
    char b_char;

    addressable_size = 0;

    test_bits.b.c = 1;
    info_block->is_big_endian = (test_bits.a != 1);

    get_int_size(long);
    sizeof_long = sizeof(long);
    if (sizeof_long == 0)
      {
        fprintf(stderr, "ERROR: sizeof(long) == 0\n");
        exit(1);
      }
    if (counter == 0)
      {
        fprintf(stderr, "ERROR: size from shifting long is zero\n");
        exit(1);
      }
    if (counter % sizeof_long != 0)
      {
        fprintf(stderr,
                "ERROR: size from shifting long is not divisible by "
                "sizeof(long)\n");
        exit(1);
      }
    addressable_size = counter/sizeof_long;
    info_block->addressable_size = addressable_size;
    if (info_block->addressable_size < 0 ||
        addressable_size != info_block->addressable_size)
      {
        fprintf(stderr, "ERROR: addresable size too large to fit in an int\n");
        exit(1);
      }

    a_char = 1;
    a_char -= 2;
    b_char = 1;
    info_block->char_is_signed = (a_char < b_char);

    set_int_size(char, C_char);
    set_int_size(short, C_short);
    set_int_size(int, C_int);
    set_int_size(long, C_long);

    set_other_size(float, C_float);
    set_other_size(double, C_double);
    set_other_size(long double, C_longdouble);
    set_other_size(void *, C_ptr);

    set_align(char, C_char);
    set_align(short, C_short);
    set_align(int, C_int);
    set_align(long, C_long);
    set_align(float, C_float);
    set_align(double, C_double);
    set_align(long double, C_longdouble);
    set_align(void *, C_ptr);

#ifdef ENABLE_LONGLONG
    set_int_size(long long, C_longlong);
    set_align(long long, C_longlong);
#else
    info_block->size[C_longlong] = info_block->size[C_long];
    info_block->align[C_longlong] = info_block->align[C_long];
#endif

    get_align(char b[2], array);
    info_block->array_align = int_counter;

    get_align(struct {char x;} b, struct);
    info_block->struct_align = int_counter;

    if (sizeof(ptrdiff_t) == sizeof(int))
      {
        info_block->ptr_diff_type = C_int;
      }
    else if (sizeof(ptrdiff_t) == sizeof(short))
      {
        info_block->ptr_diff_type = C_short;
      }
    else if (sizeof(ptrdiff_t) == sizeof(char))
      {
        info_block->ptr_diff_type = C_char;
      }
    else if (sizeof(ptrdiff_t) == sizeof(long))
      {
        info_block->ptr_diff_type = C_long;
      }
#ifdef ENABLE_LONGLONG
    else if (sizeof(ptrdiff_t) == sizeof(long long))
      {
        info_block->ptr_diff_type = C_longlong;
      }
#endif
    else
      {
        fprintf(stderr,
                "ERROR: ptrdiff_t does not match any integral types\n");
        exit(1);
      }
  }

#ifdef STAND_ALONE
#define show_size_and_align(name) \
    printf("       TYPE_SIZE  (%s, %2d)\n", #name, \
           info_block.size[C_ ## name]); \
    printf("      TYPE_ALIGN  (%s, %2d)\n\n", #name, \
           info_block.align[C_ ## name]);

int main(int argc, char *argv[])
  {
    static char *name_table[] =
      {
        "C_char",
        "C_short",
        "C_int",
        "C_long",
        "C_longlong",
        "C_float",
        "C_double",
        "C_longdouble",
        "C_ptr",
        NULL
      };

    target_info_block info_block;

    get_info(&info_block);

    printf("   IS_BIG_ENDIAN  (%s)\n\n",
           info_block.is_big_endian ? "TRUE" : "FALSE");

    printf("ADDRESSABLE_SIZE  (%2d)\n", info_block.addressable_size);

    printf("  CHAR_IS_SIGNED  (%s)\n\n",
           info_block.char_is_signed ? "TRUE" : "FALSE");

    show_size_and_align(char);
    show_size_and_align(short);
    show_size_and_align(int);
    show_size_and_align(long);
    show_size_and_align(longlong);
    show_size_and_align(float);
    show_size_and_align(double);
    show_size_and_align(longdouble);
    show_size_and_align(ptr);

    printf("%10s_ALIGN  (%2d)\n\n", "ARRAY", info_block.array_align);
    printf("%10s_ALIGN  (%2d)\n\n", "STRUCT", info_block.struct_align);

    printf("   PTR_DIFF_TYPE  (%s)\n\n", name_table[info_block.ptr_diff_type]);
  }

#endif /* def STAND_ALONE */
