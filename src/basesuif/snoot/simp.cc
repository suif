/* file "simp.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot constant folding */

#include "c.h"

boolean needconst = FALSE;

/* simplify_convert - build node for convert, simplifying and folding
   constants, if possible */
genop simplify_convert(type_node *new_type, genop to_convert)
  {
    if (new_type == to_convert.type())
        return to_convert;

    if (to_convert.suif_operand().is_expr() && new_type->unqual()->is_ptr() &&
        to_convert.type()->unqual()->is_ptr())
      {
        instruction *the_instr = to_convert.suif_operand().instr();
        assert(the_instr != NULL);
        if (the_instr->opcode() == io_cvt)
          {
            the_instr->set_result_type(new_type);
            return to_convert;
          }
      }

    if (to_convert.suif_operand().is_expr() && !to_convert.type()->is_enum())
      {
        instruction *the_instr = to_convert.suif_operand().instr();
        assert(the_instr != NULL);
        if (the_instr->opcode() == io_ldc)
          {
            in_ldc *the_ldc = (in_ldc *)the_instr;
            immed value = the_ldc->value();
            if ((value.is_int_const() &&
                 (new_type->unqual()->op() == TYPE_INT)) ||
                (value.is_float_const() &&
                 (new_type->unqual()->op() == TYPE_FLOAT)))
              {
                operand new_op =
                        fold_real_1op_rrr(io_cvt, new_type,
                                          operand(the_instr));
                return genop(new_op, to_convert.precomputation());
              }
          }
      }

    in_rrr *the_cvt = new in_rrr(io_cvt, new_type, operand(),
                                 to_convert.suif_operand(), operand());
    return genop(operand(the_cvt), to_convert.precomputation());
  }
