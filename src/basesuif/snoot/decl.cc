/* file "decl.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot declaration parsing */

#include "c.h"

Symbol cfunc = NULL;            /* current function */
char *fname = NULL;             /* current function name */

static boolean funcdclr;        /* declarator has parameters */
static int nglobals;            /* number of external ids */

static void checklab(Symbol p, Generic);
static void checkparam(Symbol p, Generic);
static void checkref(Symbol p, Generic);
static Symbol dclglobal(int sclass, char *id, type_node *ty, Coordinate *pos);
static Symbol dcllocal(int sclass, char *id, type_node *ty, Coordinate *pos);
static Symbol dclparam(int sclass, char *id, type_node *ty, Coordinate *pos);
static type_node *dclr(type_node *basety, char **id, int lev);
static type_node *dclr1(char **id, int lev);
static void decl(Symbol (*dcl)(int, char *, type_node *, Coordinate *),
                 boolean eflag);
static void doglobal(Symbol p, Generic);
static void doextern(Symbol p, Generic);
static enum_type *enumdecl(void);
static void fields(struct_type *to_fill);
static void funcdecl(int sclass, char *id, func_type *ftype, Coordinate pt);
static func_type *newstyle(func_type *ty, type_node *caller_types[],
                           Symbol callee[]);
static void oldparam(Symbol p, Generic cl);
static func_type *oldstyle(char *name, func_type *ty,
                           type_node *caller_types[], Symbol callee[]);
static type_node **parameters(int lev);
static struct_type *structdcl(int op);
static type_node *parse_type(int lev, int *sclass);
static boolean control_reaches_list_end(tree_node_list *node_list);

/* checklab - check for undefined labels; called at ends of functions */
static void checklab(Symbol p, Generic)
  {
    if (!p->defined)
        error("undefined label `%s'\n", p->name);
    p->defined = 1;
  }

/* checkref - check for unreferenced variables; called at ends of blocks */
static void checkref(Symbol p, Generic)
  {
    if ((Aflag >= 2) && p->defined && (p->ref == 0))
      {
        if (p->sclass == STATIC)
            warning("static `%t %s' is not referenced\n", p->type, p->name);
        else if (p->scope == PARAM)
            warning("parameter `%t %s' is not referenced\n", p->type, p->name);
        else if ((p->scope > PARAM) && (p->sclass != EXTERN))
            warning("local `%t %s' is not referenced\n", p->type, p->name);
      }

    Symbol q;
    if ((p->scope > PARAM) && (q = lookup(p->name, externals)))
      {
        q->ref += p->ref;
      }
    else if ((p->sclass == STATIC) && !p->defined)
      {
        if (p->ref > 0)
          {
            error("undefined static `%t %s'\n", p->type, p->name);
          }
        else if (isfunc(p->type))
          {
            warning("undefined static `%t %s'\n", p->type, p->name);

            /*
             *  In SUIF it is illegal to have a procedure in the file symbol
             *  table without a body, so we must remove this symbol.
             */
            sym_node *the_symbol = p->suif_symbol;
            assert(the_symbol != NULL);
            the_symbol->parent()->remove_sym(the_symbol);
            delete the_symbol;
            p->suif_symbol = NULL;
          }
      }

    if (xref)
        annotate_with_refs(p);
  }

/* compound - { ( decl ; )* statement* } */
void compound(label_sym *continue_lab, label_sym *break_lab,
              struct swtch *swp, int lev)
  {
    enterscope();
    ++bnumber;
    stabblock('{');
    expect('{');

    while ((kind[t] == CHAR) || (kind[t] == STATIC) ||
           ((t == ID) && (tsym != NULL) && (tsym->sclass == TYPEDEF) &&
            ((level < LOCAL) || (getchr() != ':'))))
      {
        decl(dcllocal, FALSE);
      }

    while ((kind[t] == IF) || (kind[t] == ID))
        statement(continue_lab, break_lab, swp, lev);

    foreach(identifiers, level, checkref, NULL);

    stabblock('}');

    if (level > LOCAL)
      {
        exitscope();
        expect('}');
      }
  }

/* dclglobal - called from decl to declare a global */
static Symbol dclglobal(int sclass, char *id, type_node *ty, Coordinate *pos)
  {
    if ((sclass == 0) || (sclass == REGISTER))
        sclass = AUTO;
    Symbol p = lookup(id, identifiers);
    if ((p != NULL) && (p->scope == GLOBAL))
      {
        if ((p->sclass != TYPEDEF) && eqtype(ty, p->type, TRUE))
          {
            ty = composite(ty, p->type);
          }
        else
          {
            error("redeclaration of `%s' previously declared at %w\n",
                  p->name, &p->src);
          }
        if (!isfunc(ty) && p->defined && (t == '='))
          {
            error("redefinition of `%s' previously defined at %w\n", p->name,
                  &p->src);
          }
        if (((p->sclass == STATIC) && (sclass == AUTO)) ||
            ((p->sclass != STATIC) && (sclass == STATIC)))
          {
            warning("inconsistent linkage for `%s' previously declared at"
                    " %w\n", p->name, &p->src);
          }
      }
    else if ((sclass == STATIC) && !isfunc(ty) && (ty->size() == 0) &&
             (t != '='))
      {
        warning("undefined size for static `%t %s'\n", ty, id);
      }
    if ((p == NULL) || (p->scope != GLOBAL))
        p = install(id, &globals, 1);
    p->type = ty;
    Symbol q = lookup(p->name, externals);
    if (q != NULL)
      {
        p->ref += q->ref;
        p->suif_symbol = q->suif_symbol;
      }
    if (p->suif_symbol != NULL)
      {
        if (isfunc(ty))
          {
            assert(p->suif_symbol->is_proc());
            func_type *the_func_type = (func_type *)(ty->unqual());
            proc_sym *the_proc = (proc_sym *)(p->suif_symbol);
            boolean vis = the_proc->parent()->make_type_visible(the_func_type);
            assert(vis);
            the_proc->set_type(the_func_type);
          }
        else
          {
            assert(p->suif_symbol->is_var());
            var_sym *the_var = (var_sym *)(p->suif_symbol);
            boolean vis = the_var->parent()->make_type_visible(p->type);
            assert(vis);
            the_var->set_type(p->type);
          }
      }
    if ((p->sclass == 0) || ((sclass != EXTERN) && (p->sclass != STATIC)))
      {
        p->sclass = sclass;
      }
    if (p->sclass != STATIC)
      {
        nglobals++;
        if ((Aflag >= 2) && (nglobals == 512))
            warning("more than 511 external identifiers\n");
      }
    p->src = *pos;
    if ((!p->defined) && (p->suif_symbol == NULL))
        defsymbol(p);

    if (q != NULL)
      {
        if ((((p->sclass == AUTO) ? (unsigned char)EXTERN : p->sclass) !=
             q->sclass) || !eqtype(p->type, q->type, TRUE))
          {
            warning("declaration of `%s' does not match previous declaration"
                    " at %w\n", p->name, &q->src);
          }
      }
    if (!isfunc(p->type))
      {
        initglobal(p, FALSE);
      }
    else if (t == '=')
      {
        error("illegal initialization for `%s'\n", p->name);
        t = gettok();
        initializer(NULL, p->type, 0);
      }
    return p;
  }

/* dcllocal - called from decl to declare a local */
static Symbol dcllocal(int sclass, char *id, type_node *ty, Coordinate *pos)
  {
    if (isfunc(ty))
      {
        if ((sclass != 0) && (sclass != EXTERN))
            error("invalid storage class `%k' for `%t %s'\n", sclass, ty, id);
        sclass = EXTERN;
      }
    else if (sclass == 0)
      {
        sclass = AUTO;
      }
    if ((sclass == REGISTER) &&
        (isvolatile(ty) || isstruct_or_union(ty) || isarray(ty)))
      {
        warning("register declaration ignored for `%t %s'\n", ty, id);
        sclass = AUTO;
      }

    Symbol q = lookup(id, identifiers);
    if ((q != NULL) &&
        ((q->scope >= level) || ((q->scope == PARAM) && (level == PARAM+1))))
      {
        if ((sclass == EXTERN) && (q->sclass == EXTERN) &&
            eqtype(q->type, ty, TRUE))
          {
            ty = composite(ty, q->type);
          }
        else
          {
            error("redeclaration of `%s' previously declared at %w\n",
                  q->name, &q->src);
          }
      }
    assert(level >= LOCAL);

    Symbol p = install(id, &identifiers, 0);
    p->type = ty;
    p->sclass = sclass;
    p->src = *pos;
    switch (sclass)
      {
        case EXTERN:
          {
            if ((q != NULL) && (q->scope == GLOBAL) && (q->sclass == STATIC))
              {
                p->sclass = STATIC;
                p->scope = level;
              }
            defsymbol(p);
            Symbol r = lookup(id, externals);
            if (r == NULL)
              {
                r = install(p->name, &externals, 1);
                r->src = p->src;
                r->type = p->type;
                r->sclass = p->sclass;
                r->suif_symbol = p->suif_symbol;
                q = lookup(id, globals);
                if ((q != NULL) && (q->sclass != TYPEDEF) &&
                    (q->sclass != ENUM))
                  {
                    r = q;
                  }
              }
            if ((r != NULL) &&
                ((((r->sclass == AUTO) ? (unsigned char)EXTERN : r->sclass) !=
                  p->sclass) || !eqtype(r->type, p->type, TRUE)))
              {
                warning("declaration of `%s' does not match previous "
                        "declaration at %w\n", r->name, &r->src);
              }
            break;
          }
        case STATIC:
            defsymbol(p);
            initglobal(p, FALSE);
            if (!p->defined)
              {
                if (p->type->size() > 0)
                    (void)defglobal(p);
                else
                    error("undefined size for `%t %s'\n", p->type, p->name);
              }
            p->defined = 1;
            break;
        case REGISTER:
        case AUTO:
          {
            var_sym *the_var_sym =
                    get_current_symtab()->new_var(p->type, p->name);
            if (sclass == REGISTER)
                the_var_sym->append_annote(k_is_declared_reg, new immed_list);
            the_var_sym->set_userdef();
            p->suif_symbol = the_var_sym;

            p->defined = 1;
            break;
          }
        default:
            assert(FALSE);
      }
    if (t == '=')
      {
        if (sclass == EXTERN)
            error("illegal initialization of `extern %s'\n", id);
        t = gettok();
        definept(NULL);
        genop e;
        if (isscalar(p->type) || (isstruct_or_union(p->type) && (t != '{')))
          {
            if (t == '{')
              {
                t = gettok();
                e = constexpr(0);
                (void)genconst(NULL, e, FALSE);
                expect('}');
              }
            else
              {
                e = expr1(0);
              }
            e = pointer(e);
            type_node *new_type = assign(p->type, e);
            if (new_type != NULL)
              {
                e = cast(e, new_type);
              }
            else
              {
                error("invalid initialization type; found `%t' expected "
                      "`%t'\n", e.type(), p->type);
              }
          }
        else
          {
            type_node *ty = p->type;
            boolean constified = FALSE;
            if (!isconst(ty) &&
                (!isarray(ty) || !isconst(base_from_array(ty))))
              {
                ty = qual(CONST, ty);
                constified = TRUE;
              }

            var_sym *new_var = gen_static_global(ty);
            var_def *the_def = begin_initialization(new_var);
            ty = initializer(the_def, new_var->type(), 0);
            if (isarray(new_var->type()) && new_var->type()->size() == 0)
                new_var->set_type(ty);

            if (isarray(p->type) && (p->type->size() == 0) &&
                (new_var->type()->size() > 0))
              {
                int new_size =
                        new_var->type()->size() /
                        base_from_array(new_var->type())->size();
                assert(p->suif_symbol != NULL);
                assert(p->suif_symbol->is_var());
                var_sym *p_var = (var_sym *)(p->suif_symbol);
                p_var->set_type(build_array(base_from_array(p->type),
                                            new_size));
                p->type = p_var->type();
              }
            /* If it's an array and we changed the type to be an array
             * of constant elements, we can't do a simple copy because
             * the types will conflict, so we do an explicit load in
             * that case. */
            if (isarray(ty) && constified)
              {
                operand address_op =
                        operand(new in_ldc(ty->ptr_to(), operand(),
                                           immed(new_var)));
                operand convert_op =
                        operand(new in_rrr(io_cvt, p->type->ptr_to(),
                                           operand(), address_op));
                in_rrr *new_load =
                        new in_rrr(io_lod, p->type, operand(), convert_op);
                e = genop(operand(new_load), new tree_node_list);
              }
            else
              {
                e = sym_node_to_genop(new_var);
              }
          }

        e.clean_up_bit_field_refs();
        tree_node_list *precomputation = e.precomputation();
        operand r_op = e.suif_operand();

        curr_list->append(precomputation);
        delete precomputation;

        assert(p->suif_symbol->is_var());
        tree_node *new_assignment =
                create_assignment(((var_sym *)p->suif_symbol), r_op);
        curr_list->append(new_assignment);

        p->initialized = 1;
        p->ref = 0;
      }
    if (!isfunc(p->type) && p->defined && (p->type->size() <= 0))
        error("undefined size for `%t %s'\n", p->type, id);
    return p;
  }

/* dclparam - called from decl to declare a parameter */
static Symbol dclparam(int sclass, char *id, type_node *ty, Coordinate *pos)
  {
    Symbol p = lookup(id, identifiers);
    if ((p != NULL) && (p->scope == level) &&
        (p->defined || ((p->type == NULL) && (ty == NULL))))
      {
        error("duplicate definition for `%s' previously declared at %w\n", id,
              &p->src);
      }
    else if ((p == NULL) || (p->scope < level))
      {
        p = install(id, &identifiers, 0);
      }
    if ((ty != NULL) && isfunc(ty))
        ty = ty->ptr_to();
    else if ((ty != NULL) && isarray(ty))
        ty = atop(ty);
    if (sclass == 0)
        sclass = AUTO;
    if ((sclass == REGISTER) && (ty != NULL) &&
        (isvolatile(ty) || isstruct_or_union(ty)))
      {
        warning("register declaration ignored for `%t%s\n", ty,
                stringf(id ? " %s'" : "' parameter", id));
        sclass = AUTO;
      }
    p->sclass = sclass;
    p->src = *pos;
    p->type = ty;
    if (ty != NULL)
        p->defined = 1;
    if (t == '=')
      {
        error("illegal initialization for parameter `%s'\n", id);
        t = gettok();
        genop ignored = expr1(0);
        ignored.deallocate();
      }
    return p;
  }

/* dclr - declarator */
static type_node *dclr(type_node *basety, char **id, int lev)
  {
    type_node *ty = dclr1(id, lev);

    while (ty != voidtype)
      {
        assert(ty != NULL);
        switch (ty->op())
          {
            case TYPE_PTR:
              {
                ptr_type *the_ptr_type = (ptr_type *)ty;
                assert(the_ptr_type->parent() == NULL);
                ty = the_ptr_type->ref_type();
                the_ptr_type->set_ref_type(basety);
                basety = basety->parent()->install_type(the_ptr_type);
                break;
              }
            case TYPE_FUNC:
              {
                func_type *the_func_type = (func_type *)ty;
                assert(the_func_type->parent() == NULL);
                ty = the_func_type->return_type();
                the_func_type->set_return_type(basety);
                base_symtab *target_symtab = basety->parent();
                unsigned num_args = the_func_type->num_args();
                for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                  {
                    base_symtab *this_scope =
                            the_func_type->arg_type(arg_num)->parent();
                    target_symtab = joint_symtab(target_symtab, this_scope);
                    assert(target_symtab != NULL);
                  }
                basety = target_symtab->install_type(the_func_type);
                break;
              }
            case TYPE_ARRAY:
              {
                array_type *the_array_type = (array_type *)ty;
                assert(the_array_type->parent() == NULL);
                ty = the_array_type->elem_type();
                the_array_type->set_elem_type(basety);
                basety = basety->parent()->install_type(the_array_type);
                break;
              }
            case TYPE_CONST:
            case TYPE_VOLATILE:
              {
                modifier_type *the_mod_type = (modifier_type *)ty;
                assert(the_mod_type->parent() == NULL);
                ty = the_mod_type->base();
                the_mod_type->set_base(basety);
                basety = basety->parent()->install_type(the_mod_type);
                break;
              }
            default:
                assert(FALSE);
          }
      }
    if ((Aflag >= 2) && (basety->size() > 32767))
        warning("more than 32767 bytes in `%t'\n", basety);
    return basety;
  }

/* dclr1 -
     ( id |  * ( const | volatile )* | '(' dclr1 ')' ) ( (...) | [...] )*
*/
static type_node *dclr1(char **id, int lev)
  {
    type_node *ty = voidtype;

    switch (t)
      {
        case ID:
            if (id != NULL)
                *id = token;
            else
                error("extraneous identifier `%s'\n", token);
            t = gettok();
            break;
        case '*':
            t = gettok();
            if ((t == CONST) || (t == VOLATILE))
              {
                modifier_type *ty1;
                if (t == CONST)
                    ty1 = new modifier_type(TYPE_CONST, voidtype);
                else
                    ty1 = new modifier_type(TYPE_VOLATILE, voidtype);
                ty = ty1;
                while (((t = gettok()) == CONST) || (t == VOLATILE))
                  {
                    if (t == CONST)
                        ty = new modifier_type(TYPE_CONST, ty);
                    else
                        ty = new modifier_type(TYPE_VOLATILE, ty);
                  }
                ty1->set_base(dclr1(id, lev));
              }
            else
              {
                ty = dclr1(id, lev);
              }
            ty = new ptr_type(ty);
            break;
        case '(':
            t = gettok();
            if ((kind[t] == CHAR) ||
                ((t == ID) && (tsym != NULL) && (tsym->sclass == TYPEDEF)))
              {
                type_node **params = parameters(lev);
                ty = create_function_type(ty, params);
                foreach(identifiers, level, checkparam, NULL);
                exitscope();
              }
            else
              {
                ty = dclr1(id, lev);
                expect(')');
                if ((ty == voidtype) && (id == NULL))
                    return create_function_type(ty, NULL);
              }
            break;
        case '[':
            break;
        default:
            return ty;
      }
    while ((t == '(') || (t == '['))
      {
        if (t == '(')
          {
            t = gettok();
            type_node **params = parameters(lev);
            ty = create_function_type(ty, params);
            if ((lev == 0) && (!funcdclr))
              {
                funcdclr = TRUE;
              }
            else
              {
                foreach(identifiers, level, checkparam, NULL);
                exitscope();
              }
          }
        else
          {
            t = gettok();
            array_type *the_array = new array_type(ty, array_bound(0));
            if (kind[t] == ID)
              {
                i_integer n = intexpr(']', 1);
                if (n <= 0)
                  {
                    error("`%D' is an illegal array size\n", &n);
                    n = 1;
                  }
                n -= 1;
                if (!n.is_c_int())
                  {
                    n += 1;
                    error("`%D' is too big an array size\n", &n);
                    n = 0;
                  }
                the_array->set_upper_bound(array_bound(n.c_int()));
              }
            else
              {
                expect(']');
                the_array->set_upper_bound(unknown_bound);
              }
            ty = the_array;
          }
      }
    return ty;
  }

/* decl - type [ dclr ( , dclr )* ] ; */
static void decl(Symbol (*dcl)(int, char *, type_node *, Coordinate *),
                 boolean eflag)
  {
    int sclass;
    char *id = NULL;
    type_node *ty1;
    Coordinate pt;
    static char follow[] = { CHAR, STATIC, ID, 0 };

    pt = src;
    type_node *ty = parse_type(level, &sclass);
    if ((t == ID) || (t == '*') || (t == '(') || (t == '['))
      {
        Coordinate pos;
        pos = src;
        funcdclr = FALSE;
        if (level == GLOBAL)
          {
            ty1 = dclr(ty, &id, 0);
            if (funcdclr && (id != NULL) && isfunc(ty1) &&
                ((t == '{') || (kind[t] == CHAR) ||
                 ((kind[t] == STATIC) && (t != TYPEDEF)) ||
                 ((t == ID) && (tsym != NULL) && (tsym->sclass == TYPEDEF))))
              {
                if (sclass == TYPEDEF)
                  {
                    error("invalid use of `typedef'\n");
                    sclass = EXTERN;
                  }
                fname = id;
                funcdecl(sclass, fname, (func_type *)ty1, pt);
                fname = NULL;
                return;
              }
            else if (funcdclr)
              {
                foreach(identifiers, level, checkparam, NULL);
                exitscope();
              }
          }
        else
          {
            ty1 = dclr(ty, &id, 1);
          }
        for (;;)
          {
            if ((Aflag >= 1) && !hasproto(ty1))
                warning("missing prototype\n");
            if (id == NULL)
                error("missing identifier\n");
            else if (sclass == TYPEDEF)
                (void)deftype(id, ty1, &pos);
            else
                (*dcl)(sclass, id, ty1, &pos);
            if (t != ',')
                break;
            t = gettok();
            id = NULL;
            pos = src;
            ty1 = dclr(ty, &id, 1);
          }
      }
    else if ((ty != NULL) && eflag)
      {
        error("empty declaration\n");
      }
    else if ((ty != NULL) &&
             (sclass || (!isstruct_or_union(ty) && !isenum(ty))))
      {
        warning("empty declaration\n");
      }
    test(';', follow);
  }

/* doextern - import external declared in a block, if necessary, propagate
   flags */
static void doextern(Symbol p, Generic)
  {
    Symbol q = lookup(p->name, identifiers);
    if (q != NULL)
        q->ref += p->ref;
    else
        defsymbol(p);
  }

/* doglobal - finalize tentative definitions, check for imported symbols */
static void doglobal(Symbol p, Generic)
  {
    if ((p->sclass == TYPEDEF) || (p->sclass == ENUM) || p->defined)
      {
        /* re-define symbols to catch types that have been completed */
        if ((p->sclass == AUTO) || (p->sclass == STATIC))
            defsymbol(p);
        if (Pflag && !isfunc(p->type))
            printdecl(p, p->type);
        return;
      }
    if ((p->sclass != EXTERN) && !isfunc(p->type))
      {
        if (p->type->size() > 0)
            (void)defglobal(p);
        else
            error("undefined size for `%t %s'\n", p->type, p->name);
        p->defined = 1;
        if (Pflag)
            printdecl(p, p->type);
      }
  }

/* enumdecl - enum [ id ] [ { id [ = cexpr ] ( , id [ = cexpr ] )* } ] */
static enum_type *enumdecl(void)
  {
    char *tag;
    enum_type *result;
    Symbol p;
    Coordinate pos;

    t = gettok();
    if (t == ID)
      {
        tag = token;
        pos = src;
        t = gettok();
      }
    else
      {
        tag = string("");
      }
    if (t == '{')
      {
        static char follow[] = { IF, 0 };
        int n = 0, min, max, k = -1;
        boolean found_one = FALSE;
        Symbol enum_sym = newstruct(ENUM, tag);
        assert(enum_sym->type->is_enum());
        result = (enum_type *)(enum_sym->type);
        t = gettok();
        if (t != ID)
            error("expecting an enumerator identifier\n");
        while (t == ID)
          {
            char *id = token;
            Coordinate s;
            if ((tsym != NULL) && (tsym->scope == level))
              {
                error("redeclaration of `%s' previously declared at %w\n",
                      token, &tsym->src);
              }
            s = src;
            t = gettok();
            if (t == '=')
              {
                t = gettok();
                i_integer ii_k = intexpr(0, 0);
                if (!ii_k.is_c_int())
                  {
                    error("`%D' is too big for an enumeration constant"
                          " value\n", &ii_k);
                    ii_k = 0;
                  }
                k = ii_k.c_int();
              }
            else
              {
                if (k == INT_MAX)
                  {
                    error("overflow in value for enumeration constant `%s'\n",
                          p->name);
                  }
                k++;
              }
            if (found_one)
              {
                if (k < min)
                    min = k;
                if (k > max)
                    max = k;
              }
            else
              {
                min = max = k;
                found_one = TRUE;
              }
            p = install(id, &identifiers, level < LOCAL);
            p->src = s;
            p->type = result;
            p->sclass = ENUM;
            p->u.enum_value = k;

            result->set_num_values(n + 1);
            result->set_value(n, k);
            result->set_member(n, id);
            n++;
            if ((Aflag >= 2) && (n == 128))
              {
                warning("more than 127 enumeration constants in `%t'\n",
                        result);
              }
            if (t != ',')
                break;
            t = gettok();
            if (t == '}')
                warning("non-ANSI trailing comma in enumerator list\n");
          }
        test('}', follow);

        if (option_force_enum_is_int)
          {
            result->set_signed(TRUE);
            result->set_size(inttype->size());
          }
        else if (immed_fits(immed(min), unsignedchar) &&
                 immed_fits(immed(max), unsignedchar))
          {
            result->set_signed(FALSE);
            result->set_size(unsignedchar->size());
          }
        else if (immed_fits(immed(min), signedchar) &&
                 immed_fits(immed(max), signedchar))
          {
            result->set_signed(TRUE);
            result->set_size(signedchar->size());
          }
        else if (immed_fits(immed(min), unsignedshort) &&
                 immed_fits(immed(max), unsignedshort))
          {
            result->set_signed(FALSE);
            result->set_size(unsignedshort->size());
          }
        else if (immed_fits(immed(min), shorttype) &&
                 immed_fits(immed(max), shorttype))
          {
            result->set_signed(TRUE);
            result->set_size(shorttype->size());
          }
        else
          {
            result->set_signed(TRUE);
            result->set_size(inttype->size());
          }
        enum_sym->defined = 1;
      }
    else if ((p = lookup(tag, types)) && (p->type->op() == TYPE_ENUM))
      {
        if ((*tag != 0) && xref)
            use(p, pos);
        assert(p->type->is_enum());
        result = (enum_type *)(p->type);
      }
    else
      {
        error("unknown enumeration `%s'\n",  tag);
        Symbol enum_sym = newstruct(ENUM, tag);
        assert(enum_sym->type->is_enum());
        result = (enum_type *)(enum_sym->type);
        result->set_signed(TRUE);
        result->set_size(inttype->size());
      }
    return result;
  }

/* fields - ( type dclr ( , dclr )* ; )* */
static void fields(struct_type *to_fill)
  {
    int n = 0, bits, off, overflow = 0;
    Field p;
    struct field *field_list = NULL;

    while ((kind[t] == CHAR) ||
           ((t == ID) && (tsym != NULL) && (tsym->sclass == TYPEDEF)))
      {
        static char follow[] = { IF, CHAR, '}', 0 };
        type_node *ty1 = parse_type(0, NULL);
        do
          {
            char *id = NULL;
            type_node *fty;
            fty = dclr(ty1, &id, 1);
            if (fty == NULL)
                fty = ty1;
            if ((Aflag >= 1) && !hasproto(fty))
                warning("missing prototype\n");
            p = newfield(id, &field_list, fty, to_fill);
            if (t == ':')
              {
                fty = fty->unqual();
                if (isenum(fty))
                    fty = base_from_enum(fty);
                if ((fty != inttype) && (fty != unsignedtype))
                  {
                    error("`%t' is an illegal bit field type\n", p->type);
                    p->type = inttype;
                  }
                t = gettok();
                i_integer p_to = intexpr(0, 0);
                if ((p_to > inttype->size()) || (p_to < 0))
                  {
                    error("`%D' is an illegal bit field size\n", &p_to);
                    p_to = inttype->size();
                  }
                else if ((p_to == 0) && (id != NULL))
                  {
                    warning("extraneous 0-width bit field `%t %s' ignored\n",
                            p->type, id);
                    p->name = gen_internal_name();
                  }
                p->to = p_to.c_int() + 1;
              }
            else if ((id == NULL) && isstruct_or_union(p->type))
              {
                error("unnamed substructure in `%t'\n", to_fill);
                p->name = NULL;
                break;
              }
            else
              {
                if (id == NULL)
                    error("field name missing\n");
                else if (p->type->size() == 0)
                    error("undefined size for field `%t %s'\n", p->type, id);
              }
            n++;
            if ((Aflag >= 2) && (n == 128))
                warning("more than 127 fields in `%t'\n", to_fill);
          } while ((t == ',') && (t = gettok()));
        test(';', follow);
      }

    short resulting_alignment = target.struct_align;
    off = bits = 0;
#define add(x,n) (x > INT_MAX - (n) ? (overflow = 1, x) : x + n)
    struct field *deferred = NULL;
    for (p = field_list; p != NULL;)
      {
        int a = (get_alignment(p->type) ? get_alignment(p->type) :
                target.addressable_size);
        if (to_fill->op() == TYPE_UNION)
          {
            if (p->to)
                a = get_alignment(unsignedtype);
            bits = 0;
          }
        else if ((bits == 0) || (p->to <= 1) ||
                 (bits - 1 + p->to - 1 > unsignedtype->size()))
          {
            if (bits != 0)
                off = add(off, unsignedtype->size());
            if (p->to)
                a = get_alignment(unsignedtype);
            add(off, a - 1);
            off = roundup(off, a);
            bits = 0;
          }
        if (a > resulting_alignment)
            resulting_alignment = a;
        p->offset = off;
        if (p->to)
          {
            if (bits == 0)
                bits = 1;
            p->from = bits - 1;
            p->to = p->from + p->to - 1;
            bits += p->to - p->from;
            if ((to_fill->op() == TYPE_UNION) &&
                (((bits + target.addressable_size - 2) /
                  target.addressable_size) * target.addressable_size >
                 to_fill->size()))
              {
                to_fill->set_size(((bits + target.addressable_size - 2) /
                                   target.addressable_size) *
                                  target.addressable_size);
              }
          }
        else if (to_fill->op() == TYPE_STRUCT)
          {
            off = add(off, p->type->size());
          }
        else if (p->type->size() > to_fill->size())
          {
            to_fill->set_size(p->type->size());
          }
        field_list = p->link;

        if ((deferred != NULL) && (p->from < deferred->to))
          {
            immed_list *descriptor = new immed_list;
            short def_offset = deferred->offset;
            char *name = new char[1];
            name[0] = 0;
            boolean has_constant = FALSE;
            boolean has_volatile = FALSE;

            while (deferred != p)
              {
                assert(deferred->offset == def_offset);

                if ((deferred->name == 0) || (*deferred->name > '9') ||
                    (*deferred->name < '0'))
                  {
                    if (deferred->type->is_volatile())
                      {
                        has_volatile = TRUE;
                        descriptor->append(immed("volatile"));
                      }
                    if (deferred->type->is_const())
                      {
                        has_constant = TRUE;
                        descriptor->append(immed("const"));
                      }
                    deferred->type = deferred->type->unqual();

                    if (deferred->type == inttype)
                        descriptor->append(immed("int"));
                    else if (deferred->type == unsignedtype)
                        descriptor->append(immed("unsigned"));
                    else
                        assert(FALSE);

                    descriptor->append(immed(deferred->name));
                    descriptor->append(immed(deferred->from));
                    descriptor->append(immed(deferred->to));

                    char *new_name = new char[strlen(name) +
                                              strlen(deferred->name) + 2];
                    strcpy(new_name, name);
                    strcat(new_name, "_");
                    strcat(new_name, deferred->name);
                    delete[] name;
                    name = new_name;
                  }

                struct field *next_field = deferred->link;
                delete deferred;
                deferred = next_field;
              }
            deferred = NULL;

            if (*name != 0)
              {
                type_node *field_type = unsignedtype->copy();
                field_type->append_annote(k_bit_field_info, descriptor);
                field_type = to_fill->parent()->install_type(field_type);
                if (has_volatile)
                    field_type = qual(VOLATILE, field_type);
                if (has_constant)
                    field_type = qual(CONST, field_type);

                unsigned next_field = to_fill->num_fields();
                to_fill->set_num_fields(next_field + 1);
                to_fill->set_field_type(next_field, field_type);
                to_fill->set_field_name(next_field, name);
                to_fill->set_offset(next_field, def_offset);
              }
            else
              {
                delete descriptor;
              }
            delete[] name;
          }

        if (p->to != 0)
          {
            if (deferred == NULL)
                deferred = p;
            assert(deferred->offset == p->offset);
          }
        else
          {
            assert(deferred == NULL);
            if ((p->name == 0) || (*p->name > '9') || (*p->name < '0'))
              {
                unsigned next_field = to_fill->num_fields();
                to_fill->set_num_fields(next_field + 1);
                to_fill->set_field_type(next_field, p->type);
                to_fill->set_field_name(next_field, p->name);
                to_fill->set_offset(next_field, p->offset);
              }
            delete p;
          }
        p = field_list;
      }

    if (deferred != NULL)
      {
        immed_list *descriptor = new immed_list;
        short def_offset = deferred->offset;
        char *name = new char[1];
        name[0] = 0;
        boolean has_constant = FALSE;
        boolean has_volatile = FALSE;

        while (deferred != NULL)
          {
            assert(deferred->offset == def_offset);

            if ((deferred->name == 0) || (*deferred->name > '9') ||
                (*deferred->name < '0'))
              {
                if (deferred->type->is_volatile())
                  {
                    has_volatile = TRUE;
                    descriptor->append(immed("volatile"));
                  }
                if (deferred->type->is_const())
                  {
                    has_constant = TRUE;
                    descriptor->append(immed("const"));
                  }
                deferred->type = deferred->type->unqual();

                if (deferred->type == inttype)
                    descriptor->append(immed("int"));
                else if (deferred->type == unsignedtype)
                    descriptor->append(immed("unsigned"));
                else
                    assert(FALSE);

                descriptor->append(immed(deferred->name));
                descriptor->append(immed(deferred->from));
                descriptor->append(immed(deferred->to));

                char *new_name =
                        new char[strlen(name) + strlen(deferred->name) + 2];
                strcpy(new_name, name);
                strcat(new_name, "_");
                strcat(new_name, deferred->name);
                delete[] name;
                name = new_name;
              }

            struct field *next_field = deferred->link;
            delete deferred;
            deferred = next_field;
          }

        if (*name != 0)
          {
            type_node *field_type = unsignedtype->copy();
            field_type->append_annote(k_bit_field_info, descriptor);
            field_type = to_fill->parent()->install_type(field_type);
            if (has_volatile)
                field_type = qual(VOLATILE, field_type);
            if (has_constant)
                field_type = qual(CONST, field_type);

            unsigned next_field = to_fill->num_fields();
            to_fill->set_num_fields(next_field + 1);
            to_fill->set_field_type(next_field, field_type);
            to_fill->set_field_name(next_field, name);
            to_fill->set_offset(next_field, def_offset);
          }
        else
          {
            delete descriptor;
          }
        delete[] name;
      }

    if (bits)
        off = add(off, ((bits + target.addressable_size - 2) /
                        target.addressable_size) * target.addressable_size);
    if (to_fill->op() == TYPE_STRUCT)
        to_fill->set_size(off);
    else if (off > to_fill->size())
        to_fill->set_size(off);
    add(to_fill->size(), resulting_alignment - 1);
    to_fill->set_size(roundup(to_fill->size(), resulting_alignment));
    if (overflow)
      {
        error("size of `%t' exceeds %d bytes\n", to_fill, INT_MAX);
        to_fill->set_size(INT_MAX & (~(resulting_alignment - 1)));
      }
  }

/* finalize - finalize tentative definitions, constants, check unref'd
   statics */
void finalize(void)
  {
    if (xref)
      {
        setuses(identifiers);
        foreach(types, level, fielduses, NULL);
        setuses(types);
      }
    foreach(identifiers, GLOBAL, doglobal, NULL);
    foreach(externals, GLOBAL, doextern, NULL);
    foreach(identifiers, GLOBAL, checkref, NULL);
  }

static void fillcallees(Symbol p, Generic cl);

static void fillcallees(Symbol p, Generic cl)
  {
    *(List *)cl = append(p, *(List *)cl);
  }

/* funcdecl - ... ( ... ) decl* compound */
static void funcdecl(int sclass, char *id, func_type *ftype, Coordinate pt)
  {
    int i, n;
    Symbol *callee, p;
    type_node **caller_types;
    List callee_list = NULL;

    if (isstruct_or_union(freturn(ftype)) && (freturn(ftype)->size() == 0))
        error("illegal use of incomplete type `%t'\n", freturn(ftype));
    callee_list = NULL;
    foreach(identifiers, PARAM, fillcallees, (Generic)&callee_list);
    n = length(callee_list);
    if ((Aflag >= 2) && (n > 31))
        warning("more than 31 parameters in function `%s'\n", id);
    callee = (Symbol *)ltoa(callee_list, NULL);
    caller_types = new type_node *[n];
    for (i = 0; (p = callee[i]) != NULL; i++)
      {
        if (*(p->name) >= '1' && *(p->name) <= '9')
            error("missing parameter name to function `%s'\n", id);
      }
    callee[i] = NULL;
    if (ftype->args_known())
        ftype = newstyle(ftype, caller_types, callee);
    else
        ftype = oldstyle(id, ftype, caller_types, callee);
    if ((Aflag >= 1) && !hasproto(ftype))
        warning("missing prototype\n");
    for (i = 0; (p = callee[i]) != NULL; i++)
      {
        if (p->type->size() == 0)
          {
            error("undefined size for parameter `%t %s'\n", p->type, p->name);
            caller_types[i] = p->type = inttype;
          }
      }

    p = lookup(id, identifiers);
    if ((p != NULL) && isfunc(p->type))
      {
        if (p->defined)
          {
            error("redefinition of `%s' previously defined at %w\n", p->name,
                  &p->src);
          }
        if (xref)
            use(p, p->src);
      }
    cfunc = dclglobal(sclass, id, ftype, &pt);
    cfunc->u.f.pt[0] = pt;
    cfunc->u.f.null_return = NULL;
    cfunc->defined = 1;
    if (Pflag)
        printproto(cfunc, callee);
    labels[0] = table(NULL, LABELS);
    labels[1] = table(NULL, LABELS);
    refinc = 1000;
    bnumber = -1;
    cfunc->u.f.pt[2] = *(definept(NULL));
    start_function(cfunc, callee, caller_types, n, &(cfunc->u.f.pt[2]));

    delete[] caller_types;

    compound(NULL, NULL, NULL, 0);

    assert(curr_proc != NULL);
    tree_block *current_block = curr_proc->block();
    assert(current_block != NULL);
    if (control_reaches_list_end(current_block->body()))
      {
        definept(NULL);
        return_nothing();
      }
    exitscope();
    foreach(identifiers, level, checkref, NULL);
    function();

    outflush();
    cfunc->u.f.pt[1] = src;
    expect('}');
    setuses(labels[0]);
    foreach(labels[0], LABELS, checklab, NULL);
    exitscope();
    cfunc = NULL;
  }

/* newstyle - process function arguments for new-style definition */
static func_type *newstyle(func_type *ty, type_node *caller_types[],
                           Symbol callee[])
  {
    for (int i = 0; callee[i] != NULL; i++)
        caller_types[i] = callee[i]->type;
    return ty;
  }

/* oldparam - check that p is an old-style parameter, and patch callee[i] */
static void oldparam(Symbol p, Generic cl)
  {
    Symbol *callee = (Symbol *)cl;

    for (int i = 0; callee[i] != NULL; i++)
      {
        if (p->name == callee[i]->name)
          {
            callee[i] = p;
            return;
          }
      }
    error("declared parameter `%s' is missing\n", p->name);
  }

/* oldstyle - process function arguments for old-style definition */
static func_type *oldstyle(char *name, func_type *ty,
                           type_node *caller_types[], Symbol callee[])
  {
    unsigned i;
    Symbol p;

    while ((kind[t] == CHAR) || (kind[t] == STATIC) ||
           ((t == ID) && (tsym != NULL) && (tsym->sclass == TYPEDEF)))
      {
        decl(&dclparam, TRUE);
      }
    foreach(identifiers, PARAM, oldparam, (Generic)callee);
    for (i = 0; (p = callee[i]) != NULL; i++)
      {
        if (p->type == NULL)
          {
            p->type = inttype;
            p->defined = 1;
          }
        if (p->type->unqual() == floattype)
            caller_types[i] = doubletype;
        else
            caller_types[i] = promote(p->type);
      }

    unsigned num_params = i;

    if (((p = lookup(name, identifiers)) != NULL) && (p->scope == GLOBAL) &&
        isfunc(p->type) && ((func_type *)(p->type->unqual()))->args_known())
      {
        func_type *the_func_type = (func_type *)(p->type->unqual());
        base_symtab *owning_symtab = the_func_type->parent();
        the_func_type = (func_type *)(the_func_type->copy());

        unsigned arg_num;
        for (arg_num = 0;
             (arg_num < num_params) && (arg_num < the_func_type->num_args());
             arg_num++)
          {
            unsigned ft_arg_num = (num_params - arg_num) - 1;
            if (!eqtype(the_func_type->arg_type(ft_arg_num)->unqual(),
                        caller_types[arg_num]->unqual(), TRUE))
              {
                warning("conflicting declarations of argument %u of function "
                        "`%s'\n", ft_arg_num + 1, name);
                caller_types[arg_num] = the_func_type->arg_type(ft_arg_num);
              }
          }
        if ((num_params < the_func_type->num_args()) ||
            ((num_params > the_func_type->num_args()) &&
             (!the_func_type->has_varargs())))
          {
            error("conflicting argument declarations for function `%s'\n",
                  name);
          }

        the_func_type->set_return_type(freturn(ty));
        assert(((suif_object *)(the_func_type))->object_kind() == TYPE_OBJ);
        ty = (func_type *)(owning_symtab->install_type(the_func_type));
      }
    else
      {
        base_symtab *owning_symtab = ty->parent();
        ty = new func_type(freturn(ty));
        ty = (func_type *)(owning_symtab->install_type(ty));
      }
    return ty;
  }

/* parameters - [id ( , id )* | type dclr ( , type dclr )*] */
static type_node **parameters(int lev)
  {
    type_node **proto = NULL;

    enterscope();
    if ((kind[t] == CHAR) || (kind[t] == STATIC) ||
        ((t == ID) && (tsym != NULL) && (tsym->sclass == TYPEDEF)))
      {
        int sclass;
        int n = 0;
        type_node *last = NULL;
        type_node *ty;
        List list = NULL;
        for (;;)
          {
            char *id = NULL;
            n++;
            if ((last != NULL) && (t == ELLIPSIS))
              {
                if (last == voidtype)
                    error("illegal formal parameter types\n");
                list = append(voidtype, list);
                t = gettok();
                break;
              }
            if (((t == ID) && ((tsym == NULL) || (tsym->sclass != TYPEDEF))) ||
                ((t != ID) && (t != REGISTER) && (kind[t] != CHAR)))
              {
                error("missing parameter type\n");
              }
            ty = dclr(parse_type(PARAM, &sclass), &id, lev + 1);
            if ((Aflag >= 1) && !hasproto(ty))
                warning("missing prototype\n");
            if ((ty == voidtype) && ((last != NULL) || (id != NULL)))
                error("illegal formal parameter types\n");
            if (id == NULL)
                id = stringd(n);
            if (ty == voidtype)
              {
                list = append(voidtype, list);
              }
            else
              {
                Symbol p = dclparam(sclass, id, ty, &src);
                list = append(p->type, list);
              }
            last = ty;
            if (t != ',')
                break;
            t = gettok();
          }
        proto = new type_node *[n + 1];
        proto = (type_node **)ltoa(list, (Generic *)proto);
      }
    else if (t == ID)
      {
        for (;;)
          {
            if (t != ID)
              {
                error("expecting an identifier\n");
                break;
              }
            dclparam(0, token, NULL, &src);
            t = gettok();
            if (t != ',')
                break;
            t = gettok();
          }
      }
    if (t != ')')
      {
        static char follow[] = { CHAR, STATIC, IF, ')', 0 };
        expect(')');
        skipto('{', follow);
      }
    if (t == ')')
        t = gettok();
    return proto;
  }

/* checkparam - check for old-style param list; called at ends of parameters */
static void checkparam(Symbol p, Generic)
  {
    if (p->type == NULL)
        error("extraneous specification for formal parameter `%s'\n", p->name);
  }

/* program - decl* */
void program(void)
  {
    int n;

    level = GLOBAL;
    for (n = 0; t != EOI; n++)
      {
        if ((kind[t] == CHAR) || (kind[t] == STATIC) || (t == ID))
          {
            decl(dclglobal, FALSE);
          }
        else
          {
            if (t == ';')
                warning("empty declaration\n");
            else
                error("unrecognized declaration\n");
            t = gettok();
          }
      }
    if (n == 0)
        warning("empty input file\n");
  }

/* structdcl - ( struct | union )  ( [ id ] { ( field; )+ } | id ) */
static struct_type *structdcl(int op)
  {
    char *tag;
    struct_type *result;
    Symbol p;
    Coordinate pos;

    t = gettok();
    if (t == ID)
      {
        tag = token;
        pos = src;
        t = gettok();
      }
    else
      {
        tag = string("");
      }

    if (t == '{')
      {
        static char follow[] = { IF, ',', 0 };
        Symbol struct_sym = newstruct(op, tag);
        assert(struct_sym->type->is_struct());
        result = (struct_type *)(struct_sym->type);
        if (*tag != 0)
            struct_sym->src = pos;
        t = gettok();
        if ((kind[t] == CHAR) ||
            ((t == ID) && (tsym != NULL) && (tsym->sclass == TYPEDEF)))
          {
            fields(result);
          }
        else
          {
            if (t == '}')
                error("empty %k declaration\n", op);
            else
                error("invalid %k field declarations\n", op);
          }
        test('}', follow);
        struct_sym->defined = 1;
      }
    else if ((*tag != 0) && ((p = lookup(tag, types)) != NULL) &&
             (((p->type->op() == TYPE_STRUCT) && (op == STRUCT)) ||
              ((p->type->op() == TYPE_UNION) && (op == UNION))))
      {
        if ((t == ';') && (p->scope < level))
            (void)newstruct(op, tag);
        if (xref)
            use(p, pos);
        assert(p->type->is_struct());
        result = (struct_type *)(p->type);
      }
    else
      {
        if (*tag == 0)
            error("missing %k tag\n", op);
        Symbol struct_sym = newstruct(op, tag);
        assert(struct_sym->type->is_struct());
        result = (struct_type *)(struct_sym->type);
        if ((*tag != 0) && xref)
            use(struct_sym, pos);
      }
    return result;
  }

/* type - parse basic storage class and type specification */
static type_node *parse_type(int lev, int *sclass)
  {
    int cls, cons, *p, sign, size, type, vol;
    tokencode tt;
    type_node *ty = NULL;
    boolean class_specifier_given = FALSE;
    boolean two_longs = FALSE;

    if (sclass == NULL)
        cls = AUTO;
    else
        *sclass = 0;
    for (vol = cons = sign = size = type = 0;;)
      {
        p = &type;
        tt = t;
        switch (t)
          {
            case AUTO:
            case REGISTER:
            case STATIC:
            case EXTERN:
            case TYPEDEF:
                p = (sclass ? sclass : &cls);
                if (class_specifier_given)
                    error("only one storage class specifier is allowed");
                class_specifier_given = TRUE;
                break;
            case CONST:
                p = &cons;
                break;
            case VOLATILE:
                p = &vol;
                break;
            case SIGNED:
            case UNSIGNED:
                p = &sign;
                break;
            case LONG:
            case SHORT:
                p = &size;
                break;
            case VOID:
            case CHAR:
            case INT:
            case FLOAT:
            case DOUBLE:
                ty = tsym->type;
                break;
            case ENUM:
                ty = enumdecl();
                break;
            case STRUCT:
            case UNION:
                ty = structdcl(t);
                break;
            case ID:
                if ((tsym != NULL) && (tsym->sclass == TYPEDEF) &&
                    (type == 0) && (size == 0) && (sign == 0))
                  {
                    use(tsym, src);
                    ty = tsym->type;
                    switch (ty->op())
                      {
                        case TYPE_INT:
                            tt = INT;
                            break;
                        case TYPE_FLOAT:
                            tt = FLOAT;
                            break;
                        case TYPE_VOID:
                            tt = VOID;
                            break;
                        case TYPE_PTR:
                            tt = POINTER;
                            break;
                        case TYPE_ARRAY:
                            tt = ARRAY;
                            break;
                        case TYPE_FUNC:
                            tt = FUNCTION;
                            break;
                        case TYPE_STRUCT:
                            tt = STRUCT;
                            break;
                        case TYPE_UNION:
                            tt = UNION;
                            break;
                        case TYPE_ENUM:
                            tt = ENUM;
                            break;
                        case TYPE_CONST:
                            tt = CONST;
                            break;
                        case TYPE_VOLATILE:
                            tt = VOLATILE;
                            break;
                        default:
                            assert(FALSE);
                      }
                    t = tt;
                    break;
                  }
                /* fall through */
            default:
                p = NULL;
          }
        if (p == NULL)
            break;
        if ((p == &size) && (size == LONG) && (t == LONG) && (!two_longs))
            two_longs = TRUE;
        else if (*p != 0)
            error("invalid use of `%k'\n", tt);
        *p = tt;
        if (t == tt)
            t = gettok();
      }
    if (two_longs)
      {
        static boolean warned = FALSE;
        if (!warned)
          {
            warning("non-ANSI type ``long long int'' used\n");
            warned = TRUE;
          }
      }
    if ((sclass != NULL) && (*sclass != 0) &&
        ((lev == 0) || ((lev == PARAM) && (*sclass != REGISTER)) ||
         ((lev == GLOBAL) && (*sclass == REGISTER))))
      {
        error("invalid use of `%k'\n", *sclass);
      }
    if (type == 0)
      {
        type = INT;
        ty = inttype;
      }
    if (((size == SHORT) && (type != INT)) ||
        ((size == LONG) && (!two_longs) && (type != INT) &&
         (type != DOUBLE)) ||
        ((size == LONG) && two_longs && (type != INT)) ||
        (sign && (type != INT) && (type != CHAR)))
      {
        error("invalid type specification\n");
      }
    if ((type == CHAR) && sign)
        ty = ((sign == UNSIGNED) ? unsignedchar : signedchar);
    else if (size == SHORT)
        ty = ((sign == UNSIGNED) ? unsignedshort : shorttype);
    else if ((size == LONG) && (type == DOUBLE))
        ty = longdouble;
    else if ((size == LONG) && (!two_longs))
        ty = ((sign == UNSIGNED) ? unsignedlong : longtype);
    else if (size == LONG)
        ty = ((sign == UNSIGNED) ? unsignedlonglong : longlong);
    else if ((sign == UNSIGNED) && (type == INT))
        ty = unsignedtype;
    if (cons == CONST)
        ty = qual(CONST, ty);
    if (vol == VOLATILE)
        ty = qual(VOLATILE, ty);
    return ty;
  }

/* get_typename - type dclr */
extern type_node *get_typename(void)
  {
    type_node *ty = parse_type(0, NULL);

    if ((t == '*') || (t == '(') || (t == '['))
      {
        ty = dclr(ty, NULL, 1);
        if ((Aflag >= 1) && !hasproto(ty))
            warning("missing prototype\n");
      }
    return ty;
  }

static boolean control_reaches_list_end(tree_node_list *node_list)
  {
    if (node_list == NULL)
        return TRUE;

    tree_node *last_active = last_action_node(node_list);
    if (last_active == NULL)
        return TRUE;

    switch (last_active->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)last_active;
            instruction *the_instr = the_tree_instr->instr();
            assert(the_instr != NULL);
            if ((the_instr->opcode() == io_jmp) ||
                (the_instr->opcode() == io_ret) ||
                (the_instr->opcode() == io_mbr))
              {
                return FALSE;
              }
            else
              {
                return TRUE;
              }
          }
        case TREE_LOOP:
            return TRUE;
        case TREE_FOR:
            return TRUE;
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)last_active;
            return (control_reaches_list_end(the_if->then_part()) ||
                    control_reaches_list_end(the_if->else_part()));
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)last_active;
            return control_reaches_list_end(the_block->body());
          }
        default:
            assert(FALSE);
      }
    return TRUE;
  }
