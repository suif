/* file "input.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot input processing */

#include "c.h"

unsigned char *cp;      /* current input character */
char *file;             /* current input file name */
char *firstfile;        /* first input file */
unsigned char *limit;   /* points to last character + 1 */
char *line;             /* current line */
int lineno;             /* line number of current line */

static FILE *infp;      /* input file pointer */
static long bsize;       /* number of chars in last read */
static unsigned char buffer[MAXTOKEN+1 + BUFSIZE+1];    /* input buffer */

static void pragma(void);
static void resynch(void);

/* inputInit - initialize input processing */
void inputInit(FILE *fp)
  {
    limit = cp = &buffer[MAXTOKEN + 1];
    lineno = 0;
    file = NULL;
    bsize = -1;
    infp = fp;
    nextline();
  }

/* inputstring - arrange to read str as next input */
void inputstring(char *str)
  {
    limit = cp = &buffer[MAXTOKEN+1];
    while ((*limit++ = *str++) != 0)
        ;
    *limit = '\n';
    bsize = 0;
  }

/* fillbuf - fill the input buffer, moving tail portion if necessary */
void fillbuf(void)
  {
    if (bsize == 0)
        return;
    if (limit <= cp)
      {
        cp = &buffer[MAXTOKEN + 1];
      }
    else      /* move tail portion */
      {
        int n = limit - cp;
        unsigned char *s = &buffer[MAXTOKEN + 1] - n;
        assert(s >= buffer);
        line = (char *)s - ((char *)cp - line);
        while (cp < limit)
            *s++ = *cp++;
        cp = &buffer[MAXTOKEN + 1] - n;
      }
    bsize = fread(&buffer[MAXTOKEN + 1], sizeof(unsigned char), BUFSIZE, infp);
    assert(bsize >= 0);
    limit = &buffer[MAXTOKEN + 1 + bsize];
    *limit = '\n';
  }

/* nextline - prepare to read next line */
void nextline(void)
  {
    if (cp >= limit)      /* refill buffer */
      {
        fillbuf();
        if (cp >= limit)      /* signal end of file */
          {
            cp = limit = &buffer[MAXTOKEN+1];
            *limit = '\0';
          }
        if (lineno > 0)
            return;
      }
    lineno++;
    for (line = (char *)cp; *cp == ' ' || *cp == '\t'; cp++)
        ;
    if (*cp == '#')
      {
        resynch();
        nextline();
      }
  }

/* pragma - handle #pragma */
static void pragma(void)
  {
    while ((*cp == ' ') || (*cp == '\t'))
        cp++;
    if ((*cp == '\n') || (*cp == 0))
        return;

    concat_strings = FALSE;

    t = gettok();

    const char *annote_name;
    if ((t == ID) && (strcmp(token, "suif_annote") == 0))
      {
        while ((*cp == ' ') || (*cp == '\t'))
            cp++;
        if ((*cp == '\n') || (*cp == 0))
          {
            warning("bad suif annote -- missing name; ignored\n");
            concat_strings = TRUE;
            return;
          }
        t = gettok();
        if (t != SCON)
          {
            warning("bad suif annote -- invalid name; ignored\n");
            while ((*cp != '\n') && (*cp != 0))
                cp++;
            concat_strings = TRUE;
            return;
          }
        annote_name = lexicon->enter(tsym->u.c.v.p)->sp;
        while ((*cp == ' ') || (*cp == '\t'))
            cp++;
        if ((*cp != '\n') && (*cp != 0))
            t = gettok();
      }
    else
      {
        annote_name = lexicon->enter("C pragma")->sp;
      }

    immed_list *new_annote = new immed_list;

    if (lookup_annote(annote_name) == NULL)
      {
        annote_def *new_def = new annote_def(annote_name, TRUE);
        register_annote(new_def);
      }

    for (; (*cp != 'n') && (*cp != 0);)
      {
        if (t == EOI)
            break;
        switch (t)
          {
            case FCON:
                new_annote->append(immed_and_type_for_C_floatconst(token,
                                                                   NULL));
                break;
            case ICON:
              {
                immed the_immed;
                if (*token == '\'')
                    the_immed = immed(tsym->u.c.v.i);
                else
                    the_immed = immed_and_type_for_C_intconst(token, NULL);
                new_annote->append(the_immed);
                break;
              }
            case SCON:
                new_annote->append(immed(tsym->u.c.v.p));
                break;
            case ID:
                if ((tsym != NULL) && (tsym->suif_symbol != NULL))
                  {
                    new_annote->append(immed(tsym->suif_symbol));
                    break;
                  }
                else
                  {
                    new_annote->append(immed(token));
                    break;
                  }
            default:
                new_annote->append(immed(stringf("%k", t)));
                break;
          }
        while ((*cp == ' ') || (*cp == '\t'))
            cp++;
        if ((*cp == '\n') || (*cp == 0))
            break;
        t = gettok();
      }

    if (curr_list != NULL)
      {
        in_rrr *new_mark = new in_rrr(io_mrk);
        new_mark->append_annote(annote_name, new_annote);
        curr_list->append(new tree_instr(new_mark));
      }
    else
      {
        outf->append_annote(annote_name, new_annote);
      }
    concat_strings = TRUE;
  }

/* resynch - set line number/file name in # n [ "file" ] and #pragma ... */
static void resynch(void)
  {
    for (cp++; (*cp == ' ') || (*cp == '\t'); )
        cp++;
    if (limit - cp < MAXTOKEN)
        fillbuf();
    if (strncmp((char *)cp, "pragma", 6) == 0)
      {
        cp += 6;
        pragma();
      }
    else if ((*cp >= '0') && (*cp <= '9'))
      {
line:
        for (lineno = 0; (*cp >= '0') && (*cp <= '9'); )
            lineno = 10*lineno + *cp++ - '0';
        lineno--;
        while ((*cp == ' ') || (*cp == '\t'))
            cp++;
        if (*cp == '"')
          {
            file = (char *)++cp;
            while ((*cp != 0) && (*cp != '"') && (*cp != '\n'))
                cp++;
            if (cp == limit)
              {
                char buf[MAXLINE], *s = buf;
                while (file < (char *)cp)
                    *s++ = *file++;
                while (cp == limit && *cp)
                  {
                    cp++;
                    nextline();
                    for (; (*cp != 0) && (*cp != '"') && (*cp != '\n'); cp++)
                      {
                        if (s < &buf[sizeof buf])
                            *s++ = *cp;
                      }
                  }
                file = stringn(buf, s - buf);
                if (s == &buf[sizeof buf])
                    warning("file name is too long\n");
              }
            else
              {
                file = stringn(file, (char *)cp - file);
              }
            if (*cp == '\n')
                warning("missing \" in preprocessor line\n");
            if (firstfile == NULL)
                firstfile = file;
          }
      }
    else if (strncmp((char *)cp, "line", 4) == 0)
      {
        for (cp += 4; (*cp == ' ') || (*cp == '\t'); )
            cp++;
        if ((*cp >= '0') && (*cp <= '9'))
            goto line;
        if (Aflag >= 2)
            warning("unrecognized control line\n");
      }
    else if ((Aflag >= 2) && (*cp != '\n'))
      {
        warning("unrecognized control line\n");
      }
    while (*cp)
      {
        if (*cp++ == '\n')
          {
            if (cp == limit + 1)
                nextline();
            else
                break;
          }
      }
  }
