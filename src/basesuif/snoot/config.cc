/* file "config.cc" of the snoot program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the code to set up all the target-dependent
 *  information.
 */

#include "c.h"

#define IS_BIG_ENDIAN(x)
#define ADDRESSABLE_SIZE(x)
#define CHAR_IS_SIGNED(x)
#define TYPE_SIZE(x, y)
#define TYPE_ALIGN(x, y)
#define ARRAY_ALIGN(x)
#define STRUCT_ALIGN(x)
#define PTR_DIFF_TYPE(x)
#define BUILTIN(x)

#define TARGET(x, y) target_ ## y,

typedef enum
  {
#include "config.h"
    num_targets
  } target_enum;

#undef TARGET
#define TARGET(x, y) #x,

char *target_names[] =
  {
#include "config.h"
    NULL
  };

#undef IS_BIG_ENDIAN
#undef ADDRESSABLE_SIZE
#undef CHAR_IS_SIGNED
#undef TYPE_SIZE
#undef TYPE_ALIGN
#undef ARRAY_ALIGN
#undef STRUCT_ALIGN
#undef PTR_DIFF_TYPE
#undef BUILTIN
#undef TARGET

target_info_block target_table[num_targets];
target_info_block native_target;
target_info_block *this_target;

extern void initialize_targets(void)
  {
    target_info_block *this_block = NULL;
    int builtin_num = 0;

#define IS_BIG_ENDIAN(x)
#define ADDRESSABLE_SIZE(x)
#define CHAR_IS_SIGNED(x)
#define TYPE_SIZE(x, y)
#define TYPE_ALIGN(x, y)
#define ARRAY_ALIGN(x)
#define STRUCT_ALIGN(x)
#define PTR_DIFF_TYPE(x)
#define BUILTIN(x)          ++builtin_num;
#define TARGET(x, y)        if ((this_block != NULL) && (builtin_num > 0)) \
                              { \
                                this_block->builtins = \
                                        new char*[builtin_num + 1]; \
                                this_block->builtins[builtin_num] = NULL; \
                              } \
                            this_block = &(target_table[target_ ## y]); \
                            builtin_num = 0;

#include "config.h"

    if ((this_block != NULL) && (builtin_num > 0))
      {
        this_block->builtins = new char*[builtin_num + 1];
        this_block->builtins[builtin_num] = NULL;
      }

    this_block = NULL;

#undef IS_BIG_ENDIAN
#undef ADDRESSABLE_SIZE
#undef CHAR_IS_SIGNED
#undef TYPE_SIZE
#undef TYPE_ALIGN
#undef ARRAY_ALIGN
#undef STRUCT_ALIGN
#undef PTR_DIFF_TYPE
#undef BUILTIN
#undef TARGET


#define IS_BIG_ENDIAN(x)    this_block->is_big_endian = x;
#define ADDRESSABLE_SIZE(x) this_block->addressable_size = x;
#define CHAR_IS_SIGNED(x)   this_block->char_is_signed = x;
#define TYPE_SIZE(x, y)     this_block->size[C_ ## x] = y;
#define TYPE_ALIGN(x, y)    this_block->align[C_ ## x] = y;
#define ARRAY_ALIGN(x)      this_block->array_align = x;
#define STRUCT_ALIGN(x)     this_block->struct_align = x;
#define PTR_DIFF_TYPE(x)    this_block->ptr_diff_type = x;
#define BUILTIN(x)          this_block->builtins[builtin_num] = x; \
                            ++builtin_num;
#define TARGET(x, y)        this_block = &(target_table[target_ ## y]); \
                            builtin_num = 0;

#include "config.h"

#undef IS_BIG_ENDIAN
#undef ADDRESSABLE_SIZE
#undef CHAR_IS_SIGNED
#undef TYPE_SIZE
#undef TYPE_ALIGN
#undef ARRAY_ALIGN
#undef STRUCT_ALIGN
#undef PTR_DIFF_TYPE
#undef BUILTIN
#undef TARGET

    get_info(&native_target);

    this_target = &native_target;
  }
