/* file "builtin.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the implementation of routines to handle
 *  builtin functions.
 */

#include "c.h"

struct builtin_info
  {
    const char *name;
    int type_arg_num;
    type_node *result_type;
    builtin_info *next;
  };

DECLARE_DLIST_CLASS(char_pointer_list, char *);

builtin_info *info_list = NULL;

static char_pointer_list waiting_builtins;

extern boolean is_builtin(char *id)
  {
    builtin_info *follow_info = info_list;
    while (follow_info != NULL)
      {
        if (strcmp(id, follow_info->name) == 0)
            return TRUE;
        follow_info = follow_info->next;
      }
    return FALSE;
  }

extern void type_builtin(in_gen *the_gen)
  {
    builtin_info *follow_info = info_list;
    while (follow_info != NULL)
      {
        if (strcmp(the_gen->name(), follow_info->name) == 0)
            break;
        follow_info = follow_info->next;
      }

    assert(follow_info != NULL);
    type_node *result_type = inttype;
    if (follow_info->result_type != NULL)
      {
        result_type = follow_info->result_type;
      }
    else
      {
        immed_list *arg_list =
                (immed_list *)(the_gen->peek_annote(k_builtin_args));
        if (arg_list != NULL)
          {
            immed_list_iter arg_iter(arg_list);
            int num_to_skip = follow_info->type_arg_num;
            while ((num_to_skip > 0) && !arg_iter.is_empty())
              {
                arg_iter.step();
                --num_to_skip;
              }
            if (!arg_iter.is_empty())
              {
                immed arg_data = arg_iter.step();
                if (arg_data.is_type())
                  {
                    result_type = arg_data.type();
                  }
                else if (arg_data.is_string())
                  {
                    result_type = chartype->ptr_to();
                  }
                else if (arg_data.is_unsigned_int())
                  {
                    result_type =
                            the_gen->src_op(arg_data.unsigned_int()).type();
                  }
                else
                  {
                    assert(FALSE);
                  }
              }
          }
      }
    the_gen->set_result_type(result_type);
  }

extern void register_builtin_info(char *this_info)
  {
    char *follow_this = this_info;
    if ((!isalpha(*follow_this)) && (*follow_this != '_'))
      {
        error("\"%s\" is an invalid built-in definition", this_info);
        return;
      }
    ++follow_this;
    while (isalnum(*follow_this) || (*follow_this == '_'))
        ++follow_this;

    builtin_info *new_info = new builtin_info;
    new_info->next = info_list;
    info_list = new_info;
    new_info->type_arg_num = 0;
    new_info->result_type = inttype;
    if (*follow_this == 0)
      {
        new_info->name = lexicon->enter(this_info)->sp;
      }
    else
      {
        char *temp_name = new char[(follow_this - this_info) + 1];
        strncpy(temp_name, this_info, (follow_this - this_info));
        temp_name[(follow_this - this_info)] = 0;
        new_info->name = lexicon->enter(temp_name)->sp;
        delete temp_name;

        if (!isspace(*follow_this))
          {
            error("\"%s\" is an invalid built-in definition", this_info);
            return;
          }
        while (isspace(*follow_this))
            ++follow_this;

        if (strncmp(follow_this, "always", 6) == 0)
          {
            if (!isspace(*follow_this))
              {
                error("\"%s\" is an invalid built-in definition", this_info);
                return;
              }
            while (isspace(*follow_this))
                ++follow_this;

            inputstring(follow_this);
            new_info->result_type = get_typename();
            if (t != 0)
              {
                error("\"%s\" is an invalid built-in definition", this_info);
                return;
              }
          }
        else if (strncmp(follow_this, "arg", 3) == 0)
          {
            follow_this += 3;
            int arg_num = 0;
            while (isdigit(*follow_this))
              {
                int new_num = arg_num * 10;
                if (new_num / 10 != arg_num)
                  {
                    error("overflow in built-in definition \"%s\"", this_info);
                    return;
                  }
                arg_num = new_num;
                new_num = arg_num + (*follow_this) - '0';
                if (new_num < arg_num)
                  {
                    error("overflow in built-in definition \"%s\"", this_info);
                    return;
                  }
                arg_num = new_num;
                ++follow_this;
              }
            new_info->type_arg_num = arg_num;
            new_info->result_type = NULL;
          }
        else
          {
            error("\"%s\" is an invalid built-in definition", this_info);
            return;
          }
      }
  }

extern void init_builtins(void)
  {
    char **follow = this_target->builtins;
    if (follow != NULL)
      {
        while (*follow != NULL)
          {
            register_builtin_info(*follow);
            ++follow;
          }
      }
    while (!waiting_builtins.is_empty())
      {
        char *this_info = waiting_builtins.pop();
        register_builtin_info(this_info);
        delete[] this_info;
      }
  }

extern void register_when_builtins_initialized(const char *this_info)
  {
    char *new_string = new char[strlen(this_info) + 1];
    strcpy(new_string, this_info);
    waiting_builtins.append(new_string);
  }
