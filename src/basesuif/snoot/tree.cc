/* file "tree.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot tree management */

#include "c.h"

static struct arena first[] =
  {
    { 0, 0, 0, &first[0], 0 },
    { 0, 0, 0, &first[1], 0 },
  };

Arena permanent = &first[0]; /* permanent storage */
Arena transient = &first[1]; /* transient storage; released at function end */
static Arena freearenas;     /* list of free arenas */

/* allocate - allocate n bytes in arena **p, adding a new arena if necessary */
char *allocate(int n, Arena *p)
  {
    Arena ap = *p;

    while (ap->avail + n > ap->limit)
      {
        if (ap->next != NULL)    /* move to next arena */
          {
            ap = ap->next;
            ap->avail = (char *)ap + roundup(sizeof *ap, sizeof (double));
          }
        else if ((ap->next = freearenas) != NULL)
          {
            freearenas = freearenas->next;
            ap = ap->next;
            ap->avail = (char *)ap + roundup(sizeof *ap, sizeof (double));
            ap->first = (*p)->first;
            ap->next = NULL;
          }
        else    /* allocate a new arena */
          {
            int m = n + MEMINCR * 1024 + roundup(sizeof *ap, sizeof (double));
            ap->next = (Arena)malloc(m);
            assert(ap->next != NULL);
            if ((char *)ap->next == ap->limit) /* extend previous arena? */
              {
                ap->limit = (char *)ap->next + m;
              }
            else    /* link to a new arena */
              {
                ap = ap->next;
                ap->limit = (char *)ap + m;
                ap->avail = (char *)ap + roundup(sizeof *ap, sizeof (double));
              }
            ap->first = (*p)->first;
            ap->next = NULL;
          }
      }
    *p = ap;
    ap->avail += n;
    return ap->avail - n;
  }

/* deallocate - release all space in arena *p, except the first arena;
   reset *p */
void deallocate(Arena *p)
  {
    Arena first = (*p)->first;

    (*p)->next = freearenas;
    freearenas = first->next;
    first->next = NULL;
    *p = first;
  }
