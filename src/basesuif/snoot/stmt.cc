/* file "stmt.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot statement parsing */

#include "c.h"

#define SWITCHSIZE 512          /* initial size of switch arrays */

i_rational density("0.5");      /* minimum switch density */
int refinc = 1000;              /* amount by which ref field is incremented */

struct swtch            /* switch statement data: */
  {
    label_sym *deflab;      /* default label */
    int ncases;             /* number of cases */
    int size;               /* size of value & label arrays */
    i_integer *values;      /* value, label pairs */
    label_sym **labels;
    type_node *key_type;
  };

static void insert_label(label_sym *lab);
static void branch(label_sym *target);
static void caselabel(struct swtch *swp, i_integer val,
                      label_sym *the_label_sym);
static void cmp(int op, operand p, i_integer n, label_sym *lab);
static void dostmt(struct swtch *swp, int lev);
static void forstmt(struct swtch *swp, int lev);
static boolean c_for_to_suif_for(tree_for **suif_for, tree_node_list *c_init,
                                 tree_node_list *c_test_list,
                                 operand c_test_operand,
                                 tree_node_list *c_increment);
static boolean is_gele_test(tree_node_list *c_test_list,
                            operand c_test_operand, operand step_op,
                            var_sym *for_index, operand *for_ub);
static void ifstmt(label_sym *continue_lab, label_sym *break_lab,
                   struct swtch *swp, int lev);
static var_sym *localaddr(genop p);
static void return_void(void);
static void return_value(genop to_return);
static void stmtlabel(char *label);
static int swcode(struct swtch *swp, operand key, int b[], int lb, int ub,
                  int n);
static void swgen(struct swtch *swp, operand key);
static void swstmt(label_sym *continue_lab, int lev);
static void whilestmt(struct swtch *swp, int lev);


/* branch - jump to lab */
static void branch(label_sym *target)
  {
    instruction *branch_instr = new in_bj(io_jmp, target);
    curr_list->append(new tree_instr(branch_instr));
  }

/* caselabel - add a label to the current switch list */
static void caselabel(struct swtch *swp, i_integer val,
                      label_sym *the_label_sym)
  {
    int k;

    if (swp->ncases >= swp->size)
      {
        i_integer *vals = swp->values;
        label_sym **labs = swp->labels;
        swp->size *= 2;
        swp->values = new i_integer[swp->size];
        swp->labels = new label_sym *[swp->size];
        for (k = 0; k < swp->ncases; k++)
          {
            swp->values[k] = vals[k];
            swp->labels[k] = labs[k];
          }
        delete[] vals;
        delete[] labs;
      }
    for (k = swp->ncases; (k > 0) && (swp->values[k-1] >= val); k--)
      {
        swp->values[k] = swp->values[k-1];
        swp->labels[k] = swp->labels[k-1];
      }
    if ((k < swp->ncases) && (swp->values[k] == val))
        error("duplicate case label `%D'\n", &val);
    swp->values[k] = val;
    swp->labels[k] = the_label_sym;
    ++swp->ncases;
    if ((Aflag >= 2) && (swp->ncases == 258))
        warning("more than 257 cases in a switch statement\n");
  }

/* cmp - generate code for `if (p op n) goto lab' for integer n */
static void cmp(int op, operand p, i_integer n, label_sym *lab)
  {
    operand const_operand =
            operand(new in_ldc(p.type(), operand(), ii_to_immed(n)));
    instruction *comparison;
    switch (op)
      {
        case EQ:
            comparison = new in_rrr(io_seq, booleantype, operand(), p,
                                    const_operand);
            break;
        case LT:
            comparison = new in_rrr(io_sl, booleantype, operand(), p,
                                    const_operand);
            break;
        case GT:
            comparison = new in_rrr(io_sl, booleantype, operand(),
                                    const_operand, p);
            break;
        default:
            assert(FALSE);
      }
    instruction *branch_instr = new in_bj(io_btrue, lab, operand(comparison));
    tree_instr *the_tree_instr = new tree_instr(branch_instr);
    current_list_append_node(the_tree_instr);
  }

/* insert_label - insert a label node at the current position */
static void insert_label(label_sym *lab)
  {
    current_list_append_node(new tree_instr(new in_lab(lab)));
  }

/* definept - define an execution point n: current token at current pc */
Coordinate *definept(Coordinate *p)
  {
    Coordinate *result = (p != NULL) ? p : &src;

    stabline(result);

    if (option_mark_execution_points)
      {
        if (curr_list != NULL)
            curr_list->append(sequence_point_mark(result));
      }

    return result;
  }

/* dostmt - do statement while ( expression ) */
static void dostmt(struct swtch *swp, int lev)
  {
    refinc *= 10;
    t = gettok();

    tree_node_list *loop_body = new tree_node_list;
    tree_node_list *loop_test = new tree_node_list;
    label_sym *continue_label = genlabel();
    label_sym *break_label = genlabel();
    label_sym *top_label = genlabel();

    tree_loop *the_loop =
            new tree_loop(loop_body, loop_test, continue_label, break_label,
                          top_label);

    assert(curr_list != NULL);
    curr_list->append(the_loop);

    tree_node_list *old_list = curr_list;
    curr_list = loop_body;
    statement(continue_label, break_label, swp, lev);
    curr_list = old_list;

    expect(WHILE);
    expect('(');

    definept(NULL);

    genop the_expression = conditional(')');
    the_expression.clean_up_bit_field_refs();
    loop_test->append(the_expression.precomputation());
    delete the_expression.precomputation();
    operand loop_condition = the_expression.suif_operand();

    in_bj *loop_branch = new in_bj(io_btrue, top_label, loop_condition);
    loop_test->append(new tree_instr(loop_branch));
  }

/* forstmt - for ( [expr1] ; [expr2] ; [expr3] ) statement */
static void forstmt(struct swtch *swp, int lev)
  {
    tree_node_list *e1;
    tree_node_list *e2_list;
    tree_node_list *e3;
    operand e2_operand;
    Coordinate pt1, pt2, pt3;
    static char follow[] = { IF, ID, '}', 0 };

    e1 = NULL;
    e2_list = NULL;
    e3 = NULL;
    t = gettok();
    expect('(');
    pt1 = src;
    if (kind[t] == ID)
        e1 = expr0(';');
    else
        expect(';');
    refinc *= 10;
    pt2 = src;
    if (kind[t] == ID)
      {
        genop e2 = conditional(';');
        e2.clean_up_bit_field_refs();
        e2_list = e2.precomputation();
        e2_operand = e2.suif_operand();
      }
    else
      {
        expect(';');
      }
    pt3 = src;
    if (kind[t] == ID)
        e3 = expr0(')');
    else
        test(')', follow);

    definept(&pt1);

    tree_node_list *body;
    label_sym *continue_label;
    label_sym *break_label;
    tree_for *the_for = NULL;
    boolean is_a_for =
            c_for_to_suif_for(&the_for, e1, e2_list, e2_operand, e3);
    if (is_a_for)
      {
        assert(the_for != NULL);
        body = the_for->body();
        continue_label = the_for->contlab();
        break_label = the_for->brklab();

        assert(curr_list != NULL);
        curr_list->append(e1);
        delete e1;

        curr_list->append(the_for);
        definept(&pt2);

        tree_node_list *old_list = curr_list;
        curr_list = body;

        definept(&pt2);
        definept(&pt3); /* Note that this is not quite correct, but it's the
                           closest we can come to correct given the way
                           SUIF TREE_FORs are structured. */

        curr_list = old_list;
      }
    else
      {
        body = new tree_node_list;
        continue_label = genlabel();
        break_label = genlabel();
        tree_node_list *loop_test = new tree_node_list;
        label_sym *loop_top_label = genlabel();

        tree_loop *the_loop =
                new tree_loop(body, loop_test, continue_label, break_label,
                              loop_top_label);

        assert(curr_list != NULL);

        if (e1 != NULL)
          {
            curr_list->append(e1);
            delete e1;
          }

        tree_node_list *old_list = curr_list;

        if (e2_operand.is_null())
          {
            assert(e2_list == NULL);
            curr_list->append(the_loop);
          }
        else
          {
            label_sym *if_jumpto = genlabel();
            tree_node_list *if_header = new tree_node_list;
            tree_node_list *if_then = new tree_node_list;
            tree_node_list *if_else = new tree_node_list;
            tree_if *the_if =
                    new tree_if(if_jumpto, if_header, if_then, if_else);

            curr_list->append(the_if);
            if_then->append(the_loop);

            curr_list = if_header;

            definept(&pt2);
            if (e2_list != NULL)
              {
                tree_node_list *new_e2_list =
                        e2_list->clone(get_current_symtab());
                if_header->append(new_e2_list);
                delete new_e2_list;
              }

            in_bj *if_branch =
                    new in_bj(io_bfalse, if_jumpto, e2_operand.clone());
            if_header->append(new tree_instr(if_branch));
          }

        curr_list = loop_test;

        definept(&pt3);
        if (e3 != NULL)
          {
            loop_test->append(e3);
            delete e3;
          }

        definept(&pt2);
        if (e2_list != NULL)
          {
            loop_test->append(e2_list);
            delete e2_list;
          }

        in_bj *loop_branch;
        if (e2_operand.is_null())
            loop_branch = new in_bj(io_jmp, loop_top_label);
        else
            loop_branch = new in_bj(io_btrue, loop_top_label, e2_operand);
        loop_test->append(new tree_instr(loop_branch));

        curr_list = old_list;
      }

    tree_node_list *old_list = curr_list;
    curr_list = body;
    statement(continue_label, break_label, swp, lev);
    curr_list = old_list;
  }

static boolean c_for_to_suif_for(tree_for **suif_for, tree_node_list *c_init,
                                 tree_node_list *c_test_list,
                                 operand c_test_operand,
                                 tree_node_list *c_increment)
  {
    if (c_init == NULL)
        return FALSE;
    if (c_increment == NULL)
        return FALSE;

    tree_node *init_last_node = last_action_node(c_init);
    if (init_last_node == NULL)
        return FALSE;

    if (!init_last_node->is_instr())
        return FALSE;

    tree_instr *init_tree_instr = (tree_instr *)init_last_node;
    instruction *init_instr = init_tree_instr->instr();
    assert(init_instr != NULL);
    operand init_dest = init_instr->dst_op();

    if (!init_dest.is_symbol())
        return FALSE;
    sym_node *init_symbol = init_dest.symbol();
    assert(init_symbol != NULL);

    if (!init_symbol->is_var())
        return FALSE;
    var_sym *for_index = (var_sym *)init_symbol;

    if ((for_index->type()->unqual()->op() != TYPE_INT) &&
        (for_index->type()->unqual()->op() != TYPE_PTR))
      {
        return FALSE;
      }

    tree_node *increment_node = last_action_node(c_increment);
    if (increment_node == NULL)
        return FALSE;

    if (last_action_before(c_increment, increment_node) != NULL)
        return FALSE;

    if (!increment_node->is_instr())
        return FALSE;

    tree_instr *increment_tree_instr = (tree_instr *)increment_node;
    instruction *increment_instr = increment_tree_instr->instr();
    assert(increment_instr != NULL);

    operand increment_destination = increment_instr->dst_op();
    if (!increment_destination.is_symbol())
        return FALSE;

    if (increment_destination.symbol() != for_index)
        return FALSE;

    if ((increment_instr->opcode() != io_add) &&
        (increment_instr->opcode() != io_sub))
      {
        return FALSE;
      }

    in_rrr *increment_rrr = (in_rrr *)increment_instr;
    operand increment_op1 = increment_rrr->src1_op();
    operand increment_op2 = increment_rrr->src2_op();

    if ((increment_instr->opcode() == io_sub) &&
        non_negative(increment_op2.type()))
      {
            return FALSE;
      }

    if ((increment_instr->opcode() == io_add) &&
        ((!increment_op1.is_symbol()) ||
         (increment_op1.symbol() != for_index)))
      {
        operand temp_op = increment_op1;
        increment_op1 = increment_op2;
        increment_op2 = temp_op;
      }

    if (!increment_op1.is_symbol())
        return FALSE;

    if (increment_op1.symbol() != for_index)
        return FALSE;

    if (operand_may_have_side_effects(increment_op2))
        return FALSE;

    if (!for_index->type()->unqual()->is_base())
        return FALSE;
    boolean index_signed = !non_negative(for_index->type());

    operand for_ub;

    boolean is_gele = FALSE;
    if (increment_instr->opcode() == io_add)
      {
        is_gele = is_gele_test(c_test_list, c_test_operand, increment_op2,
                               for_index, &for_ub);
      }

    tree_for_test which_test;
    if (is_gele)
      {
        if (index_signed)
            which_test = FOR_SGELE;
        else
            which_test = FOR_UGELE;
      }
    else
      {
        if (c_test_operand.is_expr())
          {
            instruction *c_test_instr = c_test_operand.instr();
            assert(c_test_instr != NULL);

            if (c_test_list != NULL)
              {
                if (last_action_node(c_test_list) != NULL)
                    return FALSE;
              }

            if (c_test_instr->opcode() == io_seq)
              {
                which_test = FOR_EQ;
              }
            else if (c_test_instr->opcode() == io_sne)
              {
                which_test = FOR_NEQ;
              }
            else if (c_test_instr->opcode() == io_sl)
              {
                if (index_signed)
                    which_test = FOR_SLT;
                else
                    which_test = FOR_ULT;
              }
            else if (c_test_instr->opcode() == io_sle)
              {
                if (index_signed)
                    which_test = FOR_SLTE;
                else
                    which_test = FOR_ULTE;
              }
            else
              {
                return FALSE;
              }

            in_rrr *c_test_rrr = (in_rrr *)c_test_instr;
            operand c_test_left = c_test_rrr->src1_op();
            operand c_test_right = c_test_rrr->src2_op();

            if ((!c_test_left.is_symbol()) ||
                (c_test_left.symbol() != for_index))
              {
                operand temp_op = c_test_left;
                c_test_left = c_test_right;
                c_test_right = temp_op;

                switch (which_test)
                  {
                    case FOR_EQ:
                    case FOR_NEQ:
                        break;
                    case FOR_SLT:
                        which_test = FOR_SGT;
                        break;
                    case FOR_ULT:
                        which_test = FOR_UGT;
                        break;
                    case FOR_SLTE:
                        which_test = FOR_SGTE;
                        break;
                    case FOR_ULTE:
                        which_test = FOR_UGTE;
                        break;
                    default:
                        assert(FALSE);
                  }
              }

            if (!c_test_left.is_symbol())
                return FALSE;
            if (c_test_left.symbol() != for_index)
                return FALSE;

            for_ub = c_test_right;
          }
        else
          {
            return FALSE;
          }
      }

    if (operand_may_have_side_effects(for_ub))
        return FALSE;

    label_sym *continue_label = genlabel();
    label_sym *break_label = genlabel();
    tree_node_list *new_body = new tree_node_list;

    init_tree_instr->remove_instr(init_instr);
    c_init->remove(init_tree_instr->list_e());
    delete init_tree_instr->list_e();
    delete init_tree_instr;
    init_instr->set_dst(operand());
    operand for_lb = operand(init_instr);

    for_ub.remove();
    if (c_test_list != NULL)
        delete c_test_list;
    if (c_test_operand.is_expr())
        delete c_test_operand.instr();

    increment_op2.remove();

    if (increment_instr->opcode() == io_sub)
      {
        boolean folded = FALSE;
        if (increment_op2.is_expr() &&
            !increment_op2.type()->unqual()->is_enum())
          {
            instruction *the_instr = increment_op2.instr();
            if (the_instr->opcode() == io_ldc)
              {
                in_ldc *the_ldc = (in_ldc *)the_instr;
                immed value = the_ldc->value();
                if (value.is_int_const())
                  {
                    the_ldc->set_value(ii_to_immed(-immed_to_ii(value)));
                    folded = TRUE;
                  }
              }
          }
        if (!folded)
          {
            increment_op2 = operand(new in_rrr(io_neg, increment_op2.type(),
                                               operand(), increment_op2));
          }
      }

    delete c_increment;

    tree_node_list *for_landing_pad = new tree_node_list;

    tree_for *new_for =
            new tree_for(for_index, which_test, continue_label, break_label,
                         new_body, for_lb, for_ub, increment_op2,
                         for_landing_pad);
    *suif_for = new_for;

    return TRUE;
  }

static boolean is_gele_test(tree_node_list *c_test_list,
                            operand c_test_operand, operand step_op,
                            var_sym *for_index, operand *for_ub)
  {
    /*
     *  Pattern match to find GELE FORs.  The pattern we are looking for
     *  in the test expression is:
     *
     *      (<step expr> < 0) ? (index >= <expr1>) : (index <= <expr1>)
     *
     *  where <step expr> is the expression found above for the step and
     *  <expr1> is any expression.  <expr1> becomes the upper bound of the
     *  GELE TREE_FOR.
     *
     *  By the time we see it here, it has been put into SUIF code, so the
     *  ``?:'' conditional syntax has been turned into:
     *
     *      if (<step expr> < 0)
     *          tmp = (index >= <expr1>);
     *      else
     *          tmp = (index <= <expr1>);
     *      tmp != 0;
     */

    if (!c_test_operand.is_expr())
        return FALSE;
    instruction *c_test_instr = c_test_operand.instr();
    assert(c_test_instr != NULL);

    if (c_test_instr->opcode() != io_sne)
        return FALSE;
    in_rrr *c_test_rrr = (in_rrr *)c_test_instr;

    operand c_test_left = c_test_rrr->src1_op();
    operand c_test_right = c_test_rrr->src2_op();

    if (!c_test_left.is_symbol())
      {
        operand tmp_op = c_test_left;
        c_test_left = c_test_right;
        c_test_right = tmp_op;
      }

    if (!c_test_left.is_symbol())
        return FALSE;

    if (!(c_test_right.is_expr() &&
          (c_test_right.instr()->opcode() == io_ldc)))
      {
        return FALSE;
      }
    in_ldc *test_right_ldc = (in_ldc *)(c_test_right.instr());
    immed value = test_right_ldc->value();
    if (value != immed(0))
        return FALSE;

    var_sym *test_var = c_test_left.symbol();
    assert(test_var != NULL);
    if (test_var->is_userdef())
        return FALSE;
    if (c_test_list == NULL)
        return FALSE;

    tree_node *last_c_test_node = last_action_node(c_test_list);
    if (last_c_test_node == NULL)
        return FALSE;
    if (last_action_before(c_test_list, last_c_test_node) != NULL)
        return FALSE;
    if (!last_c_test_node->is_if())
        return FALSE;

    tree_if *last_if = (tree_if *)last_c_test_node;
    tree_node *last_then_node = last_action_node(last_if->then_part());
    tree_node *last_else_node = last_action_node(last_if->else_part());
    tree_node *last_test_node = last_action_node(last_if->header());

    if ((last_then_node == NULL) || (last_else_node == NULL) ||
        (last_test_node == NULL))
      {
        return FALSE;
      }
    if (last_action_before(last_if->then_part(), last_then_node) != NULL)
        return FALSE;
    if (last_action_before(last_if->else_part(), last_else_node) != NULL)
        return FALSE;
    if (last_action_before(last_if->header(), last_test_node) != NULL)
        return FALSE;

    if (!last_then_node->is_instr())
        return FALSE;
    if (!last_else_node->is_instr())
        return FALSE;
    if (!last_test_node->is_instr())
        return FALSE;

    tree_instr *last_then_tree_instr = (tree_instr *)last_then_node;
    tree_instr *last_else_tree_instr = (tree_instr *)last_else_node;
    tree_instr *last_test_tree_instr = (tree_instr *)last_test_node;

    instruction *last_then_instr = last_then_tree_instr->instr();
    instruction *last_else_instr = last_else_tree_instr->instr();
    instruction *last_test_instr = last_test_tree_instr->instr();
    assert(last_then_instr != NULL);
    assert(last_else_instr != NULL);
    assert(last_test_instr != NULL);

    boolean backward_compare;
    if (last_test_instr->opcode() == io_btrue)
        backward_compare = TRUE;
    else if (last_test_instr->opcode() == io_bfalse)
        backward_compare = FALSE;
    else
        return FALSE;

    in_bj *test_branch = (in_bj *)last_test_instr;

    if (test_branch->target() != last_if->jumpto())
        return FALSE;

    operand test_comparison_op = test_branch->src_op();
    if (!test_comparison_op.is_expr())
        return FALSE;

    instruction *test_comparison_instr = test_comparison_op.instr();
    assert(test_comparison_instr != NULL);
    if ((test_comparison_instr->opcode() != io_sl) &&
        (test_comparison_instr->opcode() != io_sle))
      {
        return FALSE;
      }
    in_rrr *test_comp_rrr = (in_rrr *)test_comparison_instr;
    operand test_left = test_comp_rrr->src1_op();
    operand test_right = test_comp_rrr->src2_op();
    if (test_comp_rrr->opcode() == io_sle)
      {
        operand temp_op = test_left;
        test_left = test_right;
        test_right = temp_op;
        backward_compare = !backward_compare;
      }
    if (!is_same_operand(test_left, step_op))
        return FALSE;
    if (!test_right.is_expr())
        return FALSE;
    instruction *right_instr = test_right.instr();
    assert(right_instr != NULL);
    if (right_instr->opcode() != io_ldc)
        return FALSE;
    in_ldc *right_ldc = (in_ldc *)right_instr;
    immed right_value = right_ldc->value();
    if (right_value != immed(0))
        return FALSE;

    if (!last_then_instr->dst_op().is_symbol())
        return FALSE;
    if (!last_else_instr->dst_op().is_symbol())
        return FALSE;
    if (last_then_instr->dst_op().symbol() != test_var)
        return FALSE;
    if (last_else_instr->dst_op().symbol() != test_var)
        return FALSE;

    while (last_then_instr->opcode() == io_cvt)
      {
        in_rrr *the_cvt = (in_rrr *)last_then_instr;
        operand src_op = the_cvt->src_op();
        if ((!src_op.is_expr()) ||
            (the_cvt->result_type()->op() != TYPE_INT) ||
            (src_op.type()->op() != TYPE_INT))
          {
            break;
          }
        base_type *cvt_base = (base_type *)(the_cvt->result_type());
        base_type *src_base = (base_type *)(src_op.type());
        if ((cvt_base->size() != src_base->size()) ||
            (cvt_base->is_signed() != src_base->is_signed()))
          {
            break;
          }
        last_then_instr = src_op.instr();
      }
    while (last_else_instr->opcode() == io_cvt)
      {
        in_rrr *the_cvt = (in_rrr *)last_else_instr;
        operand src_op = the_cvt->src_op();
        if ((!src_op.is_expr()) ||
            (the_cvt->result_type()->op() != TYPE_INT) ||
            (src_op.type()->op() != TYPE_INT))
          {
            break;
          }
        base_type *cvt_base = (base_type *)(the_cvt->result_type());
        base_type *src_base = (base_type *)(src_op.type());
        if ((cvt_base->size() != src_base->size()) ||
            (cvt_base->is_signed() != src_base->is_signed()))
          {
            break;
          }
        last_else_instr = src_op.instr();
      }

    if (last_then_instr->opcode() != io_sle)
        return FALSE;
    if (last_else_instr->opcode() != io_sle)
        return FALSE;

    in_rrr *last_then_rrr = (in_rrr *)last_then_instr;
    in_rrr *last_else_rrr = (in_rrr *)last_else_instr;
    operand last_then_left = last_then_rrr->src1_op();
    operand last_else_left = last_else_rrr->src1_op();
    operand last_then_right = last_then_rrr->src2_op();
    operand last_else_right = last_else_rrr->src2_op();
    if (backward_compare)
      {
        operand temp_op = last_then_left;
        last_then_left = last_then_right;
        last_then_right = temp_op;

        temp_op = last_else_left;
        last_else_left = last_else_right;
        last_else_right = temp_op;
      }

    if (!is_same_operand(last_then_left, last_else_right))
        return FALSE;
    if (!is_same_operand(last_then_right, last_else_left))
        return FALSE;

    if (!last_then_right.is_symbol())
        return FALSE;
    if (last_then_right.symbol() != for_index)
        return FALSE;

    *for_ub = last_then_left;
    return TRUE;
  }

/* ifstmt - if ( expression ) statement [ else statement ] */
static void ifstmt(label_sym *continue_lab, label_sym *break_lab,
                   struct swtch *swp, int lev)
  {
    t = gettok();
    expect('(');
    definept(NULL);

    genop e = conditional(')');
    e.clean_up_bit_field_refs();

    label_sym *else_label = genlabel();
    tree_node_list *if_test = e.precomputation();
    operand test_operand = e.suif_operand();
    instruction *branch_instr = new in_bj(io_bfalse, else_label, test_operand);
    if_test->append(new tree_instr(branch_instr));

    tree_node_list *then_part = new tree_node_list;
    tree_node_list *else_part = new tree_node_list;
    tree_if *the_if = new tree_if(else_label, if_test, then_part, else_part);
    assert(curr_list != NULL);
    curr_list->append(the_if);

    tree_node_list *old_list = curr_list;
    curr_list = then_part;
    refinc /= 2;
    if (refinc == 0)
        refinc = 1;
    statement(continue_lab, break_lab, swp, lev);

    if (t == ELSE)
      {
        curr_list = else_part;
        t = gettok();
        statement(continue_lab, break_lab, swp, lev);
      }

    curr_list = old_list;

  }

/* localaddr - returns q if p yields the address of local/parameter q;
   otherwise returns NULL */
static var_sym *localaddr(genop p)
  {
    if (!p.suif_operand().is_expr())
        return NULL;

    instruction *the_instr = p.suif_operand().instr();
    assert(the_instr != NULL);
    if (the_instr->opcode() != io_ldc)
        return NULL;

    in_ldc *the_ldc = (in_ldc *)the_instr;
    immed value = the_ldc->value();
    if (!value.is_symbol())
        return NULL;

    sym_node *the_symbol = value.symbol();
    if (!the_symbol->is_var())
        return NULL;

    var_sym *the_var = (var_sym *)the_symbol;
    if (the_var->parent()->is_block() && !the_var->is_static())
        return the_var;

    return NULL;
  }

extern void return_nothing(void)
  {
    type_node *return_type = freturn(cfunc->type);
    if ((return_type != voidtype) &&
        ((return_type != inttype) || (Aflag >= 1)))
      {
        warning("missing return value\n");
      }

    if (return_type->unqual()->op() == TYPE_VOID)
      {
        return_void();
      }
    else if ((return_type->unqual()->op() == TYPE_INT) ||
             (return_type->unqual()->op() == TYPE_PTR) ||
             (return_type->unqual()->op() == TYPE_ENUM))
      {
        return_value(constnode(0, return_type));
      }
    else if (return_type->unqual()->op() == TYPE_FLOAT)
      {
        return_value(genop(operand(new in_ldc(return_type, operand(),
                                              immed(0.0))),
                           new tree_node_list));
      }
    else
      {
        if (cfunc->u.f.null_return == NULL)
          {
            var_sym *new_var =
                    gen_static_local(return_type, "__dummy_return_value");
            var_def *the_def = begin_initialization(new_var);
            space(the_def, new_var->type()->size());
            cfunc->u.f.null_return = new_var;
          }
        return_value(sym_node_to_genop(cfunc->u.f.null_return));
      }
  }

/* return_void - return from the current function with no value */
static void return_void(void)
  {
    instruction *ret_instr =
            new in_rrr(io_ret, type_void, operand(), operand());
    curr_list->append(new tree_instr(ret_instr));
  }

/* return_value - return p from the current function */
static void return_value(genop to_return)
  {
    to_return = pointer(to_return);
    type_node *ty = assign(freturn(cfunc->type), to_return);
    if (ty != NULL)
      {
        to_return = cast(to_return, ty);
      }
    else
      {
        error("illegal return type; found `%t' expected `%t'\n",
              to_return.type(), freturn(cfunc->type));
      }

    if (isptr(to_return.type()))
      {
        var_sym *q = localaddr(to_return);
        if ((q != NULL) && (!q->is_userdef()))
          {
            warning("pointer to a %s is an illegal return value\n",
                    (q->is_param()) ? "parameter" : "local");
          }
        else if (q != NULL)
          {
            warning("pointer to %s `%s' is an illegal return value\n",
                    (q->is_param()) ? "parameter" : "local", q->name());
          }
      }
    curr_list->append(to_return.precomputation());
    delete to_return.precomputation();
    instruction *ret_instr =
            new in_rrr(io_ret, type_void, operand(), to_return.suif_operand());
    curr_list->append(new tree_instr(ret_instr));
  }

/* statement - parse statements */
void statement(label_sym *continue_lab, label_sym *break_lab,
               struct swtch *swp, int lev)
  {
    int ref = refinc;

    if ((Aflag >= 2) && (lev == 15))
        warning("more than 15 levels of nested statements\n");
    switch (t)
      {
        case IF:
            ifstmt(continue_lab, break_lab, swp, lev + 1);
            break;
        case WHILE:
            whilestmt(swp, lev + 1);
            break;
        case DO:
            dostmt(swp, lev + 1);
            expect(';');
            break;
        case FOR:
            forstmt(swp, lev + 1);
            break;
        case SWITCH:
            swstmt(continue_lab, lev + 1);
            break;
        case CASE:
          {
            if (swp == NULL)
                error("illegal case label\n");

            label_sym *this_lab = genlabel();
            insert_label(this_lab);

            while (t == CASE)
              {
                t = gettok();
                genop p = constexpr(':');

                immed the_constant;
                eval_status status = p.evaluate_as_const(&the_constant);
                if (((status == EVAL_OK) || (status == EVAL_OVERFLOW)) &&
                    (!the_constant.is_int_const()))
                  {
                    boolean old_needconst = needconst;

                    needconst = TRUE;
                    p = cast(p, swp->key_type);
                    needconst = old_needconst;
                    status = p.evaluate_as_const(&the_constant);
                  }
                p.deallocate();

                switch (status)
                  {
                    case EVAL_OVERFLOW:
                        warning("overflow in case label constant "
                                "expression\n");
                        /* fall through */
                    case EVAL_OK:
                        if (the_constant.is_int_const())
                          {
                            caselabel(swp, immed_to_ii(the_constant),
                                      this_lab);
                          }
                        else
                          {
                            error("case label must be a constant integer "
                                  "expression");
                          }
                        break;
                    case EVAL_NOT_CONST:
                        error("case label must be a constant expression");
                        break;
                    case EVAL_DIV_BY_ZERO:
                        error("division by zero in case label constant "
                              "expression\n");
                        break;
                    case EVAL_UNKNOWN_AT_LINK:
                        error("case label constant expression value cannot "
                              "be computed at link time\n");
                        break;
                    default:
                        assert(FALSE);
                  }
              }
            statement(continue_lab, break_lab, swp, lev);
            break;
          }
        case DEFAULT:
            if (swp != NULL)
              {
                if (swp->deflab != NULL)
                  {
                    error("extra default label\n");
                  }
                else
                  {
                    swp->deflab = genlabel();
                    insert_label(swp->deflab);
                  }
              }
            else
              {
                error("illegal default label\n");
              }
            t = gettok();
            expect(':');
            statement(continue_lab, break_lab, swp, lev);
            break;
        case RETURN:
            t = gettok();
            definept(NULL);
            if (t != ';')
              {
                if (freturn(cfunc->type) == voidtype)
                  {
                    error("extraneous return value\n");
                    genop the_value = expr(0);
                    the_value.deallocate();
                    return_void();
                  }
                else
                  {
                    genop the_value = expr(0);
                    the_value.clean_up_bit_field_refs();
                    return_value(the_value);
                  }
              }
            else
              {
                return_nothing();
              }
            expect(';');
            break;
        case BREAK:
            definept(NULL);
            if (break_lab != NULL)
                branch(break_lab);
            else
                error("illegal %k statement\n", t);
            t = gettok();
            expect(';');
            break;
        case CONTINUE:
            definept(NULL);
            if (continue_lab != NULL)
                branch(continue_lab);
            else
                error("illegal %k statement\n", t);
            t = gettok();
            expect(';');
            break;
        case '{':
            compound(continue_lab, break_lab, swp, lev + 1);
            break;
        case ';':
            definept(NULL);
            t = gettok();
            break;
        case GOTO:
            definept(NULL);
            t = gettok();
            if (t == ID)
              {
                Symbol p = lookup(token, labels[0]);
                if (p == NULL)
                  {
                    p = install(token, &labels[0], 0);
                    p->suif_symbol = gen_user_label(token);
                    p->src = src;
                  }
                p->ref++;
                use(p, src);
                branch((label_sym *)(p->suif_symbol));
                t = gettok();
              }
            else
              {
                error("missing label in goto\n");
              }
            expect(';');
            break;
        case ID:
            if (getchr() == ':')
              {
                stmtlabel(token);
                statement(continue_lab, break_lab, swp, lev);
                break;
              }
            /* fall through */
        default:
            definept(NULL);
            if (kind[t] != ID)
              {
                error("unrecognized statement\n");
                t = gettok();
              }
            else
              {
                tree_node_list *e = expr0(0);
                curr_list->append(e);
                delete e;
              }
            expect(';');
      }
    if ((kind[t] != IF) && (kind[t] != ID) && (t != '}') && (t != EOI))
      {
        static char follow[] = { IF, ID, '}', 0 };
        error("illegal statement termination\n");
        skipto(0, follow);
      }
    refinc = ref;
  }

/* stmtlabel - label : */
static void stmtlabel(char *label)
  {
    Symbol p = lookup(label, labels[0]);

    if (p == NULL)
      {
        p = install(label, &labels[0], 0);
        p->suif_symbol = gen_user_label(label);
      }
    if (p->defined)
      {
        error("redefinition of label `%s' previously defined at %w\n", label,
              &p->src);
      }
    p->defined = 1;
    p->src = src;
    insert_label((label_sym *)(p->suif_symbol));
    t = gettok();
    expect(':');
  }

/* swcode - generate switch decision code for buckets b[lb..ub] */
static int swcode(struct swtch *swp, operand key, int b[], int lb, int ub,
                  int n)
  {
    int k, l, u;
    i_integer median, *v;

    v = swp->values;
    median = ((v[b[lb]] + v[b[ub + 1] - 1] + 1) / 2);
    for (l = lb, u = ub; l <= u; )
      {
        k = (l + u)/2;
        if (v[b[k]] > median)
            u = k - 1;
        else if (v[b[k+1]-1] < median)
            l = k + 1;
        else
            break;
      }
    if (l > u)      /* which of two buckets is closest to the median? */
      {
        l = b[k];
        u = b[k+1] - 1;
        if ((median < v[l]) && (k > lb) && (v[l] - median > median - v[l-1]))
          {
            k--;
          }
        else if ((median > v[u]) && (k < ub) &&
                 (median - v[u] > v[u+1] - median))
          {
            k++;
          }
      }
    l = b[k];
    u = b[k + 1] - 1;

    label_sym *lolab;
    label_sym *hilab;
    lolab = hilab = swp->deflab;
    if (k > lb)
      {
        lolab = NULL;
        if (k < ub)
           hilab = genlabel();
      }
    else if (k < ub)
      {
        hilab = NULL;
      }
    if (n == 0)
        lolab = hilab = NULL;

    i_integer lower_value = swp->values[l];
    i_integer upper_value = swp->values[u];
    i_integer num_values = upper_value - lower_value + 1;

    if ((u - l + 1 <= 3) || (!lower_value.is_c_int()) ||
        (!upper_value.is_c_int()) || (!num_values.is_c_int()))
      {
        int i;
        for (i = l; i <= u; i++)
            cmp(EQ, key.clone(), swp->values[i], swp->labels[i]);
        if ((lolab != NULL) && (lolab == hilab))
          {
            branch(lolab);
          }
        else
          {
            if (lolab != NULL)
                cmp(LT, key.clone(), swp->values[l], lolab);
            if (hilab != NULL)
                cmp(GT, key.clone(), swp->values[u], hilab);
          }
      }
    else
      {
        label_sym *skip_mbr = genlabel();

        if (num_values >= 10000)
            warning("switch generates a huge table\n");

        cmp(LT, key.clone(), lower_value, (lolab != NULL) ? lolab : skip_mbr);
        cmp(GT, key.clone(), upper_value, (hilab != NULL) ? hilab : skip_mbr);

        in_mbr *the_mbr =
                new in_mbr(key.clone(), lower_value.c_int(),
                           num_values.c_int(), swp->deflab);

        int lower_int = lower_value.c_int();
        int upper_int = upper_value.c_int();
        for (int case_num = lower_int; case_num <= upper_int; ++case_num)
            the_mbr->set_label(case_num - lower_int, swp->deflab);
        for (int bin_num = l; bin_num <= u; ++bin_num)
          {
            the_mbr->set_label((swp->values[bin_num] - lower_value).c_int(),
                               swp->labels[bin_num]);
          }

        curr_list->append(new tree_instr(the_mbr));
        insert_label(skip_mbr);
      }

    if (k > lb)
        n = swcode(swp, key, b, lb, k - 1, n - 1);
    if (k < ub)
      {
        if (hilab != NULL)
            insert_label(hilab);
        n = swcode(swp, key, b, k + 1, ub, n - 1);
      }
    return n;
  }

/* swgen - partition case labels into buckets, initiate code generation */
#define den(k,n) (i_rational(k-buckets[n]+1)/i_rational(v[k]-v[buckets[n]]+1))
static void swgen(struct swtch *swp, operand key)
  {
    int *buckets, k, n;
    i_integer *v;

    if (swp->ncases == 0)
      {
        branch(swp->deflab);
        return;
      }
    buckets = new int[swp->ncases + 1];
    v = swp->values;
    n = 0;
    for (k = 0; k < swp->ncases; k++, n++)
      {
        buckets[n] = k;
        while (n > 0)
          {
            i_rational d = den(k, n - 1);
            if ((d < density) ||
                ((k < swp->ncases - 1) && (d < den(k + 1, n))))
              {
                break;
              }
            n--;
          }
      }
    buckets[n--] = swp->ncases;
    swcode(swp, key, buckets, 0, n, n);
    delete[] buckets;

    branch(swp->deflab);
  }

/* swstmt - switch ( expression ) statement */
static void swstmt(label_sym *continue_lab, int lev)
  {
    t = gettok();
    expect('(');
    definept(NULL);
    genop e = expr(')');
    e.clean_up_bit_field_refs();

    if (!isint(e.type()))
      {
        error("illegal type `%t' in switch expression\n", e.type());
        e = cast(e, inttype);
      }
    e = cast(e, promote(e.type()));

    e.make_temporary();

    label_sym *break_lab = genlabel();

    struct swtch sw;
    sw.deflab = NULL;
    sw.ncases = 0;
    sw.size = SWITCHSIZE;
    sw.values = new i_integer[SWITCHSIZE];
    sw.labels = new label_sym *[SWITCHSIZE];
    sw.key_type = e.type();

    refinc /= 10;
    if (refinc == 0)
        refinc = 1;

    tree_node_list *switch_body = new tree_node_list;
    tree_node_list *old_list = curr_list;
    curr_list = switch_body;
    statement(continue_lab, break_lab, &sw, lev);
    curr_list = old_list;

    if (sw.deflab == NULL)
      {
        sw.deflab = break_lab;
        if (sw.ncases == 0)
            warning("switch statement with no cases\n");
      }

    tree_node_list *precomputation = e.precomputation();
    curr_list->append(precomputation);
    delete precomputation;
    swgen(&sw, e.suif_operand());
    if (e.suif_operand().is_expr())
        delete e.suif_operand().instr();

    delete[] sw.values;
    delete[] sw.labels;

    old_list->append(switch_body);
    delete switch_body;

    insert_label(break_lab);
  }

/* whilestmt - while ( expression ) statement */
static void whilestmt(struct swtch *swp, int lev)
  {

    refinc *= 10;
    t = gettok();
    expect('(');
    Coordinate pt = src;

    /* L is top of loop, L+1 is continue, L+2/L+3 is break */
    /* while (e) s ==> if (e) { do s while (e) } */

    genop e = conditional(')');
    e.clean_up_bit_field_refs();

    definept(&pt);

    label_sym *else_label = genlabel();
    tree_node_list *if_test = e.precomputation()->clone(get_current_symtab());
    operand test_operand = e.suif_operand().clone();
    instruction *branch_instr = new in_bj(io_bfalse, else_label, test_operand);
    if_test->append(new tree_instr(branch_instr));

    tree_node_list *then_part = new tree_node_list;
    tree_node_list *else_part = new tree_node_list;
    tree_if *the_if = new tree_if(else_label, if_test, then_part, else_part);
    assert(curr_list != NULL);
    curr_list->append(the_if);

    label_sym *continue_lab = genlabel();
    label_sym *break_lab = genlabel();
    label_sym *top_lab = genlabel();
    tree_node_list *loop_body = new tree_node_list;

    tree_node_list *loop_test = e.precomputation();
    operand loop_test_op = e.suif_operand().clone();
    in_bj *loop_branch = new in_bj(io_btrue, top_lab, loop_test_op);
    loop_test->append(new tree_instr(loop_branch));

    tree_loop *the_loop =
            new tree_loop(loop_body, loop_test, continue_lab, break_lab,
                          top_lab);
    then_part->append(the_loop);

    tree_node_list *old_list = curr_list;
    curr_list = loop_body;
    statement(continue_lab, break_lab, swp, lev);
    curr_list = old_list;
  }
