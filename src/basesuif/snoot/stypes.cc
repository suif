/* file "stypes.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot type handling */

#include "c.h"

    /* Basic types: */

type_node *chartype;              /* char */
type_node *doubletype;            /* double */
type_node *floattype;             /* float */
type_node *inttype;               /* signed int */
type_node *longdouble;            /* long double */
type_node *longtype;              /* long */
type_node *longlong;              /* long long */
type_node *shorttype;             /* signed short int */
type_node *signedchar;            /* signed char */
type_node *unsignedchar;          /* unsigned char */
type_node *unsignedlong;          /* unsigned long int */
type_node *unsignedlonglong;      /* unsigned long long int */
type_node *unsignedshort;         /* unsigned short int */
type_node *unsignedtype;          /* unsigned int */
type_node *voidptype;             /* void* */
type_node *voidtype;              /* void */
type_node *booleantype;           /* for internal use */
type_node *size_t_type;           /* whatever size_t is on the target
                                     machine, one of unsignedchar,
                                     unsignedshort, unsignedtype,
                                     unsignedlong, or unsignedlonglong
                                     */


    /* Symbol table entries of basic types: */

Symbol chartype_sym;           /* char */
Symbol doubletype_sym;         /* double */
Symbol floattype_sym;          /* float */
Symbol inttype_sym;            /* signed int */
Symbol longdouble_sym;         /* long double */
Symbol longtype_sym;           /* long */
Symbol longlong_sym;           /* long long */
Symbol shorttype_sym;          /* signed short int */
Symbol signedchar_sym;         /* signed char */
Symbol unsignedchar_sym;       /* unsigned char */
Symbol unsignedlong_sym;       /* unsigned long int */
Symbol unsignedlonglong_sym;   /* unsigned long long int */
Symbol unsignedshort_sym;      /* unsigned short int */
Symbol unsignedtype_sym;       /* unsigned int */
Symbol voidtype_sym;           /* void */

replacements *global_replacements;

static base_symtab *type_symtab; /* table to put types when there is nothing
                                    else to go by */

static void init_base_type(type_node **the_type_node, char *name,
                           type_ops the_op, boolean is_signed, int size,
                           int alignment, Symbol *the_symbol);
static boolean identically_qualified(type_node *type1, type_node *type2);
static boolean bit_field_annotations_same(type_node *field1,
                                          type_node *field2);
static void use_field(struct_type *the_struct, char *name);
static Coordinate *get_source(type_node *the_type);
static void set_source(type_node *the_type, Coordinate source);

/* typeInit - initialize basic types */
void typeInit(void)
  {
    int c_type;

    target.is_big_endian = this_target->is_big_endian;
    target.addressable_size = this_target->addressable_size;
    target.char_is_signed = this_target->char_is_signed;

    for (c_type = 0; c_type < num_C_types; ++c_type)
      {
        target.size[c_type] = this_target->size[c_type];
        target.align[c_type] = this_target->align[c_type];
      }

    target.array_align = this_target->array_align;
    target.struct_align = this_target->struct_align;

    target.ptr_diff_type = this_target->ptr_diff_type;

    /*
     *  Sanity check on alignments
     */
    int c_type_1;
    for (c_type_1 = C_char; c_type_1 <= C_longlong; ++c_type_1)
      {
        for (int c_type_2 = C_char; c_type_2 <= C_longlong; ++c_type_2)
          {
            if ((target.size[c_type_1] == target.size[c_type_2]) &&
                (target.align[c_type_1] != target.align[c_type_2]))
              {
                error_line(0, NULL,
                           "integral types with size %d bits have "
                           "conflicting", target.size[c_type_1]);
                error_line(1, NULL,
                           "  alignment requirements of %d and %d bits",
                           target.align[c_type_1], target.align[c_type_2]);
              }
          }
      }
    for (c_type_1 = C_float; c_type_1 <= C_longdouble; ++c_type_1)
      {
        for (int c_type_2 = C_float; c_type_2 <= C_longdouble; ++c_type_2)
          {
            if ((target.size[c_type_1] == target.size[c_type_2]) &&
                (target.align[c_type_1] != target.align[c_type_2]))
              {
                error_line(0, NULL,
                           "floating point types with size %d bits have "
                           "conflicting", target.size[c_type_1]);
                error_line(1, NULL,
                           "  alignment requirements of %d and %d bits",
                           target.align[c_type_1], target.align[c_type_2]);
              }
          }
      }

    fileset->globals()->predefine_types();
    type_symtab = type_signed->parent();

    global_replacements = new replacements;

    init_base_type(&chartype,         "char",                   TYPE_INT,
                   target.char_is_signed, target.size[C_char],
                   target.align[C_char],       &chartype_sym);
    init_base_type(&doubletype,       "double",                 TYPE_FLOAT,
                   TRUE,                  target.size[C_double],
                   target.align[C_double],     &doubletype_sym);
    init_base_type(&floattype,        "float",                  TYPE_FLOAT,
                   TRUE,                  target.size[C_float],
                   target.align[C_float],      &floattype_sym);
    init_base_type(&inttype,          "int",                    TYPE_INT,
                   TRUE,                  target.size[C_int],
                   target.align[C_int],        &inttype_sym);
    init_base_type(&longdouble,       "long double",            TYPE_FLOAT,
                   TRUE,                  target.size[C_longdouble],
                   target.align[C_longdouble], &longdouble_sym);
    init_base_type(&longtype,         "long int",               TYPE_INT,
                   TRUE,                  target.size[C_long],
                   target.align[C_long],       &longtype_sym);
    init_base_type(&longlong,         "long long int",          TYPE_INT,
                   TRUE,                  target.size[C_longlong],
                   target.align[C_longlong],   &longlong_sym);
    init_base_type(&shorttype,        "short",                  TYPE_INT,
                   TRUE,                  target.size[C_short],
                   target.align[C_short],      &shorttype_sym);
    init_base_type(&signedchar,       "signed char",            TYPE_INT,
                   TRUE,                  target.size[C_char],
                   target.align[C_char],       &signedchar_sym);
    init_base_type(&unsignedchar,     "unsigned char",          TYPE_INT,
                   FALSE,                 target.size[C_char],
                   target.align[C_char],       &unsignedchar_sym);
    init_base_type(&unsignedlong,     "unsigned long int",      TYPE_INT,
                   FALSE,                 target.size[C_long],
                   target.align[C_long],       &unsignedlong_sym);
    init_base_type(&unsignedlonglong, "unsigned long long int", TYPE_INT,
                   FALSE,                 target.size[C_longlong],
                   target.align[C_longlong],   &unsignedlonglong_sym);
    init_base_type(&unsignedshort,    "unsigned short int",     TYPE_INT,
                   FALSE,                 target.size[C_short],
                   target.align[C_short],      &unsignedshort_sym);
    init_base_type(&unsignedtype,     "unsigned int",           TYPE_INT,
                   FALSE,                 target.size[C_int],
                   target.align[C_int],        &unsignedtype_sym);
    init_base_type(&voidtype,         "void",                   TYPE_VOID,
                   FALSE,                 0,
                   0,                          &voidtype_sym);

    /* In some places, type_ptr_diff is used as an expression result
     * type, but other pieces assume that expression result types that
     * are integers will be exactly one of the standard C types.  So
     * we can't just rely on what predefine_types() to set
     * type_ptr_diff; we have to set it here to one of the standard C
     * integer types we just created. */
    switch (target.ptr_diff_type)
      {
        case C_char:
            type_ptr_diff = signedchar;
            break;
        case C_short:
            type_ptr_diff = shorttype;
            break;
        case C_int:
            type_ptr_diff = inttype;
            break;
        case C_long:
            type_ptr_diff = longtype;
            break;
        case C_longlong:
            type_ptr_diff = longlong;
            break;
        default:
            assert(FALSE);
      }

    voidptype = voidtype->ptr_to();

    booleantype = new base_type(TYPE_INT, type_signed->size(), TRUE);
    immed_list *new_immed_list = new immed_list;
    new_immed_list->append(immed("boolean"));
    booleantype->append_annote(k_type_name, new_immed_list);
    booleantype = type_symtab->install_type(booleantype);

    /*
     *  We assume that size_t is the unsigned version of ptrdiff_t.
     *  This isn't technically required by the ANSI standard, but it's
     *  likely to be true on almost any real back-end C compiler, and
     *  even if it's not, it is very unlikely that the difference will
     *  ever be noticed.
     */
    switch (target.ptr_diff_type)
      {
        case C_char:
            size_t_type = unsignedchar;
            break;
        case C_short:
            size_t_type = unsignedshort;
            break;
        case C_int:
            size_t_type = unsignedtype;
            break;
        case C_long:
            size_t_type = unsignedlong;
            break;
        case C_longlong:
            size_t_type = unsignedlonglong;
            break;
        default:
            assert(FALSE);
      }
    {
	/* 
	 *  BUILTIN in config.h only seems to handle builin expressions
	 *  and this is a built-in type.
	 */
	Coordinate c = { "", 0, 0 };
	deftype(string("__builtin_va_list"), chartype->ptr_to(), &c);
    }
  }

static void init_base_type(type_node **the_type_node, char *name,
                           type_ops the_op, boolean is_signed, int size,
                           int alignment, Symbol *the_symbol)
  {
    *the_symbol = install(string(name), &types, 1);
    *the_type_node = new base_type(the_op, size, is_signed);
    immed_list *new_immed_list = new immed_list;
    new_immed_list->append(immed(name));
    (*the_type_node)->append_annote(k_type_name, new_immed_list);
    *the_type_node = type_symtab->install_type(*the_type_node);
    (*the_symbol)->type = *the_type_node;
    assert((alignment == 0) || (size % alignment == 0));

    type_node *merged_version = new base_type(the_op, size, is_signed);
    merged_version = type_symtab->install_type(merged_version);
    global_replacements->oldtypes.append(*the_type_node);
    global_replacements->newtypes.append(merged_version);
  }

/* array - construct the type `array 0..n-1 of ty' */
extern type_node *build_array(type_node *ty, int n)
  {
    assert(ty != NULL);

    if (isfunc(ty))
      {
        error("illegal type `array of %t'\n", ty);
        return build_array(inttype, n);
      }

    if ((level > GLOBAL) && isarray(ty) && (ty->size() == 0))
        error("missing array size\n");
    if (ty->size() == 0)
      {
        if (ty->unqual() == voidtype)
            error("illegal type `array of %t'\n", ty);
        else if (Aflag >= 2)
            warning("declaring type `array of %t' is undefined\n", ty);
      }
    else if (n > INT_MAX / ty->size())
      {
        error("size of `array of %t' exceeds %d bytes\n", ty, INT_MAX);
        n = 1;
      }

    base_symtab *the_symtab = ty->parent();
    assert(the_symtab != NULL);
    array_bound upper_bound = unknown_bound;
    if (n > 0)
        upper_bound = array_bound(n - 1);
    type_node *new_type = new array_type(ty, array_bound(0), upper_bound);
    return the_symtab->install_type(new_type);
  }

/* atop - convert ty from `array of ty' to `pointer to ty' */
extern type_node *atop(type_node *ty)
  {
    if (isarray(ty))
        return base_from_array(ty)->ptr_to();
    error("type error: %s\n", "array expected");
    return ty->ptr_to();
  }

/* composite - return the composite type of ty1 & ty2, or NULL if ty1 & ty2 are
   incompatible */
extern type_node *composite(type_node *ty1, type_node *ty2)
  {
    if ((ty1 == NULL) || (ty2 == NULL))
        return NULL;
    if (ty1 == ty2)
        return ty1;
    if ((ty1->op() != ty2->op()) &&
        ((!ty1->is_modifier()) || (!ty2->is_modifier())))
      {
        return NULL;
      }
    switch (ty1->op())
      {
        case TYPE_CONST:
        case TYPE_VOLATILE:
          {
            if (!identically_qualified(ty1, ty2))
                return NULL;
            type_node *new_type = composite(ty1->unqual(), ty2->unqual());
            if (new_type == NULL)
                return NULL;
            if (ty1->is_const())
                new_type = qual(CONST, new_type);
            if (ty1->is_volatile())
                new_type = qual(VOLATILE, new_type);
            return new_type;
          }
        case TYPE_PTR:
          {
            type_node *new_type =
                    composite(base_from_pointer(ty1), base_from_pointer(ty2));
            if (new_type == NULL)
                return NULL;
            return new_type->ptr_to();
          }
        case TYPE_ARRAY:
          {
            type_node *new_type =
                    composite(base_from_array(ty1), base_from_array(ty2));
            if (new_type == NULL)
                return NULL;

            array_type *old_array1 = (array_type *)ty1;
            array_type *old_array2 = (array_type *)ty2;
            int upper_bound = 0;
            if (!old_array1->are_bounds_unknown())
                upper_bound = old_array1->upper_bound().constant() + 1;
            if (!old_array2->are_bounds_unknown())
              {
                if ((upper_bound != 0) &&
                    (upper_bound != old_array2->upper_bound().constant() + 1))
                  {
                    return NULL;
                  }
                upper_bound = old_array2->upper_bound().constant() + 1;
              }
            return build_array(new_type, upper_bound);
          }
        case TYPE_FUNC:
          {
            func_type *old_func1 = (func_type *)ty1;
            func_type *old_func2 = (func_type *)ty2;
            type_node *new_type =
                    composite(old_func1->return_type(),
                              old_func2->return_type());
            if (new_type == NULL)
                return NULL;

            unsigned num_args = 0;
            boolean args_unknown = TRUE;
            boolean varargs = FALSE;
            if (old_func1->args_known() && old_func2->args_known())
              {
                num_args = old_func1->num_args();
                if (num_args != old_func2->num_args())
                    return NULL;
                args_unknown = FALSE;
                varargs = old_func1->has_varargs();
                if (varargs != old_func2->has_varargs())
                    return NULL;
              }
            else if (old_func1->args_known() && !old_func2->args_known())
              {
                return old_func1;
              }
            else if (old_func2->args_known() && !old_func1->args_known())
              {
                return old_func2;
              }

            base_symtab *the_symtab = fileset->globals();

            boolean vis = the_symtab->make_type_visible(new_type);
            assert(vis);
            func_type *new_func = new func_type(new_type, num_args, varargs);

            if (args_unknown)
              {
                new_func->set_args_unknown();
              }
            else
              {
                new_func->set_args_known();
                for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                  {
                    type_node *arg1_type = old_func1->arg_type(arg_num);
                    type_node *arg2_type = old_func2->arg_type(arg_num);
                    type_node *new_arg = composite(arg1_type->unqual(),
                                                   arg2_type->unqual());
                    if (new_arg == NULL)
                      {
                        delete new_func;
                        return NULL;
                      }
                    if (isconst(arg1_type) || isconst(arg2_type))
                        new_arg = qual(CONST, new_arg);
                    if (isvolatile(arg1_type) || isvolatile(arg2_type))
                        new_arg = qual(VOLATILE, new_arg);

                    vis = the_symtab->make_type_visible(new_arg);
                    assert(vis);
                    new_func->set_arg_type(arg_num, new_arg);
                  }
              }

            return the_symtab->install_type(new_func);
          }
        default:
            break;
      }
    return NULL;
  }

/* deftype - define name to be equivalent to type ty */
extern Symbol deftype(char *name, type_node *ty, Coordinate *pos)
  {
    Symbol p = lookup(name, identifiers);

    if ((p != NULL) && (p->scope == level))
        error("redeclaration of `%s'\n", name);
    p = install(name, &identifiers, level < LOCAL);
    p->type = ty;
    p->sclass = TYPEDEF;
    p->src = *pos;
    if (option_keep_typedef_info)
      {
        base_symtab *current_symtab = get_current_symtab();
        var_sym *typedef_var = current_symtab->new_var(type_signed, name);
        if (current_symtab->is_file())
          {
            current_symtab->define_var(typedef_var,
                                       get_alignment(type_signed));
          }
        typedef_var->append_annote(k_typedef_name, new immed_list(ty));
      }
    return p;
  }

/* deref - dereference ty, type *ty */
extern type_node *deref(type_node *ty)
  {
    if (isptr(ty))
        return base_from_pointer(ty);
    else
        error("type error: %s\n", "pointer expected");
    return ty;
  }

/* eqtype - is ty1==ty2?  handles arrays & functions; return ret if ty1==ty2,
   but one is incomplete */
extern boolean eqtype(type_node *ty1, type_node *ty2, boolean ret)
  {
    if (ty1 == ty2)
        return TRUE;
    if ((ty1 == NULL) || (ty2 == NULL))
        return FALSE;

    if ((ty1->op() != ty2->op()) &&
        ((!ty1->is_modifier()) || (!ty2->is_modifier())))
      {
        return FALSE;
      }
    switch (ty1->op())
      {
        case TYPE_CONST:
        case TYPE_VOLATILE:
            if (!identically_qualified(ty1, ty2))
                return FALSE;
            return eqtype(ty1->unqual(), ty2->unqual(), ret);
        case TYPE_PTR:
            return eqtype(base_from_pointer(ty1), base_from_pointer(ty2), ret);
        case TYPE_ARRAY:
          {
            array_type *old_array1 = (array_type *)ty1;
            array_type *old_array2 = (array_type *)ty2;

            if (!eqtype(old_array1->elem_type(), old_array2->elem_type(), ret))
                return FALSE;
            if (old_array1->upper_bound() == old_array2->upper_bound())
                return TRUE;
            if (old_array1->are_bounds_unknown() !=
                old_array2->are_bounds_unknown())
              {
                return ret;
              }
            break;
          }
        case TYPE_FUNC:
          {
            func_type *old_func1 = (func_type *)ty1;
            func_type *old_func2 = (func_type *)ty2;
            if (!eqtype(old_func1->return_type(), old_func2->return_type(),
                        ret))
              {
                return FALSE;
              }

            if (old_func1->args_known() && old_func2->args_known())
              {
                unsigned num_args = old_func1->num_args();
                if (num_args != old_func2->num_args())
                    return FALSE;
                if (old_func1->has_varargs() != old_func2->has_varargs())
                    return FALSE;
                for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                  {
                    if (!eqtype(old_func1->arg_type(arg_num)->unqual(),
                                old_func2->arg_type(arg_num)->unqual(), ret))
                      {
                        return FALSE;
                      }
                  }
                return TRUE;
              }
            else if (old_func1->args_known() && !old_func2->args_known())
              {
                func_type *temp_func = old_func1;
                old_func1 = old_func2;
                old_func2 = temp_func;
              }
            if (!old_func2->args_known())
                return TRUE;

            if (old_func2->has_varargs())
                return (old_func2->num_args() == 0);

            unsigned num_args = old_func2->num_args();
            for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
              {
                type_node *arg_type = old_func2->arg_type(arg_num)->unqual();
                if ((promote(arg_type) != arg_type) || (arg_type == floattype))
                    return FALSE;
              }

            return TRUE;
          }
        default:
            break;
      }
    return FALSE;
  }

/* fieldref - find field name of type ty, return entry */
Field fieldref(char *name, type_node *ty)
  {
    static struct field the_field;

    assert(ty != NULL);
    assert(ty->is_struct());
    struct_type *the_struct = (struct_type *)ty;
    unsigned num_fields = the_struct->num_fields();
    for (unsigned field_num = 0; field_num < num_fields; ++field_num)
      {
        if (field_is_bit_fields(the_struct, field_num))
          {
            struct field *bit_fields = bit_field_list(the_struct, field_num);
            while (bit_fields != NULL)
              {
                if (strcmp(bit_fields->name, name) == 0)
                  {
                    the_field = *bit_fields;
                    the_field.link = NULL;
                    while (bit_fields != NULL)
                      {
                        struct field *old_field = bit_fields;
                        bit_fields = bit_fields->link;
                        delete old_field;
                      }
                    the_field.offset = the_struct->offset(field_num);
                    the_field.block_name = the_struct->field_name(field_num);
                    the_field.block_type = the_struct->field_type(field_num);
                    use_field(the_struct, name);
                    return &the_field;
                  }

                struct field *old_field = bit_fields;
                bit_fields = bit_fields->link;
                delete old_field;
              }
          }
        else
          {
            if (strcmp(the_struct->field_name(field_num), name) == 0)
              {
                the_field.name = the_struct->field_name(field_num);
                the_field.type = the_struct->field_type(field_num);
                the_field.offset = the_struct->offset(field_num);
                the_field.from = 0;
                the_field.to = 0;
                the_field.block_name = the_struct->field_name(field_num);
                the_field.block_type = the_struct->field_type(field_num);
                the_field.link = NULL;
                use_field(the_struct, name);
                return &the_field;
              }
          }
      }
    return NULL;
  }

/* freturn - for `function returning ty', return ty */
extern type_node *freturn(type_node *ty)
  {
    assert(ty != NULL);
    if (isfunc(ty))
      {
        func_type *the_func = (func_type *)ty;
        return the_func->return_type();
      }
    error("type error: %s\n", "function expected");
    return inttype;
  }

/* func - construct the type `function (void) returning ty' */
extern func_type *func(type_node *ty)
  {
    assert(ty != NULL);
    if (isarray(ty) || isfunc(ty))
        error("illegal return type `%t'\n", ty);

    base_symtab *the_symtab = ty->parent();
    assert(the_symtab != NULL);
    func_type *new_type = new func_type(ty);
    return (func_type *)(the_symtab->install_type(new_type));
  }

/* hasproto - true iff ty has no function types or they all have prototypes */
extern boolean hasproto(type_node *ty)
  {
    assert(ty != NULL);
    switch (ty->op())
      {
        case TYPE_CONST:
        case TYPE_VOLATILE:
          {
            modifier_type *the_modifier = (modifier_type *)ty;
            return hasproto(the_modifier->base());
          }
        case TYPE_PTR:
          {
            ptr_type *the_pointer = (ptr_type *)ty;
            return hasproto(the_pointer->ref_type());
          }
        case TYPE_ARRAY:
          {
            array_type *the_array = (array_type *)ty;
            return hasproto(the_array->elem_type());
          }
        case TYPE_FUNC:
          {
            func_type *the_func = (func_type *)ty;
            return (hasproto(the_func->return_type()) &&
                    the_func->args_known());
          }
        default:
            return TRUE;
      }
  }

/* newfield - install a new field in ty with type fty */
extern Field newfield(const char *name, struct field **field_list, 
		      type_node *fty, struct_type *to_fill)
  {
    Field p, *q = field_list;

    if (name == NULL)
        name = gen_internal_name();
    for (p = *q; p != NULL; q = &p->link, p = *q)
      {
        if (strcmp(p->name, name) == 0)
            error("duplicate field name `%s' in `%t'\n", name, to_fill);
      }
#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
    *q = p = new struct field;
#else
//
//  gcc version 2.6.3 gets a parse error on the code above.  The code
//  looks like it should be perfectly legal, and both gcc 2.5.8 and
//  the IRIX 5.3 C++ compiler have no problem with it, so I'm inclined
//  to think the problem is caused by brain damage in the g++
//  front-end.  A work-around follows.
//
    *q = p = (struct field *)operator new(sizeof(struct field));
#endif
    p->name = name;
    p->type = fty;
    p->offset = 0;
    p->from = 0;
    p->to = 0;
    p->block_name = NULL;
    p->block_type = NULL;
    p->link = NULL;
    if (xref)
      {
        assert(to_fill != NULL);
        if (get_field_table(to_fill) == NULL)
            set_field_table(to_fill, table(0, level));
        Table field_table = get_field_table(to_fill);
        install(name, &field_table, 1)->src = src;
        set_field_table(to_fill, field_table);
      }
    return p;
  }

/* newstruct - install a new structure/union/enum depending on op and return
   its symbol table entry */
Symbol newstruct(int op, const char *tag)
  {
    assert((op == ENUM) || (op == STRUCT) || (op == UNION));
    if ((tag == NULL) || (*tag == '\0'))  /* anonymous structure/union/enum */
        tag = gen_internal_name();
    Symbol p = lookup(tag, types);
    if ((p != NULL) &&
        ((p->scope == level) || ((p->scope == PARAM) && (level == PARAM+1))))
      {
        if ((((p->type->op() == TYPE_ENUM) && (op == ENUM)) ||
             ((p->type->op() == TYPE_STRUCT) && (op == STRUCT)) ||
             ((p->type->op() == TYPE_UNION) && (op == UNION))) && !p->defined)
          {
            return p;
          }
        error("redeclaration of `%s'\n", tag);
      }
    p = install(tag, &types, 1);
    if (op == ENUM)
      {
        p->type = new enum_type(tag, inttype->size(), TRUE, 0);
      }
    else
      {
        p->type = new struct_type((op == STRUCT) ? TYPE_STRUCT : TYPE_UNION,
                                  0, tag, 0);
      }
    set_source(p->type, src);

    base_symtab *the_symtab = get_current_symtab();
    if (the_symtab->is_file())
        the_symtab = the_symtab->parent();
    p->type = the_symtab->install_type(p->type);

    p->src = src;
    return p;
  }

/* outtype - output type ty */
extern void outtype(void (*s_printer)(const char *to_print, void *data),
                    void (*c_printer)(int to_print, void *data), void *data,
                    type_node *ty)
  {
    if (ty == chartype)
        (*s_printer)(chartype_sym->name, data);
    else if (ty == doubletype)
        (*s_printer)(doubletype_sym->name, data);
    else if (ty == floattype)
        (*s_printer)(floattype_sym->name, data);
    else if (ty == inttype)
        (*s_printer)(inttype_sym->name, data);
    else if (ty == longdouble)
        (*s_printer)(longdouble_sym->name, data);
    else if (ty == longtype)
        (*s_printer)(longtype_sym->name, data);
    else if (ty == longlong)
        (*s_printer)(longlong_sym->name, data);
    else if (ty == shorttype)
        (*s_printer)(shorttype_sym->name, data);
    else if (ty == signedchar)
        (*s_printer)(signedchar_sym->name, data);
    else if (ty == unsignedchar)
        (*s_printer)(unsignedchar_sym->name, data);
    else if (ty == unsignedlong)
        (*s_printer)(unsignedlong_sym->name, data);
    else if (ty == unsignedlonglong)
        (*s_printer)(unsignedlonglong_sym->name, data);
    else if (ty == unsignedshort)
        (*s_printer)(unsignedshort_sym->name, data);
    else if (ty == unsignedtype)
        (*s_printer)(unsignedtype_sym->name, data);
    else if (ty == voidtype)
        (*s_printer)(voidtype_sym->name, data);
    else
      {
        switch (ty->op())
          {
            case TYPE_VOID:
                (*s_printer)(voidtype_sym->name, data);
                break;
            case TYPE_CONST:
            case TYPE_VOLATILE:
                if (ty->is_const())
                    gprint(s_printer, c_printer, data, "%k ", CONST);
                if (ty->is_volatile())
                    gprint(s_printer, c_printer, data, "%k ", VOLATILE);
                gprint(s_printer, c_printer, data, "%t", ty->unqual());
                break;
            case TYPE_STRUCT:
            case TYPE_UNION:
            case TYPE_ENUM:
              {
                const char *ty_name;
                int op;
                if (ty->op() == TYPE_ENUM)
                  {
                    enum_type *the_enum = (enum_type *)ty;
                    ty_name = the_enum->name();
                    op = ENUM;
                  }
                else
                  {
                    struct_type *the_struct = (struct_type *)ty;
                    ty_name = the_struct->name();
                    op = (ty->op() == TYPE_STRUCT) ? STRUCT : UNION;
                  }
                if (ty->size() == 0)
                    (*s_printer)("incomplete ", data);
                assert(ty_name != NULL);
                if ((*ty_name >= '1') && (*ty_name <= '9'))
                  {
                    Symbol p = findtype(ty);
                    if (p == NULL)
                      {
                        gprint(s_printer, c_printer, data, "%k", op);
                        Coordinate *location = get_source(ty);
                        if (location != NULL)
                          {
                            gprint(s_printer, c_printer, data,
                                   " defined at %w", location);
                          }
                      }
                    else
                      {
                        (*s_printer)(p->name, data);
                      }
                  }
                else
                  {
                    gprint(s_printer, c_printer, data, "%k %s", op, ty_name);
                    if (ty->size() == 0)
                      {
                        Coordinate *location = get_source(ty);
                        if (location != NULL)
                          {
                            gprint(s_printer, c_printer, data,
                                   " defined at %w", location);
                          }
                      }
                  }
                break;
              }
            case TYPE_PTR:
                gprint(s_printer, c_printer, data, "pointer to %t",
                       base_from_pointer(ty));
                break;
            case TYPE_FUNC:
              {
                func_type *the_func = (func_type *)ty;
                gprint(s_printer, c_printer, data, "%t function",
                       the_func->return_type());
                if (the_func->args_known())
                  {
                    if (the_func->num_args() == 0)
                      {
                        if (the_func->has_varargs())
                            gprint(s_printer, c_printer, data, "(...)");
                        else
                            gprint(s_printer, c_printer, data, "(void)");
                      }
                    else
                      {
                        gprint(s_printer, c_printer, data, "(%t",
                               the_func->arg_type(0));
                        unsigned num_args = the_func->num_args();
                        for (unsigned arg_num = 1; arg_num < num_args;
                             ++arg_num)
                          {
                            gprint(s_printer, c_printer, data, ", %t",
                                   the_func->arg_type(arg_num));
                          }
                        if (the_func->has_varargs())
                            gprint(s_printer, c_printer, data, ", ...");
                        gprint(s_printer, c_printer, data, ")");
                      }
                  }
                break;
              }
            case TYPE_ARRAY:
              {
                array_type *the_array = (array_type *)ty;
                if (!the_array->are_bounds_unknown())
                  {
                    gprint(s_printer, c_printer, data, "array %d",
                           the_array->upper_bound().constant() + 1);
                    while ((the_array->elem_type() != NULL) &&
                           isarray(the_array->elem_type()) &&
                           (!((array_type *)(the_array->elem_type()))
                                ->are_bounds_unknown()))
                      {
                        the_array = (array_type *)(the_array->elem_type());
                        gprint(s_printer, c_printer, data, ", %d",
                               the_array->upper_bound().constant() + 1);
                      }
                  }
                else
                  {
                    (*s_printer)("incomplete array", data);
                  }
                if (the_array->elem_type() != NULL)
                  {
                    gprint(s_printer, c_printer, data, " of %t",
                           the_array->elem_type());
                  }
                break;
              }
            default:
                assert(FALSE);
          }
      }
  }

/* printdecl - output a C declaration for symbol p of type ty */
void printdecl(Symbol p, type_node *ty)
  {
    switch (p->sclass)
      {
        case AUTO:
            fprint(stderr, "%s;\n", typestring(ty, p->name));
            break;
        case STATIC:
        case EXTERN:
            fprint(stderr, "%k %s;\n", p->sclass, typestring(ty, p->name));
        case TYPEDEF:
        case ENUM:
            break;
        default:
            assert(FALSE);
      }
  }

/* printproto - output a prototype declaration for function p */
void printproto(Symbol p, Symbol callee[])
  {
    assert(p != NULL);
    type_node *p_type = p->type;
    assert(p_type->is_func());
    func_type *the_func = (func_type *)p_type;
    if (the_func->args_known())
      {
        printdecl(p, the_func);
      }
    else
      {
        unsigned num_args;
        for (num_args = 0; callee[num_args] != NULL; ++num_args)
            ;
        func_type *temp_func =
                new func_type(freturn(the_func), num_args, FALSE);
        for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
            temp_func->set_arg_type(arg_num, callee[arg_num]->type);
        printdecl(p, temp_func);
        delete temp_func;
      }
  }

/* qual - construct the type `op ty' where op is CONST or VOLATILE */
extern type_node *qual(int op, type_node *ty)
  {
    assert(ty != NULL);
    base_symtab *the_symtab = ty->parent();
    assert(the_symtab != NULL);

    type_ops suif_type_op;
    if (op == CONST)
        suif_type_op = TYPE_CONST;
    else if (op == VOLATILE)
        suif_type_op = TYPE_VOLATILE;
    else
        assert(FALSE);

    type_node *new_type;
    if (isarray(ty))
      {
        assert(ty == ty->unqual());
        assert(ty->is_array());
        array_type *old_array = (array_type *)ty;
        new_type = new array_type(qual(op, old_array->elem_type()),
                                  old_array->lower_bound(),
                                  old_array->upper_bound());
      }
    else if (isfunc(ty))
      {
        warning("qualified function type ignored\n");
        return ty;
      }
    else if ((isconst(ty) && (op == CONST)) ||
             (isvolatile(ty) && (op == VOLATILE)))
      {
        error("illegal type `%k %t'\n", op, ty);
        return ty;
      }
    else
      {
        new_type = new modifier_type(suif_type_op, ty);
      }

    return the_symtab->install_type(new_type);
  }

/* typestring - return ty as C declaration for str, which may be "" */
extern const char *typestring(type_node *ty, const char *str)
  {
    assert(ty != NULL);
    const char *name = NULL;
    if (ty == chartype)
        name = chartype_sym->name;
    else if (ty == doubletype)
        name = doubletype_sym->name;
    else if (ty == floattype)
        name = floattype_sym->name;
    else if (ty == inttype)
        name = inttype_sym->name;
    else if (ty == longdouble)
        name = longdouble_sym->name;
    else if (ty == longtype)
        name = longtype_sym->name;
    else if (ty == longlong)
        name = longlong_sym->name;
    else if (ty == shorttype)
        name = shorttype_sym->name;
    else if (ty == signedchar)
        name = signedchar_sym->name;
    else if (ty == unsignedchar)
        name = unsignedchar_sym->name;
    else if (ty == unsignedlong)
        name = unsignedlong_sym->name;
    else if (ty == unsignedlonglong)
        name = unsignedlonglong_sym->name;
    else if (ty == unsignedshort)
        name = unsignedshort_sym->name;
    else if (ty == unsignedtype)
        name = unsignedtype_sym->name;
    else if (ty == voidtype)
        name = voidtype_sym->name;

    if (name != NULL)
        return ((*str != 0) ? stringf("%s %s", name, str) : name);

    switch (ty->op())
      {
        case TYPE_CONST:
        case TYPE_VOLATILE:
          {
            type_node *unqualled = ty->unqual();
            char *quals = "";
            if (ty->is_volatile())
                quals = stringf("%k %s", VOLATILE, quals);
            if (ty->is_const())
                quals = stringf("%k %s", CONST, quals);
            if (isptr(unqualled))
                return typestring(unqualled, stringf("%s%s", quals, str));
            else
                return stringf("%s%s", quals, typestring(unqualled, str));
          }
        case TYPE_STRUCT:
        case TYPE_UNION:
        case TYPE_ENUM:
          {
            Symbol p = findtype(ty);
            if (p != NULL)
              {
                return ((*str != 0) ?
                        stringf("%s %s", p->name, str) : p->name);
              }

            const char *name;
            int op;
            if (ty->op() == TYPE_ENUM)
              {
                enum_type *the_enum = (enum_type *)ty;
                name = the_enum->name();
                op = ENUM;
              }
            else
              {
                struct_type *the_struct = (struct_type *)ty;
                name = the_struct->name();
                op = (ty->op() == TYPE_STRUCT) ? STRUCT : UNION;
              }
            assert(name != NULL);
            if ((*name >= '1') && (*name <= '9'))
                warning("unnamed %k in prototype\n", op);

            if (*str != 0)
                return stringf("%k %s %s", op, name, str);
            else
                return stringf("%k %s", op, name);
          }
        case TYPE_PTR:
          {
            Symbol p;
            if ((base_from_pointer(ty)->unqual()->size() !=
                 target.size[C_char]) &&
                ((p = findtype(ty)) != NULL))
              {
                return ((*str != 0) ?
                        stringf("%s %s", p->name, str) : p->name);
              }
            type_node *base_type = base_from_pointer(ty);
            str = stringf((isarray(base_type) || isfunc(base_type)) ?
                          "(*%s)" : "*%s", str);
            return typestring(base_type, str);
          }
        case TYPE_FUNC:
          {
            Symbol p = findtype(ty);
            if (p != NULL)
              {
                return ((*str != 0) ?
                        stringf("%s %s", p->name, str) : p->name);
              }

            func_type *the_func = (func_type *)ty;
            if (!the_func->args_known())
              {
                str = stringf("%s()", str);
                return typestring(the_func->return_type(), str);
              }
            else
              {
                if (the_func->num_args() == 0)
                  {
                    if (the_func->has_varargs())
                        str = stringf("%s(...)", str);
                    else
                        str = stringf("%s(void)", str);
                  }
                else
                  {
                    str = stringf("%s(%s", str,
                                  typestring(the_func->arg_type(0), ""));
                    unsigned num_args = the_func->num_args();
                    for (unsigned arg_num = 1; arg_num < num_args; ++arg_num)
                      {
                        str = stringf("%s, %s", str,
                                      typestring(the_func->arg_type(arg_num),
                                                 ""));
                      }
                    if (the_func->has_varargs())
                        str = stringf("%s, ...", str);
                    str = stringf("%s)", str);
                  }
                return typestring(the_func->return_type(), str);
              }
          }
        case TYPE_ARRAY:
          {
            Symbol p = findtype(ty);
            if (p != NULL)
              {
                return ((*str != 0) ?
                        stringf("%s %s", p->name, str) : p->name);
              }

            array_type *the_array = (array_type *)ty;
            if (!the_array->are_bounds_unknown())
              {
                str = stringf("%s[%d]", str,
                              the_array->upper_bound().constant() + 1);
                return typestring(the_array->elem_type(), str);
              }
            else
              {
                str = stringf("%s[]", str);
                return typestring(the_array->elem_type(), str);
              }
          }
        default:
            assert(FALSE);
      }

    assert(FALSE);
    return NULL;
  }

extern type_node *base_from_enum(type_node *the_type)
  {
    assert(the_type != NULL);
    type_node *unqualled = the_type->unqual();
    assert(unqualled != NULL);
    assert(unqualled->is_enum());
    enum_type *the_enum = (enum_type *)unqualled;
    boolean is_signed = the_enum->is_signed();
    int the_size = the_enum->size();
    if (is_signed)
      {
        if (the_size == inttype->size())
            return inttype;
        if (the_size == signedchar->size())
            return signedchar;
        if (the_size == shorttype->size())
            return shorttype;
        if (the_size == longtype->size())
            return longtype;
        if (the_size == longlong->size())
            return longlong;
      }
    else
      {
        if (the_size == unsignedtype->size())
            return unsignedtype;
        if (the_size == unsignedchar->size())
            return unsignedchar;
        if (the_size == unsignedshort->size())
            return unsignedshort;
        if (the_size == unsignedlong->size())
            return unsignedlong;
        if (the_size == unsignedlonglong->size())
            return unsignedlonglong;
      }

    base_symtab *the_symtab = the_type->parent();
    assert(the_symtab != NULL);
    type_node *new_type = new base_type(TYPE_INT, the_size, is_signed);
    return the_symtab->install_type(new_type);
  }

extern type_node *base_from_pointer(type_node *the_type)
  {
    assert(the_type != NULL);
    type_node *unqualled = the_type->unqual();
    assert(unqualled != NULL);
    assert(unqualled->is_ptr());
    ptr_type *the_pointer = (ptr_type *)unqualled;
    return the_pointer->ref_type();
  }

extern type_node *base_from_array(type_node *the_type)
  {
    assert(the_type != NULL);
    type_node *unqualled = the_type->unqual();
    assert(unqualled != NULL);
    assert(unqualled->is_array());
    array_type *the_array = (array_type *)unqualled;
    return the_array->elem_type();
  }


/*
 *  See ANSI/ISO 9899-1990, sections 6.1.2.6, 6.5.2, 6.5.3, and 6.5.4
 */

extern boolean compatible_types(type_node *type1, type_node *type2)
  {
    if (!identically_qualified(type1, type2))
        return FALSE;

    type_node *t1 = type1->unqual();
    type_node *t2 = type2->unqual();

    if (t1 == t2)
        return TRUE;

    /*
     *  ANSI/ISO 9899-1990, section 6.5.4.1, ``Semantics'', paragraph 2
     */
    if (t2->op() == TYPE_ENUM)
      {
        type_node *temp_type = t2;
        t2 = t1;
        t1 = temp_type;
      }

    switch (t1->op())
      {
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
            return FALSE;
        case TYPE_PTR:
          {
            if (t2->op() != TYPE_PTR)
                return FALSE;

            /*
             *  ANSI/ISO 9899-1990, section 6.5.4.1, ``Semantics'',
             *  paragraph 2
             */
            return (compatible_types(base_from_pointer(t1),
                                     base_from_pointer(t2)));
          }
        case TYPE_ARRAY:
          {
            if (t2->op() != TYPE_ARRAY)
                return FALSE;

            /*
             *  ANSI/ISO 9899-1990, section 6.5.4.2, ``Semantics'',
             *  paragraph 2
             */
            array_type *array1 = (array_type *)t1;
            array_type *array2 = (array_type *)t2;
            if ((!array1->are_bounds_unknown()) &&
                (!array2->are_bounds_unknown()))
              {
                array_bound upper1 = array1->upper_bound();
                array_bound upper2 = array2->upper_bound();
                if (upper1 != upper2)
                    return FALSE;
              }
            return compatible_types(array1->elem_type(), array2->elem_type());
          }
        case TYPE_FUNC:
          {
            if (t2->op() != TYPE_FUNC)
                return FALSE;

            /*
             *  ANSI/ISO 9899-1990, sectifon 6.5.4.3, ``Semantics'',
             *  paragraph 6
             */
            func_type *function1 = (func_type *)t1;
            func_type *function2 = (func_type *)t2;

            if (!compatible_types(function1->return_type(),
                                  function2->return_type()))
              {
                return FALSE;
              }

            if (!(function2->args_known()))
              {
                func_type *temp_function = function2;
                function2 = function1;
                function1 = temp_function;
              }

            if (function1->args_known())
              {
                if (function1->has_varargs() != function2->has_varargs())
                    return FALSE;

                unsigned num_args = function1->num_args();
                if (function2->num_args() != num_args)
                    return FALSE;

                for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                  {
                    type_node *arg1 = function1->arg_type(arg_num);
                    type_node *arg2 = function2->arg_type(arg_num);
                    if (!compatible_types(arg1->unqual(), arg2->unqual()))
                        return FALSE;
                  }

                return TRUE;
              }
            else if (function2->args_known())
              {
                if (function2->has_varargs())
                    return FALSE;

                unsigned num_args = function2->num_args();

                for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                  {
                    type_node *arg = function2->arg_type(arg_num);
                    if (!compatible_types(arg->unqual(),
                                          promote(arg->unqual())))
                      {
                        return FALSE;
                      }
                  }

                return TRUE;
              }
            else
              {
                return TRUE;
              }
          }
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            if (t1->op() != t2->op())
                return FALSE;

            boolean is_union = (t1->op() == TYPE_UNION);

            struct_type *struct1 = (struct_type *)t1;
            struct_type *struct2 = (struct_type *)t2;

            unsigned num_fields = struct1->num_fields();
            if (struct2->num_fields() != num_fields)
                return FALSE;

            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                const char *name1 = struct1->field_name(field_num);
                type_node *field1 = struct1->field_type(field_num);

                unsigned field_2_num;
                if (is_union)
                  {
                    field_2_num = struct2->find_field_by_name(name1);
                    if (field_2_num == (unsigned)-1)
                        return FALSE;
                  }
                else
                  {
                    field_2_num = field_num;
                  }

                if (strcmp(struct2->field_name(field_2_num), name1) != 0)
                    return FALSE;

                if (field_is_bit_fields(struct1, field_num))
                  {
                    if (!field_is_bit_fields(struct2, field_num))
                        return FALSE;
                    if (!bit_field_annotations_same(
                                struct2->field_type(field_2_num), field1))
                      {
                        return FALSE;
                      }
                  }
                else
                  {
                    if (field_is_bit_fields(struct2, field_num))
                        return FALSE;
                    if (!compatible_types(struct2->field_type(field_2_num),
                                          field1))
                      {
                        return FALSE;
                      }
                  }
              }

            return TRUE;
          }
        case TYPE_ENUM:
          {
            /*
             *  ANSI/ISO 9899-1990, section 6.5.4.1, ``Semantics'',
             *  paragraph 2
             */
            if (t2->op() != TYPE_ENUM)
              {
                if (compatible_types(base_from_enum(t1), t2))
                  {
                    if (Aflag >= 1)
                      {
                        warning("compatibility between `%t' and `%t' is "
                                "compiler-dependent\n", t1, t2);
                      }
                    return TRUE;
                  }
                return FALSE;
              }

            enum_type *enum1 = (enum_type *)t1;
            enum_type *enum2 = (enum_type *)t2;

            unsigned num_values = enum1->num_values();
            if (enum2->num_values() != num_values)
                return FALSE;

            for (unsigned value_num = 0; value_num < num_values; ++value_num)
              {
                const char *value_name = enum1->member(value_num);
                int value_value = enum1->value(value_num);
                int value_2_num = enum2->find_member_by_value(value_value);
                if (value_2_num == -1)
                    return FALSE;
                if (strcmp(enum2->member(value_2_num), value_name) != 0)
                    return FALSE;
                if (enum2->value(value_2_num) != value_value)
                    return FALSE;
              }
            return TRUE;
          }
        default:
            assert(FALSE);
      }
    return FALSE;
  }

static boolean identically_qualified(type_node *type1, type_node *type2)
  {
    return ((type1->is_const() == type2->is_const()) &&
            (type1->is_volatile() == type2->is_volatile()));
  }

static boolean bit_field_annotations_same(type_node *field1,
                                          type_node *field2)
  {
    immed_list *list1 = (immed_list *)(field1->peek_annote(k_bit_field_info));
    immed_list *list2 = (immed_list *)(field2->peek_annote(k_bit_field_info));
    if ((list1 == NULL) && (list2 == NULL))
        return TRUE;
    if ((list1 == NULL) || (list2 == NULL))
        return FALSE;
    immed_list_iter iter1(list1);
    immed_list_iter iter2(list2);
    while ((!iter1.is_empty()) && (!iter2.is_empty()))
      {
        immed value1 = iter1.step();
        immed value2 = iter2.step();
        if (value1 != value2)
            return FALSE;
      }
    return (iter1.is_empty() == iter2.is_empty());
  }

extern array_type *make_array(type_node *element_type)
  {
    assert(element_type != NULL);
    base_symtab *the_symtab = element_type->parent();
    assert(the_symtab != NULL);
    array_type *new_type = new array_type(element_type, array_bound(0));
    return (array_type *)(the_symtab->install_type(new_type));
  }

extern boolean has_constant_in_fields(type_node *the_type)
  {
    if (the_type->is_const())
        return TRUE;

    type_node *unqualled = the_type->unqual();
    if (!unqualled->is_struct())
        return FALSE;

    struct_type *the_struct = (struct_type *)unqualled;
    int num_fields = the_struct->num_fields();
    for (int field_num = 0; field_num < num_fields; ++field_num)
      {
        if (has_constant_in_fields(the_struct->field_type(field_num)))
            return TRUE;
      }
    return FALSE;
  }

extern type_node *create_function_type(type_node *return_type,
                                       type_node **arguments)
  {
    assert(return_type != NULL);

    func_type *result = new func_type(return_type);
    if (arguments == NULL)
      {
        result->set_args_unknown();
      }
    else
      {
        result->set_args_known();
        unsigned num_args = 0;
        type_node **current_arg = arguments;
        while (*current_arg != NULL)
          {
            if (*current_arg == voidtype)
              {
                if (num_args > 0)
                    result->set_varargs(TRUE);
                break;
              }
            ++current_arg;
            ++num_args;
          }
        result->set_num_args(num_args);
        for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
            result->set_arg_type(arg_num, arguments[arg_num]);
        delete arguments;
      }
    return result;
  }

extern boolean field_is_bit_fields(struct_type *the_struct,
                                   unsigned field_num)
  {
    assert(the_struct != NULL);
    assert(the_struct->num_fields() > field_num);
    type_node *field_type = the_struct->field_type(field_num);
    assert(field_type != NULL);
    return (field_type->peek_annote(k_bit_field_info) != NULL);
  }

extern struct field *bit_field_list(struct_type *the_struct,
                                    unsigned field_num)
  {
    assert(the_struct != NULL);
    assert(the_struct->num_fields() > field_num);
    type_node *field_type = the_struct->field_type(field_num);
    assert(field_type != NULL);

    immed_list *descriptor =
            (immed_list *)(field_type->peek_annote(k_bit_field_info));
    if (descriptor == NULL)
        return NULL;

    struct field *result = NULL;
    struct field **next_position = &result;
    immed_list_iter the_iter(descriptor);
    while (!the_iter.is_empty())
      {
#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
        *next_position = new struct field;
#else
//
//  gcc version 2.6.3 gets a parse error on the code above.  The code
//  looks like it should be perfectly legal, and both gcc 2.5.8 and
//  the IRIX 5.3 C++ compiler have no problem with it, so I'm inclined
//  to think the problem is caused by brain damage in the g++
//  front-end.  A work-around follows.
//
        *next_position = (struct field *)operator new(sizeof(struct field));
#endif

        immed type_value = the_iter.step();
        assert(type_value.is_string());
        const char *type_string = type_value.string();
        boolean is_volatile = FALSE;
        boolean is_constant = FALSE;

        if (strcmp(type_string, "volatile") == 0)
          {
            is_volatile = TRUE;
            assert(!the_iter.is_empty());
            type_value = the_iter.step();
            assert(type_value.is_string());
            type_string = type_value.string();
          }

        if (strcmp(type_string, "const") == 0)
          {
            is_constant = TRUE;
            assert(!the_iter.is_empty());
            type_value = the_iter.step();
            assert(type_value.is_string());
            type_string = type_value.string();
          }

        if (strcmp(type_string, "int") == 0)
            (*next_position)->type = inttype;
        else if (strcmp(type_string, "unsigned") == 0)
            (*next_position)->type = unsignedtype;
        else
            assert(FALSE);

        assert(!the_iter.is_empty());
        immed name_value = the_iter.step();
        assert(name_value.is_string());
        (*next_position)->name = name_value.string();

        assert(!the_iter.is_empty());
        immed from_value = the_iter.step();
        assert(from_value.is_integer());
        (*next_position)->from = from_value.integer();

        assert(!the_iter.is_empty());
        immed to_value = the_iter.step();
        assert(to_value.is_integer());
        (*next_position)->to = to_value.integer();

        (*next_position)->offset = 0;
        (*next_position)->link = NULL;

        next_position = &((*next_position)->link);
      }
    return result;
  }

extern Table get_field_table(struct_type *the_struct)
  {
    assert(the_struct != NULL);
    return (Table)(the_struct->peek_annote(k_field_table));
  }

extern void set_field_table(struct_type *the_struct, Table new_table)
  {
    assert(the_struct != NULL);
    the_struct->append_annote(k_field_table, new_table);
  }

/*
 *  This function expects an integer or pointer type and returns an unsigned
 *  integer type of the same size.
 */
extern type_node *unsigned_int_version(type_node *original_type)
  {
    if (original_type->op() == TYPE_INT)
      {
        base_type *the_base = (base_type *)original_type;
        if (!the_base->is_signed())
            return original_type;
      }
    else if (original_type->op() != TYPE_PTR)
      {
        return original_type;
      }
    type_node *new_type = new base_type(TYPE_INT,
                                        original_type->size(), FALSE);
    return original_type->parent()->install_type(new_type);
  }

static void use_field(struct_type *the_struct, char *name)
  {
    if (xref)
      {
        Table field_table = get_field_table(the_struct);
        assert(field_table != NULL);
        Symbol q = lookup(name, field_table);
        assert(q != NULL);
        use(q, src);
      }
  }

static Coordinate *get_source(type_node *the_type)
  {
    assert(the_type != NULL);
    immed_list *immeds =
            (immed_list *)(the_type->peek_annote(k_type_source_coordinates));
    return immeds_to_coordinate(immeds);
  }

static void set_source(type_node *the_type, Coordinate source)
  {
    assert(the_type != NULL);
    immed_list *immeds = coordinate_to_immeds(source);
    the_type->append_annote(k_type_source_coordinates, immeds);
  }
