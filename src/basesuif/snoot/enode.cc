/* file "enode.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

/*	Shift operation modified by David Moore, Portland Group 11 Feb 1998

	In the case of a shift by a negative constant, the cast to an
	unsigned of the shift amount is ungood as it produces a
	giant number later which causes an out of memory error.

	Changed code to check for this case.
*/

#include <suif_copyright.h>

/* snoot type-checking and tree construction for expressions */

#include "c.h"

static genop addnode(int op, genop l, genop r);
static genop andnode(int op, genop l, genop r);
static type_node *binary(type_node *xty, type_node *yty);
static genop cmpnode(int op, genop l, genop r);
static boolean compatible(type_node *ty1, type_node *ty2);
static genop mulnode(int op, genop l, genop r);
static genop subnode(int op, genop l, genop r);
static genop rrr_genop(type_node *result_type, if_ops opcode, genop left,
                       genop right);

genop (*opnode[])(int, genop, genop) =
  {
#define xx(a,b,c,d,e,f,g) e,
#include "token.h"
  };

/* addnode - construct tree for l + r */
static genop addnode(int op, genop l, genop r)
  {
    assert(op == ADD);

    type_node *result_type = inttype;

    if (isarith(l.type()) && isarith(r.type()))
      {
        result_type = binary(l.type(), r.type());
        l = cast(l, result_type);
        r = cast(r, result_type);
      }
    else if (isptr(l.type()) && isint(r.type()))
      {
        return addnode(ADD, r, l);
      }
    else if (isptr(r.type()) && isint(l.type()))
      {
        result_type = r.type()->unqual();
        int n = base_from_pointer(result_type)->size() /
                target.addressable_size;
        if (n == 0)
          {
            error("unknown size of type `%t'\n",
                  base_from_pointer(result_type));
          }
        l = cast(l, promote(l.type()));
        if (n > 1)
            l = mulnode(MUL, constnode(n, type_ptr_diff), l);
      }
    else
      {
        typeerror(ADD, l, r);
      }

    return rrr_genop(result_type, io_add, l, r);
  }

/* andnode - construct tree for l [&& ||] r */
static genop andnode(int op, genop l, genop r)
  {
    if (!isscalar(l.type()) || !isscalar(r.type()))
        typeerror(op, l, r);

    if_ops branch_op;
    if (op == AND)
        branch_op = io_btrue;
    else if (op == OR)
        branch_op = io_bfalse;
    else
        assert(FALSE);

    l = cond(l);
    r = cond(r);

    if (needconst)
      {
        immed const_value;
        eval_status status = l.evaluate_as_const(&const_value);
        boolean eval_right = FALSE;
        switch (status)
          {
            case EVAL_OVERFLOW:
                warning("overflow in constant expression\n");
                /* fall through */
            case EVAL_OK:
                if (const_value.is_int_const())
                  {
                    if ((immed_to_ii(const_value) == 0) != (op == AND))
                        eval_right = TRUE;
                  }
                else
                  {
                    error("left operand of `%s' is not an integer\n",
                          (op == AND) ? "&&" : "||");
                  }
                break;
            case EVAL_NOT_CONST:
                error("expression must be constant\n");
                break;
            case EVAL_DIV_BY_ZERO:
                error("division by zero in constant expression\n");
                break;
            case EVAL_UNKNOWN_AT_LINK:
                error("constant expression cannot be computed at link time\n");
                break;
            default:
                assert(FALSE);
          }

        l.deallocate();

        if (eval_right)
          {
            return r;
          }
        else
          {
            r.deallocate();
            int value = (op == AND) ? 0 : 1;
            in_ldc *new_ldc = new in_ldc(booleantype, operand(), immed(value));
            return genop(operand(new_ldc), new tree_node_list);
          }
      }

    l.make_temporary();

    tree_node_list *left_precomp = l.precomputation();
    tree_node_list *right_precomp = r.precomputation();
    operand left_operand = l.suif_operand();
    operand right_operand = r.suif_operand();

    var_sym *temp_var = get_current_symtab()->new_unique_var(booleantype);
    temp_var->reset_userdef();

    right_precomp->append(create_assignment(temp_var, right_operand));

    instruction *left_assignment =
            new in_rrr(io_cpy, booleantype, operand(temp_var),
                       left_operand.clone());
    tree_node_list *left_assign_list = new tree_node_list;
    left_assign_list->append(new tree_instr(left_assignment));

    label_sym *jumpto = genlabel();
    instruction *the_branch = new in_bj(branch_op, jumpto, left_operand);
    left_precomp->append(new tree_instr(the_branch));

    tree_if *the_if =
            new tree_if(jumpto, left_precomp, left_assign_list, right_precomp);

    tree_node_list *new_precomp = new tree_node_list;
    new_precomp->append(the_if);

    return genop(operand(temp_var), new_precomp);
  }

/* asgnnode - construct tree for l = r */
extern genop asgnnode(int op, genop l, genop r)
  {
    assert(op == ASGN);

    r = pointer(r);
    type_node *result_type = assign(l.type(), r);
    if (result_type == NULL)
      {
        typeerror(ASGN, l, r);
        if (r.type() == voidtype)
            r = cast(r, inttype);
        result_type = r.type();
      }

    boolean is_bit_field;
    unsigned short bit_field_from;
    unsigned short bit_field_to;
    unsigned short bit_field_size;
    type_node *bit_field_result_type;
    if (l.is_bit_field_ref())
      {
        l.undo_bit_field_ref(&bit_field_from, &bit_field_to,
                             &bit_field_result_type);
        is_bit_field = TRUE;
        assert(bit_field_to >= bit_field_from);
        bit_field_size = bit_field_to - bit_field_from;
      }
    else
      {
        is_bit_field = FALSE;
        l = lvalue(l);
      }

    type_node *aty = l.type();
    if (isptr(aty))
        aty = base_from_pointer(aty);
    if (has_constant_in_fields(aty))
      {
        if (l.suif_operand().is_symbol() &&
            l.suif_operand().symbol()->is_userdef())
          {
            error("assignment to const identifier `%s'\n",
                  l.suif_operand().symbol()->name());
          }
        else
          {
            error("assignment to const location\n");
          }
      }
    r = cast(r, result_type);

    if (is_bit_field && (bit_field_size < result_type->size()))
      {
        if (non_negative(result_type))
          {
            r = bitnode(BAND, r,
                        constnode(~(~(unsigned)0 << bit_field_size),
                                  unsignedtype));
          }
        else
          {
            int n = result_type->size() - bit_field_size;
            /*
             * The value of assignment is the value of the left
             * operand (sec. 3.3.16), which for bit fields is the
             * same as extracting the field.  The shnode(LSH, ...)
             * will wipe out the upper n bits and the subsequent
             * shnode(RSH, ...) will be signed and thus extend the
             * sign bit correctly.
             */
            r = shnode(LSH, r, constnode(n, inttype));
            r = shnode(RSH, r, constnode(n, inttype));
          }
      }

    tree_node_list *left_precomp = l.precomputation();
    operand left_operand = l.suif_operand();

    var_sym *target_var = NULL;
    if (left_operand.is_expr() && !is_bit_field)
      {
        instruction *left_instr = left_operand.instr();
        assert(left_instr != NULL);
        if (left_instr->opcode() == io_ldc)
          {
            in_ldc *left_ldc = (in_ldc *)left_instr;
            immed value = left_ldc->value();
            type_node *ldc_type = left_ldc->result_type()->unqual();
            if (value.is_symbol() && ldc_type->is_ptr())
              {
                sym_node *left_symbol = value.symbol();
                int left_offset = value.offset();
                if (left_symbol->is_var() && (left_offset == 0))
                  {
                    ptr_type *ldc_ptr = (ptr_type *)ldc_type;
                    var_sym *left_var = (var_sym *)left_symbol;
                    if (left_var->type()->unqual() ==
                        ldc_ptr->ref_type()->unqual())
                      {
                        target_var = left_var;
                        delete left_instr;
                      }
                  }
              }
          }
      }

    if ((target_var == NULL) || target_var->type()->is_volatile())
        r.make_temporary();

    tree_node_list *right_precomp = r.precomputation();
    operand right_operand = r.suif_operand();

    left_precomp->append(right_precomp);
    delete(right_precomp);

    operand result_suif_op;
    instruction *assignment_instr;
    tree_node *new_node;
    if (target_var != NULL)
      {
        if (target_var->type()->is_volatile())
            result_suif_op = right_operand.clone();
        else
            result_suif_op = operand(target_var);

        new_node = create_assignment(target_var, right_operand);
      }
    else
      {
        result_suif_op = right_operand.clone();
        if (is_bit_field)
          {
	    // watch out for side effects
	    if (operand_may_have_side_effects(left_operand)) {
	      type_node *tmp_type = left_operand.type()->unqual();
	      var_sym *tmp_var = 
		get_current_symtab()->new_unique_var(tmp_type);
	      left_precomp->append(create_assignment(tmp_var,
						     left_operand));
	      left_operand = tmp_var;
	    }
						     
            in_rrr *the_load =
                    new in_rrr(io_lod, unsignedtype, operand(),
                               left_operand.clone());
#ifndef DEAL_WITH_GCC_BRAIN_DAMAGE
            genop load_genop(operand(the_load), new tree_node_list);
#else
//
//  gcc version 2.6.3 gets a parse error on the code above.  The code
//  looks like it should be perfectly legal, and both gcc 2.5.8 and
//  the IRIX 5.3 C++ compiler have no problem with it, so I'm inclined
//  to think the problem is caused by brain damage in the g++
//  front-end.  A work-around follows.
//
            operand op1(the_load);
            genop load_genop(op1, new tree_node_list);
#endif
            unsigned int mask = ~(~(unsigned)0 << bit_field_size);
	    if (bit_field_size == sizeof(unsigned int) * 8) {
		mask = ~(unsigned) 0;
	    }
            mask = mask << field_right;
            genop right_op_genop(right_operand, new tree_node_list);
            right_op_genop =
                    bitnode(BOR,
                            bitnode(BAND, load_genop,
                                    constnode(~mask, unsignedtype)),
                            bitnode(BAND,
                                    shnode(LSH,
                                           cast(right_op_genop, unsignedtype),
                                           constnode(field_right, inttype)),
                                    constnode(mask, unsignedtype)));
            right_operand = right_op_genop.suif_operand();
            tree_node_list *right_op_precomp = right_op_genop.precomputation();
            left_precomp->append(right_op_precomp);
            delete right_op_precomp;
          }
        assignment_instr =
                new in_rrr(io_str, type_void, operand(), left_operand,
                           right_operand);
        new_node = new tree_instr(assignment_instr);
      }

    left_precomp->append(new_node);
    return genop(result_suif_op, left_precomp);
  }

/* binary - usual arithmetic conversions, return target type */
static type_node *binary(type_node *xty, type_node *yty)
  {
    type_node *x_unqual = xty->unqual();
    type_node *y_unqual = yty->unqual();

    if ((x_unqual == longdouble) || (y_unqual == longdouble))
        return longdouble;
    if ((x_unqual == doubletype) || (y_unqual == doubletype))
        return doubletype;
    if ((x_unqual == floattype) || (y_unqual == floattype))
        return floattype;

    x_unqual = promote(x_unqual);
    y_unqual = promote(y_unqual);

    if ((x_unqual == unsignedlonglong) || (y_unqual == unsignedlonglong))
        return unsignedlonglong;

    if (((x_unqual == longlong) && (y_unqual == unsignedlong)) ||
        ((y_unqual == longlong) && (x_unqual == unsignedlong)))
      {
        if (longlong->size() > unsignedlong->size())
            return longlong;
        else
            return unsignedlonglong;
      }

    if ((x_unqual == longlong) || (y_unqual == longlong))
        return longlong;

    if ((x_unqual == unsignedlong) || (y_unqual == unsignedlong))
        return unsignedlong;

    if (((x_unqual == longtype) && (y_unqual == unsignedtype)) ||
        ((y_unqual == longtype) && (x_unqual == unsignedtype)))
      {
        if (longtype->size() > unsignedtype->size())
            return longtype;
        else
            return unsignedlong;
      }

    if ((x_unqual == longtype) || (y_unqual == longtype))
        return longtype;
    if ((x_unqual == unsignedtype) || (y_unqual == unsignedtype))
        return unsignedtype;

    return inttype;
  }

/* bitnode - construct tree for l [& | ^ %] r */
extern genop bitnode(int op, genop l, genop r)
  {
    if_ops new_opcode;
    if (op == BAND)
        new_opcode = io_and;
    else if (op == BXOR)
        new_opcode = io_xor;
    else if (op == BOR)
        new_opcode = io_ior;
    else
        assert(op == MOD);

    type_node *result_type = inttype;

    if (isint(l.type()) && isint(r.type()))
      {
        result_type = binary(l.type(), r.type());
        l = cast(l, result_type);
        r = cast(r, result_type);
        if (op != MOD)
          {
            type_node *op_type =
                    new base_type(TYPE_INT, result_type->size(), FALSE);
            op_type = fileset->globals()->install_type(op_type);
            l = cast(l, op_type);
            r = cast(r, op_type);
          }
      }
    else
      {
        typeerror(op, l, r);
      }

    if (op == MOD)
      {
        return rrr_genop(result_type, io_rem, l, r);
      }
    else
      {
        type_node *intermediate_type;
        if (result_type->size() <= unsignedtype->size())
            intermediate_type = unsignedtype;
        else if (result_type->size() <= unsignedlong->size())
            intermediate_type = unsignedlong;
        else
            intermediate_type = unsignedlonglong;
        genop result_genop = rrr_genop(intermediate_type, new_opcode, l, r);
        return cast(result_genop, result_type);
      }
  }

/* cmpnode - construct tree for l [< <= >= >] r */
static genop cmpnode(int op, genop l, genop r)
  {
    if_ops new_opcode;
    if ((op == LE) || (op == GE))
        new_opcode = io_sle;
    else if ((op == LT) || (op == GT))
        new_opcode = io_sl;
    else if (op == EQ)
        new_opcode = io_seq;
    else if (op == NE)
        new_opcode = io_sne;
    else
        assert(FALSE);

    if ((op == GE) || (op == GT))
      {
        genop temp_genop = l;
        l = r;
        r = temp_genop;
      }
    
    if (isarith(l.type()) && isarith(r.type()))
      {
        type_node *result_type = binary(l.type(), r.type());
        l = cast(l, result_type);
        r = cast(r, result_type);
      }
   else if (compatible(l.type(), r.type()))
      {
        /* empty */;
      }
    else
      {
        typeerror(op, l, r);
      }

    return rrr_genop(booleantype, new_opcode, l, r);
  }

/* compatible - are ty1 & ty2 sans qualifiers pointers to compatible object or
   incomplete types? */
static boolean compatible(type_node *ty1, type_node *ty2)
  {
    if (isptr(ty1) && isptr(ty2))
      {
        do
          {
            ty1 = base_from_pointer(ty1)->unqual();
            ty2 = base_from_pointer(ty2)->unqual();
          } while (isptr(ty1) && isptr(ty2));
        return eqtype(ty1, ty2, FALSE);
      }
    return FALSE;
  }

/* condnode - build a tree for e ? l : r */
extern genop condnode(genop e, genop l, genop r)
  {
    type_node *result_type = NULL;
    type_node *lty = l.type();
    type_node *rty = r.type();

    if (isarith(lty) && isarith(rty))
      {
        result_type = binary(lty, rty);
        l = cast(l, result_type);
        r = cast(r, result_type);
      }
    else if (eqtype(lty, rty, TRUE) ||
             (isptr(lty) && isint(rty) && (assign(lty, r) != NULL)))
      {
        result_type = lty->unqual();
      }
    else if (isptr(rty) && isint(lty) && (assign(rty, l) != NULL))
      {
        result_type = rty->unqual();
      }
    else if (isptr(lty) && isptr(rty))
      {
        lty = base_from_pointer(lty);
        rty = base_from_pointer(rty);
        if ((lty->unqual() == voidtype) || (rty->unqual() == voidtype))
            result_type = voidtype;
        else if (eqtype(lty->unqual(), rty->unqual(), TRUE))
            result_type = lty->unqual();
        if (result_type != NULL)
          {
            if (isconst(lty) || isconst(rty))
                result_type = qual(CONST, result_type);
            if (isvolatile(lty) || isvolatile(rty))
                result_type = qual(VOLATILE, result_type);
            result_type = result_type->ptr_to();
          }
      }

    if (result_type == NULL)
      {
        typeerror(COND, l, r);
        result_type = inttype;
      }

    if (isenum(result_type))
        result_type = base_from_enum(result_type);

    e = cond(e);

    l = cast(l, result_type);
    r = cast(r, result_type);

    if (needconst)
      {
        immed const_value;
        eval_status status = e.evaluate_as_const(&const_value);
        boolean choose_left = TRUE;
        switch (status)
          {
            case EVAL_OVERFLOW:
                warning("overflow in constant expression\n");
                /* fall through */
            case EVAL_OK:
                if (const_value.is_int_const())
                  {
                    if (immed_to_ii(const_value) == 0)
                        choose_left = FALSE;
                  }
                else
                  {
                    error("test expression of conditional operator is not an "
                          "integer\n");
                  }
                break;
            case EVAL_NOT_CONST:
                error("expression must be constant\n");
                break;
            case EVAL_DIV_BY_ZERO:
                error("division by zero in constant expression\n");
                break;
            case EVAL_UNKNOWN_AT_LINK:
                error("constant expression cannot be computed at link time\n");
                break;
            default:
                assert(FALSE);
          }

        e.deallocate();

        if (choose_left)
          {
            r.deallocate();
            return l;
          }
        else
          {
            l.deallocate();
            return r;
          }
      }

    tree_node_list *left_precomp = l.precomputation();
    tree_node_list *right_precomp = r.precomputation();
    tree_node_list *test_precomp = e.precomputation();
    operand left_operand = l.suif_operand();
    operand right_operand = r.suif_operand();
    operand test_operand = e.suif_operand();

    label_sym *jumpto = genlabel();
    in_bj *branch_instr = new in_bj(io_bfalse, jumpto, test_operand);
    test_precomp->append(new tree_instr(branch_instr));

    var_sym *temp_var = get_current_symtab()->new_unique_var(result_type);
    temp_var->reset_userdef();

    left_precomp->append(create_assignment(temp_var, left_operand));
    right_precomp->append(create_assignment(temp_var, right_operand));

    tree_if *the_if =
            new tree_if(jumpto, test_precomp, left_precomp, right_precomp);
    tree_node_list *result_precomputation = new tree_node_list;
    result_precomputation->append(the_if);
    return genop(operand(temp_var), result_precomputation);
  }

/* constnode - return a tree for a constant n of type ty (int or unsigned) */
extern genop constnode(i_integer n, type_node *ty)
  {
    type_node *result_type = ty;
    if (isarray(ty))
        result_type = atop(result_type);
    return genop(operand(new in_ldc(result_type, operand(), ii_to_immed(n))),
                 new tree_node_list);
  }

/* eqnode - construct tree for l [== !=] r */
extern genop eqnode(int op, genop l, genop r)
  {
    if_ops new_opcode;
    if (op == EQ)
        new_opcode = io_seq;
    else if (op == NE)
        new_opcode = io_sne;
    else
        assert(FALSE);

    if ((isint(l.type()) || l.is_null_pointer_constant()) && isptr(r.type()))
      {
        genop temp_genop = l;
        l = r;
        r = temp_genop;
      }

    if (isptr(l.type()) && (isint(r.type()) || r.is_null_pointer_constant()))
      {
        if (!r.is_null_pointer_constant())
          {
            error("operands of %s have incompatible types\n",
                  (op == EQ) ? "==" : "!=");
          }
        r = simplify_convert(l.type(), r);
        return rrr_genop(booleantype, new_opcode, l, r);
      }
    if (isptr(l.type()) && isptr(r.type()) &&
        (!isfunc(base_from_pointer(l.type()))) &&
        (!isfunc(base_from_pointer(r.type()))) &&
        ((base_from_pointer(r.type())->unqual()->op() == TYPE_VOID) ||
         (base_from_pointer(l.type())->unqual()->op() == TYPE_VOID)))
      {
        return rrr_genop(booleantype, new_opcode, l, r);
      }
    return cmpnode(op, l, r);
  }

/* mulnode - construct tree for l [* /] r */
static genop mulnode(int op, genop l, genop r)
  {
    if_ops new_opcode;
    if (op == MUL)
        new_opcode = io_mul;
    else if (op == DIV)
        new_opcode = io_div;
    else
        assert(FALSE);

    type_node *result_type = inttype;

    if (isarith(l.type()) && isarith(r.type()))
      {
        result_type = binary(l.type(), r.type());
        l = cast(l, result_type);
        r = cast(r, result_type);
      }
    else
      {
        typeerror(op, l, r);
      }

    return rrr_genop(result_type, new_opcode, l, r);
  }

/* shnode - construct tree for l [>> <<] r */
extern genop shnode(int op, genop l, genop r)
  {
    if_ops new_opcode;
    if (op == LSH)
      {
        new_opcode = io_lsl;
      }
    else if (op == RSH)
      {
        new_opcode = io_asr;
      }
    else
      {
        assert(FALSE);
      }

    type_node *result_type = inttype;
    boolean r_sign_flipped = FALSE;

    if (isint(l.type()) && isint(r.type()))
      {
        result_type = promote(l.type());
        l = cast(l, result_type);
	immed const_value;
        eval_status status = r.evaluate_as_const(&const_value);
	if (status == EVAL_OK)
	    {
	    i_integer i = immed_to_ii(const_value);
	    if (i < 0)
	        {
		i = -i;
		if (new_opcode == io_lsl)
		    new_opcode = io_asr;
		else
		    new_opcode = io_lsl;
		r = constnode(i,unsignedtype);
		r_sign_flipped = TRUE;
		}
	    }
	if (!r_sign_flipped)
            r = cast(r, unsignedtype);
      }
    else
      {
        typeerror(op, l, r);
      }

    if ((new_opcode == io_asr) && non_negative(result_type))
        new_opcode = io_lsr;

    return rrr_genop(result_type, new_opcode, l, r);
  }

/* subnode - construct tree for l - r */
static genop subnode(int op, genop l, genop r)
  {
    assert(op == SUB);

    type_node *result_type = inttype;

    if (isarith(l.type()) && isarith(r.type()))
      {
        result_type = binary(l.type(), r.type());
        l = cast(l, result_type);
        r = cast(r, result_type);
      }
    else if (isptr(l.type()) && isint(r.type()))
      {
        result_type = l.type()->unqual();
        int n = base_from_pointer(result_type)->size() /
                target.addressable_size;
        if (n == 0)
          {
            error("unknown size of type `%t'\n",
                  base_from_pointer(result_type));
          }
        r = cast(r, promote(r.type()));
        if (n > 1)
            r = mulnode(MUL, constnode(n, type_ptr_diff), r);
      }
    else if (isptr(l.type()) && isptr(r.type()) &&
             compatible(l.type(), r.type()))
      {
        type_node *base_type = base_from_pointer(l.type()->unqual());
        int n = base_type->size() / target.addressable_size;
        if (n == 0)
            error("unknown size of type `%t'\n", base_type);
        l = rrr_genop(type_ptr_diff, io_sub, l, r);
        return rrr_genop(type_ptr_diff, io_div, l,
                         constnode(n, type_ptr_diff));
      }
    else
      {
        typeerror(op, l, r);
      }
    return rrr_genop(result_type, io_sub, l, r);
  }

/* typeerror - issue "operands of op have illegal types `l' and `r'" */
void typeerror(int op, genop l, genop r)
  {
    int i;
    static struct
      {
        Opcode op;
        char *name;
      } ops[] =
      {
        { ASGN,  "="  }, { INDIR, "*"  }, { NEG,   "-"  },
        { ADD,   "+"  }, { SUB,   "-"  }, { LSH,   "<<" },
        { MOD,   "%"  }, { RSH,   ">>" }, { BAND,  "&"  },
        { BCOM,  "~"  }, { BOR,   "|"  }, { BXOR,  "^"  },
        { DIV,   "/"  }, { MUL,   "*"  }, { EQ,    "==" },
        { GE,    ">=" }, { GT,    ">"  }, { LE,    "<=" },
        { LT,    "<"  }, { NE,    "!=" }, { AND,   "&&" },
        { NOT,   "!"  }, { OR,    "||" }, { COND,  "?:" },
        { AREF,  "[]" },
        { (Opcode)0,     NULL }
      };

    op = generic(op);
    for (i = 0; ops[i].op != 0; i++)
      {
        if (op == ops[i].op)
            break;
      }
    assert(ops[i].name != NULL);
    if (!r.suif_operand().is_null())
      {
        error("operands of %s have illegal types `%t' and `%t'\n",
              ops[i].name, l.type(), r.type());
      }
    else
      {
        error("operand of unary %s has illegal type `%t'\n", ops[i].name,
              l.type());
      }
  }

static genop rrr_genop(type_node *result_type, if_ops opcode, genop left,
                       genop right)
  {
    tree_node_list *left_precomp = left.precomputation();
    tree_node_list *right_precomp = right.precomputation();
    operand left_operand = left.suif_operand();
    operand right_operand = right.suif_operand();

    left_precomp->append(right_precomp);
    delete right_precomp;

    in_rrr *new_rrr =
            new in_rrr(opcode, result_type, operand(), left_operand,
                       right_operand);
    return genop(operand(new_rrr), left_precomp);
  }
