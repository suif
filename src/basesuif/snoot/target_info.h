/* file "target_info.h" of the snoot program for SUIF */ 

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This header defines the structure used to store parameters about
 *  the target system.
 */

#ifndef MISC_H
enum C_types
  {
    C_char,
    C_short,
    C_int,
    C_long,
    C_longlong,
    C_float,
    C_double,
    C_longdouble,
    C_ptr,
    num_C_types
  };
#endif

typedef struct
  {
    boolean is_big_endian;
    int addressable_size;
    boolean char_is_signed;
    int size[num_C_types];
    int align[num_C_types];
    int array_align;
    int struct_align;
    enum C_types ptr_diff_type;
    char **builtins;
  } target_info_block;
