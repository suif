/* file "init.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot initialization parsing */

#include "c.h"

static int initarray(var_def *to_init, int len, type_node *ty, int lev);
static int initchar(var_def *the_def, int len, type_node *ty);
static void initend(int lev, char follow[]);
static int initfields(var_def *to_init, type_node *unit_type, Field p,
                      Field q);
static int initstruct(var_def *to_init, int len, type_node *ty, int lev);
static i_integer initvalue(type_node *ty);

/* defglobal - define a global or static variable */
extern var_def *defglobal(Symbol p)
  {
    return global(p);
  }

/* genconst - generate/check constant expression e; return size in bits */
/* If def is TRUE, really generate the constant; otherwise just check the
 * constant expression and print error messages if it's not in the right
 * form.
 */
extern int genconst(var_def *to_init, genop e, boolean def)
  {
    immed the_constant;
    eval_status status = e.evaluate_as_const(&the_constant);

    switch (status)
      {
        case EVAL_OVERFLOW:
            warning("overflow in constant initializer expression\n");
            /* fall through */
        case EVAL_OK:
            if (the_constant.is_symbol())
              {
                sym_node *the_symbol = the_constant.symbol();
                if (the_symbol->is_var())
                  {
                    var_sym *the_var = (var_sym *)the_symbol;
                    if (the_var->is_auto())
                      {
                        error("static initializer refers to auto "
                              "variable %s\n", the_var->name());
                      }
                  }
              }
            if (def)
                defconst(to_init, e.type(), the_constant);
            return e.type()->size();
        case EVAL_NOT_CONST:
            error("initializer must be constant\n");
            break;
        case EVAL_DIV_BY_ZERO:
            error("division by zero in initializer\n");
            break;
        case EVAL_UNKNOWN_AT_LINK:
            error("initializer value cannot be computed at link time\n");
            break;
        default:
            assert(FALSE);
      }

    if (def)
      {
        genop temp_genop = constnode(0, inttype);
        (void)genconst(to_init, temp_genop, TRUE);
        temp_genop.deallocate();
      }
    return inttype->size();
  }

/* initarray - initialize array of ty of <= len bits; if len == 0, go to } */
static int initarray(var_def *to_init, int len, type_node *ty, int lev)
  {
    int n = 0;

    do
      {
        initializer(to_init, ty, lev);
        n += ty->size();
        if (((len > 0) && (n >= len)) || (t != ','))
            break;
        t = gettok();
      } while (t != '}');
    return n;
  }

/* initchar - initialize array of <= len ty characters; if len == 0, go to } */
static int initchar(var_def *the_def, int len, type_node *ty)
  {
    int n = 0;
    char buf[16], *s = buf;

    do
      {
        i_integer init_char = initvalue(ty);
        char char_value = 0;
        if (init_char.is_c_char())
            char_value = init_char.c_char();
        else if (init_char.is_c_unsigned_char())
            char_value = (char)init_char.c_unsigned_char();
        else if (init_char.is_c_signed_char())
            char_value = (char)init_char.c_signed_char();
        else
            warning("char array element initializer doesn't fit in a char\n");
        *s++ = char_value;
        ++n;
        if (n % (target.size[C_int] / target.size[C_char]) == 0)
          {
            defstring(the_def, target.size[C_int] / target.size[C_char], buf,
                      ty);
            s = buf;
          }
        if (((len > 0) && (n >= len)) || (t != ','))
            break;
        t = gettok();
      } while (t != '}');
    if (s > buf)
        defstring(the_def, s - buf, buf, ty);
    return n * target.size[C_char];
  }

/* initend - finish off an initialization at level lev; accepts trailing
   comma */
static void initend(int lev, char follow[])
  {
    if ((lev == 0) && (t == ','))
        t = gettok();
    test('}', follow);
  }

/* initfields - initialize <= an unsigned's worth of bit fields in fields p to
   q */
static int initfields(var_def *to_init, type_node *unit_type, Field p,
                      Field q)
  {
    i_integer bits = 0;

    assert(((size_t)(unit_type->size())) <= sizeof(unsigned) * 8);

    if (p == q)
        return 0;

    do
      {
	boolean is_signed = !isunsigned(p->type);
        bit_field_data fdat;
        fdat.from = p->from;
        fdat.to = p->to;
        fdat.ref_type_size = p->type->size();
        fdat.ref_type_signed = is_signed;
        i_integer i = initvalue(is_signed ? inttype : unsignedtype);
        if (fieldsize(fdat) < fdat.ref_type_size)
          {
            if (((p->type == inttype) && (i >= 0) &&
                 ((i & ~(fieldmask(fdat) >> 1)) !=  0)) ||
                ((p->type == inttype) && (i < 0) &&
                 ((i | (fieldmask(fdat) >> 1)) != ~0)) ||
                ((p->type == unsignedtype) && ((i & ~fieldmask(fdat)) != 0)))
              {
                warning("initializer exceeds bit-field width\n");
              }
            i &= fieldmask(fdat);
          }
        bits |= i << fieldright(fdat);
        if (p->link == q)
            break;
        p = p->link;
      } while ((t == ',') && ((t = gettok()) != 0));

    defconst(to_init, unit_type, ii_to_immed(bits));
    return unit_type->size();
  }

/* initglobal - a new global identifier p, possibly initialized */
void initglobal(Symbol p, boolean flag)
  {

    if ((t == '=') || flag)
      {
        var_def *the_def = defglobal(p);
        if (t == '=')
            t = gettok();
        type_node *ty = initializer(the_def, p->type, 0);
        if (isarray(p->type) && (p->type->size() == 0))
          {
            p->type = ty;
            assert(p->suif_symbol != NULL);
            assert(p->suif_symbol->is_var());
            ((var_sym *)(p->suif_symbol))->set_type(ty);
          }
        p->defined = 1;
        if (p->sclass == EXTERN)
            p->sclass = AUTO;
      }
  }

/* initializer - constexpr | { constexpr ( , constexpr )* [ , ] } */
extern type_node *initializer(var_def *to_init, type_node *ty, int lev)
  {
    int n = 0;
    type_node *aty;
    static char follow[] = { IF, CHAR, STATIC, 0 };

    ty = ty->unqual();
    if (isscalar(ty))
      {
        genop e;
        boolean old_needconst = needconst;

        needconst = TRUE;
        if (t == '{')
          {
            t = gettok();
            e = expr1(0);
            initend(lev, follow);
          }
        else
          {
            e = expr1(0);
          }
        e = pointer(e);
        aty = assign(ty, e);
        if (aty != NULL)
          {
            e = cast(e, aty);
          }
        else
          {
            error("invalid initialization type; found `%t' expected `%t'\n",
                  e.type(), ty);
          }
        n = genconst(to_init, e, TRUE);
        e.deallocate();
        needconst = old_needconst;
      }
    if (isstruct_or_union(ty) && (ty->size() == 0))
      {
        static char follow[] = { CHAR, STATIC, 0 };
        error("cannot initialize undefined `%t'\n", ty);
        skipto(';', follow);
        return ty;
      }
    else if (isunion(ty))
      {
        if (t == '{')
          {
            t = gettok();
            n = initstruct(to_init, ty->size(), ty, lev + 1);
            initend(lev, follow);
          }
        else
          {
            if (lev == 0)
                error("missing { in initialization of `%t'\n", ty);
            n = initstruct(to_init, ty->size(), ty, lev + 1);
          }
      }
    else if (ty->op() == TYPE_STRUCT)
      {
        if (t == '{')
          {
            t = gettok();
            n = initstruct(to_init, 0, ty, lev + 1);
            test('}', follow);
          }
        else if (lev > 0)
          {
            n = initstruct(to_init, ty->size(), ty, lev + 1);
          }
        else
          {
            error("missing { in initialization of `%t'\n", ty);
            n = initstruct(to_init, ty->size(), ty, lev + 1);
          }
      }
    if (isarray(ty))
        aty = base_from_array(ty)->unqual();
    if (isarray(ty) && ischar(aty))
      {
        if (t == SCON)
          {
            if ((ty->size() > 0) && (ty->size() == tsym->type->size() - 1))
              {
                tsym->type =
                        build_array(chartype,
                                    ty->size() / target.size[C_char]);
              }
            n = tsym->type->size();
            if ((ty->size() != 0) &&
                (n == (ty->size() + target.size[C_char])) &&
                (tsym->u.c.v.p[(n / target.size[C_char]) - 1] == 0))
              {
                warning("character array too small for terminating null "
                        "character\n");
                n -= target.size[C_char];
              }
            defstring(to_init, n / target.size[C_char], tsym->u.c.v.p, aty);
            t = gettok();
          }
        else if (t == '{')
          {
            t = gettok();
            if (t == SCON)
              {
                ty = initializer(to_init, ty, lev + 1);
                initend(lev, follow);
                return ty;
              }
            n = initchar(to_init, 0, aty);
            test('}', follow);
          }
        else if ((lev > 0) && (ty->size() > 0))
          {
            n = initchar(to_init, ty->size() / target.size[C_char], aty);
          }
        else    /* eg, char c[] = 0; */
          {
            error("missing { in initialization of `%t'\n", ty);
            n = initchar(to_init, 1, aty);
          }
      }
    else if (isarray(ty))
      {
        if (t == '{')
          {
            t = gettok();
            n = initarray(to_init, 0, aty, lev + 1);
            test('}', follow);
          }
        else if ((lev > 0) && (ty->size() > 0))
          {
            n = initarray(to_init, ty->size(), aty, lev + 1);
          }
        else
          {
            error("missing { in initialization of `%t'\n", ty);
            n = initarray(to_init, aty->size(), aty, lev + 1);
          }
      }
    if (ty->size() != 0)
      {
        if (n > ty->size())
            error("too many initializers\n");
        else if (n < ty->size())
            space(to_init, ty->size() - n);
      }
    else if (isarray(ty))
      {
        assert(base_from_array(ty)->size() != 0);
        ty = build_array(base_from_array(ty),
                         n / base_from_array(ty)->size());
      }
    return ty;
  }

/* initstruct - initialize a struct ty of <= len bits; if len == 0, go to } */
static int initstruct(var_def *to_init, int len, type_node *ty, int lev)
  {
    assert(ty->is_struct());
    struct_type *the_struct = (struct_type *)ty;

    unsigned num_fields = the_struct->num_fields();
    if (num_fields == 0)
        return 0;

    unsigned field_num = 0;
    int a, n = 0;

    do
      {
        if (the_struct->offset(field_num) > n)
          {
            space(to_init, the_struct->offset(field_num) - n);
            n += the_struct->offset(field_num) - n;
          }
        if (field_is_bit_fields(the_struct, field_num))
          {
            struct field *bit_fields = bit_field_list(the_struct, field_num);
            n += initfields(to_init, the_struct->field_type(field_num),
                            bit_fields, NULL);
            while (bit_fields != NULL)
              {
                struct field *old_field = bit_fields;
                bit_fields = bit_fields->link;
                delete old_field;
              }
          }
        else
          {
            initializer(to_init, the_struct->field_type(field_num), lev);
            n += the_struct->field_type(field_num)->size();
          }
        if (field_num + 1 < num_fields)
          {
            ++field_num;
            a = get_alignment(the_struct->field_type(field_num));
          }
        else
          {
            a = get_alignment(ty);
          }
        if ((a != 0) && (n % a != 0))
          {
            space(to_init, a - n % a);
            n = roundup(n, a);
          }
        if (((len > 0) && (n >= len)) || (t != ','))
            break;
        t = gettok();
      } while (t != '}');

    return n;
  }

/* initvalue - evaluate a constant expression for a value of integer type ty */
static i_integer initvalue(type_node *ty)
  {
    boolean old_needconst = needconst;

    needconst = TRUE;
    genop e = expr1(0);
    type_node *aty = assign(ty, e);
    if (aty != NULL)
      {
        e = cast(e, aty);
      }
    else
      {
        error("invalid initialization type; found `%t' expected `%s'\n",
              e.type(),  ty);
        e = constnode(0, ty);
      }
    needconst = old_needconst;

    immed the_constant;
    eval_status status = e.evaluate_as_const(&the_constant);
    e.deallocate();

    switch (status)
      {
        case EVAL_OVERFLOW:
            warning("overflow in constant initializer expression\n");
            /* fall through */
        case EVAL_OK:
            if (!the_constant.is_int_const())
              {
                error("initializer must be integer constant\n");
                return 0;
              }
            return immed_to_ii(the_constant);
        case EVAL_NOT_CONST:
            error("initializer must be constant\n");
            break;
        case EVAL_DIV_BY_ZERO:
            error("division by zero in initializer\n");
            break;
        case EVAL_UNKNOWN_AT_LINK:
            error("initializer value cannot be computed at link time\n");
            break;
        default:
            assert(FALSE);
      }
    return 0;
  }
