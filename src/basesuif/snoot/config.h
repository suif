/* file "config.cc" of the snoot program for SUIF */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  Add any additional targets you may need to this file, following
 *  the examples given.
 */


                        TARGET  (mips-dec-ultrix, mips_dec_ultrix)

   IS_BIG_ENDIAN  (FALSE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)

       TYPE_SIZE  (longlong, 32)
      TYPE_ALIGN  (longlong, 32)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 64)
      TYPE_ALIGN  (longdouble, 64)

       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_int)


                        TARGET  (mips-sgi-irix5, mips_sgi_irix5)

   IS_BIG_ENDIAN  (TRUE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (FALSE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)

       TYPE_SIZE  (longlong, 32)
      TYPE_ALIGN  (longlong, 32)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 64)
      TYPE_ALIGN  (longdouble, 64)

       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_int)

         BUILTIN  ("__builtin_classof")

         BUILTIN  ("__builtin_alignof")

         BUILTIN  ("_VA_FP_SAVE_AREA")


                        TARGET  (mips-sgi-irix5.3, mips_sgi_irix5_3)

   IS_BIG_ENDIAN  (TRUE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (FALSE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)

       TYPE_SIZE  (longlong, 32)
      TYPE_ALIGN  (longlong, 32)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 64)
      TYPE_ALIGN  (longdouble, 64)

       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_int)

         BUILTIN  ("__builtin_classof")

         BUILTIN  ("__builtin_alignof")

         BUILTIN  ("_VA_FP_SAVE_AREA")


                        TARGET  (mips-sgi-irix6.2, mips_sgi_irix6_2)

   IS_BIG_ENDIAN  (TRUE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (FALSE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)

       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 128)
      TYPE_ALIGN  (longdouble, 128)

       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_int)

         BUILTIN  ("__builtin_classof")

         BUILTIN  ("__builtin_alignof")

         BUILTIN  ("_VA_FP_SAVE_AREA")

         BUILTIN  ("__NO_CFOLD_WARNING")


                        TARGET  (mips-sgi-irix6.4, mips_sgi_irix6_4)

   IS_BIG_ENDIAN  (TRUE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (FALSE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)

       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 128)
      TYPE_ALIGN  (longdouble, 128)

       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_int)

         BUILTIN  ("__builtin_classof")

         BUILTIN  ("__builtin_alignof")

         BUILTIN  ("_VA_FP_SAVE_AREA")

         BUILTIN  ("__NO_CFOLD_WARNING")


                        TARGET  (sparc-sun-sunos4, sparc_sun_sunos4)

   IS_BIG_ENDIAN  (TRUE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)

       TYPE_SIZE  (longlong, 32)
      TYPE_ALIGN  (longlong, 32)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 64)
      TYPE_ALIGN  (longdouble, 64)

       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_int)

         BUILTIN  ("__builtin_va_alist")

         BUILTIN  ("__builtin_va_arg_incr")


                        TARGET  (sparc-sun-solaris2.3, sparc_sun_solaris2_3)

   IS_BIG_ENDIAN  (TRUE)
 
ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)
 
       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)
 
       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)
 
       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)
 
       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)
 
       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)
 
       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)
 
       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)
 
       TYPE_SIZE  (longdouble, 128)
      TYPE_ALIGN  (longdouble, 64)
 
       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)
 
     ARRAY_ALIGN  ( 8)
 
    STRUCT_ALIGN  ( 8)
 
   PTR_DIFF_TYPE  (C_int)
 
         BUILTIN  ("__builtin_va_alist")

         BUILTIN  ("__builtin_va_arg_incr")

                        TARGET  (i386-sun-solaris2.4, i386_sun_solaris2_4)

   IS_BIG_ENDIAN  (FALSE)
 
ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)
 
       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)
 
       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)
 
       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)
 
       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)
 
       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 32)
 
       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)
 
       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 32)
 
       TYPE_SIZE  (longdouble, 96)
      TYPE_ALIGN  (longdouble, 32)
 
       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)
 
     ARRAY_ALIGN  ( 8)
 
    STRUCT_ALIGN  ( 8)
 
   PTR_DIFF_TYPE  (C_int)

         BUILTIN  ("__builtin_va_alist")

         BUILTIN  ("__builtin_va_arg_incr")


                        TARGET  (ksr-ksr-ksr, ksr_ksr_ksr)

   IS_BIG_ENDIAN  (TRUE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 64)
      TYPE_ALIGN  (long, 64)

       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)

       TYPE_SIZE  (float, 64)
      TYPE_ALIGN  (float, 64)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 128)
      TYPE_ALIGN  (longdouble, 64)

       TYPE_SIZE  (ptr, 64)
      TYPE_ALIGN  (ptr, 64)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_long)

         BUILTIN  ("__builtin_va_start")
         BUILTIN  ("__builtin_isfloat")

                        TARGET  (i386-unknown-bsdos, i386_unknown_bsdos)

   IS_BIG_ENDIAN  (FALSE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)

       TYPE_SIZE  (longlong, 32)
      TYPE_ALIGN  (longlong, 32)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 32)

       TYPE_SIZE  (longdouble, 96)
      TYPE_ALIGN  (longdouble, 32)

       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_int)



                        TARGET  (mips-sgi-irix5-64bit, mips_sgi_irix5_64bit)

   IS_BIG_ENDIAN  (TRUE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (FALSE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 64)
      TYPE_ALIGN  (long, 64)

       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 128)
      TYPE_ALIGN  (longdouble, 128)

       TYPE_SIZE  (ptr, 64)
      TYPE_ALIGN  (ptr, 64)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_long)


                        TARGET  (alpha-dec-osf, alpha_dec_osf)

   IS_BIG_ENDIAN  (FALSE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 64)
      TYPE_ALIGN  (long, 64)

       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 64)
      TYPE_ALIGN  (longdouble, 64)

       TYPE_SIZE  (ptr, 64)
      TYPE_ALIGN  (ptr, 64)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_long)

         BUILTIN  ("__builtin_va_start")

         BUILTIN  ("__builtin_isfloat")


                        TARGET  (alpha-dec-osf3.2, alpha_dec_osf3_2)

   IS_BIG_ENDIAN  (FALSE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 64)
      TYPE_ALIGN  (long, 64)

       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 64)
      TYPE_ALIGN  (longdouble, 64)

       TYPE_SIZE  (ptr, 64)
      TYPE_ALIGN  (ptr, 64)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_long)

         BUILTIN  ("__builtin_va_start")

         BUILTIN  ("__builtin_isfloat")


                        TARGET  (sparc-sun-solaris2.x-suifarg, sparc_sun_solaris2_x_suifarg)

   IS_BIG_ENDIAN  (TRUE)
 
ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)
 
       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)
 
       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)
 
       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)
 
       TYPE_SIZE  (long, 32)
      TYPE_ALIGN  (long, 32)
 
       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)
 
       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)
 
       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)
 
       TYPE_SIZE  (longdouble, 128)
      TYPE_ALIGN  (longdouble, 64)
 
       TYPE_SIZE  (ptr, 32)
      TYPE_ALIGN  (ptr, 32)
 
     ARRAY_ALIGN  ( 8)
 
    STRUCT_ALIGN  ( 8)
 
   PTR_DIFF_TYPE  (C_int)
 
         BUILTIN  ("__builtin_va_start")

         BUILTIN  ("__builtin_va_arg arg1")

         BUILTIN  ("__builtin_va_end")


                        TARGET  (alpha-dec-osf3.2-suifarg, alpha_dec_osf3_2_suifarg)

   IS_BIG_ENDIAN  (FALSE)

ADDRESSABLE_SIZE  ( 8)
  CHAR_IS_SIGNED  (TRUE)

       TYPE_SIZE  (char,  8)
      TYPE_ALIGN  (char,  8)

       TYPE_SIZE  (short, 16)
      TYPE_ALIGN  (short, 16)

       TYPE_SIZE  (int, 32)
      TYPE_ALIGN  (int, 32)

       TYPE_SIZE  (long, 64)
      TYPE_ALIGN  (long, 64)

       TYPE_SIZE  (longlong, 64)
      TYPE_ALIGN  (longlong, 64)

       TYPE_SIZE  (float, 32)
      TYPE_ALIGN  (float, 32)

       TYPE_SIZE  (double, 64)
      TYPE_ALIGN  (double, 64)

       TYPE_SIZE  (longdouble, 64)
      TYPE_ALIGN  (longdouble, 64)

       TYPE_SIZE  (ptr, 64)
      TYPE_ALIGN  (ptr, 64)

     ARRAY_ALIGN  ( 8)

    STRUCT_ALIGN  ( 8)

   PTR_DIFF_TYPE  (C_long)

         BUILTIN  ("__builtin_va_start")

         BUILTIN  ("__builtin_va_arg arg1")

         BUILTIN  ("__builtin_va_end")
