/* file "main.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* snoot main program */

#include "c.h"

int Aflag;                      /* > 0 if -W specified */
boolean Pflag;                  /* TRUE if -P specified */
boolean xref;                   /* TRUE for cross-reference data */
proc_sym *YYnull_proc = NULL;
boolean YYnull_used;

static char *infile;            /* input file */
static char *outfile;           /* output file */

static void compile(char *str);
static void doargs(int argc, char *argv[]);
static void emitYYnull(void);
static func_type *ftype(type_node *rty, type_node *ty);
static void typestab(Symbol p, Generic cl);

int main(int argc, char *argv[])
  {
    Symbol symroot = 0;    /* root of the global symbols */
    FILE *infp = stdin;

    start_suif(argc, argv);
    init_annotes();
    initialize_targets();
    doargs(argc, argv);
    typeInit();
    init_builtins();
    init_bit_ref();
    level = GLOBAL;

    if (option_null_check)
      {
        YYnull_proc = new proc_sym(ftype(voidtype, inttype), src_c, "_YYnull");
        assert(get_current_symtab() != NULL);
        get_current_symtab()->add_sym(YYnull_proc);
      }

    if ((infile != NULL) && (*infile != '-'))
      {
        infp = fopen(infile, "r");
        if (infp == NULL)
            error_line(1, NULL, "can't read %s\n", infile);
      }
    if ((outfile == NULL) || (*outfile == 0))
        error_line(1, NULL, "no output file specified");
    outf = fileset->add_file(NULL, outfile);
    inputInit(infp);
    t = gettok();
    program();

    if (option_null_check)
        emitYYnull();

    finalize();

    Coordinate src;
    foreach(types, GLOBAL, typestab, (Generic)&symroot);
    foreach(identifiers, GLOBAL, typestab, (Generic)&symroot);
    src.file = firstfile;
    src.x = 0;
    src.y = lineno;

    annote *source_annote = new annote(k_source_file_name);
    if (firstfile != NULL)
        source_annote->immeds()->append(firstfile);
    else if (infile != NULL)
        source_annote->immeds()->append(infile);
    else
        source_annote->immeds()->append("<stdin>");
    outf->annotes()->append(source_annote);
    outf->clone_annotes(outf, global_replacements, TRUE);
    outf->symtab()->clone_helper(global_replacements, TRUE);
    fileset->globals()->clone_helper(global_replacements, TRUE);

    while (!global_replacements->oldtypes.is_empty())
      {
        type_node *old_type = global_replacements->oldtypes.pop();
        old_type->parent()->remove_type(old_type);
        delete old_type;
      }
    delete global_replacements;

    delete fileset; /* this is needed to properly write everything out */
    outflush();
    exit(errcnt > 0);
    return 0;
  }

/* compile - compile str */
static void compile(char *str)
  {
    inputstring(str);
    t = gettok();
    program();
  }

/* doargs - process program arguments */
static void doargs(int argc, char *argv[])
  {
    int i, x;

    for (i = 1; i < argc; i++)
      {
        if (strcmp(argv[i], "-W") == 0)
          {
            Aflag++;
          }
        else if (strcmp(argv[i], "-w") == 0)
          {
            wflag = TRUE;
          }
        else if (strcmp(argv[i], "-keep-comments") == 0)
          {
            option_keep_comments = TRUE;
          }
        else if (strcmp(argv[i], "-ignore-comments") == 0)
          {
            option_keep_comments = FALSE;
          }
        else if (strcmp(argv[i], "-x") == 0)
          {
            xref = TRUE;
          }
        else if (strcmp(argv[i], "-P") == 0)
          {
            Pflag = TRUE;
          }
        else if (strncmp(argv[i], "-s", 2) == 0)
          {
            density.read(&argv[i][2]);
          }
        else if (strncmp(argv[i], "-e", 2) == 0)
          {
            if ((x = strtol(&argv[i][2], NULL, 0)) > 0)
                errlimit = x;
          }
        else if (strcmp(argv[i], "-mark-execution-points") == 0)
          {
            option_mark_execution_points = TRUE;
          }
        else if (strcmp(argv[i], "-ignore-execution-points") == 0)
          {
            option_mark_execution_points = FALSE;
          }
        else if (strcmp(argv[i], "-null-check") == 0)
          {
            option_null_check = TRUE;
          }
        else if (strcmp(argv[i], "-no-null-check") == 0)
          {
            option_null_check = FALSE;
          }
        else if (strncmp(argv[i], "-T", 2) == 0)
          {
            char *target_name = &(argv[i][2]);
            for (int target_num = 0; TRUE; ++target_num)
              {
                if (target_names[target_num] == NULL)
                  {
                    error_line(0, NULL, "target `%s' is unknown;",
                               target_name);
                    error_line(0, NULL, "known targets:");
                    for (int which_target = 0;
                         target_names[which_target] != NULL; ++which_target)
                      {
                        error_line(0, NULL, "    %s",
                                   target_names[which_target]);
                      }
                    exit(1);
                  }
                if (strcmp(target_name, target_names[target_num]) == 0)
                  {
                    this_target = &(target_table[target_num]);
                    break;
                  }
              }
          }
        else if (strcmp(argv[i], "-keep-typedef-info") == 0)
          {
            option_keep_typedef_info = TRUE;
          }
        else if (strcmp(argv[i], "-builtin") == 0)
          {
            ++i;
            if (argv[i] == NULL)
                error_line(1, NULL, "missing argument for `-builtin' switch");
            register_when_builtins_initialized(argv[i]);
          }
        else if (strcmp(argv[i], "-force-enum-is-int") == 0)
          {
            option_force_enum_is_int = TRUE;
          }
        else if (*argv[i] != '-')
          {
            if (infile == NULL)
                infile = argv[i];
            else if (outfile == NULL)
                outfile = argv[i];
            else
                error_line(1, NULL, "too many file specifications");
          }
        else
          {
            error_line(1, NULL, "unrecognized switch: `%s'", argv[i]);
          }
      }
  }

/* emitYYnull - compile definition for _YYnull, if referenced */
static void emitYYnull(void)
  {
    if (YYnull_used)
      {
        Aflag = 0;
        compile(stringf("static char *_YYfile = \"%s\";\n", file));
        compile(
                "static void _YYnull(int line, ...)\n"
                "  {\n"
                "    char buf[200];\n"
                "    sprintf(buf, \"null pointer dereferenced @%s:%d\\n\",\n"
                "            _YYfile, line);\n"
                "    write(2, buf, strlen(buf));\n"
                "    abort();\n"
                "  }\n"
                "\n");
      }
  }

/* ftype - return a function type for `rty function (ty,...)' */
static func_type *ftype(type_node *rty, type_node *ty)
  {
    assert(rty != NULL);
    base_symtab *the_symtab = rty->parent();
    assert(the_symtab != NULL);
    func_type *new_type = new func_type(rty, 1, TRUE);
    new_type->set_arg_type(0, ty);
    return (func_type *)(the_symtab->install_type(new_type));
  }

/* typestab - emit stab entries for p */
static void typestab(Symbol p, Generic cl)
  {
    if ((*(Symbol *)cl == NULL) && (p->sclass != 0) && (p->sclass != TYPEDEF))
        *(Symbol *)cl = p;
  }
