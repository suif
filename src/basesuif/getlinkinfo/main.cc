/* file "main.cc" of the getlinkinfo program for SUIF */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for getlinkinfo.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:29 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void mark_outlink_set(file_set *the_file_set);
static void mark_non_outlinked(file_set *the_file_set,
                               FILE *configuration_file);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 4) || (argc % 2 != 0))
        usage();

    for (int arg_num = 2; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    mark_outlink_set(fileset);

    FILE *configuration_file = fopen(argv[1], "r");
    if (configuration_file == NULL)
      {
        error_line(1, NULL, "unable to open configuration file \"%s\"",
                   argv[1]);
      }
    mark_non_outlinked(fileset, configuration_file);
    fclose(configuration_file);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr,
            "usage: %s <configfile> <infile> <outfile>"
            " { <infile> <outfile> }*\n", _suif_prog_base_name);
    exit(1);
  }

static void mark_outlink_set(file_set *the_file_set)
  {
    immed_list *file_list = new immed_list;
    the_file_set->reset_iter();
    while (TRUE)
      {
        file_set_entry *this_file = the_file_set->next_file();
        if (this_file == NULL)
            break;
        file_list->append(immed(this_file->file_id()));
      }

    the_file_set->reset_iter();
    file_set_entry *last_file = NULL;
    while (TRUE)
      {
        file_set_entry *this_file = the_file_set->next_file();
        if (this_file == NULL)
            break;
        if (last_file != NULL)
          {
            annote *old_annote =
                    last_file->annotes()->get_annote(k_outlink_set);
            if (old_annote != NULL)
                delete old_annote;
            last_file->append_annote(k_outlink_set, file_list->clone());
          }
        last_file = this_file;
      }
    assert(last_file != NULL);
    annote *old_annote = last_file->annotes()->get_annote(k_outlink_set);
    if (old_annote != NULL)
        delete old_annote;
    last_file->append_annote(k_outlink_set, file_list);
  }

static void mark_non_outlinked(file_set *the_file_set,
                               FILE *configuration_file)
  {
    sym_node_list_iter sym_iter(the_file_set->globals()->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->peek_annote(k_outlink_not_out) == NULL)
            this_sym->append_annote(k_outlink_not_out);
      }

    char c = 0;
    int count = fscanf(configuration_file, " externrefs %c ", &c);
    if ((count != 1) || (c != '{'))
        error_line(1, NULL, "bad configuration file format");

    int cur_char = fgetc(configuration_file);

    int buffer_size = 20;
    char *buffer = new char[buffer_size];

    while (TRUE)
      {
        while (isspace(cur_char))
            cur_char = fgetc(configuration_file);

        if (cur_char == '}')
            break;

        int buffer_position = 0;
        while (!isspace(cur_char))
          {
            if (cur_char == EOF)
                error_line(1, NULL, "unexpected end of configuration file");
            buffer[buffer_position] = cur_char;
            ++buffer_position;
            if (buffer_position == buffer_size)
              {
                char *new_buffer = new char[buffer_size * 2];
                memcpy(new_buffer, buffer, buffer_size);
                delete[] buffer;
                buffer = new_buffer;
                buffer_size *= 2;
              }
            cur_char = fgetc(configuration_file);
          }
        buffer[buffer_position] = 0;
        sym_node *test_sym =
                the_file_set->globals()->lookup_sym(buffer, SYM_PROC, FALSE);
        if (test_sym == NULL)
          {
            test_sym =
                    the_file_set->globals()->lookup_sym(buffer, SYM_VAR,
                                                        FALSE);
            if (test_sym == NULL)
              {
                test_sym =
                        the_file_set->globals()->lookup_sym(buffer, SYM_LABEL,
                                                            FALSE);
              }
          }

        if (test_sym != NULL)
          {
            annote *this_annote =
                    test_sym->annotes()->get_annote(k_outlink_not_out);
            if (this_annote != NULL)
                delete this_annote;
          }
      }

    delete[] buffer;

    cur_char = fgetc(configuration_file);

    while (isspace(cur_char))
        cur_char = fgetc(configuration_file);

    if (cur_char != EOF)
        error_line(1, NULL, "garbage at end of configuration file");
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
