/* file "main.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 * This is the main program for the swighnflew program for the SUIF system.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.2 1999/08/25 03:29:26 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static boolean errors = FALSE;
static const char *k_struct_alignment;
static const char *k_stack_frame_info;
static const char *k_uses_varargs;
static boolean flag_branch_compares_only = FALSE;
static boolean flag_no_struct_return = FALSE;
static boolean flag_mark_struct_alignment = FALSE;
static boolean flag_allow_fors = FALSE;
static char *mark_varargs_name = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage_error(void);
static void parse_arguments(int argc, char *argv[], char **input_filespec,
                            char **output_filespec);
static void do_proc(tree_proc *the_tree_proc);
static void proc_struct_returns(tree_proc *the_tree_proc);
static void do_node(tree_node *the_node, void *);
static void do_instr(instruction *the_instr, void *);
static void fix_compares(instruction *the_instr);
static void fix_structure_returns(instruction *the_instr);
static void annotate_struct_alignments(instruction *the_instr);
static void replace_instruction(instruction *the_instr, var_sym *replacement);
static void insert_comparison_computation(tree_node *location,
                                          instruction *comparison,
                                          var_sym *destination);
static void put_side_effects(tree_node *location, instruction *the_instr);
static void fix_symtab_types(base_symtab *the_symtab);
static void change_func_type(func_type *the_func_type);
static boolean is_struct_or_array(type_node *the_type);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    char *input_filespec, *output_filespec;

    start_suif(argc, argv);

    parse_arguments(argc, argv, &input_filespec, &output_filespec);

    ANNOTE(k_struct_alignment, "struct alignment", TRUE);
    ANNOTE(k_stack_frame_info, "stack_frame_info", TRUE);
    ANNOTE(k_uses_varargs, "uses varargs", TRUE);

    fileset->add_file(input_filespec, output_filespec);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            do_proc(this_proc_sym->block());
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
        fix_symtab_types(fse->symtab());
      }

    fix_symtab_types(fileset->globals());
    delete fileset;

    if (errors)
        return 1;
    else
        return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage_error(void)
  {
    fprintf(stderr, "usage: %s <options> <source filespec> "
            "<destination filespec>\n", _suif_program_name);
    exit(1);
  }

static void parse_arguments(int argc, char *argv[], char **input_filespec,
                            char **output_filespec)
  {
    static cmd_line_option option_table[] =
      {
        {CLO_NOARG, "-branch-compares-only",  NULL,
             &flag_branch_compares_only},
        {CLO_NOARG, "-no-struct-return",      NULL,
             &flag_no_struct_return},
        {CLO_NOARG, "-mark-struct-alignment", NULL,
             &flag_mark_struct_alignment},
        {CLO_NOARG, "-allow-fors",            NULL,
             &flag_allow_fors},
        {CLO_STRING, "-mark-varargs",          NULL,
             &mark_varargs_name}
      };

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    if (argc != 3)
        usage_error();

    *input_filespec = argv[1];
    *output_filespec = argv[2];
  }

static void do_proc(tree_proc *the_tree_proc)
  {
    assert(the_tree_proc != NULL);
    if (flag_no_struct_return)
        proc_struct_returns(the_tree_proc);
    the_tree_proc->map(&do_node, NULL, FALSE);
    do_node(the_tree_proc, NULL);
  }

static void proc_struct_returns(tree_proc *the_tree_proc)
  {
    func_type *old_func_type = the_tree_proc->proc()->type();
    type_node *return_type = old_func_type->return_type();
    if (!is_struct_or_array(return_type))
        return;

    var_sym *result_address =
            the_tree_proc->proc_syms()->new_unique_var(return_type->ptr_to());
    result_address->reset_userdef();
    result_address->set_param();
    the_tree_proc->proc_syms()->params()->push(result_address);
  }

static void do_node(tree_node *the_node, void *)
  {
    assert(the_node != NULL);

    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            the_tree_instr->instr_map(&do_instr, NULL, FALSE);
            break;
          }
        case TREE_FOR:
            if (!flag_allow_fors)
              {
                error_line(0, the_node, "TREE_FOR found");
                errors = TRUE;
              }
            break;
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            fix_symtab_types(the_block->symtab());
            break;
          }
        default:
            break;
      }
  }

static void do_instr(instruction *the_instr, void *)
  {
    assert(the_instr != NULL);

    if ((mark_varargs_name != NULL) && (the_instr->opcode() == io_gen))
      {
        in_gen *the_gen = (in_gen *)the_instr;
        if (strcmp(the_gen->name(), mark_varargs_name) == 0)
	  {
            the_instr->owner()->proc()->block()->append_annote(k_uses_varargs);

	    tree_proc *the_instr_tp = the_instr->owner()->proc()->block();
	    if (!the_instr_tp->peek_annote(k_stack_frame_info)) {
		// add the k_stack_frame_info annote only once
		immed_list *il = new immed_list();
		il->append(immed(-1));	// unset is_leaf flag
		il->append(immed(1));	// set uses_varargs flag
		the_instr_tp->append_annote(k_stack_frame_info, il);
	    }
	  }
      }
    if (flag_branch_compares_only)
        fix_compares(the_instr);
    if (flag_no_struct_return)
        fix_structure_returns(the_instr);
    if (flag_mark_struct_alignment)
        annotate_struct_alignments(the_instr);
  }

static void fix_compares(instruction *the_instr)
  {
    if ((the_instr->opcode() == io_seq) || (the_instr->opcode() == io_sne) ||
        (the_instr->opcode() == io_sl) || (the_instr->opcode() == io_sle))
      {
        in_rrr *the_rrr = (in_rrr *)the_instr;
        tree_node *owner = the_rrr->owner();
        assert(owner != NULL);
        switch (the_rrr->dst_op().kind())
          {
            case OPER_NULL:
              {
                assert(owner->is_instr());
                tree_instr *owner_tree_instr = (tree_instr *)owner;
                owner_tree_instr->remove_instr(the_rrr);
                put_side_effects(owner, the_rrr);
                kill_node(owner);
                break;
              }
            case OPER_SYM:
              {
                var_sym *the_var = the_rrr->dst_op().symbol();
                the_rrr->set_dst(operand());
                assert(owner->is_instr());
                tree_instr *owner_tree_instr = (tree_instr *)owner;
                owner_tree_instr->remove_instr(the_rrr);
                insert_comparison_computation(owner, the_rrr, the_var);
                kill_node(owner);
                break;
              }
            case OPER_INSTR:
              {
                instruction *destination = the_rrr->dst_op().instr();
                assert(destination != NULL);

                if ((!destination->is_branch()) ||
                    (destination->opcode() == io_mbr))
                  {
                    var_sym *new_var =
                            owner->scope()->new_unique_var(
                                    the_rrr->result_type());
                    new_var->reset_userdef();
                    replace_instruction(the_rrr, new_var);
                    insert_comparison_computation(owner, the_rrr, new_var);
                  }
                break;
              }
            default:
                assert(FALSE);
          }
      }
  }

static void fix_structure_returns(instruction *the_instr)
  {
    switch (the_instr->opcode())
      {
        case io_cal:
          {
            in_cal *the_call = (in_cal *)the_instr;
            if (!is_struct_or_array(the_call->result_type()))
                return;
            var_sym *the_var;
            if (the_call->dst_op().is_symbol())
              {
                the_var = the_call->dst_op().symbol();
              }
            else
              {
                the_var = the_call->owner()->scope()->new_unique_var(
                                the_call->result_type()->unqual());
                the_var->reset_userdef();
                if (the_call->dst_op().is_instr())
                  {
                    boolean is_expr = FALSE;
                    if (the_call->parent()->instr() != the_call)
                        is_expr = TRUE;

                    instruction *parent_instr = the_call->dst_op().instr();
                    unsigned num_srcs = parent_instr->num_srcs();
                    unsigned src_num;
                    for (src_num = 0; src_num < num_srcs; ++src_num)
                      {
                        if (parent_instr->src_op(src_num) == operand(the_call))
                            break;
                      }
                    assert(src_num < num_srcs);
                    if (is_expr)
                        the_call->remove();
                    parent_instr->set_src_op(src_num, operand(the_var));
                    if (is_expr)
                      {
                        tree_instr *the_tree_instr = new tree_instr(the_call);
                        tree_node *the_node = parent_instr->owner();
                        the_node->parent()->insert_before(the_tree_instr,
                                                          the_node->list_e());
                      }
                  }
              }

            the_call->set_dst(operand());
            the_call->set_result_type(type_void);
            unsigned num_args = the_call->num_args();
            the_call->set_num_args(num_args + 1);
            for (unsigned arg_num = num_args; arg_num > 0; --arg_num)
              {
                operand this_arg = the_call->argument(arg_num - 1);
                this_arg.remove();
                the_call->set_argument(arg_num, this_arg);
              }

            in_ldc *new_ldc =
                    new in_ldc(the_var->type()->ptr_to(), operand(),
                               immed(the_var));
            the_var->set_addr_taken();
            the_call->set_argument(0, operand(new_ldc));
            break;
          }
        case io_ret:
          {
            in_rrr *the_return = (in_rrr *)the_instr;
            if (the_return->src_op().is_null() ||
                (!is_struct_or_array(the_return->src_op().type())))
              {
                return;
              }

            tree_proc *the_proc = the_return->owner()->proc()->block();
            sym_node *param1 = (*(the_proc->proc_syms()->params()))[0];
            assert(param1->is_var());
            var_sym *return_val_address = (var_sym *)param1;

            operand return_value = the_return->src_op();
            return_value.remove();
            the_return->set_src(operand());

            in_rrr *new_store =
                    new in_rrr(io_str, type_void, operand(),
                               operand(return_val_address), return_value);

            tree_instr *new_tree_instr = new tree_instr(new_store);
            tree_node *the_node = the_return->owner();
            the_node->parent()->insert_before(new_tree_instr,
                                              the_node->list_e());

            if (flag_mark_struct_alignment)
                annotate_struct_alignments(new_store);
            break;
          }
        default:
            break;
      }
  }

static void annotate_struct_alignments(instruction *the_instr)
  {
    in_rrr *the_rrr;
    switch (the_instr->opcode())
      {
        case io_cal:
          {
            in_cal *the_call = (in_cal *)the_instr;

            immed_list *new_immeds = NULL;
            unsigned num_args = the_call->num_args();
            for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
              {
                type_node *arg_type = the_call->argument(arg_num).type();
                if (is_struct_or_array(arg_type))
                  {
                    if (new_immeds == NULL)
                        new_immeds = new immed_list;
                    new_immeds->append(immed(arg_num));
                    new_immeds->append(immed(get_alignment(arg_type)));
                  }
              }

            if (new_immeds != NULL)
                the_call->append_annote(k_struct_alignment, new_immeds);
            return;
          }
        case io_lod:
          {
            in_rrr *the_load = (in_rrr *)the_instr;

            if (!is_struct_or_array(the_load->result_type()))
                return;

            if (the_load->dst_op().is_instr() && operand(the_load).is_expr() &&
                ((the_load->dst_op().instr()->opcode() == io_str) ||
                 (the_load->dst_op().instr()->opcode() == io_gen)))
              {
                return;
              }

            force_dest_not_expr(the_load);
            var_sym *dest_sym;
            switch (the_load->dst_op().kind())
              {
                case OPER_NULL:
                    dest_sym =
                            the_load->owner()->scope()->new_unique_var(
                                    the_load->result_type()->unqual());
                    dest_sym->reset_userdef();
                    break;
                case OPER_SYM:
                    dest_sym = the_load->dst_op().symbol();
                    break;
                case OPER_INSTR:
                  {
                    dest_sym =
                            the_load->owner()->scope()->new_unique_var(
                                    the_load->result_type()->unqual());
                    dest_sym->reset_userdef();
                    instruction *parent_instr = the_load->dst_op().instr();

                    unsigned num_srcs = parent_instr->num_srcs();
                    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
                      {
                        if (parent_instr->src_op(src_num) == operand(the_load))
                          {
                            parent_instr->set_src_op(src_num,
                                                     operand(dest_sym));
                            break;
                          }
                      }
                    break;
                  }
                default:
                    assert(FALSE);
              }

            type_node *dest_ptr_type = dest_sym->type()->ptr_to();
            in_ldc *new_address =
                    new in_ldc(dest_ptr_type, operand(), immed(dest_sym));
            dest_sym->set_addr_taken();
            operand destination_address = operand(new_address);

            operand source_address = the_load->src_addr_op();
            source_address.remove();

            the_rrr = new in_rrr(io_memcpy, type_void, operand(),
                                 destination_address, source_address);

            the_load->set_dst(operand());
            replace_instruction(the_load, the_rrr);
            delete the_load;

            break;
          }
        case io_memcpy:
        case io_str:
          {
            the_rrr = (in_rrr *)the_instr;
            type_node *dst_type = the_rrr->dst_addr_op().type()->unqual();
            if (!dst_type->is_ptr())
              {
                error_line(1, the_rrr->owner(),
                           "%s destination address type is not a "
                           "pointer", if_ops_name(the_instr->opcode()));
              }
            ptr_type *dst_ptr = (ptr_type *)dst_type;
            if (!is_struct_or_array(dst_ptr->ref_type()))
                return;

            if (the_instr->opcode() == io_str)
              {
                if (!the_rrr->src2_op().is_expr() ||
                    (the_rrr->src2_op().instr()->opcode() != io_lod))
                  {
                    if (the_rrr->src2_op().is_expr())
                        force_dest_not_expr(the_rrr->src2_op().instr());
                    assert(the_rrr->src2_op().is_symbol());
                    var_sym *source_sym = the_rrr->src2_op().symbol();
                    type_node *source_ptr_type = source_sym->type()->ptr_to();
                    in_ldc *new_address =
                            new in_ldc(source_ptr_type, operand(),
                                       immed(source_sym));
                    source_sym->set_addr_taken();
                    in_rrr *new_load =
                            new in_rrr(io_lod, source_sym->type()->unqual(),
                                       operand(), operand(new_address));
                    the_rrr->set_src2(new_load);
                  }

                assert(the_rrr->src2_op().is_expr());
                assert(the_rrr->src2_op().instr()->opcode() == io_lod);
                in_rrr *the_load = (in_rrr *)(the_rrr->src2_op().instr());
                operand source_address = the_load->src_addr_op();
                operand destination_address = the_rrr->dst_addr_op();
                source_address.remove();
                destination_address.remove();

                in_rrr *new_memcopy =
                        new in_rrr(io_memcpy, type_void, operand(),
                                   destination_address, source_address);
                replace_instruction(the_rrr, new_memcopy);
                delete the_rrr;
                the_rrr = new_memcopy;
              }
            break;
          }
        case io_cpy:
          {
            in_rrr *the_copy = (in_rrr *)the_instr;
            if (!is_struct_or_array(the_copy->result_type()))
                return;

            force_sources_not_exprs(the_copy);
            force_dest_not_expr(the_copy);

            assert(the_copy->src_op().is_symbol());
            var_sym *source_sym = the_copy->src_op().symbol();
  
            if (the_copy->dst_op().is_null())
              {
                var_sym *temp_var =
                        the_instr->owner()->scope()->new_unique_var(
                                the_copy->result_type(), "__dummy");
                the_copy->set_dst(operand(temp_var));
              }

            if (the_copy->dst_op().is_null())
              {
                var_sym *temp_var =
                        the_instr->owner()->scope()->new_unique_var(
                                the_copy->result_type(), "__dummy");
                the_copy->set_dst(operand(temp_var));
              }

            assert(the_copy->dst_op().is_symbol());
            var_sym *destination_sym = the_copy->dst_op().symbol();

            type_node *source_ptr_type = source_sym->type()->ptr_to();
            in_ldc *source_address =
                    new in_ldc(source_ptr_type, operand(), immed(source_sym));
            source_sym->set_addr_taken();

            type_node *destination_ptr_type =
                    destination_sym->type()->ptr_to();
            in_ldc *destination_address =
                    new in_ldc(destination_ptr_type, operand(),
                               immed(destination_sym));
            destination_sym->set_addr_taken();

            the_rrr =
                    new in_rrr(io_memcpy, type_void, operand(),
                               operand(destination_address),
                               operand(source_address));
            the_copy->set_dst(operand());
            replace_instruction(the_copy, the_rrr);
            delete the_copy;
            break;
          }
        default:
            return;
      }

    assert(the_rrr->opcode() == io_memcpy);
    assert(the_rrr->dst_addr_op().type()->unqual()->is_ptr());
    ptr_type *dst_ptr_type =
            (ptr_type *)(the_rrr->dst_addr_op().type()->unqual());
    type_node *destination_type = dst_ptr_type->ref_type()->unqual();

    immed_list *new_immeds = new immed_list;
    new_immeds->append(immed(get_alignment(destination_type)));
    the_rrr->append_annote(k_struct_alignment, new_immeds);
  }

static void replace_instruction(instruction *the_instr, var_sym *replacement)
  {
    assert(the_instr != NULL);
    assert(the_instr->dst_op().is_instr());
    instruction *destination = the_instr->dst_op().instr();
    assert(destination != NULL);

    unsigned num_srcs = destination->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        if (destination->src_op(src_num) == operand(the_instr))
          {
            the_instr->remove();
            destination->set_src_op(src_num, operand(replacement));
            return;
          }
      }
    assert(FALSE);
  }

static void insert_comparison_computation(tree_node *location,
                                          instruction *comparison,
                                          var_sym *destination)
  {
    assert(location != NULL);
    assert(location->scope()->is_block());
    block_symtab *scope = (block_symtab *)(location->scope());

    label_sym *label1 = scope->new_unique_label();
    label1->reset_userdef();
    label_sym *label2 = scope->new_unique_label();
    label2->reset_userdef();

    instruction *branch_instr =
            new in_bj(io_btrue, label1, operand(comparison));
    instruction *set_false_instr =
            new in_ldc(destination->type(), operand(destination), immed(0));
    instruction *jump_instr = new in_bj(io_jmp, label2);
    instruction *label_instr_1 = new in_lab(label1);
    instruction *set_true_instr =
            new in_ldc(destination->type(), operand(destination), immed(1));
    instruction *label_instr_2 = new in_lab(label2);

    tree_instr *branch_node = new tree_instr(branch_instr);
    tree_instr *set_false_node = new tree_instr(set_false_instr);
    tree_instr *jump_node = new tree_instr(jump_instr);
    tree_instr *label_node_1 = new tree_instr(label_instr_1);
    tree_instr *set_true_node = new tree_instr(set_true_instr);
    tree_instr *label_node_2 = new tree_instr(label_instr_2);

    tree_node_list *parent = location->parent();
    assert(parent != NULL);

    parent->insert_before(branch_node, location->list_e());
    parent->insert_before(set_false_node, location->list_e());
    parent->insert_before(jump_node, location->list_e());
    parent->insert_before(label_node_1, location->list_e());
    parent->insert_before(set_true_node, location->list_e());
    parent->insert_before(label_node_2, location->list_e());
  }

static void put_side_effects(tree_node *location, instruction *the_instr)
  {
    assert(location != NULL);

    tree_node_list *parent = location->parent();
    assert(parent != NULL);

    if ((the_instr->opcode() == io_lod) || (the_instr->opcode() == io_cal) ||
        (the_instr->opcode() == io_gen))
      {
        parent->insert_before(new tree_instr(the_instr), location->list_e());
      }
    else
      {
        unsigned num_srcs = the_instr->num_srcs();
        for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
          {
            if (the_instr->src_op(src_num).is_symbol())
              {
                var_sym *src_var = the_instr->src_op(src_num).symbol();
                if (src_var->type()->is_volatile())
                  {
                    instruction *copy_instr =
                            new in_rrr(io_cpy, src_var->type(), operand(),
                                       operand(src_var));
                    parent->insert_before(new tree_instr(copy_instr),
                                          location->list_e());
                  }
              }
            else if (the_instr->src_op(src_num).is_expr())
              {
                instruction *src_instr = the_instr->src_op(src_num).instr();
                assert(src_instr != NULL);
                src_instr->remove();
                put_side_effects(location, src_instr);
              }
          }
        delete the_instr;
      }
  }

static void fix_symtab_types(base_symtab *the_symtab)
  {
    if (flag_no_struct_return)
      {
        type_node_list_iter type_iter(the_symtab->types());
        while (!type_iter.is_empty())
          {
            type_node *this_type = type_iter.step();
            if (this_type->is_func())
              {
                func_type *this_func = (func_type *)this_type;
                if (is_struct_or_array(this_func->return_type()))
                    change_func_type(this_func);
              }
          }
      }
  }

static void change_func_type(func_type *the_func_type)
  {
    if (the_func_type->args_known())
      {
        unsigned new_num_args = the_func_type->num_args() + 1;
        the_func_type->set_num_args(new_num_args);
        for (unsigned arg_num = new_num_args - 1; arg_num > 0; --arg_num)
          {
            the_func_type->set_arg_type(arg_num,
                                        the_func_type->arg_type(arg_num - 1));
          }

        the_func_type->set_arg_type(0, the_func_type->return_type()->ptr_to());
      }

    the_func_type->set_return_type(type_void);
  }

static boolean is_struct_or_array(type_node *the_type)
  {
    return (the_type->unqual()->is_struct() || the_type->unqual()->is_array());
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
