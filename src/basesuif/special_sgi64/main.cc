/* file "main.cc" of the special_sgi64 program for SUIF */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for the special_sgi64 program
 *  for SUIF.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:18:53 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        The special_sgi64 program does special processing to interface
        with the SGI64 back-end compiler.  It does this by means of
        ``#pragma'' lines, both ``#pragma'' lines in the input from
        system header files that give SUIF information about the
        back-end, and ``#pragma'' lines that SUIF puts in the output C
        files to communicate information SUIF has to the back-end
        compiler.  It also does special optimizations that are
        specific to the SGI64 back-end.

        This program should be run twice, once after the front end
        with the ``-in'' option to read in annotations, and once just
        before s2c with the ``-out'' option.

        These are the specific functions currently performed by this
        program:

            -in option:
              * Read ``#pragma no side effects'' annotations and add
                "pure function" annotations based on that information.

            -out option:
              * Add output ``#pragma no side effects'' annotations for
                functions with "pure function" annotations.
              * Add output ``#pragma intrinsic'' annotations for input
                ``#pragma intrinsic'' annotations.
              * Replace floating point io_abs instructions with calls
                to fabs or fabsf because the back-end treats these as
                intrinsics and can produce much better code than it
                could with the abs() macro that would otherwise be
                used for this instructions.
              * Add output ``#pragma ivdep'' annotations just before
                each for loop with the SUIF ``doall'' annotation.
              * Linearize multi-dimensional array reference
                instructions with pointer variables instead of address
                of symbols in their bases.  This is a hack to deal
                with the fact that the current SGI 64-bit compiler
                doesn't deal well with such multi-dimensional array
                references.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_C_pragma;
static const char *k_s2c_pragma;
static const char *k_doall;
static proc_sym *float_abs_sym = NULL;
static proc_sym *double_abs_sym = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void process_in_fse(file_set_entry *the_fse);
static void process_out_fse(file_set_entry *the_fse);
static void add_no_side_effects_annotes(global_symtab *the_symtab,
                                       file_set_entry *the_fse);
static void replace_fp_abs_on_proc(tree_proc *the_proc);
static void replace_fp_abs_on_node(tree_node *the_node, void *);
static void replace_fp_abs_on_instr(instruction *the_instr);
static void ivdep_pragmas_on_proc(tree_proc *the_proc);
static void ivdep_pragmas_on_node(tree_node *the_node, void *);
static void special_linearization_on_symtab(base_symtab *the_symtab);
static void special_linearization_on_proc(tree_proc *the_proc);
static void special_linearization_on_node(tree_node *the_node, void *);
static void special_linearization_on_instr(instruction *the_instr);
static proc_sym *get_abs_func(char *name, type_node *arg_type);
static ptr_type *fixed_var_type(ptr_type *old_var_type);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 4) || (argc % 2 != 0))
        usage();

    boolean is_in;
    if (strcmp(argv[1], "-in") == 0)
        is_in = TRUE;
    else if (strcmp(argv[1], "-out") == 0)
        is_in = FALSE;
    else
        usage();

    ANNOTE(k_C_pragma, "C pragma", TRUE);
    ANNOTE(k_s2c_pragma, "s2c pragma", TRUE);
    ANNOTE(k_doall, "doall", TRUE);

    for (int arg_num = 2; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;

        if (is_in)
            process_in_fse(fse);
        else
            process_out_fse(fse);

        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            if (!is_in)
              {
                replace_fp_abs_on_proc(this_proc_sym->block());
                ivdep_pragmas_on_proc(this_proc_sym->block());
                special_linearization_on_proc(this_proc_sym->block());
              }
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }

        if (!is_in)
            special_linearization_on_symtab(fse->symtab());
      }

    special_linearization_on_symtab(fileset->globals());

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr,
            "usage: %s { -in | -out } <infile> <outfile> { <infile> <outfile>"
            " }*\n", _suif_prog_base_name);
    exit(1);
  }

static void process_in_fse(file_set_entry *the_fse)
  {
    annote_list_iter annote_iter(the_fse->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();
        if (this_annote->name() == k_C_pragma)
          {
            immed_list *this_data = this_annote->immeds();
            if ((this_data->count() == 6) &&
                ((*this_data)[0] == immed("no")) &&
                ((*this_data)[1] == immed("side")) &&
                ((*this_data)[2] == immed("effects")) &&
                ((*this_data)[3] == immed("(")) &&
                ((*this_data)[5] == immed(")")) &&
                (*this_data)[4].is_symbol())
              {
                sym_node *this_sym = (*this_data)[4].symbol();
                if (this_sym->is_proc())
                  {
                    if (this_sym->annotes()->peek_annote(k_pure_function) ==
                        NULL)
                      {
                        this_sym->append_annote(k_pure_function, NULL);
                      }
                  }
              }
          }
      }
  }

static void process_out_fse(file_set_entry *the_fse)
  {
    annote_list_iter annote_iter(the_fse->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();
        if (this_annote->name() == k_C_pragma)
          {
            immed_list *this_data = this_annote->immeds();
            if ((this_data->count() == 4) &&
                ((*this_data)[0] == immed("intrinsic")) &&
                ((*this_data)[1] == immed("(")) &&
                ((*this_data)[3] == immed(")")) &&
                (*this_data)[2].is_symbol())
              {
                sym_node *this_sym = (*this_data)[2].symbol();
                if (this_sym->is_proc())
                  {
                    immed_list *new_immeds = new immed_list;
                    new_immeds->append(immed("intrinsic ("));
                    new_immeds->append(this_sym);
                    new_immeds->append(immed(")"));
                    the_fse->append_annote(k_s2c_pragma, new_immeds);
                  }
              }
          }
      }

    add_no_side_effects_annotes(fileset->globals(), the_fse);
    add_no_side_effects_annotes(the_fse->symtab(), the_fse);
  }

static void add_no_side_effects_annotes(global_symtab *the_symtab,
                                       file_set_entry *the_fse)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->is_proc() &&
            (this_sym->annotes()->peek_annote(k_pure_function) != NULL))
          {
            immed_list *new_immeds = new immed_list;
            new_immeds->append(immed("no side effects ("));
            new_immeds->append(this_sym);
            new_immeds->append(immed(")"));
            the_fse->append_annote(k_s2c_pragma, new_immeds);
          }
      }
  }

static void replace_fp_abs_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&replace_fp_abs_on_node, NULL);
  }

static void replace_fp_abs_on_node(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        replace_fp_abs_on_instr(the_tree_instr->instr());
      }
  }

static void replace_fp_abs_on_instr(instruction *the_instr)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_expr())
            replace_fp_abs_on_instr(this_op.instr());
      }

    if ((the_instr->opcode() == io_abs) &&
        (the_instr->result_type()->is_same(type_float) ||
         the_instr->result_type()->is_same(type_double)))
      {
        in_rrr *the_rrr = (in_rrr *)the_instr;

        proc_sym *the_proc_sym;
        if (the_rrr->result_type()->is_same(type_float))
          {
            if (float_abs_sym == NULL)
                float_abs_sym = get_abs_func("fabsf", type_float);
            the_proc_sym = float_abs_sym;
          }
        else
          {
            if (double_abs_sym == NULL)
                double_abs_sym = get_abs_func("fabs", type_double);
            the_proc_sym = double_abs_sym;
          }

        in_ldc *new_proc_ldc =
                new in_ldc(the_proc_sym->type()->ptr_to(), operand(),
                           immed(the_proc_sym));
        in_cal *new_cal =
                new in_cal(the_instr->result_type(), operand(),
                           operand(new_proc_ldc), 1);

        operand src_op = the_rrr->src_op();
        if (src_op.is_expr())
            src_op.remove();

        new_cal->set_argument(0, src_op);
        replace_instruction(the_rrr, new_cal);
        delete the_rrr;
      }
  }

static void ivdep_pragmas_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&ivdep_pragmas_on_node, NULL);
  }

static void ivdep_pragmas_on_node(tree_node *the_node, void *)
  {
    if (the_node->is_for() && (the_node->annotes()->peek_annote(k_doall)))
      {
        immed_list *new_immeds = new immed_list;
        new_immeds->append(immed("ivdep"));
        in_rrr *new_mark = new in_rrr(io_mrk);
        new_mark->append_annote(k_s2c_pragma, new_immeds);
        tree_instr *new_node = new tree_instr(new_mark);
        the_node->parent()->insert_before(new_node, the_node->list_e());
      }
  }

static void special_linearization_on_symtab(base_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->is_var())
          {
            var_sym *this_var = (var_sym *)this_sym;
            type_node *this_type = this_var->type()->unqual();
            if (this_type->is_ptr())
              {
                ptr_type *this_ptr = (ptr_type *)this_type;
                if (this_ptr->ref_type()->unqual()->is_array())
                    this_var->set_type(fixed_var_type(this_ptr));
              }
          }
      }
  }

static void special_linearization_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&special_linearization_on_node, NULL, FALSE);
    special_linearization_on_node(the_proc, NULL);
  }

static void special_linearization_on_node(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();
        special_linearization_on_instr(the_instr);

        operand dst_op = the_instr->dst_op();
        if (dst_op.is_symbol() && dst_op.type()->is_ptr())
          {
            ptr_type *this_ptr = (ptr_type *)(dst_op.type());
            if (this_ptr->ref_type()->unqual()->is_array())
              {
                the_tree_instr->remove_instr(the_instr);
                the_instr->set_dst(operand());
                ptr_type *new_type = fixed_var_type(this_ptr);
                in_rrr *new_cvt =
                        new in_rrr(io_cvt, new_type, dst_op,
                                   operand(the_instr));
                the_tree_instr->set_instr(new_cvt);
              }
          }
      }
    else if (the_node->is_block())
      {
        tree_block *the_block = (tree_block *)the_node;
        special_linearization_on_symtab(the_block->symtab());
      }
  }

static void special_linearization_on_instr(instruction *the_instr)
  {
    if (the_instr->opcode() == io_array)
      {
        in_array *the_aref = (in_array *)the_instr;
        operand base_op = the_aref->base_op();
        while (base_op.is_expr() &&
               ((base_op.instr()->opcode() == io_cpy) ||
                (base_op.instr()->opcode() == io_cvt)))
          {
            in_rrr *the_rrr = (in_rrr *)(base_op.instr());
            if (!the_rrr->src_op().type()->is_ptr())
                break;
            base_op = the_rrr->src_op();
          }

        if ((the_aref->dims() > 1) &&
            ((!base_op.is_expr()) || (base_op.instr()->opcode() != io_ldc)))
          {
            type_node *elem_type = the_aref->elem_type();
            array_type *new_array =
                    new array_type(elem_type, array_bound(0), unknown_bound);
            type_node *new_type = elem_type->parent()->install_type(new_array);
            base_op.remove();

            operand remaining_base = the_aref->base_op();
            remaining_base.remove();
            if (remaining_base.is_expr())
                delete remaining_base.instr();

            in_rrr *new_cvt =
                    new in_rrr(io_cvt, new_type->ptr_to(), operand(), base_op);
            the_aref->set_base_op(operand(new_cvt));

            operand new_index = the_aref->index(0);
            new_index.remove();
            operand bound_op = the_aref->bound(0);
            bound_op.remove();
            if (bound_op.is_expr())
                delete bound_op.instr();
            unsigned num_dims = the_aref->dims();
            for (unsigned dim_num = 1; dim_num < num_dims; ++dim_num)
              {
                operand this_index = the_aref->index(dim_num);
                operand this_bound = the_aref->bound(dim_num);
                this_index.remove();
                this_bound.remove();

                if (this_bound.type() != new_index.type())
                  {
                    this_bound = cast_op(this_bound, type_ptr_diff);
                    new_index = cast_op(new_index, type_ptr_diff);
                  }

                new_index =
                        fold_real_2op_rrr(io_mul, new_index.type(), new_index,
                                          this_bound);

                if (this_index.type() != new_index.type())
                  {
                    this_index = cast_op(this_index, type_ptr_diff);
                    new_index = cast_op(new_index, type_ptr_diff);
                  }

                new_index =
                        fold_real_2op_rrr(io_add, new_index.type(), new_index,
                                          this_index);
              }

            the_aref->set_dims(1);
            the_aref->set_index(0, new_index);
            the_aref->set_bound(0, operand());
          }
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_expr())
          {
            special_linearization_on_instr(this_op.instr());
          }
        else if (this_op.is_symbol() && this_op.type()->is_ptr())
          {
            ptr_type *this_ptr = (ptr_type *)(this_op.type());
            if (this_ptr->ref_type()->unqual()->is_array())
              {
                in_rrr *new_cvt =
                        new in_rrr(io_cvt, this_ptr, operand(), this_op);
                the_instr->set_src_op(src_num, operand(new_cvt));
              }
          }
      }
  }

static proc_sym *get_abs_func(char *name, type_node *arg_type)
  {
    proc_sym *result = fileset->globals()->lookup_proc(name, FALSE);
    if (result == NULL)
      {
        func_type *the_func_type = new func_type(arg_type, 1);
        the_func_type->set_arg_type(0, arg_type);
        the_func_type =
                (func_type *)(arg_type->parent()->install_type(the_func_type));
        result =
                fileset->globals()->new_proc(the_func_type, src_unknown, name);
      }
    return result;
  }

static ptr_type *fixed_var_type(ptr_type *old_var_type)
  {
    type_node *elem_type = old_var_type->ref_type()->unqual();
    assert(elem_type->is_array());
    while (elem_type->is_array())
      {
        array_type *elem_array = (array_type *)elem_type;
        elem_type = elem_array->elem_type()->unqual();
      }
    return elem_type->ptr_to();
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
