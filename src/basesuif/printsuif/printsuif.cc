/*  Read and display a SUIF version 1.x file (or a set of SUIF version 1.x
    files). */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#define RCS_BASE_FILE printsuif_cc

#include <suif1.h>
#include <string.h>

RCS_BASE(
    "$Id: printsuif.cc,v 1.2 1999/08/25 03:27:38 brm Exp $")

INCLUDE_SUIF_COPYRIGHT


static void usage();
static boolean delete_dummy_srcs(instruction *, operand *r, void *);


int main(int argc, char *argv[])
{
    boolean ftn_mode = FALSE;
    boolean exp_trees = TRUE;
    boolean mach = FALSE;

    start_suif(argc, argv);

    /* check for command-line arguments */
    argv++; argc--;
    while ((*argv != NULL) && (**argv == '-')) {
	if (!strcmp(*argv, "-ftn")) {
	    ftn_mode = TRUE;
	} else if (!strcmp(*argv, "-no-exprs")) {
	    exp_trees = FALSE;
	} else if (!strcmp(*argv, "-raw")) {
	    /* set the library's raw_syms flag (if TRUE the symtab
	       lookup_sym_id and lookup_type_id methods will create dummy
	       sym_nodes/type_nodes for unknown ids) */
	    _suif_raw_syms = TRUE;
	} else if (!strcmp(*argv, "-mach")) {
	    mach = TRUE;
	} else if (!strcmp(*argv, "-print-flat")) {
	    _suif_flat_annotes = TRUE;
	} else if (!strcmp(*argv, "-no-types")) {
	    _suif_no_types = TRUE;
	} else if (!strcmp(*argv, "-no-symtabs")) {
	    _suif_no_symtabs = TRUE;
	} else {
	    usage();
	}
	argv++; argc--;
    }
    if (argc < 1) usage();

    while (*argv) {

	if (!_suif_raw_syms) {

	    fileset->add_file(*argv, NULL);

	} else {

	    in_stream *is = new in_stream(lexicon->enter(*argv)->sp);
	    is->open();

	    printf("******************** FILE %s ********************\n",
		   is->name());

	    global_symtab *syms = new global_symtab(is->name());
	    instruction *i;

	    /* read and print the file as a list of instructions */
	    while ((i = instruction::read(is, syms, NULL))) {
		i->print(stdout, 3);
		i->src_map(delete_dummy_srcs, NULL);
		delete i;
	    }

	    delete syms;
	}

	argv++; argc--;
    }

    if (!_suif_raw_syms) {

	if (mach) {
	    /* print the target machine parameters */
	    target.print(stdout, 2);
	    fputs("\n\n", stdout);
	}

	if (!_suif_no_symtabs) {
	    printf("    ****************** GLOBALS ******************\n");
	    fileset->globals()->print(stdout, 2);
	    fputs("\n\n", stdout);
	}

	fileset->reset_iter();
	file_set_entry *fse;
	while ((fse = fileset->next_file())) {
	    printf("    ****************** FILE %s ******************",
		   fse->name());
	    if (fse->are_annotations()) fse->print_annotes(stdout, 2);
	    if (!_suif_no_symtabs) {
		fputs("\n", stdout);
		fse->symtab()->print(stdout, 2);
	    } else {
		fputs("\n", stdout);
	    }

	    proc_sym *psym;
	    fse->reset_proc_iter();
	    while ((psym = fse->next_proc())) {
		fputs("\n\n", stdout);
		psym->read_proc(exp_trees, ftn_mode);
		psym->block()->print(stdout, 2);
		psym->flush_proc();
	    }

	    fputs("\n\n", stdout);
	}
    }

    exit_suif();
    return 0;
}


void
usage ()
{
    fprintf(stderr, "Usage: printsuif [flags] suif1_files\n");
    fprintf(stderr, "Flags:\n");
    fprintf(stderr, "\t-ftn\t\tFortran mode; use call-by-ref params\n");
    fprintf(stderr, "\t-no-symtabs\tdon't print the symbol tables\n");
    fprintf(stderr, "\t-no-types\tomit instruction result types\n");
    fprintf(stderr, "\t-no-exprs\tuse flat instruction lists\n");
    fprintf(stderr, "\t-print-flat\tprint annotations as flat immed lists\n");
    fprintf(stderr, "\t-raw\t\tprint raw instruction list for debugging\n");
    fprintf(stderr, "\t-mach\t\tshow the target machine parameters\n");
    exit(-1);
}


boolean
delete_dummy_srcs (instruction *, operand *r, void *)
{
    if (r->is_instr()) {
	r->remove();
	delete r->instr()->parent();
    }
    return FALSE;
}

