/* file "ip1const.cc" of the ip1const program for SUIF */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for the ip1const program for
 *  SUIF.
 */

#define RCS_BASE_FILE ip1const_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: ip1const.cc,v 1.1.1.1 1998/06/16 15:17:31 brm Exp $")

INCLUDE_SUIF_COPYRIGHT


const char *k_ip1const_ready_for_consts;
const char *k_ip1const_const_data;


static void usage(void);
static void prepare_symtab(global_symtab *the_symtab);
static void handle_all_callsites(tree_node *the_node);
static void handle_callsites_on_object(suif_object *the_object);
static void handle_callsite(in_cal *the_call);
static void handle_callee(tree_proc *the_callee);


extern int main(int argc, char **argv)
  {
    start_suif(argc, argv);

    ANNOTE(k_ip1const_ready_for_consts, "ip1const ready for consts", FALSE);
    ANNOTE(k_ip1const_const_data,       "ip1const const data",       FALSE);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    prepare_symtab(fileset->globals());

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        prepare_symtab(fse->symtab());
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = fse->next_proc();
            if (this_proc == NULL)
                break;
            this_proc->read_proc(TRUE, FALSE);
            handle_all_callsites(this_proc->block());
            this_proc->flush_proc();
          }
      }

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = fse->next_proc();
            if (this_proc == NULL)
                break;
            this_proc->read_proc(TRUE, FALSE);
            handle_callee(this_proc->block());
            this_proc->write_proc(fse);
            this_proc->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }


static void usage(void)
  {
    fprintf(stderr,
            "usage: %s [options] <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

/*
 *  For each proc_sym in the_symtab that is guaranteed to not be used
 *  outside the current file set and guaranteed to be used in direct
 *  calls only, add a k_ip1const_ready_for_consts annotation.
 */
static void prepare_symtab(global_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (!this_sym->is_proc())
            continue;
        proc_sym *this_proc = (proc_sym *)this_sym;
        if (!unreferenced_outside_fileset(this_proc))
            continue;
        if (this_proc->peek_annote(k_direct_calls_only) == NULL)
            continue;
        this_proc->append_annote(k_ip1const_ready_for_consts);
      }
  }

static void handle_all_callsites(tree_node *the_node)
  {
    walk(the_node, &handle_callsites_on_object);
  }

static void handle_callsites_on_object(suif_object *the_object)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_cal)
        return;
    in_cal *the_call = (in_cal *)the_instr;
    handle_callsite(the_call);
  }

static void handle_callsite(in_cal *the_call)
  {
    proc_sym *callee = proc_for_call(the_call);
    if (callee == NULL)
        return;
    annote *ready_annote =
            callee->annotes()->get_annote(k_ip1const_ready_for_consts);
    if (ready_annote != NULL)
      {
        delete ready_annote;
        immed_list *new_data = new immed_list;
        unsigned num_args = the_call->num_args();
        for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
          {
            immed this_immed;
            eval_status status =
                    evaluate_const_expr(the_call->argument(arg_num),
                                        &this_immed);
            if ((status == EVAL_OK) &&
                (!(this_immed.is_symbol() &&
                   ((this_immed.symbol()->is_var() &&
                     ((var_sym *)(this_immed.symbol()))->is_auto()) ||
                    (this_immed.symbol()->is_label())))))
              {
                new_data->append(this_immed);
              }
            else
              {
                new_data->append(immed());
              }
          }
        callee->append_annote(k_ip1const_const_data, new_data);
      }
    else
      {
        annote *data_annote =
                callee->annotes()->peek_annote(k_ip1const_const_data);
        if (data_annote == NULL)
            return;
        immed_list *data_list = data_annote->immeds();
        unsigned num_args = the_call->num_args();
        unsigned data_items = data_list->count();
        if (num_args > data_items)
          {
            for (unsigned extra_count = 0; extra_count < num_args - data_items;
                 ++extra_count)
              {
                data_list->append(immed());
              }
          }
        immed_list_e *follow = data_list->head();
        for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
          {
            assert(follow != NULL);
            immed_list_e *current = follow;
            follow = follow->next();
            immed old_data = current->contents;
            if (old_data.kind() == im_undef)
                continue;
            immed this_immed;
            eval_status status =
                    evaluate_const_expr(the_call->argument(arg_num),
                                        &this_immed);
            if (status == EVAL_OK)
              {
                if (old_data != this_immed)
                    current->contents = immed();
              }
            else
              {
                current->contents = immed();
              }
          }
      }
  }

static void handle_callee(tree_proc *the_callee)
  {
    proc_sym *the_proc_sym = the_callee->proc();
    annote *data_annote =
            the_proc_sym->annotes()->peek_annote(k_ip1const_const_data);
    if (data_annote == NULL)
        return;
    proc_symtab *proc_syms = the_callee->proc_syms();
    immed_list_iter immed_iter(data_annote->immeds());
    sym_node_list_iter param_iter(proc_syms->params());
    while (!immed_iter.is_empty() && !param_iter.is_empty())
      {
        immed this_immed = immed_iter.step();
        sym_node *this_sym = param_iter.step();
        if (this_immed.kind() == im_undef)
            continue;
        assert(this_sym->is_var());
        var_sym *this_param = (var_sym *)this_sym;
        type_node *param_type = this_param->type()->unqual();
        var_sym *new_var =
                proc_syms->new_unique_var(param_type, this_param->name());
        the_callee->body()->push(assign(new_var,
                                        const_op(this_immed, param_type)));
        this_param->append_annote(k_replacement, new immed_list(new_var));
      }
    do_replacement(the_callee);
  }
