#include "sf2c.h"

d_cnjg(r, z)
doublecomplex *r, *z;
{
r->_r = z->_r;
r->_i = - z->_i;
}
