#include "sf2c.h"

VOID z_div(c, a, b)
doublecomplex *a, *b, *c;
{
double ratio, den;
double abr, abi;

if( (abr = b->_r) < 0.)
	abr = - abr;
if( (abi = b->_i) < 0.)
	abi = - abi;
if( abr <= abi )
	{
	if(abi == 0)
		abort(); /* fatal("complex division by zero"); */
	ratio = b->_r / b->_i ;
	den = b->_i * (1 + ratio*ratio);
	c->_r = (a->_r*ratio + a->_i) / den;
	c->_i = (a->_i*ratio - a->_r) / den;
	}

else
	{
	ratio = b->_i / b->_r ;
	den = b->_r * (1 + ratio*ratio);
	c->_r = (a->_r + a->_i*ratio) / den;
	c->_i = (a->_i - a->_r*ratio) / den;
	}

}
