#include "sf2c.h"

VOID c_cos(r, z)
complex *r, *z;
{
double sin(), cos(), sinh(), cosh();

r->_r = cos(z->_r) * cosh(z->_i);
r->_i = - sin(z->_r) * sinh(z->_i);
}
