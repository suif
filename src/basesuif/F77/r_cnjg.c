#include "sf2c.h"

VOID r_cnjg(r, z)
complex *r, *z;
{
r->_r = z->_r;
r->_i = - z->_i;
}
