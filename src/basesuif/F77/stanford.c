/* DEM */
/* functions sf2c creates that we don't want macro-expanded */

#include <stdarg.h>
#include "sf2c.h"

/* MIN functions */
integer i_min(long num_args, ...)
{
	va_list ap;
	integer best,temp,i;

	va_start(ap, num_args);
	
	best = va_arg(ap,integer);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,integer);
		if (temp < best) best = temp;
	}
	
	va_end(ap);
	return best;
}

shortint h_min(long num_args, ...)
{
	va_list ap;
	shortint best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,integer);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,integer);
		if (temp < best) best = temp;
	}
	
	va_end(ap);
	return best;
}

real r_min(long num_args, ...)
{
	va_list ap;
	real best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,doublereal);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,doublereal);
		if (temp < best) best = temp;
	}
	
	va_end(ap);
	return best;
}

doublereal d_min(long num_args, ...)
{
	va_list ap;
	doublereal best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,doublereal);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,doublereal);
		if (temp < best) best = temp;
	}
	
	va_end(ap);
	return best;
}

real ri_min(long num_args, ...)
{
	va_list ap;
	real best;
	integer temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,integer);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,integer);
		if (temp < best) best = temp;
	}
	
	va_end(ap);
	return best;
}

doublereal di_min(long num_args, ...)
{
	va_list ap;
	doublereal best;
	integer temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,integer);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,integer);
		if (temp < best) best = temp;
	}
	
	va_end(ap);
	return best;
}

integer ir_min(long num_args, ...)
{
	va_list ap;
	real best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,doublereal);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,doublereal);
		if (temp < best) best = temp;
	}
	
	va_end(ap);
	return best;
}

integer id_min(long num_args, ...)
{
	va_list ap;
	doublereal best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,doublereal);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,doublereal);
		if (temp < best) best = temp;
	}
	
	va_end(ap);
	return best;
}

/* MAX functions */
integer i_max(long num_args, ...)
{
	va_list ap;
	integer best,temp,i;

	va_start(ap, num_args);
	
	best = va_arg(ap,integer);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,integer);
		if (temp > best) best = temp;
	}
	
	va_end(ap);
	return best;
}

shortint h_max(long num_args, ...)
{
	va_list ap;
	shortint best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,integer);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,integer);
		if (temp > best) best = temp;
	}
	
	va_end(ap);
	return best;
}

real r_max(long num_args, ...)
{
	va_list ap;
	real best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,doublereal);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,doublereal);
		if (temp > best) best = temp;
	}
	
	va_end(ap);
	return best;
}

doublereal d_max(long num_args, ...)
{
	va_list ap;
	doublereal best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,doublereal);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,doublereal);
		if (temp > best) best = temp;
	}
	
	va_end(ap);
	return best;
}

real ri_max(long num_args, ...)
{
	va_list ap;
	real best;
	integer temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,integer);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,integer);
		if (temp > best) best = temp;
	}
	
	va_end(ap);
	return best;
}

doublereal di_max(long num_args, ...)
{
	va_list ap;
	doublereal best;
	integer temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,integer);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,integer);
		if (temp > best) best = temp;
	}
	
	va_end(ap);
	return best;
}

integer ir_max(long num_args, ...)
{
	va_list ap;
	real best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,doublereal);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,doublereal);
		if (temp > best) best = temp;
	}
	
	va_end(ap);
	return best;
}

integer id_max(long num_args, ...)
{
	va_list ap;
	doublereal best,temp;
	integer i;

	va_start(ap, num_args);
	
	best = va_arg(ap,doublereal);
	for (i=2; i<=num_args; i++) {
		temp = va_arg(ap,doublereal);
		if (temp > best) best = temp;
	}
	
	va_end(ap);
	return best;
}


/* ABS functions */

double sd_abs(x)
doublereal x;
{
if(x >= 0)
	return(x);
return(- x);
}

double sc_abs(z)
complex *z;
{
return sqrt((z->_r * z->_r) + (z->_i * z->_i));
}

double sr_abs(x)
real x;
{
if(x >= 0)
	return(x);
return(- x);
}

double sz_abs(z)
doublecomplex *z;
{
return sqrt((z->_r * z->_r) + (z->_i * z->_i));
}

integer si_abs(x)
integer x;
{
if(x >= 0)
	return(x);
return(- x);
}

shortint sh_abs(x)
shortint x;
{
if(x >= 0)
	return(x);
return(- x);
}
