#include "sf2c.h"

VOID c_log(r, z)
complex *r, *z;
{
double log(), f77_cabs(), atan2();

r->_i = atan2(z->_i, z->_r);
r->_r = log( f77_cabs(z->_r, z->_i) );
}
