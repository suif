#include "sf2c.h"

VOID c_exp(r, z)
complex *r, *z;
{
double expx;
double exp(), cos(), sin();

expx = exp(z->_r);
r->_r = expx * cos(z->_i);
r->_i = expx * sin(z->_i);
}
