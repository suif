#include "sf2c.h"

VOID pow_ci(p, a, b) 	/* p = a**b  */
complex *p, *a;
integer *b;
{
doublecomplex p1, a1;

a1._r = a->_r;
a1._i = a->_i;

pow_zi(&p1, &a1, b);

p->_r = p1._r;
p->_i = p1._i;
}
