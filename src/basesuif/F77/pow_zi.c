#include "sf2c.h"

VOID pow_zi(p, a, b) 	/* p = a**b  */
doublecomplex *p, *a;
integer *b;
{
integer n;
double t;
doublecomplex x;
static doublecomplex one = {1.0, 0.0};

n = *b;
p->_r = 1;
p->_i = 0;

if(n == 0)
	return;
if(n < 0)
	{
	n = -n;
	z_div(&x, &one, a);
	}
else
	{
	x._r = a->_r;
	x._i = a->_i;
	}

for( ; ; )
	{
	if(n & 01)
		{
		t = p->_r * x._r - p->_i * x._i;
		p->_i = p->_r * x._i + p->_i * x._r;
		p->_r = t;
		}
	if(n >>= 1)
		{
		t = x._r * x._r - x._i * x._i;
		x._i = 2 * x._r * x._i;
		x._r = t;
		}
	else
		break;
	}
}
