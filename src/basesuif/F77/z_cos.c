#include "sf2c.h"

VOID z_cos(r, z)
doublecomplex *r, *z;
{
double sin(), cos(), sinh(), cosh();

r->_r = cos(z->_r) * cosh(z->_i);
r->_i = - sin(z->_r) * sinh(z->_i);
}
