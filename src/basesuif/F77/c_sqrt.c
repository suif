#include "sf2c.h"

VOID c_sqrt(r, z)
complex *r, *z;
{
double mag, t, sqrt(), f77_cabs();

if( (mag = f77_cabs(z->_r, z->_i)) == 0.)
	r->_r = r->_i = 0.;
else if(z->_r > 0)
	{
	r->_r = t = sqrt(0.5 * (mag + z->_r) );
	t = z->_i / t;
	r->_i = 0.5 * t;
	}
else
	{
	t = sqrt(0.5 * (mag - z->_r) );
	if(z->_i < 0)
		t = -t;
	r->_i = t;
	t = z->_i / t;
	r->_r = 0.5 * t;
	}
}
