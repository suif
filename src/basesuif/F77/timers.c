/******************************************/
/* timer routines for bench mark programs */
/******************************************/

#include	<sys/types.h>
#include	<sys/times.h>
#include	"sf2c.h"

cputim_(t)
real	*t;
{
struct	tms	tbuf;

times(&tbuf);
*t = (real)(tbuf.tms_utime+tbuf.tms_stime)/60;
}

elapse_(t)
real	*t;
{
*t = (real)time((long *)0);
}
