#include "sf2c.h"

VOID z_sqrt(r, z)
doublecomplex *r, *z;
{
double mag, sqrt(), f77_cabs();

if( (mag = f77_cabs(z->_r, z->_i)) == 0.)
	r->_r = r->_i = 0.;
else if(z->_r > 0)
	{
	r->_r = sqrt(0.5 * (mag + z->_r) );
	r->_i = z->_i / r->_r / 2;
	}
else
	{
	r->_i = sqrt(0.5 * (mag - z->_r) );
	if(z->_i < 0)
		z->_i = - z->_i;
	r->_r = z->_i / r->_i / 2;
	}
}
