#include "sf2c.h"

VOID z_sin(r, z)
doublecomplex *r, *z;
{
double sin(), cos(), sinh(), cosh();

r->_r = sin(z->_r) * cosh(z->_i);
r->_i = cos(z->_r) * sinh(z->_i);
}
