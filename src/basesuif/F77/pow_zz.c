#include "sf2c.h"

VOID pow_zz(r,a,b)
doublecomplex *r, *a, *b;
{
double logr, logi, x, y;
double log(), exp(), cos(), sin(), atan2(), f77_cabs();

logr = log( f77_cabs(a->_r, a->_i) );
logi = atan2(a->_i, a->_r);

x = exp( logr * b->_r - logi * b->_i );
y = logr * b->_i + logi * b->_r;

r->_r = x * cos(y);
r->_i = x * sin(y);
}
