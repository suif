#include "sf2c.h"

VOID c_sin(r, z)
complex *r, *z;
{
double sin(), cos(), sinh(), cosh();

r->_r = sin(z->_r) * cosh(z->_i);
r->_i = cos(z->_r) * sinh(z->_i);
}
