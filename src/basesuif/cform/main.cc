/* file "main.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 * This is the main program for the cform program for the SUIF system.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:13 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

        The specific effects of cform are as follows:

          * It re-indexes all arrays so they start at zero.  It
            assumes that all the array type information on the base
            operand of all aref instructions is correct, i.e. that it
            corresponds exactly to the offset operand.

 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

boolean errors = FALSE;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void do_proc(tree_proc *the_tree_proc);
static void reindex_types(base_symtab *the_symtab);
static void reindex_array_type(array_type *the_array_type);
static void reindex_on_tree_node(tree_node *the_node, void *);
static void reindex_on_instr(instruction *the_instr);
static void reindex_on_operand(operand the_operand);
static void reindex_on_aref(in_array *the_aref);
static operand operand_from_int_const(type_node *result_type, int the_const);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if (argc < 2)
        error_line(1, NULL, "no file specifications given");
    else if (argc == 2)
        error_line(1, NULL, "no output file specification given");
    else if (argc > 3)
        error_line(1, NULL, "too many file specifications given");

    fileset->add_file(argv[1], argv[2]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            do_proc(this_proc_sym->block());
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
        reindex_types(fse->symtab());
      }

    reindex_types(fileset->globals());
    delete fileset;

    if (errors)
        return 1;
    else
        return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void do_proc(tree_proc *the_tree_proc)
  {
    assert(the_tree_proc != NULL);
    the_tree_proc->map(&reindex_on_tree_node, NULL, FALSE);
    reindex_on_tree_node(the_tree_proc, NULL);
  }

static void reindex_types(base_symtab *the_symtab)
  {
    assert(the_symtab != NULL);

    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (this_type->is_array())
            reindex_array_type((array_type *)this_type);
      }
  }

static void reindex_array_type(array_type *the_array_type)
  {
    if (the_array_type->lower_bound().is_constant() &&
        (the_array_type->lower_bound().constant() == 0))
      {
        return;
      }

    if (the_array_type->upper_bound().is_variable() ||
        !the_array_type->lower_bound().is_constant())
      {
        return;
      }

    if (the_array_type->upper_bound().is_constant())
      {
        int new_bound =
                the_array_type->upper_bound().constant() -
                the_array_type->lower_bound().constant();
        the_array_type->set_upper_bound(array_bound(new_bound));
      }

    the_array_type->set_lower_bound(array_bound(0));
  }

static void reindex_on_tree_node(tree_node *the_node, void *)
  {
    assert(the_node != NULL);

    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        reindex_on_instr(the_tree_instr->instr());
      }

    if (the_node->is_block())
      {
        tree_block *the_block = (tree_block *)the_node;
        reindex_types(the_block->symtab());
      }
  }

static void reindex_on_instr(instruction *the_instr)
  {
    assert(the_instr != NULL);

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
        reindex_on_operand(the_instr->src_op(src_num));

    if (the_instr->opcode() == io_array)
        reindex_on_aref((in_array *)the_instr);
  }

static void reindex_on_operand(operand the_operand)
  {
    if (the_operand.is_expr())
        reindex_on_instr(the_operand.instr());
  }

static void reindex_on_aref(in_array *the_aref)
  {
    assert(the_aref != NULL);

    type_node *base_type = the_aref->base_op().type()->unqual();

    if (!base_type->is_ptr())
      {
        error_line(0, the_aref->parent(),
                   "base operand of array reference is not a pointer");
        errors = TRUE;
        return;
      }
    ptr_type *the_ptr = (ptr_type *)base_type;

    type_node *current_type = the_ptr->ref_type()->unqual();

    operand new_offset =
            operand(new in_ldc(type_ptr_diff, operand(), immed(0)));

    unsigned num_dims = the_aref->dims();
    for (unsigned dim_num = 0; dim_num < num_dims; ++dim_num)
      {
        if (!current_type->is_array())
          {
            if (dim_num == 0)
              {
                error_line(0, the_aref->parent(),
                           "base operand of array reference is not a pointer "
                           "to an array");
              }
            else
              {
                error_line(0, the_aref->parent(),
                           "array reference with %d dimensions on type with "
                           "only %d dimension%s", num_dims, dim_num,
                           (dim_num > 1) ? "s" : "");
              }
            errors = TRUE;
            return;
          }
        array_type *this_array_type = (array_type *)current_type;

        operand old_bound = the_aref->bound(dim_num);

        new_offset =
                fold_real_2op_rrr(io_mul, type_ptr_diff, new_offset,
                                  old_bound.clone());

        operand old_lower =
                operand_from_array_bound(this_array_type->lower_bound());

        if (this_array_type->upper_bound().is_variable() ||
            !this_array_type->lower_bound().is_constant())
          {
            new_offset =
                    fold_real_2op_rrr(io_add, type_ptr_diff, new_offset,
                                      old_lower);
          }
        else
          {
            old_bound.remove();
            if (old_bound.is_expr())
                delete old_bound.instr();

            if (this_array_type->upper_bound().is_unknown())
              {
                the_aref->set_bound(dim_num, operand());
              }
            else
              {
                operand old_upper =
                        operand_from_array_bound(
                                this_array_type->upper_bound());
                operand new_bound =
                        fold_real_2op_rrr(io_sub, type_ptr_diff, old_upper,
                                          old_lower.clone());
                new_bound =
                        fold_real_2op_rrr(io_add, type_ptr_diff, new_bound, 
                                          operand_from_int_const(type_ptr_diff,
                                                                 1));
                the_aref->set_bound(dim_num, new_bound);
              }

            operand index = the_aref->index(dim_num);
            index.remove();
            index = fold_real_2op_rrr(io_sub, type_ptr_diff, index, old_lower);
            the_aref->set_index(dim_num, index);
          }

        current_type = this_array_type->elem_type();
      }

    operand old_offset = the_aref->offset_op();
    old_offset.remove();
    if (old_offset.is_expr())
        delete old_offset.instr();
    the_aref->set_offset_op(new_offset);
  }

static operand operand_from_int_const(type_node *result_type, int the_const)
  {
    in_ldc *new_ldc = new in_ldc(result_type, operand(), immed(the_const));
    return operand(new_ldc);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
