#include <stdio.h>
#include "iob.h"
/*
 * Changed by CSW for SUIF:
 *     The original code for nice_printf() and some related printing
 *     routines needed varargs functionality, but it got it by
 *     assuming every parameter is an int and just passing a whole
 *     bunch of int parameters around.  This breaks on the DEC Alpha,
 *     a 64-bit machine.  So I've changed this to use the ANSI C way
 *     of handling varargs.  That means we need prototypes included
 *     whenever one of the functions in question is handled, so
 *     "output.h" needs to be included.
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes: */
#include "output.h"
/* END SUIF changes */
#include <string.h>

#define MEMBSIZE	32000
#define GMEMBSIZE	16000

 extern char *Malloc();

 char *
gmem(n)
 int n;
{
	static char *last, *next;
	char *rv;
	rv = next;
	if ((next += n) > last) {
		rv = Malloc(n + GMEMBSIZE);
		
		next = rv + n;
		last = next + GMEMBSIZE;
		}
	return rv;
	}

 struct memblock {
	struct memblock *next;
	char buf[MEMBSIZE];
	};
 typedef struct memblock memblock;

 static memblock mem0;
 memblock *curmemblock = &mem0, *firstmemblock = &mem0;
	
 char	*mem_first = mem0.buf,
	*mem_next  = mem0.buf,
	*mem_last  = mem0.buf + sizeof(mem0.buf),
	*mem0_last = mem0.buf + sizeof(mem0.buf);

 char *
mem(n, round)
 int n;
{
	memblock *b;
	register char *rv, *s;

	if (round)
		mem_next =
#ifdef CRAY
		    (long)mem_next & 0xe000000000000000
			? (char *)(((long)mem_next & 0x1fffffffffffffff) + 1)
			: mem_next;
#else
			 (char *)(((long)mem_next + sizeof(char *)-1)
				& ~(sizeof(char *)-1));
#endif
	rv = mem_next;
	s = rv + n;
	if (s >= mem_last) {
		if (n > sizeof(mem0.buf))  {
			fprintf(stderr, "mem(%d) failure!\n", n);
			exit(1);
			}
		if (!(b = curmemblock->next)) {
			b = (memblock *)Malloc(sizeof(memblock));
			curmemblock->next = b;
			b->next = 0;
			}
		curmemblock = b;
		rv = b->buf;
		mem_last = rv + sizeof(b->buf);
		s = rv + n;
		}
	mem_next = s;
	return rv;
	}

 char *
tostring(s,n)
 register char *s;
 register int n;
{
	register char *s1;
	char *rv;
	register int k = n + 2, L;
	for(L = 0; L < n; L++)
		if (s[L] == '"')
			k++; 
	rv = s1 = mem(k,0);
	*s1++ = '"';
	for(L = 0; L < n; L++) {
		if (s[L] == '"')
			*s1++ = '\\';
		*s1++ = s[L];
		}
	*s1 = 0;
	return rv;
	}

 char *
cpstring(s)
 register char *s;
{
	return strcpy(mem(strlen(s)+1,0), s);
	}

 void
new_iob_data(ios, name)
 register io_setup *ios;
 char *name;
{
	register iob_data *iod;
	register char **s, **se;

	iod = (iob_data *)
		mem(sizeof(iob_data) + ios->nelt*sizeof(char *), 1);
	iod->next = iob_list;
	iob_list = iod;
	iod->type = ios->fields[0];
	iod->name = cpstring(name);
	s = iod->fields;
	se = s + ios->nelt;
	while(s < se)
		*s++ = "0";
	*s = 0;
	}

 char *
string_num(pfx, n)
 char *pfx;
 long n;
{
	char buf[32];
	sprintf(buf, "%s%ld", pfx, n);
	/* can't trust return type of sprintf -- BSD gets it wrong */
	return strcpy(mem(strlen(buf)+1,0), buf);
	}

static defines *define_list;

 void
define_start(outfile, s1, s2, post)
 FILE *outfile;
 char *s1, *s2, *post;
{
	defines *d;
	int n, n1;

	n = n1 = strlen(s1);
	if (s2)
		n += strlen(s2);
	d = (defines *)mem(sizeof(defines)+n, 1);
	d->next = define_list;
	define_list = d;
	strcpy(d->defname, s1);
	if (s2)
		strcpy(d->defname + n1, s2);
	nice_printf(outfile, "#define %s %s", d->defname, post);
	}

 void
other_undefs(outfile)
 FILE *outfile;
{
	defines *d;
	if (d = define_list) {
		define_list = 0;
		nice_printf(outfile, "\n");
		do
			nice_printf(outfile, "#undef %s\n", d->defname);
			while(d = d->next);
		nice_printf(outfile, "\n");
		}
	}
