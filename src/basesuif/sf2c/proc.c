#include "defs.h"
#include "names.h"
#include "output.h"
#include "p1defs.h"

#define ESNULL (struct Extsym *)0
#define EXNULL (union Expression *)0

LOCAL dobss(), docomleng(), docommon(), doentry(),
	epicode(), nextarg(), retval();

static char Blank[] = BLANKCOMMON;

 static char *postfix[] = { "h", "i", "r", "d", "c", "z", "i" };

 void
unamstring(q, s)
 register Addrp q;
 register char *s;
{
	register int k;
	register char *t;

	k = strlen(s);
	if (k < IDENT_LEN) {
		q->uname_tag = UNAM_IDENT;
		t = q->user.ident;
		}
	else {
		q->uname_tag = UNAM_CHARP;
		q->user.Charp = t = mem(k+1, 0);
		}
	strcpy(t, s);
	}

 static void
fix_entry_returns()	/* for multiple entry points */
{
	Addrp a;
	int i;
	struct Entrypoint *e;
	Namep np;

	e = entries = (struct Entrypoint *)revchain((chainp)entries);
	allargs = revchain(allargs);
	if (!multitype)
		return;

	/* TYLOGICAL should have been turned into TYLONG or TYSHORT by now */

	for(i = TYSHORT; i <= TYLOGICAL; i++)
		if (a = retslotx[i])
			sprintf(a->user.ident, "(*ret_val).%s",
				postfix[i-TYSHORT]);

	do {
		np = e->enamep;
		switch(np->vtype) {
			case TYSHORT:
			case TYLONG:
			case TYREAL:
			case TYDREAL:
			case TYCOMPLEX:
			case TYDCOMPLEX:
			case TYLOGICAL:
				np->vstg = STGARG;
			}
		}
		while(e = e->entnextp);
	}

 static void
putentries(outfile)	/* put out wrappers for multiple entries */
 FILE *outfile;
{
	char base[IDENT_LEN];
	struct Entrypoint *e;
	Namep *A, *Ae, *Ae1, **Alp, *a, **a1, np;
	chainp args, lengths, list_args();
	void list_arg_types();
	int i, k, mt, nL, type;
	extern char *dfltarg[], *dfltproc[];

	nL = (nallargs + nallchargs) * sizeof(Namep *);
	A = (Namep *)ckalloc(nL + nallargs*sizeof(Namep **));
	Ae = A + nallargs;
	Alp = (Namep **)(Ae1 = Ae + nallchargs);
	i = k = 0;
	for(a1 = Alp, args = allargs; args; a1++, args = args->nextp) {
		np = (Namep)args->datap;
		if (np->vtype == TYCHAR && np->vclass != CLPROC)
			*a1 = &Ae[i++];
		}

	e = entries;
	mt = multitype;
	multitype = 0;
	sprintf(base, "%s0_", e->enamep->cvarname);
	do {
		np = e->enamep;
		proctype = type = np->vtype;
		nice_printf(outfile, "\n%s ", c_type_decl(type, 1));
		nice_printf(outfile, "%s", np->cvarname);
		if (Ansi)
			lengths = list_args((FILEP)0, e, 0);
		else {
			lengths = list_args(outfile, e, 0);
			nice_printf(outfile, "\n");
			}
	    	list_arg_types(outfile, e, lengths, 0);
		nice_printf(outfile, "{\n");
		next_tab(outfile);
		if (mt)
			nice_printf(outfile,
				"Multitype ret_val;\n%s(%d, &ret_val",
				base, k); /*)*/
		else if (ISCOMPLEX(type))
			nice_printf(outfile, "%s(%d,%s", base, k,
				retslotx[type]->user.ident); /*)*/
		else if (type == TYCHAR)
			nice_printf(outfile,
				"%s(%d, ret_val, ret_val_len", base, k); /*)*/
		else
			nice_printf(outfile, "return %s(%d", base, k); /*)*/
		k++;
		memset((char *)A, 0, nL);
		for(args = e->arglist; args; args = args->nextp) {
			np = (Namep)args->datap;
			A[np->argno] = np;
			if (np->vtype == TYCHAR && np->vclass != CLPROC)
				*Alp[np->argno] = np;
			}
		args = allargs;
		for(a = A; a < Ae; a++, args = args->nextp)
			nice_printf(outfile, ", %s", (np = *a)
				? np->cvarname
				: ((Namep)args->datap)->vclass == CLPROC
				? dfltproc[((Namep)args->datap)->vtype]
				: dfltarg[((Namep)args->datap)->vtype]);
		for(; a < Ae1; a++)
			if (np = *a)
				nice_printf(outfile, ", %s_len", np->cvarname);
			else
				nice_printf(outfile, ", (ftnint)0");
		nice_printf(outfile, /*(*/ ");\n");
		if (mt) {
			if (type == TYCOMPLEX)
				nice_printf(outfile,
		    "r_v->_r = ret_val.c._r; r_v->_i = ret_val.c._i;\nreturn 0;\n");
			else if (type == TYDCOMPLEX)
				nice_printf(outfile,
		    "r_v->_r = ret_val.z._r; r_v->_i = ret_val.z._i;\nreturn 0;\n");
			else nice_printf(outfile, "return ret_val.%s;\n",
				postfix[type-TYSHORT]);
			}
		else if (ONEOF(type, M(TYCHAR)|M(TYCOMPLEX)|M(TYDCOMPLEX)))
			nice_printf(outfile, "return 0;\n");
		nice_printf(outfile, "}\n");
		prev_tab(outfile);
		}
		while(e = e->entnextp);
	free((char *)A);
	}

 void
entry_goto(outfile)
 FILEP outfile;
{
	struct Entrypoint *e = entries;
	int k = 0;

	nice_printf(outfile, "switch(n__) {\n");
	next_tab(outfile);
	while(e = e->entnextp)
		nice_printf(outfile, "case %d: goto %s;\n", ++k,
			user_label((long)(extsymtab - e->entryname - 1)));
	nice_printf(outfile, "}\n\n");
	prev_tab(outfile);
	}

/* start a new procedure */

newproc()
{
	if(parstate != OUTSIDE)
	{
		execerr("missing end statement", CNULL);
		endproc();
	}

	parstate = INSIDE;
	procclass = CLMAIN;	/* default */
}



/* end of procedure. generate variables, epilogs, and prologs */

endproc()
{
	struct Labelblock *lp;

	if(parstate < INDATA)
		enddcl();
	if(ctlstack >= ctls)
		err("DO loop or BLOCK IF not closed");
	for(lp = labeltab ; lp < labtabend ; ++lp)
		if(lp->stateno!=0 && lp->labdefined==NO)
			errstr("missing statement label %s",
				convic(lp->stateno) );

/* Save copies of the common variables in extptr -> allextp */

	{ struct Extsym *ext;

	    for (ext = extsymtab; ext < nextext; ext++)
		if (ext -> extstg == STGCOMMON && ext -> extp) {
		    extern int usedefsforcommon;

/* Put the pointer to these variables in the init field if this is a
   BLOCK DATA entry */
	    if (ext -> extinit == (chainp) YES)
		ext -> extinit = ext -> extp;

/* Write out the abbreviations for common block reference */

		    if (usedefsforcommon)
			write_abbrevs (c_file, 1, ext -> extp);
		    copy_data (ext -> extp);
		    ext -> extp = CHNULL;
		    ext -> used_here = 1;

		} /* if */
	}

	if (nentry > 1)
		fix_entry_returns();
	epicode();
	donmlist();
	dobss();
	start_formatting ();
	fixlwm();
	if (nentry > 1)
		putentries(c_file);

	procinit();	/* clean up for next procedure */
}



/* End of declaration section of procedure.  Allocate storage. */

enddcl()
{
	register struct Entrypoint *ep;
	struct Entrypoint *ep0;

	parstate = INEXEC;
	docommon();

/* Now the hash table entries for fields of common blocks have STGCOMMON,
   vdcldone, voffset, and varno.  And the common blocks themselves have
   their full sizes in extleng. */

	doequiv();
	docomleng();

/* This implies that entry points in the declarations are buffered in
   entries   but not written out */

	if (ep = ep0 = (struct Entrypoint *)revchain((chainp)entries)) {
		/* entries could be 0 in case of an error */
		do doentry(ep);
			while(ep = ep->entnextp);
		entries = (struct Entrypoint *)revchain((chainp)ep0);
		}
}

/* ROUTINES CALLED WHEN ENCOUNTERING ENTRY POINTS */

/* Main program or Block data */

startproc(progname, class)
struct Extsym * progname;
int class;
{
	register struct Entrypoint *p;

	p = ALLOC(Entrypoint);
	if(class == CLMAIN) {
		puthead(CNULL, CLMAIN);
		if (progname)
		    strcpy (main_alias, progname->cextname);
	} else
		puthead(CNULL, CLBLOCK);
	if(class == CLMAIN)
		newentry( mkname("MAIN_"), 0 );
	p->entryname = progname;
	p->entrylabel = newlabel();
	entries = p;

	procclass = class;
	retlabel = newlabel();
/*
 *  Changed by CSW for SUIF:
 *      We want a switch to turn off diagnostic output.
 */
/* BEFORE SUIF changes: */
#if 0
	fprintf(diagfile, "   %s", (class==CLMAIN ? "MAIN" : "BLOCK DATA") );
	if(progname) {
		fprintf(diagfile, " %s", progname->fextname);
		procname = progname->cextname;
		}
	fprintf(diagfile, ":\n");
	fflush(diagfile);
#endif
/* AFTER SUIF changes: */
	if (!quiet_flag)
	{
		fprintf(diagfile, "   %s",
			(class==CLMAIN ? "MAIN" : "BLOCK DATA") );
		if(progname) {
			fprintf(diagfile, " %s", progname->fextname);
			procname = progname->cextname;
			}
		fprintf(diagfile, ":\n");
		fflush(diagfile);
	}
/* END SUIF changes */
}

/* subroutine or function statement */

struct Extsym *newentry(v, substmsg)
 register Namep v;
 int substmsg;
{
	register struct Extsym *p;
	char buf[128], badname[64];
	static int nbad;
	static char already[] = "external name already used";

	p = mkext(v->fvarname, addunder(v->cvarname));

	if(p->extinit || ! ONEOF(p->extstg, M(STGUNKNOWN)|M(STGEXT)) )
	{
		sprintf(badname, "%s_bad%d", v->fvarname, ++nbad);
		if (substmsg) {
			sprintf(buf,"%s\n\tsubstituting \"%s\"",
				already, badname);
			dclerr(buf, v);
			}
		else
			dclerr(already, v);
		p = mkext(v->fvarname, badname);
	}
	v->vstg = STGAUTO;
	v->vprocclass = PTHISPROC;
	v->vclass = CLPROC;
	p->extstg = STGEXT;
	p->extinit = (chainp) YES;
	return(p);
}


entrypt(class, type, length, entry, args)
int class, type;
ftnint length;
struct Extsym *entry;
chainp args;
{
	register Namep q;
	register struct Entrypoint *p;
	extern int types3[];

	if(class != CLENTRY)
		puthead( procname = entry->cextname, class);
/*
 *  Changed by CSW for SUIF:
 *      We want a switch to turn off diagnostic output.
 */
/* BEFORE SUIF changes: */
#if 0
	if(class == CLENTRY)
		fprintf(diagfile, "       entry ");
	fprintf(diagfile, "   %s:\n", entry->fextname);
	fflush(diagfile);
#endif
/* AFTER SUIF changes: */
	if (!quiet_flag)
	{
		if(class == CLENTRY)
			fprintf(diagfile, "       entry ");
		fprintf(diagfile, "   %s:\n", entry->fextname);
		fflush(diagfile);
	}
/* END SUIF changes */
	q = mkname(entry->fextname);

	if( (type = lengtype(type, (int) length)) != TYCHAR)
		length = 0;
	if(class == CLPROC)
	{
		procclass = CLPROC;
		proctype = type;
		procleng = length;

		retlabel = newlabel();
		if(type == TYSUBR)
			ret0label = newlabel();
	}

	p = ALLOC(Entrypoint);

	p->entnextp = entries;
	entries = p;

	p->entryname = entry;
	p->arglist = revchain(args);
	p->entrylabel = newlabel();
	p->enamep = q;

	if(class == CLENTRY)
	{
		class = CLPROC;
		if(proctype == TYSUBR)
			type = TYSUBR;
	}

	q->vclass = class;
	q->vprocclass = PTHISPROC;
	settype(q, type, (int) length);
	/* hold all initial entry points till end of declarations */
	if(parstate >= INDATA)
		doentry(p);
}

/* generate epilogs */

/* epicode -- write out the proper function return mechanism at the end of
   the procedure declaration.  Handles multiple return value types, as
   well as cooercion into the proper value */

LOCAL epicode()
{
	extern int lastwasbranch;

	if(procclass==CLPROC)
	{
		if(proctype==TYSUBR)
		{

/* Return a zero only when the alternate return mechanism has been
   specified in the function header */

			if (substars && lastwasbranch == NO)
			    p1put_subr_ret (ENULL);	
		}
		else if (!multitype && lastwasbranch == NO)
			retval(proctype);
	}
	lastwasbranch = NO;
}


/* generate code to return value of type  t */

LOCAL retval(t)
register int t;
{
	register Addrp p;

	switch(t)
	{
	case TYCHAR:
	case TYCOMPLEX:
	case TYDCOMPLEX:
		break;

	case TYLOGICAL:
		t = tylogical;
	case TYADDR:
	case TYSHORT:
	case TYLONG:
	case TYREAL:
	case TYDREAL:
		p = (Addrp) cpexpr(retslot);
		p->vtype = t;
		p1put_subr_ret (mkconv (t, fixtype (p)));
		break;

	default:
		badtype("retval", t);
	}
}


/* Allocate extra argument array if needed. Generate prologs. */

procode(outfile)
FILE *outfile;
{
	register struct Entrypoint *p;
	for(p = entries ; p ; p = p->entnextp)
		prolog(outfile, p);

}

/*
   manipulate argument lists (allocate argument slot positions)
 * keep track of return types and labels
 */

LOCAL doentry(ep)
struct Entrypoint *ep;
{
	register int type;
	register Namep np;
	chainp p;
	register Namep q;
	Addrp mkarg(), rs;

	if (++nentry > 1)
		p1put_label((long)(extsymtab - ep->entryname - 1));

/* The main program isn't allowed to have parameters, so any given
   parameters are ignored */

	if(procclass == CLMAIN || procclass == CLBLOCK)
		return;

/* So now we're working with something other than CLMAIN or CLBLOCK.
   Determine the type of its return value. */

	impldcl( np = mkname(ep->entryname->fextname) );
	type = np->vtype;
	if(proctype == TYUNKNOWN)
		if( (proctype = type) == TYCHAR)
			procleng = (np->vleng ? np->vleng->constblock.Const.ci : (ftnint) (-1));

	if(proctype == TYCHAR)
	{
		if(type != TYCHAR)
			err("noncharacter entry of character function");

/* Functions returning type   char   can only have multiple entries if all
   entries return the same length */

		else if( (np->vleng ? np->vleng->constblock.Const.ci :
		    (ftnint) (-1)) != procleng)
			err("mismatched character entry lengths");
	}
	else if(type == TYCHAR)
		err("character entry of noncharacter function");
	else if(type != proctype)
		multitype = YES;
	if(rtvlabel[type] == 0)
		rtvlabel[type] = newlabel();
	ep->typelabel = rtvlabel[type];

	if(type == TYCHAR)
	{
		if(chslot < 0)
		{
			chslot = nextarg(TYADDR);
			chlgslot = nextarg(TYLENG);
		}
		np->vstg = STGARG;

/* Put a new argument in the function, one which will hold the result of
   a character function.  This will have to be named sometime, probably in
   mkarg(). */

		if(procleng < 0) {
			np->vleng = (expptr) mkarg(TYLENG, chlgslot);
			np->vleng->addrblock.uname_tag = UNAM_IDENT;
			strcpy (np -> vleng -> addrblock.user.ident,
				new_func_length());
			}
		if (!retslotx[TYCHAR]) {
			retslotx[TYCHAR] = rs =
				autovar(0, type, ISCONST(np->vleng)
					? np->vleng : ICON(0), "");
			strcpy(rs->user.ident, "ret_val");
			}
	}

/* Handle a   complex   return type -- declare a new parameter (pointer to
   a complex value) */

	else if( ISCOMPLEX(type) ) {
		if (!retslotx[type])
			retslotx[type] =
				autovar(0, type, EXNULL, " ret_val");
				/* the blank is for use in output_addr */
		np->vstg = STGARG;
		if(cxslot < 0)
			cxslot = nextarg(TYADDR);
		}
	else if (type != TYSUBR) {
		if (!retslotx[type])
			retslotx[type] = retslot =
				autovar(1, type, EXNULL, " ret_val");
				/* the blank is for use in output_addr */
		np->vstg = STGAUTO;
		}

	for(p = ep->arglist ; p ; p = p->nextp)
		if(! (( q = (Namep) (p->datap) )->vknownarg) ) {
			q->vknownarg = 1;
			q->vardesc.varno = nextarg(TYADDR);
			allargs = mkchain((char *)q, allargs);
			q->argno = nallargs++;
			}

	for(p = ep->arglist ; p ; p = p->nextp)
		if(! (( q = (Namep) (p->datap) )->vdcldone) )
		{
			impldcl(q);
			q->vdcldone = YES;
			if(q->vtype == TYCHAR)
			{

/* If we don't know the length of a char*(*) (i.e. a string), we must add
   in this additional length argument. */

				++nallchargs;
				if (q->vclass == CLPROC)
					nallchargs--;
				else if (q->vleng == NULL) {
					/* character*(*) */
					q->vleng = (expptr)
					    mkarg(TYLENG, nextarg(TYLENG) );
					unamstring(q->vleng,
						new_arg_length(q));
				} /* if q -> vleng == NULL */

/* Only generate a dummy argument when processing the first entry - why
   not for all of them? mwm July-12 */

				else if(nentry == 1)
					nextarg(TYLENG);
			}
		}
}



LOCAL nextarg(type)
int type;
{
	int k;
	k = lastargslot;
	lastargslot += typesize[type];
	return(k);
}


LOCAL dobss()
{
	register struct Hashentry *p;
	register Namep q;
	int qstg, qclass, qtype;

	for(p = hashtab ; p<lasthash ; ++p)
		if(q = p->varp)
		{
			qstg = q->vstg;
			qtype = q->vtype;
			qclass = q->vclass;

			if( (qclass==CLUNKNOWN && qstg!=STGARG) ||
			    (qclass==CLVAR && qstg==STGUNKNOWN) ) {
				if (!(q->vis_assigned | q->vimpldovar))
					warn1("local variable %s never used",
						q->fvarname);
				}
			else if(qclass==CLVAR && qstg==STGBSS)
			{ ; }

/* Give external procedures the proper storage class */

			else if(qclass==CLPROC && q->vprocclass==PEXTERNAL && qstg!=STGARG)
				mkext(q->fvarname,addunder(q->cvarname))->extstg
					= STGEXT;

			if(qclass==CLVAR) {
			    if (qstg!=STGARG) {
/* DEM no longer needed */
/*
				if(q->vdim && !ISICON(q->vdim->nelt) )
					dclerr("adjustable dimension on non-argument", q);
*/
				if(qtype==TYCHAR && (q->vleng==NULL || !ISICON(q->vleng)))
					dclerr("adjustable leng on nonargument", q);
			    } /* if qstg != STGARG */
			} /* if qclass == CLVAR */
		}

}



donmlist()
{
	register struct Hashentry *p;
	register Namep q;

	for(p=hashtab; p<lasthash; ++p)
		if( (q = p->varp) && q->vclass==CLNAMELIST)
			namelist(q);
}


/* iarrlen -- Returns the size of the array in bytes, or -1 */

ftnint iarrlen(q)
register Namep q;
{
	ftnint leng;

	leng = typesize[q->vtype];
	if(leng <= 0)
		return(-1);
	if(q->vdim)
		if( ISICON(q->vdim->nelt) )
			leng *= q->vdim->nelt->constblock.Const.ci;
/* DEM deal with semiconstants */
		else if(q->vdim->dims[0].dimsize->addrblock.isaconst) {
			int iter;
			for (iter=0; iter < q->vdim->ndim; iter++) {
			   if(q->vdim->dims[iter].dimsize->addrblock.isaconst) 
				leng *= 
				q->vdim->dims[iter].dimsize->addrblock.kludgeci;
			   else return(-1);
			}
		} else	return(-1);
	if(q->vleng)
		if( ISICON(q->vleng) )
			leng *= q->vleng->constblock.Const.ci;
		else return(-1);
	return(leng);
}

namelist(np)
Namep np;
{
	register chainp q;
	register Namep v;
	int y;

	if (!np->visused)
		return;
	y = 0;

	for(q = np->varxptr.namelist ; q ; q = q->nextp)
	{
		vardcl( v = (Namep) (q->datap) );
		if( !ONEOF(v->vstg, MSKSTATIC) )
			dclerr("may not appear in namelist", v);
		else {
			v->vnamelist = 1;
/*
 * Changed by CSW for SUIF:
 *     There was a bug in the original f2c code here: if a variable
 *     was in a namelist but useauto was also set, the variable would
 *     be declared auto but its address would also be used as an
 *     initializer for a static variable, the namelist.  So we fix
 *     this by explicitly setting the vsave flag here, which says that
 *     this variable should be declared static.
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes */
			v->vsave = 1;
/* END SUIF changes */
			v->visused = 1;
			y = 1;
			}
	np->visused = y;
	}
}

/* docommon -- called at the end of procedure declarations, before
   equivalences and the procedure body */

LOCAL docommon()
{
    register struct Extsym *extptr;
    register chainp q, q1;
    struct Dimblock *t;
    expptr neltp;
    register Namep comvar;
    ftnint size;
    int i, k, type;
/*DEM local vars */ int iter;

    for(extptr = extsymtab ; extptr<nextext ; ++extptr)
	if (extptr->extstg == STGCOMMON && (q = extptr->extp)) {

/* If a common declaration also had a list of variables ... */

	    q = extptr->extp = revchain(q);
	    for(k = 1; q ; q = q->nextp)
	    {
		comvar = (Namep) (q->datap);

		if(comvar->vdcldone == NO)
		    vardcl(comvar);
		type = comvar->vtype;
		if (type != TYCHAR)
			k = 0;
		if(extptr->extleng % typealign[type] != 0) {
/*
 * Changed by CSW for SUIF:
 *     We want warnings to use the warning function, not the error
 *     function.  If the program succeeds, it's a warning, not an
 *     error.  This both allows us to turn off the warning output with
 *     a command line flag and reduces the confusion of the user by
 *     using the word ``warning'' instead of ``error''.
 */
/* BEFORE SUIF changes: */
#if 0
		    dclerr("common alignment", comvar);
		    --nerr; /* don't give bad return code for this */
#endif
/* AFTER SUIF changes: */
		    warn1("Declaration error for %s: common alignment",
			  comvar->fvarname);
/* END SUIF changes */
#if 0
		    extptr->extleng = roundup(extptr->extleng, typealign[type]);
#endif
		} /* if extptr -> extleng % */

/* Set the offset into the common block */

		comvar->voffset = extptr->extleng;
		comvar->vardesc.varno = extptr - extsymtab;
		if(type == TYCHAR)
		    size = comvar->vleng->constblock.Const.ci;
		else
		    size = typesize[type];
		if(t = comvar->vdim) {
/* DEM: constant is now in kludgecis */
/*		    if( (neltp = t->nelt) && ISCONST(neltp) )
			size *= neltp->constblock.Const.ci;
*/
		    for (iter=0; iter < t->ndim; iter++) {	
			if (t->dims[iter].dimsize->addrblock.isaconst) {
			  size *= t->dims[iter].dimsize->addrblock.kludgeci;
			} else
				dclerr("adjustable array in common", comvar);
		    }
		}

/* Adjust the length of the common block so far */

		extptr->extleng += size;
	    } /* for */

	    extptr->exchar = k;

/* Determine curno and, if new, save this identifier chain */

	    q1 = extptr->extp;
	    for (q = extptr->allextp, i = 0; q; i++, q = q->nextp)
		if (struct_eq((chainp)q->datap, q1))
			break;
/* DEM allow one common to be subset of the other */
		else  {
		    int sub;
		    sub = struct_sub((chainp)q->datap,q1);
		    if (sub == 1) {
			q->datap = (char *)q1;
			break;
		    } else if (sub == 2) {
			break;
		    } 
		}
			
	    if (q)
		extptr->curno = extptr->maxno - i;
	    else {
		extptr->curno = ++extptr->maxno;
		extptr->allextp = mkchain((char *)extptr->extp,
						extptr->allextp);
		}
	} /* if extptr -> extstg == STGCOMMON */

/* Now the hash table entries have STGCOMMON, vdcldone, voffset, and
   varno.  And the common block itself has its full size in extleng. */

} /* docommon */


/* copy_data -- copy the Namep entries so they are available even after
   the hash table is empty */

copy_data (list)
chainp list;
{
    for (; list; list = list -> nextp) {
	Namep namep = ALLOC (Nameblock);
	int size, nd, i;
	struct Dimblock *dp;

	cpn(sizeof(struct Nameblock), list->datap, (char *)namep);
	namep->fvarname = strcpy(gmem(strlen(namep->fvarname)+1),
		namep->fvarname);
	namep->cvarname = strcmp(namep->fvarname, namep->cvarname)
		? strcpy(gmem(strlen(namep->cvarname)+1), namep->cvarname)
		: namep->fvarname;
	if (namep -> vleng)
	    namep -> vleng = (expptr) cpexpr (namep -> vleng);
	if (namep -> vdim) {
	    nd = namep -> vdim -> ndim;
/*
 * Changed by CSW for SUIF:
 *     We added fields to struct Dimblock, so the expression here to
 *     compute the space to allocate for it must change.
 */
/* BEFORE SUIF changes: */
#if 0
	    size = sizeof(int) + (3 + 2 * nd) * sizeof (expptr);
#endif
/* AFTER SUIF changes: */
	    size = sizeof(int) + (4 + 5 * nd) * sizeof (expptr);
/* END SUIF changes */
	    dp = (struct Dimblock *) ckalloc (size);
	    cpn(size, (char *)namep->vdim, (char *)dp);
	    namep -> vdim = dp;
	    dp->nelt = (expptr)cpexpr(dp->nelt);
	    for (i = 0; i < nd; i++) {
		dp -> dims[i].dimsize = (expptr) cpexpr (dp -> dims[i].dimsize);
	    } /* for */
	} /* if */
	list -> datap = (char *) namep;
    } /* for */
} /* copy_data */



LOCAL docomleng()
{
	register struct Extsym *p;

	for(p = extsymtab ; p < nextext ; ++p)
		if(p->extstg == STGCOMMON)
		{
			if(p->maxleng!=0 && p->extleng!=0 && p->maxleng!=p->extleng
			    && strcmp(Blank, p->cextname) )
				warn1("incompatible lengths for common block %s",
				    p->fextname);
			if(p->maxleng < p->extleng)
				p->maxleng = p->extleng;
			p->extleng = 0;
		}
}




/* ROUTINES DEALING WITH AUTOMATIC AND TEMPORARY STORAGE */

frtemp(p)
Addrp p;
{
	/* put block on chain of temps to be reclaimed */
	static chainp zork;
	holdtemps = mkchain((char *)p, holdtemps);
	if (zork == holdtemps)
		printf("");
}

 void
freetemps()
{
	register chainp p, p1;
	register Addrp q;
	register int t;

	p1 = holdtemps;
	while(p = p1) {
		q = (Addrp)p->datap;
		t = q->vtype;
		if (t == TYCHAR && q->varleng != 0) {
			/* restore clobbered character string lengths */
			frexpr(q->vleng);
			q->vleng = ICON(q->varleng);
			}
		p1 = p->nextp;
		p->nextp = templist[t];
		templist[t] = p;
		}
	holdtemps = 0;
	}

/* allocate an automatic variable slot for each of   nelt   variables */

Addrp autovar(nelt0, t, lengp, name)
register int nelt0, t;
expptr lengp;
char *name;
{
	ftnint leng;
	register Addrp q;
	char *temp_name ();
	register int nelt = nelt0 > 0 ? nelt0 : 1;
	extern char *av_pfix[];

	if(t == TYCHAR)
		if( ISICON(lengp) )
			leng = lengp->constblock.Const.ci;
		else	{
			Fatal("automatic variable of nonconstant length");
		}
	else
		leng = typesize[t];

	q = ALLOC(Addrblock);
	q->tag = TADDR;
	q->vtype = t;
	if(t == TYCHAR)
	{
		q->vleng = ICON(leng);
		q->varleng = leng;
	}
	q->vstg = STGAUTO;
	q->ntempelt = nelt;
	q->isarray = (nelt > 1);
	q->memoffset = ICON(0);

	/* kludge for nls so we can have ret_val rather than ret_val_4 */
	if (*name == ' ')
		unamstring(q, name+1);
	else {
		q->uname_tag = UNAM_IDENT;
		temp_name(av_pfix[t], ++autonum[t], q->user.ident);
		}
	if (nelt0 > 0)
		declare_new_addr (q);
	return(q);
}


/* Returns a temporary of the appropriate type.  Will reuse existing
   temporaries when possible */

Addrp mktmpn(nelt, type, lengp)
int nelt;
register int type;
expptr lengp;
{
	ftnint leng;
	chainp p, oldp;
	register Addrp q;

	if(type==TYUNKNOWN || type==TYERROR)
		badtype("mktmpn", type);

	if(type==TYCHAR)
		if( ISICON(lengp) )
			leng = lengp->constblock.Const.ci;
		else	{
			err("adjustable length");
			return( (Addrp) errnode() );
		}
	else if (type > TYCHAR || type < TYADDR) {
		erri("mktmpn: unexpected type %d", type);
		exit(1);
		}
/*
 * Changed by CSW for SUIF:
 *     For SUIF we don't ever want temporaries reused -- that would
 *     just needlessly complicate later analysis.  If live ranges
 *     don't overlap, we can figure out whether to put them in the
 *     same registers or memory locations in SUIF.
 */
/* BEFORE SUIF changes: */
#if 0
/*
 * if a temporary of appropriate shape is on the templist,
 * remove it from the list and return it
 */
	for(oldp=CHNULL, p=templist[type];  p  ;  oldp=p, p=p->nextp)
	{
		q = (Addrp) (p->datap);
		if(q->ntempelt==nelt &&
		    (type!=TYCHAR || q->vleng->constblock.Const.ci==leng) )
		{
			if(oldp)
				oldp->nextp = p->nextp;
			else
				templist[type] = p->nextp;
			free( (charptr) p);
			return(q);
		}
	}
#endif
/* AFTER SUIF changes: */
/* END SUIF changes */
	q = autovar(nelt, type, lengp, "");
	return(q);
}




/* mktemp -- create new local variable; call it something like   name   
   lengp   is taken directly, not copied */

Addrp Mktemp(type, lengp)
int type;
expptr lengp;
{
	Addrp rv;
	/* arrange for temporaries to be recycled */
	/* at the end of this statement... */
	frtemp((Addrp)cpexpr(rv = mktmpn(1,type,lengp)));
	return rv;
}

/* mktemp0 omits frtemp() */
Addrp mktemp0(type, lengp)
int type;
expptr lengp;
{
	Addrp rv;
	/* arrange for temporaries to be recycled */
	/* when this Addrp is freed */
	rv = mktmpn(1,type,lengp);
	rv->istemp = YES;
	return rv;
}

/* VARIOUS ROUTINES FOR PROCESSING DECLARATIONS */

/* comblock -- Declare a new common block.  Input parameters name the block;
   s   will be NULL if the block is unnamed */

struct Extsym *comblock(s)
 register char *s;
{
	struct Extsym *p;
	register char *t;
	register int c;
	char cbuf[256], *s0;

/* Give the unnamed common block a unique name */

	if(*s == 0)
		p = mkext(Blank,Blank);
	else {
		s0 = s;
		t = cbuf;
		while(c = *t++ = *s++)
			if (c == '_')
				*t++ = '_';
		*t-- = 0;
		*t = '_';
		p = mkext(s0,cbuf);
		}
	if(p->extstg == STGUNKNOWN)
		p->extstg = STGCOMMON;
	else if(p->extstg != STGCOMMON)
	{
		errstr("%s cannot be a common block name", s);
		return(0);
	}

	return( p );
}


/* incomm -- add a new variable to a common declaration */

incomm(c, v)
struct Extsym *c;
Namep v;
{
	if(v->vstg != STGUNKNOWN && !v->vimplstg)
		dclerr("incompatible common declaration", v);
	else
	{
		v->vstg = STGCOMMON;
		c->extp = mkchain((char *)v, c->extp);
	}
}




/* settype -- set the type or storage class of a Namep object.  If
   v -> vstg == STGUNKNOWN && type < 0,   attempt to reset vstg to be
   -type.  This function will not change any earlier definitions in   v,
   in will only attempt to fill out more information give the other params */

settype(v, type, length)
register Namep  v;
register int type;
register int length;
{
	if(type == TYUNKNOWN)
		return;

	if(type==TYSUBR && v->vtype!=TYUNKNOWN && v->vstg==STGARG)
	{
		v->vtype = TYSUBR;
		frexpr(v->vleng);
		v->vleng = 0;
	}
	else if(type < 0)	/* storage class set */
	{
		if(v->vstg == STGUNKNOWN)
			v->vstg = - type;
		else if(v->vstg != -type)
			dclerr("incompatible storage declarations", v);
	}
	else if(v->vtype == TYUNKNOWN)
	{
		if( (v->vtype = lengtype(type, length))==TYCHAR && length>=0)
			v->vleng = ICON(length);
	}
	else if(v->vtype!=type || (type==TYCHAR && v->vleng->constblock.Const.ci!=length) )
		dclerr("incompatible type declarations", v);
}





/* lengtype -- returns the proper compiler type, given input of Fortran
   type and length specifier */

lengtype(type, length)
register int type;
register int length;
{
	switch(type)
	{
	case TYREAL:
		if(length == typesize[TYDREAL])
			return(TYDREAL);
		if(length == typesize[TYREAL])
			goto ret;
		break;

	case TYCOMPLEX:
		if(length == typesize[TYDCOMPLEX])
			return(TYDCOMPLEX);
		if(length == typesize[TYCOMPLEX])
			goto ret;
		break;

	case TYSHORT:
	case TYDREAL:
	case TYDCOMPLEX:
	case TYCHAR:
	case TYUNKNOWN:
	case TYSUBR:
	case TYERROR:
		goto ret;

	case TYLOGICAL:
		if(length == typesize[TYLOGICAL])
			goto ret;
		if(length == 1) {
/*
 * Changed by CSW for SUIF:
 *     We want warnings to use the warning function, not the error
 *     function.  If the program succeeds, it's a warning, not an
 *     error.  This both allows us to turn off the warning output with
 *     a command line flag and reduces the confusion of the user by
 *     using the word ``warning'' instead of ``error''.
 */
/* BEFORE SUIF changes: */
#if 0
			err("treating LOGICAL*1 as LOGICAL");
			--nerr;	/* allow generation of .c file */
#endif
/* AFTER SUIF changes: */
			warn("treating LOGICAL*1 as LOGICAL");
/* END SUIF changes */
			goto ret;
			}
		break;

	case TYLONG:
		if(length == 0)
			return(tyint);
		if(length == typesize[TYSHORT])
			return(TYSHORT);
		if(length == typesize[TYLONG])
			goto ret;
		break;
	default:
		badtype("lengtype", type);
	}

	if(length != 0)
		err("incompatible type-length combination");

ret:
	return(type);
}





/* setintr -- Set Intrinsic function */

setintr(v)
register Namep  v;
{
	int k;

	if(v->vstg == STGUNKNOWN)
		v->vstg = STGINTR;
	else if(v->vstg!=STGINTR)
		dclerr("incompatible use of intrinsic function", v);
	if(v->vclass==CLUNKNOWN)
		v->vclass = CLPROC;
	if(v->vprocclass == PUNKNOWN)
		v->vprocclass = PINTRINSIC;
	else if(v->vprocclass != PINTRINSIC)
		dclerr("invalid intrinsic declaration", v);
	if(k = intrfunct(v->fvarname)) {
		if ((*(struct Intrpacked *)&k).f4)
			if (noextflag)
				goto unknown;
			else
				dcomplex_seen++;
		v->vardesc.varno = k;
		}
	else {
 unknown:
		dclerr("unknown intrinsic function", v);
		}
}



/* setext -- Set External declaration -- assume that unknowns will become
   procedures */

setext(v)
register Namep  v;
{
	if(v->vclass == CLUNKNOWN)
		v->vclass = CLPROC;
	else if(v->vclass != CLPROC)
		dclerr("invalid external declaration", v);

	if(v->vprocclass == PUNKNOWN)
		v->vprocclass = PEXTERNAL;
	else if(v->vprocclass != PEXTERNAL)
		dclerr("invalid external declaration", v);
} /* setext */




/* create dimensions block for array variable */
struct dim_struct {
	expptr lb, ub; 
};

setbound(v, nd, dims)
register Namep  v;
int nd;
struct dim_struct dims[ ];
{
	register expptr q, t;
	register struct Dimblock *p;
	int i;
	extern expptr make_int_expr ();
	extern int doing_setbound;
	extern chainp new_vars;
	char buf[256];
	int offset_size;   /* DEM used for offset of semi-constants */

	if(v->vclass == CLUNKNOWN)
		v->vclass = CLVAR;
	else if(v->vclass != CLVAR)
	{
		dclerr("only variables may be arrays", v);
		return;
	}

/*
 * Changed by CSW for SUIF:
 *     We added fields to struct Dimblock, so the expression here to
 *     compute the space to allocate for it must change.
 */
/* BEFORE SUIF changes: */
#if 0
	v->vdim = p = (struct Dimblock *)
	    ckalloc( sizeof(int) + (3+2*nd)*sizeof(expptr) );
#endif
/* AFTER SUIF changes: */
	v->vdim = p = (struct Dimblock *)
	    ckalloc( sizeof(int) + (4+5*nd)*sizeof(expptr) );
/* END SUIF changes */
	p->ndim = nd--;
	p->nelt = ICON(1);
	doing_setbound = 1;

	for(i = 0; i <= nd; ++i)
	{
/*
 * Changed by CSW for SUIF:
 *     We've replaced the dimexpr field with new fields, so those need
 *     to be set here.
 */
/* BEFORE SUIF changes: */
#if 0
		if( (q = dims[i].ub) == NULL)
		{
			if(i == nd)
			{
				frexpr(p->nelt);
				p->nelt = NULL;
			}
			else
				err("only last bound may be asterisk");
			/*p->dims[i].dimsize = ICON(1);
			;
			p->dims[i].dimexpr = NULL; */
/* DEM create last dimension variable for variable dimension arrays */
/* CSW: Create the last dimensions variable in a way we can tell later is
        really a star */
/* commented out by CSW
				sprintf(buf, " %s_dim%d", v->fvarname, i+1);
*/
				sprintf(buf, " %s_star_end", v->fvarname);
/* end CSW change */
				p->dims[i].dimsize = (expptr)
					  autovar(1, tyint, EXNULL, buf);
				p->dims[i].dimexpr =
					  make_int_expr(putx(fixtype(ICON(1))));
				p->dims[i].dimsize->addrblock.isaconst = 1;
				p->dims[i].dimsize->addrblock.kludgeci = 1;
		}
		else
		{

			if(dims[i].lb)
			{
				q = mkexpr(OPMINUS, q, cpexpr(dims[i].lb));
				q = mkexpr(OPPLUS, q, ICON(1) );
			}
/* DEM create dimension vars for const dim arrays also */
			if( ISCONST(q) )
			{
/*
				p->dims[i].dimsize = q;
				p->dims[i].dimexpr = (expptr) PNULL;
*/
				sprintf(buf, " %s_dim%d", v->fvarname, i+1);
				p->dims[i].dimsize = (expptr)
					  autovar(1, tyint, EXNULL, buf);
				p->dims[i].dimexpr =
					  make_int_expr(putx(fixtype(q)));
				p->dims[i].dimsize->addrblock.isaconst = 1;
				p->dims[i].dimsize->addrblock.kludgeci =
					q->constblock.Const.ci;
				if (i == nd)
					  v->vlastdim = new_vars;
				
			}
			else {
				sprintf(buf, " %s_dim%d", v->fvarname, i+1);
				p->dims[i].dimsize = (expptr)
					autovar(1, tyint, EXNULL, buf);
				p->dims[i].dimexpr =
					make_int_expr(putx(fixtype(q)));
				p->dims[i].dimsize->addrblock.isaconst = 0;
				if (i == nd)
					v->vlastdim = new_vars;
			}
			if(p->nelt)
				p->nelt = mkexpr(OPSTAR, p->nelt,
				    cpexpr(p->dims[i].dimsize) );
		}
#endif
/* AFTER SUIF changes: */
		sprintf(buf, " %s_lb%d", v->fvarname, i + 1);
		p->dims[i].lbvar = (expptr)autovar(1, tyint, EXNULL, buf);

		if (dims[i].lb == NULL)
		  {
		    p->dims[i].lbexpr = make_int_expr(putx(fixtype(ICON(1))));
		  }
		else
		  {
		    p->dims[i].lbexpr =
			    make_int_expr(putx(fixtype(cpexpr(dims[i].lb))));
		  }

		if (dims[i].ub == NULL)
		{
			p->dims[i].ubvar = NULL;
			p->dims[i].ubexpr = NULL;

			if(i == nd)
			{
				frexpr(p->nelt);
				p->nelt = NULL;
			}
			else
				err("only last bound may be asterisk");
			sprintf(buf, " %s_dim%d", v->fvarname, i+1);
			/*
			 * The field dimsize will never get written out, but
			 * internally sf2c may follow the pointer, so we need
			 * some expression here.  We just re-use lbvar.
			 */
			p->dims[i].dimsize = cpexpr(p->dims[i].lbvar);
			p->dims[i].dimsize->addrblock.isaconst = 1;
			p->dims[i].dimsize->addrblock.kludgeci = 1;
		}
		else
		{
			sprintf(buf, " %s_ub%d", v->fvarname, i + 1);
			p->dims[i].ubvar =
				(expptr)autovar(1, tyint, EXNULL, buf);
			p->dims[i].ubexpr =
			    make_int_expr(putx(fixtype(cpexpr(dims[i].ub))));

			sprintf(buf, " %s_dim%d", v->fvarname, i+1);
			p->dims[i].dimsize = (expptr)
				autovar(1, tyint, EXNULL, buf);

			q = cpexpr(dims[i].ub);
			if(dims[i].lb)
			{
				q = mkexpr(OPMINUS, q, cpexpr(dims[i].lb));
				q = mkexpr(OPPLUS, q, ICON(1) );
			}
			if( ISCONST(q) )
			{
				p->dims[i].dimsize->addrblock.isaconst = 1;
				p->dims[i].dimsize->addrblock.kludgeci =
					q->constblock.Const.ci;
				
			}
			else {
				p->dims[i].dimsize->addrblock.isaconst = 0;
			}
			frexpr(q);
			if (i == nd)
				v->vlastdim = new_vars;
			if(p->nelt)
				p->nelt = mkexpr(OPSTAR, p->nelt,
				    cpexpr(p->dims[i].dimsize) );
		}
/* END SUIF changes */
	}

/*
 * Changed by CSW for SUIF:
 *     We added the ``basevar'' field and it must be set here.
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes: */
	sprintf(buf, " %s_base", v->fvarname);
	p->basevar = (expptr) autovar(1, tyint, EXNULL, buf);

/* END SUIF changes */
	q = dims[nd].lb;
/*
 * Changed by CSW for SUIF:
 *     DEM's attempt to keep the kludged constant value in offset_size
 *     isn't fully implemented here, so it needs to be fixed.
 */
/* BEFORE SUIF changes: */
#if 0

/* DEM calculate semi-constant offset also */
	if(q == NULL) {
		q = ICON(1);
		offset_size=1;
	} else offset_size = q->constblock.Const.ci;

	for(i = nd-1 ; i>=0 ; --i)
	{
		int offset_adder;

		t = dims[i].lb;
		if(t == NULL) {
			t = ICON(1);
			offset_adder = 1;
		} else offset_adder = t->constblock.Const.ci;
		if(p->dims[i].dimsize) {
/* DEM deal with semi-constants */
			int tval = t->constblock.Const.ci;
			int pval; 
			int qval;
			if (p->dims[i].dimsize->addrblock.isaconst &&
			    ISCONST(q)) {
				pval = p->dims[i].dimsize->addrblock.kludgeci;
				qval = q->constblock.Const.ci; 
				q = mkexpr(OPPLUS, t,
				  mkexpr(OPSTAR,cpexpr(p->dims[i].dimsize),q));
/* DEM avoid overwriting bug */ q = cpexpr(q);
				q->addrblock.isaconst = 1;
				q->addrblock.kludgeci = tval + qval*pval;
			} else if (p->dims[i].dimsize->addrblock.isaconst &&
					q->addrblock.isaconst) {
				pval = p->dims[i].dimsize->addrblock.kludgeci;
				qval = q->addrblock.kludgeci; 
				q = mkexpr(OPPLUS, t,
				  mkexpr(OPSTAR,cpexpr(p->dims[i].dimsize),q));
/* DEM avoid overwriting bug */ q = cpexpr(q); 
				q->addrblock.isaconst = 1;
				q->addrblock.kludgeci = tval + qval*pval;
			} else {
				q = mkexpr(OPPLUS, t, 
				  mkexpr(OPSTAR,cpexpr(p->dims[i].dimsize),q));
			}
		}
	}
#endif
/* AFTER SUIF changes: */
/* DEM calculate semi-constant offset also */
	if(q == NULL)
		q = ICON(1);

	for(i = nd-1 ; i>=0 ; --i)
	{
		t = dims[i].lb;
		if(t == NULL)
			t = ICON(1);
		if(p->dims[i].dimsize) {
/* DEM deal with semi-constants */
			int tval;
			int pval; 
			int qval;
			int new_is_const = 1;

			if (ISCONST(t))
				tval = t->constblock.Const.ci;
			else if (t->addrblock.isaconst)
				tval = t->addrblock.kludgeci;
			else
				new_is_const = 0;

			if (ISCONST(p->dims[i].dimsize))
				pval = p->dims[i].dimsize->constblock.Const.ci;
			else if (p->dims[i].dimsize->addrblock.isaconst)
				pval = p->dims[i].dimsize->addrblock.kludgeci;
			else
				new_is_const = 0;

			if (ISCONST(q))
				qval = q->constblock.Const.ci;
			else if (q->addrblock.isaconst)
				qval = q->addrblock.kludgeci;
			else
				new_is_const = 0;

			q = mkexpr(OPPLUS, t,
				  mkexpr(OPSTAR,cpexpr(p->dims[i].dimsize),q));
			if (new_is_const) {
/* DEM avoid overwriting bug */ q = cpexpr(q);
				q->addrblock.isaconst = 1;
				offset_size = tval + qval*pval;
				q->addrblock.kludgeci = offset_size;
			}
		}
	}

	if (ISCONST(q))
		offset_size = q->constblock.Const.ci;
	else if (q->addrblock.isaconst)
		offset_size = q->addrblock.kludgeci;

/* END SUIF changes */
				
/* DEM create offset vars for const dim arrays also */
	if( ISCONST(q) || q->addrblock.isaconst)
	{
/*
		p->baseoffset = q;
		p->basexpr = NULL;
*/
		sprintf(buf, " %s_offset", v->fvarname);
		p->baseoffset = (expptr) autovar(1, tyint, EXNULL, buf);
		p->baseoffset->addrblock.isaconst = 1;
		p->baseoffset->addrblock.kludgeci = offset_size;
		p->basexpr = make_int_expr(putx(fixtype(q)));
	}
	else
	{
		sprintf(buf, " %s_offset", v->fvarname);
		p->baseoffset = (expptr) autovar(1, tyint, EXNULL, buf);
		p->basexpr = make_int_expr(putx(fixtype(q)));
	}
	doing_setbound = 0;
}



write_abbrevs (outfile, function_head, vars)
FILE *outfile;
int function_head;
chainp vars;
{
    for (; vars; vars = vars -> nextp) {
	Namep name = (Namep) vars -> datap;

	if (function_head)
	    nice_printf (outfile, "#define ");
	else
	    nice_printf (outfile, "#undef ");
	output_name (outfile, name);

	if (function_head) {
	    struct Extsym *comm = &extsymtab[name -> vardesc.varno];

	    nice_printf (outfile, " (");
	    output_extern (outfile, comm);
	    nice_printf (outfile, "%d.", comm->curno);
	    nice_printf (outfile, "%s)", name->cvarname);
	} /* if function_head */
	nice_printf (outfile, "\n");
    } /* for */
} /* write_abbrevs */
