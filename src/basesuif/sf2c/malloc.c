#ifndef CRAY
#define STACKMIN 512
#define MINBLK (2*sizeof(struct mem) + 16)
#define MSTUFF _malloc_stuff_
#define F MSTUFF.free
#define B MSTUFF.busy
#define SBGULP 8192
/*
 * Changed by CSW for SUIF:
 *     The <stdlib.h> header is needed for calloc() and <string.h> is
 *     needed for memcpy() and memset().
 */
/* BEFORE SUIF changes: */
#if 0
char *memcpy();
#endif
/* AFTER SUIF changes */

#include <stdlib.h>
#include <string.h>
/* END SUIF changes */

struct mem {
	struct mem *next;
	unsigned len;
	};

struct { struct mem *free;
#ifdef Use_B
	struct mem
#else
	char
#endif
		*busy;
	} MSTUFF;

/*
 * Changed by CSW for SUIF:
 *     Now that we're using ANSI headers, we need to have the
 *     definitions of ANSI-specified functions fit ANSI.
 */
/* BEFORE SUIF changes: */
#if 0
char *
malloc(size)
register unsigned size;
#endif
/* AFTER SUIF changes */
void *malloc(size_t size)
/* END SUIF changes */
{
	register struct mem *p, *q, *r, *s;
	unsigned register k, m;
	extern char *sbrk();
	char *top, *top1;

	size = (size+7) & ~7;
	r = (struct mem *) &F;
	for (p = F, q = 0; p; r = p, p = p->next) {
		if ((k = p->len) >= size && (!q || m > k)) { m = k; q = p; s = r; }
		}
	if (q) {
		if (q->len - size >= MINBLK) { /* split block */
			p = (struct mem *) (((char *) (q+1)) + size);
			p->next = q->next;
			p->len = q->len - size - sizeof(struct mem);
			s->next = p;
			q->len = size;
			}
		else s->next = q->next;
#ifdef Use_B
		for (p = B, r = (struct mem *) &B; ; r = p, p = p->next)
			if (p < q) break;
		q->next = p;
		r->next = q;
#endif
		}
	else {
#ifdef Use_B
		top = sbrk(0);
		if (F > B) { q = F; F = F->next; }
		else if (B) q = (struct mem *) (((char *) (B+1)) + B->len);
#else
		top = B ? B : sbrk(0);
		if (F && (char *)(F+1) + F->len == B)
			{ q = F; F = F->next; }
#endif
		else q = (struct mem *) top;
		top1 = (char *)(q+1) + size;
		if (top1 > top) {
			if (sbrk(top1-top+SBGULP) == (char *) -1)
				return 0;
			r = (struct mem *)top1;
			r->len = SBGULP - sizeof(struct mem);
			r->next = F;
			F = r;
			top1 += SBGULP;
			}
		q->len = size;
#ifdef Use_B
		q->next = B;
		B = q;
#else
		B = top1;
#endif
		}
	return (char *) (q+1);
	}
/*
 * Changed by CSW for SUIF:
 *     David Moore pointed out that both malloc() and calloc() are
 *     used by this program and the results of both are eventually
 *     used with calls to free(), yet this program defines its own
 *     malloc() and free() without defining calloc().  So if the
 *     system calloc() doesn't call malloc(), the local free() will
 *     erroneously be called on something allocated by the wrong
 *     allocator.  Following David Moore's recommendation, we define a
 *     local calloc() that uses the local malloc().
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes */

void *calloc(size_t nmemb, size_t size)
{
        void *result = malloc(nmemb * size);
        memset(result, 0, nmemb * size);
        return result;
}
/* END SUIF changes */

/*
 * Changed by CSW for SUIF:
 *     Now that we're using ANSI headers, we need to have the
 *     definitions of ANSI-specified functions fit ANSI.
 */
/* BEFORE SUIF changes: */
#if 0
free(f)
char *f;
{
#endif
/* AFTER SUIF changes */
void free(void *ptr)
{
        char *f = (char *)ptr;
/* END SUIF changes */
	struct mem *p, *q, *r;
	char *pn, *qn;

	if (!f) return;
	q = (struct mem *) (f - sizeof(struct mem));
	qn = f + q->len;
#ifdef Use_B
	for (p = B, r = (struct mem *) &B; p >= q; r = p, p = p->next) {
		if (p == q) {
			r->next = q->next;
			break;
			}
		}
#endif
	for (p = F, r = (struct mem *) &F; ; r = p, p = p->next) {
		if (qn == (char *) p) {
			q->len += p->len + sizeof(struct mem);
			p = p->next;
			}
		pn = p ? ((char *) (p+1)) + p->len : 0;
		if (pn == (char *) q) {
			p->len += sizeof(struct mem) + q->len;
			q->len = 0;
			q->next = p;
			r->next = p;
			break;
			}
		if (pn < (char *) q) {
			r->next = q;
			q->next = p;
			break;
			}
		}
	}

/*
 * Changed by CSW for SUIF:
 *     Now that we're using ANSI headers, we need to have the
 *     definitions of ANSI-specified functions fit ANSI.
 */
/* BEFORE SUIF changes: */
#if 0
char *
realloc(f, size)
char *f;
unsigned size;
{
#endif
/* AFTER SUIF changes */
void *realloc(void *ptr, size_t size)
{
        char *f = (char *)ptr;
/* END SUIF changes */
	struct mem *p;
	char *q, *f1;
	unsigned s1;

	if (!f) return malloc(size);
	p = (struct mem *) (f - sizeof(struct mem));
	s1 = p->len;
	free(f);
	if (s1 > size) s1 = size + 7 & ~7;
	if (!p->len) {
		f1 = (char *)(p->next + 1);
		memcpy(f1, f, s1);
		f = f1;
		}
	q = malloc(size);
	if (q && q != f)
		memcpy(q, f, s1);
	return q;
	}

#ifdef Compile_memcpy
#include <stdio.h>
memcpy(t, f, n)
char *f, *t;
{
	register *r, *re, *s;

	r = (int *) f;
	re = (int *) (f + n);

	if ((unsigned)r & 3 || (unsigned)re & 3 || (unsigned)t & 3) {
		fprintf(stderr, "memcpy(0x%x, 0x%x, 0x%x)\n", t, f, n);
		exit(1);
		}

	if (f > t) for (s = (int *)t; r < re;) *s++ = *r++;
	else if (f < t) for (s = (int *)(t + n); r < re;) *--s = *--re;
	}
#endif
#endif
