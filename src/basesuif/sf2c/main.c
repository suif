extern char F2C_version[];
/*
 * Changed by CSW for SUIF:
 *     These two variables are linked in by the standard SUIF
 *     Makefiles.
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes: */
extern char *prog_ver_string;
extern char *prog_who_string;
/* END SUIF changes */

#include "defs.h"
#include "parse.h"
/*
 * Changed by CSW for SUIF:
 *     The original code for nice_printf() and some related printing
 *     routines needed varargs functionality, but it got it by
 *     assuming every parameter is an int and just passing a whole
 *     bunch of int parameters around.  This breaks on the DEC Alpha,
 *     a 64-bit machine.  So I've changed this to use the ANSI C way
 *     of handling varargs.  That means we need prototypes included
 *     whenever one of the functions in question is handled, so
 *     "output.h" needs to be included.
 *
 *     Also, <stdlib.h> is needed because we've added uses of getenv()
 *     and malloc().
 */
/* BEFORE SUIF changes: */
#if 0
#endif
#include <unistd.h>
/* AFTER SUIF changes: */
#include "output.h"
#include <stdlib.h>
/* END SUIF changes */
#include <signal.h>

int complex_seen, dcomplex_seen;

LOCAL void clfiles();
LOCAL int Max_ftn_files;

char **ftn_files;
int current_ftn_file = 0;

flag ppflag = NO; /* DEM, put array references on unique line numbers */
flag semilabflag = NO; /* DEM put a semicolon after each label */
/*
 * Changed by CSW for SUIF:
 *     We want additional command line flags.
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes: */
flag version_only = NO; /* just print the version and exit */
flag quiet_flag = NO;   /* turn off diagnostic output */
/* END SUIF changes */

flag ftn66flag = NO;
flag nowarnflag = NO;
flag noextflag = NO;
flag  no66flag = NO;		/* Must also set noextflag to this
					   same value */
flag zflag = YES;		/* recognize double complex intrinsics */
flag debugflag = NO;
flag onetripflag = NO;
flag shortsubs = YES;		/* Use short subscripts on arrays? */
flag shiftcase = YES;
flag undeftype = NO;
flag checksubs = NO;
flag r8flag = NO;
int tyreal = TYREAL;
extern void r8fix();

int maxregvar = MAXREGVAR;	/* if maxregvar > MAXREGVAR, error */
int maxequiv = MAXEQUIV;
int maxext = MAXEXT;
int maxstno = MAXSTNO;
int maxctl = MAXCTL;
int maxhash = MAXHASH;
int extcomm, ext1comm, useauto;
int can_include = YES;	/* so we can disable includes for netlib */

static char *def_i2 = "";

static int useshortints = NO;	/* YES => tyint = TYSHORT */
static int uselongints = NO;	/* YES => shortsubs = NO;
				   tyint = TYLONG */
static int useshortsubs = NO;	/* YES => shortsubs = YES */
int addftnsrc = NO;		/* Include ftn source in output */
int usedefsforcommon = NO;	/* Use #defines for common reference */
int forcedouble = YES;		/* force real functions to double */
int Ansi = NO;
int define_equivs = YES;
int tyioint = TYLONG;
int szleng = SZLENG;
int inqmask = M(TYLONG)|M(TYLOGICAL);
int wordalign = NO;
int chars_per_wd, gflag;

extern char *fl_fmt_string, *db_fmt_string;
static char *tmpdir = "/tmp";

#define f2c_entry(swit,count,type,store,size) \
	p_entry ("-", swit, 0, count, type, store, size)

arg_info table[] = {
    f2c_entry ("o", P_INFINITE_ARGS, P_STRING, NULL, 0),
    f2c_entry ("pp", P_NO_ARGS, P_INT, &ppflag, YES), /* DEM */
    f2c_entry ("lab", P_NO_ARGS, P_INT, &semilabflag, YES), /* DEM */
/*
 * Changed by CSW for SUIF:
 *     We want command line options that f2c didn't have.
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes: */
    f2c_entry ("version", P_NO_ARGS, P_INT, &version_only, YES),
    f2c_entry ("quiet", P_NO_ARGS, P_INT, &quiet_flag, YES),
/* END SUIF changes */
    f2c_entry ("w66", P_NO_ARGS, P_INT, &ftn66flag, YES),
    f2c_entry ("w", P_NO_ARGS, P_INT, &nowarnflag, YES),
    f2c_entry ("66", P_NO_ARGS, P_INT, &no66flag, YES),
    f2c_entry ("d", P_ONE_ARG, P_INT, &debugflag, YES),
    f2c_entry ("1", P_NO_ARGS, P_INT, &onetripflag, YES),
    f2c_entry ("onetrip", P_NO_ARGS, P_INT, &onetripflag, YES),
    f2c_entry ("I2", P_NO_ARGS, P_INT, &useshortints, YES),
    f2c_entry ("I4", P_NO_ARGS, P_INT, &uselongints, YES),
    f2c_entry ("Is", P_NO_ARGS, P_INT, &useshortsubs, YES),
    f2c_entry ("U", P_NO_ARGS, P_INT, &shiftcase, NO),
    f2c_entry ("u", P_NO_ARGS, P_INT, &undeftype, YES),
    f2c_entry ("O", P_ONE_ARG, P_INT, &maxregvar, 0),
    f2c_entry ("C", P_NO_ARGS, P_INT, &checksubs, YES),
    f2c_entry ("Nq", P_ONE_ARG, P_INT, &maxequiv, 0),
    f2c_entry ("Nx", P_ONE_ARG, P_INT, &maxext, 0),
    f2c_entry ("Ns", P_ONE_ARG, P_INT, &maxstno, 0),
    f2c_entry ("Nc", P_ONE_ARG, P_INT, &maxctl, 0),
    f2c_entry ("Nn", P_ONE_ARG, P_INT, &maxhash, 0),
    f2c_entry ("c", P_NO_ARGS, P_INT, &addftnsrc, YES),
    f2c_entry ("p", P_NO_ARGS, P_INT, &usedefsforcommon, YES),
    f2c_entry ("R", P_NO_ARGS, P_INT, &forcedouble, NO),
    f2c_entry ("A", P_NO_ARGS, P_INT, &Ansi, YES),
    f2c_entry ("Fd", P_ONE_ARG, P_STRING, &db_fmt_string, 0),
    f2c_entry ("Fr", P_ONE_ARG, P_STRING, &fl_fmt_string, 0),
    f2c_entry ("ev", P_NO_ARGS, P_INT, &define_equivs, NO),
    f2c_entry ("ext", P_NO_ARGS, P_INT, &noextflag, YES),
    f2c_entry ("z", P_NO_ARGS, P_INT, &zflag, NO),
    f2c_entry ("a", P_NO_ARGS, P_INT, &useauto, YES),
    f2c_entry ("r8", P_NO_ARGS, P_INT, &r8flag, YES),
    f2c_entry ("i2", P_NO_ARGS, P_INT, &tyioint, NO),
    f2c_entry ("w8", P_NO_ARGS, P_INT, &wordalign, YES),
    f2c_entry ("!I", P_NO_ARGS, P_INT, &can_include, NO),
    f2c_entry ("W", P_ONE_ARG, P_INT, &chars_per_wd, 0),
    f2c_entry ("g", P_NO_ARGS, P_INT, &gflag, YES),
    f2c_entry ("T", P_ONE_ARG, P_STRING, &tmpdir, 0),
    f2c_entry ("E", P_NO_ARGS, P_INT, &extcomm, 1),
    f2c_entry ("e1c", P_NO_ARGS, P_INT, &ext1comm, 1),
    f2c_entry ("ec", P_NO_ARGS, P_INT, &ext1comm, 2)
}; /* table */

char *c_functions	= "c_functions";
char *coutput		= "c_output";
char *initfname		= "raw_data";
char *blkdfname		= "block_data";
char *p1_file		= "p1_file";
char *p1_filebak	= "p1_file.BAK";
char *sortfname		= "init_file";

void list_init_data();

 static void
set_tmp_names()
{
	int k, pid;
	if (debugflag == 1)
		return;
	pid = getpid();
	k = strlen(tmpdir) + 16;
	c_functions = (char *)ckalloc(6*k);
	initfname = c_functions + k;
	blkdfname = initfname + k;
	p1_file = blkdfname + k;
	p1_filebak = p1_file + k;
	sortfname = p1_filebak + k;
	sprintf(c_functions, "%s/f2c%d_func", tmpdir, pid);
	/* keep initfname short enough to append .b in backup_filename */
	sprintf(initfname, "%s/f2c%d_rd", tmpdir, pid);
	sprintf(blkdfname, "%s/f2c%d_blkd", tmpdir, pid);
	sprintf(p1_file, "%s/f2c%d_p1f", tmpdir, pid);
	sprintf(p1_filebak, "%s/f2c%d_p1fb", tmpdir, pid);
	sprintf(sortfname, "%s/f2c%d_sort", tmpdir, pid);
	if (debugflag)
		fprintf(stderr, "%s %s %s %s %s %s\n", c_functions,
			initfname, blkdfname, p1_file, p1_filebak, sortfname);
	}

 static char *
c_name(s)char *s;
{
	char *b, *s0;
	int c;

	b = s0 = s;
	while((c = *s++))
		if (c == '/')
			b = s;
	if (--s < s0 + 3 || s[-2] != '.'
			 || ((c = *--s) != 'f' && c != 'F')) {
		infname = s0;
		Fatal("file name must end in .f or .F");
		}
	*s = 'c';
	b = copys(b);
	*s = c;
	return b;
	}

char *current_time ()
{
    extern long time ();
    extern char *ctime ();
    long int now = time ((long *) 0);
    static char buf[50];

    strcpy (buf, ctime (&now));
    return buf;
} /* current_time */


void set_externs ()
{

/* Adjust the global flags according to the command line parameters */

    if (chars_per_wd > 0) {
	typesize[TYADDR] = typesize[TYLONG] = typesize[TYREAL] =
		typesize[TYLOGICAL] = chars_per_wd;
	typesize[TYDREAL] = typesize[TYCOMPLEX] = chars_per_wd << 1;
	typesize[TYDCOMPLEX] = chars_per_wd << 2;
	typesize[TYSHORT] = chars_per_wd >> 1;
	typesize[TYCILIST] = 5*chars_per_wd;
	typesize[TYICILIST] = 6*chars_per_wd;
	typesize[TYOLIST] = 9*chars_per_wd;
	typesize[TYCLLIST] = 3*chars_per_wd;
	typesize[TYALIST] = 2*chars_per_wd;
	typesize[TYINLIST] = 26*chars_per_wd;
	}

    if (wordalign)
	typealign[TYDREAL] = typealign[TYDCOMPLEX] = typealign[TYREAL];
    if (!tyioint) {
	tyioint = TYSHORT;
	szleng = typesize[TYSHORT];
	def_i2 = "#define f2c_i2 1\n";
	inqmask = M(TYSHORT)|M(TYLOGICAL);
	goto checklong;
	}
    else
	szleng = typesize[TYLONG];
    if (useshortints) {
	inqmask = M(TYLONG);
 checklong:
	typename[TYLOGICAL] = "shortlogical";
	if (uselongints)
	    err ("Can't use both long and short ints");
	else
	    tyint = tylogical = TYSHORT;
	}
    else if (uselongints) {
	shortsubs = NO;
	tyint = TYLONG;
    } /* if uselongints */

    if (useshortsubs)
	shortsubs = YES;

    if (no66flag)
	noextflag = no66flag;
    if (noextflag)
	zflag = 0;

    if (r8flag) {
	tyreal = TYDREAL;
	r8fix();
	}

    if (maxregvar > MAXREGVAR) {
	warni("-O%d: too many register variables", maxregvar);
	maxregvar = MAXREGVAR;
    } /* if maxregvar > MAXREGVAR */

/* Check the list of input files */

    {
	int bad, i, cur_max = Max_ftn_files;

	for (i = bad = 0; i < cur_max && ftn_files[i]; i++)
	    if (ftn_files[i][0] == '-') {
		errstr ("Invalid flag '%s'", ftn_files[i]);
		bad++;
		}
	if (bad)
		exit(1);

    } /* block */
} /* set_externs */

 static void
killed()
{
	void unlinkall();
	signal(SIGINT, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	signal(SIGHUP, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	unlinkall();
	exit(126);
	}

 static void
flovflo()
{
	Fatal("floating exception during constant evaluation; cannot recover");
	/* vax returns a reserved operand that generates
	   an illegal operand fault on next instruction,
	   which if ignored causes an infinite loop.
	*/
	signal(SIGFPE, flovflo);
}


 static void
sig1catch(sig) int sig;
{
	if (signal(sig, SIG_IGN) != SIG_IGN)
		signal(sig, killed);
	}

 static void
sigcatch()
{
	sig1catch(SIGINT);
	sig1catch(SIGQUIT);
	sig1catch(SIGHUP);
	sig1catch(SIGTERM);
	signal(SIGFPE, flovflo);  /* catch overflows */
	}

 static int
comm2dcl()
{
	struct Extsym *ext;
	if (ext1comm)
		for(ext = extsymtab; ext < nextext; ext++)
			if (ext->extstg == STGCOMMON && !ext->extinit)
				return ext1comm;
	return 0;
	}

 char *filename0;

main(argc, argv)
int argc;
char **argv;
{
	int c2d, k, pid, retcode, status, w;
	FILE *c_output;
	char *filename, *cdfilename;
	char **outfiles;
	static char stderrbuf[BUFSIZ];
	extern void define_commons();

	setbuf(stderr, stderrbuf);	/* arrange for fast error msgs */

	fileinit();

#define DONE(c)	{ retcode |= c; goto finis; }

	Max_ftn_files = argc - 1;
	ftn_files = (char **)ckalloc(argc*sizeof(char *));

	outfiles = (char **)ckalloc(argc*sizeof(char *));
/*
 * Changed by CSW for SUIF:
 *     There is an assignment here from (char **) to (int *), and some
 *     compilers complain about this, so for portability we want to
 *     have an explicit cast here.
 */
/* BEFORE SUIF changes: */
#if 0
	table[0].result_ptr = outfiles;
#endif
/* AFTER SUIF changes: */
	table[0].result_ptr = (int *)outfiles;
/* END SUIF changes */
	table[0].table_size = argc - 1;
/*
 * Changed by CSW for SUIF:
 *     The standard for the SUIF system is to allow the $TMPDIR to
 *     choose a directory for temporary files.  This code used to
 *     hard-code "/tmp" and allow it to be changed only by the -T
 *     command-line argument.  But it's very convenient to have
 *     $TMPDIR work on all SUIF passes, so I've changed this code to
 *     allow $TMPDIR to override the default "/tmp" while still
 *     allowing the ``-T'' command-line argument to override the
 *     $TMPDIR environment variable.
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes: */

          {
            char *new_tmpdir;

            new_tmpdir = getenv("TMPDIR");
            if (new_tmpdir != NULL)
              {
                tmpdir = malloc(strlen(new_tmpdir) + 1);
                strcpy(tmpdir, new_tmpdir);
              }
          }
/* END SUIF changes */

	parse_args (argc, argv, table, sizeof(table)/sizeof(arg_info),
		ftn_files, Max_ftn_files);
	if (!can_include && ext1comm == 2)
		ext1comm = 1;
	if (ext1comm && !extcomm)
		extcomm = 2;

/*
 * Changed by CSW for SUIF:
 *     We want the ``-version'' command to work here as it does in
 *     other SUIF passes.
 */
/* BEFORE SUIF changes: */
#if 0
#endif
/* AFTER SUIF changes: */
        if (version_only)
          {
            fprintf(stderr, "sf2c %s %s\n", prog_ver_string, prog_who_string);
            fprintf(stderr, "  (%s)\n", F2C_version);
            exit(0);
          }
/* END SUIF changes */

	if (outfiles[0] != NULL) {
		for(k = 0; 1; k++) {
			if ((outfiles[k] == NULL) && (ftn_files[k] == NULL))
				break;
			if (outfiles[k] == NULL) {
				Fatal("more input files than output files "
                                      "specified");
				}
			if (ftn_files[k] == NULL) {
				Fatal("more output files than input files "
                                      "specified");
				}
			}
		}

	set_externs ();

	retcode = 0;
/*
 * Changed by CSW for SUIF:
 *     There's a memory bug in the original f2c here: if there are no
 *     command line arguments at all, the ftn_files[] array will
 *     contain only one element, a NULL to signify the end of the
 *     array.  But this loop always starts at the second array
 *     element, so in this case it will start going through some
 *     unrelated memory.
 */
/* BEFORE SUIF changes: */
#if 0
	for(k = 1; ftn_files[k]; k++) {
		if (!(pid = fork()))
			break;
		while((w = wait(&status)) != pid)
			if (w == -1)
				Fatal("bad wait code");
		retcode |= status >> 8;
		}
#endif
/* AFTER SUIF changes: */
	k = 1;
	if (ftn_files[0]) {
		for(k = 1; ftn_files[k]; k++) {
			if (!(pid = fork()))
				break;
			while((w = wait(&status)) != pid)
				if (w == -1)
					Fatal("bad wait code");
			retcode |= status >> 8;
			}
		}
/* END SUIF changes */
	filename0 = filename = ftn_files[current_ftn_file = k - 1];

	set_tmp_names();
	sigcatch();

	initfile = opf(initfname);
	c_file   = opf(c_functions);
	pass1_file=opf(p1_file);
	initkey();
	if (filename && *filename) {
		if (outfiles[0] == NULL) {
			if (debugflag != 1)
				coutput = c_name(filename);
			}
		else {
			coutput = outfiles[current_ftn_file];
			}
		cdfilename = coutput;
		}
	else {
		filename = "";
		cdfilename = "f2c_out.c";
		c_output = stdout;
		}
	
	if(inilex( copys(filename) ))
		DONE(1);
	if (filename0) {
/*
 *  Changed by CSW for SUIF:
 *      We want a switch to turn off diagnostic output.
 */
/* BEFORE SUIF changes: */
#if 0
		fprintf(diagfile, "%s:\n", filename);
		fflush(stderr);
#endif
/* AFTER SUIF changes: */
		if (!quiet_flag)
		{
			fprintf(diagfile, "%s:\n", filename);
			fflush(diagfile);
		}
/* END SUIF changes */
		if ((c_output = fopen (coutput, "w")) == (FILE *) NULL)
			Fatal("main - couldn't open c_output");
		}

	procinit();
	if(k = yyparse())
	{
		fprintf(diagfile, "Bad parse, return code %d\n", k);
		DONE(1);
	}
	if(nerr > 0)
		DONE(1);

/* Write out the declarations which are global to this file */

	if ((c2d = comm2dcl()) == 1)
		nice_printf(c_output, "/*>>>'/dev/null'<<<*/\n\n\
/* Split this into several files by piping it through\n\n\
sed \"s/^\\/\\*>>>'\\(.*\\)'<<<\\*\\/\\$/cat >'\\1' <<'\\/*<<<\\1>>>*\\/'/\" | /bin/sh\n\
 */\n\
/*<<</dev/null>>>*/\n\
/*>>>'%s'<<<*/\n", cdfilename);
/* DEM */
/*
 * Changed by CSW for SUIF:
 *     We want to print out the SUIF version information.
 */
/* BEFORE SUIF changes: */
#if 0
	nice_printf (c_output, "/* %s -- translated by sf2c ", filename);
	nice_printf (c_output, "(%s).\n", F2C_version);
#endif
/* AFTER SUIF changes: */
	nice_printf (c_output, "/* %s -- translated by\n", filename);
	nice_printf (c_output, "   sf2c %s %s\n", prog_ver_string,
		     prog_who_string);
	nice_printf (c_output, "     (%s).\n", F2C_version);
/* END SUIF changes */
	nice_printf (c_output,
	"   You must link the resulting object file with the libraries:\n\
	-lF77 -lI77 -lm -lc   (in that order)\n*/\n\n");
	nice_printf (c_output, "%s#include \"sf2c.h\"\n\n", def_i2);
	nice_printf (c_file, "\n");
	fclose (c_file);
	c_file = c_output;		/* HACK to get the next indenting
					   to work */
	write_common_decls (c_output);
	if (blkdfile)
		list_init_data(&blkdfile, blkdfname, c_output);
	write_globals (c_output);
	if ((c_file = fopen (c_functions, "r")) == (FILE *) NULL)
	    Fatal("main - couldn't reopen c_functions");
	ffilecopy (c_file, c_output);
	if (*main_alias) {
	    nice_printf (c_output, "/* Main program alias */ ");
	    nice_printf (c_output, "int %s () { MAIN__ (); }\n",
		    main_alias);
	} /* if *main_alias */
	if (c2d) {
		if (c2d == 1)
			fprintf(c_output, "/*<<<%s>>>*/\n", cdfilename);
		else
			fclose(c_output);
		define_commons(c_output);
		}
	if (c2d != 2)
		fclose (c_output);

	if(parstate != OUTSIDE)
	{
		warn("missing END statement");
		endproc();
	}

	if(nerr > 0)
		DONE(1);

finis:
	done(retcode);
	return(retcode);
}



done(k)
int k;
{
	static int recurs	= NO;

	if(recurs == NO)
	{
		recurs = YES;
		clfiles();
	}
	exit(k);
}


FILEP opf(fn)
char *fn;
{
	FILEP fp;
	if( fp = fopen(fn, "w") )
		return(fp);

	fatalstr("cannot open intermediate file %s", fn);
	/* NOTREACHED */ return 0;
}

 void
unlinkall()
{
	if (!debugflag) {
		unlink(c_functions);
		unlink(initfname);
		unlink(p1_file);
		unlink(sortfname);
		unlink(blkdfname);
		}
	}

LOCAL void clfiles()
{
	clf(&initfile);
	clf (&c_file);
	clf (&pass1_file);
	unlinkall();
}


clf(p)
FILEP *p;
{
	if(p!=NULL && *p!=NULL && *p!=stdout)
	{
		if(ferror(*p))
			Fatal("writing error");
		fclose(*p);
	}
	*p = NULL;
}
