#include "defs.h"

warni(s,t)
 char *s;
 int t;
{
	char buf[100];
	sprintf(buf,s,t);
	warn(buf);
	}

void warn1(s,t)
char *s, *t;
{
	char buff[100];
	sprintf(buff, s, t);
	warn(buff);
}


void warn(s)
char *s;
{
	if(nowarnflag)
		return;
	if (infname && *infname)
		fprintf(diagfile, "Warning on line %ld of %s: %s\n",
			lineno, infname, s);
	else
		fprintf(diagfile, "Warning on line %ld: %s\n", lineno, s);
	fflush(diagfile);
	++nwarn;
}


void errstr(s, t)
char *s, *t;
{
	char buff[100];
	sprintf(buff, s, t);
	err(buff);
}



void erri(s,t)
char *s;
int t;
{
	char buff[100];
	sprintf(buff, s, t);
	err(buff);
}

void errl(s,t)
char *s;
long t;
{
	char buff[100];
	sprintf(buff, s, t);
	err(buff);
}



void err(s)
char *s;
{
	if (infname && *infname)
		fprintf(diagfile, "Error on line %ld of %s: %s\n",
			lineno, infname, s);
	else
		fprintf(diagfile, "Error on line %ld: %s\n", lineno, s);
	fflush(diagfile);
	++nerr;
}


yyerror(s)
char *s;
{ 
	err(s); 
}



dclerr(s, v)
char *s;
Namep v;
{
	char buff[100];

	if(v)
	{
		sprintf(buff, "Declaration error for %s: %s", v->fvarname, s);
		err(buff);
	}
	else
		errstr("Declaration error %s", s);
}



execerr(s, n)
char *s, *n;
{
	char buf1[100], buf2[100];

	sprintf(buf1, "Execution error %s", s);
	sprintf(buf2, buf1, n);
	err(buf2);
}


void Fatal(t)
char *t;
{
	extern void unlinkall();

	fprintf(diagfile, "Compiler error line %ld of %s: %s\n", lineno, infname, t);
	unlinkall();
	done(3);
	exit(3);
}




fatalstr(t,s)
char *t, *s;
{
	char buff[100];
	sprintf(buff, t, s);
	Fatal(buff);
}



void fatali(t,d)
char *t;
int d;
{
	char buff[100];
	sprintf(buff, t, d);
	Fatal(buff);
}



badthing(thing, r, t)
char *thing, *r;
int t;
{
	char buff[50];
	sprintf(buff, "Impossible %s %d in routine %s", thing, t, r);
	Fatal(buff);
}



badop(r, t)
char *r;
int t;
{
	badthing("opcode", r, t);
}



badtag(r, t)
char *r;
int t;
{
	badthing("tag", r, t);
}





badstg(r, t)
char *r;
int t;
{
	badthing("storage class", r, t);
}




badtype(r, t)
char *r;
int t;
{
	badthing("type", r, t);
}


void many(s, c, n)
char *s, c;
int n;
{
	char buff[250];

	sprintf(buff,
	    "Too many %s.\nTable limit now %d.\nTry recompiling using the -N%c%d option\n",
	    s, n, c, 2*n);
	Fatal(buff);
}


err66(s)
char *s;
{
	errstr("Fortran 77 feature used: %s", s);
}



errext(s)
char *s;
{
	errstr("F77 compiler extension used: %s", s);
}
