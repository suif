.TH SF2C 1 "April 11 1994" "Stanford University" "SUIF Compiler"
.SH NAME
sf2c \- first pass of the SUIF Fortran 77 front end
.SH SYNOPSIS
.B sf2c
[
.I options
]
.I infile
.SH DESCRIPTION
The
.I sf2c
program forms the first part of the SUIF Fortran 77 front end.  It is
typically run automatically by
.I scc,
not directly by the user.  The output of
.I sf2c
should be run through
.I cpp
(the usual C pre-processor, not a part of SUIF)
.I snoot,
.I fixfortran,
and the
.I porky \-defaults
passes, in that order, to complete the SUIF Fortran front end.
.PP
The 
.I sf2c
pass converts Fortran code into a special kind of C code.  The C code
is legal and has the same semantics as the Fortran program, but it
also has special Fortran-specific information encoded in things like
assignments to dummy variables.  The most important information this
encodes is the original array type and bound information, including
symbolic bounds.
.PP
This special C program is passed through
.I cpp
and then
.I snoot,
the regular ANSI C front end, to translate it into SUIF code.  Then
fixfortran extracts the Fortran information that had been encoded in
C, restoring array types, call-by-reference and common-block
variables, and more, when possible.  Note that fixfortran is tightly
coupled with sf2c and expects its input to be in exactly the form sf2c
put it in.  Other Fortran to C converters will not work.  It is vital
that no changes are made to the C code before
.I cpp
and
.I snoot
or to the SUIF code coming out of snoot before it gets to fixfortran.
If any changes are made, fixfortran will become confused and probably
die with some error message, but it is possible it will produce
incorrect code in that case.
.PP
Our
.I sf2c
pass is based largely on a version of the Bell Labs
.I f2c
Fortran-to-C converter.  Most of the original
.I f2c
options and features are
available in
.I sf2c.
The major changes we've made have been aimed at getting more
high-level Fortran information encoded in the C output.  There are
some strange contortions we put
.I sf2c
and fixfortran through to preserve this information given the
structure of the old
.I f2c.
It is strongly recommended that the output of
.I sf2c
never be used directly; the entire front end of
.I sf2c\-cpp\-snoot\-fixfortran\-porky
should be treated as a black box.  There are a few command line flags
to sf2c that might be of general interest, but the internals of the
front end are not.
.PP
The
.I sf2c
pass takes arguments that end with
.I \.f
or
.I \.F
as Fortran 77 source programs.  If no output file is specified (the
.B \-o
option), the output (the special C file) is given the same name but
put in the current directory with an extension of
.I \.c.
If no arguments are given, then
.I sf2c
reads Fortran from standard input and writes the special C file to
standard output.
.PP
The resulting C code needs to include the
.I sf2c.h
header file, but this is normally taken care of automatically by
.I scc.
The code must eventually be linked with the libraries
.I libF77
(or its variant,
.I libF77_doall,
in the case of parallelized code)
and
.I libI77,
which are included in the SUIF distribution.
This, too, is normally handled automatically when you use scc to link.
.SH OPTIONS ADDED FOR SUIF
.TP
.B \-g
Insert C ``#line'' directives to indicate the line numbers in the
original Fortran source.  That way the eventual SUIF file's "line"
annotations will correctly refer to line numbers in the Fortran
source, not an intermediate Fortran file.
.sp
Unfortunately,
.I f2c
was not set up to keep careful track of line numbers, so the result is
line numbers that are often off by one in some parts of the program.
They are close enough to be quite useful, though.
.sp
Note that this option replaces the
.B \-g
flag in
.I f2c.
The old version of this flag printed the line numbers in comments, not
``#line'' directives.
.TP
.BI \-o " filename"
Use the given
.I filename
as the output file, instead of trying to figure out an output file
based on the input file name.
.TP
.B \-pp
Put each array reference on a separate line.  Line breaks are
introduced after each array reference if this option is used.  Then if
the
.B \-g
option is not used, array refernce instructions can be uniquely
identified by their line numbers.
.TP
.B \-lab
Put a semicolon after each label.  Why this might be useful is
unclear.
.TP
.B \-version
Print version information to stderr and exit immediately.  This is
consistent with the
.B \-version
flag for most other SUIF passes that is implemented in the main SUIF
library.
.TP
.B \-quiet
Do not print verbose commentary while compiling.  Without this flag,
.I sf2c
prints out the names of procedures as they are encountered, among
other things.
.SH OPTIONS IDENTICAL TO f77 AND f2c
.TP
.B \-C
Compile code to check that subscripts are within declared array
bounds.
.TP
.B \-I2
Render INTEGER and LOGICAL as short,
INTEGER\(**4 as long int.  Assume the default \fIlibF77\fR
and \fIlibI77\fR:  allow only INTEGER\(**4 (and no LOGICAL)
variables in INQUIREs.  This flag is incompatible with the
.I \-I4
flag.
.TP
.B \-onetrip
The semantics of a DO loop are changed so that their bodies are
performed at least once if reached.  (Fortran 77 DO loops are not
performed at all if the upper limit is smaller than the lower limit.)
.TP
.B \-U
Honor the case of variables and external names.  Fortran keywords must
be in lower case.
.TP
.B \-u
Make the default type of a variable `undefined' rather than using the
default Fortran rules.
.TP
.PD 0
.B \-w
.TP
.PD
.B \-w66
Suppress warning messages.  The 
.B \-w
form supresses all warning messages, the
.B \-w66,
form supresses only Fortran 66 compatibility warnings.
.SH OPTIONS RETAINED FROM f2c
.TP
.B \-A
Use ANSI C constructs.  The default is old-style (pre-ANSI) C.  SUIF
note: this option is irrelevant as far as SUIF is concerned because
the old-style C used is a common subset of old C and ANSI C.  The
.I snoot
pass is fine with or without this option, as it understands the
complete ANSI C language.
.TP
.B \-a
Give local variables automatic (rather than static) storage unless
they appear in a DATA, EQUIVALENCE, or SAVE statement.  SUIF note: for
SUIF it's usually better to use this option because it's easier to
analyze automatic variables.  But SUIF will still be correct whether
or not this flag is set.
.TP
.B \-c
Insert comments showing the original Fortran source.  SUIF note: if
this option is used along with the appropriate
.I cpp
and
.I snoot
options, comments can be retained all through the SUIF code in the
form of annotations on mark instructions.
.TP
.B \-E
Declare uninitialized struct created for COMMON to be \f(CWExtern\fR
(which \fIsf2c.h\fR \v(CW#define\fRs to be \f(CWextern\fR if it is
otherwise undefined).
.TP
.PD 0
.B \-ec
.TP
.PD
.B \-e1c
These options are meaningless for SUIF.  They cause
.B sf2c
to emit a separate file for each uninitialized COMMON;
e.g. \f(CWCOMMON /ABC/\fR
yields \f(CWabc_com.c\fR, which defines \f(CWabc_\fR.
If the option is
.B \-e1c,
then bundle the separate files that would result from
.B \-ec
into the output file, along with comments giving a
.I sed (1)
script that separates the output file into the files that
.B \-ec
would give.
.TP
.B \-ext
Complain about f77 extensions.
.TP
.B \-i2
Do something similar to what is done by
.B \-I2,
but assume a modified
.I libF77
and
.I libI77
(compiled with -Df2c_i2), so INTEGER and LOGICAL variables may be
assigned in INQUIREs.  This is incompatible with the
.I \-I4
option.
.TP
.B \-R
Do not promote REAL functions and operations to DOUBLE PRECISION (as
.I f77
does).
.TP
.B \-r8
Promote REAL to DOUBLE PRECISION, COMPLEX to DOUBLE COMPLEX.
.TP
.BI \-T dir
Put temporary files in directory
.I dir
instead of /tmp, the default.  Note that the
.I TMPDIR
environment variable may also be used to choose the directory to use
for temporary files.  The
.B \-T
option overrides the
.I TMPDIR
environment variable if both are specified.
.TP
.BI \-w8
Do not complain when COMMON or EQUIVALENCE forces doubles to be
word-aligned.
.TP
.BI \-W n
Assume
.I n
characters/word (default 4) when initializing numeric variables with
character data.
.TP
.B \-z
Do not implicitly recognize DOUBLE COMPLEX.
.TP
.B !I
Disable the included statement.
.SH OPTIONS THAT ARE UNDOCUMENTED IN f77
.B \-66
Disallow all extensions to Fortran 66.
.TP
.B \-d
Turn on debugging mode.  Some effects of debugging mode are that
temporary intermediate files are not removed and some verbose
information is generated.
.TP
.B \-1
Undocumented synonym for the
.B \-onetrip
flag.
.TP
.B \-I4
Render INTEGER and LOGICAL as `long int'.  If this option is used,
neither of the options
.B \-I2
or
.B \-i2
may be used, as they contradict this option.
.TP
.B \-Is
Use `short int' as the type for array subscripts.
.TP
.BI \-O n
Set the maximum number of register variables to use.  For SUIF we
ignore whether the variable was declared ``register'' in C anyway, so
this option is meaningless.
.TP
.PD 0
.BI \-Nq n
.TP
.BI \-Nx n
.TP
.BI \-Ns n
.TP
.BI \-Nc n
.TP
.PD
.BI \-Nn n
Set various implementation size limits (internal table sizes).  These
flags set, respectively, limits on the number of equivalences (default
150), external symbols (default 200), statement labels (default 801),
nesting of loops or if-then-elses (default 20), and identifier names
(default 401).
.TP
.B \-p
Use #defines within each procedure to make references to common block
variables look to human readers like local variables.  For SUIF this
is irrelevant since the macros will have been expanded by the C
pre-processor before SUIF code is ever produced.  SUIF's Fortran mode
allows SUIF programs to see common block variables as locals.
.TP
.PD 0
.BI \-Fr format-string
.TP
.PD
.BI \-Fd format-string
Use the specified string to format, respectively, `float' or `double'
data written to the output C file.  The defaults are "(float)%s" (or
"%sf" if the
.B \-A
flag is used to specify ANSI-C) for `float' and "%.17g" for `double'.
.TP
.B \-ev
This option might mean to #define things in equivalences to look like
ordinary local variables to human readers of the C file.  If so, it is
irrelevant to SUIF since the C pre-processor substitutes for the
macros before the code is converted to SUIF.
.SH ENVIRONMENT VARIABLES
.TP
.B TMPDIR
This variable specifies the directory to use for temporary files
instead of the default /tmp directory.  See also the
.B \-T
option.
.SH SEE ALSO
.I scc,
.I snoot,
.I fixfortran,
.I porky,
and
.I f77.
.SH HISTORY
The version of
.I f2c
upon which
.I sf2c
is based calls itself, "VERSION 14 December 1989 7:55:33".  It was
adapted for an early version of SUIF by Dror Maydan.  Chris Wilson
made more modifications to get the information required by the current
SUIF system.
.PP
Parts of this document were taken from the man page distributed with
.I f2c.
