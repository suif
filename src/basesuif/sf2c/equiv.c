#include "defs.h"

LOCAL eqvcommon(), eqveqv(), nsubs();

/* ROUTINES RELATED TO EQUIVALENCE CLASS PROCESSING */

/* called at end of declarations section to process chains
   created by EQUIVALENCE statements
 */
/* DEM suboffset needs to know if doing equiv */ 
int Doing_Equiv = 0;
doequiv()
{
	register int i;
	int inequiv;			/* True if one namep occurs in
					   several EQUIV declarations */
	int comno;		/* Index into Extsym table of the last
				   COMMON block seen (implicitly assuming
				   that only one will be given) */
	int ovarno;
	ftnint comoffset;	/* Index into the COMMON block */
	ftnint offset;		/* Offset from array base */
	ftnint leng;
	register struct Equivblock *equivdecl;
	register struct Eqvchain *q;
	struct Primblock *primp;
	register Namep np;
	int k, k1, ns, pref, t;
	chainp cp;
	static int type_pref[NTYPES] = { 0, 0, 2, 4, 5, 7, 6, 8, 3, 1 };

/*DEM*/ Doing_Equiv = 1;

	for(i = 0 ; i < nequiv ; ++i)
	{

/* Handle each equivalence declaration */

		equivdecl = &eqvclass[i];
		equivdecl->eqvbottom = equivdecl->eqvtop = 0;
		comno = -1;



		for(q = equivdecl->equivs ; q ; q = q->eqvnextp)
		{
			offset = 0;
			primp = q->eqvitem.eqvlhs;
			vardcl(np = primp->namep);
			if(primp->argsp || primp->fcharp)
			{
				expptr offp, suboffset();

/* Pad ones onto the end of an array declaration when needed */

				if(np->vdim!=NULL && np->vdim->ndim>1 &&
				    nsubs(primp->argsp)==1 )
				{
					if(! ftn66flag)
						warni
			("1-dim subscript in EQUIVALENCE, %d-dim declared",
						    np -> vdim -> ndim);
					cp = NULL;
					ns = np->vdim->ndim;
					while(--ns > 0)
						cp = mkchain((char *)ICON(1), cp);
					primp->argsp->listp->nextp = cp;
				}

				offp = suboffset(primp);
				if(ISICON(offp))
					offset = offp->constblock.Const.ci;
/* DEM deal with semi-constants as well */
				else if (offp->addrblock.isaconst)
					offset = offp->addrblock.kludgeci;
				else	{
					dclerr
			("nonconstant subscript in equivalence ",
					    np);
					np = NULL;
				}
				frexpr(offp);
			}

/* Free up the primblock, since we now have a hash table (Namep) entry */

			frexpr(primp);

			if(np && (leng = iarrlen(np))<0)
			{
				dclerr("adjustable in equivalence", np);
				np = NULL;
			}

			if(np) switch(np->vstg)
			{
			case STGUNKNOWN:
			case STGBSS:
			case STGEQUIV:
				break;

			case STGCOMMON:

/* The code assumes that all COMMON references in a given EQUIVALENCE will
   be to the same COMMON block, and will all be consistent */

				comno = np->vardesc.varno;
				comoffset = np->voffset + offset;
				break;

			default:
				dclerr("bad storage class in equivalence", np);
				np = NULL;
				break;
			}

			if(np)
			{
				q->eqvoffset = offset;

/* eqvbottom   gets the largest difference between the array base address
   and the address specified in the EQUIV declaration */

				equivdecl->eqvbottom =
				    lmin(equivdecl->eqvbottom, -offset);

/* eqvtop   gets the largest difference between the end of the array and
   the address given in the EQUIVALENCE */

				equivdecl->eqvtop =
				    lmax(equivdecl->eqvtop, leng-offset);
			}
			q->eqvitem.eqvname = np;
		}

/* Now all equivalenced variables are in the hash table with the proper
   offset, and   eqvtop and eqvbottom   are set. */

		if(comno >= 0)

/* Get rid of all STGEQUIVS, they will be mapped onto STGCOMMON variables
   */

			eqvcommon(equivdecl, comno, comoffset);
		else for(q = equivdecl->equivs ; q ; q = q->eqvnextp)
		{
			if(np = q->eqvitem.eqvname)
			{
				inequiv = NO;
				if(np->vstg==STGEQUIV)
					if( (ovarno = np->vardesc.varno) == i)
					{

/* Can't EQUIV different elements of the same array */

						if(np->voffset + q->eqvoffset != 0)
							dclerr
			("inconsistent equivalence", np);
					}
					else	{
						offset = np->voffset;
						inequiv = YES;
					}

				np->vstg = STGEQUIV;
				np->vardesc.varno = i;
				np->voffset = - q->eqvoffset;

				if(inequiv)

/* Combine 2 equivalence declarations */

					eqveqv(i, ovarno, q->eqvoffset + offset);
			}
		}
	}

/* Now each equivalence declaration is distinct (all connections have been
   merged in eqveqv()), and some may be empty. */

	for(i = 0 ; i < nequiv ; ++i)
	{
		equivdecl = & eqvclass[i];
		if(equivdecl->eqvbottom!=0 || equivdecl->eqvtop!=0) {

/* a live chain */

			k = TYCHAR;
			pref = 1;
/*
 *  Changed by CSW for SUIF:
 *      There is a bug in the original f2c code here: a variable can
 *      occur on the list more than once if there was a loop in the
 *      equivalences that did not cause a contradiction.  In that
 *      case, equivdecl->eqvbottom would incorrectly be subtracted off
 *      that variable more than once.  To fix this problem, we add a
 *      flag to tell us when a variable has already been seen on this
 *      list.
 */
/* BEFORE SUIF changes: */
#if 0
			for(q = equivdecl->equivs ; q; q = q->eqvnextp)
			{
				np = q->eqvitem.eqvname;
#endif
/* AFTER SUIF changes: */
			for(q = equivdecl->equivs ; q; q = q->eqvnextp)
			{
				np = q->eqvitem.eqvname;
				np->veqvseen = 0;
			}
			for(q = equivdecl->equivs ; q; q = q->eqvnextp)
			{
				np = q->eqvitem.eqvname;
				if (np->veqvseen)
					continue;
				np->veqvseen = 1;
/* END SUIF changes */
				np->voffset -= equivdecl->eqvbottom;
				t = typealign[k1 = np->vtype];
				if (pref < type_pref[k1]) {
					k = k1;
					pref = type_pref[k1];
					}
				if(np->voffset % t != 0)
					dclerr("bad alignment forced by equivalence", np);
			}
			equivdecl->eqvtype = k;
		}
		freqchain(equivdecl);
	}
/*DEM*/ Doing_Equiv = 0;
}





/* put equivalence chain p at common block comno + comoffset */

LOCAL eqvcommon(p, comno, comoffset)
struct Equivblock *p;
int comno;
ftnint comoffset;
{
	int ovarno;
	ftnint k, offq;
	register Namep np;
	register struct Eqvchain *q;

	if(comoffset + p->eqvbottom < 0)
	{
		errstr("attempt to extend common %s backward",
		    extsymtab[comno].fextname);
		freqchain(p);
		return;
	}

	if( (k = comoffset + p->eqvtop) > extsymtab[comno].extleng)
		extsymtab[comno].extleng = k;


	for(q = p->equivs ; q ; q = q->eqvnextp)
		if(np = q->eqvitem.eqvname)
		{
			switch(np->vstg)
			{
			case STGUNKNOWN:
			case STGBSS:
				np->vstg = STGCOMMON;
				np->vcommequiv = 1;
				np->vardesc.varno = comno;

/* np -> voffset   will point to the base of the array */

				np->voffset = comoffset - q->eqvoffset;
				break;

			case STGEQUIV:
				ovarno = np->vardesc.varno;

/* offq   will point to the current element, even if it's in an array */

				offq = comoffset - q->eqvoffset - np->voffset;
				np->vstg = STGCOMMON;
				np->vcommequiv = 1;
				np->vardesc.varno = comno;

/* np -> voffset   will point to the base of the array */

				np->voffset = comoffset - q->eqvoffset;
				if(ovarno != (p - eqvclass))
					eqvcommon(&eqvclass[ovarno], comno, offq);
				break;

			case STGCOMMON:
				if(comno != np->vardesc.varno ||
				    comoffset != np->voffset+q->eqvoffset)
					dclerr("inconsistent common usage", np);
				break;


			default:
				badstg("eqvcommon", np->vstg);
			}
		}

	freqchain(p);
	p->eqvbottom = p->eqvtop = 0;
}


/* Move all items on ovarno chain to the front of   nvarno   chain.
 * adjust offsets of ovarno elements and top and bottom of nvarno chain
 */

LOCAL eqveqv(nvarno, ovarno, delta)
int ovarno, nvarno;
ftnint delta;
{
	register struct Equivblock *neweqv, *oldeqv;
	register Namep np;
	struct Eqvchain *q, *q1;

	neweqv = eqvclass + nvarno;
	oldeqv = eqvclass + ovarno;
	neweqv->eqvbottom = lmin(neweqv->eqvbottom, oldeqv->eqvbottom - delta);
	neweqv->eqvtop = lmax(neweqv->eqvtop, oldeqv->eqvtop - delta);
	oldeqv->eqvbottom = oldeqv->eqvtop = 0;

	for(q = oldeqv->equivs ; q ; q = q1)
	{
		q1 = q->eqvnextp;
		if( (np = q->eqvitem.eqvname) && np->vardesc.varno==ovarno)
		{
			q->eqvnextp = neweqv->equivs;
			neweqv->equivs = q;
			q->eqvoffset -= delta;
			np->vardesc.varno = nvarno;
			np->voffset -= delta;
		}
		else	free( (charptr) q);
	}
	oldeqv->equivs = NULL;
}




freqchain(p)
register struct Equivblock *p;
{
	register struct Eqvchain *q, *oq;

	for(q = p->equivs ; q ; q = oq)
	{
		oq = q->eqvnextp;
		free( (charptr) q);
	}
	p->equivs = NULL;
}





/* nsubs -- number of subscripts in this arglist (just the length of the
   list) */

LOCAL nsubs(p)
register struct Listblock *p;
{
	register int n;
	register chainp q;

	n = 0;
	if(p)
		for(q = p->listp ; q ; q = q->nextp)
			++n;

	return(n);
}
