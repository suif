/* file "annotate_all.cc" */

/*  Copyright (c) 1997 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for annotate_all.
 */

#define RCS_BASE_FILE annotate_all_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: annotate_all.cc,v 1.1.1.1 1998/06/16 15:17:00 brm Exp $")

INCLUDE_SUIF_COPYRIGHT


static const char *k_all_number;


static void usage(void);
static void annotate_all_on_object(suif_object *the_object);


extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    ANNOTE(k_all_number, "all number", TRUE);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    walk(fileset->globals(), &annotate_all_on_object);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        walk(fse->symtab(), &annotate_all_on_object);
        annotate_all_on_object(fse);
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = fse->next_proc();
            if (this_proc == NULL)
                break;
            this_proc->read_proc(TRUE, FALSE);
            walk(this_proc->block(), &annotate_all_on_object);
            this_proc->write_proc(fse);
            this_proc->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }


static void usage(void)
  {
    fprintf(stderr,
            "usage: %s [options] <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void annotate_all_on_object(suif_object *the_object)
  {
    static i_integer next_num = 1;

    if (the_object->is_tree_obj())
      {
        tree_node *the_node = (tree_node *)the_object;
        if (the_node->is_instr())
            return;
      }
    the_object->append_annote(k_all_number,
                              new immed_list(ii_to_immed(next_num)));
    ++next_num;
  }
