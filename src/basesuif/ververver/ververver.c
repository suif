/* file "ververver.c" */

/* by Chris Wilson <cwilson@cs.stanford.edu> */

/*
 *  This is a software package version compatibility system.
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define INITIAL_WORD_BUF_SIZE 20
#define INITIAL_LINE_BUF_SIZE 20

typedef int boolean;
#define TRUE 1
#define FALSE 0

static char *prog_name;
static char *this_package_name;
static char *data_file;
static char *installed_data_dir;
static char *current_data_file;

static void usage(void);
static void do_check(void);
static void do_install(void);
static void do_deinstall(void);
static boolean pack_ver_is_installed(char *pack_ver_string);
static boolean is_installed(char *package_name, char *version);
static char **read_line(FILE *fp);
static void deallocate_line(char **line);
static char *data_for_package(char *package_name);

extern int main(int argc, char **argv)
  {
    char *flag;

    prog_name = argv[0];
    if (argc != 5)
      {
        usage();
        exit(1);
      }
    flag = argv[1];
    this_package_name = argv[2];
    data_file = argv[3];
    installed_data_dir = argv[4];
    if (strcmp(flag, "-check") == 0)
      {
        do_check();
      }
    else if (strcmp(flag, "-install") == 0)
      {
        do_install();
      }
    else if (strcmp(flag, "-deinstall") == 0)
      {
        do_deinstall();
      }
    else
      {
        usage();
        exit(1);
      }
    return 0;
  }

static void usage(void)
  {
    fprintf(stderr,
            "usage:  %s { -check | -install | -deinstall } "
                     "<package-name>\n"
            "                  <data-file> <installed-data-dir>\n", prog_name);
  }

static void do_check(void)
  {
    boolean failed = FALSE;
    FILE *fp;

    if (is_installed(this_package_name, NULL))
      {
        fprintf(stderr,
                "%s: A version of package `%s' is already installed;\n",
                prog_name, this_package_name);
        fprintf(stderr,
                "%s: it must be removed before a new version can be "
                "installed.\n", prog_name);
        failed = TRUE;
      }
    fp = fopen(data_file, "r");
    if (fp == NULL)
      {
        fprintf(stderr, "%s: unable to open data file `%s'\n", prog_name,
                data_file);
        exit(1);
      }
    while (TRUE)
      {
        char **line;

        current_data_file = data_file;
        line = read_line(fp);
        if (line == NULL)
          {
            if (ferror(fp))
              {
                fprintf(stderr, "%s: error reading data file `%s'\n",
                        prog_name, data_file);
                exit(1);
              }
            fclose(fp);
            if (failed)
                exit(1);
            return;
          }
        if (strcmp(line[0], "incompatibility:") == 0)
          {
            char **follow = &(line[1]);

            while (*follow != NULL)
              {
                if (pack_ver_is_installed(*follow))
                  {
                    fprintf(stderr,
                            "%s: Package `%s' is incompatible with `%s'.\n",
                            prog_name, *follow, this_package_name);
                    failed = TRUE;
                  }
                ++follow;
              }
          }
        else if (strcmp(line[0], "prerequisite:") == 0)
          {
            char **follow = &(line[1]);

            while (*follow != NULL)
              {
                if (!pack_ver_is_installed(*follow))
                  {
                    fprintf(stderr,
                            "%s: Package `%s' is a prerequisite for `%s',\n",
                            prog_name, *follow, this_package_name);
                    fprintf(stderr, "%s: but it is not currently installed.\n",
                            prog_name);
                    failed = TRUE;
                  }
                ++follow;
              }
          }
        deallocate_line(line);
      }
  }

static void do_install(void)
  {
    FILE *data_fp;
    FILE *output_fp;
    char *output_name;

    do_check();
    data_fp = fopen(data_file, "r");
    if (data_fp == NULL)
      {
        fprintf(stderr, "%s: unable to open data file `%s'\n", prog_name,
                data_file);
        exit(1);
      }
    output_name = data_for_package(this_package_name);
    output_fp = fopen(output_name, "w");
    if (output_fp == NULL)
      {
        fprintf(stderr, "%s: unable to create data file `%s'\n", prog_name,
                output_name);
        exit(1);
      }
    while (TRUE)
      {
        char **line;

        current_data_file = data_file;
        line = read_line(data_fp);
        if (line == NULL)
          {
            if (ferror(data_fp))
              {
                fprintf(stderr, "%s: error reading data file `%s'\n",
                        prog_name, data_file);
                exit(1);
              }
            fclose(data_fp);
            if (ferror(output_fp))
              {
                fprintf(stderr, "%s: error writing data file `%s'\n",
                        prog_name, output_name);
                exit(1);
              }
            fclose(output_fp);
            free(output_name);
            return;
          }
        if (strcmp(line[0], "compatibility:") == 0)
          {
            char **follow = &(line[1]);

            while (*follow != NULL)
              {
                fprintf(output_fp, "%s\n", *follow);
                ++follow;
              }
          }
        deallocate_line(line);
      }
  }

static void do_deinstall(void)
  {
    char *filename;

    filename = data_for_package(this_package_name);
    remove(filename);
    free(filename);
  }

static boolean pack_ver_is_installed(char *pack_ver_string)
  {
    char *name;
    char *slash_position;
    boolean result;

    slash_position = strchr(pack_ver_string, '/');
    if (slash_position == NULL)
        return is_installed(pack_ver_string, NULL);
    name = malloc((slash_position - pack_ver_string) + 1);
    strncpy(name, pack_ver_string, (slash_position - pack_ver_string));
    name[slash_position - pack_ver_string] = 0;
    result = is_installed(name, slash_position + 1);
    free(name);
    return result;
  }

static boolean is_installed(char *package_name, char *version)
  {
    char *filename_buffer = NULL;
    FILE *fp;

    filename_buffer = data_for_package(package_name);
    fp = fopen(filename_buffer, "r");
    if (fp == NULL)
      {
        free(filename_buffer);
        return FALSE;
      }
    else
      {
        if (version == NULL)
          {
            fclose(fp);
            free(filename_buffer);
            return TRUE;
          }
        current_data_file = filename_buffer;
        while (TRUE)
          {
            char **line;
            char **follow;

            line = read_line(fp);
            if (line == NULL)
              {
                if (ferror(fp))
                  {
                    fprintf(stderr, "%s: error reading data file `%s'\n",
                            prog_name, filename_buffer);
                    exit(1);
                  }
                fclose(fp);
                free(filename_buffer);
                current_data_file = NULL;
                return FALSE;
              }
            follow = line;
            while (*follow != NULL)
              {
                if (strcmp(*follow, version) == 0)
                  {
                    deallocate_line(line);
                    if (ferror(fp))
                      {
                        fprintf(stderr, "%s: error reading data file `%s'\n",
                                prog_name, filename_buffer);
                        exit(1);
                      }
                    fclose(fp);
                    free(filename_buffer);
                    current_data_file = NULL;
                    return TRUE;
                  }
                ++follow;
              }
            deallocate_line(line);
          }
      }
  }

static char **read_line(FILE *fp)
  {
    static char **line_buffer;
    static unsigned long line_buf_size;
    int inchar;
    unsigned long word_num;
    char **result;

    inchar = fgetc(fp);
    while (isspace(inchar))
        inchar = fgetc(fp);
    while (inchar == '#')
      {
        while (inchar != '\n')
            inchar = fgetc(fp);
        while (isspace(inchar))
            inchar = fgetc(fp);
      }
    if (inchar == EOF)
        return NULL;
    if (line_buffer == NULL)
      {
        line_buf_size = INITIAL_LINE_BUF_SIZE;
        line_buffer = malloc(line_buf_size * sizeof(char *));
      }
    word_num = 0;
    do
      {
        static char *word_buffer = NULL;
        static unsigned long word_buf_size;
        unsigned long position;
        char *this_word;

        if (word_buffer == NULL)
          {
            word_buf_size = INITIAL_WORD_BUF_SIZE;
            word_buffer = malloc(word_buf_size);
          }
        position = 0;
        do
          {
            if (!isprint(inchar))
              {
                fprintf(stderr,
                        "%s: non-printing character in data file `%s'\n",
                        prog_name, current_data_file);
                exit(1);
              }
            word_buffer[position] = inchar;
            ++position;
            if (position == word_buf_size)
              {
                char *new_buffer;

                word_buf_size *= 2;
                new_buffer = malloc(word_buf_size);
                memcpy(new_buffer, word_buffer, position);
                free(word_buffer);
                word_buffer = new_buffer;
              }
            inchar = fgetc(fp);
          } while ((!isspace(inchar)) && (inchar != EOF));
        this_word = malloc(position + 1);
        memcpy(this_word, word_buffer, position);
        this_word[position] = 0;
        line_buffer[word_num] = this_word;
        ++word_num;
        if (word_num == line_buf_size)
          {
            char **new_buffer;

            line_buf_size *= 2;
            new_buffer = malloc(line_buf_size * sizeof(char *));
            memcpy(new_buffer, line_buffer, word_num * sizeof(char *));
            free(line_buffer);
            line_buffer = new_buffer;
          }
        while (isspace(inchar) && (inchar != '\n'))
            inchar = fgetc(fp);
      } while ((inchar != '\n') && (inchar != EOF));
    result = malloc((word_num + 1) * sizeof(char *));
    memcpy(result, line_buffer, word_num * sizeof(char *));
    result[word_num] = NULL;
    return result;
  }

static void deallocate_line(char **line)
  {
    char **follow = line;
    while (*follow != NULL)
      {
        free(*follow);
        ++follow;
      }
    free(line);
  }

static char *data_for_package(char *package_name)
  {
    char *result;

    result = malloc(strlen(installed_data_dir) + 1 + strlen(package_name) + 1);
    strcpy(result, installed_data_dir);
    strcat(result, "/");
    strcat(result, package_name);
    return result;
  }
