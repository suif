/* file "linksuif.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*  SUIF Linker */
/* This program will read a set of SUIF files and create a single
 * common symbol table that can be shared by them all.  It will then
 * write out new object files which use that common symbol table.
 * When creating a common symbol table it attempts as much as possible
 * to include a single unique copy of each type definition, and then
 * makes all code point to that unique definition.
 *
 * To Do (ie, bugs):
 *
 * - This program is extremely slow when given large input files.  I
 * suspect that the main problem is that everything is done using
 * linked lists.  Using hash tables instead of linked lists for symbol
 * table lookup would probably improve performance dramatically.
 *
 * - This can now sucessfully link the gcc SPEC95 benchmark.  But it
 * probably still has bugs.  More extensive testing needs to be done.
 *
 * - I have no idea how to handle the case where a structure is
 * defined differently in different object files.  Right now I just
 * issue a warning and duplicate the structure.  There may be a better
 * way of handling this.
 */

#define RCS_BASE_FILE linksuif_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>
#include <ctype.h>

RCS_BASE(
    "$Id: linksuif.cc,v 1.3 1999/08/25 16:51:34 suifoid Exp $")

INCLUDE_SUIF_COPYRIGHT


/*
 *  We have a special linker class to be friends with some things in
 *  the SUIF library to allow the library to specify functions only
 *  the linker is allowed to use.
 */
class suif_linker
  {
public:
    void set_fse_symtab(file_symtab *the_file_symtab, file_set_entry *the_fse)
      {
        the_file_symtab->set_fse(the_fse);
        the_fse->set_symtab(the_file_symtab);
      }
    void set_fse_id(file_set_entry *the_fse, int new_id)
      {
        the_fse->set_file_id(new_id);
      }
  };

struct field_rename
  {
    const char *old_name;
    const char *new_name;
    struct_type *the_type;

    boolean operator==(const field_rename &other) const
      {
        return ((old_name == other.old_name) &&
                (new_name == other.new_name) && (the_type == other.the_type));
      }
  };

DECLARE_LIST_CLASS(field_rename_list, field_rename);

static const char *k_linksuif_replacement;
static const char *k_linksuif_temp_link;
static const char *k_linksuif_temp_link_compat;
static const char *k_linksuif_temp_mark;
static const char *k_linksuif_temp_cast;
static suif_linker the_suif_linker;
static boolean errors_found = FALSE;
static func_type *temp_func_type = NULL;
static type_node *temp_var_type = NULL;
static sym_node_list *cast_list = NULL;



static void usage(void);
static void link_files(int num_files, char **in_names, char **out_names);
static replacements *global_translation(global_symtab *old_symtab,
                                        global_symtab *new_symtab);
static type_node *type_translation(type_node *old_type,
                                   global_symtab *new_symtab);
static void fix_translated_type(sym_node *old_symbol,
                                global_symtab *new_symtab);
static void merge_common_initializers(var_def *def1, var_def *def2);
static void translate_annotes(suif_object *new_object, suif_object *old_object,
                              global_symtab *new_symtab);
static sym_node *symbol_translation(sym_node *old_symbol,
                                    global_symtab *new_symtab);
static var_sym *var_translation(var_sym *old_var, global_symtab *new_symtab);
static proc_sym *proc_translation(proc_sym *old_proc,
                                  global_symtab *new_symtab);
static array_bound bound_translation(array_bound old_bound,
                                     global_symtab *new_symtab);
static boolean isomorphic_types(type_node *type1, type_node *type2,
				boolean ignore_zero_length_structs);
static boolean isomorphic_bounds(array_bound bound1, array_bound bound2);
static boolean isomorphic_annotes(suif_object *object1, suif_object *object2);
static boolean isomorphic_vars(var_sym *var1, var_sym *var2);
static void remove_temp_links(type_node *the_type);
static type_node *composite_type(type_node *type1, type_node *type2);
static array_bound composite_bound(array_bound bound1, array_bound bound2,
                                   boolean *success);
static modifier_type *qualify_with(modifier_type *old_modifiers,
                                   type_node *new_type);
static boolean possibly_compatible_types(type_node *type1, type_node *type2);
static void add_proc_casts(tree_proc *the_proc);
static void add_node_casts(tree_node *the_node, void *);
static void add_instr_casts(instruction *the_instr, void *);
static void mark_type_unique(type_node *the_type);
static type_node *merge_common_types(type_node *type1, type_node *type2,
                                     field_rename_list *rename_list);
static void fix_fields_on_proc(tree_proc *the_proc,
                               field_rename_list *rename_list);
static void fix_fields_on_node(tree_node *the_node, void *data);
static void fix_fields_on_instr(instruction *the_instr, void *data);
static boolean is_anonymous(type_node *the_type);
static boolean has_cast(sym_node *the_sym, type_node **old_type);
static void enter_cast(sym_node *the_sym, type_node *old_type);
static void init_casts(void);
static void clear_casts(void);
static boolean casts_found(void);


extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    /* When we translate a type in an object file to point to a type
     * in our linked (global) symbol table, record that mapping in an
     * annotation so we can use it quickly later on: */
    ANNOTE(k_linksuif_replacement, "linksuif replacement", FALSE);

    /* When comparing a type in an object file with a type in our
     * linked (global) symbol table we need to keep track of sets of
     * what appear to be equivalent types.  This link is used for that
     * purpose: */
    ANNOTE(k_linksuif_temp_link,   "linksuif temp link",   FALSE);

    /* When we traverse type tables looking for compatible types for
     * function call arguments we need to track pairs of compatible
     * types.  This is used for that purpose: */
    ANNOTE(k_linksuif_temp_link_compat,"linksuif temp link compat",FALSE);

    /* Used to mark a type so that it is definately unique and will be
     * inserted into the new type table, rather than just merged into
     * an already existing type: */
    ANNOTE(k_linksuif_temp_mark,   "linksuif temp mark",   FALSE);
    ANNOTE(k_linksuif_temp_cast,   "linksuif test cast",   FALSE);

    int num_files = (argc - 1) / 2;
    char **in_names = new char *[num_files];
    char **out_names = new char *[num_files];

    for (int file_num = 0; file_num < num_files; ++file_num)
      {
        in_names[file_num] = argv[file_num * 2 + 1];
        out_names[file_num] = argv[file_num * 2 + 2];
      }

    link_files(num_files, in_names, out_names);

    delete[] in_names;
    delete[] out_names;

    exit_suif();

    if (errors_found)
        return 1;
    else
        return 0;
  }


static void usage(void)
  {
    fprintf(stderr, "Usage: %s infile1 outfile1 infile2 outfile2 ...\n",
            _suif_program_name);
    exit(1);
  }

static void link_files(int num_files, char **in_names, char **out_names)
  {
    file_set *output_file_set = fileset;
    fileset = NULL;

    long max_open_out_files = output_file_set->get_max_open_files();
    assert(max_open_out_files > 0);
    long max_open_in_files = 1;
    if (max_open_out_files > num_files)
        max_open_in_files = max_open_out_files - num_files;
    max_open_out_files -= max_open_in_files;
    output_file_set->set_max_open_files(max_open_out_files);

    machine_params save_target;

    /*
     *  First, go through all the files and put symbols in the new
     *  global symtab for each symbol that is defined and set their
     *  types to match the definitions.  Then start back over all the
     *  files.  This insures that the type on the definition will take
     *  precedence.
     */

    file_set **input_file_sets;
    input_file_sets = new file_set *[num_files];

    /******************************************************************/
    /* Pass 1: Link function and variable definitions.		      */

    field_rename_list field_renames;
    /* First look for definitions of symbols: */
    int file_num;
    for (file_num = 0; file_num < num_files; ++file_num)
      {
        fileset = new file_set;
        input_file_sets[file_num] = fileset;

        fileset->set_max_open_files(1);

        file_set_entry *in_fse = fileset->add_file(in_names[file_num], NULL);

        if (file_num == 0)
          {
            save_target = target;
          }
        else
          {
            if (save_target != target)
              {
                error_line(1, NULL,
                           "files %s and %s are for different target machines",
                           in_names[0], in_names[file_num]);
              }
          }

        in_fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = in_fse->next_proc();
            if (this_proc == NULL)
                break;

            if (this_proc->parent()->kind() != SYMTAB_GLOBAL)
                continue;

            proc_sym *new_proc_sym =
                    proc_translation(this_proc, output_file_set->globals());
            func_type *new_func_type =
                    (func_type *)type_translation(this_proc->type(),
                                                  output_file_set->globals());
            new_proc_sym->set_type(new_func_type);
          }

        var_def_list_iter def_iter(in_fse->symtab()->var_defs());
        while (!def_iter.is_empty())
          {
            var_def *this_def = def_iter.step();
            var_sym *this_var = this_def->variable();
            if (this_var->parent()->kind() != SYMTAB_GLOBAL)
                continue;

            var_sym *new_var =
                    var_translation(this_var, output_file_set->globals());

            type_node *new_type =
                    type_translation(this_var->type(),
                                     output_file_set->globals());

            if (is_common(this_var))
              {
                if (is_common(new_var))
                  {
                    if (new_var->type() != new_type)
                      {
                        type_node *merged_type =
                                merge_common_types(new_var->type(), new_type,
                                                   &field_renames);
                        new_var->set_type(merged_type);
                      }
                  }
                else
                  {
                    new_var->set_type(new_type);
                    new_var->append_annote(k_common_block, new immed_list);
                  }
              }
            else
              {
                new_var->set_type(new_type);
              }
          }

        if ((file_num >= max_open_in_files - 1) &&
            (num_files != max_open_in_files))
          {
            fileset->set_max_open_files(0);
          }

        fileset = NULL;
      }

    /******************************************************************/
    /* Pass 2: Merge symbol tables and translate functions to use the */
    /*         new symbol table.                                      */

    for (file_num = 0; file_num < num_files; ++file_num)
      {
        fileset = input_file_sets[file_num];
        file_set_entry *in_fse = fileset->file_list()->head()->contents;

        if (max_open_out_files == 0)
            output_file_set->set_max_open_files(1);

        /*
         *  Make sure all the predefined types are pointing to things
         *  in the output file_set when writing, because some of these
         *  types are used by low-level I/O.  The add_file() method
         *  writes some header information that uses some of these
         *  types.
         */
        output_file_set->globals()->predefine_types();

        file_set_entry *out_fse =
                output_file_set->add_file(NULL, out_names[file_num]);
        the_suif_linker.set_fse_id(out_fse, file_num + 1);

        if (max_open_out_files == 0)
            output_file_set->set_max_open_files(0);

        fileset->set_max_open_files(1);

        init_casts();

        replacements *the_replacements =
                global_translation(fileset->globals(),
                                   output_file_set->globals());

        file_symtab *old_file_symtab = out_fse->symtab();
        file_symtab *the_file_symtab = in_fse->symtab();
        the_file_symtab->clone_helper(the_replacements, TRUE);
        the_suif_linker.set_fse_symtab(the_file_symtab, out_fse);
        fileset->globals()->remove_child(the_file_symtab);
        output_file_set->globals()->add_child(the_file_symtab);

        /*
         *  A new k_history annotation gets added to both the input
         *  and output file set entries by the library; obviously we
         *  only want one of them to survive.  The one on the input
         *  file set entry is already in the correct position at the
         *  end of the other k_history annotations, so just get rid of
         *  the one already on the output file set entry before
         *  translating the other annotations onto it.
         */
        annote *output_hist_annote = out_fse->annotes()->get_annote(k_history);
        delete output_hist_annote;
        translate_annotes(out_fse, in_fse, output_file_set->globals());

        in_fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc = in_fse->next_proc();
            if (this_proc == NULL)
                break;

            /*
             *  Make sure all the predefined types are pointing to
             *  things in the input file_set when reading, because
             *  some of these types are used by low-level I/O.
             */
            fileset->globals()->predefine_types();

            /*
             *  When reading a procedure, symbol and type ids will be
             *  searched for through the symbol table tree, and some
             *  will be in the old inter-file symbol table, so we must
             *  temporarily put the file symbol table back under this
             *  old symtab.
             */
            output_file_set->globals()->remove_child(the_file_symtab);
            fileset->globals()->add_child(the_file_symtab);
            this_proc->read_proc(FALSE, FALSE);
            fileset->globals()->remove_child(the_file_symtab);
            output_file_set->globals()->add_child(the_file_symtab);

            tree_proc *this_block = this_proc->block();
            fix_fields_on_proc(this_block, &field_renames);
            this_block->clone_helper(the_replacements, TRUE);

            if (casts_found())
                add_proc_casts(this_block);

            if (!this_proc->parent()->is_file())
              {
                proc_sym *new_proc_sym =
                        proc_translation(this_proc,
                                         output_file_set->globals());
                if (new_proc_sym->file() != NULL)
                  {
                    error_line(1, NULL, "procedure %s multiply defined",
                               new_proc_sym->name());
                  }
                new_proc_sym->set_block(this_block);
                this_proc->set_block(NULL);
                this_proc = new_proc_sym;
              }

            func_type *this_func_type = this_proc->type();
            if (this_func_type->args_known())
              {
                unsigned arg_count = this_func_type->num_args();
                proc_symtab *this_proc_symtab = this_block->proc_syms();
                sym_node_list_e *follow = this_proc_symtab->params()->head();
                for (unsigned arg_num = 0; arg_num < arg_count; ++arg_num)
                  {
                    if (follow == NULL)
                      {
                        error_line(1, this_block,
                                   "not enough formals for type");
                      }
                    assert(follow->contents->is_var());
                    var_sym *this_param = (var_sym *)(follow->contents);
                    if (this_param->type() !=
                        this_func_type->arg_type(arg_num))
                      {
                        var_sym *dummy =
                                this_proc_symtab->new_unique_var(
                                        this_func_type->arg_type(arg_num),
                                        this_param->name());
                        dummy->set_param();
                        follow->contents = dummy;
                        this_param->reset_param();
                        this_block->body()->push(assign(this_param,
                                cast_op(dummy, this_param->type()->unqual())));
                      }
                    follow = follow->next();
                  }
                if (follow != NULL)
                    error_line(1, this_block, "too many formals for type");
              }

            /*
             *  Make sure all the predefined types are pointing to
             *  things in the output file_set when writing, because
             *  some of these types are used by low-level I/O.
             */
            output_file_set->globals()->predefine_types();

            if (max_open_out_files == 0)
              {
                fileset->set_max_open_files(0);
                output_file_set->set_max_open_files(1);
              }

            this_proc->write_proc(out_fse);
            this_proc->flush_proc();

            if (max_open_out_files == 0)
              {
                output_file_set->set_max_open_files(0);
                fileset->set_max_open_files(1);
              }
          }

        clear_casts();

        /*
         *  We want the empty file_symtab to be deleted, not the one
         *  with all the information and which is now attached to the
         *  output file_set.
         */
        the_suif_linker.set_fse_symtab(old_file_symtab, in_fse);

        delete the_replacements;

        delete fileset;
        fileset = NULL;
      }

    while (!field_renames.is_empty())
        field_renames.pop();

    fileset = output_file_set;

    /*
     *  Make sure the predefined types are pointing to the remaining
     *  fileset, as they will be used later when writing out the
     *  global symbol tables.
     */
    fileset->globals()->predefine_types();

    if (max_open_out_files == 0)
        fileset->set_max_open_files(1);

    delete[] input_file_sets;
  }

static replacements *global_translation(global_symtab *old_symtab,
                                        global_symtab *new_symtab)
  {
    replacements *the_replacements = new replacements;

    assert(old_symtab->var_defs()->is_empty());

    sym_node_list_iter sym_iter(old_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *old_sym = sym_iter.step();
        fix_translated_type(old_sym, new_symtab);
        sym_node *new_sym = symbol_translation(old_sym, new_symtab);
        the_replacements->newsyms.append(new_sym);
        the_replacements->oldsyms.append(old_sym);
      }

    type_node_list_iter type_iter(old_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *old_type = type_iter.step();
        type_node *new_type = type_translation(old_type, new_symtab);
        the_replacements->newtypes.append(new_type);
        the_replacements->oldtypes.append(old_type);
      }

    new_symtab->number_globals();

    return the_replacements;
  }

/*
 *  The interesting problem here is dealing with recursion among
 *  types, either through structures or annotations.  We only want to
 *  merge sets of connected structures that are exactly the same --
 *  the graph structures must be isomorphic and the mapping must be
 *  bijective.  The algorithm we use to check this is to walk both
 *  graphs simultaneously and put temporary annotations on both old
 *  and new types pointing to the corresponding type in the other
 *  symbol table.  When we reach something that already has such an
 *  annotation, we check and see that the two types both have such
 *  annotations and that they point to one another.  We have to leave
 *  these annotations on everywhere until the entire graph has been
 *  walked, and then we walk the graph again to remove these temporary
 *  annotations.
 */
static type_node *type_translation(type_node *old_type,
                                   global_symtab *new_symtab)
  {

    /* First check to see if we have already done this particular
     * translation and know what it should be replaced with.  If so,
     * just return it immediately: */
    immed_list *replacement_immeds =
            (immed_list *)(old_type->peek_annote(k_linksuif_replacement));
    if (replacement_immeds != NULL)
      {
        assert(replacement_immeds->count() == 1);
        immed first_immed = replacement_immeds->head()->contents;
        assert(first_immed.is_type());
        return first_immed.type();
      }

    /* Before we see if there is an identical type in the new symbol
     * table, first merge in any new information about the type which
     * this version gives us: */

    /* Now check to see if this type is absolutely identical to
     * something already in the new symbol table.  If so, mark this
     * translation for future reference and return it. */
    type_node_list_iter new_types(new_symtab->types());
    while (!new_types.is_empty())
      {
        type_node *this_new_type = new_types.step();
        boolean found = isomorphic_types(this_new_type, old_type, FALSE);
        remove_temp_links(old_type);
        if (found)
          {
            replacement_immeds = new immed_list;
            replacement_immeds->append(immed(this_new_type));
            old_type->append_annote(k_linksuif_replacement,
                                    replacement_immeds);
            return this_new_type;
          }
      }

    /* Now check to see if this type is identical except for undefined
     * structures to something already in the new symbol table. If so,
     * we have to merge in the structure definitions and then we are
     * done. */
    new_types.reset(new_symtab->types());
    while (!new_types.is_empty())
      {
        type_node *this_new_type = new_types.step();
        boolean found = isomorphic_types(this_new_type, old_type, TRUE);
        remove_temp_links(old_type);
        if (found)
          {
            replacement_immeds = new immed_list;
            replacement_immeds->append(immed(this_new_type));
            old_type->append_annote(k_linksuif_replacement,
                                    replacement_immeds);

	    switch (old_type->op())
	      {
	        case TYPE_PTR:
		  {
		    ptr_type *old_ptr = (ptr_type *)old_type;
		    type_node *trans = type_translation(old_ptr->ref_type(),
							new_symtab);
		    assert(this_new_type->op() == TYPE_PTR);
		    assert(trans == ((ptr_type *)this_new_type)->ref_type());
		    break;
		  }
	        case TYPE_ARRAY:
		  {
		    array_type *old_array = (array_type *)old_type;
		    type_node *trans = type_translation(old_array->elem_type(),
							new_symtab);
		    assert(this_new_type->op() == TYPE_ARRAY);
		    assert(trans == ((array_type *)this_new_type)->
			   elem_type());
		    break;
		  }
	        case TYPE_FUNC:
		  {
		    func_type *new_func = (func_type *)this_new_type;
		    func_type *old_func = (func_type *)old_type;
		    
		    type_node *trans = type_translation(
			old_func->return_type(), new_symtab);
		    assert(this_new_type->op() == TYPE_FUNC);
		    assert(trans == ((func_type *)this_new_type)->
			   return_type());

		    assert(old_func->args_known() == new_func->args_known());
		    
		    if (old_func->args_known())
		      {
			unsigned num_args = old_func->num_args();
			assert(new_func->num_args() == old_func->num_args());

			for (unsigned arg_num = 0; arg_num < num_args;
			     ++arg_num)
			  {
			    type_node *new_arg =
				type_translation(old_func->arg_type(arg_num),
						 new_symtab);
			    assert(new_arg == new_func->arg_type(arg_num));
			  }
		      }
		    break;
		  }
	        case TYPE_GROUP:
	        case TYPE_STRUCT:
	        case TYPE_UNION:
		  {
		    struct_type *old_struct = (struct_type *)old_type;
		    struct_type *new_struct = (struct_type *)this_new_type;
		    assert(this_new_type->is_struct());
		    assert(strcmp(old_struct->name(),
				  new_struct->name()) == 0);
		    
		    /* If the new type is empty, insert the old type's
		     * fields into it: */
		    if(new_struct->size() == 0)
		      {
			assert(old_struct->size() != 0);
			
			unsigned num_fields = old_struct->num_fields();
			new_struct->set_size(old_struct->size());
			new_struct->set_num_fields(num_fields);
			for (unsigned field_num = 0;
			     field_num < num_fields; ++field_num)
			  {
			    new_struct->set_field_name(
				field_num,
				old_struct->field_name(field_num));
			    type_node *new_field_type =
				type_translation(
				    old_struct->field_type(field_num),
				    new_symtab);
			    new_struct->set_field_type(field_num,
						       new_field_type);
			    new_struct->set_offset(
				field_num,
				old_struct->offset(field_num));
			  }
		      }
		    else if(old_struct->size() != 0)
		      {
			assert(old_struct->size() == new_struct->size());
			unsigned num_fields = old_struct->num_fields();
			assert(num_fields == new_struct->num_fields());
			
			for (unsigned field_num = 0;
			     field_num < num_fields; ++field_num)
			  {
			      assert(strcmp(old_struct->field_name(field_num),
					    new_struct->field_name(field_num))
				     == 0);
			      type_node *new_field_type =
				  type_translation(
				      old_struct->field_type(field_num),
				      new_symtab);
			      assert(new_struct->field_type(field_num) ==
				     new_field_type);
			      assert(old_struct->offset(field_num) ==
				     new_struct->offset(field_num));
			  }
		      }
		    break;
		  }
	        case TYPE_CONST:
	        case TYPE_VOLATILE:
	        case TYPE_CALL_BY_REF:
	        case TYPE_NULL:
		  {
		    modifier_type *old_modifier = (modifier_type *)old_type;
		    type_node *trans = type_translation(old_modifier->base(),
							new_symtab);
		    assert(this_new_type->op() == TYPE_CONST	   ||
			   this_new_type->op() == TYPE_VOLATILE	   ||
			   this_new_type->op() == TYPE_CALL_BY_REF ||
			   this_new_type->op() == TYPE_NULL);
		    assert(trans == ((modifier_type *)this_new_type)->base());
		    break;
		  }
	        default:
		    break;
	      }	

	    return this_new_type;
          }
      }

    /* We now know that the type is not in the global symtab.  So we
     * must add it: */

    type_node *result;

    switch (old_type->op())
      {
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
          {
            base_type *old_base = (base_type *)old_type;
            result = new base_type(old_type->op(), old_type->size(),
                                   old_base->is_signed());
            break;
          }
        case TYPE_PTR:
          {
            result = new ptr_type(new base_type(TYPE_VOID, 0));
            break;
          }
        case TYPE_ARRAY:
          {
            array_type *old_array = (array_type *)old_type;
            array_bound new_lower =
                    bound_translation(old_array->lower_bound(), new_symtab);
            array_bound new_upper =
                    bound_translation(old_array->upper_bound(), new_symtab);
            result = new array_type(new base_type(TYPE_VOID, 0), new_lower,
                                    new_upper);
            break;
          }
        case TYPE_FUNC:
          {
            func_type *old_func = (func_type *)old_type;
            if (old_func->args_known())
              {
                result = new func_type(old_func->has_varargs(),
                                       new base_type(TYPE_VOID, 0));
              }
            else
              {
                result = new func_type(new base_type(TYPE_VOID, 0));
              }
            break;
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *old_struct = (struct_type *)old_type;
            result = new struct_type(old_struct->op(), old_struct->size(),
                                     old_struct->name(), 0);
            break;
          }
        case TYPE_ENUM:
          {
            enum_type *old_enum = (enum_type *)old_type;

            unsigned num_values = old_enum->num_values();
            enum_type *new_enum =
                    new enum_type(old_enum->name(), old_enum->size(),
                                  old_enum->is_signed(), num_values);
            for (unsigned value_num = 0; value_num < num_values; ++value_num)
              {
                new_enum->set_member(value_num, old_enum->member(value_num));
                new_enum->set_value(value_num, old_enum->value(value_num));
              }
            result = new_enum;
            break;
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            result = new modifier_type(old_type->op(),
                                       new base_type(TYPE_VOID, 0));
            break;
          }
        default:
            assert(FALSE);
      }

    /* Before recursing to add in any subtypes of this type, first
     * install it and mark it as an immediate replacement for the old
     * type.  This allows us to deal with recursive types. */
    mark_type_unique(result);
    result = new_symtab->install_type(result);

    replacement_immeds = new immed_list;
    replacement_immeds->append(immed(result));
    old_type->append_annote(k_linksuif_replacement, replacement_immeds);

    /* Now fill in the contents of the type and recurse to translate
     * any subtypes: */
    switch (old_type->op())
      {
        case TYPE_PTR:
          {
            ptr_type *new_ptr = (ptr_type *)result;
            ptr_type *old_ptr = (ptr_type *)old_type;
            new_ptr->set_ref_type(type_translation(old_ptr->ref_type(),
                                                   new_symtab));
            break;
          }
        case TYPE_ARRAY:
          {
            array_type *new_array = (array_type *)result;
            array_type *old_array = (array_type *)old_type;
            new_array->set_elem_type(type_translation(old_array->elem_type(),
                                                      new_symtab));
            break;
          }
        case TYPE_FUNC:
          {
            func_type *new_func = (func_type *)result;
            func_type *old_func = (func_type *)old_type;

            new_func->set_return_type(type_translation(old_func->return_type(),
                                                       new_symtab));
            if (old_func->args_known())
              {
                unsigned num_args = old_func->num_args();
                new_func->set_num_args(num_args);
                for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                  {
                    type_node *new_arg =
                            type_translation(old_func->arg_type(arg_num),
                                             new_symtab);
                    new_func->set_arg_type(arg_num, new_arg);
                  }
              }
            break;
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *new_struct = (struct_type *)result;
            struct_type *old_struct = (struct_type *)old_type;

            unsigned num_fields = old_struct->num_fields();
            new_struct->set_num_fields(num_fields);
            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                new_struct->set_field_name(field_num,
                                           old_struct->field_name(field_num));
                type_node *new_field_type =
                        type_translation(old_struct->field_type(field_num),
                                         new_symtab);
                new_struct->set_field_type(field_num, new_field_type);
                new_struct->set_offset(field_num,
                                       old_struct->offset(field_num));
              }
            break;
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *new_modifier = (modifier_type *)result;
            modifier_type *old_modifier = (modifier_type *)old_type;
            new_modifier->set_base(type_translation(old_modifier->base(),
                                                    new_symtab));
            break;
          }
        default:
            break;
      }

    translate_annotes(result, old_type, new_symtab);

    immed_list *mark_immeds =
            (immed_list *)(result->get_annote(k_linksuif_temp_mark));
    delete mark_immeds;

    return result;
  }

static void fix_translated_type(sym_node *old_symbol,
                                global_symtab *new_symtab)
  {
    sym_node *new_symbol = symbol_translation(old_symbol, new_symtab);

    switch (old_symbol->kind())
      {
        case SYM_PROC:
          {
            assert(new_symbol->is_proc());

            proc_sym *old_proc = (proc_sym *)old_symbol;
            proc_sym *new_proc = (proc_sym *)new_symbol;

            type_node *new_type =
                    type_translation(old_proc->type(), new_symtab);
            if (new_proc->type() == temp_func_type)
              {
                func_type *new_func_type = (func_type *)new_type;
                new_proc->set_type(new_func_type);
              }
            else
              {
                if (new_proc->src_lang() != old_proc->src_lang())
                  {
                    if (new_proc->src_lang() == src_unknown)
                      {
                        new_proc->set_src_lang(old_proc->src_lang());
                      }
                    else if (old_proc->src_lang() != src_unknown)
                      {
                        error_line(1, NULL,
                                   "clashing source language types for "
                                   "procedure %s", old_symbol->name());
                      }
                  }
                type_node *comp_type =
                        composite_type(new_proc->type(), new_type);
                if (comp_type == NULL)
                  {
                    if (!possibly_compatible_types(new_proc->type(), new_type))
                      {
                        warning_line(NULL,
                                     "global declarations of procedure %s "
                                     "have incompatible types",
                                     old_symbol->name());
                      }
                    enter_cast(new_proc, new_type);
                  }
                else if (new_proc->type() != comp_type)
                  {
                    assert(comp_type->is_func());
                    func_type *new_func_type = (func_type *)comp_type;
                    new_proc->set_type(new_func_type);
                  }
              }
            break;
          }
        case SYM_LABEL:
            assert(FALSE);
        case SYM_VAR:
          {
            assert(new_symbol->is_var());

            var_sym *old_var = (var_sym *)old_symbol;
            var_sym *new_var = (var_sym *)new_symbol;

            type_node *new_type =
                    type_translation(old_var->type(), new_symtab);
            if (new_var->type() == temp_var_type)
              {
                new_var->set_type(new_type);
              }
            else
              {
                if (is_common(new_var) || is_common(old_var))
                  {
                    if ((!is_common(new_var)) || (!is_common(old_var)))
                      {
                        error_line(1, NULL,
                                   "common block name %s conflicts with "
                                   "non-common global", new_var->name());
                      }
                  }
                else
                  {
                    type_node *comp_type =
                            composite_type(new_var->type(), new_type);
                    if (comp_type == NULL)
                      {
                        if (!possibly_compatible_types(new_var->type(),
                                                       new_type))
                          {
                            warning_line(NULL,
                                         "global declarations of variable %s "
                                         "have incompatible types",
                                         old_symbol->name());
                          }
                        enter_cast(new_var, new_type);
                      }
                    else if (new_var->type() != comp_type)
                      {
                        new_var->set_type(comp_type);
                      }
                  }

                if (new_var->has_var_def() && old_var->has_var_def())
                  {
                    if (!(is_common(new_var) && is_common(old_var)))
                      {
                        warning_line(old_symbol, 
                                     "global variable %s multiply defined",
                                     old_symbol->name());
                      }
                    var_def *new_def = new_var->definition();
                    var_def *old_def = old_var->definition();
                    merge_common_initializers(new_def, old_def);
                  }
              }
            break;
          }
        default:
            assert(FALSE);
      }

    translate_annotes(new_symbol, old_symbol, new_symtab);
  }

static void merge_common_initializers(var_def *def1, var_def *def2)
  {
    static const char *init_annote_names[] =
      {
        k_multi_init, k_repeat_init, k_fill
      };

    base_init_struct_list *initializers1 = read_init_data(def1);
    base_init_struct_list *initializers2 = read_init_data(def2);

    for (size_t annote_num = 0;
         annote_num < sizeof(init_annote_names) / sizeof(char *); ++annote_num)
      {
        const char *this_annote_name = init_annote_names[annote_num];
        while (TRUE)
          {
            immed_list *this_data =
                    (immed_list *)(def1->get_annote(this_annote_name));
            if (this_data == NULL)
                break;
            delete this_data;
          }

        while (TRUE)
          {
            immed_list *this_data =
                    (immed_list *)(def2->get_annote(this_annote_name));
            if (this_data == NULL)
                break;
            delete this_data;
          }
      }

    base_init_struct_list *merged_initializers = new base_init_struct_list;
    while ((!initializers1->is_empty()) && (!initializers2->is_empty()))
      {
        int fill_size = 0;
        while (((*initializers1)[0]->the_fill_init() != NULL) &&
               ((*initializers2)[0]->the_fill_init() != NULL))
          {
            fill_init_struct *fill1 = (*initializers1)[0]->the_fill_init();
            fill_init_struct *fill2 = (*initializers1)[0]->the_fill_init();
            int size1 = fill1->size;
            int size2 = fill2->size;
            assert((fill1->data == 0) && (fill2->data == 0));

            if (size1 > size2)
              {
                fill_size += size2;
                fill1->size -= size2;
                base_init_struct *old_init = initializers2->pop();
                delete old_init;
                if (initializers2->is_empty())
                    break;
              }
            else
              {
                fill_size += size1;
                fill2->size -= size1;
                base_init_struct *old_init = initializers1->pop();
                delete old_init;
                if (initializers1->is_empty())
                    break;
              }
          }

        if (fill_size != 0)
            merged_initializers->append(new fill_init_struct(fill_size, 0));

        if (initializers1->is_empty() || initializers2->is_empty())
            break;

        base_init_struct_list *fill_list;
        base_init_struct_list *non_fill_list;
        if ((*initializers1)[0]->the_fill_init() == NULL)
          {
            fill_list = initializers2;
            non_fill_list = initializers1;
          }
        else
          {
            fill_list = initializers1;
            non_fill_list = initializers2;
          }

        base_init_struct *chunk = non_fill_list->pop();
        int fill_amount = chunk->total_size();
        while ((fill_amount > 0) && (!fill_list->is_empty()))
          {
            fill_init_struct *fill_chunk = (*fill_list)[0]->the_fill_init();
            if (fill_chunk == NULL)
              {
                error_line(1, NULL, "conflicting data for global %s",
                           def1->variable()->name());
              }

            if (fill_chunk->size > fill_amount)
              {
                fill_chunk->size -= fill_amount;
                fill_amount = 0;
              }
            else
              {
                fill_amount -= fill_chunk->size;
                base_init_struct *old_chunk = fill_list->pop();
                delete old_chunk;
              }
          }

        merged_initializers->append(chunk);
      }

    if (!initializers1->is_empty())
        merged_initializers->append(initializers1);
    else if (!initializers2->is_empty())
        merged_initializers->append(initializers2);

    delete initializers1;
    delete initializers2;

    write_init_data(def1, merged_initializers);

    def2->parent()->remove_def(def2);
    delete def2;

    delete merged_initializers;
  }

static void translate_annotes(suif_object *new_object, suif_object *old_object,
                              global_symtab *new_symtab)
  {
    annote_list_iter old_annotes(old_object->annotes());
    while (!old_annotes.is_empty())
      {
        annote *this_annote = old_annotes.step();
        if (this_annote->name() == k_linksuif_replacement)
            continue;

        immed_list *new_data = NULL;
        if (this_annote->data() != NULL)
          {
            new_data = new immed_list;
            immed_list_iter old_immed_iter(this_annote->immeds());
            while (!old_immed_iter.is_empty())
              {
                immed this_immed = old_immed_iter.step();
                if (this_immed.is_symbol())
                  {
                    this_immed = immed(symbol_translation(this_immed.symbol(),
                                                          new_symtab),
                                       this_immed.offset());
                  }
                else if (this_immed.is_type())
                  {
                    this_immed = immed(type_translation(this_immed.type(),
                                                        new_symtab));
                  }
                new_data->append(this_immed);
              }
          }
        annote *new_annote = new annote(this_annote->name(), new_data);
        new_object->annotes()->append(new_annote);
      }
  }

static sym_node *symbol_translation(sym_node *old_symbol,
                                    global_symtab *new_symtab)
  {
    immed_list *replacement_immeds =
            (immed_list *)(old_symbol->peek_annote(k_linksuif_replacement));
    if (replacement_immeds != NULL)
      {
        assert(replacement_immeds->count() == 1);
        immed first_immed = replacement_immeds->head()->contents;
        assert(first_immed.is_symbol());
        return first_immed.symbol();
      }

    sym_node *result;
    if (old_symbol->is_var() &&
        (((var_sym *)old_symbol)->parent_var() != NULL))
      {
        result = NULL;
        var_sym *old_var = (var_sym *)old_symbol;
        var_sym *old_parent = old_var->parent_var();
        var_sym *new_parent = var_translation(old_parent, new_symtab);
        unsigned num_children = new_parent->num_children();
        for (unsigned child_num = 0; child_num < num_children; ++child_num)
          {
            var_sym *this_child = new_parent->child_var(child_num);
            if (this_child->offset() == old_var->offset())
              {
                boolean same_types =
                        isomorphic_types(this_child->type(), old_var->type(),
					 FALSE);
                remove_temp_links(old_var->type());
                if (same_types)
                  {
                    result = this_child;
                    break;
                  }
              }
          }
      }
    else
      {
        result = new_symtab->lookup_sym(old_symbol->name(), old_symbol->kind(),
                                        FALSE);
      }

    if (result == NULL)
      {
        switch (old_symbol->kind())
          {
            case SYM_PROC:
              {
                if (temp_func_type == NULL)
                  {
                    type_node *new_return = new base_type(TYPE_VOID, 0);
                    temp_func_type = new func_type(new_return);
                    mark_type_unique(temp_func_type);
                    temp_func_type =
                            (func_type *)(new_symtab->install_type(
                                    temp_func_type));
                  }

                proc_sym *old_proc = (proc_sym *)old_symbol;
                result = new_symtab->new_proc(temp_func_type,
                                              old_proc->src_lang(),
                                              old_proc->name());
                break;
              }
            case SYM_LABEL:
                assert(FALSE);
            case SYM_VAR:
              {
                if (temp_var_type == NULL)
                  {
                    temp_var_type =
                            new base_type(TYPE_INT, target.size[C_int], TRUE);
                    mark_type_unique(temp_var_type);
                    temp_var_type = new_symtab->install_type(temp_var_type);
                  }

                var_sym *old_var = (var_sym *)old_symbol;
                var_sym *new_var = new_symtab->new_var(temp_var_type,
                                                       old_symbol->name());
                result = new_var;

                if (old_var->parent_var() != NULL)
                  {
                    var_sym *new_parent =
                            var_translation(old_var->parent_var(), new_symtab);
                    new_parent->add_child(new_var, old_var->offset());
                  }

                break;
              }
            default:
                assert(FALSE);
          }
        result->copy_flags(old_symbol);
      }

    if (result->is_var())
      {
        assert(old_symbol->is_var());
        var_sym *new_var = (var_sym *)result;
        var_sym *old_var = (var_sym *)old_symbol;
        if (old_var->is_addr_taken())
            new_var->set_addr_taken();
      }

    replacement_immeds = new immed_list;
    replacement_immeds->append(immed(result));
    old_symbol->append_annote(k_linksuif_replacement, replacement_immeds);

    return result;
  }

static var_sym *var_translation(var_sym *old_var, global_symtab *new_symtab)
  {
    sym_node *new_sym = symbol_translation(old_var, new_symtab);
    assert(new_sym->is_var());
    return (var_sym *)new_sym;
  }

static proc_sym *proc_translation(proc_sym *old_proc,
                                  global_symtab *new_symtab)
  {
    sym_node *new_sym = symbol_translation(old_proc, new_symtab);
    assert(new_sym->is_proc());
    return (proc_sym *)new_sym;
  }

static array_bound bound_translation(array_bound old_bound,
                                     global_symtab *new_symtab)
  {
    if (old_bound.is_variable())
        return array_bound(var_translation(old_bound.variable(), new_symtab));
    else
        return old_bound;
  }

/* Check to see if two types are identical.  This is done by
 * recursively descending through the type tree and comparing node by
 * node.  Before recursing a link is left behind to prevent looping on
 * recursive types.  The third parameter allows this algorithm to
 * check and see if two types are "mergable", which checks the case if
 * the zero length (undefined) structures from one type were updated
 * with the more complete type information in the second type tree. */
static boolean isomorphic_types(type_node *type1,
				type_node *type2,
				boolean ignore_zero_length_structs)
  {
    immed_list *replacement_immeds =
            (immed_list *)(type1->peek_annote(k_linksuif_replacement));
    if (replacement_immeds != NULL)
      {
        assert(replacement_immeds->count() == 1);
        immed first_immed = replacement_immeds->head()->contents;
        assert(first_immed.is_type());
        return (first_immed.type() == type2);
      }

    replacement_immeds =
            (immed_list *)(type2->peek_annote(k_linksuif_replacement));
    if (replacement_immeds != NULL)
      {
        assert(replacement_immeds->count() == 1);
        immed first_immed = replacement_immeds->head()->contents;
        assert(first_immed.is_type());
        return (first_immed.type() == type1);
      }

    immed_list *link_immeds =
            (immed_list *)(type1->peek_annote(k_linksuif_temp_link));
    if (link_immeds != NULL)
      {
        assert(link_immeds->count() >= 1);
	immed_list_e *current_e = link_immeds->head();
	
	while(current_e != NULL)
	  {
	    immed immed_contents = current_e->contents;
	    assert(immed_contents.is_type());
	    if (immed_contents.type() == type2)
	      {
		return TRUE;
	      }
	    current_e = current_e->next();
	  }
      }

    if (type1->op() != type2->op())
        return FALSE;

    /* If either type has a "unique" mark on it, we don't need to 
     * compare it to anything else since is by definition unique: */ 
    immed_list *mark_immeds1 = 
	(immed_list *)(type1->peek_annote(k_linksuif_temp_mark)); 
    immed_list *mark_immeds2 = 
	(immed_list *)(type2->peek_annote(k_linksuif_temp_mark)); 
    if(mark_immeds1 != NULL || 
       mark_immeds2 != NULL) 
	{ 
	    return FALSE; 
	} 
    
    switch (type1->op())
      {
        case TYPE_ENUM:
          {
            enum_type *enum1 = (enum_type *)type1;
            enum_type *enum2 = (enum_type *)type2;

            unsigned num_values = enum1->num_values();

	    if (num_values != enum2->num_values())
              {
                return FALSE;
              }

	    if (!(is_anonymous(enum1) && is_anonymous(enum2)))
	      {
		if ((strcmp(enum1->name(), enum2->name()) != 0))
		  {
		    return FALSE;
		  }
	      }

            for (unsigned value_num = 0; value_num < num_values; ++value_num)
              {
                if ((strcmp(enum1->member(value_num), enum2->member(value_num))
                     != 0) ||
                    (enum1->value(value_num) != enum2->value(value_num)))
                  {
                    return FALSE;
                  }
              }

            /* fall through */
          }
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
          {
            base_type *base1 = (base_type *)type1;
            base_type *base2 = (base_type *)type2;

            if ((base1->is_signed() != base2->is_signed()) ||
                (base1->size() != base2->size()))
              {
                return FALSE;
              }

            break;
          }
        case TYPE_PTR:
            break;
        case TYPE_ARRAY:
            break;
        case TYPE_FUNC:
          {
            func_type *func1 = (func_type *)type1;
            func_type *func2 = (func_type *)type2;

            if ((func1->num_args() != func2->num_args()) ||
                (func1->args_known() != func2->args_known()) ||
                (func1->has_varargs() != func2->has_varargs()))
              {
                return FALSE;
              }
            break;
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *struct1 = (struct_type *)type1;
            struct_type *struct2 = (struct_type *)type2;

            unsigned num_fields = struct1->num_fields();

            if (!(is_anonymous(struct1) && is_anonymous(struct2)) &&
		strcmp(struct1->name(), struct2->name()) != 0)
	      {
                return FALSE;
	      }

            if ((struct1->size() != struct2->size()) ||
                (num_fields != struct2->num_fields()))
              {
		if (ignore_zero_length_structs &&
		    (struct1->size() == 0 || struct2->size() == 0))
		  {
		    /* Should not encounter anonymous zero-length structs: */
		    assert(!is_anonymous(struct1) && !is_anonymous(struct2));
		    return TRUE;
		  }
		else
		  {
		    if(!is_anonymous(struct1) &&
		       !(struct1->size() == 0 || struct2->size() == 0))
		      {	
			warning_line(NULL,
				     "Conflicting definitions for %s",
				     struct1->name());
		      }
		    return FALSE;
		  }
              }

            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                if ((strcmp(struct1->field_name(field_num),
                            struct2->field_name(field_num)) != 0) ||
                    (struct1->offset(field_num) != struct2->offset(field_num)))
                  {
		    if(!is_anonymous(struct1))
		      {
			warning_line(NULL,
				     "Conflicting definitions for %s",
				     struct1->name());
		      }
                    return FALSE;
                  }
              }

            break;
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
            break;
        default:
            assert(FALSE);
      }

    immed_list *link1_immeds =
            (immed_list *)(type1->get_annote(k_linksuif_temp_link));
    if (link1_immeds == NULL)
      {
	link1_immeds = new immed_list;
      }
    link1_immeds->append(immed(type2));
    type1->append_annote(k_linksuif_temp_link, link1_immeds);

    immed_list *link2_immeds =
            (immed_list *)(type2->get_annote(k_linksuif_temp_link));
    if (link2_immeds == NULL)
      {
	link2_immeds = new immed_list;
      }
    link2_immeds->append(immed(type1));
    type2->append_annote(k_linksuif_temp_link, link2_immeds);

    if (!isomorphic_annotes(type1, type2))
        return FALSE;

    switch (type1->op())
      {
        case TYPE_PTR:
          {
            ptr_type *ptr1 = (ptr_type *)type1;
            ptr_type *ptr2 = (ptr_type *)type2;

            return isomorphic_types(ptr1->ref_type(), ptr2->ref_type(),
				    ignore_zero_length_structs);
          }
        case TYPE_ARRAY:
          {
            array_type *array1 = (array_type *)type1;
            array_type *array2 = (array_type *)type2;

            if (!isomorphic_bounds(array1->lower_bound(),
                                   array2->lower_bound()))
              {
                return FALSE;
              }

            if (!isomorphic_bounds(array1->upper_bound(),
                                   array2->upper_bound()))
              {
                return FALSE;
              }

            return isomorphic_types(array1->elem_type(), array2->elem_type(),
				    ignore_zero_length_structs);
          }
        case TYPE_FUNC:
          {
            func_type *func1 = (func_type *)type1;
            func_type *func2 = (func_type *)type2;

            unsigned num_args = func1->num_args();
            for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
              {
                if (!isomorphic_types(func1->arg_type(arg_num),
                                      func2->arg_type(arg_num),
				      ignore_zero_length_structs))
                  {
                    return FALSE;
                  }
              }

            return isomorphic_types(func1->return_type(),
                                    func2->return_type(),
				    ignore_zero_length_structs);
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *struct1 = (struct_type *)type1;
            struct_type *struct2 = (struct_type *)type2;

            unsigned num_fields = struct1->num_fields();

            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                if (!isomorphic_types(struct1->field_type(field_num),
                                      struct2->field_type(field_num),
				      ignore_zero_length_structs))
                  {
                    return FALSE;
                  }
              }

            return TRUE;
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *mod1 = (modifier_type *)type1;
            modifier_type *mod2 = (modifier_type *)type2;

            return isomorphic_types(mod1->base(), mod2->base(),
				    ignore_zero_length_structs);
          }
        default:
            return TRUE;
      }
  }

static boolean isomorphic_bounds(array_bound bound1, array_bound bound2)
  {
    if (!bound1.is_variable())
        return bound1 == bound2;

    if (!bound2.is_variable())
        return FALSE;

    return isomorphic_vars(bound1.variable(), bound2.variable());
  }

static boolean isomorphic_annotes(suif_object *object1, suif_object *object2)
  {
    annote_list_iter annote_iter1(object1->annotes());
    annote_list_iter annote_iter2(object2->annotes());
    while (!annote_iter1.is_empty())
      {
        if (annote_iter2.is_empty())
            return FALSE;

        annote *annote1 = annote_iter1.step();

	while(strcmp(annote1->name(), k_linksuif_temp_link) == 0)
	  {
	    if(annote_iter1.is_empty())
	      {
		annote1 = NULL;
		break;
	      }
	    annote1 = annote_iter1.step();
	  }
	
        annote *annote2 = annote_iter2.step();

	while(strcmp(annote2->name(), k_linksuif_temp_link) == 0)
	  {
	    if(annote_iter2.is_empty())
	      {
		annote2 = NULL;
		break;
	      }
	    annote2 = annote_iter1.step();
	  }

	if(annote1 == NULL && annote2 == NULL) {
	    return TRUE;
	} else if(annote1 == NULL || annote2 == NULL) {
	    return FALSE;
	}

        if (strcmp(annote1->name(), annote2->name()) != 0)
            return FALSE;

        immed_list_iter data_iter1(annote1->immeds());
        immed_list_iter data_iter2(annote2->immeds());

        while (!data_iter1.is_empty())
          {
            if (data_iter2.is_empty())
                return FALSE;

            immed data1 = data_iter1.step();
            immed data2 = data_iter2.step();

            if (data1.kind() != data2.kind())
                return FALSE;

            switch (data1.kind())
              {
                case im_symbol:
                    if (data1.offset() != data2.offset())
                        return FALSE;
                    if (strcmp(data1.symbol()->name(), data2.symbol()->name())
                        != 0)
                      {
                        return FALSE;
                      }
                    break;
                case im_type:
                    if (!isomorphic_types(data1.type(), data2.type(), FALSE))
                        return FALSE;
                    break;
                default:
                    if (data1 != data2)
                        return FALSE;
              }
          }

        if (!data_iter2.is_empty())
            return FALSE;
      }

    if (!annote_iter2.is_empty())
        return FALSE;

    return TRUE;
  }

static boolean isomorphic_vars(var_sym *var1, var_sym *var2)
  {
    var_sym *parent1 = var1->parent_var();
    if (parent1 != NULL)
      {
        var_sym *parent2 = var2->parent_var();
        if (parent2 != NULL)
          {
            return (isomorphic_vars(parent1, parent2) &&
                    (var1->offset() == var2->offset()) &&
                    isomorphic_types(var1->type(), var2->type(), FALSE));
          }
        else
          {
            return FALSE;
          }
      }
    else
      {
        if (var2->parent_var() != NULL)
            return FALSE;
        else
            return (strcmp(var1->name(), var2->name()) == 0);
      }
  }

static void remove_temp_links(type_node *the_type)
  {
    void *link_data = the_type->peek_annote(k_linksuif_temp_link);

    if (link_data != NULL)
      {
        immed_list *link_immeds =
                (immed_list *)(the_type->get_annote(k_linksuif_temp_link));
        assert(link_immeds->count() >= 1);

	immed_list_e *current_e = link_immeds->head();
	
	while(current_e != NULL)
	  {
	    immed immed_contents = current_e->contents;
	    assert(immed_contents.is_type());

	    type_node *linked_type = immed_contents.type();
	    immed_list *back_link =
                (immed_list *)(linked_type->get_annote(k_linksuif_temp_link));
	    if(back_link != NULL)
	      {
		delete back_link;
	      }
	    current_e = current_e->next();
	  }
        delete link_immeds;

        annote_list_iter annote_iter(the_type->annotes());
        while (!annote_iter.is_empty())
          {
            annote *this_annote = annote_iter.step();

            if (this_annote->data() != NULL)
              {
                immed_list_iter data_iter(this_annote->immeds());
                while (!data_iter.is_empty())
                  {
                    immed this_immed = data_iter.step();
                    if (this_immed.is_type())
                        remove_temp_links(this_immed.type());
                  }
              }
          }

        switch (the_type->op())
          {
            case TYPE_PTR:
              {
                ptr_type *the_ptr_type = (ptr_type *)the_type;
                remove_temp_links(the_ptr_type->ref_type());
                return;
              }
            case TYPE_ARRAY:
              {
                array_type *the_array_type = (array_type *)the_type;
                remove_temp_links(the_array_type->elem_type());
                return;
              }
            case TYPE_FUNC:
              {
                func_type *the_func_type = (func_type *)the_type;
                remove_temp_links(the_func_type->return_type());

                unsigned num_args = the_func_type->num_args();
                for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                    remove_temp_links(the_func_type->arg_type(arg_num));

                return;
              }
            case TYPE_GROUP:
            case TYPE_STRUCT:
            case TYPE_UNION:
              {
                struct_type *the_struct_type = (struct_type *)the_type;

                unsigned num_fields = the_struct_type->num_fields();
                for (unsigned field_num = 0; field_num < num_fields;
                     ++field_num)
                  {
                    remove_temp_links(the_struct_type->field_type(field_num));
                  }

                return;
              }
            case TYPE_CONST:
            case TYPE_VOLATILE:
            case TYPE_CALL_BY_REF:
            case TYPE_NULL:
              {
                modifier_type *the_mod_type = (modifier_type *)the_type;
                remove_temp_links(the_mod_type->base());
                return;
              }
            default:
                return;
          }
      }
  }

/*
 *  Return the composite type of the two given types, or NULL if it is
 *  too difficult to find a composite.  The two types are assumed to
 *  come from different translation units.  The basic rules for
 *  forming composites are those given in the ANSI C standard.  The
 *  ANSI standard does not specify the composite of structurally
 *  compatible structure or union types, however.  This function
 *  assumes that exactly equivalent structure types have already been
 *  merged, so it simply gives up and returns NULL for non-identical
 *  structure or union types or types referencing such structure or
 *  union types.
 *
 *  The ANSI standard does not speak to the composite when one of the
 *  types is an enumerated type; for most compilers, it doesn't
 *  matter.  Indeed, even within SUIF, it is unlikely to matter much
 *  which type is chosen as long as something compatible with both is
 *  used.  We choose in this case to just install a new integer type
 *  of the appropriate kind and return it, without any annotations or
 *  other modifications.
 *
 *  This function also has to handle the SUIF extensions to the type
 *  system for ANSI C.  Symbolic bounds in arrays will match other
 *  symbolic bounds refering to the same symbol or unkown bounds, in
 *  which case the composite has the symbolic bound.  NULL is returned
 *  if one bound is symbolic and the other refers to a different
 *  symbol or to a constant.
 *
 *  Annotations do not affect whether or not the two types have a
 *  composite returned by this function.  If both are the same type,
 *  that type will be the composite and the annotations will remain.
 *  Otherwise, the new type will have no annotations.
 *
 *  The order of modifier types does not matter, but if one type has a
 *  modifier with a given ``op()'', the other must also, or NULL is
 *  returned.  The modifiers on the composite will be in the order of
 *  those on type1.
 *
 *  Note that since the types are assumed to come from different
 *  translation units, we can assume they are both installed in the
 *  global symbol table.
 */
static type_node *composite_type(type_node *type1, type_node *type2)
  {
    static type_ops modifier_ops[] =
      { TYPE_CONST, TYPE_VOLATILE, TYPE_CALL_BY_REF, TYPE_NULL};

    if (type1 == type2)
        return type1;

    if (type1->is_modifier() || type2->is_modifier())
      {
        size_t mod_num;
        for (mod_num = 0; mod_num < sizeof(modifier_ops) / sizeof(type_ops);
             ++mod_num)
          {
            modifier_type *mod1 = type1->find_modifier(modifier_ops[mod_num]);
            modifier_type *mod2 = type2->find_modifier(modifier_ops[mod_num]);

            if ((mod1 == NULL) && (mod2 == NULL))
                continue;

            if ((mod1 == NULL) || (mod2 == NULL))
                return NULL;
          }

        type_node *base_composite =
                composite_type(type1->unqual(), type2->unqual());
        if (base_composite == NULL)
            return NULL;

        return qualify_with((modifier_type *)type1, base_composite);
      }

    if (type1->is_enum() || type2->is_enum())
      {
        if ((type1->op() != TYPE_ENUM) && (type1->op() != TYPE_INT))
            return NULL;
        if ((type2->op() != TYPE_ENUM) && (type2->op() != TYPE_INT))
            return NULL;

        base_type *base1 = (base_type *)type1;
        base_type *base2 = (base_type *)type2;
        if ((base1->size() != base2->size()) ||
            (base1->is_signed() != base2->is_signed()))
          {
            return NULL;
          }

        base_type *new_base =
                new base_type(TYPE_INT, base1->size(), base1->is_signed());
        return base1->parent()->install_type(new_base);
      }

    if (type1->op() != type2->op())
        return NULL;

    type_node *new_composite;

    switch (type1->op())
      {
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
          {
            base_type *base1 = (base_type *)type1;
            base_type *base2 = (base_type *)type2;

            boolean is_signed = base1->is_signed();
            int size = base1->size();

            if ((is_signed != base2->is_signed()) || (size != base2->size()))
                return NULL;

            new_composite = new base_type(base1->op(), size, is_signed);
            break;
          }
        case TYPE_PTR:
          {
            ptr_type *ptr1 = (ptr_type *)type1;
            ptr_type *ptr2 = (ptr_type *)type2;

            new_composite = composite_type(ptr1->ref_type(), ptr2->ref_type());
            if (new_composite == NULL)
                return NULL;

            new_composite = new ptr_type(new_composite);
            break;
          }
        case TYPE_ARRAY:
          {
            array_type *array1 = (array_type *)type1;
            array_type *array2 = (array_type *)type2;

            array_bound lower1 = array1->lower_bound();
            array_bound lower2 = array2->lower_bound();

            boolean bounds_match;
            array_bound new_lower =
                    composite_bound(lower1, lower2, &bounds_match);
            if (!bounds_match)
                return NULL;

            array_bound upper1 = array1->upper_bound();
            array_bound upper2 = array2->upper_bound();

            array_bound new_upper =
                    composite_bound(upper1, upper2, &bounds_match);
            if (!bounds_match)
                return NULL;

            new_composite =
                    composite_type(array1->elem_type(), array2->elem_type());
            if (new_composite == NULL)
                return NULL;

            new_composite =
                    new array_type(new_composite, new_lower, new_upper);
            break;
          }
        case TYPE_FUNC:
          {
            func_type *func1 = (func_type *)type1;
            func_type *func2 = (func_type *)type2;

            if (func1->has_varargs() != func2->has_varargs())
                return NULL;

            new_composite =
                    composite_type(func1->return_type(), func2->return_type());
            if (new_composite == NULL)
                return NULL;

            func_type *new_func =
                    new func_type(func1->has_varargs(), new_composite);

            if (func1->args_known())
              {
                if (func2->args_known())
                  {
                    unsigned num_args = func1->num_args();
                    if (num_args != func2->num_args())
                      {
                        delete new_func;
                        return NULL;
                      }

                    new_func->set_num_args(num_args);

                    for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                      {
                        type_node *new_arg =
                                composite_type(func1->arg_type(arg_num),
                                               func2->arg_type(arg_num));
                        if (new_arg == NULL)
                          {
                            delete new_func;
                            return NULL;
                          }

                        new_func->set_arg_type(arg_num, new_arg);
                      }
                  }
                else
                  {
                    unsigned num_args = func1->num_args();
                    new_func->set_num_args(num_args);

                    for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                      {
                        new_func->set_arg_type(arg_num,
                                               func1->arg_type(arg_num));
                      }
                  }
              }
            else
              {
                if (func2->args_known())
                  {
                    unsigned num_args = func2->num_args();
                    new_func->set_num_args(num_args);

                    for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                      {
                        new_func->set_arg_type(arg_num,
                                               func2->arg_type(arg_num));
                      }
                  }
                else
                  {
                    new_func->set_args_unknown();
                  }
              }

            new_composite = new_func;

            break;
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
            return NULL;
        default:
            assert(FALSE);
      }

    return type1->parent()->install_type(new_composite);
  }

static array_bound composite_bound(array_bound bound1, array_bound bound2,
                                   boolean *success)
  {
    if (bound1.is_unknown())
      {
        *success = TRUE;
        return bound2;
      }
    if (bound2.is_unknown())
      {
        *success = TRUE;
        return bound1;
      }

    if (bound1 != bound2)
      {
        *success = FALSE;
        return unknown_bound;
      }
    else
      {
        *success = TRUE;
        return bound1;
      }
  }

static modifier_type *qualify_with(modifier_type *old_modifiers,
                                   type_node *new_type)
  {
    type_node *new_base;
    if (old_modifiers->base()->is_modifier())
      {
        new_base = qualify_with((modifier_type *)(old_modifiers->base()),
                                new_type);
      }
    else
      {
        new_base = new_type;
      }

    type_node *new_modifier = new modifier_type(old_modifiers->op(), new_base);
    new_modifier = new_base->parent()->install_type(new_modifier);
    return (modifier_type *)new_modifier;
  }

/*
 *  This function takes two types that are assumed to come from
 *  different translation units and returns FALSE if it can tell that
 *  the two types are not compatible acording to the rules of ANSI C.
 *  If the types are compatible or the algorithm this function uses
 *  cannot tell if they are compatible, it returns TRUE.  The reason
 *  it is difficult is that ANSI C specifies a kind of structural
 *  compatibility instead of name compatibility for structures in
 *  different translation units.  This ANSI definition of structural
 *  compatibility is flawed in that it is recursive without a base
 *  case (so in some cases types cannot be proved compatible or
 *  incompatible) and it makes no provision for incomplete structures
 *  and unions.
 *
 *  Also, note that integral types that happen to be translated by the
 *  front end into the same SUIF type will be considered possibly
 *  compatible by this function.  So the C ``int'' and ``long'' types
 *  will cause this function to return TRUE if both are 32 bit
 *  integers for a particular target machine.
 *
 *  The SUIF type system contains some extensions to the ANSI C type
 *  system that must also be handled.  Symbolic bounds in arrays are
 *  treated as unkown bounds here -- they are considered possibly
 *  compatible with any other bounds.  Hence arrays are only found to
 *  be incompatible when they have known integral bounds that differ
 *  or incompatible element types.  Annotations are entirely ignored,
 *  as are modifiers other than ``const'' or ``volatile'' modifiers,
 *  and these can come in any order, so long as there is one of the
 *  appropriate kind on each type or none on either.
 */
static boolean possibly_compatible_types(type_node *type1, type_node *type2)
  {
    if (type1 == type2)
        return TRUE;

    if (type1->is_modifier() || type2->is_modifier())
      {
        if ((type1->is_const() != type2->is_const()) ||
            (type1->is_volatile() != type2->is_volatile()))
          {
            return FALSE;
          }

        return possibly_compatible_types(type1->unqual(), type2->unqual());
      }

    if (type1->op() != type2->op())
        return FALSE;

    switch (type1->op())
      {
        case TYPE_ENUM:
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
          {
            base_type *base1 = (base_type *)type1;
            base_type *base2 = (base_type *)type2;

            return ((base1->is_signed() == base2->is_signed()) &&
                    (base1->size() == base2->size()));
          }
        case TYPE_PTR:
          {
            ptr_type *ptr1 = (ptr_type *)type1;
            ptr_type *ptr2 = (ptr_type *)type2;

            return possibly_compatible_types(ptr1->ref_type(),
                                             ptr2->ref_type());
          }
        case TYPE_ARRAY:
          {
            array_type *array1 = (array_type *)type1;
            array_type *array2 = (array_type *)type2;

            array_bound lower1 = array1->lower_bound();
            array_bound lower2 = array2->lower_bound();
            if (lower1.is_constant() && lower2.is_constant())
              {
                if (lower1.constant() != lower2.constant())
                    return FALSE;
              }

            array_bound upper1 = array1->upper_bound();
            array_bound upper2 = array2->upper_bound();
            if (upper1.is_constant() && upper2.is_constant())
              {
                if (upper1.constant() != upper2.constant())
                    return FALSE;
              }

            return possibly_compatible_types(array1->elem_type(),
                                             array2->elem_type());
          }
        case TYPE_FUNC:
          {
            func_type *func1 = (func_type *)type1;
            func_type *func2 = (func_type *)type2;

            if (!possibly_compatible_types(func1->return_type(),
                                           func2->return_type()))
              {
                return FALSE;
              }

            if (!func1->args_known() || (!func2->args_known()))
                return TRUE;

            unsigned num_args = func1->num_args();

            if ((num_args != func2->num_args()) ||
                (func1->has_varargs() != func2->has_varargs()))
              {
                return FALSE;
              }

            for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
              {
                if (!possibly_compatible_types(func1->arg_type(arg_num),
                                               func2->arg_type(arg_num)))
                  {
                    return FALSE;
                  }
              }

            return TRUE;
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *struct1 = (struct_type *)type1;
            struct_type *struct2 = (struct_type *)type2;

            if ((struct1->size() == 0) || (struct2->size() == 0))
                return TRUE;

            unsigned num_fields = struct1->num_fields();

            if ((struct1->size() != struct2->size()) ||
                (num_fields != struct2->num_fields()))
              {
                return FALSE;
              }

            if ((struct1->peek_annote(k_linksuif_temp_link_compat) != NULL) ||
                (struct2->peek_annote(k_linksuif_temp_link_compat) != NULL))
              {
                return TRUE;
              }

            immed_list *link1_immeds = new immed_list;
            link1_immeds->append(immed(type2));
            type1->append_annote(k_linksuif_temp_link_compat, link1_immeds);

            immed_list *link2_immeds = new immed_list;
            link2_immeds->append(immed(type1));
            type2->append_annote(k_linksuif_temp_link_compat, link2_immeds);

            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                if ((strcmp(struct1->field_name(field_num),
                            struct2->field_name(field_num)) != 0) ||
                    (struct1->offset(field_num) != struct2->offset(field_num)))
                  {
                    return FALSE;
                  }
                if (!possibly_compatible_types(struct1->field_type(field_num),
                                               struct2->field_type(field_num)))
                  {
                    return FALSE;
                  }
              }

            link1_immeds =
                    (immed_list *)(struct1->get_annote(
			k_linksuif_temp_link_compat));
            assert(link1_immeds != NULL);
            delete link1_immeds;

            link2_immeds =
                    (immed_list *)(struct2->get_annote(
			k_linksuif_temp_link_compat));
            assert(link2_immeds != NULL);
            delete link2_immeds;

            return TRUE;
          }
        default:
            assert(FALSE);
            return FALSE;
      }
  }

static void add_proc_casts(tree_proc *the_proc)
  {
    the_proc->map(&add_node_casts, NULL);
  }

static void add_node_casts(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        the_tree_instr->instr_map(&add_instr_casts, NULL, FALSE);
        add_instr_casts(the_tree_instr->instr(), NULL);
      }
  }

static void add_instr_casts(instruction *the_instr, void *)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_symbol() &&
            (this_op.symbol()->parent()->kind() == SYMTAB_GLOBAL))
          {
            var_sym *this_var = this_op.symbol();
            type_node *old_type;
            boolean exists = has_cast(this_var, &old_type);
            if (exists)
              {
                assert(old_type != NULL);
                type_node_list *casts =
                        cast_sequence(this_op.type(), old_type);
                if (casts != NULL)
                  {
                    while (!casts->is_empty())
                      {
                        type_node *this_type = casts->pop();
                        in_rrr *new_cast =
                                new in_rrr(io_cvt, this_type, operand(),
                                           this_op);
                        tree_node *new_node = new tree_instr(new_cast);
                        tree_node *old_node = the_instr->owner();
                        old_node->parent()->insert_before(new_node,
                                                          old_node->list_e());
                        this_op = operand(new_cast);
                      }
                    the_instr->set_src_op(src_num, this_op);
                  }
                else
                  {
                    type_node *var_type = this_var->type();
                    ptr_type *var_ptr = new ptr_type(var_type);
                    var_ptr =
                            (ptr_type *)(var_type->parent()->install_type(
                                    var_ptr));
                    in_ldc *the_ldc =
                            new in_ldc(var_ptr, operand(), immed(var_type));
                    tree_node *new_node = new tree_instr(the_ldc);
                    tree_node *old_node = the_instr->owner();
                    old_node->parent()->insert_before(new_node,
                                                      old_node->list_e());

                    ptr_type *old_ptr = new ptr_type(old_type);
                    old_ptr =
                            (ptr_type *)(old_type->parent()->install_type(
                                    old_ptr));
                    in_rrr *the_cast =
                            new in_rrr(io_cvt, old_ptr, operand(),
                                       operand(the_ldc));
                    new_node = new tree_instr(the_cast);
                    old_node->parent()->insert_before(new_node,
                                                      old_node->list_e());

                    in_rrr *the_load =
                            new in_rrr(io_lod, old_type, operand(),
                                       operand(the_cast));
                    new_node = new tree_instr(the_load);
                    old_node->parent()->insert_before(new_node,
                                                      old_node->list_e());

                    the_instr->set_src_op(src_num, operand(the_load));
                  }
              }
          }
      }

    operand dest_op = the_instr->dst_op();
    if (dest_op.is_symbol() &&
        (dest_op.symbol()->parent()->kind() == SYMTAB_GLOBAL))
      {
        var_sym *dest_var = dest_op.symbol();
        type_node *old_type;
        boolean exists = has_cast(dest_var, &old_type);
        if (exists)
          {
            assert(old_type != NULL);
            type_node_list *casts = cast_sequence(old_type, dest_op.type());
            if (casts != NULL)
              {
                the_instr->set_dst(operand());
                while (!casts->is_empty())
                  {
                    type_node *this_type = casts->pop();
                    in_rrr *new_cast =
                            new in_rrr(io_cvt, this_type, operand(),
                                       operand(the_instr));
                    tree_node *new_node = new tree_instr(new_cast);
                    tree_node *old_node = the_instr->owner();
                    old_node->parent()->insert_after(new_node,
                                                     old_node->list_e());
                    the_instr = new_cast;
                  }
                the_instr->set_dst(operand(dest_var));
              }
            else
              {
                the_instr->set_dst(operand());

                type_node *var_type = dest_var->type();
                ptr_type *var_ptr = new ptr_type(var_type);
                var_ptr =
                        (ptr_type *)(var_type->parent()->install_type(
                                var_ptr));
                in_ldc *the_ldc =
                        new in_ldc(var_ptr, operand(), immed(var_type));
                tree_node *new_node = new tree_instr(the_ldc);
                tree_node *old_node = the_instr->owner();
                old_node->parent()->insert_after(new_node, old_node->list_e());
                old_node = new_node;

                ptr_type *old_ptr = new ptr_type(old_type);
                old_ptr =
                        (ptr_type *)(old_type->parent()->install_type(
                                old_ptr));
                in_rrr *the_cast =
                        new in_rrr(io_cvt, old_ptr, operand(),
                                   operand(the_ldc));
                new_node = new tree_instr(the_cast);
                old_node->parent()->insert_after(new_node, old_node->list_e());
                old_node = new_node;

                in_rrr *the_memcpy =
                        new in_rrr(io_memcpy, type_void, operand(),
                                   operand(the_cast), operand(the_instr));
                new_node = new tree_instr(the_memcpy);
                old_node->parent()->insert_after(new_node, old_node->list_e());
              }
          }
      }
  }

/*
 *  We put an annotation on to keep it separate from other types when
 *  installing.
 */
static void mark_type_unique(type_node *the_type)
  {
    static int mark_counter = 0;

    immed_list *mark_immeds = new immed_list;
    mark_immeds->append(immed(mark_counter));
    the_type->append_annote(k_linksuif_temp_mark, mark_immeds);
    ++mark_counter;
  }

static type_node *merge_common_types(type_node *type1, type_node *type2,
                                     field_rename_list *rename_list)
  {
    if (!type1->is_struct())
        error_line(1, type1, "non-structure type for common block");
    if (!type2->is_struct())
        error_line(1, type2, "non-structure type for common block");

    struct_type *group1 = (struct_type *)type1;
    struct_type *group2 = (struct_type *)type2;

    /*
     *  If a common block is declared in a file that only initializes
     *  it and has no procedures, fixfortran will have no way to
     *  restore the type information, so the type will remain a C
     *  ``struct'' or ``union'' type instead of a group type.  But
     *  this is the only way a ``struct'' or ``union'' type will still
     *  be on a common block, and in this case the ``struct'' or
     *  ``union'' type contains no real information about how the
     *  variable is used, so we can safely throw away that type.
     */
    if (group1->op() != TYPE_GROUP)
        return group2;
    if (group2->op() != TYPE_GROUP)
        return group1;

    int new_size = group1->size();
    if (group2->size() > new_size)
        new_size = group2->size();

    unsigned num_fields = group1->num_fields();

    struct_type *new_group =
            new struct_type(TYPE_GROUP, new_size, group1->name(), num_fields);

    for (unsigned field_num1 = 0; field_num1 < num_fields; ++field_num1)
      {
        new_group->set_field_name(field_num1, group1->field_name(field_num1));
        new_group->set_field_type(field_num1, group1->field_type(field_num1));
        new_group->set_offset(field_num1, group1->offset(field_num1));
      }

    unsigned num_fields2 = group2->num_fields();
    for (unsigned field_num2 = 0; field_num2 < num_fields2; ++field_num2)
      {
        const char *this_field_name = group2->field_name(field_num2);
        type_node *this_field_type = group2->field_type(field_num2);
        int this_offset = group2->offset(field_num2);

        boolean rename_needed = FALSE;
        unsigned field_num;
        for (field_num = 0; field_num < num_fields; ++field_num)
          {
            if (strcmp(this_field_name, new_group->field_name(field_num)) == 0)
              {
                if ((this_field_type != new_group->field_type(field_num)) ||
                    (this_offset != new_group->offset(field_num)))
                  {
                    rename_needed = TRUE;
                    field_num = num_fields;
                  }
                break;
              }
          }

        if (rename_needed)
          {
            char *test_name =
                    new char[strlen(this_field_name) + sizeof(int) * 3 + 3];
            sprintf(test_name, "%s_", this_field_name);
            char *num_place = &(test_name[strlen(test_name)]);
            int test_num = 0;
            while (TRUE)
              {
                sprintf(num_place, "%d", test_num);
                field_num = new_group->find_field_by_name(test_name);
                if (field_num >= num_fields)
                    break;
                if ((this_field_type == new_group->field_type(field_num)) &&
                    (this_offset == new_group->offset(field_num)))
                  {
                    break;
                  }
                ++test_num;
              }
            const char *new_field_name = lexicon->enter(test_name)->sp;
            delete[] test_name;

            field_rename rename_item;
            rename_item.old_name = this_field_name;
            rename_item.new_name = new_field_name;
            rename_item.the_type = group2;
            rename_list->append(rename_item);

            this_field_name = new_field_name;
          }

        if (field_num >= num_fields)
          {
            field_num = num_fields;
            ++num_fields;
            new_group->set_num_fields(num_fields);
            new_group->set_field_name(field_num, this_field_name);
            new_group->set_field_type(field_num, this_field_type);
            new_group->set_offset(field_num, this_offset);
          }
      }

    return group1->parent()->install_type(new_group);
  }

static void fix_fields_on_proc(tree_proc *the_proc,
                               field_rename_list *rename_list)
  {
    if (rename_list->is_empty())
        return;
    the_proc->map(&fix_fields_on_node, rename_list);
  }

static void fix_fields_on_node(tree_node *the_node, void *data)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        the_tree_instr->instr_map(&fix_fields_on_instr, data);
      }
  }

static void fix_fields_on_instr(instruction *the_instr, void *data)
  {
    field_rename_list *rename_list = (field_rename_list *)data;
    annote *fields_annote = the_instr->annotes()->peek_annote(k_fields);
    if (fields_annote == NULL)
        return;
    immed_list *fields_immeds = fields_annote->immeds();

    immed_list_iter fields_iter(fields_immeds);
    while (!fields_iter.is_empty())
      {
        immed fields_immed = fields_iter.step();
        if (!fields_immed.is_string())
          {
            error_line(1, the_instr, "badly formed \"%s\" annotation",
                       k_fields);
          }
        const char *field_string = fields_immed.string();
        field_rename_list_iter rename_iter(rename_list);
        while (!rename_iter.is_empty())
          {
            field_rename this_item = rename_iter.step();
            if (this_item.old_name == field_string)
                break;
          }
        if (!rename_iter.is_empty())
            break;
      }

    if (fields_iter.is_empty())
        return;

    type_node *starting_type;
    switch (the_instr->opcode())
      {
        case io_cpy:
        case io_cvt:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            starting_type = the_rrr->src_op().type();
            break;
          }
        case io_add:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            if (the_rrr->src1_op().type()->unqual()->is_ptr())
                starting_type = the_rrr->src1_op().type();
            else
                starting_type = the_rrr->src2_op().type();
            break;
          }
        case io_sub:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            starting_type = the_rrr->src1_op().type();
            break;
          }
        case io_ldc:
          {
            in_ldc *the_ldc = (in_ldc *)the_instr;
            immed value = the_ldc->value();
            if ((!value.is_symbol()) || (!value.symbol()->is_var()))
              {
                error_line(1, the_instr,
                           "\"%s\" annotation on ldc of non-variable",
                           k_fields);
              }
            var_sym *the_var = (var_sym *)(value.symbol());
            starting_type = the_var->type()->ptr_to();
            break;
          }
        case io_array:
          {
            in_array *the_array = (in_array *)the_instr;
            type_node *base_type = the_array->base_op().type()->unqual();
            if (!base_type->is_ptr())
              {
                error_line(1, the_instr,
                           "base operand of array instruction has "
                           "non-pointer type");
              }
            ptr_type *base_ptr = (ptr_type *)base_type;
            type_node *elem_type = base_ptr->ref_type();

            unsigned dims_left = the_array->dims();
            while (dims_left > 0)
              {
                elem_type = elem_type->unqual();
                if (!elem_type->is_array())
                  {
                    error_line(1, the_instr,
                               "array instruction has more dimensions than "
                               "base type");
                  }
                array_type *elem_array = (array_type *)elem_type;
                elem_type = elem_array->elem_type();
                --dims_left;
              }

            starting_type = elem_type->ptr_to();
            break;
          }
        default:
            error_line(1, the_instr,
                       "\"%s\" annotation on illegal instruction", k_fields);
            return;
      }

    starting_type = starting_type->unqual();
    if (!starting_type->is_ptr())
      {
        error_line(1, the_instr, "\"%s\" annotation on non-pointer base type",
                   k_fields);
      }
    ptr_type *starting_ptr = (ptr_type *)starting_type;

    type_node *current_type = starting_ptr->ref_type()->unqual();
    immed_list_e *current_e = fields_immeds->head();
    while (current_e != NULL)
      {
        immed fields_immed = current_e->contents;
        if (!fields_immed.is_string())
          {
            error_line(1, the_instr, "badly formed \"%s\" annotation",
                       k_fields);
          }
        const char *field_string = fields_immed.string();

        field_rename_list_iter rename_iter(rename_list);
        while (!rename_iter.is_empty())
          {
            field_rename this_item = rename_iter.step();
            if ((this_item.the_type == current_type) &&
                (this_item.old_name == field_string))
              {
                current_e->contents = immed(this_item.new_name);
                break;
              }
          }

        if (!current_type->is_struct())
          {
            error_line(1, the_instr, "\"%s\" annotation on non-structure type",
                       k_fields);
          }
        struct_type *this_struct = (struct_type *)current_type;
        unsigned field_num = this_struct->find_field_by_name(field_string);
        if (field_num >= this_struct->num_fields())
          {
            error_line(1, the_instr,
                       "no field matches \"%s\" annotation \"%s\"", k_fields,
                       field_string);
          }
        current_type = this_struct->field_type(field_num)->unqual();

        current_e = current_e->next();
      }
  }

/* Check to see if a type that is supposed to have a name actually has
 * been given a dummy name by snoot because it is anonymous: */
static boolean is_anonymous(type_node *the_type)
  {
    const char *name = NULL;
    switch (the_type->op())
      {
        case TYPE_ENUM:
	  {
	    name = ((enum_type *)the_type)->name();
	    assert(name != NULL);
	    break;
	  }
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
        case TYPE_PTR:
        case TYPE_ARRAY:
        case TYPE_FUNC:
	  {
	    break;
	  }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
	    name = ((struct_type *)the_type)->name();
	    assert(name != NULL);
	    break;
          }
        default:
            assert(FALSE);
	    break;
      }
    if(name)
      {
        return (isdigit(name[0]) || name[0] == '\0');
      }
    else
      {
	return FALSE;
      }
  }


static boolean has_cast(sym_node *the_sym, type_node **old_type)
  {
    annote *cast_annote =
            the_sym->annotes()->peek_annote(k_linksuif_temp_cast);
    if (cast_annote == NULL)
        return FALSE;
    immed the_immed = cast_annote->immeds()->head()->contents;
    assert(the_immed.is_type());
    *old_type = the_immed.type();
    return TRUE;
  }

static void enter_cast(sym_node *the_sym, type_node *old_type)
  {
    cast_list->append(the_sym);
    the_sym->append_annote(k_linksuif_temp_cast, new immed_list(old_type));
  }

static void init_casts(void)
  {
    cast_list = new sym_node_list;
  }

static void clear_casts(void)
  {
    while (!cast_list->is_empty())
      {
        sym_node *this_sym = cast_list->pop();
        annote *old_annote =
                this_sym->annotes()->get_annote(k_linksuif_temp_cast);
        assert(old_annote != NULL);
        delete old_annote;
      }
    delete cast_list;
    cast_list = NULL;
  }

static boolean casts_found(void)
  {
    return (!(cast_list->is_empty()));
  }
