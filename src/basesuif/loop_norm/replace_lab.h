#ifndef REPLACE_LABEL_H

#include <suif1.h>
#include <suif1_visitor.h>

class replace_dest_label_visitor : public suif1_visitor {
  label_sym *_new_lab;
  label_sym *_old_lab;
public:
  replace_dest_label_visitor(label_sym *new_lab, label_sym *old_lab) {
    _new_lab = new_lab;
    _old_lab = old_lab;
  }
  void post_process_instruction(instruction *in) {
    if (in->format() == inf_bj) {
      in_bj *inb = (in_bj*)in;
      if (inb->target() == _old_lab) {
	inb->set_target(_new_lab);
      }
    } else if (in->format() == inf_mbr) {
      in_mbr *inm = (in_mbr*)in;
      unsigned n;
      for (n = 0; n < inm->num_labs(); n++) {
	if (inm->label(n) == _old_lab) {
	  inm->set_label(n, _new_lab);
	}
      }
    }
  }
};

#endif /* REPLACE_LABEL_H */

