/* file loop_norm.cc */

/*  Copyright (c) 1997 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 * PURPOSE OF THE PASS GOES HERE
 * remove all of the possibly side-effecting stuff from
 * The do{ } while loop test so that other passes will
 * work correctly.
 */

#define RCS_BASE_FILE outline_cc

#include <string.h>
#include <suif1_visitor.h>
#include <useful.h>
#include "replace_lab.h"
#include <check.h>

RCS_BASE(
    "$Id: loop_norm.cc,v 1.2 1999/08/25 03:27:21 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/

static boolean verbose_loop = FALSE;
static boolean g_clean = TRUE;
static boolean g_do_if = TRUE;

/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/
static void usage(void);
static void add_all_files(int argc, char **argv, boolean has_output);
/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    Begin Visitor Classes
 *----------------------------------------------------------------------*/

// globals for the speculate id
// int speculate_id_num = 0;

class loop_norm_visitor : public suif1_visitor {
private:
  //  tree_node_list_base *tnlb;
  
public:
  loop_norm_visitor() { 
    //    tnlb = new tree_node_list_base;
  }

  boolean do_flush_proc() { return FALSE; }
  boolean do_write_proc() { return TRUE; }

  boolean do_process_block() { return TRUE; }
  boolean do_process_symtabs() { return FALSE; }

  boolean do_process_instruction() { return FALSE; }

  void post_process_block(tree_loop *the_loop) { 
    tree_node_list *test_list = the_loop->test();
    tree_node *test_node = 
      single_effect(test_list);
    if (test_node == NULL) {
    

      if (g_clean) {
	// Must be clean for g_clean case
	//	checkfail = TRUE;
	//	check_node(the_loop);
      }
      // OK, create a new label
      label_sym *old_contlab = the_loop->contlab();
      label_sym *new_contlab = 
	the_loop->proc()->block()->symtab()->new_unique_label();
      
      if (verbose_loop) {
	test_list->print(stderr);
      }
      instruction *i = new in_lab(new_contlab);
      the_loop->body()->append(new tree_instr(i));
      
      // Move any interesting tree instrs
      move_non_branches(test_list,
			the_loop->body(),
			the_loop->toplab());
      
      // rename continue labels;
      replace_dest_label_visitor vis(new_contlab, old_contlab);
      vis.process_block_list(the_loop->body());
    }
    // Now, if the loop condition is 0, break it down.
    break_down_null_loop(the_loop);
    

  }

  void post_process_block(tree_if *the_if) { 
    if (!g_do_if)  return;
    tree_node_list *test_list = the_if->header();
    tree_node *test_node = 
      single_effect(test_list);
    if (test_node == NULL) {
      if (g_clean) {
	// Must be clean for g_clean case
	//	checkfail = TRUE;
	//	check_node(the_if);
      }
      // OK, create a new label
      //      label_sym *old_contlab = the_loop->contlab();
      //      label_sym *new_contlab = 
      //	the_loop->proc()->block()->symtab()->new_unique_label();
      
      if (verbose_loop) {
	test_list->print(stderr);
      }
      //      instruction *i = new in_lab(new_contlab);
      //      the_loop->body()->append(new tree_instr(i));
      
      // Move any interesting tree instrs
      move_non_branches_before(test_list,
			       the_if,
			       the_if->jumpto());
      
      // rename continue labels;
      //      replace_dest_label_visitor vis(new_contlab, old_contlab);
      //      vis.process_block_list(the_loop->body());
    }
    // Now, if the loop condition is 0, break it down.
    //    break_down_null_if(the_loop);
    

  }

  void break_down_null_loop(tree_loop *the_loop) {
    tree_node_list *test_list = the_loop->test();
    if (!test_list->is_empty()) {
      return;
    }
    // Tear it down: Use useful.
    dismantle(the_loop);
  }

      class has_jump_visitor : public suif1_visitor {
	label_sym *_target_label;
      public:
	boolean _has_jump;
	has_jump_visitor(label_sym *target_label) {
	  _target_label = target_label;
	  _has_jump = FALSE;
	}
	  
	boolean do_walk_instruction() { return(TRUE); }
	void pre_process_instruction(instruction *i) {
	  if (g_clean) { // assume that we have NO non-local branches.
	    if (i->format() == inf_bj) {
	      in_bj *bj = (in_bj*)i;
	      if (bj->target() == _target_label) {
		_has_jump = TRUE;
	      }
	    }
	  } else {
	    if (i->is_branch()) {
	      _has_jump = TRUE;
	    }
	  }
	}
      };


  void move_non_branches(tree_node_list *from_list,
			 tree_node_list *to_list,
			 label_sym *target_label) {
    
    while(!from_list->is_empty()) {
      tree_node *next = from_list->head()->contents;
	
      // Assume that all jumps will be
      // within this scope if g_clean is set
      if ((!g_clean) ||
	  next->is_instr()) {
	has_jump_visitor vis(target_label);
	vis.process_block(next);
	if (vis._has_jump) {
	  return;
	}
      }

      // Otherwise move it;
      from_list->remove(next->list_e());
      to_list->append(next);
    }
  }

  void move_non_branches_before(
				tree_node_list *from_list,
				tree_node *to_pos,
				label_sym *target_label) {
    
    while(!from_list->is_empty()) {
      tree_node *next = from_list->head()->contents;
	
      // Assume that all jumps will be
      // within this scope if g_clean is set
      if ((!g_clean) ||
	  next->is_instr()) {
	has_jump_visitor vis(target_label);
	vis.process_block(next);
	if (vis._has_jump) {
	  return;
	}
      }

      // Otherwise move it;
      from_list->remove(next->list_e());
      // use the useful function
      insert_tree_node_before(next, to_pos);
    }
  }
      
};



  


/*----------------------------------------------------------------------*
    End Visitor Classes
 *----------------------------------------------------------------------*/
  
int main(int argc, char *argv[])
{
  // Auto initializers
  start_suif(argc, argv);

  // Parse command line
   static cmd_line_option option_table[] =
   {
         {CLO_NOARG, "-v", NULL, &verbose_loop}
         // {CLO_NOARG, "-clean", NULL, &g_clean},
         // {CLO_NOARG, "-do-if", NULL, &g_do_if}
   };
   parse_cmd_line(argc, argv, option_table,
 		 sizeof(option_table) / sizeof(cmd_line_option));

  add_all_files(argc, argv, TRUE);

  loop_norm_visitor vis;
  vis.process_fileset(fileset);

  exit_suif();
  return(0);
}

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/


/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr,
            "usage: %s { options } <source filespec> <destination filespec>\n"
            "       %*s         { <source filespec> <destination filespec> }"
            " *\n", _suif_program_name, (int)(strlen(_suif_program_name)),
            " ");
    fprintf(stderr,
	    "      options    ::\n"
	    "                 :: -v verbose\n"
	    );
    exit(2);
  }

static void add_all_files(int argc, char **argv, boolean has_output) {
  if (has_output) {
    if ((argc < 3) || ((argc % 2) != 1))
      usage();

    // Add all of the files
    for (int arg_num = 1; arg_num < argc; arg_num += 2)
      fileset->add_file(argv[arg_num], argv[arg_num+1]);
    return;
  } 
  
  if (argc < 2)
    usage();

  // Add all of the files
  for (int arg_num = 1; arg_num < argc; arg_num += 1)
    fileset->add_file(argv[arg_num], NULL);
}

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/

