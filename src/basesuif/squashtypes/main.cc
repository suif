/* file "main.cc" */

/*  Copyright (c) 1994, 1995, 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 * This is the main program for the squashtypes program for the SUIF system.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:18:56 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage_error(void);
static void do_proc(tree_proc *the_tree_proc);
static void do_node(tree_node *the_node, void *);
static void squashtypes(base_symtab *the_symtab);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    if (argc % 2 != 1)
        usage_error();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            do_proc(this_proc_sym->block());
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
        squashtypes(fse->symtab());
      }

    squashtypes(fileset->globals());
    delete fileset;

    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage_error(void)
  {
    fprintf(stderr,
            "usage: %s <source filespec> <destination filespec>\n"
            "    %*s  { <source filespec> <destination filespec> } *\n",
            _suif_program_name, (int)(strlen(_suif_program_name)), " ");
    exit(1);
  }

static void do_proc(tree_proc *the_tree_proc)
  {
    the_tree_proc->map(&do_node, NULL, FALSE);
    do_node(the_tree_proc, NULL);
  }

static void do_node(tree_node *the_node, void *)
  {
    if (the_node->is_block())
      {
        tree_block *the_block = (tree_block *)the_node;
        squashtypes(the_block->symtab());
      }
  }

static void squashtypes(base_symtab *the_symtab)
  {
    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (this_type->op() == TYPE_GROUP)
          {
            struct_type *this_struct = (struct_type *)this_type;
            unsigned num_fields = this_struct->num_fields();
            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                type_node *field_type = this_struct->field_type(field_num);
                if (field_type->op() == TYPE_GROUP)
                  {
                    struct_type *inner_struct = (struct_type *)field_type;
                    if (inner_struct->num_fields() > 0)
                      {
                        unsigned inner_count = inner_struct->num_fields();
                        this_struct->set_num_fields(num_fields + inner_count -
                                                    1);
                        this_struct->set_field_name(field_num,
                                inner_struct->field_name(0));
                        this_struct->set_field_type(field_num,
                                inner_struct->field_type(0));
                        this_struct->set_offset(field_num,
                                                inner_struct->offset(0));
                        for (unsigned inner_num = 1; inner_num < inner_count;
                             ++inner_num)
                          {
                            unsigned new_num = num_fields + inner_num - 1;
                            this_struct->set_field_name(new_num,
                                    inner_struct->field_name(inner_num));
                            this_struct->set_field_type(new_num,
                                    inner_struct->field_type(inner_num));
                            this_struct->set_offset(new_num,
                                                    inner_struct->offset(0));
                          }
                        num_fields += (inner_count - 1);
                        --field_num;
                      }
                  }
              }
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
