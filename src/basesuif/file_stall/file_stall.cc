/* file "file_stall.cc" */

/* by Chris Wilson */

/* 1/23/1996 */


#define boolean old_boolean

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#undef boolean


typedef int boolean;
#define TRUE 1
#define FALSE 0

#define max(a, b) ((a >= b) ? (a) : (b))
#define min(a, b) ((a >= b) ? (b) : (a))

#define SECS_PER_MINUTE (60.0)
#define SECS_PER_HOUR (60.0 * 60.0)
#define SECS_PER_DAY (60.0 * 60.0 * 24.0)
#define SECS_PER_YEAR (60.0 * 60.0 * 24.0 * 365.24)


static char *prog_name = NULL;
static char *user_event_name = NULL;
static char *check_file_name = NULL;
static boolean silent = FALSE;


static void usage(void);
static void do_stall(void);
static boolean check_for_file(void);


extern int main(int argc, char *argv[])
  {
    prog_name = argv[0];
    boolean end_of_flags = FALSE;
    for (int arg_num = 1; arg_num < argc; ++arg_num)
      {
        if (end_of_flags || (argv[arg_num][0] != '-'))
          {
            if (check_file_name != NULL)
              {
                fprintf(stderr, "%s: too many files name specified\n",
                        argv[0]);
                usage();
              }
            check_file_name = argv[arg_num];
            if (user_event_name == NULL)
                user_event_name = check_file_name;
          }
        else
          {
            if (strcmp(argv[arg_num], "--") == 0)
              {
                end_of_flags = TRUE;
              }
            else if (strcmp(argv[arg_num], "-name") == 0)
              {
                ++arg_num;
                if (arg_num >= argc)
                  {
                    fprintf(stderr,
                            "%s: `-name' flag with no name specified\n",
                            argv[0]);
                    usage();
                  }
                user_event_name = argv[arg_num];
              }
            else if (strcmp(argv[arg_num], "-silent") == 0)
              {
                silent = TRUE;
              }
            else
              {
                fprintf(stderr, "%s: unknown flag `%s'\n", argv[0],
                        argv[arg_num]);
                usage();
              }
          }
      }

    if (check_file_name == NULL)
      {
        fprintf(stderr, "%s: no file name specified\n", argv[0]);
        usage();
      }

    do_stall();
    return 0;
  }


static void usage(void)
  {
    fprintf(stderr, "\nUsage: %s { <flag> }* <filename>\n", prog_name);
    fprintf(stderr, "Flags:\n");
    fprintf(stderr, "    -name <name>  output name for this event\n");
    fprintf(stderr, "    -silent       print nothing if there is no stall\n");
    fprintf(stderr, "    --            end of flags\n");
    exit(1);
  }

static void do_stall(void)
  {
    boolean found_at_start = check_for_file();
    if (found_at_start)
      {
        if (!silent)
            fprintf(stdout, "--- No stall for %s.\n", user_event_name);
        return;
      }

    fprintf(stdout, "--- Stalling for %s ...", user_event_name);
    fflush(stdout);

    time_t start_time = time(NULL);
    unsigned wait_time = 0;
    while (TRUE)
      {
        unsigned interval = max(1, min(30, (unsigned)(wait_time / 20)));
        sleep(interval);
        boolean done = check_for_file();
        if (done)
          {
            time_t finish_time = time(NULL);
            double elapsed_seconds = difftime(finish_time, start_time);
            fprintf(stdout, " done.  Stalled ");
            boolean mention_seconds = (elapsed_seconds < SECS_PER_DAY);
            boolean mention_fractions = (elapsed_seconds < SECS_PER_MINUTE);
            boolean comma_needed = FALSE;
            if (elapsed_seconds >= SECS_PER_YEAR)
              {
                double years = elapsed_seconds / SECS_PER_YEAR;
                fprintf(stdout, "%d year", (int)(years));
                if ((int)(years) != 1)
                    fprintf(stdout, "s");
                elapsed_seconds -= ((int)(years)) * SECS_PER_YEAR;
                comma_needed = TRUE;
              }
            if (elapsed_seconds >= SECS_PER_DAY)
              {
                double days = elapsed_seconds / SECS_PER_DAY;
                if (comma_needed)
                    fprintf(stdout, ", ");
                fprintf(stdout, "%d day", (int)(days));
                if ((int)(days) != 1)
                    fprintf(stdout, "s");
                elapsed_seconds -= ((int)(days)) * SECS_PER_DAY;
                comma_needed = TRUE;
              }
            if (elapsed_seconds >= SECS_PER_HOUR)
              {
                double hours = elapsed_seconds / SECS_PER_HOUR;
                if (comma_needed)
                    fprintf(stdout, ", ");
                fprintf(stdout, "%d hour", (int)(hours));
                if ((int)(hours) != 1)
                    fprintf(stdout, "s");
                elapsed_seconds -= ((int)(hours)) * SECS_PER_HOUR;
                comma_needed = TRUE;
              }
            if ((elapsed_seconds >= SECS_PER_MINUTE) || (!mention_seconds))
              {
                double minutes = elapsed_seconds / SECS_PER_MINUTE;
                if (comma_needed)
                    fprintf(stdout, ", ");
                fprintf(stdout, "%d minute", (int)(minutes));
                if ((int)(minutes) != 1)
                    fprintf(stdout, "s");
                elapsed_seconds -= ((int)(minutes)) * SECS_PER_MINUTE;
                comma_needed = TRUE;
              }
            if (mention_seconds)
              {
                if (comma_needed)
                    fprintf(stdout, ", ");
                if (mention_fractions)
                  {
                    fprintf(stdout, "%.2f seconds", elapsed_seconds);
                  }
                else
                  {
                    fprintf(stdout, "%.0f second", elapsed_seconds);
                    if ((int)(elapsed_seconds) != 1)
                        fprintf(stdout, "s");
                  }
              }
            fprintf(stdout, ".\n");
            return;
          }
        if (wait_time < 600)
            wait_time += interval;
      }
  }

static boolean check_for_file(void)
  {
    FILE *fp = fopen(check_file_name, "r");
    if (fp == NULL)
      {
        return FALSE;
      }
    else
      {
        fclose(fp);
        return TRUE;
      }
  }
