/* file "chain_arefs.cc" */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to chain together multiple in_array instructions in series
 * into a single in_array instruction for the porky program for SUIF
 */


#define RCS_BASE_FILE chain_arefs_cc

#include "porky.h"
#include <limits.h>

RCS_BASE(
    "$Id: chain_arefs.cc,v 1.2 1999/08/25 03:27:24 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

DECLARE_DLIST_CLASS(in_array_list, in_array *);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static in_array_list *aref_list = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void find_arefs(suif_object *the_object);
static void process_list(void);
static void chain_on_aref(in_array *the_aref);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void chain_arefs_on_proc(tree_proc *the_proc)
  {
    assert(aref_list == NULL);
    aref_list = new in_array_list;
    walk(the_proc, &find_arefs);
    process_list();
    delete aref_list;
    aref_list = NULL;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void find_arefs(suif_object *the_object)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_array)
        return;
    in_array *the_array = (in_array *)the_instr;
    aref_list->append(the_array);
  }

static void process_list(void)
  {
    while (!aref_list->is_empty())
      {
        in_array *this_aref = aref_list->pop();
        chain_on_aref(this_aref);
      }
  }

static void chain_on_aref(in_array *the_aref)
  {
    if (the_aref->offset() != 0)
        return;

    unsigned first_dims = the_aref->dims();
    type_node *base_type = the_aref->base_op().type();
    assert(base_type->is_ptr());
    ptr_type *base_ptr = (ptr_type *)base_type;
    type_node *follow_type = base_ptr->ref_type();
      {
        for (unsigned dim_num = 0; dim_num < first_dims; ++dim_num)
          {
            follow_type = follow_type->unqual();
            assert(follow_type->is_array());
            array_type *this_array_type = (array_type *)follow_type;
            follow_type = this_array_type->elem_type();
          }
      }
    type_node *first_elem_type = follow_type;
    if (first_elem_type->size() != 0)
        assert((unsigned)first_elem_type->size() == (unsigned)the_aref->elem_size());

    instruction *follow_instr = the_aref;
    in_array *other_aref;
    while (TRUE)
      {
        operand dest_op = follow_instr->dst_op();
        if (!dest_op.is_instr())
            return;
        instruction *dest_instr = dest_op.instr();
        if (dest_instr->opcode() == io_array)
          {
            other_aref = (in_array *)dest_instr;
            break;
          }
        else if ((dest_instr->opcode() == io_cvt) ||
                 (dest_instr->opcode() == io_cpy))
          {
            if (!dest_instr->result_type()->is_ptr())
                return;
            follow_instr = dest_instr;
          }
        else
          {
            return;
          }
      }
    if (other_aref->base_op() != operand(follow_instr))
        return;

    type_node *other_base_type = other_aref->base_op().type();
    assert(other_base_type->is_ptr());
    ptr_type *other_base_ptr = (ptr_type *)other_base_type;
    type_node *other_base_array_type = other_base_ptr->ref_type();
    if (other_base_array_type->unqual() != first_elem_type->unqual())
        return;

    unsigned other_dims = other_aref->dims();
    if (UINT_MAX - first_dims < other_dims)
        return;
    if (other_aref->bound(0).is_null())
      {
        type_node *other_array_unqual = other_base_array_type->unqual();
        assert(other_array_unqual->is_array());
        array_type *other_array_type = (array_type *)other_array_unqual;
        int this_size = other_array_type->size();
        if (this_size == 0)
            return;
        int elem_size = other_array_type->elem_type()->size();
        assert(elem_size != 0);
        immed count_immed = immed(this_size / elem_size);
        other_aref->set_bound(0, const_op(count_immed, type_ptr_diff));
      }
    other_aref->set_dims(other_dims + first_dims);
      {
        for (unsigned dim_num = (other_dims + first_dims) - 1;
             dim_num >= first_dims; --dim_num)
          {
            operand index_op = other_aref->index(dim_num - first_dims);
            index_op.remove();
            other_aref->set_index(dim_num, index_op);
            operand bound_op = other_aref->bound(dim_num - first_dims);
            bound_op.remove();
            other_aref->set_bound(dim_num, bound_op);
          }
      }
      {
        for (unsigned dim_num = 0; dim_num < first_dims; ++dim_num)
          {
            operand index_op = the_aref->index(dim_num);
            index_op.remove();
            other_aref->set_index(dim_num, index_op);
            operand bound_op = the_aref->bound(dim_num);
            bound_op.remove();
            other_aref->set_bound(dim_num, bound_op);
          }
      }
    operand old_base = other_aref->base_op();
    old_base.remove();
    operand new_base = the_aref->base_op();
    new_base.remove();
    other_aref->set_base_op(new_base);

    operand new_offset_op = the_aref->offset_op();
    if (!new_offset_op.is_null())
      {
        new_offset_op.remove();
        for (unsigned dim_num = first_dims; dim_num < other_dims + first_dims;
             ++dim_num)
          {
            new_offset_op *= the_aref->bound(dim_num).clone();
          }
        operand old_offset_op = other_aref->offset_op();
        old_offset_op.remove();
        other_aref->set_offset_op(new_offset_op + old_offset_op);
      }
    kill_op(old_base);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
