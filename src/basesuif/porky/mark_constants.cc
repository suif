/* file "mark_constants.cc" */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to mark constant static variables for the porky program for
 * SUIF */


#define RCS_BASE_FILE mark_constants_cc

#include "porky.h"

RCS_BASE(
    "$Id: mark_constants.cc,v 1.2 1999/08/25 03:27:32 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_mark_constants_orig_constant;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void pre_mark_constants_on_object(suif_object *the_object);
static void mark_non_const(var_sym *the_var);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_mark_constants(void)
  {
    ANNOTE(k_porky_mark_constants_orig_constant,
           "porky mark constants orig constant", FALSE);
  }

extern void mark_constants_on_proc(tree_proc *the_proc)
  {
    so_walker the_walker;
    the_walker.set_pre_function(&pre_mark_constants_on_object);
    the_walker.walk(the_proc);
  }

extern void mark_constants_start_symtab(base_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (!this_sym->is_var())
            continue;
        var_sym *this_var = (var_sym *)this_sym;
        if (this_var->peek_annote(k_is_constant) != NULL)
            this_var->append_annote(k_porky_mark_constants_orig_constant);
        if ((!this_var->is_auto()) && unreferenced_outside_fileset(this_var))
            this_var->append_annote(k_is_constant);
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void pre_mark_constants_on_object(suif_object *the_object)
  {
    if (the_object->is_tree_obj())
      {
        tree_node *the_node = (tree_node *)the_object;
        if (the_node->is_block())
          {
            tree_block *the_block = (tree_block *)the_node;
            mark_constants_start_symtab(the_block->symtab());
          }
      }
    else if (the_object->is_instr_obj())
      {
        instruction *the_instr = (instruction *)the_object;
        operand dest_op = the_instr->dst_op();
        if (dest_op.is_symbol())
            mark_non_const(dest_op.symbol());
        if (the_instr->opcode() == io_ldc)
          {
            in_ldc *the_ldc = (in_ldc *)the_instr;
            immed value = the_ldc->value();
            if (value.is_symbol())
              {
                sym_node *value_sym = value.symbol();
                if (value_sym->is_var())
                  {
                    var_sym *value_var = (var_sym *)value_sym;
                    mark_non_const(value_var);
                  }
              }
          }
      }
  }

static void mark_non_const(var_sym *the_var)
  {
    if (the_var->peek_annote(k_porky_mark_constants_orig_constant) != NULL)
        return;
    annote *old_const_annote = the_var->annotes()->get_annote(k_is_constant);
    if (old_const_annote != NULL)
        delete old_const_annote;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
