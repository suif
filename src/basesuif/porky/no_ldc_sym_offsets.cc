/* file "no_ldc_sym_offsets.cc" */

/*  Copyright (c) 1997 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to dismantle ldc instructions of symbol immeds with non-zero
 * offsets, for the porky program for SUIF */


#define RCS_BASE_FILE no_ldc_sym_offsets_cc

#include "porky.h"

RCS_BASE(
    "$Id: no_ldc_sym_offsets.cc,v 1.2 1999/08/25 03:27:32 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void no_ldc_sym_offsets_on_object(suif_object *the_object,
                                         so_walker *the_walker);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void no_ldc_sym_offsets_on_proc(tree_proc *the_proc)
  {
    so_walker the_walker;
    the_walker.set_post_function(&no_ldc_sym_offsets_on_object);
    the_walker.walk(the_proc);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void no_ldc_sym_offsets_on_object(suif_object *the_object,
                                         so_walker *the_walker)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_ldc)
        return;
    in_ldc *the_ldc = (in_ldc *)the_instr;
    immed value = the_ldc->value();
    if (!value.is_symbol())
        return;
    if (value.offset() == 0)
        return;
    in_rrr *new_addition =
            new in_rrr(io_add, the_ldc->result_type(), operand(), operand(),
                       const_op(immed(value.offset() /
                                      target.addressable_size),
                                type_ptr_diff));
    instruction *new_instr;
    if (the_ldc->result_type()->is_ptr())
      {
        new_instr = new_addition;
      }
    else
      {
        new_addition->set_result_type(type_void->ptr_to());
        new_instr =
                new in_rrr(io_cvt, the_ldc->result_type(), operand(),
                           operand(new_addition));
      }
    the_walker->replace_object(new_instr);
    the_ldc->set_result_type(addr_type(value.symbol()));
    the_ldc->set_value(immed(sym_addr(value.symbol(), 0)));
    new_addition->set_src1(operand(the_ldc));
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
