/* file "form_arrays.cc" */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to form arrays from k_form_array annotations for the porky
 * program for SUIF */


#define RCS_BASE_FILE form_arrays_cc

#include "porky.h"
#include <limits.h>
#include <string.h>

RCS_BASE(
    "$Id: form_arrays.cc,v 1.2 1999/08/25 03:27:28 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void add_replacement_annotes_on_object(suif_object *the_object);
static void add_replacement_annotes_on_symtab(base_symtab *the_symtab);
static void remove_init_annotes(var_def *the_def);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void form_arrays_on_symtab(global_symtab *the_symtab)
  {
    add_replacement_annotes_on_symtab(the_symtab);
    do_replacement(the_symtab);
  }

extern void form_arrays_on_proc(tree_proc *the_proc)
  {
    walk(the_proc, &add_replacement_annotes_on_object);
    do_replacement(the_proc);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void add_replacement_annotes_on_object(suif_object *the_object)
  {
    if (!the_object->is_symtab_obj())
        return;
    base_symtab *the_symtab = (base_symtab *)the_object;
    add_replacement_annotes_on_symtab(the_symtab);
  }

static void add_replacement_annotes_on_symtab(base_symtab *the_symtab)
  {
    annote_list_iter annote_iter(the_symtab->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();
        if (strcmp(this_annote->name(), k_form_array) == 0)
          {
            immed_list *the_immeds = this_annote->immeds();
            int num_arrays = the_immeds->count();
            if (num_arrays > 0)
              {
                immed_list_iter immed_iter(the_immeds);
                immed first_immed = immed_iter.step();
                if (!first_immed.is_symbol())
                  {
                    error_line(1, the_symtab,
                               "bad format for \"%s\" annotation: non-symbol "
                               "as first data item", k_form_array);
                  }
                sym_node *first_sym = first_immed.symbol();
                if (!first_sym->is_var())
                  {
                    error_line(1, the_symtab,
                               "bad format for \"%s\" annotation: non-variable"
                               " symbol as first data item", k_form_array);
                  }
                var_sym *this_var = (var_sym *)first_sym;
                type_node *first_type = this_var->type();
                type_node *new_type =
                        new array_type(first_type, 0, num_arrays - 1);
                var_sym *new_var =
                        the_symtab->new_unique_var(new_type, "_combo_");
                var_def *new_def = NULL;
                if (!this_var->is_auto())
                  {
                    if (!this_var->has_var_def())
                      {
                        error_line(1, the_symtab,
                                   "bad format for \"%s\" annotation: first "
                                   "data item is not auto but has no var_def "
                                   "available", k_form_array);
                      }
                    var_def *old_def = this_var->definition();
                    new_def =
                            old_def->parent()->define_var(new_var,
                                    get_alignment(new_var->type()));
                  }
                int var_num = 0;
                i_integer fill_needed = 0;
                while (TRUE)
                  {
                    if (this_var->parent() != the_symtab)
                      {
                        error_line(1, the_symtab,
                                   "bad format for \"%s\" annotation: "
                                   "variable from a different symbol table "
                                   "as data item %d", k_form_array, var_num);
                      }
                    if (this_var->parent_var() != NULL)
                      {
                        error_line(1, the_symtab,
                                   "bad format for \"%s\" annotation: "
                                   "sub-variable as data item %d",
                                   k_form_array, var_num);
                      }
                    if (this_var->is_param())
                      {
                        error_line(1, the_symtab,
                                   "bad format for \"%s\" annotation: "
                                   "parameter as data item %d", k_form_array,
                                   var_num);
                      }

                    unsigned num_children = this_var->num_children();
                    while (num_children > 0)
                      {
                        var_sym *this_child = this_var->child_var(0);
                        int old_offset = this_child->offset();
                        this_var->remove_child(this_child);
                        int new_offset =
                                old_offset + (var_num * first_type->size());
                        new_var->add_child(this_child, new_offset);
                        --num_children;
                      }

                    if (new_def == NULL)
                      {
                        if (!this_var->is_auto())
                          {
                            error_line(1, the_symtab,
                                       "bad format for \"%s\" annotation: data"
                                       " item %d is not auto, but the first "
                                       "item is", k_form_array, var_num);
                          }
                      }
                    else
                      {
                        var_def *old_def = this_var->definition();
                        if (old_def == NULL)
                          {
                            error_line(1, the_symtab,
                                       "bad format for \"%s\" annotation: data"
                                       " item %d does not have a var_def "
                                       "available, but the first item does",
                                       k_form_array, var_num);
                          }
                        base_init_struct_list *old_init_data =
                                read_init_data(old_def);
                        if (old_init_data->is_empty())
                          {
                            fill_needed += first_type->size();
                          }
                        else
                          {
                            if (fill_needed > 0)
                              {
                                base_init_struct_list *new_data =
                                        new base_init_struct_list;
                                while (fill_needed > INT_MAX)
                                  {
                                    new_data->append(new fill_init_struct(
                                            INT_MAX, 0));
                                    fill_needed -= INT_MAX;
                                  }
                                new_data->append(new fill_init_struct(
                                        fill_needed.c_int(), 0));
                                write_init_data(new_def, new_data);
                                deallocate_init_data(new_data);
                              }
                            fill_needed = first_type->size();
                            base_init_struct_list_iter
                                    init_iter(old_init_data);
                            while (!init_iter.is_empty())
                              {
                                base_init_struct *this_init = init_iter.step();
                                fill_needed -= this_init->total_size();
                                if (fill_needed < 0)
                                  {
                                    error_line(1, old_def,
                                               "initialization data for `%s' "
                                               "is too big for the variable "
                                               "size", this_var->name());
                                  }
                              }
                            write_init_data(new_def, old_init_data);
                            remove_init_annotes(old_def);
                          }
                        deallocate_init_data(old_init_data);
                      }

                    in_array *new_aref =
                            new in_array(first_type->ptr_to(), operand(),
                                         addr_op(new_var), first_type->size(),
                                         1);
                    new_aref->set_index(0, const_op(immed(var_num),
                                                    type_ptr_diff));
                    new_aref->set_bound(0, operand());
                    immed_list *new_data = new immed_list(fold_load(new_aref));
                    this_var->append_annote(k_replacement, new_data);

                    if (immed_iter.is_empty())
                        break;
                    ++var_num;
                    immed next_immed = immed_iter.step();
                    if (!next_immed.is_symbol())
                      {
                        error_line(1, the_symtab,
                                   "bad format for \"%s\" annotation: "
                                   "non-symbol as data item %d", k_form_array,
                                   var_num);
                      }
                    sym_node *next_sym = next_immed.symbol();
                    if (!next_sym->is_var())
                      {
                        error_line(1, the_symtab,
                                   "bad format for \"%s\" annotation: "
                                   "non-variable symbol as data item %d",
                                   k_form_array, var_num);
                      }
                    this_var = (var_sym *)next_sym;
                    if (this_var->type() != first_type)
                      {
                        error_line(1, the_symtab,
                                   "bad format for \"%s\" annotation: variable"
                                   " for data item %d has a different type "
                                   "than that of the first item", k_form_array,
                                   var_num);
                      }
                  }
              }
          }
      }
  }

static void remove_init_annotes(var_def *the_def)
  {
    annote_list_e *follow = the_def->annotes()->head();
    while (follow != NULL);
      {
        annote_list_e *next = follow->next();
        annote *this_annote = follow->contents;
        if ((strcmp(this_annote->name(), k_multi_init) == 0) ||
            (strcmp(this_annote->name(), k_repeat_init) == 0) ||
            (strcmp(this_annote->name(), k_fill) == 0))
          {
            the_def->annotes()->remove(follow);
            delete follow;
            delete this_annote;
          }
        follow = next;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
