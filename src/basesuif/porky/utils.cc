/* file "utils.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE utils_cc

#include "porky.h"

RCS_BASE(
    "$Id: utils.cc,v 1.2 1999/08/25 03:27:36 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

int verbosity_level = 0;
boolean strength_reduce = FALSE;
boolean made_progress;
boolean merge_globals = TRUE;
boolean fast_structured_facts = FALSE;
boolean cse_no_pointers = FALSE;
int max_gele_split_depth = 5;

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void insert_tree_node_list_before(tree_node_list *to_insert,
                                         tree_node *before)
  {
    assert(to_insert != NULL);
    assert(before != NULL);

    tree_node_list *parent_list = before->parent();
    tree_node_list_e *list_place = before->list_e();
    parent_list->insert_before(to_insert, list_place);
  }

extern void append_tree_node_to_list(tree_node_list *the_list,
                                     tree_node *the_node)
  {
    assert(the_list != NULL);
    assert(the_node != NULL);

    the_list->append(the_node);
  }

/*
 *  This function returns the last node in a list that might have any effect
 *  when executed.  Mark instructions never have an effect, so they aren't
 *  counted here.  If there is no node that might have an effect, NULL is
 *  returned.
 */
extern tree_node *last_action_node(tree_node_list *the_node_list)
  {
    return last_action_before(the_node_list, NULL);
  }

/*
 *  This function is the same as last_action_node() except that it only
 *  considers nodes that come before the given node.  If the given node is
 *  NULL, that is considered the end of the list, so the result is the same as
 *  that of last_action_node().  If the given node is not on the list, NULL is
 *  returned.
 */
extern tree_node *last_action_before(tree_node_list *the_node_list,
                                     tree_node *the_node)
  {
    if (the_node_list == NULL)
        return NULL;

    if (the_node_list->is_empty())
        return NULL;

    tree_node_list_e *current_element = the_node_list->tail();

    if (the_node != NULL)
      {
        while (current_element != NULL)
          {
            tree_node_list_e *old_element = current_element;
            current_element = current_element->prev();
            if (old_element->contents == the_node)
                break;
          }
      }

    while (current_element != NULL)
      {
        tree_node *this_node = current_element->contents;
        switch (this_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)this_node;
                instruction *the_instr = the_tree_instr->instr();
                assert(the_instr != NULL);
                if ((the_instr->opcode() != io_mrk) &&
                    (the_instr->opcode() != io_nop))
                  {
                    return this_node;
                  }
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)this_node;
                tree_node *last_in_block = last_action_node(the_block->body());
                if (last_in_block != NULL)
                    return last_in_block;
              }
            default:
                return this_node;
          }
        current_element = current_element->prev();
      }
    return NULL;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
