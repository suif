/* file "structured_facts.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the interface to the code to find, propagate,
 *  and apply facts through structured control flow.
 */

#ifndef STRUCTURED_FACTS_H
#define STRUCTURED_FACTS_H

#include <suif1.h>
#include "fact.h"

RCS_HEADER(structured_facts_h,
     "$Id: structured_facts.h,v 1.2 1999/08/25 03:27:35 brm Exp $")

/*
 *  boolean use_kill_summary(void)
 *      If this method returns TRUE, then the
 *      propagated_fact::killed_by_var_write(),
 *      propagated_fact::killed_by_unknown_mem_write(), and
 *      propagated_fact::killed_by_arbitrary_function_call() methods
 *      are used; otherwise, the propagated_fact::killed_by_instr(),
 *      propagated_fact::killed_by_for_bounds(), and
 *      propagated_fact::killed_by_for_step() methods are used.  The
 *      latter set of method is more general, but for the cases in
 *      which the former are sufficient, the algorithm runs more
 *      efficiently and there is less problem-specific code to write.
 */
class fact_manager
  {
public:
    virtual void initial_fact_creator(propagated_fact_list *the_facts) = 0;
    virtual void instr_fact_creator(propagated_fact_list *the_facts,
                                    instruction *the_instr) = 0;
    virtual void bound_fact_creator(propagated_fact_list *the_facts,
                                    tree_for *the_for) = 0;
    virtual void step_fact_creator(propagated_fact_list *the_facts,
                                   tree_for *the_for) = 0;
    virtual void index_fact_creator(propagated_fact_list *the_facts,
                                    tree_for *the_for) = 0;
    virtual void act_on_facts(instruction *the_instr,
                              propagated_fact_list *fact_list) = 0;
    virtual void act_on_for_facts(tree_for *the_for,
                                  propagated_fact_list *fact_list) = 0;
    virtual boolean use_kill_summary(void) = 0;
  };

extern void apply_through_structured_control(tree_node_list *the_list,
                                             fact_manager *the_manager);

#endif
