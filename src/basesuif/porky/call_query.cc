/* file "call_query.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* detecting the structure of calls in an expression tree for the
 * porky program for SUIF */

#define RCS_BASE_FILE call_query_cc

#include "porky.h"

RCS_BASE(
    "$Id: call_query.cc,v 1.2 1999/08/25 03:27:24 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern call_order call_query(instruction *the_instr)
  {
    call_order result = NO_CALLS;

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_src = the_instr->src_op(src_num);
        if (this_src.is_expr())
          {
            call_order this_order = call_query(this_src.instr());
            if (this_order == CALLS_TOTALLY_ORDERED)
              {
                if (result == NO_CALLS)
                    result = CALLS_TOTALLY_ORDERED;
                else
                    result = CALLS_NOT_TOTALLY_ORDERED;
              }
            else if (this_order == CALLS_NOT_TOTALLY_ORDERED)
              {
                result = CALLS_NOT_TOTALLY_ORDERED;
              }
          }
      }

    if ((instr_is_impure_call(the_instr) || (the_instr->opcode() == io_gen)) &&
        (result == NO_CALLS))
      {
        result = CALLS_TOTALLY_ORDERED;
      }

    return result;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
