/* file "copy_form.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to change to and from copy form, where copy instructions are
 * the bases for all expression trees, for the porky program for SUIF
 */


#define RCS_BASE_FILE copy_form_cc

#include "porky.h"

RCS_BASE(
    "$Id: copy_form.cc,v 1.2 1999/08/25 03:27:26 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void to_copy_form_on_node(tree_node *the_node, void *);
static void from_copy_form_on_node(tree_node *the_node, void *);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void to_copy_form_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&to_copy_form_on_node, NULL);
  }

extern void from_copy_form_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&from_copy_form_on_node, NULL);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void to_copy_form_on_node(tree_node *the_node, void *)
  {
    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();

    if (the_instr->result_type()->unqual()->op() == TYPE_VOID)
        return;

    if (the_instr->opcode() == io_cpy)
      {
        in_rrr *the_cpy = (in_rrr *)the_instr;
        if (the_cpy->src_op().type() == the_cpy->result_type())
            return;
      }

    the_tree_instr->remove_instr(the_instr);
    operand dst_op = the_instr->dst_op();
    the_instr->set_dst(operand());
    in_rrr *new_copy =
            new in_rrr(io_cpy, the_instr->result_type(), dst_op,
                       operand(the_instr));
    the_tree_instr->set_instr(new_copy);
  }

static void from_copy_form_on_node(tree_node *the_node, void *)
  {
    tree_node_list *parent_list = the_node->parent();
    tree_node *parent_node = parent_list->parent();
    if (parent_node->is_for())
      {
        tree_for *parent_for = (tree_for *)parent_node;
        if ((parent_for->ub_list() == parent_list) ||
            (parent_for->lb_list() == parent_list) ||
            (parent_for->step_list() == parent_list))
          {
            return;
          }
      }

    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();

    if (the_instr->result_type()->unqual()->op() == TYPE_VOID)
        return;

    if (the_instr->opcode() != io_cpy)
        return;

    in_rrr *the_copy = (in_rrr *)the_instr;

    operand src_op = the_copy->src_op();
    if (!src_op.is_expr())
        return;

    instruction *src_instr = src_op.instr();
    if (src_instr->result_type() != the_copy->result_type())
        return;

    the_tree_instr->remove_instr(the_copy);
    operand dst_op = the_copy->dst_op();
    src_instr->remove();
    the_copy->set_src(operand());
    delete the_copy;
    src_instr->set_dst(dst_op);
    the_tree_instr->set_instr(src_instr);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
