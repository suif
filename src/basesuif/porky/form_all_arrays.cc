/* file "form_all_arrays.cc" */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to form arrays out of compatible variables where possible for
 * the porky program for SUIF */


#define RCS_BASE_FILE form_all_arrays_cc

#include "porky.h"

RCS_BASE(
    "$Id: form_all_arrays.cc,v 1.2 1999/08/25 03:27:28 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_faa_marked;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void handle_object(suif_object *the_object);
static void handle_symtab(base_symtab *the_symtab);
static void combine_vars(var_sym *var1, var_sym *var2);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_form_all_arrays(void)
  {
    k_porky_faa_marked = lexicon->enter("porky faa marked")->sp;
  }

extern void form_all_arrays_on_symtab(global_symtab *the_symtab)
  {
    handle_symtab(the_symtab);
  }

extern void form_all_arrays_on_proc(tree_proc *the_proc)
  {
    walk(the_proc, &handle_object);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void handle_object(suif_object *the_object)
  {
    if (!the_object->is_symtab_obj())
        return;
    base_symtab *the_symtab = (base_symtab *)the_object;
    handle_symtab(the_symtab);
  }

static void handle_symtab(base_symtab *the_symtab)
  {
    sym_node_list_iter outer_iter(the_symtab->symbols());
    while (!outer_iter.is_empty())
      {
        sym_node *outer_sym = outer_iter.step();
        if (!outer_sym->is_var())
            continue;
        var_sym *outer_var = (var_sym *)outer_sym;
        if (outer_var->is_param())
            continue;
        if (outer_var->parent_var() != NULL)
            continue;
        if (!unreferenced_outside_fileset(outer_var))
            continue;
        type_node *outer_type = outer_var->type();
        sym_node_list_iter inner_iter(the_symtab->symbols());
        while (!inner_iter.is_empty())
          {
            sym_node *inner_sym = inner_iter.step();
            if (inner_sym == outer_sym)
                break;
            if (!inner_sym->is_var())
                continue;
            var_sym *inner_var = (var_sym *)inner_sym;
            if (inner_var->is_param())
                continue;
            if (inner_var->parent_var() != NULL)
                continue;
            if (inner_var->type() != outer_type)
                continue;
            if (!unreferenced_outside_fileset(inner_var))
                continue;
            if (inner_var->is_auto() != outer_var->is_auto())
                continue;
            combine_vars(inner_var, outer_var);
            break;
          }
      }
  }

static void combine_vars(var_sym *var1, var_sym *var2)
  {
    annote *existing_annote =
            (annote *)(var1->peek_annote(k_porky_faa_marked));
    if (existing_annote != NULL)
      {
        existing_annote->immeds()->append(var2);
      }
    else
      {
        annote *new_annote =
                new annote(k_form_array, new immed_list(var1, var2));
        var1->parent()->annotes()->append(new_annote);
        var1->append_annote(k_porky_faa_marked, new_annote);
        var2->append_annote(k_porky_faa_marked, new_annote);
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
