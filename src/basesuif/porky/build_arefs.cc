/* file "build_arefs.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to build array reference instructions out of pointer
 * arithmetic wherever possible for the porky program for SUIF */

#define RCS_BASE_FILE build_arefs_cc

#include "porky.h"

RCS_BASE(
    "$Id: build_arefs.cc,v 1.2 1999/08/25 03:27:24 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

DECLARE_LIST_CLASS(boolean_list, boolean);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void build_arefs_on_node(tree_node *the_node, void *);
static void build_arefs_on_instr(instruction *the_instr);
static in_array *build_aref_for_addr(operand old_address);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void build_arefs_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&build_arefs_on_node, NULL);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void build_arefs_on_node(tree_node *the_node, void *)
  {
    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();

    if (the_instr->opcode() == io_str)
      {
        in_rrr *the_str = (in_rrr *)the_instr;
        operand dst_addr_op = the_str->dst_addr_op();
        in_array *new_aref = build_aref_for_addr(dst_addr_op);
        if (new_aref != NULL)
          {
            dst_addr_op.remove();
            if (dst_addr_op.is_expr())
                delete dst_addr_op.instr();
            the_str->set_dst_addr_op(operand(new_aref));
          }
      }
    else if (the_instr->opcode() == io_memcpy)
      {
        in_rrr *the_memcpy = (in_rrr *)the_instr;

        operand dst_addr_op = the_memcpy->dst_addr_op();
        in_array *new_aref = build_aref_for_addr(dst_addr_op);
        if (new_aref != NULL)
          {
            dst_addr_op.remove();
            if (dst_addr_op.is_expr())
                delete dst_addr_op.instr();
            the_memcpy->set_dst_addr_op(operand(new_aref));
          }

        operand src_addr_op = the_memcpy->src_addr_op();
        new_aref = build_aref_for_addr(src_addr_op);
        if (new_aref != NULL)
          {
            src_addr_op.remove();
            if (src_addr_op.is_expr())
                delete src_addr_op.instr();
            the_memcpy->set_src_addr_op(operand(new_aref));
          }
      }

    build_arefs_on_instr(the_instr);
  }

static void build_arefs_on_instr(instruction *the_instr)
  {
    if (the_instr->opcode() == io_lod)
      {
        in_rrr *the_load = (in_rrr *)the_instr;

        operand src_addr_op = the_load->src_addr_op();
        in_array *new_aref = build_aref_for_addr(src_addr_op);
        if (new_aref != NULL)
          {
            src_addr_op.remove();
            if (src_addr_op.is_expr())
                delete src_addr_op.instr();
            the_load->set_src_addr_op(operand(new_aref));
          }
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_src = the_instr->src_op(src_num);
        if (this_src.is_expr())
            build_arefs_on_instr(this_src.instr());
      }
  }

static in_array *build_aref_for_addr(operand old_address)
  {
    boolean_list is_added_list;
    operand_list added_ops;

    operand base_address = old_address;

    if (base_address.is_expr() && (base_address.instr()->opcode() == io_array))
        return NULL;

    while (base_address.is_expr())
      {
        instruction *base_instr = base_address.instr();
        switch (base_instr->opcode())
          {
            case io_cpy:
            case io_cvt:
              {
                in_rrr *the_cpy = (in_rrr *)base_instr;
                if (the_cpy->src_op().type()->unqual()->is_ptr())
                  {
                    base_address = the_cpy->src_op();
                    continue;
                  }
                break;
              }
            case io_add:
            case io_sub:
              {
                in_rrr *the_rrr = (in_rrr *)base_instr;
                if (the_rrr->src1_op().type()->unqual()->is_ptr())
                  {
                    base_address = the_rrr->src1_op();
                    added_ops.push(the_rrr->src2_op());
                    if (the_rrr->opcode() == io_add)
                        is_added_list.push(TRUE);
                    else
                        is_added_list.push(FALSE);
                    continue;
                  }
                else if (the_rrr->src2_op().type()->unqual()->is_ptr())
                  {
                    base_address = the_rrr->src2_op();
                    added_ops.push(the_rrr->src1_op());
                    assert(the_rrr->opcode() == io_add);
                    is_added_list.push(TRUE);
                    continue;
                  }
                break;
              }
            default:
                break;
          }
        break;
      }

    type_node *base_type = base_address.type()->unqual();
    assert(base_type->is_ptr());
    ptr_type *base_ptr = (ptr_type *)base_type;
    type_node *base_object_type = base_ptr->ref_type()->unqual();

    if (base_address.is_expr() && (base_address.instr()->opcode() == io_ldc))
      {
        in_ldc *base_ldc = (in_ldc *)(base_address.instr());
        immed value = base_ldc->value();
        if (value.is_symbol() && (value.offset() == 0) &&
            value.symbol()->is_var())
          {
            var_sym *base_var = (var_sym *)(value.symbol());
            base_object_type = base_var->type()->unqual();
          }
      }

    type_node *original_addr_type = old_address.type()->unqual();
    assert(original_addr_type->is_ptr());
    ptr_type *original_ptr = (ptr_type *)original_addr_type;
    type_node *original_obj_type = original_ptr->ref_type()->unqual();

    type_node *base_elem_type = base_object_type;
    while (base_elem_type->is_array())
      {
        if (base_elem_type->is_same(original_obj_type))
            break;
        array_type *base_array_type = (array_type *)base_elem_type;
        base_elem_type = base_array_type->elem_type()->unqual();
      }

    if (!base_elem_type->is_same(original_obj_type))
        return NULL;

    array_type *new_base_array;
    new_base_array =
            new array_type(original_obj_type, array_bound(0), unknown_bound);
    new_base_array =
            (array_type *)(original_obj_type->parent()->install_type(
                                   new_base_array));

    operand index_op = operand_int(type_ptr_diff, 0);
    while (!added_ops.is_empty())
      {
        assert(!is_added_list.is_empty());
        operand this_op = added_ops.pop().clone();
        boolean this_is_added = is_added_list.pop();
        if_ops opcode = (this_is_added ? io_add : io_sub);
        this_op = cast_op(this_op, type_ptr_diff);
        index_op =
                fold_real_2op_rrr(opcode, type_ptr_diff, index_op, this_op);
      }
    assert(is_added_list.is_empty());

    int byte_elem_size =
            (original_obj_type->size() + target.addressable_size - 1) /
            target.addressable_size;
    if (!op_divisible_by(index_op, byte_elem_size))
      {
        if (index_op.is_expr())
            delete index_op.instr();
        return NULL;
      }

    index_op =
            fold_real_2op_rrr(io_div, type_ptr_diff, index_op,
                              operand_int(type_ptr_diff, byte_elem_size));

    operand new_base_op =
            fold_real_1op_rrr(io_cvt, new_base_array->ptr_to(),
                              base_address.clone());

    /* if there are constant array references in the base, fold them away */
    boolean old_suppress_array_folding = suppress_array_folding;
    suppress_array_folding = FALSE;
    new_base_op = fold_constants(new_base_op);
    suppress_array_folding = old_suppress_array_folding;

    in_array *new_aref =
            new in_array(original_addr_type, operand(), new_base_op,
                         original_obj_type->size(), 1);
    new_aref->set_index(0, index_op);
    new_aref->set_bound(0, operand());

    return new_aref;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
