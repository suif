/* file "fact.h" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* propagated fact definition for the porky program for SUIF */

#ifndef FACT_H
#define FACT_H

#include <suif1.h>

RCS_HEADER(fact_h,
     "$Id: fact.h,v 1.2 1999/08/25 03:27:27 brm Exp $")

class propagated_fact_list;

class propagated_fact
  {
public:
    virtual propagated_fact *clone(void) = 0;
    virtual boolean killed_by_var_write(var_sym *) { return FALSE; }
    virtual boolean killed_by_unknown_mem_write(void) { return FALSE; }
    virtual boolean killed_by_arbitrary_function_call(void) { return FALSE; }
    virtual boolean killed_by_instr(instruction *) { return FALSE; }
    virtual boolean killed_by_for_bounds(tree_for *) { return FALSE; }
    virtual boolean killed_by_for_step(tree_for *)  { return FALSE; }
    virtual boolean killed_exiting_scope(base_symtab *scope) = 0;
    virtual boolean is_same_fact(propagated_fact *other_fact) = 0;
    virtual void merge_reasons(propagated_fact *other_fact) = 0;
  };

DECLARE_LIST_CLASS(propagated_fact_list, propagated_fact *);

#endif
