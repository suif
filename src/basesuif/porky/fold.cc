/* file "fold.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE fold_cc

#include "porky.h"

RCS_BASE(
    "$Id: fold.cc,v 1.2 1999/08/25 03:27:28 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static unsigned num_folded;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void fold_consts_on_a_node(tree_node *the_node, void *);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void fold_all_constants(tree_proc *the_proc)
  {
    num_folded = 0;

    the_proc->body()->map(&fold_consts_on_a_node, NULL, FALSE);

    if (num_folded > 0)
      {
        made_progress = TRUE;

        if (verbosity_level >= 3)
          {
            printf("    Constant folding simplified a total of %u "
                   "expressions from procedure `%s'.\n", num_folded,
                   the_proc->proc()->name());
          }
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void fold_consts_on_a_node(tree_node *the_node, void *)
  {
    assert(the_node != NULL);
    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();
    assert(the_instr != NULL);

    if (the_node->parent()->parent()->is_for())
      {
        tree_for *parent_for = (tree_for *)(the_node->parent()->parent());
        if ((parent_for->body() != the_node->parent()) &&
            (parent_for->landing_pad() != the_node->parent()))
          {
            assert(the_instr->opcode() == io_cpy);
            in_rrr *dummy_copy = (in_rrr *)the_instr;
            if (!dummy_copy->src_op().is_expr())
                return;
            the_instr = dummy_copy->src_op().instr();
	  }
      }

    fold_had_effect = FALSE;
    fold_constants(the_instr);
    if (fold_had_effect)
      {
        ++num_folded;
        if (verbosity_level >= 4)
          {
            printf("      Simplification found for expression at line %u.\n",
                   source_line_num(the_node));
          }
      }

    the_instr = the_tree_instr->instr();
    if ((the_instr->opcode() == io_btrue) ||
        (the_instr->opcode() == io_bfalse))
      {
        in_bj *the_branch = (in_bj *)the_instr;
        operand source_operand = the_branch->src_op();
        if (source_operand.is_expr())
          {
            instruction *source_instr = source_operand.instr();
            assert(source_instr != NULL);
            if (source_instr->opcode() == io_ldc)
              {
                in_ldc *the_ldc = (in_ldc *)source_instr;
                immed value = the_ldc->value();
                if (!value.is_int_const())
                  {
                    error_line(1, the_node,
                               "constant source operand of branch is not an "
                               "integer");
                  }
                else
                  {
                    i_integer constant = immed_to_ii(value);
                    if ((constant != 0) && (constant != 1))
                      {
                        error_line(1, the_node,
                                   "constant source operand of branch is %d, "
                                   "not 1 or 0");
                      }
                    else
                      {
                        boolean taken;
                        if (the_instr->opcode() == io_btrue)
                            taken = (constant != 0);
                        else
                            taken = (constant == 0);
                        ++num_folded;
                        if (verbosity_level >= 4)
                          {
                            if (taken)
                                printf("      Always");
                            else
                                printf("      Never");
                            printf(" taken conditional branch at line %u ",
                            source_line_num(the_node));
                            if (taken)
                                printf("turned into a jump.\n");
                            else
                                printf("removed.\n");
                          }
                        if (taken)
                          {
                            the_ldc->remove();
                            delete the_ldc;
                            the_branch->set_opcode(io_jmp);
                          }
                        else
                          {
                            kill_node(the_node);
                          }
                      }
                  }
              }
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
