/* file "for_normalize.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to normalize for loops to have zero lower bound and step size
 * of one for the porky program for SUIF */

#define RCS_BASE_FILE for_normalize_cc

#include "porky.h"

RCS_BASE(
    "$Id: for_normalize.cc,v 1.2 1999/08/25 03:27:28 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void normalize_fors_on_node(tree_node *the_node, void *);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void normalize_fors_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&normalize_fors_on_node, NULL);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void normalize_fors_on_node(tree_node *the_node, void *)
  {
    if (!the_node->is_for())
        return;

    tree_for *the_for = (tree_for *)the_node;

    guard_for(the_for);

    if (!operand_is_int_const(the_for->lb_op()))
        make_lb_temp(the_for);
    if (!operand_is_int_const(the_for->step_op()))
        make_step_temp(the_for);

    operand old_lb_op = the_for->lb_op();
    operand old_step_op = the_for->step_op();

    int lb_int;
    boolean lb_is_int = operand_is_int_const(old_lb_op, &lb_int);

    int step_int;
    boolean step_is_int = operand_is_int_const(old_step_op, &step_int);

    if (lb_is_int && step_is_int && (lb_int == 0) && (step_int == 1))
        return;

    if (!operand_is_int_const(the_for->ub_op()))
        make_ub_temp(the_for);

    tree_for_test new_test;
    switch (the_for->test())
      {
        case FOR_SGTE:
        case FOR_SLTE:
        case FOR_SGT:
        case FOR_SLT:
            new_test = FOR_SLTE;
            break;
        case FOR_UGTE:
        case FOR_ULTE:
        case FOR_UGT:
        case FOR_ULT:
            new_test = FOR_ULTE;
            break;
        default:
            return;
      }

    var_sym *old_index = the_for->index();
    type_node *index_type = old_index->type()->unqual();
    var_sym *new_index = the_for->scope()->new_unique_var(index_type);

    operand new_ub_op = cast_op(iteration_count(the_for) - 1, index_type);

    old_lb_op.remove();
    old_step_op.remove();

    operand old_ub_op = the_for->ub_op();
    old_ub_op.remove();
    if (old_ub_op.is_expr())
        delete old_ub_op.instr();

    the_for->set_lb_op(operand_int(index_type, 0));
    the_for->set_ub_op(new_ub_op);
    the_for->set_step_op(operand_int(index_type, 1));

    the_for->set_test(new_test);

    the_for->set_index(new_index);

    operand old_ind_op =
#ifndef DEAL_WITH_SOLARIS_BRAIN_DAMAGE
            cast_op((new_index * old_step_op) + old_lb_op, index_type);
#else
            cast_op(fold_mul(new_index, old_step_op) + old_lb_op, index_type);
#endif
    tree_node *assign_node = assign(old_index, old_ind_op);
    the_for->body()->push(assign_node->clone());
    the_for->parent()->insert_after(assign_node, the_for->list_e());
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
