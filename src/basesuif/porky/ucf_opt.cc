/* file "ucf_opt.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to do optimization of unstructured control flow for the porky
 * program for SUIF */


#define RCS_BASE_FILE ucf_opt_cc

#include "porky.h"

RCS_BASE(
    "$Id: ucf_opt.cc,v 1.2 1999/08/25 03:27:35 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

DECLARE_LIST_CLASS(in_bj_list, in_bj *);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_ucf_opt_forward;
static const char *k_porky_ucf_opt_in_use;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void all_passes_on_list(tree_node_list *the_list);
static void pass1_on_list(tree_node_list *the_list, label_sym **pending_label);
static void pass1_on_node(tree_node *the_node, label_sym **pending_label);
static void pass2_on_list(tree_node_list *the_list);
static void pass2_on_node(tree_node *the_node);
static void pass3_on_list(tree_node_list *the_list, boolean *unreachable,
                          in_bj_list *pending_branches,
                          in_bj_list *pending_jumps);
static void pass3_on_node(tree_node *the_node, boolean *unreachable,
                          in_bj_list *pending_branches,
                          in_bj_list *pending_jumps);
static void handle_label_instr(in_lab *the_lab_instr,
                               in_bj_list *pending_branches,
                               in_bj_list *pending_jumps);
static void forward_label(label_sym *from_lab, label_sym *to_lab);
static label_sym *forwarded_target(label_sym *old_label);
static label_sym *forward_one(label_sym *old_label);
static void remove_forwarding_annotes(tree_node *the_node, void *);
static void add_jump(label_sym *target, instruction *the_instr);
static void remove_jump(label_sym *target, instruction *the_instr);
static void delete_node(tree_node *the_node);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_ucf_opt(void)
  {
    ANNOTE(k_porky_ucf_opt_forward, "porky ucf opt forward", FALSE);
    ANNOTE(k_porky_ucf_opt_in_use,  "porky ucf opt in use",  FALSE);
  }

extern void optimize_ucf_on_proc(tree_proc *the_proc)
  {
    all_passes_on_list(the_proc->body());
    the_proc->map(&remove_forwarding_annotes, NULL);
    remove_forwarding_annotes(the_proc, NULL);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

/*
 *  Pass 1: unused labels and finding label forwarding, recursive calls
 *  Pass 2: doing label forwarding, unreachable code, and no-skip branches
 */
static void all_passes_on_list(tree_node_list *the_list)
  {
    label_sym *pending_label = NULL;
    build_label_info(the_list);
    pass1_on_list(the_list, &pending_label);
    in_bj_list pending_branches;
    in_bj_list pending_jumps;
    boolean unreachable = FALSE;
    pass2_on_list(the_list);
    pass3_on_list(the_list, &unreachable, &pending_branches, &pending_jumps);
    remove_label_info(the_list);
  }

static void pass1_on_list(tree_node_list *the_list, label_sym **pending_label)
  {
    tree_node_list_iter node_iter(the_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        pass1_on_node(this_node, pending_label);
      }
  }

static void pass1_on_node(tree_node *the_node, label_sym **pending_label)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            switch (the_instr->opcode())
              {
                case io_mrk:
                case io_nop:
                    break;
                case io_jmp:
                  {
                    in_bj *the_bj = (in_bj *)the_instr;
                    if (*pending_label != NULL)
                        forward_label(*pending_label, the_bj->target());
                    *pending_label = NULL;
                    break;
                  }
                case io_lab:
                  {
                    in_lab *the_lab_instr = (in_lab *)the_instr;
                    label_sym *this_lab_sym = the_lab_instr->label();
                    label_info *the_info = get_lab_info(this_lab_sym);
                    if (the_info->forward_jumps.is_empty() &&
                        the_info->backward_jumps.is_empty())
                      {
                        delete_node(the_tree_instr);
                      }
                    else
                      {
                        if (*pending_label != NULL)
                            forward_label(*pending_label, this_lab_sym);
                        *pending_label = this_lab_sym;
                      }
                    break;
                  }
                default:
                    *pending_label = NULL;
                    break;
              }
            break;
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            *pending_label = NULL;
            all_passes_on_list(the_loop->body());
            all_passes_on_list(the_loop->test());
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            *pending_label = NULL;
            all_passes_on_list(the_for->body());
            all_passes_on_list(the_for->landing_pad());
            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;
            *pending_label = NULL;
            all_passes_on_list(the_if->header());
            all_passes_on_list(the_if->then_part());
            all_passes_on_list(the_if->else_part());
            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            pass1_on_list(the_block->body(), pending_label);
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void pass2_on_list(tree_node_list *the_list)
  {
    tree_node_list_iter node_iter(the_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        pass2_on_node(this_node);
      }
  }

static void pass2_on_node(tree_node *the_node)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            switch (the_instr->opcode())
              {
                case io_mrk:
                case io_nop:
                    break;
                case io_btrue:
                case io_bfalse:
                case io_jmp:
                  {
                    in_bj *the_bj = (in_bj *)the_instr;
                    label_sym *old_target = the_bj->target();
                    label_sym *new_target = forwarded_target(old_target);
                    if (old_target != new_target)
                      {
                        the_bj->set_target(new_target);
                        add_jump(new_target, the_bj);
                        remove_jump(old_target, the_bj);
                      }
                    break;
                  }
                case io_lab:
                    break;
                case io_mbr:
                  {
                    in_mbr *the_mbr = (in_mbr *)the_instr;

                    unsigned num_labs = the_mbr->num_labs();
                    for (unsigned lab_num = 0; lab_num < num_labs; ++lab_num)
                      {
                        label_sym *old_target = the_mbr->label(lab_num);
                        label_sym *new_target = forwarded_target(old_target);
                        if (old_target != new_target)
                          {
                            the_mbr->set_label(lab_num, new_target);
                            add_jump(new_target, the_mbr);
                            remove_jump(old_target, the_mbr);
                          }
                      }

                    label_sym *old_target = the_mbr->default_lab();
                    label_sym *new_target = forwarded_target(old_target);
                    if (old_target != new_target)
                      {
                        the_mbr->set_default_lab(new_target);
                        add_jump(new_target, the_mbr);
                        remove_jump(old_target, the_mbr);
                      }

                    break;
                  }
                default:
                    break;
              }
            break;
          }
        case TREE_LOOP:
        case TREE_FOR:
        case TREE_IF:
            break;
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            pass2_on_list(the_block->body());
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void pass3_on_list(tree_node_list *the_list, boolean *unreachable,
                          in_bj_list *pending_branches, 
                          in_bj_list *pending_jumps)
  {
    tree_node_list_iter node_iter(the_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        pass3_on_node(this_node, unreachable, pending_branches, pending_jumps);
      }
  }

static void pass3_on_node(tree_node *the_node, boolean *unreachable,
                          in_bj_list *pending_branches,
                          in_bj_list *pending_jumps)
  {
    if (the_node->is_instr() &&
        (((tree_instr *)the_node)->instr()->opcode() == io_lab))
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        in_lab *the_lab_instr = (in_lab *)(the_tree_instr->instr());
        label_sym *this_lab_sym = the_lab_instr->label();
        label_info *lab_info = get_lab_info(this_lab_sym);
        if (lab_info->forward_jumps.is_empty() &&
            lab_info->backward_jumps.is_empty())
          {
            delete_node(the_node);
            return;
          }
        else
          {
            *unreachable = FALSE;
          }
      }

    if (*unreachable && (!the_node->is_block()))
      {
        delete_node(the_node);
        return;
      }

    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            switch (the_instr->opcode())
              {
                case io_mrk:
                case io_nop:
                    break;
                case io_btrue:
                case io_bfalse:
                  {
                    in_bj *the_bj = (in_bj *)the_instr;
                    pending_branches->push(the_bj);
                    pending_jumps->push((in_bj *)NULL);
                    break;
                  }
                case io_jmp:
                  {
                    in_bj *the_bj = (in_bj *)the_instr;
                    *unreachable = TRUE;
                    if ((!pending_branches->is_empty()) &&
                        (pending_branches->head()->contents->opcode() !=
                         io_jmp))
                      {
                        assert(!pending_jumps->is_empty());
                        assert(pending_jumps->head()->contents == NULL);
                        pending_jumps->head()->contents = the_bj;
                      }
                    else
                      {
                        pending_branches->push(the_bj);
                        pending_jumps->push((in_bj *)NULL);
                      }
                    break;
                  }
                case io_lab:
                  {
                    in_lab *the_lab_instr = (in_lab *)the_instr;
                    handle_label_instr(the_lab_instr, pending_branches,
                                       pending_jumps);
                    break;
                  }
                case io_mbr:
                    /* fall through */
                default:
                    if (the_instr->opcode() == io_ret)
                        *unreachable = TRUE;
                    while (!pending_branches->is_empty())
                        (void)pending_branches->pop();
                    while (!pending_jumps->is_empty())
                        (void)pending_jumps->pop();
                    break;
              }
            break;
          }
        case TREE_LOOP:
        case TREE_FOR:
        case TREE_IF:
            while (!pending_branches->is_empty())
                (void)pending_branches->pop();
            while (!pending_jumps->is_empty())
                (void)pending_jumps->pop();
            break;
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            pass3_on_list(the_block->body(), unreachable, pending_branches,
                          pending_jumps);
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void handle_label_instr(in_lab *the_lab_instr,
                               in_bj_list *pending_branches,
                               in_bj_list *pending_jumps)
  {
    label_sym *this_lab_sym = the_lab_instr->label();
    if (!pending_branches->is_empty())
      {
        in_bj *last_branch = pending_branches->pop();
        in_bj *last_jump = pending_jumps->pop();
        if (last_branch->target() == this_lab_sym)
          {
            if (last_jump != NULL)
              {
                if (last_branch->opcode() == io_btrue)
                  {
                    last_branch->set_opcode(io_bfalse);
                  }
                else
                  {
                    assert(last_branch->opcode() == io_bfalse);
                    last_branch->set_opcode(io_btrue);
                  }

                label_sym *jump_lab = last_jump->target();

                last_branch->set_target(jump_lab);
                add_jump(jump_lab, last_branch);
                remove_jump(this_lab_sym, last_branch);

                delete_node(last_jump->owner());

                pending_branches->push(last_branch);
                pending_jumps->push((in_bj *)NULL);
              }
            else
              {
                tree_node *branch_node = last_branch->owner();
                operand condition_op = last_branch->src_op();
                condition_op.remove();
                tree_node_list *side_effects =
                        reduce_to_side_effects(condition_op);
                if (side_effects != NULL)
                  {
                    branch_node->parent()->insert_before(side_effects,
                            branch_node->list_e());
                    delete side_effects;
                  }
                delete_node(branch_node);
              }

            label_info *branch_lab_info = get_lab_info(this_lab_sym);
            if (branch_lab_info->forward_jumps.is_empty() &&
                branch_lab_info->backward_jumps.is_empty())
              {
                delete_node(the_lab_instr->owner());
              }
            else
              {
                while (!pending_branches->is_empty())
                    (void)pending_branches->pop();
                while (!pending_jumps->is_empty())
                    (void)pending_jumps->pop();
              }
          }
        else if ((last_jump != NULL) && (last_jump->target() == this_lab_sym))
          {
            pending_branches->push(last_branch);
            pending_branches->push(last_jump);
            pending_jumps->push((in_bj *)NULL);
            pending_jumps->push((in_bj *)NULL);
            handle_label_instr(the_lab_instr, pending_branches, pending_jumps);
          }
        else
          {
            while (!pending_branches->is_empty())
                (void)pending_branches->pop();
            while (!pending_jumps->is_empty())
                (void)pending_jumps->pop();
          }
      }
  }

static void forward_label(label_sym *from_lab, label_sym *to_lab)
  {
    annote *forward_annote =
            from_lab->annotes()->peek_annote(k_porky_ucf_opt_forward);
    if (forward_annote == NULL)
      {
        immed_list *new_immeds = new immed_list;
        new_immeds->append(immed(to_lab));
        from_lab->append_annote(k_porky_ucf_opt_forward, new_immeds);
      }
    else
      {
        immed_list *the_immeds = forward_annote->immeds();
        assert(the_immeds->count() == 1);
        the_immeds->head()->contents = immed(to_lab);
      }
  }

static label_sym *forwarded_target(label_sym *old_label)
  {
    if (old_label->annotes()->peek_annote(k_porky_ucf_opt_in_use) != NULL)
      {
        warning_line(old_label,
                     "Empty control-flow cycle without an exit found through "
                     "these labels:");
        label_sym *follow_lab = old_label;
        do
          {
            warning_line(old_label, "    `%s'", follow_lab->name());
            follow_lab = forward_one(follow_lab);
            assert(follow_lab != NULL);
          } while (follow_lab != old_label);
        warning_line(old_label, "Label forwarding disabled for this cycle.");
      }

    label_sym *target_lab = forward_one(old_label);
    if (target_lab == NULL)
        return old_label;

    old_label->append_annote(k_porky_ucf_opt_in_use);
    label_sym *result = forwarded_target(target_lab);
    annote *old_annote =
            old_label->annotes()->get_annote(k_porky_ucf_opt_in_use);
    delete old_annote;
    return result;
  }

static label_sym *forward_one(label_sym *old_label)
  {
    annote *forward_annote =
            old_label->annotes()->peek_annote(k_porky_ucf_opt_forward);
    if (forward_annote == NULL)
        return NULL;
    assert(forward_annote->immeds()->count() == 1);
    immed the_immed = forward_annote->immeds()->head()->contents;
    assert(the_immed.is_symbol());
    sym_node *target_sym = the_immed.symbol();
    assert(target_sym->is_label());
    label_sym *target_lab = (label_sym *)target_sym;
    return target_lab;
  }

static void remove_forwarding_annotes(tree_node *the_node, void *)
  {
    if (the_node->is_block())
      {
        tree_block *the_block = (tree_block *)the_node;
        sym_node_list_iter sym_iter(the_block->symtab()->symbols());
        while (!sym_iter.is_empty())
          {
            sym_node *this_sym = sym_iter.step();
            if (this_sym->is_label())
              {
                annote *old_annote =
                        this_sym->annotes()->peek_annote(
                                k_porky_ucf_opt_forward);
                if (old_annote != NULL)
                  {
                    old_annote =
                            this_sym->annotes()->get_annote(
                                    k_porky_ucf_opt_forward);
                    delete old_annote;
                  }
              }
          }
      }
  }

/*
 *  In this program we don't actually care about forward versus
 *  backward jumps or the order of the jumps, so we just append to the
 *  list of forward jumps.
 */
static void add_jump(label_sym *target, instruction *the_instr)
  {
    label_info *lab_info = get_lab_info(target);
    assert(lab_info != NULL);
    lab_info->forward_jumps.append(the_instr);
  }

static void remove_jump(label_sym *target, instruction *the_instr)
  {
    label_info *lab_info = get_lab_info(target);
    assert(lab_info != NULL);
    instruction_list_e *instr_e = lab_info->forward_jumps.lookup(the_instr);
    if (instr_e != NULL)
      {
        lab_info->forward_jumps.remove(instr_e);
      }
    else
      {
        instr_e = lab_info->backward_jumps.lookup(the_instr);
        assert(instr_e != NULL);
        lab_info->backward_jumps.remove(instr_e);
      }
    delete instr_e;
    if ((lab_info->definition == NULL) && lab_info->forward_jumps.is_empty() &&
        lab_info->backward_jumps.is_empty())
      {
        delete lab_info;
      }
  }

static void delete_node(tree_node *the_node)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();
        switch (the_instr->opcode())
          {
            case io_btrue:
            case io_bfalse:
            case io_jmp:
              {
                in_bj *the_bj = (in_bj *)the_instr;
                remove_jump(the_bj->target(), the_bj);
                break;
              }
            case io_lab:
              {
                in_lab *the_lab_instr = (in_lab *)the_instr;
                label_info *lab_info = get_lab_info(the_lab_instr->label());
                lab_info->definition = NULL;
                if (lab_info->forward_jumps.is_empty() &&
                    lab_info->backward_jumps.is_empty())
                  {
                    delete lab_info;
                  }
                break;
              }
            case io_mbr:
              {
                in_mbr *the_mbr = (in_mbr *)the_instr;
                unsigned num_labs = the_mbr->num_labs();
                for (unsigned lab_num = 0; lab_num < num_labs; ++lab_num)
                    remove_jump(the_mbr->label(lab_num), the_mbr);
                remove_jump(the_mbr->default_lab(), the_mbr);
                break;
              }
            default:
                break;
          }
      }

    tree_node_list_e *the_list_e = the_node->list_e();
    the_node->parent()->remove(the_list_e);
    delete the_list_e;
    delete the_node;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
