/* file "constants.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE constants_cc

#include "porky.h"

RCS_BASE(
    "$Id: constants.cc,v 1.2 1999/08/25 03:27:25 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

DECLARE_LIST_CLASS(label_list, label_sym *);
DECLARE_LIST_CLASS(alist_list, alist **);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Variable Declarations
 *----------------------------------------------------------------------*/

static const char *k_porky_multi_def;
static const char *k_porky_single_def;
static label_list *pending_labels = NULL;
static alist_list *pending_lists = NULL;

/*----------------------------------------------------------------------*
    End Private Variable Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void propagate_static_consts(tree_node *the_node);
static void static_prop_on_node(tree_node *the_node, void *);
static void kill_possibly_written(tree_node *the_node, void *data);
static void kill_symbol(var_sym *the_var, alist *static_consts);
static void replace_static_consts(tree_node *the_node, void *data);
static void instr_replace_statics(instruction *the_instr,
                                  alist *static_consts);
static alist *all_candidate_vars(tree_block *the_block);
static void add_candidate_vars(block_symtab *the_symtab, alist *the_list);
static void constants_on_list(tree_node_list *the_node_list,
                              alist **value_list);
static void constants_on_instruction(instruction *the_instr,
                                     alist *value_list);
static operand constants_on_operand(operand old_op, alist *value_list);
static void clear_all_values(alist *value_list);
static alist *duplicate_alist(alist *the_alist);
static alist *merge_value_lists(alist *list1, alist *list2);
static void clear_all_writes(tree_node_list *the_node_list, alist *value_list);
static void clear_writes_on_node(tree_node *the_node, void *data);
static void clear_var(alist *value_list, var_sym *the_var);
static void possible_jump_to(label_sym *the_label, alist *the_alist);
static void prop_single_def_constants(tree_node *the_node);
static void mark_single_def_on_object(suif_object *the_object);
static void prop_single_def_consts_on_operand(operand the_operand,
                                              so_walker *the_walker);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_propagate_constants(void)
  {
    k_porky_single_def = lexicon->enter("porky single use")->sp;
    ANNOTE(k_porky_multi_def, "porky multi use", FALSE);
  }

extern void propagate_constants(tree_node *the_node)
  {
    if (the_node->kind() != TREE_BLOCK)
        return;

    propagate_static_consts(the_node);

    prop_single_def_constants(the_node);

    assert(pending_labels == NULL);
    assert(pending_lists == NULL);
    pending_labels = new label_list;
    pending_lists = new alist_list;

    tree_block *the_block = (tree_block *)the_node;
    alist *candidates = all_candidate_vars(the_block);
    constants_on_list(the_block->body(), &candidates);
    delete candidates;

    assert(pending_labels->is_empty());
    assert(pending_lists->is_empty());
    delete pending_labels;
    delete pending_lists;
    pending_labels = NULL;
    pending_lists = NULL;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void propagate_static_consts(tree_node *the_node)
  {
    static_prop_on_node(the_node, NULL);
    the_node->map(&static_prop_on_node, NULL);
  }

static void static_prop_on_node(tree_node *the_node, void *)
  {
    if (!the_node->is_block())
        return;

    tree_block *the_block = (tree_block *)the_node;
    block_symtab *the_symtab = the_block->symtab();

    var_def_list_iter def_iter(the_symtab->var_defs());
    alist static_consts;
    while (!def_iter.is_empty())
      {
        var_def *this_def = def_iter.step();
        if (this_def->variable()->is_addr_taken())
            continue;

        static_consts.enter(this_def->variable(), this_def);
      }

    if (static_consts.is_empty())
        return;

    the_node->map(&kill_possibly_written, &static_consts);
    if (static_consts.is_empty())
        return;

    the_node->map(&replace_static_consts, &static_consts);
  }

static void kill_possibly_written(tree_node *the_node, void *data)
  {
    alist *static_consts = (alist *)data;

    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            if (the_instr->dst_op().is_symbol())
                kill_symbol(the_instr->dst_op().symbol(), static_consts);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            kill_symbol(the_for->index(), static_consts);
            break;
          }
        default:
            break;
      }
  }

static void kill_symbol(var_sym *the_var, alist *static_consts)
  {
    alist_iter static_iter(static_consts);
    while (!static_iter.is_empty())
      {
        alist_e *the_element = static_iter.step();
        var_sym *key_var = (var_sym *)(the_element->key);
        if (!key_var->overlaps(the_var))
            continue;

        static_consts->remove(the_element);
        delete the_element;
      }
  }

static void replace_static_consts(tree_node *the_node, void *data)
  {
    alist *static_consts = (alist *)data;

    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();
    instr_replace_statics(the_instr, static_consts);
  }

static void instr_replace_statics(instruction *the_instr,
                                  alist *static_consts)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_src = the_instr->src_op(src_num);
        if (this_src.is_expr())
          {
            instr_replace_statics(this_src.instr(), static_consts);
          }
        else if (this_src.is_symbol() &&
                 ((this_src.type()->op() == TYPE_INT) ||
                  (this_src.type()->op() == TYPE_ENUM) ||
                  (this_src.type()->op() == TYPE_FLOAT) ||
                  this_src.type()->is_ptr()))
          {
            var_def *this_def = NULL;
            boolean found =
                    static_consts->exists(this_src.symbol(),
                                          (const void **)(&this_def));
            if (found)
              {
                assert(this_def != NULL);
                base_init_struct_list *initialization =
                        read_init_data(this_def);
                immed new_value;
                if (initialization->is_empty())
                  {
                    if (this_src.type()->unqual()->op() == TYPE_FLOAT)
                        new_value = immed(0.0);
                    else
                        new_value = immed(0);
                  }
                else
                  {
                    if (initialization->count() != 1)
                      {
                        deallocate_init_data(initialization);
                        continue;
                      }
                    base_init_struct *the_init =
                            initialization->head()->contents;
                    if (the_init->the_multi_init() != NULL)
                      {
                        multi_init_struct *the_multi_init =
                                the_init->the_multi_init();
                        if (the_multi_init->data->count() != 1)
                          {
                            deallocate_init_data(initialization);
                            continue;
                          }
                        new_value = the_multi_init->data->head()->contents;
                      }
                    else if (the_init->the_repeat_init() != NULL)
                      {
                        repeat_init_struct *the_repeat_init =
                                the_init->the_repeat_init();
                        if (the_repeat_init->repetitions != 1)
                          {
                            deallocate_init_data(initialization);
                            continue;
                          }
                         new_value = the_repeat_init->data;
                      }
                    else
                      {
                        if (this_src.type()->unqual()->op() == TYPE_FLOAT)
                            new_value = immed(0.0);
                        else
                            new_value = immed(0);
                      }

                    made_progress = TRUE;
                    in_ldc *new_ldc =
                            new in_ldc(this_src.type(), operand(), new_value);
                    the_instr->set_src_op(src_num, operand(new_ldc));
                  }
                deallocate_init_data(initialization);
              }
          }
      }
  }

static alist *all_candidate_vars(tree_block *the_block)
  {
    block_symtab *the_symtab = the_block->symtab();
    alist *the_list = new alist;
    add_candidate_vars(the_symtab, the_list);
    return the_list;
  }

static void add_candidate_vars(block_symtab *the_symtab, alist *the_list)
  {
    sym_node_list *symbols = the_symtab->symbols();
    sym_node_list_iter the_iter(symbols);
    while (!the_iter.is_empty())
      {
        sym_node *the_symbol = the_iter.step();
        assert(the_symbol != NULL);
        if (!the_symbol->is_var())
            continue;

        var_sym *the_var = (var_sym *)the_symbol;
        if (the_var->type()->is_volatile())
            continue;

        if (the_var->is_addr_taken())
            continue;

        the_list->enter(the_var, the_var);
      }

    base_symtab_list_iter the_symtab_iter(the_symtab->children());
    while (!the_symtab_iter.is_empty())
      {
        base_symtab *child_symtab = the_symtab_iter.step();
        assert(child_symtab->is_block());
        block_symtab *child_block_symtab = (block_symtab *)child_symtab;
        add_candidate_vars(child_block_symtab, the_list);
      }
  }

static void constants_on_list(tree_node_list *the_node_list,
                              alist **value_list)
  {
    tree_node_list_iter the_iter(the_node_list);
    while (!the_iter.is_empty())
      {
        tree_node *the_node = the_iter.step();
        switch (the_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)the_node;
                instruction *the_instr = the_tree_instr->instr();
                assert(the_instr != NULL);
                constants_on_instruction(the_instr, *value_list);

                if (the_instr->opcode() == io_lab)
                  {
                    clear_all_values(*value_list);
                    break;
                  }

                switch (the_instr->opcode())
                  {
                    case io_lab:
                        clear_all_values(*value_list);
                        break;
                    case io_btrue:
                    case io_bfalse:
                    case io_jmp:
                      {
                        in_bj *the_bj = (in_bj *)the_instr;
                        if (!pending_labels->is_empty())
                            possible_jump_to(the_bj->target(), *value_list);
                        break;
                      }
                    case io_mbr:
                      {
                        in_mbr *the_mbr = (in_mbr *)the_instr;
                        if (!pending_labels->is_empty())
                          {
                            possible_jump_to(the_mbr->default_lab(),
                                             *value_list);
                            unsigned num_labels = the_mbr->num_labs();
                            for (unsigned label_num = 0;
                                 label_num < num_labels; ++label_num)
                              {
                                possible_jump_to(the_mbr->label(label_num),
                                                 *value_list);
                              }
                          }
                        break;
                      }
                    default:
                        break;
                  }

                operand destination = the_instr->dst_op();
                if (!destination.is_symbol())
                    break;

                var_sym *the_var = destination.symbol();
                assert(the_var != NULL);

                while (the_instr->opcode() == io_cpy)
                  {
                    in_rrr *the_cpy = (in_rrr *)the_instr;
                    operand source = the_cpy->src1_op();
                    if (!source.is_expr())
                        break;
                    the_instr = source.instr();
                    assert(the_instr != NULL);
                  }
                clear_var(*value_list, the_var);
                void *new_value;
                if (the_instr->opcode() == io_ldc)
                    new_value = the_instr;
                else
                    new_value = the_var;

                alist_e *old_entry = (*value_list)->search(the_var);
                if (old_entry == NULL)
                    break;

                (*value_list)->remove(old_entry);
                delete old_entry;
                (*value_list)->enter(the_var, new_value);
                break;
              }
            case TREE_LOOP:
              {
                tree_loop *the_loop = (tree_loop *)the_node;
                clear_all_writes(the_loop->body(), *value_list);
                clear_all_writes(the_loop->test(), *value_list);

                alist *continue_list = duplicate_alist(*value_list);
                alist *break_list = duplicate_alist(*value_list);

                pending_labels->push(the_loop->contlab());
                pending_lists->push(&continue_list);
                pending_labels->push(the_loop->brklab());
                pending_lists->push(&break_list);
                constants_on_list(the_loop->body(), value_list);
                pending_labels->pop();
                pending_lists->pop();
                pending_labels->pop();
                pending_lists->pop();
                *value_list = merge_value_lists(continue_list, *value_list);

                constants_on_list(the_loop->test(), value_list);
                *value_list = merge_value_lists(break_list, *value_list);

                break;
              }
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)the_node;

                operand lb_op = the_for->lb_op();
                if (lb_op.is_symbol())
                  {
                    operand new_op =
                        constants_on_operand(lb_op, *value_list);
                    the_for->set_lb_op(new_op);
                  }
                else if (lb_op.is_expr())
                  {
                    instruction *this_instr = lb_op.instr();
                    constants_on_instruction(this_instr, *value_list);
                  }

                operand ub_op = the_for->ub_op();
                if (ub_op.is_symbol())
                  {
                    operand new_op =
                        constants_on_operand(ub_op, *value_list);
                    the_for->set_ub_op(new_op);
                  }
                else if (ub_op.is_expr())
                  {
                    instruction *this_instr = ub_op.instr();
                    constants_on_instruction(this_instr, *value_list);
                  }

                operand step_op = the_for->step_op();
                if (step_op.is_symbol())
                  {
                    operand new_op =
                        constants_on_operand(step_op, *value_list);
                    the_for->set_step_op(new_op);
                  }
                else if (step_op.is_expr())
                  {
                    instruction *this_instr = step_op.instr();
                    constants_on_instruction(this_instr, *value_list);
                  }

                alist *value_noiters = duplicate_alist(*value_list);

                pending_labels->push(the_for->brklab());
                pending_lists->push(&value_noiters);

                constants_on_list(the_for->landing_pad(), value_list);

                clear_var(*value_list, the_for->index());
                clear_all_writes(the_for->body(), *value_list);
                constants_on_list(the_for->body(), value_list);

                pending_labels->pop();
                pending_lists->pop();
                *value_list = merge_value_lists(value_noiters, *value_list);

                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)the_node;
                alist *value2 = duplicate_alist(*value_list);

                pending_labels->push(the_if->jumpto());
                pending_lists->push(&value2);
                constants_on_list(the_if->header(), value_list);
                pending_labels->pop();
                pending_lists->pop();

                constants_on_list(the_if->then_part(), value_list);
                constants_on_list(the_if->else_part(), &value2);
                *value_list = merge_value_lists(*value_list, value2);
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)the_node;
                constants_on_list(the_block->body(), value_list);
                break;
              }
            default:
                assert(FALSE);
          }
      }
  }

static void constants_on_instruction(instruction *the_instr, alist *value_list)
  {
    int num_srcs = the_instr->num_srcs();
    for (int src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_src = the_instr->src_op(src_num);
        if (this_src.is_symbol())
          {
            operand new_op = constants_on_operand(this_src, value_list);
            the_instr->set_src_op(src_num, new_op);
          }
        else if (this_src.is_expr())
          {
            instruction *this_instr = this_src.instr();
            constants_on_instruction(this_instr, value_list);
          }
      }
  }

static operand constants_on_operand(operand old_op, alist *value_list)
  {
    if (old_op.is_symbol())
      {
        sym_node *this_symbol = old_op.symbol();
        const void *result;
        boolean exists = value_list->exists(this_symbol, &result);
        if (!exists)
            return old_op;
        if (result == this_symbol)
            return old_op;

        made_progress = TRUE;
        assert(result != NULL);
        in_ldc *the_ldc = (in_ldc *)result;
        in_ldc *new_ldc = new in_ldc(the_ldc->result_type(), operand(),
                                     the_ldc->value());
        return operand(new_ldc);
      }
    else if (old_op.is_expr())
      {
        instruction *this_instr = old_op.instr();
        constants_on_instruction(this_instr, value_list);
        return old_op;
      }
    else
      {
        return old_op;
      }
  }

static void clear_all_values(alist *value_list)
  {
    alist_e *current_e = (alist_e *)(value_list->head());
    while (current_e != NULL)
      {
        current_e->info = current_e->key;
        current_e = current_e->next();
      }
  }

static alist *duplicate_alist(alist *the_alist)
  {
    alist *new_list = new alist;

    alist_e *current_e = (alist_e *)(the_alist->head());
    while (current_e != NULL)
      {
        new_list->enter(current_e->key, current_e->info);
        current_e = current_e->next();
      }
    return new_list;
  }

static alist *merge_value_lists(alist *list1, alist *list2)
  {
    alist *new_list = new alist;

    if (list1->is_empty())
      {
        delete list1;
        delete list2;
        return new_list;
      }

    alist_e *current_e = (alist_e *)(list1->head());
    while (current_e != NULL)
      {
        assert(current_e->key != NULL);
        if ((current_e->key == current_e->info) || (current_e->info == NULL))
          {
            new_list->enter(current_e->key, current_e->key);
          }
        else
          {
            const void *result;
            boolean exists = list2->exists(current_e->key, &result);
            if (!exists)
              {
                new_list->enter(current_e->key, current_e->key);
              }
            else
              {
                if ((result == current_e->key) || (result == NULL))
                  {
                    new_list->enter(current_e->key, current_e->key);
                  }
                else
                  {
                    in_ldc *ldc1 = (in_ldc *)(current_e->info);
                    in_ldc *ldc2 = (in_ldc *)result;
                    if (ldc1->value() == ldc2->value())
                        new_list->enter(current_e->key, ldc1);
                    else
                        new_list->enter(current_e->key, current_e->key);
                  }
              }
          }
        current_e = current_e->next();
      }
    delete list1;
    delete list2;
    return new_list;
  }

static void clear_all_writes(tree_node_list *the_node_list, alist *value_list)
  {
    the_node_list->map(&clear_writes_on_node, value_list);
  }

static void clear_writes_on_node(tree_node *the_node, void *data)
  {
    alist *value_list = (alist *)data;
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();
        assert(the_instr != NULL);

        operand destination = the_instr->dst_op();
        if (!destination.is_symbol())
            return;

        var_sym *the_var = destination.symbol();
        assert(the_var != NULL);
        clear_var(value_list, the_var);
      }
    else if (the_node->is_for())
      {
        tree_for *the_for = (tree_for *)the_node;
        clear_var(value_list, the_for->index());
      }
  }

static void clear_var(alist *value_list, var_sym *the_var)
  {
    assert(the_var != NULL);

    alist_iter static_iter(value_list);
    while (!static_iter.is_empty())
      {
        alist_e *the_element = static_iter.step();
        var_sym *key_var = (var_sym *)(the_element->key);
        if (!key_var->overlaps(the_var))
            continue;

        value_list->remove(the_element);
        delete the_element;
        value_list->enter(key_var, key_var);
      }
  }

static void possible_jump_to(label_sym *the_label, alist *the_alist)
  {
    label_list_iter label_iter(pending_labels);
    alist_list_iter list_iter(pending_lists);
    while (!label_iter.is_empty())
      {
        label_sym *pending_label = label_iter.step();
        alist **pending_list = list_iter.step();
        if (pending_label == the_label)
          {
            *pending_list =
                    merge_value_lists(*pending_list,
                                      duplicate_alist(the_alist));
          }
      }
    assert(list_iter.is_empty());
  }

static void prop_single_def_constants(tree_node *the_node)
  {
    walk(the_node, &mark_single_def_on_object);
    walk(the_node, &prop_single_def_consts_on_operand);
  }

static void mark_single_def_on_object(suif_object *the_object)
  {
    if (the_object->is_tree_obj())
      {
        tree_node *the_node = (tree_node *)the_object;
        if (the_node->is_for())
          {
            tree_for *the_for = (tree_for *)the_node;
            var_sym *index_var = the_for->index();
            if (index_var->peek_annote(k_porky_multi_def) != NULL)
                return;
            annote *old_annote =
                    index_var->annotes()->get_annote(k_porky_single_def);
            if (old_annote != NULL)
                delete old_annote;
            index_var->append_annote(k_porky_multi_def);
          }
        return;
      }
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (!the_instr->dst_op().is_symbol())
        return;
    var_sym *dest_var = the_instr->dst_op().symbol();
    if (dest_var->is_global() || dest_var->is_param() ||
        dest_var->is_addr_taken() || dest_var->is_static())
      {
        return;
      }
    if (dest_var->peek_annote(k_porky_multi_def) != NULL)
        return;
    annote *old_annote = dest_var->annotes()->get_annote(k_porky_single_def);
    if (old_annote != NULL)
      {
        delete old_annote;
        dest_var->append_annote(k_porky_multi_def);
        return;
      }
    dest_var->append_annote(k_porky_single_def, the_instr);
  }

static void prop_single_def_consts_on_operand(operand the_operand,
                                              so_walker *the_walker)
  {
    if (!the_operand.is_symbol())
        return;
    if (the_walker->in_dest_op())
        return;
    var_sym *this_sym = the_operand.symbol();
    instruction *this_instr =
            (instruction *)(this_sym->peek_annote(k_porky_single_def));
    if (this_instr == NULL)
        return;
    if (this_instr->opcode() != io_ldc)
        return;
    instruction *new_instr = this_instr->clone();
    new_instr->set_dst(operand());
    the_walker->replace_op(operand(new_instr));
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
