/* file "globalize.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to turn static local variables into globals for the porky
 * program for SUIF */

#define RCS_BASE_FILE globalize_cc

#include "porky.h"

RCS_BASE(
    "$Id: globalize.cc,v 1.2 1999/08/25 03:27:29 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_globalize_original_type;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void globalize_on_node(tree_node *the_node, var_sym_list *type_changes);
static void globalize_on_list(tree_node_list *node_list,
                              var_sym_list *type_changes);
static void globalize_on_block_symtab(block_symtab *the_symtab,
                                      var_sym_list *type_changes);
static void globalize_on_instr(instruction *the_instr,
                               var_sym_list *type_changes);
static operand globalize_on_operand(operand the_op,
                                    var_sym_list *type_changes);
static void globalize_var(var_sym *to_globalize, var_sym_list *type_changes);
static type_node *original_type(var_sym *the_var);
static void kill_annotes_not_visible(base_symtab *the_symtab,
                                     suif_object *the_object);
static type_node *closest_visible_type(base_symtab *the_symtab,
                                       type_node *original_type);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_globalize(void)
  {
    k_porky_globalize_original_type =
            lexicon->enter("porky globalize original type")->sp;
  }

extern void globalize_on_proc(tree_proc *the_proc)
  {
    var_sym_list type_changes;
    globalize_on_node(the_proc, &type_changes);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void globalize_on_node(tree_node *the_node, var_sym_list *type_changes)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            globalize_on_instr(the_tree_instr->instr(), type_changes);
            break;
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            globalize_on_list(the_loop->body(), type_changes);
            globalize_on_list(the_loop->test(), type_changes);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;

            operand lb_op = the_for->lb_op();
            lb_op.remove();
            lb_op = globalize_on_operand(lb_op, type_changes);
            the_for->set_lb_op(lb_op);

            operand ub_op = the_for->ub_op();
            ub_op.remove();
            ub_op = globalize_on_operand(ub_op, type_changes);
            the_for->set_ub_op(ub_op);

            operand step_op = the_for->step_op();
            step_op.remove();
            step_op = globalize_on_operand(step_op, type_changes);
            the_for->set_step_op(step_op);

            globalize_on_list(the_for->landing_pad(), type_changes);
            globalize_on_list(the_for->body(), type_changes);

            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;
            globalize_on_list(the_if->header(), type_changes);
            globalize_on_list(the_if->then_part(), type_changes);
            globalize_on_list(the_if->else_part(), type_changes);
            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            globalize_on_block_symtab(the_block->symtab(), type_changes);
            globalize_on_list(the_block->body(), type_changes);
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void globalize_on_list(tree_node_list *node_list,
                              var_sym_list *type_changes)
  {
    tree_node_list_iter node_iter(node_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        globalize_on_node(this_node, type_changes);
      }
  }

static void globalize_on_block_symtab(block_symtab *the_symtab,
                                      var_sym_list *type_changes)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->is_var())
          {
            var_sym *this_var = (var_sym *)this_sym;
            if (this_var->is_static())
                globalize_var(this_var, type_changes);
          }
      }
  }

static void globalize_on_instr(instruction *the_instr,
                               var_sym_list *type_changes)
  {
    if (type_changes->is_empty())
        return;

    if (the_instr->dst_op().is_symbol())
      {
        var_sym *dest_var = the_instr->dst_op().symbol();
        var_sym_list_iter var_iter(type_changes);
        while (!var_iter.is_empty())
          {
            var_sym *test_var = var_iter.step();
            if (test_var == dest_var)
              {
                tree_instr *parent = the_instr->parent();
                assert(parent->instr() == the_instr);
                parent->remove_instr(the_instr);
                in_rrr *new_cast =
                        new in_rrr(io_cvt, dest_var->type()->unqual(),
                                   operand(dest_var), operand(the_instr));
                parent->set_instr(new_cast);
                return;
              }
          }
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_symbol())
          {
            var_sym *this_var = this_op.symbol();
            var_sym_list_iter var_iter(type_changes);
            while (!var_iter.is_empty())
              {
                var_sym *test_var = var_iter.step();
                if (test_var == this_var)
                  {
                    in_rrr *new_cast =
                            new in_rrr(io_cvt,
                                       original_type(this_var)->unqual(),
                                       operand(), operand(this_var));
                    the_instr->set_src_op(src_num, operand(new_cast));
                  }
              }
          }
        else if (this_op.is_expr())
          {
            globalize_on_instr(this_op.instr(), type_changes);
          }
      }
  }

static operand globalize_on_operand(operand the_op,
                                    var_sym_list *type_changes)
  {
    if (type_changes->is_empty())
        return the_op;

    if (the_op.is_symbol())
      {
        var_sym *this_var = the_op.symbol();
        var_sym_list_iter var_iter(type_changes);
        while (!var_iter.is_empty())
          {
            var_sym *test_var = var_iter.step();
            if (test_var == this_var)
              {
                in_rrr *new_cast =
                        new in_rrr(io_cvt, original_type(this_var)->unqual(),
                                   operand(), operand(this_var));
                return operand(new_cast);
              }
          }
      }
    else if (the_op.is_expr())
      {
        globalize_on_instr(the_op.instr(), type_changes);
      }

    return the_op;
  }

static void globalize_var(var_sym *to_globalize, var_sym_list *type_changes)
  {
    base_symtab *follow_symtab = to_globalize->parent();
    assert(follow_symtab != NULL);
    do
      {
        follow_symtab = follow_symtab->parent();
        assert(follow_symtab != NULL);
      } while (!follow_symtab->is_file());

    kill_annotes_not_visible(follow_symtab, to_globalize);

    if (to_globalize->has_var_def())
      {
        var_def *the_def = to_globalize->definition();
        kill_annotes_not_visible(follow_symtab, the_def);
        the_def->parent()->remove_def(the_def);
        follow_symtab->add_def(the_def);
      }

    to_globalize->parent()->remove_sym(to_globalize);
    to_globalize->clear_sym_id();
    follow_symtab->add_sym(to_globalize);

    if (!follow_symtab->is_ancestor(to_globalize->type()->parent()))
      {
        type_node *original_type = to_globalize->type();
        type_node *new_type =
                closest_visible_type(follow_symtab, original_type);
        to_globalize->append_annote(k_porky_globalize_original_type,
                                    original_type);
        to_globalize->set_type(new_type);
        type_changes->append(to_globalize);
      }
  }

static type_node *original_type(var_sym *the_var)
  {
    return (type_node *)(the_var->peek_annote(
                                 k_porky_globalize_original_type));
  }

/*
 *  Note that this is not a general purpose routine to kill
 *  annotations that refer to things that aren't visible in a
 *  particular scope -- it assumes that all static variables are ok.
 */
static void kill_annotes_not_visible(base_symtab *the_symtab,
                                     suif_object *the_object)
  {
    annote_list_e *list_e = the_object->annotes()->head();
    while (list_e != NULL)
      {
        annote_list_e *next_e = list_e->next();
        annote *this_annote = list_e->contents;
        if (this_annote->name() != k_porky_globalize_original_type)
          {
            immed_list_iter immed_iter(this_annote->immeds());
            while (!immed_iter.is_empty())
              {
                immed this_immed = immed_iter.step();
                boolean throw_away = FALSE;
                if (this_immed.is_symbol())
                  {
                    sym_node *this_sym = this_immed.symbol();
                    if (!the_symtab->is_ancestor(this_sym->parent()))
                      {
                        throw_away = TRUE;
                        if (this_sym->is_var())
                          {
                            var_sym *this_var = (var_sym *)this_sym;
                            if (this_var->is_static())
                                throw_away = FALSE;
                          }
                      }
                  }
                else if (this_immed.is_type())
                  {
                    if (!the_symtab->is_ancestor(this_immed.type()->parent()))
                        throw_away = TRUE;
                  }
                else if (this_immed.is_op())
                  {
                    throw_away = TRUE;
                  }
                else if (this_immed.is_instr())
                  {
                    throw_away = TRUE;
                  }

                if (throw_away)
                  {
                    the_object->annotes()->remove(list_e);
                    delete list_e;
                    delete this_annote;
                    break;
                  }
              }
          }
        list_e = next_e;
      }
  }

/*
 *  This assumes that original type is an object type with known size.
 */
static type_node *closest_visible_type(base_symtab *the_symtab,
                                       type_node *original_type)
  {
    if (the_symtab->is_ancestor(original_type->parent()))
        return original_type;

    switch (original_type->op())
      {
        case TYPE_INT:
        case TYPE_FLOAT:
        case TYPE_VOID:
          {
            base_type *old_base_type = (base_type *)original_type;
            type_node *new_base_type =
                    new base_type(old_base_type->op(), old_base_type->size(),
                                  old_base_type->is_signed());
            return fileset->globals()->install_type(new_base_type);
          }
        case TYPE_PTR:
            return type_ptr;
        case TYPE_ARRAY:
          {
            array_type *old_array = (array_type *)original_type;
            type_node *new_elem_type =
                    closest_visible_type(the_symtab, old_array->elem_type());
            array_bound lower_bound = old_array->lower_bound();
            array_bound upper_bound = old_array->upper_bound();
            assert(!lower_bound.is_variable());
            assert(!upper_bound.is_variable());
            array_type *new_array =
                    new array_type(new_elem_type, lower_bound, upper_bound);
            return new_elem_type->parent()->install_type(new_array);
          }
        case TYPE_FUNC:
            assert(FALSE);
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *old_struct = (struct_type *)original_type;
            unsigned num_fields = old_struct->num_fields();
            struct_type *new_struct =
                    new struct_type(old_struct->op(), old_struct->size(),
                                    old_struct->name(), num_fields);
            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
              {
                type_node *old_field_type = old_struct->field_type(field_num);
                type_node *new_field_type =
                        closest_visible_type(the_symtab, old_field_type);
                                             
                new_struct->set_field_name(field_num,
                                           old_struct->field_name(field_num));
                new_struct->set_field_type(field_num, new_field_type);
              }
            return the_symtab->install_type(new_struct);
          }
        case TYPE_ENUM:
          {
            base_type *old_base_type = (base_type *)original_type;
            type_node *new_base_type =
                    new base_type(TYPE_INT, old_base_type->size(),
                                  old_base_type->is_signed());
            return fileset->globals()->install_type(new_base_type);
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *old_modifier = (modifier_type *)original_type;
            type_node *new_base_type =
                    closest_visible_type(the_symtab, old_modifier->base());
            modifier_type *new_modifier =
                    new modifier_type(old_modifier->op(), new_base_type);
            return new_base_type->parent()->install_type(new_modifier);
          }
        default:
            assert(FALSE);
            return NULL;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
