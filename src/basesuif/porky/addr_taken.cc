/* file "addr_taken.cc" */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to set or reset the is_addr_taken() flag of var_syms as
 * appropriate, for the porky program for SUIF */


#define RCS_BASE_FILE addr_taken_cc

#include "porky.h"

RCS_BASE(
    "$Id: addr_taken.cc,v 1.2 1999/08/25 03:27:23 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void fix_addr_taken_on_object(suif_object *the_object);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void fix_addr_taken_start_symtab(base_symtab *the_symtab)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (!this_sym->is_var())
            continue;
        var_sym *this_var = (var_sym *)this_sym;
        if (!unreferenced_outside_fileset(this_sym))
            continue;
        this_var->reset_addr_taken();
      }
  }

extern void fix_addr_taken_on_proc(tree_proc *the_proc)
  {
    so_walker the_walker;
    the_walker.set_pre_function(&fix_addr_taken_on_object);
    the_walker.walk(the_proc);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void fix_addr_taken_on_object(suif_object *the_object)
  {
    if (the_object->is_instr_obj())
      {
        instruction *the_instr = (instruction *)the_object;
        if (the_instr->opcode() == io_ldc)
          {
            in_ldc *the_ldc = (in_ldc *)the_instr;
            immed value = the_ldc->value();
            if (value.is_symbol())
              {
                sym_node *the_sym = value.symbol();
                if (the_sym->is_var())
                  {
                    var_sym *this_var = (var_sym *)the_sym;
                    this_var->set_addr_taken();
                  }
              }
          }
      }
    else if (the_object->is_tree_obj())
      {
        tree_node *the_tree_node = (tree_node *)the_object;
        if (the_tree_node->is_block())
          {
            tree_block *this_block = (tree_block *)the_tree_node;
            fix_addr_taken_start_symtab(this_block->symtab());
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
