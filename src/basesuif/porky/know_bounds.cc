/* file "know_bounds.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE know_bounds_cc

#include "porky.h"

RCS_BASE(
    "$Id: know_bounds.cc,v 1.2 1999/08/25 03:27:30 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

DECLARE_LIST_CLASS(rrr_list, in_rrr *);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void node_know_bounds(tree_node *the_node, rrr_list *context);
static void node_list_know_bounds(tree_node_list *the_list, rrr_list *context);
static void instr_know_bounds(instruction *the_instr, rrr_list *context);
static void reverse_compare_op(in_rrr *comp_rrr);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void proc_know_bounds(tree_proc *the_proc)
  {
    rrr_list context;
    node_know_bounds(the_proc, &context);
    assert(context.is_empty());
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void node_know_bounds(tree_node *the_node, rrr_list *context)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instr_know_bounds(the_tree_instr->instr(), context);
            break;
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            node_list_know_bounds(the_loop->body(), context);
            node_list_know_bounds(the_loop->test(), context);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;

            if (the_for->lb_op().is_expr())
                instr_know_bounds(the_for->lb_op().instr(), context);
            if (the_for->ub_op().is_expr())
                instr_know_bounds(the_for->ub_op().instr(), context);
            if (the_for->step_op().is_expr())
                instr_know_bounds(the_for->step_op().instr(), context);

            node_list_know_bounds(the_for->landing_pad(), context);

            if_ops upper_opcode;
            boolean going_up;
            switch (the_for->test())
              {
                case FOR_EQ:
                case FOR_NEQ:
                    upper_opcode = io_nop;
                    going_up = TRUE;
                    break;
                case FOR_SGT:
                case FOR_UGT:
                    upper_opcode = io_sl;
                    going_up = FALSE;
                    break;
                case FOR_SGTE:
                case FOR_UGTE:
                    upper_opcode = io_sle;
                    going_up = FALSE;
                    break;
                case FOR_SLT:
                case FOR_ULT:
                    upper_opcode = io_sl;
                    going_up = TRUE;
                    break;
                case FOR_SLTE:
                case FOR_ULTE:
                    upper_opcode = io_sle;
                    going_up = TRUE;
                    break;
                default:
                    assert(FALSE);
              }

            in_rrr *lower_rrr = NULL;
            in_rrr *upper_rrr = NULL;
            if (upper_opcode != io_nop)
              {
                operand lower_left = the_for->lb_op().clone();
                operand lower_right = operand(the_for->index());
                operand upper_left = operand(the_for->index());
                operand upper_right = the_for->ub_op().clone();

                boolean lower_ok =
                        (!might_modify(lower_left, the_for->body()) &&
                         (!operand_may_reference_var(lower_left,
                                                     the_for->index())));
                boolean upper_ok =
                        (!might_modify(upper_right, the_for->body()) &&
                         (!operand_may_reference_var(upper_right,
                                                     the_for->index())));

                if (!going_up)
                  {
                    operand temp_op = lower_left;
                    lower_left = lower_right;
                    lower_right = temp_op;

                    temp_op = upper_left;
                    upper_left = upper_right;
                    upper_right = temp_op;
                  }

                if (lower_ok)
                  {
                    lower_rrr =
                            new in_rrr(io_sle, type_signed, operand(),
                                       lower_left, lower_right);
                    context->push(lower_rrr);
                  }
                else
                  {
                    kill_op(lower_left);
                    kill_op(lower_right);
                  }

                if (upper_ok)
                  {
                    upper_rrr =
                            new in_rrr(upper_opcode, type_signed, operand(),
                                       upper_left, upper_right);
                    context->push(upper_rrr);
                  }
                else
                  {
                    kill_op(upper_left);
                    kill_op(upper_right);
                  }
              }

            node_list_know_bounds(the_for->body(), context);

            if (lower_rrr != NULL)
              {
                (void)(context->pop());
                delete lower_rrr;
              }
            if (upper_rrr != NULL)
              {
                (void)(context->pop());
                delete upper_rrr;
              }
            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;
            node_list_know_bounds(the_if->header(), context);

            tree_node *test_node = single_effect(the_if->header());
            in_rrr *comp_rrr = NULL;
            if ((test_node != NULL) && test_node->is_instr())
              {
                tree_instr *test_tree_instr = (tree_instr *)test_node;
                instruction *test_instr = test_tree_instr->instr();
                if ((test_instr->opcode() == io_btrue) ||
                    (test_instr->opcode() == io_bfalse))
                  {
                    in_bj *test_bj = (in_bj *)test_instr;
                    if (test_bj->target() == the_if->jumpto())
                      {
                        operand test_op = test_bj->src_op().clone();
                        if (test_op.is_expr())
                          {
                            instruction *comp_instr = test_op.instr();
                            switch (comp_instr->opcode())
                              {
                                case io_sl:
                                case io_sle:
                                case io_seq:
                                case io_sne:
                                    comp_rrr = (in_rrr *)comp_instr;
                                    break;
                                default:
                                    break;
                              }
                          }

                        if (comp_rrr == NULL)
                          {
                            operand zero_op = operand_int(type_signed, 0);
                            comp_rrr =
                                    new in_rrr(io_sne, type_signed, operand(),
                                               test_op, zero_op);
                          }

                        if (test_bj->opcode() == io_btrue)
                            reverse_compare_op(comp_rrr);
                      }
                  }
              }

            boolean ok_in_then = FALSE;
            boolean ok_in_else = FALSE;
            if (comp_rrr != NULL)
              {
                ok_in_then = !might_modify(comp_rrr, the_if->then_part());
                ok_in_else = !might_modify(comp_rrr, the_if->else_part());
              }

            if (ok_in_then)
                context->push(comp_rrr);
            node_list_know_bounds(the_if->then_part(), context);
            if (ok_in_then && (!ok_in_else))
                (void)(context->pop());
            else if ((!ok_in_then) && ok_in_else)
                context->push(comp_rrr);
            if (ok_in_else)
                reverse_compare_op(comp_rrr);
            node_list_know_bounds(the_if->else_part(), context);
            if (ok_in_else)
                (void)(context->pop());

            if (comp_rrr != NULL)
                delete comp_rrr;

            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            node_list_know_bounds(the_block->body(), context);
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void node_list_know_bounds(tree_node_list *the_list, rrr_list *context)
  {
    tree_node_list_iter node_iter(the_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        node_know_bounds(this_node, context);
      }
  }

static void instr_know_bounds(instruction *the_instr, rrr_list *context)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_expr())
            instr_know_bounds(this_op.instr(), context);
      }

    if ((the_instr->opcode() == io_sl) || (the_instr->opcode() == io_sle) ||
        (the_instr->opcode() == io_seq) || (the_instr->opcode() == io_sne))
      {
        in_rrr *the_rrr = (in_rrr *)the_instr;
        operand op1 = the_rrr->src1_op();
        operand op2 = the_rrr->src2_op();
        operand temp_this_diff =
                fold_real_2op_rrr(io_sub, op1.type(), op1.clone(),
                                  op2.clone());

        rrr_list_iter rrr_iter(context);
        while (!rrr_iter.is_empty())
          {
            in_rrr *comp_rrr = rrr_iter.step();
            operand temp_comp1 = comp_rrr->src1_op().clone();
            operand temp_comp2 = comp_rrr->src2_op().clone();
            operand temp_comp_op1 =
                    fold_real_2op_rrr(io_sub, temp_comp1.type(),
                                      temp_comp1.clone(), temp_comp2.clone());
            operand temp_comp_op2 =
                    fold_real_2op_rrr(io_sub, temp_comp1.type(), temp_comp2,
                                      temp_comp1);
            operand temp_total1 =
                    fold_real_2op_rrr(io_sub, temp_this_diff.type(),
                                      temp_comp_op1, temp_this_diff.clone());
            operand temp_total2 =
                    fold_real_2op_rrr(io_sub, temp_this_diff.type(),
                                      temp_comp_op2, temp_this_diff.clone());
            temp_total1 = fold_constants(temp_total1);
            temp_total2 = fold_constants(temp_total2);

            boolean is_zero = FALSE;
            boolean is_one = FALSE;
            int const_value;

            eval_status result =
                    evaluate_const_int_expr(temp_total1, &const_value);
            if (result == EVAL_OK)
              {
                switch (the_rrr->opcode())
                  {
                    case io_sl:
                        switch (comp_rrr->opcode())
                          {
                            case io_sl:
                                is_one = (const_value >= 0);
                                break;
                            case io_sle:
                                is_one = (const_value > 0);
                                break;
                            case io_seq:
                                is_one = (const_value > 0);
                                is_zero = (const_value <= 0);
                                break;
                            case io_sne:
                                break;
                            default:
                                assert(FALSE);
                          }
                        break;
                    case io_sle:
                        switch (comp_rrr->opcode())
                          {
                            case io_sl:
                                is_one = (const_value >= -1);
                                break;
                            case io_sle:
                                is_one = (const_value >= 0);
                                break;
                            case io_seq:
                                is_one = (const_value >= 0);
                                is_zero = (const_value < 0);
                                break;
                            case io_sne:
                                break;
                            default:
                                assert(FALSE);
                          }
                        break;
                    case io_seq:
                        switch (comp_rrr->opcode())
                          {
                            case io_sl:
                                is_zero = (const_value >= 0);
                                break;
                            case io_sle:
                                is_zero = (const_value > 0);
                                break;
                            case io_seq:
                                is_one = (const_value == 0);
                                is_zero = (const_value != 0);
                                break;
                            case io_sne:
                                is_zero = (const_value == 0);
                                break;
                            default:
                                assert(FALSE);
                          }
                        break;
                    case io_sne:
                        switch (comp_rrr->opcode())
                          {
                            case io_sl:
                                is_one = (const_value >= 0);
                                break;
                            case io_sle:
                                is_one = (const_value > 0);
                                break;
                            case io_seq:
                                is_one = (const_value != 0);
                                is_zero = (const_value == 0);
                                break;
                            case io_sne:
                                is_one = (const_value == 0);
                                break;
                            default:
                                assert(FALSE);
                          }
                        break;
                    default:
                        assert(FALSE);
                  }
              }

            result = evaluate_const_int_expr(temp_total2, &const_value);
            if (result == EVAL_OK)
              {
                switch (the_rrr->opcode())
                  {
                    case io_sl:
                        switch (comp_rrr->opcode())
                          {
                            case io_sl:
                                is_zero = (const_value <= 0);
                                break;
                            case io_sle:
                                is_zero = (const_value <= 1);
                                break;
                            case io_seq:
                                is_one = (const_value > 0);
                                is_zero = (const_value <= 0);
                                break;
                            case io_sne:
                                break;
                            default:
                                assert(FALSE);
                          }
                        break;
                    case io_sle:
                        switch (comp_rrr->opcode())
                          {
                            case io_sl:
                                is_zero = (const_value <= 0);
                                break;
                            case io_sle:
                                is_zero = (const_value < 0);
                                break;
                            case io_seq:
                                is_one = (const_value >= 0);
                                is_zero = (const_value < 0);
                                break;
                            case io_sne:
                                break;
                            default:
                                assert(FALSE);
                          }
                        break;
                    case io_seq:
                        switch (comp_rrr->opcode())
                          {
                            case io_sl:
                                is_zero = (const_value <= 0);
                                break;
                            case io_sle:
                                is_zero = (const_value < 0);
                                break;
                            case io_seq:
                                is_one = (const_value == 0);
                                is_zero = (const_value != 0);
                                break;
                            case io_sne:
                                is_zero = (const_value == 0);
                                break;
                            default:
                                assert(FALSE);
                          }
                        break;
                    case io_sne:
                        switch (comp_rrr->opcode())
                          {
                            case io_sl:
                                is_one = (const_value <= 0);
                                break;
                            case io_sle:
                                is_one = (const_value < 0);
                                break;
                            case io_seq:
                                is_one = (const_value != 0);
                                is_zero = (const_value == 0);
                                break;
                            case io_sne:
                                is_one = (const_value == 0);
                                break;
                            default:
                                assert(FALSE);
                          }
                        break;
                    default:
                        assert(FALSE);
                  }
              }

            kill_op(temp_total1);
            kill_op(temp_total2);

            if (is_one || is_zero)
              {
                assert((!is_one) || (!is_zero));
                in_ldc *new_ldc =
                        new in_ldc(the_instr->result_type(), operand(),
                                   immed(is_zero ? 0 : 1));
                replace_instruction(the_instr, new_ldc);
                delete the_instr;
                kill_op(temp_this_diff);
                return;
              }
          }

        kill_op(temp_this_diff);
      }
  }

static void reverse_compare_op(in_rrr *comp_rrr)
  {
    switch (comp_rrr->opcode())
      {
        case io_seq:
            comp_rrr->set_opcode(io_sne);
            break;
        case io_sne:
            comp_rrr->set_opcode(io_seq);
            break;
        case io_sl:
        case io_sle:
          {
            operand op1 = comp_rrr->src1_op();
            operand op2 = comp_rrr->src2_op();
            op1.remove();
            op2.remove();
            comp_rrr->set_src1(op2);
            comp_rrr->set_src2(op1);
            comp_rrr->set_opcode((comp_rrr->opcode() == io_sl) ? io_sle :
                                 io_sl);
            break;
          }
        default:
            assert(FALSE);
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
