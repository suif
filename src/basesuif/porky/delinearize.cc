/* file "delinearize.cc" */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to try to delinearize references to multi-dimensional arrays
 * for the porky program for SUIF */


#define RCS_BASE_FILE delinearize_cc

#include "porky.h"
#include <limits.h>

RCS_BASE(
    "$Id: delinearize.cc,v 1.2 1999/08/25 03:27:27 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void delinearize_on_object(suif_object *the_object);
static void delinearize_on_aref(in_array *the_aref);
static boolean linear_match(i_integer size, operand original_op,
                            operand *result_a, operand *result_b);
static boolean upper_bound_guaranteed(operand the_op, i_integer bound,
                                      tree_node *position);
static boolean lower_bound_guaranteed(operand the_op, i_integer bound,
                                      tree_node *position);
static boolean upper_bound_guaranteed(var_sym *the_var, i_integer bound,
                                      tree_node *position);
static boolean lower_bound_guaranteed(var_sym *the_var, i_integer bound,
                                      tree_node *position);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void delinearize_on_proc(tree_proc *the_proc)
  {
    so_walker the_walker;
    the_walker.set_post_function(&delinearize_on_object);
    the_walker.walk(the_proc);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void delinearize_on_object(suif_object *the_object)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_array)
        return;
    in_array *the_aref = (in_array *)the_instr;
    delinearize_on_aref(the_aref);
  }

static void delinearize_on_aref(in_array *the_aref)
  {
    operand base_op = the_aref->base_op();
    sym_node *base_sym = operand_address_root_symbol(base_op);
    if ((base_sym == NULL) || (!base_sym->is_var()))
        return;
    var_sym *base_var = (var_sym *)base_sym;
    if (base_var->parent_var() != NULL)
        return;
    type_node *var_type = base_var->type()->unqual();
    if (!var_type->is_array())
        return;
    array_type *var_array_type = (array_type *)var_type;

    type_node *op_type = base_op.type();
    assert(op_type->is_ptr());
    ptr_type *op_ptr = (ptr_type *)op_type;
    type_node *op_ref = op_ptr->ref_type()->unqual();
    assert(op_ref->is_array());
    array_type *op_array_type = (array_type *)op_ref;

    if (the_aref->dims() != 1)
        return;
    if (!the_aref->offset_op().is_null())
        return;

    unsigned dim_count = 0;
    type_node *follow_type = var_array_type;
    while (follow_type->is_array() &&
           (follow_type != op_array_type->elem_type()))
      {
        array_type *follow_array = (array_type *)follow_type;
        if ((!follow_array->lower_bound().is_constant()) ||
            (!follow_array->upper_bound().is_constant()))
          {
            break;
          }
        follow_type = follow_array->elem_type()->unqual();
        ++dim_count;
        if (dim_count == UINT_MAX)
            break;
      }
    if (follow_type != op_array_type->elem_type())
        return;
    if (dim_count <= 1)
        return;

    array_type **array_table = new array_type *[dim_count];
    follow_type = var_array_type;
    unsigned dim_follow = 0;
    while (follow_type != op_array_type->elem_type())
      {
        assert(follow_type->is_array());
        array_type *follow_array = (array_type *)follow_type;
        array_table[dim_follow] = follow_array;
        follow_type = follow_array->elem_type()->unqual();
        ++dim_follow;
      }

    operand *index_ops = new operand[dim_count];

    operand remainder = the_aref->index(0).clone();
    for (unsigned dim_num = dim_count - 1; dim_num > 0; --dim_num)
      {
        i_integer lb = array_table[dim_num]->lower_bound().constant();
        i_integer ub = array_table[dim_num]->upper_bound().constant();
        operand inner_index, outer_index;
        boolean is_match =
                linear_match((ub - lb) + 1, remainder, &outer_index,
                             &inner_index);
        kill_op(remainder);
        if (!is_match)
          {
            for (unsigned dim_num = 0; dim_num < dim_count; ++dim_num)
                kill_op(index_ops[dim_num]);
            delete[] index_ops;
            delete[] array_table;
            return;
          }
        index_ops[dim_num] = inner_index;
        remainder = outer_index;
      }
    index_ops[0] = remainder;

      {
        for (unsigned dim_num = 0; dim_num < dim_count; ++dim_num)
          {
            i_integer lb = array_table[dim_num]->lower_bound().constant();
            i_integer ub = array_table[dim_num]->upper_bound().constant();
            operand this_index = index_ops[dim_num];
            if ((!upper_bound_guaranteed(this_index, ub, the_aref->owner())) ||
                (!lower_bound_guaranteed(this_index, lb, the_aref->owner())))
              {
                for (unsigned dim_num = 0; dim_num < dim_count; ++dim_num)
                    kill_op(index_ops[dim_num]);
                delete[] index_ops;
                delete[] array_table;
                return;
              }
          }
      }

    i_integer offset_ii = 0;
      {
        for (unsigned dim_num = 0; dim_num < dim_count; ++dim_num)
          {
            i_integer lb = array_table[dim_num]->lower_bound().constant();
            i_integer ub = array_table[dim_num]->upper_bound().constant();
            offset_ii = offset_ii * ((ub - lb) + 1) + lb;
          }
      }
    operand new_offset_op = const_op(offset_ii, type_ptr_diff);
    base_op.remove();
    base_op = cast_op(base_op, var_array_type->ptr_to());
    in_array *new_aref =
            new in_array(the_aref->result_type(), operand(), base_op,
                         the_aref->elem_size(), dim_count, the_aref->offset(),
                         new_offset_op);
      {
        for (unsigned dim_num = 0; dim_num < dim_count; ++dim_num)
          {
            new_aref->set_index(dim_num, index_ops[dim_num]);
            i_integer lb = array_table[dim_num]->lower_bound().constant();
            i_integer ub = array_table[dim_num]->upper_bound().constant();
            new_aref->set_bound(dim_num,
                                const_op((ub - lb) + 1, type_ptr_diff));
          }
      }
    replace_instruction(the_aref, new_aref);
    delete the_aref;
    delete[] index_ops;
    delete[] array_table;
  }

/*
 *  If possible, return *result_a, *result_b s.t.
 *
 *      original_op = (*result_a * size) + *result_b
 */
static boolean linear_match(i_integer size, operand original_op,
                            operand *result_a, operand *result_b)
  {
    if (!original_op.is_expr())
        return FALSE;
    instruction *the_instr = original_op.instr();
    switch (the_instr->opcode())
      {
        case io_ldc:
          {
            return FALSE;
          } 
        case io_cvt:
        case io_cpy:
          {
            in_rrr *the_cvt = (in_rrr *)the_instr;
            type_node *source_type = the_cvt->src_op().type();
            type_node *dest_type = original_op.type();
            if ((source_type->op() != TYPE_INT) ||
                (dest_type->op() != TYPE_INT))
              {
                return FALSE;
              }
            base_type *source_base = (base_type *)source_type;
            base_type *dest_base = (base_type *)dest_type;
            if ((source_base->size() > dest_base->size()) ||
                (source_base->is_signed() != dest_base->is_signed()))
              {
                return FALSE;
              }
            return linear_match(size, the_cvt->src_op(), result_a, result_b);
          }
        case io_add:
          {
            in_rrr *the_add = (in_rrr *)the_instr;
            operand src1 = the_add->src1_op();
            operand src2 = the_add->src2_op();

              {
                operand test_a, test_b;
                boolean test_result =
                        linear_match(size, src1, &test_a, &test_b);
                if (test_result)
                  {
                    *result_a = test_a;
                    *result_b = test_b + src2;
                    return TRUE;
                  }
              }

              {
                operand test_a, test_b;
                boolean test_result =
                        linear_match(size, src2, &test_a, &test_b);
                if (test_result)
                  {
                    *result_a = test_a;
                    *result_b = test_b + src1;
                    return TRUE;
                  }
              }

            return FALSE;
          }
        case io_sub:
          {
            in_rrr *the_sub = (in_rrr *)the_instr;
            operand src1 = the_sub->src1_op();
            operand src2 = the_sub->src2_op();

              {
                operand test_a, test_b;
                boolean test_result =
                        linear_match(size, src1, &test_a, &test_b);
                if (test_result)
                  {
                    *result_a = test_a;
                    *result_b = test_b - src2;
                    return TRUE;
                  }
              }

              {
                operand test_a, test_b;
                boolean test_result =
                        linear_match(size, src2, &test_a, &test_b);
                if (test_result)
                  {
                    *result_a = (-test_a);
                    *result_b = (-test_b) + src1;
                    return TRUE;
                  }
              }

            return FALSE;
          }
        case io_mul:
          {
            in_rrr *the_mul = (in_rrr *)the_instr;
            operand src1 = the_mul->src1_op();
            operand src2 = the_mul->src2_op();

              {
                immed const_value;
                eval_status status = evaluate_const_expr(src1, &const_value);
                if ((status == EVAL_OK) && (const_value == ii_to_immed(size)))
                  {
                    *result_a = src2.clone();
                    *result_b = const_op((i_integer)0, src2.type());
                    return TRUE;
                  }
              }

              {
                immed const_value;
                eval_status status = evaluate_const_expr(src2, &const_value);
                if ((status == EVAL_OK) && (const_value == ii_to_immed(size)))
                  {
                    *result_a = src1.clone();
                    *result_b = const_op((i_integer)0, src1.type());
                    return TRUE;
                  }
              }

            return FALSE;
          }
        default:
            return FALSE;
      }
  }

static boolean upper_bound_guaranteed(operand the_op, i_integer bound,
                                      tree_node *position)
  {
    switch (the_op.kind())
      {
        case OPER_NULL:
            return FALSE;
        case OPER_SYM:
            return upper_bound_guaranteed(the_op.symbol(), bound, position);
        case OPER_INSTR:
          {
            instruction *the_instr = the_op.instr();
            switch (the_instr->opcode())
              {
                case io_cpy:
                case io_cvt:
                  {
                    in_rrr *the_rrr = (in_rrr *)the_instr;
                    type_node *source_type = the_rrr->src_op().type();
                    type_node *dest_type = the_op.type();
                    if ((source_type->op() != TYPE_INT) ||
                        (dest_type->op() != TYPE_INT))
                      {
                        return FALSE;
                      }
                    base_type *source_base = (base_type *)source_type;
                    base_type *dest_base = (base_type *)dest_type;
                    if ((source_base->size() > dest_base->size()) ||
                        (source_base->is_signed() != dest_base->is_signed()))
                      {
                        return FALSE;
                      }
                    return upper_bound_guaranteed(the_rrr->src_op(), bound,
                                                  position);
                  }
                default:
                    return FALSE;
              }
          }
        default:
            assert(FALSE);
            return FALSE;
      }
  }

static boolean lower_bound_guaranteed(operand the_op, i_integer bound,
                                      tree_node *position)
  {
    switch (the_op.kind())
      {
        case OPER_NULL:
            return FALSE;
        case OPER_SYM:
            return lower_bound_guaranteed(the_op.symbol(), bound, position);
        case OPER_INSTR:
          {
            instruction *the_instr = the_op.instr();
            switch (the_instr->opcode())
              {
                case io_cpy:
                case io_cvt:
                  {
                    in_rrr *the_rrr = (in_rrr *)the_instr;
                    type_node *source_type = the_rrr->src_op().type();
                    type_node *dest_type = the_op.type();
                    if ((source_type->op() != TYPE_INT) ||
                        (dest_type->op() != TYPE_INT))
                      {
                        return FALSE;
                      }
                    base_type *source_base = (base_type *)source_type;
                    base_type *dest_base = (base_type *)dest_type;
                    if ((source_base->size() > dest_base->size()) ||
                        (source_base->is_signed() != dest_base->is_signed()))
                      {
                        return FALSE;
                      }
                    return lower_bound_guaranteed(the_rrr->src_op(), bound,
                                                  position);
                  }
                default:
                    return FALSE;
              }
          }
        default:
            assert(FALSE);
            return FALSE;
      }
  }

/*
 *  Guarantee the_var <= bound?
 */
static boolean upper_bound_guaranteed(var_sym *the_var, i_integer bound,
                                      tree_node *position)
  {
    tree_node *follow_node = position;
    while (TRUE)
      {
        tree_node_list *parent = follow_node->parent();
        if (parent == NULL)
            return FALSE;
        follow_node = parent->parent();
        if (follow_node == NULL)
            return FALSE;
        if (!follow_node->is_for())
            continue;
        tree_for *the_for = (tree_for *)follow_node;
        if (the_for->index() != the_var)
            continue;
        if (the_for->body() != parent)
            continue;
        immed step_immed;
        eval_status step_status =
                evaluate_const_expr(the_for->step_op(), &step_immed);
        if (step_status != EVAL_OK)
            continue;
        if (!step_immed.is_int_const())
            continue;
        i_integer step_ii = immed_to_ii(step_immed);
        if (step_ii > 0)
          {
            immed bound_immed;
            eval_status bound_status =
                    evaluate_const_expr(the_for->ub_op(), &bound_immed);
            if (bound_status != EVAL_OK)
                continue;
            if (!bound_immed.is_int_const())
                continue;
            i_integer bound_ii = immed_to_ii(bound_immed);
            switch (the_for->test())
              {
                case FOR_SLT:
                case FOR_ULT:
                    if (bound >= bound_ii - 1)
                        return TRUE;
                    break;
                case FOR_SLTE:
                case FOR_ULTE:
                    if (bound >= bound_ii)
                        return TRUE;
                    break;
                default:
                    break;
              }
          }
        else if (step_ii < 0)
          {
            immed bound_immed;
            eval_status bound_status =
                    evaluate_const_expr(the_for->lb_op(), &bound_immed);
            if (bound_status != EVAL_OK)
                continue;
            if (!bound_immed.is_int_const())
                continue;
            i_integer bound_ii = immed_to_ii(bound_immed);
            if (bound >= bound_ii)
                return TRUE;
          }
      }
  }

/*
 *  Guarantee the_var >= bound?
 */
static boolean lower_bound_guaranteed(var_sym *the_var, i_integer bound,
                                      tree_node *position)
  {
    tree_node *follow_node = position;
    while (TRUE)
      {
        tree_node_list *parent = follow_node->parent();
        if (parent == NULL)
            return FALSE;
        follow_node = parent->parent();
        if (follow_node == NULL)
            return FALSE;
        if (!follow_node->is_for())
            continue;
        tree_for *the_for = (tree_for *)follow_node;
        if (the_for->index() != the_var)
            continue;
        immed step_immed;
        eval_status step_status =
                evaluate_const_expr(the_for->step_op(), &step_immed);
        if (step_status != EVAL_OK)
            continue;
        if (!step_immed.is_int_const())
            continue;
        i_integer step_ii = immed_to_ii(step_immed);
        if (step_ii > 0)
          {
            immed bound_immed;
            eval_status bound_status =
                    evaluate_const_expr(the_for->lb_op(), &bound_immed);
            if (bound_status != EVAL_OK)
                continue;
            if (!bound_immed.is_int_const())
                continue;
            i_integer bound_ii = immed_to_ii(bound_immed);
            if (bound <= bound_ii)
                return TRUE;
          }
        else if (step_ii < 0)
          {
            immed bound_immed;
            eval_status bound_status =
                    evaluate_const_expr(the_for->ub_op(), &bound_immed);
            if (bound_status != EVAL_OK)
                continue;
            if (!bound_immed.is_int_const())
                continue;
            i_integer bound_ii = immed_to_ii(bound_immed);
            switch (the_for->test())
              {
                case FOR_SGT:
                case FOR_UGT:
                    if (bound <= bound_ii + 1)
                        return TRUE;
                    break;
                case FOR_SGTE:
                case FOR_UGTE:
                    if (bound <= bound_ii)
                        return TRUE;
                    break;
                default:
                    break;
              }
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
