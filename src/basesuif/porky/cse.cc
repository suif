/* file "cse.cc" */

/*  Copyright (c) 1994,95 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Common Sub-expression Elimination for the porky program for SUIF */

#define RCS_BASE_FILE cse_cc

#include "fact.h"
#include "structured_facts.h"
#include "porky.h"

RCS_BASE(
    "$Id: cse.cc,v 1.2 1999/08/25 03:27:26 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

class operand_place
  {
public:
    instruction *parent;
    unsigned src_num;

    operand_place(void)  { parent = NULL; src_num = 0; }
    operand_place(instruction *new_parent, unsigned new_src_num)
      { parent = new_parent; src_num = new_src_num; }
    boolean operator==(const operand_place &other) const
      { return ((parent == other.parent) && (src_num == other.src_num)); }
    boolean operator!=(const operand_place &other) const
      { return !(*this == other); }
    operand_place &operator=(const operand_place &other)
      { parent = other.parent; src_num = other.src_num; return *this; }
    operand op(void) const  { return parent->src_op(src_num); }
  };

DECLARE_LIST_CLASS(operand_place_list, operand_place);

class cse_fact : public propagated_fact
  {
protected:
    virtual ~cse_fact(void) { }

public:
    virtual boolean is_expr_fact(void) = 0;
  };

class cse_expr_fact : public cse_fact
  {
    friend class cse_fact_manager;

private:
    operand_place the_expr;
    operand original_form;
    operand_place_list other_exprs;

    cse_expr_fact(operand_place new_expr, operand new_original)
      {
        the_expr = new_expr;
        original_form = new_original;
      }

    boolean fact_ok_for_node(tree_node *the_node);
    boolean fact_ok_for_instr(instruction *the_instr);
    boolean fact_ok_for_op(operand the_op);

public:
    cse_expr_fact(operand_place new_place);
    virtual ~cse_expr_fact(void)  { }

    propagated_fact *clone(void)
      {
        cse_expr_fact *new_fact = new cse_expr_fact(the_expr, original_form);
        operand_place_list_iter op_place_iter(&other_exprs);
        while (!op_place_iter.is_empty())
          {
            operand_place this_op_place = op_place_iter.step();
            new_fact->other_exprs.append(this_op_place);
          }
        return new_fact;
      }
    boolean killed_by_var_write(var_sym *the_var)
      {
        return operand_may_reference_var(original_form, the_var);
      }
    boolean killed_by_unknown_mem_write(void)
      {
        return (operand_reads_addressed_var(original_form) ||
                operand_contains_load(original_form));
      }
    boolean killed_by_arbitrary_function_call(void)
      {
        return (operand_reads_addressed_var(original_form) ||
                operand_contains_load(original_form) ||
                operand_may_read_statically_allocated_var(original_form));
      }
    boolean killed_exiting_scope(base_symtab *scope)
      {
        return operand_uses_scope(the_expr.op(), scope);
      }
    boolean is_same_fact(propagated_fact *other_fact)
      {
        cse_fact *other_cse_fact = (cse_fact *)other_fact;
        if (!other_cse_fact->is_expr_fact())
            return FALSE;
        cse_expr_fact *other_expr_fact = (cse_expr_fact *)other_fact;
        return operands_are_same_expr(other_expr_fact->original_form,
                                      original_form);
      }
    void merge_reasons(propagated_fact *other_fact)
      {
        cse_expr_fact *other_expr_fact = (cse_expr_fact *)other_fact;
        operand_place_list_iter iter1(&(other_expr_fact->other_exprs));
        while (!iter1.is_empty())
          {
            operand_place op_place1 = iter1.step();
            operand_place_list_iter iter2(&other_exprs);
            boolean found = FALSE;
            while (!iter2.is_empty())
              {
                operand_place op_place2 = iter2.step();
                if (op_place1 == op_place2)
                  {
                    found = TRUE;
                    break;
                  }
              }

            if ((!found) && (op_place1 != the_expr))
                other_exprs.append(op_place1);
          }

        other_exprs.append(other_expr_fact->the_expr);
      }

    boolean is_expr_fact(void) { return TRUE; }
    void fix_expr(operand_place the_place, propagated_fact_list *fact_list);
  };

class cse_live_fact : public cse_fact
  {
private:
    var_sym *the_assigned_var;
    instruction_list the_assignments;

    cse_live_fact(var_sym *new_assigned_var)
      {
        the_assigned_var = new_assigned_var;
      }

public:
    cse_live_fact(instruction *new_assignment)
      {
        the_assigned_var = new_assignment->dst_op().symbol();
        the_assignments.append(new_assignment);
      }
    virtual ~cse_live_fact(void) { }

    propagated_fact *clone(void)
      {
        cse_live_fact *new_fact = new cse_live_fact(the_assigned_var);
        instruction_list_iter instruction_iter(assignments());
        while (!instruction_iter.is_empty())
          {
            instruction *this_instruction = instruction_iter.step();
            new_fact->the_assignments.append(this_instruction);
          }
        return new_fact;
      }
    boolean killed_by_var_write(var_sym *the_var)
      {
        return the_var->overlaps(the_assigned_var);
      }
    boolean killed_by_unknown_mem_write(void)
      {
        return the_assigned_var->is_addr_taken();
      }
    boolean killed_by_arbitrary_function_call(void)
      {
        return (the_assigned_var->is_addr_taken() ||
                !(the_assigned_var->is_auto()));
      }
    boolean killed_exiting_scope(base_symtab *scope)
      {
        return (the_assigned_var->parent() == scope);
      }
    boolean is_same_fact(propagated_fact *other_fact)
      {
        cse_fact *other_cse_fact = (cse_fact *)other_fact;
        if (other_cse_fact->is_expr_fact())
            return FALSE;
        cse_live_fact *other_live_fact = (cse_live_fact *)other_cse_fact;
        return other_live_fact->assigned_var() == assigned_var();
      }
    void merge_reasons(propagated_fact *other_fact)
      {
        cse_live_fact *other_live_fact = (cse_live_fact *)other_fact;
        instruction_list_iter iter1(other_live_fact->assignments());
        while (!iter1.is_empty())
          {
            instruction *instr1 = iter1.step();
            instruction_list_iter iter2(assignments());
            boolean found = FALSE;
            while (!iter2.is_empty())
              {
                instruction *instr2 = iter2.step();
                if (instr1 == instr2)
                  {
                    found = TRUE;
                    break;
                  }
              }

            if (!found)
                the_assignments.append(instr1);
          }
      }

    boolean is_expr_fact(void) { return FALSE; }
    var_sym *assigned_var(void) { return the_assigned_var; }
    instruction_list *assignments(void) { return &the_assignments; }
  };

class cse_fact_manager : public fact_manager
  {
private:
    void fix_expr(instruction *the_instr, propagated_fact_list *fact_list);

public:
    void initial_fact_creator(propagated_fact_list *) { }
    void instr_fact_creator(propagated_fact_list *the_facts,
                            instruction *the_instr);
    void bound_fact_creator(propagated_fact_list *the_facts,
                            tree_for *the_for);
    void step_fact_creator(propagated_fact_list *the_facts, tree_for *the_for);
    void index_fact_creator(propagated_fact_list *, tree_for *);
    void act_on_facts(instruction *the_instr, propagated_fact_list *fact_list);
    void act_on_for_facts(tree_for *the_for, propagated_fact_list *fact_list);
    boolean use_kill_summary(void) { return TRUE; }

    void try_all_facts(instruction *fact_expr, instruction *comp_expr);
  };

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static instruction_list *old_instructions = NULL;
static const char *k_porky_cse_var = NULL;
static const char *k_porky_cse_orig_expr = NULL;
static const char *k_porky_cse_reeval = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void node_save_orig(tree_node *the_node, void *);
static void node_remove_origs(tree_node *the_node, void *);
static void node_self_cse(tree_node *the_node, void *);
static void expr_save_orig(instruction *the_instr);
static void bind_children(instruction *the_instr, instruction *copy);
static operand peek_expr_orig(operand_place the_place);
static boolean this_value_live(var_sym *the_var, instruction *the_expr,
                               propagated_fact_list *fact_list,
                               operand_place_list *other_places);
static void force_var_assignment(var_sym *the_var, operand_place the_place);
static boolean exprs_dont_overlap(instruction *expr1, instruction *expr2);
static in_rrr *dummy_cpy(tree_node_list *node_list);
static void add_fact(propagated_fact_list *fact_list,
                     propagated_fact *new_fact);
static boolean cached_operand_reevaluation_ok(operand the_operand);
static boolean cached_instr_reevaluation_ok(instruction *the_instr);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void proc_cse(tree_proc *the_proc)
  {
    if (k_porky_cse_var == NULL)
      {
        ANNOTE(k_porky_cse_var,       "porky cse var",       FALSE);
        k_porky_cse_orig_expr = lexicon->enter("porky cse orig expr")->sp;
        ANNOTE(k_porky_cse_reeval,    "porky cse reeval",    FALSE);
      }

    to_copy_form_on_proc(the_proc);

    assert(old_instructions == NULL);
    old_instructions = new instruction_list;

    the_proc->map(&node_save_orig, NULL);
    the_proc->map(&node_self_cse, NULL);

    cse_fact_manager the_manager;
    apply_through_structured_control(the_proc->body(), &the_manager);

    the_proc->map(&node_remove_origs, NULL);

    while (!old_instructions->is_empty())
      {
        instruction *this_instruction = old_instructions->pop();
        delete this_instruction;
      }
    delete old_instructions;
    old_instructions = NULL;

    from_copy_form_on_proc(the_proc);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

boolean cse_expr_fact::fact_ok_for_node(tree_node *the_node)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *base_instr = the_tree_instr->instr();
            operand dst_op = base_instr->dst_op();
            if (dst_op.is_symbol() && killed_by_var_write(dst_op.symbol()))
                return FALSE;

            if ((base_instr->opcode() == io_str) ||
                (base_instr->opcode() == io_memcpy))
              {
                in_rrr *the_rrr = (in_rrr *)base_instr;
                operand dest_addr = the_rrr->dst_addr_op();
                sym_node *modified_sym =
                        operand_address_root_symbol(dest_addr);
                if (modified_sym != NULL)
                  {
                    if (modified_sym->is_var())
                      {
                        var_sym *modified_var = (var_sym *)modified_sym;
                        if (killed_by_var_write(modified_var))
                            return FALSE;
                      }
                  }
                else
                  {
                    if (killed_by_unknown_mem_write())
                        return FALSE;
                  }
              }

            return fact_ok_for_instr(base_instr);
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            return (fact_ok_for_op(the_for->lb_op()) &&
                    fact_ok_for_op(the_for->ub_op()) &&
                    fact_ok_for_op(the_for->step_op()));
          }
        default:
            assert(FALSE);
            return FALSE;
      }
  }

boolean cse_expr_fact::fact_ok_for_instr(instruction *the_instr)
  {
    if (instr_is_impure_call(the_instr))
      {
        if (killed_by_arbitrary_function_call())
            return FALSE;
        in_cal *the_call = (in_cal *)the_instr;
        unsigned num_args = the_call->num_args();
        for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
          {
            operand this_arg = the_call->argument(arg_num);
            if (this_arg.type()->unqual()->is_ptr())
              {
                sym_node *modified_sym = operand_address_root_symbol(this_arg);
                if (modified_sym != NULL)
                  {
                    if (modified_sym->is_var())
                      {
                        var_sym *modified_var = (var_sym *)modified_sym;
                        if (killed_by_var_write(modified_var))
                            return FALSE;
                      }
                  }
                else
                  {
                    if (killed_by_unknown_mem_write())
                        return FALSE;
                  }
              }
          }
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        if (!fact_ok_for_op(the_instr->src_op(src_num)))
            return FALSE;
      }

    return TRUE;
  }

boolean cse_expr_fact::fact_ok_for_op(operand the_op)
  {
    if (the_op.is_expr())
        return fact_ok_for_instr(the_op.instr());
    else
        return TRUE;
  }

cse_expr_fact::cse_expr_fact(operand_place new_place)
  {
    the_expr = new_place;
    original_form = peek_expr_orig(new_place);
  }

void cse_expr_fact::fix_expr(operand_place the_place,
                             propagated_fact_list *fact_list)
  {
    if (the_expr.parent->owner() == NULL)
      {
        /*
         *  In this case, this expression has already been replaced
         *  and is on the old_instructions list, so just ignore it.
         *  The expression that replaced it should be around, so it
         *  will be used for anything this could have been used for.
         */
        return;
      }

    if (the_place == the_expr)
        return;

    if (reuse_desired(the_place.op()) &&
        operands_are_same_expr(peek_expr_orig(the_place), original_form) &&
        exprs_dont_overlap(the_place.parent, the_expr.parent))
      {
        var_sym *the_var = NULL;

        base_symtab *new_scope = the_place.parent->owner()->scope();
        new_scope =
                common_symtab(new_scope, the_expr.parent->owner()->scope());
        operand_place_list_iter other_place_iter(&other_exprs);
        while (!other_place_iter.is_empty())
          {
            operand_place this_other_place = other_place_iter.step();
            tree_node *other_node = this_other_place.parent->owner();
            new_scope = common_symtab(new_scope, other_node->scope());
          }

        if ((the_expr.parent->opcode() == io_cpy) &&
            the_expr.parent->dst_op().is_symbol())
          {
            the_var = the_expr.parent->dst_op().symbol();

            if (the_var->peek_annote(k_porky_cse_var) != NULL)
              {
                if (!new_scope->is_ancestor(the_var->parent()))
                  {
                    the_var->parent()->remove_sym(the_var);
                    new_scope->add_sym(the_var);
                  }

                operand_place_list_iter other_iter(&other_exprs);
                while (!other_iter.is_empty())
                  {
                    operand_place this_other_place = other_iter.step();
                    force_var_assignment(the_var, this_other_place);
                  }
              }
            else
              {
                if ((!new_scope->is_ancestor(the_var->parent())) ||
                    (!this_value_live(the_var, the_expr.parent, fact_list,
                                      &other_exprs)))
                  {
                    the_var = NULL;
                  }
              }
          }

        if (the_var == NULL)
          {
            the_var =
                    new_scope->new_unique_var(the_expr.op().type()->unqual());
            the_var->append_annote(k_porky_cse_var, new immed_list);
            force_var_assignment(the_var, the_expr);
            operand_place_list_iter other_iter(&other_exprs);
            while (!other_iter.is_empty())
              {
                operand_place this_other_place = other_iter.step();
                force_var_assignment(the_var, this_other_place);
              }
          }

        operand old_op = the_place.op();
        old_op.remove();
        the_place.parent->set_src_op(the_place.src_num, operand(the_var));
        if (old_op.is_expr())
            old_instructions->append(old_op.instr());

        return;
      }
  }


void cse_fact_manager::fix_expr(instruction *the_instr,
                                propagated_fact_list *fact_list)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand_place the_place(the_instr, src_num);
        operand this_op = the_place.op();
        propagated_fact_list_iter fact_iter(fact_list);
        boolean done = FALSE;
        while (!fact_iter.is_empty())
          {
            propagated_fact *this_fact = fact_iter.step();
            cse_fact *this_cse_fact = (cse_fact *)this_fact;
            if (this_cse_fact->is_expr_fact())
              {
                cse_expr_fact *this_expr_fact = (cse_expr_fact *)this_cse_fact;
                if ((this_expr_fact->the_expr.parent->owner() != NULL) &&
                    operands_are_same_expr(peek_expr_orig(the_place),
                                           this_expr_fact->original_form))
                  {
                    this_expr_fact->fix_expr(the_place, fact_list);
                    done = TRUE;
                    break;
                  }
              }
          }
        if ((!done) && this_op.is_expr())
            fix_expr(this_op.instr(), fact_list);
      }
  }

void cse_fact_manager::try_all_facts(instruction *fact_expr,
                                     instruction *comp_expr)
  {
    if (fact_expr->opcode() == io_ldc)
        return;

    propagated_fact_list facts;
    fix_expr(comp_expr, &facts);
    while (!facts.is_empty())
      {
        propagated_fact *this_fact = facts.pop();
        delete this_fact;
      }

    unsigned num_srcs = fact_expr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        facts.append(new cse_expr_fact(operand_place(fact_expr, src_num)));
        operand this_op = fact_expr->src_op(src_num);
        if (this_op.is_expr())
            try_all_facts(this_op.instr(), comp_expr);
      }
  }

void cse_fact_manager::instr_fact_creator(propagated_fact_list *the_facts,
                                          instruction *the_instr)
  {
    if (the_instr->opcode() == io_mrk)
        return;

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (cached_operand_reevaluation_ok(this_op) &&
            ((!cse_no_pointers) || (!this_op.type()->unqual()->is_ptr())))
          {
            cse_expr_fact *new_fact =
                    new cse_expr_fact(operand_place(the_instr, src_num));
            if (new_fact->fact_ok_for_node(the_instr->owner()))
                add_fact(the_facts, new_fact);
            else
                delete new_fact;
          }
        if (this_op.is_expr())
            instr_fact_creator(the_facts, this_op.instr());
      }

    if (!the_instr->dst_op().is_symbol())
        return;
    var_sym *dest_var = the_instr->dst_op().symbol();
    if (dest_var->type()->is_volatile())
        return;
    if (cse_no_pointers && dest_var->type()->unqual()->is_ptr())
        return;

    add_fact(the_facts, new cse_live_fact(the_instr));
  }

void cse_fact_manager::bound_fact_creator(propagated_fact_list *the_facts,
                                          tree_for *the_for)
  {
    instr_fact_creator(the_facts, dummy_cpy(the_for->lb_list()));
    instr_fact_creator(the_facts, dummy_cpy(the_for->ub_list()));
  }

void cse_fact_manager::step_fact_creator(propagated_fact_list *the_facts,
                                         tree_for *the_for)
  {
    instr_fact_creator(the_facts, dummy_cpy(the_for->step_list()));
  }

void cse_fact_manager::index_fact_creator(propagated_fact_list *, tree_for *)
  {
    return;
  }

void cse_fact_manager::act_on_facts(instruction *the_instr,
                                    propagated_fact_list *fact_list)
  {
    fix_expr(the_instr, fact_list);
  }

void cse_fact_manager::act_on_for_facts(tree_for *the_for,
                                        propagated_fact_list *fact_list)
  {
    fix_expr(dummy_cpy(the_for->lb_list()), fact_list);
    fix_expr(dummy_cpy(the_for->ub_list()), fact_list);
    fix_expr(dummy_cpy(the_for->step_list()), fact_list);
  }


static void node_save_orig(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();
        expr_save_orig(the_instr);
      }
  }

static void node_remove_origs(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();
        annote *old_annote =
                the_instr->annotes()->get_annote(k_porky_cse_orig_expr);
        assert(old_annote != NULL);
        instruction *old_instr = (instruction *)(old_annote->data());
        delete old_annote;
        delete old_instr;
      }
  }

static void node_self_cse(tree_node *the_node, void *)
  {
    cse_fact_manager the_manager;

    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();
        if (the_instr->opcode() == io_lab)
            return;
        the_manager.try_all_facts(the_instr, the_instr);
      }
    else if (the_node->is_for())
      {
        tree_for *the_for = (tree_for *)the_node;

        in_rrr *lb_cpy = dummy_cpy(the_for->lb_list());
        in_rrr *ub_cpy = dummy_cpy(the_for->ub_list());
        in_rrr *step_cpy = dummy_cpy(the_for->step_list());

        the_manager.try_all_facts(lb_cpy, lb_cpy);
        the_manager.try_all_facts(lb_cpy, ub_cpy);
        the_manager.try_all_facts(lb_cpy, step_cpy);

        the_manager.try_all_facts(ub_cpy, lb_cpy);
        the_manager.try_all_facts(ub_cpy, ub_cpy);
        the_manager.try_all_facts(ub_cpy, step_cpy);

        the_manager.try_all_facts(step_cpy, lb_cpy);
        the_manager.try_all_facts(step_cpy, ub_cpy);
        the_manager.try_all_facts(step_cpy, step_cpy);
      }
  }

static void expr_save_orig(instruction *the_instr)
  {
    instruction *clone = the_instr->clone();
    the_instr->append_annote(k_porky_cse_orig_expr, clone);
    bind_children(the_instr, clone);
  }

static void bind_children(instruction *the_instr, instruction *copy)
  {
    unsigned num_srcs = the_instr->num_srcs();
    assert(copy->num_srcs() == num_srcs);
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_expr())
          {
            instruction *this_child = this_op.instr();
            operand copy_op = copy->src_op(src_num);
            assert(copy_op.is_expr());
            instruction *copy_child = copy_op.instr();
            this_child->append_annote(k_porky_cse_orig_expr, copy_child);
            bind_children(this_child, copy_child);
          }
      }
  }

static operand peek_expr_orig(operand_place the_place)
  {
    annote *original_expr_annote =
            the_place.parent->annotes()->peek_annote(k_porky_cse_orig_expr);
    assert(original_expr_annote != NULL);
    instruction *orig_parent = (instruction *)(original_expr_annote->data());
    return orig_parent->src_op(the_place.src_num);
  }

static boolean this_value_live(var_sym *the_var, instruction *the_expr,
                               propagated_fact_list *fact_list,
                               operand_place_list *other_places)
  {
    propagated_fact_list_iter fact_iter(fact_list);
    while (!fact_iter.is_empty())
      {
        propagated_fact *this_fact = fact_iter.step();
        cse_fact *this_cse_fact = (cse_fact *)this_fact;
        if (!this_cse_fact->is_expr_fact())
          {
            cse_live_fact *this_live_fact = (cse_live_fact *)this_fact;
            if (this_live_fact->assigned_var() == the_var)
              {
                instruction_list *assignments = this_live_fact->assignments();
                instruction_list_iter instr_iter(assignments);
                while (!instr_iter.is_empty())
                  {
                    instruction *this_instr = instr_iter.step();
                    if (this_instr != the_expr)
                      {
                        boolean found = FALSE;
                        operand_place_list_iter other_iter(other_places);
                        while (!other_iter.is_empty())
                          {
                            operand_place list_place = other_iter.step();
                            if (this_instr == list_place.parent)
                              {
                                found = TRUE;
                                break;
                              }
                          }
                        if (!found)
                            return FALSE;
                      }
                  }
                return TRUE;
              }
          }
      }

    return FALSE;
  }

static void force_var_assignment(var_sym *the_var, operand_place the_place)
  {
    operand old_op = the_place.op();
    if (old_op == operand(the_var))
        return;

    tree_node *owner = the_place.parent->owner();
    assert(owner != NULL);
    old_op.remove();
    the_place.parent->set_src_op(the_place.src_num, operand(the_var));
    in_rrr *new_cpy =
            new in_rrr(io_cpy, the_var->type()->unqual(), operand(the_var),
                       old_op);
    tree_node *new_node = new tree_instr(new_cpy);
    owner->parent()->insert_before(new_node, owner->list_e());
    expr_save_orig(new_cpy);
    node_self_cse(new_node, NULL);
  }

static boolean exprs_dont_overlap(instruction *expr1, instruction *expr2)
  {
    if (expr1 == expr2)
        return FALSE;

    instruction *follow = expr1;
    while (TRUE)
      {
        if (!follow->dst_op().is_instr())
            break;
        follow = follow->dst_op().instr();
        if (follow == expr2)
            return FALSE;
      }

    follow = expr2;
    while (TRUE)
      {
        if (!follow->dst_op().is_instr())
            break;
        follow = follow->dst_op().instr();
        if (follow == expr1)
            return FALSE;
      }

    return TRUE;
  }

static in_rrr *dummy_cpy(tree_node_list *node_list)
  {
    tree_node_list_e *head_e = node_list->head();
    assert(head_e != NULL);
    tree_node *head_node = head_e->contents;
    assert(head_node->is_instr());
    tree_instr *head_tree_instr = (tree_instr *)head_node;
    instruction *head_instr = head_tree_instr->instr();
    assert(head_instr->opcode() == io_cpy);
    return (in_rrr *)head_instr;
  }

static void add_fact(propagated_fact_list *fact_list,
                     propagated_fact *new_fact)
  {
    propagated_fact_list_iter fact_iter(fact_list);
    while (!fact_iter.is_empty())
      {
        propagated_fact *old_fact = fact_iter.step();
        if (old_fact->is_same_fact(new_fact))
          {
            delete new_fact;
            return;
          }
      }
    fact_list->append(new_fact);
  }

static boolean cached_operand_reevaluation_ok(operand the_operand)
  {
    if (the_operand.is_symbol())
        return (!the_operand.symbol()->type()->is_volatile());
    else if (the_operand.is_expr())
        return cached_instr_reevaluation_ok(the_operand.instr());
    else
        return TRUE;
  }

static boolean cached_instr_reevaluation_ok(instruction *the_instr)
  {
    if (instr_is_impure_call(the_instr))
        return FALSE;

    annote *reeval_annote =
            the_instr->annotes()->peek_annote(k_porky_cse_reeval);
    if (reeval_annote != NULL)
      {
        immed_list *immeds = reeval_annote->immeds();
        assert(immeds->count() == 1);
        immed value = immeds->head()->contents;
        assert(value.is_int_const());
        if (value == immed(FALSE))
          {
            return FALSE;
          }
        else
          {
            assert(value == immed(TRUE));
            return TRUE;
          }
      }

    boolean result = TRUE;

    switch (the_instr->opcode())
      {
        case io_lod:
          {
            in_rrr *the_load = (in_rrr *)the_instr;
            type_node *src_addr_type = the_load->src_addr_op().type();
            assert(src_addr_type->is_ptr());
            ptr_type *src_ptr = (ptr_type *)src_addr_type;
            if (src_ptr->ref_type()->is_volatile())
                result = FALSE;
            break;
          }
        case io_gen:
            result = FALSE;
            break;
        default:
            break;
      }

    if (result == TRUE)
      {
        unsigned num_srcs = the_instr->num_srcs();
        for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
          {
            if (!operand_reevaluation_ok(the_instr->src_op(src_num)))
              {
                result = FALSE;
                break;
              }
          }
      }

    the_instr->append_annote(k_porky_cse_reeval,
                             new immed_list(immed(result)));
    return result;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
