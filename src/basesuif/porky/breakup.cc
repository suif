/* file "breakup.cc" */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to break up large expression trees for the porky program for
 * SUIF */


#define RCS_BASE_FILE breakup_cc

#include "porky.h"

RCS_BASE(
    "$Id: breakup.cc,v 1.2 1999/08/25 03:27:24 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void breakup_on_object(suif_object *the_object, so_walker *the_walker);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void breakup_on_proc(tree_proc *the_proc, i_integer limit_size)
  {
    so_walker the_walker;
    the_walker.set_data(0, &limit_size);
    the_walker.walk(the_proc, &breakup_on_object);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void breakup_on_object(suif_object *the_object, so_walker *the_walker)
  {
    if (!the_object->is_tree_obj())
        return;
    tree_node *the_node = (tree_node *)the_object;
    if (!the_node->is_instr())
        return;
    tree_instr *the_tree_instr = (tree_instr *)the_node;
    i_integer *limit_size = (i_integer *)(the_walker->get_data(0).ptr);
    breakup_large_expressions(the_tree_instr->instr(), *limit_size);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
