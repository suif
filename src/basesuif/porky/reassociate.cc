/* file "reassociate.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE reassociate_cc

#include "porky.h"

RCS_BASE(
    "$Id: reassociate.cc,v 1.2 1999/08/25 03:27:34 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void factor_wrt(operand the_operand, var_sym *the_var,
                       operand *part_vo, operand *part_vi, int *part_ko,
                       int *part_ki, boolean *bad);
static void cannonicize_operand_tree(operand *the_operand);
static boolean recursive_cannonicize(instruction *, operand *the_operand,
                                     void *);
static operand reassociate_operand(operand original_operand, var_sym *the_var,
                                   boolean *bad);
static tree_for *nearest_enclosing_for(tree_node *the_node);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void try_reassociation(operand the_operand)
  {
    if (the_operand.kind() != OPER_INSTR)
        return;

    instruction *original_instruction = the_operand.instr();
    tree_node *owner = original_instruction->owner();

    tree_for *nearest_for = nearest_enclosing_for(owner);
    if (nearest_for == NULL)
        return;

    var_sym *the_var = nearest_for->index();

    instruction *placeholder =
        new in_rrr(io_cpy, the_operand.type()->unqual(), operand(), operand(),
                   operand());

    replace_instruction(original_instruction, placeholder);

    boolean bad = FALSE;
    operand new_operand = reassociate_operand(the_operand, the_var, &bad);

    replace_instruction_with_operand(placeholder, new_operand);

    if (bad)
        return;

    if (verbosity_level > 1)
        printf("reassociation\n");
  }

/*
 *  Try to find operands *op_a and *op_b such that
 *
 *      <the_operand> == <*op_a> + <*op_b> * <var_x>
 *
 *  holds and neither *op_a nor *op_b uses var_x.  If such operands
 *  can be found, return TRUE and set *op_a and *op_b to those values.
 *  Otherwise, return FALSE and don't touch *op_a and *op_b.
 */
extern boolean linear_form(operand the_operand, var_sym *var_x, operand *op_a,
                           operand *op_b)
  {
    operand part_vo, part_vi;
    int part_ko, part_ki;
    boolean is_bad = FALSE;

    factor_wrt(the_operand, var_x, &part_vo, &part_vi, &part_ko, &part_ki,
               &is_bad);
    if (is_bad)
        return FALSE;

    *op_a = operand_add_const(part_vo.type()->unqual(), part_vo, part_ko);
    *op_b = operand_add_const(part_vi.type()->unqual(), part_vi, part_ki);

    return TRUE;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

/*
 *  Factor with respect to the_var.  That is, factor it into the form
 *
 *      (part_vo + part_ko) + (part_vi + part_ki) * the_var
 *
 *  where part_vo and part_vi are variable expressions not including the_var
 *  and part_ko and part_ki are integer constants.
 *
 *  If this can't be done, set *bad = TRUE.
 *
 *  The original operand, the_operand is unchanged in any case.
 */
static void factor_wrt(operand the_operand, var_sym *the_var,
                       operand *part_vo, operand *part_vi, int *part_ko,
                       int *part_ki, boolean *bad)
  {
    if (*bad)
        return;

    switch (the_operand.kind())
      {
        case OPER_NULL:
            assert(FALSE);
        case OPER_SYM:
          {
            var_sym *var_used = the_operand.symbol();

            *part_vi = operand_int(the_operand.type()->unqual(), 0);
            *part_ko = 0;
            if (var_used == the_var)
              {
                *part_vo = operand_int(the_operand.type()->unqual(), 0);
                *part_ki = 1;
              }
            else
              {
                *part_vo = operand(var_used);
                *part_ki = 0;
              }
            break;
          }
        case OPER_INSTR:
          {
            instruction *instr = the_operand.instr();
            switch (instr->format())
              {
                case inf_ldc:
                  {
                    *part_vi = operand_int(the_operand.type()->unqual(), 0);
                    *part_ki = 0;

                    in_ldc *the_ldc = (in_ldc *)instr;
                    immed the_value = the_ldc->value();
                    if (the_value.kind() == im_int)
                      {
                        *part_vo =
                                operand_int(the_operand.type()->unqual(), 0);
                        *part_ko = the_value.integer();
                      }
                    else
                      {
                        operand empty_operand;
                        immed the_value = the_ldc->value();
                        in_ldc *new_ldc = new in_ldc(the_ldc->result_type(),
                                                     empty_operand, the_value);
                        *part_vo = operand(new_ldc);
                        *part_ko = 0;
                      }
                    break;
                  }
                case inf_rrr:
                  {
                    in_rrr *the_rrr = (in_rrr *)instr;
                    type_node *the_type = the_rrr->result_type();
                    if_ops opcode = instr->opcode();

                    switch (opcode)
                      {
                        case io_cpy:
                          {
                            operand new_operand = the_rrr->src1_op();
                            factor_wrt(new_operand, the_var, part_vo, part_vi,
                                       part_ko, part_ki, bad);
                            return;
                          }
                        case io_add:
                        case io_sub:
                          {
                            operand source_1 = the_rrr->src1_op();
                            operand source_2 = the_rrr->src2_op();
                            operand part_vo_1, part_vo_2;
                            operand part_vi_1, part_vi_2;
                            int part_ko_1, part_ko_2;
                            int part_ki_1, part_ki_2;

                            factor_wrt(source_1, the_var, &part_vo_1,
                                       &part_vi_1, &part_ko_1, &part_ki_1,
                                       bad);
                            if (*bad)
                                return;

                            factor_wrt(source_2, the_var, &part_vo_2,
                                       &part_vi_2, &part_ko_2, &part_ki_2,
                                       bad);
                            if (*bad)
                              {
                                if (part_vo_1.kind() == OPER_INSTR)
                                    delete part_vo_1.instr();
                                if (part_vi_1.kind() == OPER_INSTR)
                                    delete part_vi_1.instr();
                                return;
                              }

                            boolean ok;

                            ok = try_constant_operation(the_type, opcode,
                                                        part_ko_1, part_ko_2,
                                                        part_ko);
                            if (ok)
                              {
                                ok = try_constant_operation(the_type, opcode,
                                                            part_ki_1,
                                                            part_ki_2,
                                                            part_ki);
                              }

                            if (ok)
                              {
                                if (opcode == io_add)
                                  {
                                    *part_vo = operand_add(the_type, part_vo_1,
                                                           part_vo_2);
                                    *part_vi = operand_add(the_type, part_vi_1,
                                                           part_vi_2);
                                  }
                                else
                                  {
                                    *part_vo = operand_sub(the_type, part_vo_1,
                                                           part_vo_2);
                                    *part_vi = operand_sub(the_type, part_vi_1,
                                                           part_vi_2);
                                  }
                              }
                            else
                              {
                                if (part_vo_1.kind() == OPER_INSTR)
                                    delete part_vo_1.instr();
                                if (part_vo_2.kind() == OPER_INSTR)
                                    delete part_vo_2.instr();
                                if (part_vi_1.kind() == OPER_INSTR)
                                    delete part_vi_1.instr();
                                if (part_vi_2.kind() == OPER_INSTR)
                                    delete part_vi_2.instr();
                                *bad = TRUE;
                              }

                            return;
                          }
                        case io_neg:
                          {
                            operand source_1 = the_rrr->src1_op();
                            operand part_vo_1;
                            operand part_vi_1;
                            int part_ko_1;
                            int part_ki_1;

                            factor_wrt(source_1, the_var, &part_vo_1,
                                       &part_vi_1, &part_ko_1, &part_ki_1,
                                       bad);
                            if (*bad)
                                return;

                            boolean ok;

                            ok = try_constant_operation(the_type, opcode,
                                                        part_ko_1, 0, part_ko);
                            if (ok)
                              {
                                ok = try_constant_operation(the_type, opcode,
                                                            part_ki_1, 0,
                                                            part_ki);
                              }

                            if (ok)
                              {
                                *part_vo = operand_neg(the_type, part_vo_1);
                                *part_vi = operand_neg(the_type, part_vi_1);
                              }
                            else
                              {
                                if (part_vo_1.kind() == OPER_INSTR)
                                    delete part_vo_1.instr();
                                if (part_vi_1.kind() == OPER_INSTR)
                                    delete part_vi_1.instr();
                                *bad = TRUE;
                              }

                            return;
                          }
                        case io_mul:
                          {
                            operand source_1 = the_rrr->src1_op();
                            operand source_2 = the_rrr->src2_op();
                            operand part_vo_1, part_vo_2;
                            operand part_vi_1, part_vi_2;
                            int part_ko_1, part_ko_2;
                            int part_ki_1, part_ki_2;

                            factor_wrt(source_1, the_var, &part_vo_1,
                                       &part_vi_1, &part_ko_1, &part_ki_1,
                                       bad);
                            if (*bad)
                                return;

                            factor_wrt(source_2, the_var, &part_vo_2,
                                       &part_vi_2, &part_ko_2, &part_ki_2,
                                       bad);
                            if (*bad)
                              {
                                if (part_vo_1.kind() == OPER_INSTR)
                                  {
                                    delete part_vo_1.instr();
                                    part_vo_1.set_null();
                                  }
                                if (part_vi_1.kind() == OPER_INSTR)
                                  {
                                    delete part_vi_1.instr();
                                    part_vi_1.set_null();
                                  }
                                return;
                              }

                            part_vi_1 = fold_constants(part_vi_1);
                            part_vi_2 = fold_constants(part_vi_2);
                            boolean vi_1_zero = operand_is_zero(part_vi_1);
                            boolean vi_2_zero = operand_is_zero(part_vi_2);
                            boolean ki_1_zero = (part_ki_1 == 0);
                            boolean ki_2_zero = (part_ki_2 == 0);

                            boolean ok;

                            ok = ((vi_1_zero && ki_1_zero) ||
                                  (vi_2_zero && ki_2_zero));

                            if (ok)
                              {
                                if (vi_1_zero && ki_1_zero)
                                  {
                                    operand temp_op = part_vi_1;
                                    part_vi_1 = part_vi_2;
                                    part_vi_2 = temp_op;

                                    temp_op = part_vo_1;
                                    part_vo_1 = part_vo_2;
                                    part_vo_2 = temp_op;

                                    int temp_int = part_ki_1;
                                    part_ki_1 = part_ki_2;
                                    part_ki_2 = temp_int;

                                    temp_int = part_ko_1;
                                    part_ko_1 = part_ko_2;
                                    part_ko_2 = temp_int;
                                  }
                              }

                            if (ok)
                              {
                                ok = try_constant_operation(the_type, io_mul,
                                                            part_ko_1,
                                                            part_ko_2,
                                                            part_ko);
                              }

                            if (ok)
                              {
                                ok = try_constant_operation(the_type, io_mul,
                                                            part_ki_1,
                                                            part_ko_2,
                                                            part_ki);
                              }

                            if (ok)
                              {
                                ok = (operand_reevaluation_ok(part_vo_1) &&
                                      operand_reevaluation_ok(part_vo_2) &&
                                      operand_reevaluation_ok(part_vi_1));
                              }

                            if (ok)
                              {
                                operand product;
                                operand sum;

                                sum = operand_multiply(the_type,
                                                       part_vo_1.clone(),
                                                       part_vo_2.clone());
                                product = operand_mul_by_const(the_type,
                                                               part_vo_1,
                                                               part_ko_2);
                                sum = operand_add(the_type, sum, product);
                                product =
                                        operand_mul_by_const(the_type,
                                                             part_vo_2.clone(),
                                                             part_ko_1);
                                *part_vo = operand_add(the_type, sum, product);

                                sum = operand_multiply(the_type,
                                                       part_vi_1.clone(),
                                                       part_vo_2.clone());
                                product = operand_mul_by_const(the_type,
                                                               part_vi_1,
                                                               part_ko_2);
                                sum = operand_add(the_type, sum, product);
                                product = operand_mul_by_const(the_type,
                                                               part_vo_2,
                                                               part_ki_1);
                                *part_vi = operand_add(the_type, sum, product);

                                if (part_vi_2.kind() == OPER_INSTR)
                                    delete part_vi_2.instr();
                              }
                            else
                              {
                                if (part_vo_1.kind() == OPER_INSTR)
                                    delete part_vo_1.instr();
                                if (part_vo_2.kind() == OPER_INSTR)
                                    delete part_vo_2.instr();
                                if (part_vi_1.kind() == OPER_INSTR)
                                    delete part_vi_1.instr();
                                if (part_vi_2.kind() == OPER_INSTR)
                                    delete part_vi_2.instr();
                                *bad = TRUE;
                              }

                            return;
                          }
                        default:
                            break;
                      }

                    /* fall through */
                  }
                case inf_cal:
                case inf_array:
                case inf_gen:
                  {
                    if (operand_references_var(the_operand, the_var))
                      {
                        *bad = TRUE;
                        break;
                      }

                    *part_vo = the_operand.clone();
                    *part_vi = operand_int(the_operand.type()->unqual(), 0);
                    *part_ko = 0;
                    *part_ki = 0;
                    break;
                  }
                default:
                    assert(FALSE);
              }
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void cannonicize_operand_tree(operand *the_operand)
  {
    (void)recursive_cannonicize(NULL, the_operand, NULL);
  }

/*
 *  This is in here because it was in the old suif expander.  It uses
 *  commutivity of addition and multiplication to put constants on the right
 *  for addition and on the left for multiplication.
 *
 *  Presumably this is done because some later pass wants this format for
 *  dismantled array references.
 */
static boolean recursive_cannonicize(instruction *, operand *the_operand,
                                     void *)
  {
    if (the_operand->kind() != OPER_INSTR)
        return FALSE;

    instruction *instr = the_operand->instr();

    instr->src_map(&recursive_cannonicize, NULL);

    if_ops opcode = instr->opcode();

    if ((opcode == io_add) || (opcode == io_mul))
      {
        in_rrr *the_rrr = (in_rrr *)instr;
        operand source_1 = the_rrr->src1_op();
        source_1.remove();
        operand source_2 = the_rrr->src2_op();
        source_2.remove();

        source_1 = fold_constants(source_1);
        source_2 = fold_constants(source_2);

        if (((opcode == io_add) && operand_is_int_const(source_1)) ||
            ((opcode == io_mul) && operand_is_int_const(source_2)))
          {
            the_rrr->set_src1(source_2);
            the_rrr->set_src2(source_1);
          }
        else
          {
            the_rrr->set_src1(source_1);
            the_rrr->set_src2(source_2);
          }
      }
    return FALSE;
  }

static operand reassociate_operand(operand original_operand, var_sym *the_var,
                                   boolean *bad)
  {
    operand part_vo;
    operand part_vi;
    int part_ko;
    int part_ki;
    operand the_operand = original_operand;
    type_node *original_type = original_operand.type()->unqual();

    factor_wrt(the_operand, the_var, &part_vo, &part_vi, &part_ko, &part_ki,
               bad);
    if (*bad)
        return the_operand;

    if (the_operand.kind() == OPER_INSTR)
        delete the_operand.instr();

    cannonicize_operand_tree(&part_vo);
    cannonicize_operand_tree(&part_vi);

    operand sum_1;
    operand sum_2;

    sum_1 = operand_add_const(part_vo.type()->unqual(), part_vo, part_ko);
    sum_2 = operand_add_const(part_vi.type()->unqual(), part_vi, part_ki);
    operand var_operand(the_var);
    sum_2 = operand_multiply(var_operand.type()->unqual(), sum_2, var_operand);
    the_operand = operand_add(original_type, sum_1, sum_2);

    return the_operand;
  }

static tree_for *nearest_enclosing_for(tree_node *the_node)
  {
    tree_node *follow_node;
    tree_node_list *node_list;

    follow_node = the_node;
    while (TRUE)
      {
        node_list = follow_node->parent();
        if (node_list == NULL)
            return NULL;

        follow_node = node_list->parent();
        if (follow_node == NULL)
            return NULL;

        if (follow_node->kind() == TREE_FOR)
            return (tree_for *)follow_node;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
