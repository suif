/* file "dead_code.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* Dead code elimination for the porky program for SUIF */

#define RCS_BASE_FILE dead_code_cc

#include "fact.h"
#include "structured_facts.h"
#include "porky.h"

RCS_BASE(
    "$Id: dead_code.cc,v 1.2 1999/08/25 03:27:26 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

enum source_kinds
  {
    SK_INITIAL, SK_DEST_WRITE, SK_CALL_OR_GEN, SK_MEM_WRITE, SK_FOR_INDEX
  };

class dce_def_record;
class dce_use_record;
class dce_uses_record;

class dce_def
  {
private:
    var_sym *the_var;
    source_kinds the_kind;
    instruction *the_instr;
    tree_for *current_for;

public:
    dce_def(var_sym *initial_var)
      {
        assert(initial_var != NULL);
        the_var = initial_var;
        the_kind = SK_INITIAL;
        the_instr = NULL;
        current_for = NULL;
      }
    dce_def(var_sym *initial_var, source_kinds initial_kind)
      {
        assert(initial_var != NULL);
        assert(initial_kind == SK_CALL_OR_GEN);
        the_var = initial_var;
        the_kind = initial_kind;
        the_instr = NULL;
        current_for = NULL;
      }
    dce_def(var_sym *initial_var, source_kinds initial_kind,
            instruction *initial_instr)
      {
        assert(initial_var != NULL);
        assert((initial_kind != SK_INITIAL) &&
               (initial_kind != SK_CALL_OR_GEN) &&
               (initial_kind != SK_FOR_INDEX));
        the_var = initial_var;
        the_kind = initial_kind;
        the_instr = initial_instr;
        current_for = NULL;
      }
    dce_def(tree_for *initial_for)
      {
        assert(initial_for != NULL);
        the_var = initial_for->index();
        the_kind = SK_FOR_INDEX;
        the_instr = NULL;
        current_for = initial_for;
      }
    dce_def(const dce_def *initial_def)
      {
        the_var = initial_def->the_var;
        the_kind = initial_def->the_kind;
        the_instr = initial_def->the_instr;
        current_for = initial_def->current_for;
      }
    virtual ~dce_def(void) { }

    var_sym *var() { return the_var; }
    source_kinds kind() { return the_kind; }
    instruction *instr()
      {
        assert((the_kind != SK_INITIAL) && (the_kind != SK_FOR_INDEX));
        return the_instr;
      }
    tree_for *the_for()
      {
        assert(the_kind == SK_FOR_INDEX);
        return current_for;
      }

    boolean is_same(dce_def *other_def)
      {
        if (var() != other_def->var())
            return FALSE;
        if (kind() != other_def->kind())
            return FALSE;

        switch (kind())
          {
            case SK_INITIAL:
            case SK_CALL_OR_GEN:
                return TRUE;
            case SK_DEST_WRITE:
            case SK_MEM_WRITE:
                return (instr() == other_def->instr());
            case SK_FOR_INDEX:
                return (the_for() == other_def->the_for());
            default:
                assert(FALSE);
                return FALSE;
          }
      }
  };

class impossible_source_fact : public propagated_fact
  {
private:
    dce_def the_def;

public:
    impossible_source_fact(var_sym *initial_var) : the_def(initial_var) { }
    impossible_source_fact(var_sym *initial_var, source_kinds initial_kind,
                           instruction *initial_instr) :
            the_def(initial_var, initial_kind, initial_instr) { }
    impossible_source_fact(tree_for *initial_for) : the_def(initial_for) { }
    impossible_source_fact(const dce_def *initial_def) : the_def(initial_def)
      {
      }
    virtual ~impossible_source_fact(void) { }

    propagated_fact *clone(void)
      {
        return new impossible_source_fact(def());
      }
    boolean killed_by_instr(instruction *this_instr)
      {
        switch (kind())
          {
            case SK_INITIAL:
                return FALSE;
            case SK_DEST_WRITE:
                return (this_instr == instr());
            case SK_CALL_OR_GEN:
                if ((this_instr->opcode() == io_cal) ||
                    (this_instr->opcode() == io_gen))
                  {
                    return TRUE;
                  }
                break;
            case SK_MEM_WRITE:
                return (this_instr == instr());
            case SK_FOR_INDEX:
                return FALSE;
            default:
                assert(FALSE);
                return FALSE;
          }

        unsigned num_srcs = this_instr->num_srcs();
        for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
          {
            operand this_op = this_instr->src_op(src_num);
            if (this_op.is_expr())
              {
                if (killed_by_instr(this_op.instr()))
                    return TRUE;
              }
          }

        return FALSE;
      }
    boolean killed_by_for_bounds(tree_for *this_for)
      {
        switch (kind())
          {
            case SK_INITIAL:
                return FALSE;
            case SK_DEST_WRITE:
                return FALSE;
            case SK_CALL_OR_GEN:
                break;
            case SK_MEM_WRITE:
                return FALSE;
            case SK_FOR_INDEX:
                return (the_for() == this_for);
            default:
                assert(FALSE);
                return FALSE;
          }

        if (this_for->ub_op().is_expr())
          {
            if (killed_by_instr(this_for->ub_op().instr()))
                return TRUE;
          }

        if (this_for->lb_op().is_expr())
          {
            if (killed_by_instr(this_for->lb_op().instr()))
                return TRUE;
          }

        if (this_for->step_op().is_expr())
          {
            if (killed_by_instr(this_for->step_op().instr()))
                return TRUE;
          }

        return FALSE;
      }
    boolean killed_by_for_step(tree_for *this_for)
      {
        switch (kind())
          {
            case SK_INITIAL:
                return FALSE;
            case SK_DEST_WRITE:
                return FALSE;
            case SK_CALL_OR_GEN:
                break;
            case SK_MEM_WRITE:
                return FALSE;
            case SK_FOR_INDEX:
                return (the_for() == this_for);
            default:
                assert(FALSE);
                return FALSE;
          }

        return FALSE;
      }
    boolean killed_exiting_scope(base_symtab *scope)
      {
        if ((kind() == SK_INITIAL) && (var()->parent() == scope))
            return TRUE;

        return FALSE;
      }
    boolean is_same_fact(propagated_fact *other_fact)
      {
        impossible_source_fact *other_is_fact =
                (impossible_source_fact *)other_fact;
        return (the_def.is_same(other_is_fact->def()));
      }
    void merge_reasons(propagated_fact * /* other_fact */)
      {
        return;
      }

    dce_def *def() { return &the_def; }
    var_sym *var() { return the_def.var(); }
    source_kinds kind() { return the_def.kind(); }
    instruction *instr() { return the_def.instr(); }
    tree_for *the_for() { return the_def.the_for(); }
  };

class dce_fact_manager : public fact_manager
  {
private:
    void act_on_node_facts(tree_node *the_node,
                           propagated_fact_list *fact_list);
    void other_defs_impossible(dce_def *the_def,
                               propagated_fact_list *fact_list);

public:
    void initial_fact_creator(propagated_fact_list *the_facts);
    void instr_fact_creator(propagated_fact_list *the_facts,
                            instruction *the_instr);
    void bound_fact_creator(propagated_fact_list *, tree_for *);
    void step_fact_creator(propagated_fact_list *, tree_for *);
    void index_fact_creator(propagated_fact_list *the_facts,
                            tree_for *the_for);
    void act_on_facts(instruction *the_instr, propagated_fact_list *fact_list);
    void act_on_for_facts(tree_for *the_for, propagated_fact_list *fact_list);
    boolean use_kill_summary(void) { return FALSE; }
  };

DECLARE_LIST_CLASS(dce_def_list, dce_def_record *);
DECLARE_LIST_CLASS(dce_use_list, dce_use_record *);

class dce_def_record
  {
private:
    dce_def the_def;

public:
    dce_use_list possible_uses;

    dce_def_record(var_sym *initial_var) : the_def(initial_var) { }
    dce_def_record(var_sym *initial_var, source_kinds initial_kind) :
            the_def(initial_var, initial_kind) { }
    dce_def_record(var_sym *initial_var, source_kinds initial_kind,
                   instruction *initial_instr) :
            the_def(initial_var, initial_kind, initial_instr) { }
    dce_def_record(tree_for *initial_for) : the_def(initial_for) { }
    ~dce_def_record(void);

    dce_def *def(void) { return &the_def; }
  };

class dce_use_record
  {
private:
    var_sym *the_var;
    tree_node *the_node;

public:
    dce_def_list possible_defs;

    dce_use_record(var_sym *init_var, tree_node *init_node)
      {
        the_var = init_var;
        the_node = init_node;
      }
    ~dce_use_record(void)
      {
        while (!possible_defs.is_empty())
          {
            dce_def_record *this_def = possible_defs.pop();
            dce_use_list_e *the_elem = this_def->possible_uses.lookup(this);
            assert(the_elem != NULL);
            this_def->possible_uses.remove(the_elem);
            delete the_elem;
          }
      }

    var_sym *var() { return the_var; }
    tree_node *node() { return the_node; }
  };

class all_defs_record
  {
private:
    var_sym *the_var;

public:
    dce_def_list all_defs;

    all_defs_record(var_sym *init_var) { the_var = init_var; }
    ~all_defs_record(void)
      {
        while (!all_defs.is_empty())
          {
            dce_def_record *this_record = all_defs.pop();
            delete this_record;
          }
      }

    var_sym *var() { return the_var; }
  };

DECLARE_LIST_CLASS(all_defs_list, all_defs_record *);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_bound_var;
static const char *k_porky_use_list = NULL;
static all_defs_list *all_interesting_vars = NULL;
static instruction_list *dead_list = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void bound_mark_bound_vars(array_bound the_bound);
static void node_mark_bound_vars(tree_node *the_node, void *);
static boolean is_bound_var(var_sym *the_var);
static void add_defs_for_node(tree_node *the_node, void *);
static void complete_defs_for_node(tree_node *the_node, void *);
static void add_uses_for_node(tree_node *the_node, void *);
static void remove_uses_for_node(tree_node *the_node, void *);
static dce_use_record *find_use_record(tree_node *the_node, var_sym *the_var);
static void mark_dead(void);
static void remove_dead(void);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_dead_code(void)
  {
    ANNOTE(k_porky_bound_var, "porky bound var", FALSE);
  }

extern void proc_dead_code(tree_proc *the_proc)
  {
    if (k_porky_use_list == NULL)
      {
        ANNOTE(k_porky_use_list, "porky use list", FALSE);
      }

    node_mark_bound_vars(the_proc, NULL);
    the_proc->map(&node_mark_bound_vars, NULL);

    dce_fact_manager the_manager;
    all_interesting_vars = new all_defs_list;
    the_proc->map(&add_defs_for_node, NULL);
    the_proc->map(&complete_defs_for_node, NULL);
    the_proc->map(&add_uses_for_node, NULL);
    apply_through_structured_control(the_proc->body(), &the_manager);

    dead_list = new instruction_list();
    mark_dead();
    remove_dead();
    delete dead_list;
    dead_list = NULL;

    the_proc->map(&remove_uses_for_node, NULL);
    while (!all_interesting_vars->is_empty())
      {
        all_defs_record *this_record = all_interesting_vars->pop();
        delete this_record;
      }

    delete all_interesting_vars;
    all_interesting_vars = NULL;
  }

extern void proc_dead_code0(tree_proc *the_proc)
  {
    if (k_porky_use_list == NULL)
      {
        ANNOTE(k_porky_use_list, "porky use list", FALSE);
      }

    node_mark_bound_vars(the_proc, NULL);
    the_proc->map(&node_mark_bound_vars, NULL);

    /* unused: dce_fact_manager the_manager; */
    all_interesting_vars = new all_defs_list;
    the_proc->map(&add_defs_for_node, NULL);
    the_proc->map(&complete_defs_for_node, NULL);
    the_proc->map(&add_uses_for_node, NULL);

    dead_list = new instruction_list();
    mark_dead();
    remove_dead();
    delete dead_list;
    dead_list = NULL;

    the_proc->map(&remove_uses_for_node, NULL);
    while (!all_interesting_vars->is_empty())
      {
        all_defs_record *this_record = all_interesting_vars->pop();
        delete this_record;
      }

    delete all_interesting_vars;
    all_interesting_vars = NULL;
  }

extern void symtab_mark_bound_vars(base_symtab *the_symtab)
  {
    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (this_type->is_array())
          {
            array_type *the_array = (array_type *)this_type;
            bound_mark_bound_vars(the_array->lower_bound());
            bound_mark_bound_vars(the_array->upper_bound());
          }
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

void dce_fact_manager::act_on_node_facts(tree_node *the_node,
                                         propagated_fact_list *fact_list)
  {
    propagated_fact_list_iter fact_iter(fact_list);
    while (!fact_iter.is_empty())
      {
        propagated_fact *this_fact = fact_iter.step();
        impossible_source_fact *this_is_fact =
                (impossible_source_fact *)this_fact;
        dce_use_record *this_dce_use =
                find_use_record(the_node, this_is_fact->var());
        if (this_dce_use != NULL)
          {
            dce_def_list_e *this_elem = this_dce_use->possible_defs.head();
            while (this_elem != NULL)
              {
                dce_def_record *this_def = this_elem->contents;
                if (this_def->def()->is_same(this_is_fact->def()))
                  {
                    dce_use_list_e *use_elem =
                            this_def->possible_uses.lookup(this_dce_use);
                    assert(use_elem != NULL);
                    this_def->possible_uses.remove(use_elem);
                    delete use_elem;
                    this_dce_use->possible_defs.remove(this_elem);
                    delete this_elem;
                    break;
                  }
                this_elem = this_elem->next();
              }
          }
      }
  }

void dce_fact_manager::other_defs_impossible(dce_def *the_def,
                                             propagated_fact_list *the_facts)
  {
    propagated_fact_list *new_facts = new propagated_fact_list;

    all_defs_list_iter defs_iter(all_interesting_vars);
    while (!defs_iter.is_empty())
      {
        all_defs_record *this_record = defs_iter.step();
        if (this_record->var() == the_def->var())
          {
            dce_def_list_iter def_iter(&(this_record->all_defs));
            while (!def_iter.is_empty())
              {
                dce_def_record *this_def = def_iter.step();
                if (!this_def->def()->is_same(the_def))
                  {
                    new_facts->append(new impossible_source_fact(
                            this_def->def()));
                  }
              }
            break;
          }
      }

    propagated_fact_list_iter old_iter(the_facts);
    while (!old_iter.is_empty())
      {
        propagated_fact *old_fact = old_iter.step();
        propagated_fact_list_e *new_elem = new_facts->head();
        while (new_elem != NULL)
          {
            if (new_elem->contents->is_same_fact(old_fact))
              {
                new_facts->remove(new_elem);
                delete new_elem->contents;
                delete new_elem;
                break;
              }
            new_elem = new_elem->next();
          }
      }

    the_facts->append(new_facts);
    delete new_facts;
  }

void dce_fact_manager::initial_fact_creator(propagated_fact_list *the_facts)
  {
    all_defs_list_iter defs_iter(all_interesting_vars);
    while (!defs_iter.is_empty())
      {
        all_defs_record *this_record = defs_iter.step();
        dce_def_list_iter def_iter(&(this_record->all_defs));
        while (!def_iter.is_empty())
          {
            dce_def_record *that_def = def_iter.step();
            if (that_def->def()->kind() != SK_INITIAL)
                the_facts->append(new impossible_source_fact(that_def->def()));
          }
      }
  }

void dce_fact_manager::instr_fact_creator(propagated_fact_list *the_facts,
                                          instruction *the_instr)
  {
    if (the_instr->dst_op().is_symbol())
      {
        var_sym *dest_var = the_instr->dst_op().symbol();
        dce_def this_def(dest_var, SK_DEST_WRITE, the_instr);
        other_defs_impossible(&this_def, the_facts);
      }
  }

void dce_fact_manager::bound_fact_creator(propagated_fact_list *, tree_for *)
  {
    return;
  }

void dce_fact_manager::step_fact_creator(propagated_fact_list *, tree_for *)
  {
    return;
  }

void dce_fact_manager::index_fact_creator(propagated_fact_list *the_facts,
                                          tree_for *the_for)
  {
    dce_def this_def(the_for);
    other_defs_impossible(&this_def, the_facts);
    return;
  }

void dce_fact_manager::act_on_facts(instruction *the_instr,
                                    propagated_fact_list *fact_list)
  {
    tree_node *the_node = the_instr->owner();
    act_on_node_facts(the_node, fact_list);
  }

void dce_fact_manager::act_on_for_facts(tree_for *the_for,
                                        propagated_fact_list *fact_list)
  {
    act_on_node_facts(the_for, fact_list);
  }

dce_def_record::~dce_def_record(void)
  {
    while (!possible_uses.is_empty())
      {
        dce_use_record *this_use = possible_uses.pop();
        dce_def_list_e *the_elem = this_use->possible_defs.lookup(this);
        assert(the_elem != NULL);
        this_use->possible_defs.remove(the_elem);
        delete the_elem;
      }
  }


static void bound_mark_bound_vars(array_bound the_bound)
  {
    if (the_bound.is_variable())
      {
        var_sym *the_var = the_bound.variable();
        if (the_var->annotes()->peek_annote(k_porky_bound_var) == NULL)
            the_var->append_annote(k_porky_bound_var, new immed_list);
      }
  }

static void node_mark_bound_vars(tree_node *the_node, void *)
  {
    if (the_node->is_block())
      {
        tree_block *the_block = (tree_block *)the_node;
        symtab_mark_bound_vars(the_block->symtab());
      }
  }

static boolean is_bound_var(var_sym *the_var)
  {
    return (the_var->annotes()->peek_annote(k_porky_bound_var) != NULL);
  }

static void add_defs_for_node(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();
        if (the_instr->dst_op().is_symbol())
          {
            var_sym *the_var = the_instr->dst_op().symbol();
            all_defs_record *the_record = NULL;
            all_defs_list_iter defs_iter(all_interesting_vars);
            while (!defs_iter.is_empty())
              {
                all_defs_record *this_record = defs_iter.step();
                if (this_record->var() == the_var)
                  {
                    the_record = this_record;
                    break;
                  }
              }
            if (the_record == NULL)
              {
                the_record = new all_defs_record(the_var);
                the_record->all_defs.append(new dce_def_record(the_var));
                if ((!(the_var->is_auto())) || the_var->is_addr_taken())
                  {
                    the_record->all_defs.append(new dce_def_record(the_var,
                            SK_CALL_OR_GEN));
                  }
                all_interesting_vars->append(the_record);
              }

            the_record->all_defs.append(new dce_def_record(the_var,
                                                           SK_DEST_WRITE,
                                                           the_instr));
          }
      }
  }

static void complete_defs_for_node(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();
        switch (the_instr->opcode())
          {
            case io_str:
            case io_memcpy:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                sym_node *base_sym =
                        operand_address_root_symbol(the_rrr->dst_addr_op());
                if ((base_sym != NULL) && base_sym->is_var())
                  {
                    var_sym *base_var = (var_sym *)base_sym;
                    all_defs_list_iter defs_iter(all_interesting_vars);
                    while (!defs_iter.is_empty())
                      {
                        all_defs_record *this_record = defs_iter.step();
                        if (this_record->var()->overlaps(base_var))
                          {
                            this_record->all_defs.append(
                                    new dce_def_record(this_record->var(),
                                                       SK_MEM_WRITE,
                                                       the_instr));
                          }
                      }
                  }
                else
                  {
                    all_defs_list_iter defs_iter(all_interesting_vars);
                    while (!defs_iter.is_empty())
                      {
                        all_defs_record *this_record = defs_iter.step();
                        if (this_record->var()->is_addr_taken())
                          {
                            this_record->all_defs.append(
                                    new dce_def_record(this_record->var(),
                                                       SK_MEM_WRITE,
                                                       the_instr));
                          }
                      }
                  }
                break;
              }
            default:
                break;
          }
      }
    else if (the_node->is_for())
      {
        tree_for *the_for = (tree_for *)the_node;
        all_defs_list_iter defs_iter(all_interesting_vars);
        while (!defs_iter.is_empty())
          {
            all_defs_record *this_record = defs_iter.step();
            if (this_record->var() == the_for->index())
              {
                this_record->all_defs.append(new dce_def_record(the_for));
                break;
              }
          }
      }
  }

static void add_uses_for_node(tree_node *the_node, void *)
  {
    tree_node_list *parent_list = the_node->parent();
    if (parent_list != NULL)
      {
        tree_node *parent_node = parent_list->parent();
        if ((parent_node != NULL) && parent_node->is_for())
          {
            tree_for *parent_for = (tree_for *)parent_node;
            if ((parent_for->lb_list() == parent_list) ||
                (parent_for->ub_list() == parent_list) ||
                (parent_for->step_list() == parent_list))
              {
                return;
              }
          }
      }

    dce_use_list *the_use_list = NULL;

    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        instruction *the_instr = the_tree_instr->instr();

        all_defs_list_iter defs_iter(all_interesting_vars);
        while (!defs_iter.is_empty())
          {
            all_defs_record *this_record = defs_iter.step();
            if (instr_may_reference_var(the_instr, this_record->var()) ||
                ((the_instr->opcode() == io_ret) &&
                 (this_record->var()->is_global() ||
                  this_record->var()->type()->is_call_by_ref() ||
                  this_record->var()->is_static())))
              {
                dce_use_record *use_record =
                        new dce_use_record(this_record->var(), the_node);
                dce_def_list_iter def_iter(&(this_record->all_defs));
                while (!def_iter.is_empty())
                  {
                    dce_def_record *this_def = def_iter.step();
                    use_record->possible_defs.append(this_def);
                    this_def->possible_uses.append(use_record);
                  }
                if (the_use_list == NULL)
                    the_use_list = new dce_use_list;
                the_use_list->append(use_record);
              }
          }
      }
    else if (the_node->is_for())
      {
        tree_for *the_for = (tree_for *)the_node;

        all_defs_list_iter defs_iter(all_interesting_vars);
        while (!defs_iter.is_empty())
          {
            all_defs_record *this_record = defs_iter.step();
            if (operand_may_reference_var(the_for->lb_op(),
                                          this_record->var()) ||
                operand_may_reference_var(the_for->ub_op(),
                                          this_record->var()) ||
                operand_may_reference_var(the_for->step_op(),
                                          this_record->var()))
              {
                dce_use_record *use_record =
                        new dce_use_record(this_record->var(), the_node);
                dce_def_list_iter def_iter(&(this_record->all_defs));
                while (!def_iter.is_empty())
                  {
                    dce_def_record *this_def = def_iter.step();
                    use_record->possible_defs.append(this_def);
                    this_def->possible_uses.append(use_record);
                  }
                if (the_use_list == NULL)
                    the_use_list = new dce_use_list;
                the_use_list->append(use_record);
              }
          }
      }

    if (the_use_list != NULL)
        the_node->append_annote(k_porky_use_list, the_use_list);
  }

static void remove_uses_for_node(tree_node *the_node, void *)
  {
    dce_use_list *the_use_list =
            (dce_use_list *)(the_node->get_annote(k_porky_use_list));
    if (the_use_list != NULL)
      {
        while (!the_use_list->is_empty())
          {
            dce_use_record *this_record = the_use_list->pop();
            delete this_record;
          }
        delete the_use_list;
      }
  }

static dce_use_record *find_use_record(tree_node *the_node, var_sym *the_var)
  {
    dce_use_list *the_use_list =
            (dce_use_list *)(the_node->peek_annote(k_porky_use_list));
    if (the_use_list == NULL)
        return NULL;

    dce_use_list_iter use_iter(the_use_list);
    while (!use_iter.is_empty())
      {
        dce_use_record *this_record = use_iter.step();
        if (this_record->var() == the_var)
            return this_record;
      }
    return NULL;
  }

static void mark_dead(void)
  {
    all_defs_list_iter defs_iter(all_interesting_vars);
    while (!defs_iter.is_empty())
      {
        all_defs_record *this_record = defs_iter.step();
        dce_def_list_iter def_iter(&(this_record->all_defs));
        while (!def_iter.is_empty())
          {
            dce_def_record *this_def = def_iter.step();
            if (this_def->possible_uses.is_empty() &&
                !is_bound_var(this_def->def()->var()))
              {
                dce_def *dead_def = this_def->def();
                if (dead_def->kind() == SK_DEST_WRITE)
                    dead_list->append(dead_def->instr());
              }
          }
      }
  }

static void remove_dead(void)
  {
    while (!dead_list->is_empty())
      {
        instruction *this_instr = dead_list->pop();
        tree_instr *parent = this_instr->parent();
        parent->remove_instr(this_instr);
        this_instr->set_dst(operand());

        dce_use_list *use_list =
                (dce_use_list *)(parent->get_annote(k_porky_use_list));

        tree_node_list *side_effects =
                reduce_to_side_effects(operand(this_instr));
        if (side_effects != NULL)
          {
            while (!side_effects->is_empty())
              {
                tree_node *this_effect = side_effects->pop();
                assert(this_effect->is_instr());
                tree_instr *effect_tree_instr = (tree_instr *)this_effect;
                instruction *effect_instr = effect_tree_instr->instr();

                if (use_list != NULL)
                  {
                    dce_use_list *new_use_list = NULL;
                    dce_use_list_iter use_iter(use_list);
                    while (!use_iter.is_empty())
                      {
                        dce_use_record *this_record = use_iter.step();
                        if (instr_may_reference_var(effect_instr,
                                                    this_record->var()))
                          {
                            if (new_use_list == NULL)
                                new_use_list = new dce_use_list;
                            dce_use_record *new_record =
                                    new dce_use_record(this_record->var(),
                                                       this_effect);

                            dce_def_list_iter def_iter(&(
                                    this_record->possible_defs));
                            while (!def_iter.is_empty())
                              {
                                dce_def_record *this_def = def_iter.step();
                                new_record->possible_defs.append(this_def);
                                this_def->possible_uses.append(new_record);
                              }

                            new_use_list->append(new_record);
                          }
                      }
                    if (new_use_list != NULL)
                      {
                        this_effect->append_annote(k_porky_use_list,
                                                   new_use_list);
                      }
                  }
                parent->parent()->insert_before(this_effect, parent->list_e());
              }

            delete side_effects;
          }

        if (use_list != NULL)
          {
            while (!use_list->is_empty())
              {
                dce_use_record *this_record = use_list->pop();
                while (!this_record->possible_defs.is_empty())
                  {
                    dce_def_record *this_def =
                            this_record->possible_defs.pop();
                    dce_use_list_e *use_elem = this_def->possible_uses.head();
                    while (use_elem != NULL)
                      {
                        if (use_elem->contents == this_record)
                          {
                            this_def->possible_uses.remove(use_elem);
                            delete use_elem;
                            break;
                          }
                        use_elem = use_elem->next();
                      }

                    if (this_def->possible_uses.is_empty())
                      {
                        dce_def *dead_def = this_def->def();
                        if (dead_def->kind() == SK_DEST_WRITE)
                            dead_list->append(dead_def->instr());
                      }
                  }
                delete this_record;
              }

            delete use_list;
          }

        parent->parent()->remove(parent->list_e());
        delete parent->list_e();
        delete parent;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
