/* file "kill_enum.cc" */

/*  Copyright (c) 1997 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to replace references to enumerated types with references to
 * corresponding plain integer types, for the porky program for SUIF
 */


#define RCS_BASE_FILE kill_enum_cc

#include "porky.h"

RCS_BASE(
    "$Id: kill_enum.cc,v 1.2 1999/08/25 03:27:30 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_kill_enum_base;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void kill_enum_on_object(suif_object *the_object);
static void kill_enum_on_type(type_node *the_type, so_walker *the_walker);
static void kill_enum_ready_symtab(base_symtab *the_symtab);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_kill_enum(void)
  {
    k_porky_kill_enum_base = lexicon->enter("porky kill enum base")->sp;
  }

extern void kill_enum_on_proc(tree_proc *the_proc)
  {
    so_walker the_walker;
    the_walker.set_pre_function(&kill_enum_on_object);
    the_walker.set_leaf_function(&kill_enum_on_type);
    the_walker.walk(the_proc);
  }

extern void kill_enum_on_symtab(base_symtab *the_symtab)
  {
    kill_enum_ready_symtab(the_symtab);
    so_walker the_walker;
    the_walker.set_pre_function(&kill_enum_on_object);
    the_walker.set_leaf_function(&kill_enum_on_type);
    the_walker.walk(the_symtab);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void kill_enum_on_object(suif_object *the_object)
  {
    if (!the_object->is_tree_obj())
        return;
    tree_node *the_node = (tree_node *)the_object;
    if (!the_node->is_block())
        return;
    tree_block *the_block = (tree_block *)the_node;
    kill_enum_ready_symtab(the_block->symtab());
  }

static void kill_enum_on_type(type_node *the_type, so_walker *the_walker)
  {
    if (!the_type->is_enum())
        return;
    type_node *replacement =
            (type_node *)(the_type->peek_annote(k_porky_kill_enum_base));
    assert(replacement != NULL);
    the_walker->replace_type(replacement);
  }

static void kill_enum_ready_symtab(base_symtab *the_symtab)
  {
    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (!this_type->is_enum())
            continue;
        enum_type *this_enum = (enum_type *)this_type;
        base_type *new_base =
                new base_type(TYPE_INT, this_enum->size(),
                              this_enum->is_signed());
        this_enum->append_annote(k_porky_kill_enum_base,
                                 this_enum->parent()->install_type(new_base));
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
