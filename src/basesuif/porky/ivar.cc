/* file "ivar.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE ivar_cc

#include "porky.h"

RCS_BASE(
    "$Id: ivar.cc,v 1.2 1999/08/25 03:27:30 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

typedef struct
  {
    var_sym *the_var_sym;
    boolean the_boolean;
  } var_bool;

typedef struct
  {
    sym_node *the_sym_node;
    int the_offset;
    boolean the_boolean;
  } sym_offset_boolean;

typedef struct
  {
    tree_instr *sum_instr;
    operand pre_sum;
    operand post_sum;
    boolean past_instr;
  } ivar_sum_node;

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variable Declarations
 *----------------------------------------------------------------------*/

static char *f_suif_acc = "f_suif_acc";
static char *i_suif_acc = "i_suif_acc";
static char *f_suif_sum = "f_suif_sum";
static char *i_suif_sum = "i_suif_sum";
static int sum_num = 1;

/*----------------------------------------------------------------------*
    End Private Global Variable Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static var_sym_list *all_candidate_vars(tree_block *the_block);
static void add_candidate_vars(block_symtab *the_symtab,
                               var_sym_list *the_list);
static void ivars_on_list(tree_node_list *the_node_list,
                          var_sym_list *the_list);
static void reductions_on_list(tree_node_list *the_node_list,
                               var_sym_list *the_list);
static void ivars_on_node(tree_node *the_node, void *data);
static void reductions_on_node(tree_node *the_node, void *data);
static operand sum_up(operand to_sum, var_sym *index_var, operand lower_bound,
                      operand upper_bound, operand step);
static boolean is_one_step_summable(operand to_sum, var_sym *index_var);
static boolean reads_besides_writes(tree_node_list *the_node_list,
                                    var_sym *the_variable);
static void reads_on_unwrites_on_node(tree_node *the_node, void *data);
static boolean instr_may_read_var(instruction *the_instr, var_sym *the_var);
static boolean operand_may_read_var(operand the_operand, var_sym *the_var);
static boolean is_loop_constant_index_function(tree_for *the_for,
                                               operand the_operand);
static boolean loop_constant_check_function(tree_for *the_for,
                                            operand the_operand,
                                            boolean allow_index);
static tree_for *next_for_up(tree_node *the_node);
static void replace_ivar_refs(var_sym *old_var, tree_node_list *the_list,
                              ivar_sum_node *the_sum_node);
static void clone_replace_on_instr(instruction *the_instr, var_sym *to_replace,
                                   operand replacement, tree_node *place);
static operand clone_replace_on_operand(operand the_operand,
                                        var_sym *to_replace,
                                        operand replacement, tree_node *place);
static boolean contains_function_that_might_modify(instruction *the_instr,
                                                   var_sym *the_variable);
static boolean call_modifies_arg(in_cal *the_call, unsigned arg_num);
static void fix_breaks(tree_for *the_for, var_sym *final_iteration_count);
static void fix_breaks(tree_node_list *the_list, tree_for *the_for,
                       var_sym *final_iteration_count);
static void fix_break(instruction *break_instr, tree_for *the_for,
                      var_sym *final_iteration_count);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

/*
 *  These passes assume that lb, ub, and step are loop invariants, so
 *  the porky ``-defaults'' pass should be run before, or
 *  concurrently with, either of these passes.
 */
extern void find_ivars(tree_node *the_node)
  {
    if (the_node->kind() != TREE_BLOCK)
        return;

    tree_block *the_block = (tree_block *)the_node;
    var_sym_list *candidates = all_candidate_vars(the_block);
    ivars_on_list(the_block->body(), candidates);
    delete candidates;
  }

extern void find_reductions(tree_node *the_node)
  {
    if (the_node->kind() != TREE_BLOCK)
        return;

    tree_block *the_block = (tree_block *)the_node;
    var_sym_list *candidates = all_candidate_vars(the_block);
    reductions_on_list(the_block->body(), candidates);
    delete candidates;
  }

extern boolean is_loop_constant(tree_for *the_for, operand the_operand)
  {
    return loop_constant_check_function(the_for, the_operand, FALSE);
  }

extern int possible_writes(tree_node_list *the_node_list,
                           var_sym *the_variable, instruction **where)
  {
    boolean found_one = FALSE;
    boolean bad_control_flow = FALSE;
    instruction *location;

    tree_node_list_iter the_iter(the_node_list);
    while (!the_iter.is_empty())
      {
        tree_node *the_node = the_iter.step();
        switch (the_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)the_node;
                instruction *the_instr = the_tree_instr->instr();
                assert(the_instr != NULL);

                if (the_variable->type()->is_call_by_ref())
                  {
                    if (contains_function_that_might_modify(the_instr,
                                                            the_variable))
                      {
                        return 2;
                      }
                  }
                else if (the_variable->is_addr_taken())
                  {
                    if (instr_contains_impure_call(the_instr))
                        return 2;
                    if (instr_might_store(the_instr, the_variable))
                        return 2;
                  }

                if ((the_instr->opcode() == io_btrue) ||
                    (the_instr->opcode() == io_bfalse) ||
                    (the_instr->opcode() == io_jmp) ||
                    (the_instr->opcode() == io_mbr) ||
                    (the_instr->opcode() == io_lab))
                  {
                    bad_control_flow = TRUE;
                  }

                operand destination = the_instr->dst_op();
                if (!destination.is_symbol())
                    break;

                var_sym *the_symbol = destination.symbol();
                if (the_symbol->overlaps(the_variable))
                  {
                    if (the_symbol == the_variable)
                      {
                        if (found_one || bad_control_flow)
                            return 2;
                        found_one = TRUE;
                        location = the_instr;
                      }
                    else
                      {
                        return 2;
                      }
                  }

                break;
              }
            case TREE_LOOP:
              {
                tree_loop *the_loop = (tree_loop *)the_node;
                instruction *new_location;
                int result = possible_writes(the_loop->body(), the_variable,
                                             &new_location);
                if (result != 0)
                    return 2;

                result = possible_writes(the_loop->test(), the_variable,
                                         &new_location);
                if (result != 0)
                    return 2;

                break;
              }
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)the_node;
                if (the_for->index()->overlaps(the_variable))
                    return 2;

                instruction *new_location;
                int result = possible_writes(the_for->landing_pad(),
                                             the_variable, &new_location);
                if (result != 0)
                    return 2;

                result = possible_writes(the_for->body(), the_variable,
                                         &new_location);
                if (result == 2)
                    return 2;
                if (result == 1)
                  {
                    if (found_one || bad_control_flow)
                        return 2;
                    found_one = TRUE;
                    location = new_location;
                  }
                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)the_node;
                instruction *new_location;
                int result = possible_writes(the_if->header(), the_variable,
                                             &new_location);
                if (result == 2)
                    return 2;
                if (result == 1)
                  {
                    if (found_one || bad_control_flow)
                        return 2;
                    found_one = TRUE;
                    location = new_location;
                  }

                result = possible_writes(the_if->then_part(), the_variable,
                                         &new_location);
                if (result != 0)
                    return 2;

                result = possible_writes(the_if->else_part(), the_variable,
                                         &new_location);
                if (result != 0)
                    return 2;

                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)the_node;
                instruction *new_location;
                int result = possible_writes(the_block->body(), the_variable,
                                             &new_location);
                if (result == 2)
                    return 2;
                if (result == 1)
                  {
                    if (found_one || bad_control_flow)
                        return 2;
                    found_one = TRUE;
                    location = new_location;
                  }
                break;
              }
            default:
                assert(FALSE);
          }
      }
    if (found_one)
      {
        *where = location;
        return 1;
      }
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static var_sym_list *all_candidate_vars(tree_block *the_block)
  {
    block_symtab *the_symtab = the_block->symtab();
    var_sym_list *the_list = new var_sym_list;
    add_candidate_vars(the_symtab, the_list);
    return the_list;
  }

static void add_candidate_vars(block_symtab *the_symtab,
                               var_sym_list *the_list)
  {
    sym_node_list *symbols = the_symtab->symbols();
    sym_node_list_iter the_iter(symbols);
    while (!the_iter.is_empty())
      {
        sym_node *the_symbol = the_iter.step();
        if (!the_symbol->is_var())
            continue;

        var_sym *the_var = (var_sym *)the_symbol;
        if (the_var->type()->is_volatile())
            continue;

        the_list->append(the_var);
      }

    base_symtab_list_iter the_symtab_iter(the_symtab->children());
    while (!the_symtab_iter.is_empty())
      {
        base_symtab *child_symtab = the_symtab_iter.step();
        assert(child_symtab->is_block());
        block_symtab *child_block_symtab = (block_symtab *)child_symtab;
        add_candidate_vars(child_block_symtab, the_list);
      }
  }

static void ivars_on_list(tree_node_list *the_node_list,
                          var_sym_list *the_list)
  {
    the_node_list->map(&ivars_on_node, the_list, TRUE);
  }

static void reductions_on_list(tree_node_list *the_node_list,
                               var_sym_list *the_list)
  {
    the_node_list->map(&reductions_on_node, the_list, TRUE);
  }

static void ivars_on_node(tree_node *the_node, void *data)
  {
    var_sym_list *var_list = (var_sym_list *)data;
    if (!the_node->is_for())
        return;
    tree_for *the_for = (tree_for *)the_node;

    var_sym_list_iter the_iter(var_list);
    while (!the_iter.is_empty())
      {
        var_sym *this_var = the_iter.step();
        instruction *where;
        int writes = possible_writes(the_for->body(), this_var, &where);
        if (writes != 1)
            continue;

        while (where->opcode() == io_cpy)
          {
            in_rrr *the_cpy = (in_rrr *)where;
            operand source = the_cpy->src1_op();
            if (!source.is_expr())
                break;
            where = source.instr();
            assert(where != NULL);
          }

        if ((where->opcode() != io_add) && (where->opcode() != io_sub))
            continue;

        in_rrr *the_rrr = (in_rrr *)where;
        if (the_rrr->opcode() == io_add)
          {
            operand source1 = the_rrr->src1_op();
            if ((!source1.is_symbol()) || (source1.symbol() != this_var))
              {
                operand source2 = the_rrr->src2_op();
                source1.remove();
                source2.remove();
                the_rrr->set_src1(source2);
                the_rrr->set_src2(source1);
              }
          }

        operand source1 = the_rrr->src1_op();
        if (!source1.is_symbol())
            continue;

        if (source1.symbol() != this_var)
            continue;

        if (!is_loop_constant_index_function(the_for, the_rrr->src2_op()))
            continue;

        if (!is_one_step_summable(the_rrr->src2_op(), the_for->index()))
            continue;

        tree_for *containing_for = next_for_up(the_rrr->owner());

        if (containing_for != the_for)
            continue;

        if (verbosity_level > 0)
          {
            printf("Ivar detection: Variable %s in loop on %s at line %d is "
                   "an induction variable.\n", this_var->name(),
                   the_for->index()->name(), source_line_num(the_for));
          }

        guard_for(the_for);

        if (might_modify(the_for->lb_op(), the_for->body()))
            make_lb_temp(the_for);

        if (might_modify(the_for->ub_op(), the_for->body()))
            make_ub_temp(the_for);

        if (might_modify(the_for->step_op(), the_for->body()))
            make_step_temp(the_for);

        immed step_immed;
        eval_status step_eval_status =
                evaluate_const_expr(the_for->step_op(), &step_immed);
        boolean step_is_negative_const =
                ((step_eval_status == EVAL_OK) && step_immed.is_int_const() &&
                 immed_to_ii(step_immed) < i_integer(0));

        var_sym *initial_var = this_var;

        operand increment = the_rrr->src2_op().clone();

        operand iter_num =
                sum_up(increment.clone(), the_for->index(),
                       the_for->lb_op().clone(), operand(the_for->index()),
                       the_for->step_op().clone());
        operand next_index;
        if (step_is_negative_const)
          {
            next_index =
                    operand(the_for->index()) - (-immed_to_ii(step_immed));
          }
        else
          {
            next_index =
                    operand_add(the_for->index()->type()->unqual(),
                                operand(the_for->index()),
                                the_for->step_op().clone());
          }
        operand iter_num2 =
                sum_up(increment, the_for->index(), the_for->lb_op().clone(),
                       next_index, the_for->step_op().clone());

        iter_num = ((the_rrr->opcode() == io_sub) ?
                    operand(initial_var) - iter_num :
                    operand(initial_var) + iter_num);
        iter_num2 = ((the_rrr->opcode() == io_sub) ?
                     operand(initial_var) - iter_num2 :
                     operand(initial_var) + iter_num2);

        ivar_sum_node the_sum_node;

        the_sum_node.sum_instr = the_rrr->parent();
        the_sum_node.pre_sum = iter_num;
        the_sum_node.post_sum = iter_num2;
        the_sum_node.past_instr = FALSE;

        replace_ivar_refs(this_var, the_for->body(), &the_sum_node);

        assert(the_sum_node.past_instr);

        kill_op(iter_num);
        kill_op(iter_num2);

        iter_num = the_for->ub_op().clone();
        if ((the_for->test() == FOR_SGTE) || (the_for->test() == FOR_UGTE))
            iter_num -= 1;
        if ((the_for->test() == FOR_SLTE) || (the_for->test() == FOR_ULTE))
            iter_num += 1;

        if (potential_break(the_for))
          {
            var_sym *final_iteration_count =
                    the_for->scope()->new_unique_var(the_for->ub_op().type());
            tree_node *initial_assignment =
                    assign(final_iteration_count, iter_num);
            insert_before(initial_assignment, the_for);
            fix_breaks(the_for, final_iteration_count);
            iter_num = operand(final_iteration_count);
          }

        increment = the_rrr->src2_op().clone();
        increment = cast_op(increment, diff_type(this_var->type()->unqual()));
        iter_num = sum_up(increment, the_for->index(),
                          the_for->lb_op().clone(), iter_num,
                          the_for->step_op().clone());

        tree_node *assign_node =
                assign(this_var,
                       ((the_rrr->opcode() == io_sub) ?
                        initial_var - iter_num : initial_var + iter_num));

        tree_node_list *parent_list = the_for->parent();
        assert(parent_list != NULL);
        tree_node_list_e *parent_element = the_for->list_e();
        assert(parent_element != NULL);
        parent_list->insert_after(assign_node, parent_element);

        tree_node *incrementer = the_rrr->owner();
        assert(incrementer != NULL);
        tree_node_list *parent = incrementer->parent();
        assert(parent != NULL);
        tree_node_list_e *going = incrementer->list_e();
        going = parent->remove(going);
        delete going;
        delete incrementer;
      }
  }

static void reductions_on_node(tree_node *the_node, void *data)
  {
    var_sym_list *var_list = (var_sym_list *)data;
    if (!the_node->is_for())
        return;
    tree_for *the_for = (tree_for *)the_node;

    var_sym_list_iter the_iter(var_list);
    while (!the_iter.is_empty())
      {
        var_sym *this_var = the_iter.step();
        instruction *where;
        int writes = possible_writes(the_for->body(), this_var, &where);
        if (writes != 1)
            continue;

        if (reads_besides_writes(the_for->body(), this_var))
            continue;

        while (where->opcode() == io_cpy)
          {
            in_rrr *the_cpy = (in_rrr *)where;
            operand source = the_cpy->src1_op();
            if (!source.is_expr())
                break;
            where = source.instr();
            assert(where != NULL);
          }

        if ((where->opcode() != io_add) && (where->opcode() != io_sub))
            continue;

        in_rrr *the_rrr = (in_rrr *)where;
        if (the_rrr->opcode() == io_add)
          {
            operand source1 = the_rrr->src1_op();
            if ((!source1.is_symbol()) || (source1.symbol() != this_var))
              {
                operand source2 = the_rrr->src2_op();
                source1.remove();
                source2.remove();
                the_rrr->set_src1(source2);
                the_rrr->set_src2(source1);
              }
          }

        operand source1 = the_rrr->src1_op();
        if (!source1.is_symbol())
            continue;

        if (source1.symbol() != this_var)
            continue;

        operand to_sum = the_rrr->src2_op();
        if (operand_may_read_var(to_sum, this_var))
            continue;

        /*
         *  Note that we don't care in this case whether the expressions being
         *  summed up are independent of anything else in the loop.  It's
         *  still correct if we move the summation out.  Then the parallelizer
         *  has to figure out if the loop can really be parallelized.  If not,
         *  we could put the summation back in the loop in a later pass.
         */

        tree_for *containing_for = next_for_up(the_rrr->owner());

        if (containing_for != the_for)
            continue;

        /*
         *  Now we've recognized a place where we can move the summation out
         *  of the loop.
         */

        printf("Reduction of variable %s in loop on %s at line %d found.\n",
               this_var->name(), the_for->index()->name(),
               source_line_num(the_for));

        char *acc_name;
        char *sum_name;
        if (this_var->type()->unqual()->op() == TYPE_INT)
          {
            acc_name = i_suif_acc;
            sum_name = i_suif_sum;
          }
        else
          {
            acc_name = f_suif_acc;
            sum_name = f_suif_sum;
          }

        proc_sym *sum_sym = fileset->globals()->lookup_proc(sum_name);
        if (sum_sym == NULL)
          {
            func_type *sum_type =
                    new func_type(this_var->type()->unqual(), 1);
            sum_type->set_arg_type(0, type_signed);
            sum_type =
                    (func_type *)(fileset->globals()->install_type(sum_type));

            sum_sym = fileset->globals()->new_proc(sum_type, src_fortran,
                                                   sum_name);
            sum_sym->reset_userdef();
          }

        proc_sym *acc_sym = fileset->globals()->lookup_proc(acc_name);
        if (acc_sym == NULL)
          {
            func_type *acc_type =
                    new func_type(this_var->type()->unqual(), 2);
            acc_type->set_arg_type(0, type_signed);
            acc_type->set_arg_type(1, this_var->type()->unqual());
            acc_type =
                    (func_type *)(fileset->globals()->install_type(acc_type));

            acc_sym = fileset->globals()->new_proc(acc_type, src_fortran,
                                                   acc_name);
            acc_sym->reset_userdef();
          }

        type_node *acc_type = acc_sym->type()->ptr_to();
        operand acc_addr =
                operand(new in_ldc(acc_type, operand(), immed(acc_sym)));
        in_cal *acc_call = new in_cal(type_void, operand(), acc_addr, 2);
        acc_call->set_argument(0, operand(new in_ldc(type_signed, operand(),
                                                     immed(sum_num))));
        to_sum.remove();
        acc_call->set_argument(1, to_sum);
        tree_instr *acc_tree_instr = new tree_instr(acc_call);

        type_node *sum_type = sum_sym->type()->ptr_to();
        operand sum_addr =
                operand(new in_ldc(sum_type, operand(), immed(sum_sym)));
        in_cal *sum_call =
                new in_cal(this_var->type()->unqual(), operand(), sum_addr, 1);
        sum_call->set_argument(0, operand(new in_ldc(type_signed, operand(),
                                                     immed(sum_num))));
        instruction *new_rrr =
                new in_rrr(the_rrr->opcode(), this_var->type()->unqual(),
                           operand(this_var), operand(this_var),
                           operand(sum_call));
        tree_instr *sum_tree_instr = new tree_instr(new_rrr);

        ++sum_num;

        tree_node_list *for_parent = the_for->parent();
        tree_node_list_e *for_element = the_for->list_e();
        assert(for_parent != NULL);
        assert(for_element != NULL);
        for_parent->insert_after(sum_tree_instr, for_element);

        tree_node *old_tree_node = the_rrr->owner();
        assert(old_tree_node != NULL);
        tree_node_list *parent = old_tree_node->parent();
        assert(parent != NULL);
        tree_node_list_e *going = old_tree_node->list_e();
        assert(going != NULL);
        parent->insert_after(acc_tree_instr, going);

        going = parent->remove(going);
        delete going;
        delete old_tree_node;
      }
  }

/*
 *  Sum up the value of to_sum as index_var goes from lower_bound to
 *  upper_bound by adding ``step'' at a time.  If ``step'' is
 *  positive, continue as long as index_var is less than upper_bound;
 *  otherwise, continue as long as index_var is greater than
 *  upper_bound.
 */
static operand sum_up(operand to_sum, var_sym *index_var, operand lower_bound,
                      operand upper_bound, operand step)
  {
    operand op_a, op_b;
    type_node *sum_type = to_sum.type()->unqual();

    boolean is_good = linear_form(to_sum, index_var, &op_a, &op_b);
    assert(is_good);

    operand count = ((upper_bound - lower_bound.clone()) - 1) / step.clone();

    operand count_1 = count.clone() + 1;

    operand result_op = ((count_1.clone() + count) / 2) * step;
    result_op = cast_op(result_op, sum_type) * op_b.clone();

    operand new_term =
            ((cast_op(lower_bound, sum_type) * op_b) + op_a) *
            cast_op(count_1, sum_type);

    return result_op + new_term;
  }

/*
 *  Return TRUE iff to_sum is in a form such that it can be summed
 *  over index_var in a single expression tree by sum_up().
 */
static boolean is_one_step_summable(operand to_sum, var_sym *index_var)
  {
    operand op_a, op_b;

    boolean is_good = linear_form(to_sum, index_var, &op_a, &op_b);
    if (!is_good)
        return FALSE;

    kill_op(op_a);
    kill_op(op_b);
    return TRUE;
  }

/*
 *  Returns TRUE iff the variable is read nowhere in the node list execpt
 *  possibly in operands, including sub expressions, of instructions that
 *  write that same variable.
 */
static boolean reads_besides_writes(tree_node_list *the_node_list,
                                    var_sym *the_variable)
  {
    assert(the_node_list != NULL);
    var_bool the_data;

    the_data.the_boolean = FALSE;
    the_data.the_var_sym = the_variable;
    the_node_list->map(&reads_on_unwrites_on_node, &the_data);
    return the_data.the_boolean;
  }

static void reads_on_unwrites_on_node(tree_node *the_node, void *data)
  {
    assert(the_node != NULL);
    assert(data != NULL);

    var_bool *the_var_bool = (var_bool *)data;
    if (the_var_bool->the_boolean)
        return;

    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();
    assert(the_instr != NULL);

    operand destination = the_instr->dst_op();
    if (destination.is_symbol())
      {
        sym_node *the_symbol = destination.symbol();
        assert(the_symbol != NULL);
        if (the_symbol == the_var_bool->the_var_sym)
            return;
      }

    if (instr_may_read_var(the_instr, the_var_bool->the_var_sym))
        the_var_bool->the_boolean = TRUE;
  }

static boolean instr_may_read_var(instruction *the_instr, var_sym *the_var)
  {
    assert(the_instr != NULL);

    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        immed value = the_ldc->value();
        if (value.is_symbol())
          {
            sym_node *the_symbol = value.symbol();
            if (the_symbol->is_var())
              {
                var_sym *value_var = (var_sym *)the_symbol;
                if (value_var->overlaps(the_var))
                    return TRUE;
              }
          }
        return FALSE;
      }
    else
      {
        unsigned num_srcs = the_instr->num_srcs();
        for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
          {
            operand this_operand = the_instr->src_op(src_num);
            if (operand_may_read_var(this_operand, the_var))
                return TRUE;
          }
        return FALSE;
      }
  }

static boolean operand_may_read_var(operand the_operand, var_sym *the_var)
  {
    if (the_operand.is_symbol())
      {
        var_sym *this_symbol = the_operand.symbol();
        if (this_symbol->overlaps(the_var))
            return TRUE;
      }
    else if (the_operand.is_expr())
      {
        instruction *this_instr = the_operand.instr();
        assert(this_instr != NULL);
        if (instr_may_read_var(this_instr, the_var))
            return TRUE;
      }
    return FALSE;
  }

/*
 *  Return TRUE iff the operand is a function of the loop index and
 *  nothing else that is not a loop constant.
 */
static boolean is_loop_constant_index_function(tree_for *the_for,
                                               operand the_operand)
  {
    return loop_constant_check_function(the_for, the_operand, TRUE);
  }

/*
 *  If allow_index is TRUE, return TRUE iff the operand is a function
 *  of the loop index and nothing else that is not a loop constant.
 *  If allow_index is FALSE, return TRUE iff the operand is a loop
 *  constant.
 */
static boolean loop_constant_check_function(tree_for *the_for,
                                            operand the_operand,
                                            boolean allow_index)
  {
    if (the_operand.is_expr())
      {
        instruction *the_instr = the_operand.instr();
        assert(the_instr != NULL);
        if (instr_is_impure_call(the_instr) || (the_instr->opcode() == io_lod))
            return FALSE;
        if ((the_instr->opcode() == io_lod) || (the_instr->opcode() == io_cal))
          {
            unsigned num_srcs = the_instr->num_srcs();
            unsigned src_num = 0;
            if (the_instr->opcode() == io_cal)
                src_num = 1;
            for (; src_num < num_srcs; ++src_num)
              {
                operand this_op = the_instr->src_op(src_num);
                if (this_op.type()->unqual()->is_ptr())
                  {
                    sym_node *addr_symbol =
                            operand_address_root_symbol(this_op);
                    if (addr_symbol == NULL)
                        return FALSE;

                    if (!addr_symbol->is_var())
                        return FALSE;
                    var_sym *addr_var = (var_sym *)addr_symbol;

                    if (!loop_constant_check_function(the_for,
                                                      operand(addr_var),
                                                      allow_index))
                      {
                        return FALSE;
                      }
                  }
              }
          }
        unsigned num_srcs = the_instr->num_srcs();
        for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
          {
            if (!loop_constant_check_function(the_for,
                                              the_instr->src_op(src_num),
                                              allow_index))
              {
                return FALSE;
              }
          }
        return TRUE;
      }
    else if (the_operand.is_symbol())
      {
        sym_node *the_symbol = the_operand.symbol();
        assert(the_symbol != NULL);
        var_sym *the_var = (var_sym *)the_symbol;
        if (the_var->type()->is_volatile())
            return FALSE;

        if ((the_var == the_for->index()) && !allow_index)
            return FALSE;

        if (the_var->overlaps(the_for->index()))
            return FALSE;

        return !might_modify(operand(the_var), the_for->body());
      }
    else if (the_operand.is_null())
      {
        return TRUE;
      }
    else
      {
        assert(FALSE);
      }
    return FALSE;
  }

static tree_for *next_for_up(tree_node *the_node)
  {
    if (the_node == NULL)
        return NULL;

    tree_node_list *parent = the_node->parent();
    if (parent == NULL)
        return NULL;

    tree_node *result_node = parent->parent();

    while ((result_node != NULL) && (!result_node->is_for()))
      {
        parent = result_node->parent();
        if (parent == NULL)
            break;
        result_node = parent->parent();
      }

    return (tree_for *)result_node;
  }

static void replace_ivar_refs(var_sym *old_var, tree_node_list *the_list,
                              ivar_sum_node *the_sum_node)
  {
    tree_node_list_iter the_iter(the_list);
    while (!the_iter.is_empty())
      {
        tree_node *the_node = the_iter.step();
        assert(the_node != NULL);

        if (the_node == the_sum_node->sum_instr)
          {
            assert(!the_sum_node->past_instr);
            the_sum_node->past_instr = TRUE;
            continue;
          }

        operand current_value =
                the_sum_node->past_instr ? the_sum_node->post_sum :
                                           the_sum_node->pre_sum;
        switch (the_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)the_node;
                instruction *the_instr = the_tree_instr->instr();
                clone_replace_on_instr(the_instr, old_var, current_value,
                                       the_instr->owner());
                break;
              }
            case TREE_LOOP:
              {
                tree_loop *the_loop = (tree_loop *)the_node;
                replace_ivar_refs(old_var, the_loop->body(), the_sum_node);
                replace_ivar_refs(old_var, the_loop->test(), the_sum_node);
                break;
              }
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)the_node;

                operand lb_op = the_for->lb_op();
                lb_op.remove();
                lb_op = clone_replace_on_operand(lb_op, old_var,
                                                 current_value, the_for);
                the_for->set_lb_op(lb_op);

                operand ub_op = the_for->ub_op();
                ub_op.remove();
                ub_op = clone_replace_on_operand(ub_op, old_var,
                                                 current_value, the_for);
                the_for->set_ub_op(ub_op);

                operand step_op = the_for->step_op();
                step_op.remove();
                step_op = clone_replace_on_operand(step_op, old_var,
                                                   current_value, the_for);
                the_for->set_step_op(step_op);

                replace_ivar_refs(old_var, the_for->landing_pad(),
                                  the_sum_node);
                replace_ivar_refs(old_var, the_for->body(), the_sum_node);
                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)the_node;
                replace_ivar_refs(old_var, the_if->header(), the_sum_node);
                replace_ivar_refs(old_var, the_if->then_part(), the_sum_node);
                replace_ivar_refs(old_var, the_if->else_part(), the_sum_node);
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)the_node;
                replace_ivar_refs(old_var, the_block->body(), the_sum_node);
                break;
              }
            default:
                assert(FALSE);
          }
      }
  }

static void clone_replace_on_instr(instruction *the_instr, var_sym *to_replace,
                                   operand replacement, tree_node *place)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand source = the_instr->src_op(src_num);
        source.remove();
        source = clone_replace_on_operand(source, to_replace, replacement,
                                          place);
        the_instr->set_src_op(src_num, source);
      }

    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        immed value = the_ldc->value();
        if (value.is_symbol() && (value.symbol() == to_replace))
          {
            type_node *new_type = to_replace->type()->unqual();
            var_sym *new_var = to_replace->parent()->new_unique_var(new_type);
            new_var->reset_userdef();
            new_var->set_addr_taken();
            tree_node *assign_node = assign(new_var, replacement.clone());
            place->parent()->insert_before(assign_node, place->list_e());
            the_ldc->set_value(immed(new_var, value.offset()));
          }
      }
  }

static operand clone_replace_on_operand(operand the_operand,
                                        var_sym *to_replace,
                                        operand replacement, tree_node *place)
  {
    if (the_operand.is_expr())
      {
        instruction *this_instr = the_operand.instr();
        assert(this_instr != NULL);
        clone_replace_on_instr(this_instr, to_replace, replacement, place);
      }
    else if (the_operand.is_symbol())
      {
        sym_node *this_symbol = the_operand.symbol();
        if (this_symbol == to_replace)
            return replacement.clone();
      }
    return the_operand;
  }

static boolean contains_function_that_might_modify(instruction *the_instr,
                                                   var_sym *the_variable)
  {
    if (instr_is_impure_call(the_instr))
      {
        in_cal *the_call = (in_cal *)the_instr;

        operand address = the_call->addr_op();
        if (address.is_expr())
          {
            instruction *addr_instr = address.instr();
            assert(addr_instr != NULL);
            if (contains_function_that_might_modify(addr_instr, the_variable))
                return TRUE;
          }

        unsigned num_args = the_call->num_args();
        for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
          {
            operand this_arg = the_call->argument(arg_num);
            if (this_arg.is_expr())
	      {
                instruction *arg_instr = this_arg.instr();
                assert(arg_instr != NULL);
                if (arg_instr->opcode() == io_ldc)
                  {
                    in_ldc *the_ldc = (in_ldc *)arg_instr;
                    immed value = the_ldc->value();
                    if (value.is_symbol() && value.symbol()->is_var())
                      {
                        var_sym *value_var = (var_sym *)(value.symbol());
                        if (value_var->overlaps(the_variable))
                          {
                            if (call_modifies_arg(the_call, arg_num))
                                return TRUE;
                          }
                      }
                  }
                else if (contains_function_that_might_modify(arg_instr,
                                                             the_variable))
                  {
                    return TRUE;
                  }
              }
          }

        return FALSE;
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand source = the_instr->src_op(src_num);
        if (source.is_expr())
          {
            instruction *src_instr = source.instr();
            assert(src_instr != NULL);
            if (contains_function_that_might_modify(src_instr, the_variable))
                return TRUE;
          }
      }

    return FALSE;
  }

static boolean call_modifies_arg(in_cal *the_call, unsigned arg_num)
  {
    proc_sym *the_function = proc_for_call(the_call);

    if (the_function == NULL)
        return TRUE;

    func_type *the_type = the_function->type();
    assert(the_type != NULL);

    if (arg_num >= the_type->num_args())
        return TRUE;

    type_node *arg_type = the_type->arg_type(arg_num);
    assert(arg_type != NULL);
    arg_type = arg_type->unqual();
    if (!arg_type->is_ptr())
        return TRUE;

    ptr_type *the_pointer = (ptr_type *)arg_type;
    type_node *base_type = the_pointer->ref_type();
    assert(base_type != NULL);

    return (!base_type->is_const());
  }

static void fix_breaks(tree_for *the_for, var_sym *final_iteration_count)
  {
    fix_breaks(the_for->body(), the_for, final_iteration_count);
  }

static void fix_breaks(tree_node_list *the_list, tree_for *the_for,
                       var_sym *final_iteration_count)
  {
    tree_node_list_iter node_iter(the_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        if (this_node->is_block())
          {
            tree_block *this_block = (tree_block *)this_node;
            fix_breaks(this_block->body(), the_for, final_iteration_count);
            continue;
          }
        else if (!this_node->is_instr())
          {
            continue;
          }
        tree_instr *this_tree_instr = (tree_instr *)this_node;
        instruction *this_instr = this_tree_instr->instr();
        switch (this_instr->opcode())
          {
            case io_btrue:
            case io_bfalse:
            case io_jmp:
              {
                in_bj *this_bj = (in_bj *)this_instr;
                if (this_bj->target() == the_for->brklab())
                    fix_break(this_bj, the_for, final_iteration_count);
                break;
              }
            case io_mbr:
              {
                in_mbr *this_mbr = (in_mbr *)this_instr;
                if (this_mbr->default_lab() == the_for->brklab())
                  {
                    fix_break(this_mbr, the_for, final_iteration_count);
                    break;
                  }
                unsigned num_labs = this_mbr->num_labs();
                for (unsigned lab_num = 0; lab_num < num_labs; ++lab_num)
                  {
                    if (this_mbr->label(lab_num) == the_for->brklab())
                      {
                        fix_break(this_mbr, the_for, final_iteration_count);
                        break;
                      }
                  }
                break;
              }
            default:
                break;
          }
      }
  }

static void fix_break(instruction *break_instr, tree_for *the_for,
                      var_sym *final_iteration_count)
  {
    tree_node *assignment_node =
            assign(final_iteration_count, operand(the_for->index()) + 1);
    base_symtab *scope = the_for->scope();
    assert(scope->is_block());
    block_symtab *block_scope = (block_symtab *)scope;
    switch (break_instr->opcode())
      {
        case io_btrue:
        case io_bfalse:
          {
            in_bj *the_bj = (in_bj *)break_instr;
            label_sym *new_label = block_scope->new_unique_label();
            insert_after(new tree_instr(new in_lab(new_label)),
                         break_instr->owner());
            insert_after(new tree_instr(new in_bj(io_jmp, the_for->brklab())),
                         break_instr->owner());
            insert_after(assignment_node, break_instr->owner());
            the_bj->set_opcode((the_bj->opcode() == io_btrue) ? io_bfalse :
                               io_btrue);
            the_bj->set_target(new_label);
            break;
          }
        case io_jmp:
            insert_before(assignment_node, break_instr->owner());
            break;
        case io_mbr:
          {
            in_mbr *the_mbr = (in_mbr *)break_instr;
            label_sym *new_label = block_scope->new_unique_label();
            insert_after(new tree_instr(new in_bj(io_jmp, the_for->brklab())),
                         break_instr->owner());
            insert_after(assignment_node, break_instr->owner());
            insert_after(new tree_instr(new in_lab(new_label)),
                         break_instr->owner());
            if (the_mbr->default_lab() == the_for->brklab())
                the_mbr->set_default_lab(new_label);
            unsigned num_labs = the_mbr->num_labs();
            for (unsigned lab_num = 0; lab_num < num_labs; ++lab_num)
              {
                if (the_mbr->label(lab_num) == the_for->brklab())
                    the_mbr->set_label(lab_num, new_label);
              }
            break;
          }
        default:
            break;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
