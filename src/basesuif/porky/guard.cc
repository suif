/* file "guard.cc" */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to guard tree_for nodes for the porky program for SUIF */


#define RCS_BASE_FILE guard_cc

#include "porky.h"

RCS_BASE(
    "$Id: guard.cc,v 1.2 1999/08/25 03:27:29 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void guard_fors_on_object(suif_object *the_object);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void guard_fors_on_proc(tree_proc *the_proc)
  {
    walk(the_proc, &guard_fors_on_object);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void guard_fors_on_object(suif_object *the_object)
  {
    if (!the_object->is_tree_obj())
        return;
    tree_node *the_node = (tree_node *)the_object;
    if (!the_node->is_for())
        return;
    tree_for *the_for = (tree_for *)the_node;
    guard_for(the_for);
    the_for->parent()->insert_before(the_for->landing_pad(),
                                     the_for->list_e());
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
