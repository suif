/* file "uncbr.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to change uses of call-by-ref variables into uses of local
 * copies for the porky program for SUIF */


#define RCS_BASE_FILE uncbr_cc

#include "porky.h"
#include <string.h>

RCS_BASE(
    "$Id: uncbr.cc,v 1.2 1999/08/25 03:27:35 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void finalize_uncbr_on_node(tree_node *the_node, void *data);
static void spill_calls(instruction *the_instr);
static tree_node *guard_node(tree_node *inner_node, var_sym *test_var,
                             block_symtab *scope);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void uncbr_on_proc(tree_proc *the_proc)
  {
    tree_node_list *finalization_list = new tree_node_list;

    proc_symtab *proc_syms = the_proc->proc_syms();
    sym_node_list_e *follow_params = proc_syms->params()->head();

    boolean is_multi_entry;
    if (follow_params != NULL)
      {
        sym_node *this_sym = follow_params->contents;
        is_multi_entry = (strcmp(this_sym->name(), "n__") == 0);
      }

    while (follow_params != NULL)
      {
        sym_node *this_sym = follow_params->contents;
        assert(this_sym->is_var());
        var_sym *this_param = (var_sym *)this_sym;
        type_node *this_type = this_param->type();
        if (this_type->is_call_by_ref() && (this_type->size() != 0) &&
            (!this_type->unqual()->is_array()))
          {
            var_sym *new_var =
                proc_syms->new_unique_var(this_type, this_param->name());
            this_param->set_type(this_type->unqual());
            follow_params->contents = new_var;
            new_var->set_param();
            this_param->reset_param();

            in_rrr *top_cpy =
                    new in_rrr(io_cpy, this_type->unqual(),
                               operand(this_param), operand(new_var));
            tree_node *new_top_node = new tree_instr(top_cpy);
            if (is_multi_entry)
                new_top_node = guard_node(new_top_node, new_var, proc_syms);
            the_proc->body()->push(new_top_node);

            in_rrr *bottom_cpy =
                    new in_rrr(io_cpy, this_type->unqual(), operand(new_var),
                               operand(this_param));
            tree_node *new_bottom_node = new tree_instr(bottom_cpy);
            if (is_multi_entry)
              {
                new_bottom_node =
                        guard_node(new_bottom_node, new_var, proc_syms);
              }
            finalization_list->append(new_bottom_node);
          }

        follow_params = follow_params->next();
      }

    if (!finalization_list->is_empty())
        the_proc->map(&finalize_uncbr_on_node, finalization_list);

    delete finalization_list;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void finalize_uncbr_on_node(tree_node *the_node, void *data)
  {
    if (!the_node->is_instr())
        return;
    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();

    if (the_instr->opcode() != io_ret)
        return;

    spill_calls(the_instr);

    tree_node_list *finalization_list = (tree_node_list *)data;
    tree_node_list *new_list = finalization_list->clone();
    the_node->parent()->insert_before(new_list, the_node->list_e());
  }

static void spill_calls(instruction *the_instr)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_src = the_instr->src_op(src_num);
        if (this_src.is_expr())
          {
            instruction *this_instr = this_src.instr();
            if (instr_is_impure_call(this_instr))
              {
                this_src.remove();
                tree_node *owner = the_instr->owner();
                var_sym *temp_var =
                        owner->scope()->new_unique_var(
                                this_instr->result_type());
                temp_var->reset_addr_taken();
                temp_var->reset_userdef();
                this_instr->set_dst(operand(temp_var));
                the_instr->set_src_op(src_num, operand(temp_var));
                tree_instr *new_tree_instr = new tree_instr(this_instr);
                owner->parent()->insert_before(new_tree_instr,
                                               owner->list_e());
              }
            else
              {
                spill_calls(this_instr);
              }
          }
      }
  }

static tree_node *guard_node(tree_node *inner_node, var_sym *test_var,
                             block_symtab *scope)
  {
    label_sym *jumpto = scope->new_unique_label();
    tree_node_list *new_header = new tree_node_list;
    tree_node_list *new_then_part = new tree_node_list;
    tree_node_list *new_else_part = new tree_node_list;

    tree_if *new_if =
            new tree_if(jumpto, new_header, new_then_part, new_else_part);

    new_then_part->append(inner_node);

    ptr_type *the_ptr = test_var->type()->unqual()->ptr_to();
    in_ldc *null_ldc = new in_ldc(the_ptr, operand(), immed(0));
    in_ldc *var_ldc = new in_ldc(the_ptr, operand(), immed(test_var));
    test_var->set_addr_taken();
    in_rrr *the_sne =
            new in_rrr(io_seq, type_signed, operand(), operand(var_ldc),
                       operand(null_ldc));
    in_bj *the_branch = new in_bj(io_btrue, jumpto, operand(the_sne));
    tree_instr *branch_node = new tree_instr(the_branch);
    new_header->append(branch_node);

    return new_if;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
