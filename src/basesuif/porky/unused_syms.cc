/* file "unused_syms.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* elimination of unused symbols for the porky program for SUIF */

#define RCS_BASE_FILE unused_syms_cc

#include "porky.h"

RCS_BASE(
    "$Id: unused_syms.cc,v 1.2 1999/08/25 03:27:36 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_sym_unused;
static const char *k_porky_body_unused;
static unsigned num_syms_removed;
static unsigned num_types_removed;
static boolean remove_unused_syms;
static boolean remove_unused_types;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void node_unused_syms(tree_node *the_node);
static void node_list_unused_syms(tree_node_list *the_node_list);
static void ref_annotes(suif_object *the_object);
static void ref_symbol(sym_node *the_symbol);
static void ref_from_symbol(sym_node *the_symbol);
static void ref_type(type_node *the_type);
static void ref_from_type(type_node *the_type);
static void ref_operand(operand the_operand);
static void ref_instruction(instruction *the_instr);
static void ref_immed(immed the_immed);
static void mark_if_unremovable(sym_node *the_symbol);
static void remove_symbol(sym_node *the_symbol);
static void remove_type(type_node *the_type);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_unused(void)
  {
    ANNOTE(k_porky_sym_unused,  "porky sym unused",  FALSE);
    ANNOTE(k_porky_body_unused, "porky body unused", TRUE);
  }

extern void proc_unused_syms(tree_proc *the_proc, boolean for_syms,
                             boolean for_types)
  {
    num_syms_removed = 0;
    num_types_removed = 0;

    remove_unused_syms = for_syms;
    remove_unused_types = for_types;

    node_unused_syms(the_proc);

    if ((num_syms_removed > 0) || (num_types_removed  > 0))
      {
        made_progress = TRUE;

        if (verbosity_level >= 3)
          {
            printf("    Removed a total of ");
            if (num_syms_removed > 0)
              {
                printf("%u unused symbols ", num_syms_removed);
                if (num_types_removed > 0)
                    printf("and ");
              }
            if (num_types_removed > 0)
                printf("%u unused types ", num_types_removed);
            printf("from procedure `%s'.\n", the_proc->proc()->name());
          }
      }
  }

extern void unused_start_symtab(base_symtab *the_symtab, boolean for_syms,
                                boolean for_types)
  {
    if (for_syms)
      {
        sym_node_list_iter sym_iter(the_symtab->symbols());
        while (!sym_iter.is_empty())
          {
            sym_node *this_sym = sym_iter.step();
            this_sym->append_annote(k_porky_sym_unused);
          }
      }

    if (for_types)
      {
        type_node_list_iter type_iter(the_symtab->types());
        while (!type_iter.is_empty())
          {
            type_node *this_type = type_iter.step();
            /* type_void is used internally by the library when
             * creating mark instructions for I/O. */
            if (this_type != type_void)
                this_type->append_annote(k_porky_sym_unused);
          }
      }

    if (!for_syms)
      {
        sym_node_list_iter sym_iter(the_symtab->symbols());
        while (!sym_iter.is_empty())
          {
            sym_node *this_sym = sym_iter.step();
            ref_from_symbol(this_sym);
          }
      }

    var_def_list_iter def_iter(the_symtab->var_defs());
    while (!def_iter.is_empty())
      {
        var_def *this_def = def_iter.step();
        ref_annotes(this_def);
      }

    if (!for_types)
      {
        type_node_list_iter type_iter(the_symtab->types());
        while (!type_iter.is_empty())
          {
            type_node *this_type = type_iter.step();
            ref_from_type(this_type);
          }
      }

    if (the_symtab->is_proc())
      {
        proc_symtab *the_proc_symtab = (proc_symtab *)the_symtab;
        sym_node_list_iter param_iter(the_proc_symtab->params());
        while (!param_iter.is_empty())
          {
            sym_node *this_param = param_iter.step();
            ref_symbol(this_param);
          }
      }

    ref_annotes(the_symtab);

    if (the_symtab->is_file())
      {
        file_symtab *the_file_symtab = (file_symtab *)the_symtab;
        ref_annotes(the_file_symtab->fse());
      }
  }

extern void unused_end_symtab(base_symtab *the_symtab)
  {
    if (the_symtab->is_global())
      {
        num_syms_removed = 0;
        num_types_removed = 0;
      }

    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();

        if (this_sym->annotes()->peek_annote(k_porky_sym_unused) != NULL)
            mark_if_unremovable(this_sym);
      }

    sym_iter.reset(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->annotes()->peek_annote(k_porky_sym_unused) != NULL)
            remove_symbol(this_sym);
      }

    type_node_list_iter type_iter(the_symtab->types());
    while (!type_iter.is_empty())
      {
        type_node *this_type = type_iter.step();
        if (this_type->annotes()->peek_annote(k_porky_sym_unused) != NULL)
            remove_type(this_type);
      }

    if (the_symtab->is_global() &&
        ((num_syms_removed > 0) || (num_types_removed > 0)))
      {
        made_progress = TRUE;
        if (verbosity_level >= 3)
          {
            printf("    Removed a total of ");
            if (num_syms_removed > 0)
              {
                printf("%u unused symbols ", num_syms_removed);
                if (num_types_removed > 0)
                    printf("and ");
              }
            if (num_types_removed > 0)
                printf("%u unused types ", num_types_removed);
            printf("from ");
            if (the_symtab->is_file())
              {
                file_symtab *the_file_symtab = (file_symtab *)the_symtab;
                printf("file \"%s\"", the_file_symtab->fse()->name());
              }
            else
              {
                printf("inter-file");
              }
            printf(" symbol table.\n");
          }
      }
  }

extern boolean is_unused_procedure(proc_sym *the_proc)
  {
    return (the_proc->annotes()->peek_annote(k_porky_body_unused) != NULL);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void node_unused_syms(tree_node *the_node)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;

            ref_instruction(the_tree_instr->instr());

            break;
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;

            ref_symbol(the_loop->contlab());
            ref_symbol(the_loop->brklab());
            ref_symbol(the_loop->toplab());
            node_list_unused_syms(the_loop->body());
            node_list_unused_syms(the_loop->test());

            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;

            ref_symbol(the_for->index());
            ref_symbol(the_for->contlab());
            ref_symbol(the_for->brklab());
            node_list_unused_syms(the_for->landing_pad());
            node_list_unused_syms(the_for->body());
            ref_operand(the_for->lb_op());
            ref_operand(the_for->ub_op());
            ref_operand(the_for->step_op());

            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;

            ref_symbol(the_if->jumpto());
            node_list_unused_syms(the_if->header());
            node_list_unused_syms(the_if->then_part());
            node_list_unused_syms(the_if->else_part());

            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;

            unused_start_symtab(the_block->symtab(), remove_unused_syms,
                                remove_unused_types);
            ref_annotes(the_block);
            node_list_unused_syms(the_block->body());
            unused_end_symtab(the_block->symtab());

            return;
          }
        default:
            assert(FALSE);
      }

    ref_annotes(the_node);
  }

static void node_list_unused_syms(tree_node_list *the_node_list)
  {
    tree_node_list_iter node_iter(the_node_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        node_unused_syms(this_node);
      }
  }

static void ref_annotes(suif_object *the_object)
  {
    annote_list_iter annote_iter(the_object->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();
        immed_list *immeds = this_annote->immeds();
        if (immeds != NULL)
          {
            immed_list_iter immed_iter(immeds);
            while (!immed_iter.is_empty())
              {
                immed this_immed = immed_iter.step();
                ref_immed(this_immed);
              }
          }
      }
  }

static void ref_symbol(sym_node *the_symbol)
  {
    annote *unused_annote =
            the_symbol->annotes()->get_annote(k_porky_sym_unused);
    if (unused_annote != NULL)
      {
        delete unused_annote;
        ref_from_symbol(the_symbol);
      }
  }

static void ref_from_symbol(sym_node *the_symbol)
  {
    ref_annotes(the_symbol);
    if (the_symbol->is_var())
      {
        var_sym *the_var = (var_sym *)the_symbol;
        ref_type(the_var->type());
        if (the_var->parent_var() != NULL)
          {
            /* If this is the only child of the parent and the child
             * is of the same size and alignment, the parent can be
             * removed even if the child is used, if the parent itself
             * is never directly referenced.  Otherwise, the parent
             * cannot generally be removed if the child is needed. */
            var_sym *parent_var = the_var->parent_var();
            while ((parent_var->num_children() == 1) &&
                   (parent_var->type()->size() == the_var->type()->size()) &&
                   (get_alignment(parent_var->type()) ==
                    get_alignment(the_var->type())) &&
                   ((!parent_var->has_var_def()) ||
                    type_is_compatible_with_initializations(the_var->type(),
                            parent_var->definition())))
              {
                if (parent_var->has_var_def())
                    ref_annotes(parent_var->definition());
                parent_var = parent_var->parent_var();
                if (parent_var == NULL)
                    break;
              }
            if (parent_var != NULL)
                ref_symbol(parent_var);
          }
        if (the_var->has_var_def())
            ref_annotes(the_var->definition());
      }
    else if (the_symbol->is_proc())
      {
        proc_sym *the_proc = (proc_sym *)the_symbol;
        ref_type(the_proc->type());
      }
  }

static void ref_type(type_node *the_type)
  {
    annote *unused_annote =
            the_type->annotes()->get_annote(k_porky_sym_unused);
    if (unused_annote != NULL)
      {
        delete unused_annote;
        ref_from_type(the_type);
      }
  }

static void ref_from_type(type_node *the_type)
  {
    ref_annotes(the_type);
    switch (the_type->op())
      {
        case TYPE_PTR:
          {
            ptr_type *the_ptr = (ptr_type *)the_type;
            ref_type(the_ptr->ref_type());
            break;
          }
        case TYPE_ARRAY:
          {
            array_type *the_array = (array_type *)the_type;

            if (the_array->lower_bound().is_variable())
                ref_symbol(the_array->lower_bound().variable());

            if (the_array->upper_bound().is_variable())
                ref_symbol(the_array->upper_bound().variable());

            ref_type(the_array->elem_type());
            break;
          }
        case TYPE_FUNC:
          {
            func_type *the_func = (func_type *)the_type;

            ref_type(the_func->return_type());

            unsigned num_args = the_func->num_args();
            for (unsigned arg_num = 0; arg_num < num_args; ++arg_num)
                ref_type(the_func->arg_type(arg_num));

            break;
          }
        case TYPE_GROUP:
        case TYPE_STRUCT:
        case TYPE_UNION:
          {
            struct_type *the_struct = (struct_type *)the_type;

            unsigned num_fields = the_struct->num_fields();
            for (unsigned field_num = 0; field_num < num_fields; ++field_num)
                ref_type(the_struct->field_type(field_num));

            break;
          }
        case TYPE_CONST:
        case TYPE_VOLATILE:
        case TYPE_CALL_BY_REF:
        case TYPE_NULL:
          {
            modifier_type *the_modifier = (modifier_type *)the_type;
            ref_type(the_modifier->base());
            break;
          }
        default:
            break;
      }
  }

static void ref_operand(operand the_operand)
  {
    if (the_operand.is_symbol())
        ref_symbol(the_operand.symbol());
    else if (the_operand.is_expr())
        ref_instruction(the_operand.instr());
  }

static void ref_instruction(instruction *the_instr)
  {
    ref_annotes(the_instr);

    if (the_instr->dst_op().is_symbol())
        ref_symbol(the_instr->dst_op().symbol());

    ref_type(the_instr->result_type());

    switch (the_instr->format())
      {
        case inf_bj:
          {
            in_bj *the_bj = (in_bj *)the_instr;
            ref_symbol(the_bj->target());
            break;
          }
        case inf_ldc:
          {
            in_ldc *the_ldc = (in_ldc *)the_instr;
            ref_immed(the_ldc->value());
            break;
          }
        case inf_mbr:
          {
            in_mbr *the_mbr = (in_mbr *)the_instr;
            ref_symbol(the_mbr->default_lab());
            unsigned num_labs = the_mbr->num_labs();
            for (unsigned lab_num = 0; lab_num < num_labs; ++lab_num)
                ref_symbol(the_mbr->label(lab_num));
            break;
          }
        case inf_lab:
          {
            in_lab *the_lab = (in_lab *)the_instr;
            ref_symbol(the_lab->label());
            break;
          }
        default:
            break;
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
        ref_operand(the_instr->src_op(src_num));
  }

static void ref_immed(immed the_immed)
  {
    switch (the_immed.kind())
      {
        case im_symbol:
            ref_symbol(the_immed.symbol());
            return;
        case im_type:
            ref_type(the_immed.type());
            return;
        case im_op:
            ref_operand(the_immed.op());
            return;
        case im_instr:
            ref_instruction(the_immed.instr());
            return;
        default:
            return;
      }
  }

static void mark_if_unremovable(sym_node *the_symbol)
  {
    if (the_symbol->is_proc())
      {
        proc_sym *the_proc = (proc_sym *)the_symbol;
        if (the_proc->file() != NULL)
          {
            if (unreferenced_outside_fileset(the_proc))
              {
                if (the_proc->peek_annote(k_porky_body_unused) == NULL)
                  {
                    if (verbosity_level >= 3)
                      {
                        printf("    Procedure `%s' is unused but the body "
                               "has been written,\n", the_proc->name());
                        printf("    so it will not be removed, just "
                               "marked.\n");
                        printf("    Rerun `%s -unused-syms' to remove it.\n\n",
                               _suif_prog_base_name);
                      }

                    the_proc->append_annote(k_porky_body_unused);
                    ref_symbol(the_symbol);
                  }
              }
            else
              {
                ref_symbol(the_symbol);
              }
          }
      }
    else if (the_symbol->is_var() &&
             (!unreferenced_outside_fileset(the_symbol)))
      {
        var_sym *this_var = (var_sym *)the_symbol;

        /* We cannot remove the definition if the variable might be
         * referenced outside the fileset. */
        if (this_var->has_var_def())
            ref_symbol(the_symbol);
      }
  }

static void remove_symbol(sym_node *the_symbol)
  {
    ++num_syms_removed;
    if (verbosity_level >= 4)
        printf("      Removing unused symbol `%s'.\n", the_symbol->name());

    if (the_symbol->is_var())
      {
        var_sym *this_var = (var_sym *)the_symbol;

        var_sym *heir = NULL;
        if ((this_var->num_children() == 1) &&
            (this_var->type()->size() ==
             this_var->child_var(0)->type()->size()) &&
            (get_alignment(this_var->type()) ==
             get_alignment(this_var->child_var(0)->type())))
          {
            heir = this_var->child_var(0);
          }

        while (this_var->num_children() > 0)
            this_var->remove_child(this_var->child_var(0));

        if (this_var->parent_var() != NULL)
            this_var->parent_var()->remove_child(this_var);

        if (this_var->has_var_def())
          {
            var_def *the_def = this_var->definition();
            if (heir == NULL)
              {
                the_def->parent()->remove_def(the_def);
                delete the_def;
              }
            else
              {
                the_def->set_variable(heir);
              }
          }
      }
    the_symbol->parent()->remove_sym(the_symbol);
    delete the_symbol;
  }

static void remove_type(type_node *the_type)
  {
    ++num_types_removed;
    if (verbosity_level >= 4)
      {
        printf("      Removing unused type `");
        the_type->print();
        printf("'.\n");
      }

    the_type->parent()->remove_type(the_type);
    delete the_type;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
