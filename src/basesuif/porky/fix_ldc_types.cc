/* file "fix_ldc_types.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE fix_ldc_types_cc

#include "porky.h"

RCS_BASE(
    "$Id: fix_ldc_types.cc,v 1.2 1999/08/25 03:27:28 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void fix_node_ldc_types(tree_node *the_node, void *);
static void fix_instr_ldc_types(instruction *the_instr, void *);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void fix_proc_ldc_types(tree_proc *the_proc)
  {
    the_proc->body()->map(&fix_node_ldc_types, NULL);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void fix_node_ldc_types(tree_node *the_node, void *)
  {
    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    the_tree_instr->instr_map(&fix_instr_ldc_types, NULL);
  }

static void fix_instr_ldc_types(instruction *the_instr, void *)
  {
    if (the_instr->opcode() != io_ldc)
        return;

    in_ldc *the_ldc = (in_ldc *)the_instr;
    if (the_ldc->peek_annote(k_fields) != NULL)
        return;

    immed value = the_ldc->value();
    if (!value.is_symbol())
        return;

    type_node *return_type = the_ldc->result_type()->unqual();
    if (return_type->op() != TYPE_PTR)
      {
        error_line(1, the_instr->parent(),
                   "non-pointer type for symbolic address");
      }

    if (value.offset() != 0)
        return;

    sym_node *symbol = value.symbol();
    type_node *sym_type;
    if (symbol->is_var())
      {
        var_sym *the_var = (var_sym *)symbol;
        sym_type = the_var->type();
      }
    else
      {
        if (!symbol->is_proc())
            error_line(1, the_instr->parent(), "ldc of label");
        proc_sym *the_proc = (proc_sym *)symbol;
        sym_type = the_proc->type();
      }

    ptr_type *return_ptr = (ptr_type *)return_type;
    if (return_ptr->ref_type() != sym_type)
        the_ldc->set_result_type(sym_type->ptr_to());
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
