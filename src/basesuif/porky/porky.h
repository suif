/* file "porky.h" */

/*  Copyright (c) 1994,95 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* include file for the porky program for SUIF */

#ifndef PORKY_H
#define PORKY_H

#include <suif1.h>
#include <useful.h>

RCS_HEADER(porky_h,
     "$Id: porky.h,v 1.2 1999/08/25 03:27:33 brm Exp $")


/*----------------------------------------------------------------------*
    Begin Global Variables
 *----------------------------------------------------------------------*/

extern int verbosity_level;
extern boolean strength_reduce;
extern boolean made_progress;
extern boolean merge_globals;
extern boolean fast_structured_facts;
extern boolean cse_no_pointers;
extern int max_gele_split_depth;

/*----------------------------------------------------------------------*
    End Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

typedef enum
  {
    NO_CALLS,
    CALLS_TOTALLY_ORDERED,
    CALLS_NOT_TOTALLY_ORDERED
  } call_order;

typedef struct goto_data goto_data;

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    in file "main.cc":
 *----------------------------------------------------------------------*/

extern void process_node(tree_node *the_node, void *);

/*----------------------------------------------------------------------*
    in file "utils.cc":
 *----------------------------------------------------------------------*/

extern void insert_tree_node_list_before(tree_node_list *to_insert,
                                         tree_node *before);
extern void append_tree_node_to_list(tree_node_list *the_list,
                                     tree_node *the_node);
extern tree_node *last_action_node(tree_node_list *the_node_list);
extern tree_node *last_action_before(tree_node_list *the_node_list,
                                     tree_node *the_node);

/*----------------------------------------------------------------------*
    in file "codegen.cc":
 *----------------------------------------------------------------------*/

extern operand operand_multiply(type_node *the_type, operand arg1,
                                operand arg2);
extern operand operand_add(type_node *the_type, operand arg1, operand arg2);
extern operand operand_sub(type_node *the_type, operand arg1, operand arg2);
extern operand operand_lsl(operand arg1, operand arg2);
extern operand operand_neg(type_node *the_type, operand arg);
extern operand operand_div(type_node *the_type, operand arg1, operand arg2);
extern operand operand_rrr(type_node *the_type, if_ops opcode, operand arg1,
                           operand arg2);
extern operand operand_mul_by_const(type_node *the_type, operand arg1,
                                    int arg2);
extern operand operand_add_const(type_node *the_type, operand arg1, int arg2);
extern operand operand_sub_const(type_node *the_type, operand arg1, int arg2);
extern operand operand_div_by_const(type_node *the_type, operand arg1,
                                    int arg2);
extern operand operand_int(type_node *the_type, int the_int);
extern tree_if *create_if(operand test, tree_node_list *then_part,
                          tree_node_list *else_part, base_symtab *scope);
extern boolean try_constant_operation(type_node *the_type, if_ops opcode,
                                      int source_1, int source_2, int *result);
extern boolean operand_is_zero(operand the_operand);
extern boolean operand_is_int_const(operand the_operand);
extern boolean int_const_from_operand(operand the_operand, int *value);
extern void replace_instruction_with_operand(instruction *old,
                                             operand new_operand);

/*----------------------------------------------------------------------*
    in file "gotos.cc":
 *----------------------------------------------------------------------*/

extern void calculate_goto_information(tree_node *the_node);
extern boolean node_is_bad(tree_node *the_node);
extern boolean node_contains_goto(tree_node *the_node);
extern boolean node_has_incoming_goto(tree_node *the_node);
extern goto_data *goto_data_from_node(tree_node *the_node);
extern void set_node_goto_data(goto_data *the_data, tree_node *the_node);

/*----------------------------------------------------------------------*
    in file "dismantle.cc":
 *----------------------------------------------------------------------*/

extern void dismantle_for(tree_for *the_for);
extern void dismantle_for_ops_reevaluated(tree_for *the_for);
extern void dismantle_instr(instruction *the_instr);
extern operand dismantle_array(in_array *the_array);
extern void dismantle_mbr(in_mbr *the_mbr);
extern boolean if_test_must_be_true(tree_if *the_if);
extern boolean if_test_must_be_false(tree_if *the_if);
extern boolean for_must_execute(tree_for *the_for);
extern boolean for_must_not_execute(tree_for *the_for);
extern void dismantle_c_gele(tree_for *the_for);
extern void split_fortran_gele(tree_for *the_for, tree_for **left = NULL,
                               tree_for **right = NULL);

/*----------------------------------------------------------------------*
    in file "reassociate.cc":
 *----------------------------------------------------------------------*/

extern void try_reassociation(operand the_operand);
extern boolean linear_form(operand the_operand, var_sym *var_x, operand *op_a,
                           operand *op_b);

/*----------------------------------------------------------------------*
    in file "expand_block.cc":
 *----------------------------------------------------------------------*/

extern boolean block_symtab_empty(tree_block *the_block);

/*----------------------------------------------------------------------*
    in file "clean_bad.cc":
 *----------------------------------------------------------------------*/

extern void clean_bad_nodes(proc_sym *proc, boolean fancy);

/*----------------------------------------------------------------------*
    in file "control_flow.cc":
 *----------------------------------------------------------------------*/

extern void control_flow_prune(tree_node *the_node);

/*----------------------------------------------------------------------*
    in file "propagate.cc":
 *----------------------------------------------------------------------*/

enum forward_prop_kind { FPK_ALL, FPK_SIMPLE_LOCAL_VARS };

extern void forward_propagate(tree_node *the_node,
                              forward_prop_kind which_kind);
extern boolean instr_might_store(instruction *the_instr, var_sym *the_var);

/*----------------------------------------------------------------------*
    in file "spill_calls.cc":
 *----------------------------------------------------------------------*/

extern void spill_calls(tree_node *the_node);

/*----------------------------------------------------------------------*
    in file "for_semantics.cc":
 *----------------------------------------------------------------------*/

extern void change_semantics(tree_node *the_node);

/*----------------------------------------------------------------------*
    in file "constants.cc":
 *----------------------------------------------------------------------*/

extern void init_propagate_constants(void);
extern void propagate_constants(tree_node *the_node);

/*----------------------------------------------------------------------*
    in file "ivar.cc":
 *----------------------------------------------------------------------*/

extern void find_ivars(tree_node *the_node);
extern void find_reductions(tree_node *the_node);
extern boolean is_loop_constant(tree_for *the_for, operand the_operand);

/*
 *  Returns the following:
 *      0 - no writes possible
 *      1 - exactly one write, possibly nested within for loops,
 *          ``where'' points to it
 *      2 - possible writes, but not necessarily one
 */
extern int possible_writes(tree_node_list *the_node_list,
                           var_sym *the_variable, instruction **where);

/*----------------------------------------------------------------------*
    in file "fold.cc":
 *----------------------------------------------------------------------*/

extern void fold_all_constants(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "loopinfo.cc":
 *----------------------------------------------------------------------*/

extern void mark_loop_mod_ref(tree_node *the_node);
extern void initialize_mod_ref(void);

/*----------------------------------------------------------------------*
    in file "private.cc":
 *----------------------------------------------------------------------*/

extern void init_privatization(void);
extern void do_privatization(tree_node *the_node);

/*----------------------------------------------------------------------*
    in file "fix_ldc_types.cc":
 *----------------------------------------------------------------------*/

extern void fix_proc_ldc_types(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "scalarize.cc":
 *----------------------------------------------------------------------*/

extern void init_scalarization(void);
extern void scalarize_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "know_bounds.cc":
 *----------------------------------------------------------------------*/

extern void proc_know_bounds(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "no_struct_copy.cc":
 *----------------------------------------------------------------------*/

extern void proc_no_struct_copy(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "no_sub_vars.cc":
 *----------------------------------------------------------------------*/

extern void proc_no_sub_vars(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "cse.cc":
 *----------------------------------------------------------------------*/

extern void proc_cse(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "dead_code.cc":
 *----------------------------------------------------------------------*/

extern void init_dead_code(void);
extern void proc_dead_code(tree_proc *the_proc);
extern void proc_dead_code0(tree_proc *the_proc);
extern void symtab_mark_bound_vars(base_symtab *the_symtab);

/*----------------------------------------------------------------------*
    in file "unused_syms.cc":
 *----------------------------------------------------------------------*/

extern void init_unused(void);
extern void proc_unused_syms(tree_proc *the_proc, boolean for_syms,
                             boolean for_types);
extern void unused_start_symtab(base_symtab *the_symtab, boolean for_syms,
                                boolean for_types);
extern void unused_end_symtab(base_symtab *the_symtab);
extern boolean is_unused_procedure(proc_sym *the_proc);

/*----------------------------------------------------------------------*
    in file "loop_invariants.cc":
 *----------------------------------------------------------------------*/

extern void proc_loop_invariants(tree_proc *the_proc, boolean fred_only);

/*----------------------------------------------------------------------*
    in file "call_query.cc":
 *----------------------------------------------------------------------*/

/*
 *  This function is used to tell whether or not the impure function
 *  call instructions and io_gen instructions in an expression tree
 *  form a perfect ordering in terms of the instruction tree.  The
 *  reason we want to know this is that if they do form a perfect
 *  order, we are guaranteed that the children of the lowest level
 *  call instruction will be evaluated before any calls are made.
 *  Otherwise, we cannot know that any particular instructions in the
 *  entire tree will be executed before all calls; there will always
 *  be legal orders of evaluation that evaluate some call before any
 *  given instruction.
 */
extern call_order call_query(instruction *the_instr);

/*----------------------------------------------------------------------*
    in file "bitpack.cc":
 *----------------------------------------------------------------------*/

extern void init_bitpack(void);
extern void bit_pack(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "if_hoist.cc":
 *----------------------------------------------------------------------*/

extern void init_if_hoist(void);
extern void if_hoist_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "structure.cc":
 *----------------------------------------------------------------------*/

extern void find_fors_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "globalize.cc":
 *----------------------------------------------------------------------*/

extern void init_globalize(void);
extern void globalize_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "glob_priv.cc":
 *----------------------------------------------------------------------*/

extern void init_glob_priv(void);
extern void glob_priv_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "build_arefs.cc":
 *----------------------------------------------------------------------*/

extern void build_arefs_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "for_normalize.cc":
 *----------------------------------------------------------------------*/

extern void normalize_fors_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "ucf_opt.cc":
 *----------------------------------------------------------------------*/

extern void init_ucf_opt(void);
extern void optimize_ucf_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "uncbr.cc":
 *----------------------------------------------------------------------*/

extern void uncbr_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "loop_conditionals.cc":
 *----------------------------------------------------------------------*/

extern void init_loop_conditionals(void);
extern void loop_conditionals_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "copy_form.cc":
 *----------------------------------------------------------------------*/

extern void to_copy_form_on_proc(tree_proc *the_proc);
extern void from_copy_form_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "reuse.cc":
 *----------------------------------------------------------------------*/

extern boolean reuse_desired(operand the_op);

/*----------------------------------------------------------------------*
    in file "array_globalize.cc":
 *----------------------------------------------------------------------*/

extern void array_globalize_on_symtab(base_symtab *the_symtab,
                                      int array_glob_size, boolean do_autos);
extern void array_globalize_on_proc(tree_proc *the_proc, int array_glob_size,
                                    boolean do_autos);

/*----------------------------------------------------------------------*
    in file "guard.cc":
 *----------------------------------------------------------------------*/

extern void guard_fors_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "child_scalarize.cc":
 *----------------------------------------------------------------------*/

extern void child_scalarize_proc(tree_proc *the_proc, boolean aggressive);

/*----------------------------------------------------------------------*
    in file "nest.cc":
 *----------------------------------------------------------------------*/

extern void init_nest(void);
extern void nest_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "delinearize.cc":
 *----------------------------------------------------------------------*/

extern void delinearize_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "form_arrays.cc":
 *----------------------------------------------------------------------*/

extern void form_arrays_on_symtab(global_symtab *the_symtab);
extern void form_arrays_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "chain_arefs.cc":
 *----------------------------------------------------------------------*/

extern void chain_arefs_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "form_all_arrays.cc":
 *----------------------------------------------------------------------*/

extern void init_form_all_arrays(void);
extern void form_all_arrays_on_symtab(global_symtab *the_symtab);
extern void form_all_arrays_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "extract.cc":
 *----------------------------------------------------------------------*/

extern void ub_from_var_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "breakup.cc":
 *----------------------------------------------------------------------*/

extern void breakup_on_proc(tree_proc *the_proc, i_integer limit_size);

/*----------------------------------------------------------------------*
    in file "mark_constants.cc":
 *----------------------------------------------------------------------*/

extern void init_mark_constants(void);
extern void mark_constants_on_proc(tree_proc *the_proc);
extern void mark_constants_start_symtab(base_symtab *the_symtab);

/*----------------------------------------------------------------------*
    in file "addr_taken.cc":
 *----------------------------------------------------------------------*/

extern void fix_addr_taken_start_symtab(base_symtab *the_symtab);
extern void fix_addr_taken_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "no_ldc_sym_offsets.cc":
 *----------------------------------------------------------------------*/

extern void no_ldc_sym_offsets_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "only_simple_var_ops.cc":
 *----------------------------------------------------------------------*/

extern void only_simple_var_ops_on_proc(tree_proc *the_proc);

/*----------------------------------------------------------------------*
    in file "kill_enum.cc":
 *----------------------------------------------------------------------*/

extern void init_kill_enum(void);
extern void kill_enum_on_proc(tree_proc *the_proc);
extern void kill_enum_on_symtab(base_symtab *the_symtab);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/

#endif
