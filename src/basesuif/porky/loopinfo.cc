/* file "loopinfo.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE loopinfo_cc

#include "porky.h"

RCS_BASE(
    "$Id: loopinfo.cc,v 1.2 1999/08/25 03:27:31 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Variable Declarations
 *----------------------------------------------------------------------*/

static const char *k_loop_mod_syms;
static const char *k_loop_ref_syms;
static const char *k_loop_privatizable_syms;
static const char *k_call_mod_syms;
static const char *k_call_ref_syms;

/*----------------------------------------------------------------------*
    End Private Variable Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void mark_mod_ref_on_node(tree_node *the_node, void *);
static void mark_mod_ref_on_for(tree_for *the_for);
static void add_mod_refs_for_node_list(tree_node_list *node_list,
                                       var_sym_list *mod_list,
                                       var_sym_list *ref_list,
                                       var_sym_list *privatizable_list);
static void add_mod_refs_for_node(tree_node *the_node, var_sym_list *mod_list,
                                  var_sym_list *ref_list,
                                  var_sym_list *privatizable_list);
static void add_var_to_list(var_sym_list *sym_list, var_sym *the_var);
static boolean var_on_list(var_sym_list *sym_list, var_sym *the_var);
static void add_mod_refs_for_instr(instruction *the_instr,
                                   var_sym_list *mod_list,
                                   var_sym_list *ref_list,
                                   var_sym_list *privatizable_list,
                                   boolean in_load, boolean in_store);
static void add_mod_refs_for_operand(operand the_operand,
                                     var_sym_list *mod_list,
                                     var_sym_list *ref_list,
                                     var_sym_list *privatizable_list,
                                     boolean in_load, boolean in_store);
static boolean no_irregular_control_flow(tree_node_list *the_node_list);
static boolean irregular_control_flow(tree_node_list *the_node_list,
                                      label_sym *allowed_label);
static boolean outside_scope(tree_node *the_node, sym_node *the_symbol);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void mark_loop_mod_ref(tree_node *the_node)
  {
    if (the_node->kind() != TREE_BLOCK)
        return;

    tree_block *the_block = (tree_block *)the_node;
    the_block->map(&mark_mod_ref_on_node, NULL);
  }

extern void initialize_mod_ref(void)
  {
    ANNOTE(k_loop_mod_syms,          "loop mod syms",          TRUE);
    ANNOTE(k_loop_ref_syms,          "loop ref syms",          TRUE);
    ANNOTE(k_loop_privatizable_syms, "loop privatizable syms", TRUE);
    ANNOTE(k_call_mod_syms,          "call mod syms",          TRUE);
    ANNOTE(k_call_ref_syms,          "call ref syms",          TRUE);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void mark_mod_ref_on_node(tree_node *the_node, void *)
  {
    if (!the_node->is_for())
        return;

    tree_for *the_for = (tree_for *)the_node;
    mark_mod_ref_on_for(the_for);
  }

static void mark_mod_ref_on_for(tree_for *the_for)
  {
    assert(the_for != NULL);

    immed_list *ref_immeds =
            (immed_list *)(the_for->peek_annote(k_loop_ref_syms));
    immed_list *mod_immeds =
            (immed_list *)(the_for->peek_annote(k_loop_mod_syms));
    immed_list *privatizable_immeds =
            (immed_list *)(the_for->peek_annote(k_loop_privatizable_syms));

    if ((ref_immeds != NULL) || (mod_immeds != NULL) ||
        (privatizable_immeds != NULL))
      {
        return;
      }

    var_sym_list *mod_list = new var_sym_list;
    var_sym_list *ref_list = new var_sym_list;
    var_sym_list *privatizable_list = new var_sym_list;
    add_mod_refs_for_node_list(the_for->body(), mod_list, ref_list,
                               privatizable_list);

    ref_immeds = new immed_list;
    mod_immeds = new immed_list;
    privatizable_immeds = new immed_list;

    var_sym_list_iter mod_iter(mod_list);
    while (!mod_iter.is_empty())
      {
        var_sym *this_var = mod_iter.step();
        assert(this_var != NULL);
        if (!outside_scope(the_for, this_var))
            mod_immeds->append(immed(this_var));
      }
    delete mod_list;

    var_sym_list_iter ref_iter(ref_list);
    while (!ref_iter.is_empty())
      {
        var_sym *this_var = ref_iter.step();
        assert(this_var != NULL);
        if (!outside_scope(the_for, this_var))
            ref_immeds->append(immed(this_var));
      }
    delete ref_list;

    if (no_irregular_control_flow(the_for->body()) &&
        (!node_has_incoming_goto(the_for)))
      {
        var_sym_list_iter privatizable_iter(privatizable_list);
        while (!privatizable_iter.is_empty())
          {
            var_sym *this_var = privatizable_iter.step();
            assert(this_var != NULL);
            if (!outside_scope(the_for, this_var))
                privatizable_immeds->append(immed(this_var));
          }
      }
    delete privatizable_list;

    the_for->append_annote(k_loop_ref_syms, ref_immeds);
    the_for->append_annote(k_loop_mod_syms, mod_immeds);
    the_for->append_annote(k_loop_privatizable_syms, privatizable_immeds);
  }

static void add_mod_refs_for_node_list(tree_node_list *node_list,
                                       var_sym_list *mod_list,
                                       var_sym_list *ref_list,
                                       var_sym_list *privatizable_list)
  {
    assert(node_list != NULL);
    tree_node_list_iter the_iter(node_list);
    while(!the_iter.is_empty())
      {
        tree_node *this_node = the_iter.step();
        assert(this_node != NULL);
        add_mod_refs_for_node(this_node, mod_list, ref_list,
                              privatizable_list);
      }
  }

static void add_mod_refs_for_node(tree_node *the_node, var_sym_list *mod_list,
                                  var_sym_list *ref_list,
                                  var_sym_list *privatizable_list)
  {
    assert(the_node != NULL);
    assert(mod_list != NULL);
    assert(ref_list != NULL);
    assert(privatizable_list != NULL);
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            assert(the_instr != NULL);

            add_mod_refs_for_instr(the_instr, mod_list, ref_list,
                                   privatizable_list, TRUE, TRUE);

            operand destination = the_instr->dst_op();
            if (destination.is_symbol())
              {
                var_sym *the_var = destination.symbol();
                assert(the_var != NULL);
                add_var_to_list(mod_list, the_var);
                if (!var_on_list(ref_list, the_var))
                    add_var_to_list(privatizable_list, the_var);
              }
            break;
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            add_mod_refs_for_node_list(the_loop->body(), mod_list, ref_list,
                                       privatizable_list);
            add_mod_refs_for_node_list(the_loop->test(), mod_list, ref_list,
                                       privatizable_list);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            mark_mod_ref_on_for(the_for);

            add_var_to_list(mod_list, the_for->index());
            if (!var_on_list(ref_list, the_for->index()))
                add_var_to_list(privatizable_list, the_for->index());

            add_mod_refs_for_node_list(the_for->landing_pad(), mod_list,
                                       ref_list, privatizable_list);

            immed_list *ref_immeds =
                    (immed_list *)(the_for->peek_annote(k_loop_ref_syms));
            immed_list *mod_immeds =
                    (immed_list *)(the_for->peek_annote(k_loop_mod_syms));
            immed_list *privatizable_immeds =
                    (immed_list *)(the_for->peek_annote(
                                       k_loop_privatizable_syms));

            if (ref_immeds != NULL)
              {
                immed_list_iter the_iter(ref_immeds);
                while (!the_iter.is_empty())
                  {
                    immed value = the_iter.step();
                    if (value.is_symbol())
                      {
                        sym_node *the_symbol = value.symbol();
                        assert(the_symbol != NULL);
                        if (the_symbol->is_var())
                          {
                            var_sym *the_var = (var_sym *)the_symbol;
                            add_var_to_list(ref_list, the_var);
                          }
                      }
                  }
              }

            if (mod_immeds != NULL)
              {
                immed_list_iter the_iter(mod_immeds);
                while (!the_iter.is_empty())
                  {
                    immed value = the_iter.step();
                    if (value.is_symbol())
                      {
                        sym_node *the_symbol = value.symbol();
                        assert(the_symbol != NULL);
                        if (the_symbol->is_var())
                          {
                            var_sym *the_var = (var_sym *)the_symbol;
                            add_var_to_list(mod_list, the_var);
                          }
                      }
                  }
              }

            if (privatizable_immeds != NULL)
              {
                immed_list_iter the_iter(privatizable_immeds);
                while (!the_iter.is_empty())
                  {
                    immed value = the_iter.step();
                    if (value.is_symbol())
                      {
                        sym_node *the_symbol = value.symbol();
                        assert(the_symbol != NULL);
                        if (the_symbol->is_var())
                          {
                            var_sym *the_var = (var_sym *)the_symbol;
                            add_var_to_list(privatizable_list, the_var);
                          }
                      }
                  }
              }

            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;
            add_mod_refs_for_node_list(the_if->header(), mod_list, ref_list,
                                       privatizable_list);
            var_sym_list *then_privatizable = new var_sym_list;
            var_sym_list *else_privatizable = new var_sym_list;
            var_sym_list *then_ref = new var_sym_list;
            var_sym_list *else_ref = new var_sym_list;

            add_mod_refs_for_node_list(the_if->then_part(), mod_list,
                                       then_ref, then_privatizable);
            add_mod_refs_for_node_list(the_if->else_part(), mod_list,
                                       else_ref, else_privatizable);

            var_sym_list_iter then_iter(then_privatizable);
            while (!then_iter.is_empty())
              {
                var_sym *this_var = then_iter.step();

                if (var_on_list(privatizable_list, this_var))
                    continue;

                if (var_on_list(ref_list, this_var))
                    continue;

                if (var_on_list(else_privatizable, this_var) ||
                    (!var_on_list(else_ref, this_var)))
                  {
                    add_var_to_list(privatizable_list, this_var);
                  }
              }

            var_sym_list_iter else_iter(else_privatizable);
            while (!else_iter.is_empty())
              {
                var_sym *this_var = else_iter.step();

                if (var_on_list(privatizable_list, this_var))
                    continue;

                if (var_on_list(ref_list, this_var))
                    continue;

                if (!var_on_list(then_ref, this_var))
                    add_var_to_list(privatizable_list, this_var);
              }

            var_sym_list_iter then_ref_iter(then_ref);
            while (!then_ref_iter.is_empty())
              {
                var_sym *this_var = then_ref_iter.step();
                add_var_to_list(ref_list, this_var);
              }

            var_sym_list_iter else_ref_iter(else_ref);
            while (!else_ref_iter.is_empty())
              {
                var_sym *this_var = else_ref_iter.step();
                add_var_to_list(ref_list, this_var);
              }

            delete then_privatizable;
            delete else_privatizable;
            delete then_ref;
            delete else_ref;

            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            add_mod_refs_for_node_list(the_block->body(), mod_list, ref_list,
                                       privatizable_list);
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void add_var_to_list(var_sym_list *sym_list, var_sym *the_var)
  {
    if (sym_list->lookup(the_var) != NULL)
        return;

    sym_list->append(the_var);
  }

static boolean var_on_list(var_sym_list *sym_list, var_sym *the_var)
  {
    return (sym_list->lookup(the_var) != NULL);
  }

static void add_mod_refs_for_instr(instruction *the_instr,
                                   var_sym_list *mod_list,
                                   var_sym_list *ref_list,
                                   var_sym_list *privatizable_list,
                                   boolean in_load, boolean in_store)
  {
    assert(the_instr != NULL);
    assert(mod_list != NULL);
    assert(ref_list != NULL);
    assert(privatizable_list != NULL);

    if (the_instr->opcode() == io_cal)
      {
        in_cal *the_call = (in_cal *)the_instr;
        add_mod_refs_for_operand(the_call->addr_op(), mod_list, ref_list,
                                 privatizable_list, TRUE, FALSE);

        immed_list *call_mods =
                (immed_list *)(the_call->peek_annote(k_call_mod_syms));
        immed_list *call_refs =
                (immed_list *)(the_call->peek_annote(k_call_ref_syms));

        boolean pure_function = FALSE;
        proc_sym *call_sym = proc_for_call(the_call);
        if (call_sym != NULL)
          {
            pure_function =
                    (call_sym->annotes()->peek_annote(k_pure_function) !=
                     NULL);
          }

        int num_args = the_call->num_args();
        for (int arg_num = 0; arg_num < num_args; ++arg_num)
          {
            operand this_arg = the_call->argument(arg_num);
            boolean handled = FALSE;

            boolean arg_in_load = TRUE;
            boolean arg_in_store = TRUE;

            if (call_mods != NULL)
              {
                if (this_arg.is_symbol())
                    handled = TRUE;
                arg_in_load = FALSE;
                arg_in_store = FALSE;
              }
            else if (pure_function)
              {
                arg_in_store = FALSE;
              }

            if (!handled)
              {
                add_mod_refs_for_operand(this_arg, mod_list, ref_list,
                                         privatizable_list, arg_in_load,
                                         arg_in_store);
              }
          }

        if (call_refs != NULL)
          {
            immed_list_iter the_iter(call_refs);
            while (!the_iter.is_empty())
              {
                immed value = the_iter.step();
                if (value.is_symbol())
                  {
                    sym_node *the_symbol = value.symbol();
                    assert(the_symbol != NULL);
                    if (the_symbol->is_var())
                      {
                        var_sym *the_var = (var_sym *)the_symbol;
                        add_var_to_list(ref_list, the_var);
                      }
                  }
              }
          }

        if (call_mods != NULL)
          {
            immed_list_iter the_iter(call_mods);
            while (!the_iter.is_empty())
              {
                immed value = the_iter.step();
                if (value.is_symbol())
                  {
                    sym_node *the_symbol = value.symbol();
                    assert(the_symbol != NULL);
                    if (the_symbol->is_var())
                      {
                        var_sym *the_var = (var_sym *)the_symbol;
                        add_var_to_list(mod_list, the_var);
                        if (!var_on_list(ref_list, the_var))
                            add_var_to_list(privatizable_list, the_var);
                      }
                  }
              }
          }
      }
    else if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        immed value = the_ldc->value();
        if (value.is_symbol())
          {
            sym_node *the_symbol = value.symbol();
            assert(the_symbol != NULL);
            if (the_symbol->is_var())
              {
                var_sym *the_var = (var_sym *)the_symbol;
                if (in_store)
                    add_var_to_list(mod_list, the_var);
                if (in_load)
                    add_var_to_list(ref_list, the_var);
              }
          }
      }
    else
      {
        boolean op_in_load = in_load;
        boolean op_in_store = in_store;

        int num_srcs = the_instr->num_srcs();
        for (int src_num = 0; src_num < num_srcs; ++src_num)
          {
            switch (the_instr->opcode())
              {
                case io_lod:
                    op_in_load = TRUE;
                    op_in_store = FALSE;
                    break;
                case io_str:
                    if (src_num == 0)
                        op_in_store = TRUE;
                    else
                        op_in_store = FALSE;
                    op_in_load = FALSE;
                    break;
                case io_memcpy:
                    if (src_num == 0)
                      {
                        op_in_store = TRUE;
                        op_in_load = FALSE;
                      }
                    else
                      {
                        op_in_store = FALSE;
                        op_in_load = TRUE;
                      }
                    break;
                default:
                    break;
              }
            operand this_operand = the_instr->src_op(src_num);
            add_mod_refs_for_operand(this_operand, mod_list, ref_list,
                                     privatizable_list, op_in_load,
                                     op_in_store);
          }
      }
  }

static void add_mod_refs_for_operand(operand the_operand,
                                     var_sym_list *mod_list,
                                     var_sym_list *ref_list,
                                     var_sym_list *privatizable_list,
                                     boolean in_load, boolean in_store)
  {
    assert(mod_list != NULL);
    assert(ref_list != NULL);
    assert(privatizable_list != NULL);

    if (the_operand.is_expr())
      {
        instruction *the_instr = the_operand.instr();
        assert(the_instr != NULL);
        add_mod_refs_for_instr(the_instr, mod_list, ref_list,
                               privatizable_list, in_load, in_store);
      }
    else if (the_operand.is_symbol())
      {
        var_sym *the_var = the_operand.symbol();
        assert(the_var != NULL);
        add_var_to_list(ref_list, the_var);
      }
  }

/*
 *  Note that break or continue statements are considered irregular control
 *  flow.  Only branches or jumps that are part of the construct itself, such
 *  as jumps in the test part of a TREE_IF to the jumpto() label are permitted.
 */
static boolean no_irregular_control_flow(tree_node_list *the_node_list)
  {
    assert(the_node_list != NULL);
    boolean result = !irregular_control_flow(the_node_list, NULL);
    return result;
  }

static boolean irregular_control_flow(tree_node_list *the_node_list,
                                      label_sym *allowed_label)
  {
    tree_node_list_iter the_iter(the_node_list);
    while (!the_iter.is_empty())
      {
        tree_node *this_node = the_iter.step();
        switch (this_node->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)this_node;
                instruction *the_instr = the_tree_instr->instr();
                assert(the_instr != NULL);
                if ((the_instr->opcode() == io_btrue) ||
                    (the_instr->opcode() == io_bfalse) ||
                    (the_instr->opcode() == io_jmp))
                  {
                    in_bj *the_bj = (in_bj *)the_instr;
                    if (the_bj->target() != allowed_label)
                        return TRUE;
                  }
                else if (the_instr->opcode() == io_mbr)
                  {
                    in_mbr *the_mbr = (in_mbr *)the_instr;
                    int num_labels = the_mbr->num_labs();
                    for (int lab_num = 0; lab_num < num_labels; ++lab_num)
                      {
                        if (the_mbr->label(lab_num) == allowed_label)
                            return TRUE;
                      }
                  }
                break;
              }
            case TREE_LOOP:
              {
                tree_loop *the_loop = (tree_loop *)this_node;
                if (irregular_control_flow(the_loop->body(), NULL))
                    return TRUE;
                if (irregular_control_flow(the_loop->test(),
                                           the_loop->toplab()))
                  {
                    return TRUE;
                  }
                break;
              }
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)this_node;
                if (irregular_control_flow(the_for->body(), NULL))
                    return TRUE;
                if (irregular_control_flow(the_for->landing_pad(), NULL))
                    return TRUE;
                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)this_node;
                if (irregular_control_flow(the_if->header(), the_if->jumpto()))
                    return TRUE;
                if (irregular_control_flow(the_if->then_part(), NULL))
                    return TRUE;
                if (irregular_control_flow(the_if->else_part(), NULL))
                    return TRUE;
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)this_node;
                if (irregular_control_flow(the_block->body(), NULL))
                    return TRUE;
                break;
              }
            default:
                assert(FALSE);
          }
      }

    return FALSE;
  }

static boolean outside_scope(tree_node *the_node, sym_node *the_symbol)
  {
    assert(the_node != NULL);
    assert(the_symbol != NULL);
    base_symtab *symbol_symtab = the_symbol->parent();
    assert(symbol_symtab != NULL);
    tree_node *this_node = the_node;
    do
      {
        tree_node_list *parent = this_node->parent();
        if (parent == NULL)
          {
            this_node = NULL;
            break;
          }
        this_node = parent->parent();
        assert(this_node != NULL);
      } while (!this_node->is_block());

    base_symtab *enclosing;
    if (this_node == NULL)
      {
        enclosing = the_node->proc()->parent();
      }
    else
      {
        assert(this_node->is_block());
        tree_block *this_block = (tree_block *)this_node;
        enclosing = this_block->symtab();
      }

    if (enclosing == symbol_symtab)
        return FALSE;

    while (enclosing != symbol_symtab)
      {
        if (symbol_symtab->is_global())
            return FALSE;
        symbol_symtab = symbol_symtab->parent();
        assert(symbol_symtab != NULL);
      }
    return TRUE;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
