/* file "child_scalarize.cc" */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE child_scalarize_cc

#include "porky.h"

RCS_BASE(
    "$Id: child_scalarize.cc,v 1.2 1999/08/25 03:27:25 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void child_scalarize_so(suif_object *the_object, so_walker *the_walker);
static void child_scalarize_aref(in_array *the_aref, boolean aggressive);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void child_scalarize_proc(tree_proc *the_proc, boolean aggressive)
  {
    boolean old_suppress_array_folding = suppress_array_folding;
    suppress_array_folding = FALSE;
    so_walker the_walker;
    the_walker.set_data(0, (int)aggressive);
    the_walker.walk(the_proc, &child_scalarize_so);
    suppress_array_folding = old_suppress_array_folding;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void child_scalarize_so(suif_object *the_object, so_walker *the_walker)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_array)
        return;
    boolean aggressive = the_walker->get_data(0).si;
    child_scalarize_aref((in_array *)the_instr, aggressive);
  }

static void child_scalarize_aref(in_array *the_aref, boolean aggressive)
  {
    immed value;
    eval_status status = evaluate_const_instr(the_aref, &value);
    if (status != EVAL_OK)
        return;
    if (!value.is_symbol())
        return;
    sym_addr address = root_address(value.addr());
    sym_node *this_sym = address.symbol();
    if (!this_sym->is_var())
        return;
    var_sym *this_var = (var_sym *)this_sym;
    type_node *result_type = the_aref->result_type();
    if (!result_type->is_ptr())
        return;
    ptr_type *result_ptr = (ptr_type *)result_type;
    var_sym *new_child =
            this_var->find_child(address.offset(), result_ptr->ref_type());
    if (new_child == NULL)
      {
        if (!aggressive)
            return;
        if (this_var->type()->unqual()->op() != TYPE_GROUP)
            return;
        new_child =
                this_var->build_child(address.offset(), result_ptr->ref_type(),
                                      "__gensub");
        assert(new_child != NULL);
      }
    in_ldc *new_ldc = new in_ldc(result_ptr, operand(), immed(new_child));
    replace_instruction(the_aref, new_ldc);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
