/* file "if_hoist.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* if-hoisting for the porky program for SUIF */

#define RCS_BASE_FILE if_hoist_cc

#include "porky.h"

RCS_BASE(
    "$Id: if_hoist.cc,v 1.2 1999/08/25 03:27:30 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_ifh_cond_var;
static const char *k_porky_ifh_const_writes;
static const char *k_porky_ifh_writes;
static const char *k_porky_ifh_reads;
static const char *k_porky_ifh_call;
static const char *k_porky_ifh_mem_op;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void if_hoist_on_list(tree_node_list *the_list);
static void if_hoist_on_node(tree_node *the_node);
static void mark_summary_on_node(tree_node *the_node, void *);
static void collect_summary_on_list(tree_node_list *the_list,
                                    var_sym_list *constant_writes,
                                    var_sym_list *writes, var_sym_list *reads,
                                    boolean *calls, boolean *mem_ops);
static void collect_summary_on_instr(instruction *the_instr,
                                    var_sym_list *constant_writes,
                                    var_sym_list *writes, var_sym_list *reads,
                                    boolean *calls, boolean *mem_ops);
static boolean no_dependences(tree_node *node1, tree_node *node2);
static var_sym *condition_var(tree_if *the_if);
static void set_condition_var(tree_if *the_if, var_sym *the_var);
static boolean constant_write_var(tree_node *the_node, var_sym *the_var);
static boolean node_is_branch(tree_node *the_node);
static var_sym_list *const_write_vars(tree_node *the_node);
static var_sym_list *node_write_vars(tree_node *the_node);
static var_sym_list *node_ref_vars(tree_node *the_node);
static boolean call_in_node(tree_node *the_node);
static boolean mem_op_in_node(tree_node *the_node);
static void set_call_in_node(tree_node *the_node);
static void set_mem_op_in_node(tree_node *the_node);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_if_hoist(void)
  {
    k_porky_ifh_cond_var = lexicon->enter("porky ifh cond var")->sp;
    k_porky_ifh_const_writes = lexicon->enter("porky ifh const writes")->sp;
    k_porky_ifh_writes = lexicon->enter("porky ifh writes")->sp;
    k_porky_ifh_reads = lexicon->enter("porky ifh reads")->sp;
    k_porky_ifh_call = lexicon->enter("porky ifh call")->sp;
    k_porky_ifh_mem_op = lexicon->enter("porky ifh mem op")->sp;
  }

extern void if_hoist_proc(tree_proc *the_proc)
  {
    the_proc->map(&mark_summary_on_node, NULL, FALSE);
    if_hoist_on_node(the_proc);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void if_hoist_on_list(tree_node_list *the_list)
  {
    tree_node_list_e *current_e = the_list->tail();
    while (current_e != NULL)
      {
        tree_node_list_e *prev_e = current_e->prev();

        tree_node *this_node = current_e->contents;

        if_hoist_on_node(this_node);

        if (this_node->is_if())
          {
            tree_if *moving_if = (tree_if *)this_node;
            var_sym *test_var = condition_var(moving_if);
            if (test_var != NULL)
              {
                tree_node_list_e *follow_e = prev_e;
                while (follow_e != NULL)
                  {
                    tree_node *check_node = follow_e->contents;
                    if (!no_dependences(check_node, this_node))
                        break;
                    if (node_is_branch(check_node))
                        break;
                    follow_e = follow_e->prev();
                  }

                if ((follow_e != NULL) && (follow_e->contents->is_if()) &&
                    constant_write_var(follow_e->contents, test_var))
                  {
                    tree_if *target_if = (tree_if *)(follow_e->contents);
                    the_list->remove(current_e);
                    delete current_e;
                    tree_if *else_if = moving_if->clone();
                    target_if->then_part()->append(moving_if);
                    target_if->else_part()->append(else_if);
                  }
              }
          }

        current_e = prev_e;
      }
  }

static void if_hoist_on_node(tree_node *the_node)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
            break;
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            if_hoist_on_list(the_loop->body());
            if_hoist_on_list(the_loop->test());
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            if_hoist_on_list(the_for->body());
            if_hoist_on_list(the_for->landing_pad());
            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;
            if_hoist_on_list(the_if->header());
            if_hoist_on_list(the_if->then_part());
            if_hoist_on_list(the_if->else_part());
            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            if_hoist_on_list(the_block->body());
            break;
          }
        default:
            assert(FALSE);
      }
  }

static void mark_summary_on_node(tree_node *the_node, void *)
  {
    boolean has_calls = FALSE;
    boolean has_mem_ops = FALSE;

    var_sym_list *constant_writes = const_write_vars(the_node);
    var_sym_list *writes = node_write_vars(the_node);
    var_sym_list *reads = node_ref_vars(the_node);

    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            collect_summary_on_instr(the_tree_instr->instr(), constant_writes,
                                     writes, reads, &has_calls, &has_mem_ops);
            break;
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            collect_summary_on_list(the_loop->body(), constant_writes, writes,
                                    reads, &has_calls, &has_mem_ops);
            collect_summary_on_list(the_loop->test(), constant_writes, writes,
                                    reads, &has_calls, &has_mem_ops);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            collect_summary_on_list(the_for->body(), constant_writes, writes,
                                    reads, &has_calls, &has_mem_ops);
            collect_summary_on_list(the_for->landing_pad(), constant_writes,
                                    writes, reads, &has_calls, &has_mem_ops);
            collect_summary_on_list(the_for->lb_list(), constant_writes,
                                    writes, reads, &has_calls, &has_mem_ops);
            collect_summary_on_list(the_for->ub_list(), constant_writes,
                                    writes, reads, &has_calls, &has_mem_ops);
            collect_summary_on_list(the_for->step_list(), constant_writes,
                                    writes, reads, &has_calls, &has_mem_ops);
            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;

              {
                var_sym_list temp_const_writes;
                var_sym_list temp_writes;
                var_sym_list temp_reads;
                boolean temp_calls;
                boolean temp_mem_ops;
                collect_summary_on_list(the_if->header(), &temp_const_writes,
                                        &temp_writes, &temp_reads, &temp_calls,
                                        &temp_mem_ops);
                if (temp_reads.count() == 1)
                    set_condition_var(the_if, temp_reads.head()->contents);
              }

            collect_summary_on_list(the_if->header(), constant_writes, writes,
                                    reads, &has_calls, &has_mem_ops);
            collect_summary_on_list(the_if->then_part(), constant_writes,
                                    writes, reads, &has_calls, &has_mem_ops);
            collect_summary_on_list(the_if->else_part(), constant_writes,
                                    writes, reads, &has_calls, &has_mem_ops);
            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            collect_summary_on_list(the_block->body(), constant_writes, writes,
                                    reads, &has_calls, &has_mem_ops);
            break;
          }
        default:
            assert(FALSE);
      }

    if (has_calls)
        set_call_in_node(the_node);

    if (has_mem_ops)
        set_mem_op_in_node(the_node);
  }

static void collect_summary_on_list(tree_node_list *the_list,
                                    var_sym_list *constant_writes,
                                    var_sym_list *writes, var_sym_list *reads,
                                    boolean *calls, boolean *mem_ops)
  {
    tree_node_list_iter node_iter(the_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();

        var_sym_list_iter const_write_iter(const_write_vars(this_node));
        while (!const_write_iter.is_empty())
          {
            var_sym *this_var = const_write_iter.step();
            constant_writes->append(this_var);
          }

        var_sym_list_iter write_iter(node_write_vars(this_node));
        while (!write_iter.is_empty())
          {
            var_sym *this_var = write_iter.step();
            writes->append(this_var);
          }

        var_sym_list_iter read_iter(node_ref_vars(this_node));
        while (!read_iter.is_empty())
          {
            var_sym *this_var = read_iter.step();
            reads->append(this_var);
          }

        if (call_in_node(this_node))
            *calls = TRUE;

        if (mem_op_in_node(this_node))
            *mem_ops = TRUE;
      }
  }

static void collect_summary_on_instr(instruction *the_instr,
                                    var_sym_list *constant_writes,
                                    var_sym_list *writes, var_sym_list *reads,
                                    boolean *calls, boolean *mem_ops)
  {
    if (the_instr->dst_op().is_symbol())
      {
        writes->append(the_instr->dst_op().symbol());
        if (the_instr->opcode() == io_ldc)
            constant_writes->append(the_instr->dst_op().symbol());
      }

    switch (the_instr->opcode())
      {
        case io_cal:
        case io_gen:
            *calls = TRUE;
            break;
        case io_lod:
        case io_str:
        case io_memcpy:
            *mem_ops = TRUE;
            break;
        default:
            break;
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_symbol())
          {
            reads->append(this_op.symbol());
          }
        else if (this_op.is_expr())
          {
            collect_summary_on_instr(this_op.instr(), constant_writes, writes,
                                     reads, calls, mem_ops);
          }
      }
  }

static boolean no_dependences(tree_node *node1, tree_node *node2)
  {
    if ((call_in_node(node1) || mem_op_in_node(node1)) &&
        (call_in_node(node2) || mem_op_in_node(node2)))
      {
        return FALSE;
      }

    var_sym_list *writes1 = node_write_vars(node1);
    var_sym_list *writes2 = node_write_vars(node2);
    var_sym_list *reads1 = node_ref_vars(node1);
    var_sym_list *reads2 = node_ref_vars(node2);

    var_sym_list_iter write_iter1(writes1);
    while (!write_iter1.is_empty())
      {
        var_sym *this_write1 = write_iter1.step();
        if (this_write1->is_addr_taken())
            return FALSE;

        var_sym_list_iter read_iter2(reads2);
        while (!read_iter2.is_empty())
          {
            var_sym *this_read2 = read_iter2.step();
            if (this_read2->is_addr_taken())
                return FALSE;
            if (this_write1->overlaps(this_read2))
                return FALSE;
          }
      }

    var_sym_list_iter read_iter1(reads1);
    while (!read_iter1.is_empty())
      {
        var_sym *this_read1 = read_iter1.step();
        if (this_read1->is_addr_taken())
            return FALSE;

        var_sym_list_iter write_iter2(writes2);
        while (!write_iter2.is_empty())
          {
            var_sym *this_write2 = write_iter2.step();
            if (this_write2->is_addr_taken())
                return FALSE;
            if (this_read1->overlaps(this_write2))
                return FALSE;
          }
      }

    return TRUE;
  }

static var_sym *condition_var(tree_if *the_if)
  {
    return (var_sym *)(the_if->peek_annote(k_porky_ifh_cond_var));
  }

static void set_condition_var(tree_if *the_if, var_sym *the_var)
  {
    the_if->append_annote(k_porky_ifh_cond_var, the_var);
  }

static boolean constant_write_var(tree_node *the_node, var_sym *the_var)
  {
    var_sym_list_iter the_iter(const_write_vars(the_node));
    while (!the_iter.is_empty())
      {
        var_sym *test_var = the_iter.step();
        if (test_var == the_var)
            return TRUE;
      }
    return FALSE;
  }

static boolean node_is_branch(tree_node *the_node)
  {
    if (!the_node->is_instr())
        return FALSE;
    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();

    switch (the_instr->opcode())
      {
        case io_mbr:
        case io_btrue:
        case io_bfalse:
        case io_jmp:
            return TRUE;
        default:
            return FALSE;
      }
  }

static var_sym_list *const_write_vars(tree_node *the_node)
  {
    annote *the_annote =
            the_node->annotes()->peek_annote(k_porky_ifh_const_writes);
    if (the_annote == NULL)
      {
        the_annote = new annote(k_porky_ifh_const_writes, new var_sym_list);
        the_node->annotes()->append(the_annote);
      }
    return (var_sym_list *)(the_annote->data());
  }

static var_sym_list *node_write_vars(tree_node *the_node)
  {
    annote *the_annote = the_node->annotes()->peek_annote(k_porky_ifh_writes);
    if (the_annote == NULL)
      {
        the_annote = new annote(k_porky_ifh_writes, new var_sym_list);
        the_node->annotes()->append(the_annote);
      }
    return (var_sym_list *)(the_annote->data());
  }

static var_sym_list *node_ref_vars(tree_node *the_node)
  {
    annote *the_annote = the_node->annotes()->peek_annote(k_porky_ifh_reads);
    if (the_annote == NULL)
      {
        the_annote = new annote(k_porky_ifh_reads, new var_sym_list);
        the_node->annotes()->append(the_annote);
      }
    return (var_sym_list *)(the_annote->data());
  }

static boolean call_in_node(tree_node *the_node)
  {
    return (the_node->annotes()->peek_annote(k_porky_ifh_call) != NULL);
  }

static boolean mem_op_in_node(tree_node *the_node)
  {
    return (the_node->annotes()->peek_annote(k_porky_ifh_mem_op) != NULL);
  }

static void set_call_in_node(tree_node *the_node)
  {
    the_node->append_annote(k_porky_ifh_call, NULL);
  }

static void set_mem_op_in_node(tree_node *the_node)
  {
    the_node->append_annote(k_porky_ifh_mem_op, NULL);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
