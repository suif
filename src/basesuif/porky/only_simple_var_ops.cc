/* file "only_simple_var_ops.cc" */

/*  Copyright (c) 1997 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to dismantle all variable operands except those for simple
 * local, non-static variables without the addr_taken flag set, for
 * the porky program for SUIF */


#define RCS_BASE_FILE only_simple_var_ops_cc

#include "porky.h"

RCS_BASE(
    "$Id: only_simple_var_ops.cc,v 1.2 1999/08/25 03:27:32 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void only_simple_var_ops_on_object(suif_object *the_object,
                                          so_walker *the_walker);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void only_simple_var_ops_on_proc(tree_proc *the_proc)
  {
    so_walker the_walker;
    the_walker.set_post_function(&only_simple_var_ops_on_object);
    the_walker.walk(the_proc);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void only_simple_var_ops_on_object(suif_object *the_object,
                                          so_walker *the_walker)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_src = the_instr->src_op(src_num);
        if (this_src.is_symbol())
          {
            var_sym *this_var = this_src.symbol();
            if ((this_var->is_addr_taken()) ||
                (!this_var->parent()->is_block()) || (this_var->has_var_def())
                || (any_part_volatile(this_var->type())))
              {
                this_var->set_addr_taken();
                in_rrr *new_load =
                        new in_rrr(io_lod,
                                   this_var->type()->unqual(), operand(),
                                   addr_op(this_var));
                the_instr->set_src_op(src_num, operand(new_load));
              }
          }
      }

    operand destination_op = the_instr->dst_op();
    if (destination_op.is_symbol())
      {
        var_sym *dest_var = destination_op.symbol();
        if ((dest_var->is_addr_taken()) || (!dest_var->parent()->is_block()) ||
            (dest_var->has_var_def()) || (any_part_volatile(dest_var->type())))
          {
            dest_var->set_addr_taken();
            in_rrr *new_store =
                    new in_rrr(io_str, type_void, operand(), addr_op(dest_var),
                               operand());
            the_walker->replace_object(new_store);
            the_instr->set_dst(operand());
            new_store->set_src2(operand(the_instr));
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
