/* file "structured_facts.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the implementation of the code to find,
 *  propagate, and apply facts through structured control flow.
 */

#define RCS_BASE_FILE structured_facts_cc

#include <useful.h>
#include "structured_facts.h"
#include "porky.h"

RCS_BASE(
    "$Id: structured_facts.cc,v 1.2 1999/08/25 03:27:35 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

struct live_list
  {
public:
    propagated_fact_list facts;
    fact_manager *the_manager;
    boolean unreachable;

    live_list(fact_manager *initial_manager)
      {
        the_manager = initial_manager;
        unreachable = FALSE;
      }
    void reset(void);
    void kill_addressed(void);
    void kill_for_arbitrary_call(void);
    void kill(var_sym *the_var);
    void kill_for_instr(instruction *the_instr);
    void kill_for_for_bounds(tree_for *the_for);
    void kill_for_for_step(tree_for *the_for);
    void kill_for_scope_exit(base_symtab *scope);
    void kill_possibly_written(tree_node_list *the_list);
    live_list *clone(void);
    void merge(live_list *second_list);
    fact_manager *manager(void) { return the_manager; }
    ~live_list();
  };

DECLARE_LIST_CLASS(label_list, label_sym *);
DECLARE_LIST_CLASS(live_list_list, live_list *);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Variable Declarations
 *----------------------------------------------------------------------*/

static label_list *pending_labels = NULL;
static live_list_list *pending_lists = NULL;

/*----------------------------------------------------------------------*
    End Private Variable Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void forward_prop_list(tree_node_list *the_node_list,
                              live_list *the_live_list);
static boolean forward_prop_leaf_call_children(live_list *the_live_list,
                                               instruction *the_instr);
static void kill_possibly_written_on_node(tree_node *the_node, void *data);
static void possible_jump_to(label_sym *the_label, live_list *the_live_list);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void apply_through_structured_control(tree_node_list *the_list,
                                             fact_manager *the_manager)
  {
    assert(pending_labels == NULL);
    assert(pending_lists == NULL);
    pending_labels = new label_list;
    pending_lists = new live_list_list;

    live_list the_live_list(the_manager);
    the_manager->initial_fact_creator(&(the_live_list.facts));
    forward_prop_list(the_list, &the_live_list);

    assert(pending_labels->is_empty());
    assert(pending_lists->is_empty());
    delete pending_labels;
    delete pending_lists;
    pending_labels = NULL;
    pending_lists = NULL;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

void live_list::kill(var_sym *the_var)
  {
    propagated_fact_list_e *this_elem = facts.head();
    while (this_elem != NULL)
      {
        propagated_fact *this_fact = this_elem->contents;
        propagated_fact_list_e *next_elem = this_elem->next();

        if (this_fact->killed_by_var_write(the_var))
          {
            facts.remove(this_elem);
            delete this_elem;
            delete this_fact;
          }
        this_elem = next_elem;
      }
  }

void live_list::kill_for_instr(instruction *the_instr)
  {
    propagated_fact_list_e *this_elem = facts.head();
    while (this_elem != NULL)
      {
        propagated_fact *this_fact = this_elem->contents;
        propagated_fact_list_e *next_elem = this_elem->next();

        if (this_fact->killed_by_instr(the_instr))
          {
            facts.remove(this_elem);
            delete this_elem;
            delete this_fact;
          }
        this_elem = next_elem;
      }
  }

void live_list::kill_for_for_bounds(tree_for *the_for)
  {
    if (manager()->use_kill_summary())
      {
        kill(the_for->index());
      }
    else
      {
        propagated_fact_list_e *this_elem = facts.head();
        while (this_elem != NULL)
          {
            propagated_fact *this_fact = this_elem->contents;
            propagated_fact_list_e *next_elem = this_elem->next();

            if (this_fact->killed_by_for_bounds(the_for))
              {
                facts.remove(this_elem);
                delete this_elem;
                delete this_fact;
              }
            this_elem = next_elem;
          }
      }
  }

void live_list::kill_for_for_step(tree_for *the_for)
  {
    if (manager()->use_kill_summary())
      {
        kill(the_for->index());
      }
    else
      {
        propagated_fact_list_e *this_elem = facts.head();
        while (this_elem != NULL)
          {
            propagated_fact *this_fact = this_elem->contents;
            propagated_fact_list_e *next_elem = this_elem->next();

            if (this_fact->killed_by_for_step(the_for))
              {
                facts.remove(this_elem);
                delete this_elem;
                delete this_fact;
              }
            this_elem = next_elem;
          }
      }
  }

void live_list::kill_for_scope_exit(base_symtab *scope)
  {
    propagated_fact_list_e *this_elem = facts.head();
    while (this_elem != NULL)
      {
        propagated_fact *this_fact = this_elem->contents;
        propagated_fact_list_e *next_elem = this_elem->next();

        if (this_fact->killed_exiting_scope(scope))
          {
            facts.remove(this_elem);
            delete this_elem;
            delete this_fact;
          }
        this_elem = next_elem;
      }
  }

live_list::~live_list()
  {
    while (!facts.is_empty())
      {
        propagated_fact *this_fact = facts.pop();
        delete this_fact;
      }
  }

void live_list::reset(void)
  {
    while (!facts.is_empty())
      {
        propagated_fact *this_fact = facts.pop();
        delete this_fact;
      }
    unreachable = FALSE;
  }

void live_list::kill_addressed(void)
  {
    propagated_fact_list_e *this_elem = facts.head();
    while (this_elem != NULL)
      {
        propagated_fact *this_fact = this_elem->contents;
        propagated_fact_list_e *next_elem = this_elem->next();

        if (this_fact->killed_by_unknown_mem_write())
          {
            facts.remove(this_elem);
            delete this_elem;
            delete this_fact;
          }
        this_elem = next_elem;
      }
  }

void live_list::kill_for_arbitrary_call(void)
  {
    propagated_fact_list_e *this_elem = facts.head();
    while (this_elem != NULL)
      {
        propagated_fact *this_fact = this_elem->contents;
        propagated_fact_list_e *next_elem = this_elem->next();

        if (this_fact->killed_by_arbitrary_function_call())
          {
            facts.remove(this_elem);
            delete this_elem;
            delete this_fact;
          }
        this_elem = next_elem;
      }
  }

void live_list::kill_possibly_written(tree_node_list *the_list)
  {
    the_list->map(&kill_possibly_written_on_node, this);
  }

live_list *live_list::clone(void)
  {
    live_list *the_clone = new live_list(manager());

    propagated_fact_list_iter fact_iter(&facts);
    while (!fact_iter.is_empty())
      {
        propagated_fact *this_fact = fact_iter.step();
        the_clone->facts.append(this_fact->clone());
      }

    return the_clone;
  }

void live_list::merge(live_list *second_list)
  {
    if (second_list->unreachable)
      {
        delete second_list;
        return;
      }
    if (unreachable)
      {
        unreachable = FALSE;
        while (!facts.is_empty())
          {
            propagated_fact *this_fact = facts.pop();
            delete this_fact;
          }
        while (!second_list->facts.is_empty())
          {
            propagated_fact *this_fact = second_list->facts.pop();
            facts.append(this_fact);
          }
        delete second_list;
        return;
      }

    propagated_fact_list_e *this_elem = facts.head();
    while (this_elem != NULL)
      {
        propagated_fact *fact1 = this_elem->contents;
        propagated_fact_list_iter fact_iter2(&(second_list->facts));
        boolean found = FALSE;
        while (!fact_iter2.is_empty())
          {
            propagated_fact *fact2 = fact_iter2.step();
            if (fact1->is_same_fact(fact2))
              {
                fact1->merge_reasons(fact2);
                found = TRUE;
                break;
              }
          }

        propagated_fact_list_e *next_elem = this_elem->next();
        if (!found)
          {
            facts.remove(this_elem);
            delete this_elem;
            delete fact1;
          }
        this_elem = next_elem;
      }

    delete second_list;
  }


static void forward_prop_list(tree_node_list *the_node_list,
                              live_list *the_live_list)
  {
    if (the_node_list == NULL)
        return;

    fact_manager *the_manager = the_live_list->manager();

    tree_node_list_iter the_iter(the_node_list);
    while (!(the_iter.is_empty()))
      {
        tree_node *the_node = the_iter.step();
        switch (the_node->kind())
          {
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)the_node;

                the_live_list->kill_for_for_bounds(the_for);

                the_manager->bound_fact_creator(&(the_live_list->facts),
                                                the_for);

                the_manager->act_on_for_facts(the_for,
                                              &(the_live_list->facts));

                live_list *break_live_list;
                if (the_for->peek_annote(k_guarded))
                  {
                    break_live_list = new live_list(the_live_list->manager());
                    break_live_list->unreachable = TRUE;
                  }
                else
                  {
                    break_live_list = the_live_list->clone();
                  }

                pending_labels->push(the_for->brklab());
                pending_lists->push(break_live_list);

                forward_prop_list(the_for->landing_pad(), the_live_list);

                live_list *iter_start_live_list;
                if (!fast_structured_facts)
                  {
                    iter_start_live_list = the_live_list->clone();
                    pending_labels->push(the_for->contlab());
                    pending_lists->push(iter_start_live_list);
                  }

                the_manager->step_fact_creator(&(the_live_list->facts),
                                                the_for);

                the_manager->index_fact_creator(&(the_live_list->facts),
                                                the_for);
                the_live_list->kill_for_for_step(the_for);
                the_live_list->kill_possibly_written(the_for->body());
                forward_prop_list(the_for->body(), the_live_list);

                if (!fast_structured_facts)
                  {
                    pending_labels->pop();
                    pending_lists->pop();
                    the_live_list->merge(iter_start_live_list);

                    the_manager->step_fact_creator(&(the_live_list->facts),
                                                   the_for);

                    the_manager->index_fact_creator(&(the_live_list->facts),
                                                    the_for);
                    the_live_list->kill_for_for_step(the_for);
                    forward_prop_list(the_for->body(), the_live_list);
                  }

                the_live_list->kill_for_for_step(the_for);

                pending_labels->pop();
                pending_lists->pop();
                the_live_list->merge(break_live_list);

                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)the_node;
                live_list *else_live_list =
                        new live_list(the_live_list->manager());
                else_live_list->unreachable = TRUE;

                pending_labels->push(the_if->jumpto());
                pending_lists->push(else_live_list);
                forward_prop_list(the_if->header(), the_live_list);
                pending_labels->pop();
                pending_lists->pop();

                forward_prop_list(the_if->then_part(), the_live_list);
                forward_prop_list(the_if->else_part(), else_live_list);
                the_live_list->merge(else_live_list);
                break;
              }
            case TREE_LOOP:
              {
                tree_loop *the_loop = (tree_loop *)the_node;
                the_live_list->kill_possibly_written(the_loop->body());
                the_live_list->kill_possibly_written(the_loop->test());

                live_list *continue_list =
                        new live_list(the_live_list->manager());
                continue_list->unreachable = TRUE;
                live_list *break_list =
                        new live_list(the_live_list->manager());
                break_list->unreachable = TRUE;

                pending_labels->push(the_loop->contlab());
                pending_lists->push(continue_list);
                pending_labels->push(the_loop->brklab());
                pending_lists->push(break_list);
                forward_prop_list(the_loop->body(), the_live_list);
                pending_labels->pop();
                pending_lists->pop();
                pending_labels->pop();
                pending_lists->pop();
                the_live_list->merge(continue_list);

                forward_prop_list(the_loop->test(), the_live_list);
                the_live_list->merge(break_list);

                break;
              }
            case TREE_INSTR:
              {
                tree_instr *the_tree_instr = (tree_instr *)the_node;

                instruction *the_instr = the_tree_instr->instr();
                if_ops opcode = the_instr->opcode();

                if (the_manager->use_kill_summary())
                  {
                    call_order the_call_order = call_query(the_instr);

                    if (the_call_order == NO_CALLS)
                      {
                        the_manager->act_on_facts(the_instr,
                                                  &(the_live_list->facts));
                      }
                    else if (the_call_order == CALLS_TOTALLY_ORDERED)
                      {
                        forward_prop_leaf_call_children(the_live_list,
                                                        the_instr);
                      }

                    if (the_call_order != NO_CALLS)
                      {
                        the_live_list->kill_for_arbitrary_call();
                        the_manager->act_on_facts(the_instr,
                                                  &(the_live_list->facts));
                      }

                    if ((opcode == io_str) || (opcode == io_memcpy))
                      {
                        in_rrr *the_rrr = (in_rrr *)the_instr;
                        operand dest_addr = the_rrr->dst_addr_op();
                        sym_node *modified_sym =
                                operand_address_root_symbol(dest_addr);
                        if (modified_sym != NULL)
                          {
                            if (modified_sym->is_var())
                              {
                                var_sym *the_var = (var_sym *)modified_sym;
                                the_live_list->kill(the_var);
                              }
                          }
                        else
                          {
                            the_live_list->kill_addressed();
                          }
                      }

                    operand destination_operand = the_instr->dst_op();

                    if (destination_operand.kind() == OPER_SYM)
                      {
                        var_sym *the_var = destination_operand.symbol();
                        the_live_list->kill(the_var);
                      }
                  }
                else
                  {
                    the_manager->act_on_facts(the_instr,
                                              &(the_live_list->facts));
                    the_live_list->kill_for_instr(the_instr);
                  }

                switch (opcode)
                  {
                    case io_lab:
                        the_live_list->reset();
                        break;
                    case io_btrue:
                    case io_bfalse:
                    case io_jmp:
                      {
                        in_bj *the_bj = (in_bj *)the_instr;
                        if (!pending_labels->is_empty())
                            possible_jump_to(the_bj->target(), the_live_list);
                        break;
                      }
                    case io_mbr:
                      {
                        in_mbr *the_mbr = (in_mbr *)the_instr;
                        if (!pending_labels->is_empty())
                          {
                            possible_jump_to(the_mbr->default_lab(),
                                             the_live_list);
                            unsigned num_labels = the_mbr->num_labs();
                            for (unsigned label_num = 0;
                                 label_num < num_labels; ++label_num)
                              {
                                possible_jump_to(the_mbr->label(label_num),
                                                 the_live_list);
                              }
                          }
                        break;
                      }
                    default:
                        break;
                  }

                if ((opcode == io_ret) || (opcode == io_jmp))
                    the_live_list->unreachable = TRUE;

                the_manager->instr_fact_creator(&(the_live_list->facts),
                                                the_tree_instr->instr());

                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)the_node;
                forward_prop_list(the_block->body(), the_live_list);
                the_live_list->kill_for_scope_exit(the_block->symtab());
                break;
              }
            default:
                assert(FALSE);
          }
      }
  }

static boolean forward_prop_leaf_call_children(live_list *the_live_list,
                                               instruction *the_instr)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_expr())
          {
            boolean done =
                    forward_prop_leaf_call_children(the_live_list,
                                                    this_op.instr());
            if (done)
                return TRUE;
          }
      }

    if (instr_is_impure_call(the_instr))
      {
        the_live_list->manager()->act_on_facts(the_instr,
                                               &(the_live_list->facts));
        return TRUE;
      }
    else
      {
        return FALSE;
      }
  }

static void kill_possibly_written_on_node(tree_node *the_node, void *data)
  {
    live_list *the_live_list = (live_list *)data;

    if (the_live_list->manager()->use_kill_summary())
      {
        if (the_node->is_instr())
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            if_ops opcode = the_instr->opcode();

            if (call_query(the_instr) != NO_CALLS)
              {
                the_live_list->kill_for_arbitrary_call();
              }
            else if ((opcode == io_str) || (opcode == io_memcpy))
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                operand dest_addr = the_rrr->dst_addr_op();
                sym_node *modified_sym =
                        operand_address_root_symbol(dest_addr);
                if (modified_sym != NULL)
                  {
                    if (modified_sym->is_var())
                      {
                        var_sym *the_var = (var_sym *)modified_sym;
                        the_live_list->kill(the_var);
                      }
                  }
                else
                  {
                    the_live_list->kill_addressed();
                  }
              }

            operand destination_operand = the_instr->dst_op();

            if (destination_operand.kind() == OPER_SYM)
              {
                var_sym *the_var = destination_operand.symbol();
                the_live_list->kill(the_var);
              }
          }
        else if (the_node->is_for())
          {
            tree_for *the_for = (tree_for *)the_node;
            the_live_list->kill(the_for->index());
          }
      }
    else
      {
        if (the_node->is_instr())
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            the_live_list->kill_for_instr(the_instr);
          }
        else if (the_node->is_for())
          {
            tree_for *the_for = (tree_for *)the_node;
            the_live_list->kill_for_for_bounds(the_for);
            the_live_list->kill_for_for_step(the_for);
          }
      }
  }

static void possible_jump_to(label_sym *the_label, live_list *the_live_list)
  {
    label_list_iter label_iter(pending_labels);
    live_list_list_iter list_iter(pending_lists);
    while (!label_iter.is_empty())
      {
        label_sym *pending_label = label_iter.step();
        live_list *pending_list = list_iter.step();
        if (pending_label == the_label)
            pending_list->merge(the_live_list->clone());
      }
    assert(list_iter.is_empty());
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
