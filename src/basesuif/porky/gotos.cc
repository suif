/* file "gotos.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE gotos_cc

#include "porky.h"

RCS_BASE(
    "$Id: gotos.cc,v 1.2 1999/08/25 03:27:29 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

struct goto_data
  {
    boolean incoming;
    boolean outgoing;
    boolean containing;
  };

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void find_labels_on_a_node(tree_node *the_node, void *);
static void enter_target(label_sym *the_label, tree_node *the_node);
static void find_gotos_on_a_node(tree_node *the_node, void *);
static void possible_jump(tree_node *from, label_sym *to);
static tree_node_list *nearest_common_ancestor(tree_node *node_1,
                                               tree_node *node_2);
static void mark_node_incoming(tree_node *the_node);
static void mark_node_outgoing(tree_node *the_node);
static void mark_node_contains(tree_node *the_node);
static boolean node_has_incoming(tree_node *the_node);
static boolean node_has_outgoing(tree_node *the_node);
static boolean node_contains(tree_node *the_node);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static alist *targets = NULL;
static alist *incoming_list = NULL;
static alist *outgoing_list = NULL;
static alist *containing_list = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void calculate_goto_information(tree_node *the_node)
  {
    if (targets != NULL)
        delete targets;
    targets = new alist();
    if (incoming_list != NULL)
        delete incoming_list;
    incoming_list = new alist();
    if (outgoing_list != NULL)
        delete outgoing_list;
    outgoing_list = new alist();
    if (containing_list != NULL)
        delete containing_list;
    containing_list = new alist();
    the_node->map(&find_labels_on_a_node, NULL);
    the_node->map(&find_gotos_on_a_node, NULL);
  }

extern boolean node_is_bad(tree_node *the_node)
  {
    return (node_has_incoming(the_node) || node_has_outgoing(the_node));
  }

extern boolean node_contains_goto(tree_node *the_node)
  {
    return node_contains(the_node);
  }

extern boolean node_has_incoming_goto(tree_node *the_node)
  {
    return node_has_incoming(the_node);
  }

extern goto_data *goto_data_from_node(tree_node *the_node)
  {
    goto_data *the_data = (goto_data *)malloc(sizeof(goto_data));
    the_data->incoming = node_has_incoming(the_node);
    the_data->outgoing = node_has_outgoing(the_node);
    the_data->containing = node_contains(the_node);
    return the_data;
  }

extern void set_node_goto_data(goto_data *the_data, tree_node *the_node)
  {
    if (the_data->incoming)
        mark_node_incoming(the_node);
    if (the_data->outgoing)
        mark_node_outgoing(the_node);
    if (the_data->containing)
        mark_node_contains(the_node);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void find_labels_on_a_node(tree_node *the_node, void *)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();

            if (the_instr->format() != inf_lab)
                return;

            in_lab *the_lab = (in_lab *)the_instr;

            enter_target(the_lab->label(), the_node);
            return;
          }
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            enter_target(the_loop->contlab(), the_node);
            enter_target(the_loop->brklab(), the_node);
            enter_target(the_loop->toplab(), the_node);
            return;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            enter_target(the_for->contlab(), the_node);
            enter_target(the_for->brklab(), the_node);
            return;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)the_node;
            enter_target(the_if->jumpto(), the_node);
            return;
          }
        default:
            return;
      }
  }

static void enter_target(label_sym *the_label, tree_node *the_node)
  {
    if (targets->exists(the_label))
        error_line(1, the_node, "conflicting labels");

    targets->enter((void *)the_label, (void *)the_node);
  }

static void find_gotos_on_a_node(tree_node *the_node, void *)
  {
    if (the_node->kind() != TREE_INSTR)
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();
    if_ops opcode = the_instr->opcode();

    switch (opcode)
      {
        case io_btrue:
        case io_bfalse:
        case io_jmp:
          {
            in_bj *the_bj = (in_bj *)the_instr;
            possible_jump(the_node, the_bj->target());
            return;
          }
        case io_mbr:
          {
            in_mbr *the_mbr = (in_mbr *)the_instr;
            possible_jump(the_node, the_mbr->default_lab());

            int num_labels = the_mbr->num_labs();
            for (int index = 0; index < num_labels; ++index)
                possible_jump(the_node, the_mbr->label(index));

            return;
          }
        case io_ret:
            mark_node_contains(the_node);
            return;
        default:
            return;
      }
  }

static void possible_jump(tree_node *from, label_sym *to)
  {
    assert(from != NULL);
    assert(to != NULL);

    tree_node *to_node = (tree_node *)(targets->lookup((void *)to));

    if (to_node == NULL)
        error_line(1, from, "couldn't find target label");

    tree_node_list *nearest = nearest_common_ancestor(from, to_node);

    tree_node *follow = from;
    while (TRUE)
      {
        tree_node_list *parent = follow->parent();
        if (parent == NULL)
            break;
        follow = parent->parent();

        /*
         * The following test is for the special case of a branch or jump
         * from the first level of the test part of a LOOP node to that
         * LOOP's own toplab.  In that one case, the branch or jump is not
         * considered a goto at all, but part of the LOOP.
         */
        if ((follow == to_node) && (from->parent() == parent))
          {
            if (to_node->kind() == TREE_LOOP)
              {
                tree_loop *the_loop = (tree_loop *)to_node;
                if ((the_loop->toplab() == to) && (the_loop->test() == parent))
                    break;
              }

            /*
             * Now we have the analogous case for IF nodes: the special
             * case of a branch or jump from the first level of the test
             * part of an IF node to that IF's own jumpto() label.
             */
            if (to_node->kind() == TREE_IF)
              {
                tree_if *the_if = (tree_if *)to_node;
                if ((the_if->jumpto() == to) && (the_if->header() == parent))
                    break;
              }
          }

        mark_node_contains(follow);
      }

    tree_node *bad_node = from;

    while (TRUE)
      {
        tree_node_list *parent = bad_node->parent();
        if (parent == nearest)
            break;
        assert(parent != NULL);
        bad_node = parent->parent();
        mark_node_outgoing(bad_node);
      }

    if (nearest != NULL)
      {
        if (nearest->parent() == to_node)
            return;
      }

    bad_node = to_node;

    while (TRUE)
      {
        mark_node_incoming(bad_node);
        tree_node_list *parent = bad_node->parent();
        if (parent == nearest)
            break;
        assert(parent != NULL);
        bad_node = parent->parent();
      }
  }

/*
 *  This function doesn't really find the nearest common ancestor.  It really
 *  finds the nearest list that is to both nodes either anscestor or immediate
 *  child.
 */
static tree_node_list *nearest_common_ancestor(tree_node *node_1,
                                               tree_node *node_2)
  {
    int node_depth_1 = 0;
    tree_node *follow = node_1;
    while (follow != NULL)
      {
        tree_node_list *parent = follow->parent();
        if (parent == NULL)
            break;
        follow = parent->parent();
        if (follow == node_2)
            return parent;
        ++node_depth_1;
      }

    int node_depth_2 = 0;
    follow = node_2;
    while (follow != NULL)
      {
        tree_node_list *parent = follow->parent();
        if (parent == NULL)
            break;
        follow = parent->parent();
        if (follow == node_1)
            return parent;
        ++node_depth_2;
      }

    tree_node *follow_1 = node_1;
    tree_node *follow_2 = node_2;

    while (node_depth_1 > node_depth_2)
      {
        assert(follow_1 != NULL);
        tree_node_list *parent = follow_1->parent();
        assert(parent != NULL);
        follow_1 = parent->parent();
        --node_depth_1;
      }

    while (node_depth_2 > node_depth_1)
      {
        assert(follow_2 != NULL);
        tree_node_list *parent = follow_2->parent();
        assert(parent != NULL);
        follow_2 = parent->parent();
        --node_depth_2;
      }

    while (TRUE)
      {
        assert(follow_1 != NULL);
        tree_node_list *parent_1 = follow_1->parent();

        assert(follow_2 != NULL);
        tree_node_list *parent_2 = follow_2->parent();

        if (parent_1 == parent_2)
            return parent_1;

        assert(parent_1 != NULL);
        assert(parent_2 != NULL);
        follow_1 = parent_1->parent();
        follow_2 = parent_2->parent();
      }
  }

static void mark_node_incoming(tree_node *the_node)
  {
    incoming_list->enter(the_node, incoming_list);
  }

static void mark_node_outgoing(tree_node *the_node)
  {
    outgoing_list->enter(the_node, outgoing_list);
  }

static void mark_node_contains(tree_node *the_node)
  {
    containing_list->enter(the_node, containing_list);
  }

static boolean node_has_incoming(tree_node *the_node)
  {
    return incoming_list->exists(the_node);
  }

static boolean node_has_outgoing(tree_node *the_node)
  {
    return outgoing_list->exists(the_node);
  }

static boolean node_contains(tree_node *the_node)
  {
    return containing_list->exists(the_node);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
