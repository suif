/* file "make_empty_file_set_symbol_table.cc" */


/*
       Copyright (c) 1998 Stanford University

       All rights reserved.

       This software is provided under the terms described in
       the "suif_copyright.h" include file.
*/

#include <suif_copyright.h>


/*
      This is the implementation of the
      make-empty-file-set-symbol-table pass of the porky library.
*/


#define _MODULE_ "libporky"

#pragma implementation "make_empty_file_set_symbol_table.h"


#include <suif.h>
#include <suifpasses.h>
#include "make_empty_file_set_symbol_table.h"


void make_empty_file_set_symbol_table_pass::do_file_set_block_preorder(
        file_set_block *the_file_set_block)
  {
    if (the_file_set_block == NULL)
        return;
    basic_symbol_table *fs_table =
            the_file_set_block->get_file_set_symbol_table();
    basic_symbol_table *external_table =
            the_file_set_block->get_external_symbol_table();
    if ((fs_table == NULL) || (external_table == NULL))
        return;
    while (!fs_table->stos().is_empty())
      {
        sto *this_sto = fs_table->stos().elem(0);
        fs_table->remove_sto(this_sto);
        external_table->add_sto(this_sto);
      }
  }
