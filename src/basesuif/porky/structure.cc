/* file "structure.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* control-flow structuring for the porky program for SUIF */

#define RCS_BASE_FILE structure_cc

#include "porky.h"

RCS_BASE(
    "$Id: structure.cc,v 1.2 1999/08/25 03:27:35 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void find_fors_on_node(tree_node *the_node, void *);
static boolean is_signed_type(type_node *the_type);
static boolean castable_to_signed_type(operand the_op);
static operand cast_to_signed_type(operand the_op);
static type_node *make_signed_type(type_node *original_type);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void find_fors_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&find_fors_on_node, NULL, FALSE);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void find_fors_on_node(tree_node *the_node, void *)
  {
    if (!the_node->is_loop())
        return;
    tree_loop *the_loop = (tree_loop *)the_node;

    tree_node *test_node = single_effect(the_loop->test());
    if (test_node == NULL)
        return;

    if (!test_node->is_instr())
        return;
    tree_instr *test_tree_instr = (tree_instr *)test_node;
    instruction *test_instr = test_tree_instr->instr();

    boolean test_reversed;
    switch (test_instr->opcode())
      {
        case io_btrue:
            test_reversed = FALSE;
            break;
        case io_bfalse:
            test_reversed = TRUE;
            break;
        default:
            return;
      }
    in_bj *test_bj = (in_bj *)test_instr;

    if (test_bj->target() != the_loop->toplab())
        return;

    operand test_op = test_bj->src_op();

    if (!test_op.is_expr())
        return;
    instruction *comp_instr = test_op.instr();

    if ((comp_instr->opcode() != io_sl) && (comp_instr->opcode() != io_sle))
        return;
    in_rrr *comp_rrr = (in_rrr *)comp_instr;

    var_sym *index;
    operand upper_bound;
    tree_for_test the_test;
    if (comp_rrr->src1_op().is_symbol() &&
        !might_modify(comp_rrr->src2_op(), the_loop->body()))
      {
        index = comp_rrr->src1_op().symbol();
        upper_bound = comp_rrr->src2_op();
        if (comp_instr->opcode() == io_sl)
            the_test = FOR_SLT;
        else
            the_test = FOR_SLTE;
      }
    else if (comp_rrr->src2_op().is_symbol() &&
             !might_modify(comp_rrr->src1_op(), the_loop->body()))
      {
        index = comp_rrr->src2_op().symbol();
        upper_bound = comp_rrr->src1_op();
        if (comp_instr->opcode() == io_sl)
            the_test = FOR_SGT;
        else
            the_test = FOR_SGTE;
      }
    else
      {
        return;
      }

    if (index->type()->is_volatile())
        return;

    if (test_reversed)
      {
        switch (the_test)
          {
            case FOR_SGT:
                the_test = FOR_SLTE;
                break;
            case FOR_SGTE:
                the_test = FOR_SLT;
                break;
            case FOR_SLT:
                the_test = FOR_SGTE;
                break;
            case FOR_SLTE:
                the_test = FOR_SGT;
                break;
            default:
                assert(FALSE);
          }
      }

    instruction *write_instr;
    int num_writes = possible_writes(the_loop->body(), index, &write_instr);
    if (num_writes != 1)
        return;

    if ((write_instr->opcode() != io_add) && (write_instr->opcode() != io_sub))
        return;
    in_rrr *write_rrr = (in_rrr *)write_instr;

    operand step;
    if ((write_rrr->src1_op() == operand(index)) &&
        !might_modify(write_rrr->src2_op(), the_loop->body()))
      {
        step = write_rrr->src2_op();
      }
    else if ((write_rrr->opcode() == io_add) &&
             (write_rrr->src2_op() == operand(index)) &&
             !might_modify(write_rrr->src1_op(), the_loop->body()))
      {
        step = write_rrr->src1_op();
      }
    else
      {
        return;
      }

    /* make sure the write is not in a tree_for */
    tree_node *follow_up = write_instr->owner();
    while (follow_up != the_loop)
      {
        assert(follow_up != NULL);
        assert(follow_up->parent() != NULL);
        if (follow_up->is_for())
            return;
        follow_up = follow_up->parent()->parent();
      }

    boolean is_signed;
    type_node *index_type = index->type()->unqual();
    switch (index_type->op())
      {
        case TYPE_INT:
        case TYPE_ENUM:
          {
            base_type *the_base = (base_type *)index_type;
            is_signed = the_base->is_signed();
            break;
          }
        case TYPE_FLOAT:
            is_signed = TRUE;
            break;
        case TYPE_PTR:
            is_signed = FALSE;
            break;
        default:
            return;
      }

    if ((write_instr->opcode() == io_sub) && (!is_signed_type(step.type())) &&
        (!castable_to_signed_type(step)))
      {
        return;
      }

    /* Disallow any fors with non-integer index.  If the following
     * ``if'' statement is removed, fors with floating-point index
     * type will be detected.  Currently, it is illegal in SUIF to
     * have a non-integer type for a tree_for index. */
    if (diff_type(index_type)->op() != TYPE_INT)
      {
        return;
      }

    if (verbosity_level > 0)
      {
        printf("For detection: found for with index `%s' at line %d.\n",
               index->name(), source_line_num(the_loop));
      }

    upper_bound = upper_bound.clone();
    step = step.clone();

    tree_loop *new_loop = the_loop->clone();

    tree_node_list *body = new_loop->body();
    new_loop->set_body(new tree_node_list);

    boolean zero_lower_bound = index_type->is_ptr();

    tree_node_list *then_part = new tree_node_list;

    if (write_instr->opcode() == io_sub)
      {
        if (!is_signed_type(step.type()))
            step = cast_to_signed_type(step);
        step = -step;
      }

    tree_node *post_node = NULL;

    var_sym *lower_bound_var =
            the_loop->scope()->new_unique_var(index->type()->unqual());
    then_part->push(assign(lower_bound_var, operand(index)));
    operand lower_bound = operand(lower_bound_var);
    type_node *new_index_type = diff_type(index_type);
    var_sym *new_index = the_loop->scope()->new_unique_var(new_index_type);
    operand index_calc_op = operand(new_index);
    if (zero_lower_bound)
      {
        index_calc_op =
                fold_add(index_type, lower_bound.clone(), index_calc_op);
        upper_bound = fold_sub(new_index_type, upper_bound, lower_bound);
        lower_bound = operand_int(new_index_type, 0);
      }
    body->push(assign(index, index_calc_op.clone()));
    post_node = assign(index, index_calc_op);
    index_type = new_index_type;
    index = new_index;

    assert(index_type->is_base());
    base_type *new_base = (base_type *)index_type;
    is_signed = new_base->is_signed();

    if (!is_signed)
      {
        switch (the_test)
          {
            case FOR_SGT:
                the_test = FOR_UGT;
                break;
            case FOR_SGTE:
                the_test = FOR_UGTE;
                break;
            case FOR_SLT:
                the_test = FOR_ULT;
                break;
            case FOR_SLTE:
                the_test = FOR_ULTE;
                break;
            default:
                assert(FALSE);
          }
      }

    tree_for *new_for =
            new tree_for(index, the_test, new_loop->contlab(),
                         new_loop->brklab(), body, lower_bound, upper_bound,
                         step, new tree_node_list);

    delete new_loop;

    then_part->append(new_for);

    if (post_node != NULL)
        then_part->append(post_node);

    tree_node_list *else_part = new tree_node_list;

    assert(the_loop->scope()->is_block());
    block_symtab *block_scope = (block_symtab *)(the_loop->scope());
    label_sym *jumpto = block_scope->new_unique_label();

    tree_node_list *test_part = new tree_node_list;
    in_bj *bj_clone = (in_bj *)(test_bj->clone());
    if (bj_clone->opcode() == io_btrue)
        bj_clone->set_opcode(io_bfalse);
    else
        bj_clone->set_opcode(io_btrue);
    bj_clone->set_target(jumpto);
    test_part->append(new tree_instr(bj_clone));

    tree_if *the_if = new tree_if(jumpto, test_part, then_part, else_part);

    the_loop->parent()->insert_after(the_if, the_loop->list_e());

    the_loop->parent()->remove(the_loop->list_e());
    else_part->append(the_loop->list_e());
  }

static boolean is_signed_type(type_node *the_type)
  {
    switch (the_type->unqual()->op())
      {
        case TYPE_INT:
        case TYPE_ENUM:
          {
            base_type *the_base = (base_type *)(the_type->unqual());
            return the_base->is_signed();
          }
        case TYPE_FLOAT:
            return TRUE;
        default:
            return FALSE;
      }
  }

static boolean castable_to_signed_type(operand the_op)
  {
    immed value;
    eval_status status = evaluate_const_expr(the_op, &value);
    if (status != EVAL_OK)
        return FALSE;
    type_node *signed_type = make_signed_type(the_op.type());
    if (signed_type == NULL)
        return FALSE;
    return immed_fits(value, signed_type);
  }

static operand cast_to_signed_type(operand the_op)
  {
    immed value;
    eval_status status = evaluate_const_expr(the_op, &value);
    assert(status == EVAL_OK);
    type_node *signed_type = make_signed_type(the_op.type());
    assert(signed_type != NULL);
    return const_op(value, signed_type);
  }

static type_node *make_signed_type(type_node *original_type)
  {
    if (is_signed_type(original_type))
        return original_type;
    type_node *original_unqual = original_type->unqual();
    if ((original_unqual->op() != TYPE_INT) &&
        (original_unqual->op() != TYPE_ENUM))
      {
        return NULL;
      }
    type_node *new_type =
            new base_type(TYPE_INT, original_unqual->size(), TRUE);
    return fileset->globals()->install_type(new_type);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
