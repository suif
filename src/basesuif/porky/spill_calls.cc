/* file "spill_calls.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE spill_calls_cc

#include "porky.h"

RCS_BASE(
    "$Id: spill_calls.cc,v 1.2 1999/08/25 03:27:34 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void spill_calls_on_a_node(tree_node *the_node, void *);
static void spill_calls_on_children(operand the_operand);
static boolean spill_calls_on_operand(instruction *, operand *the_operand,
                                      void *);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void spill_calls(tree_node *the_node)
  {
    the_node->map(&spill_calls_on_a_node, NULL, FALSE);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void spill_calls_on_a_node(tree_node *the_node, void *)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            the_instr->src_map(&spill_calls_on_operand, NULL);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            spill_calls_on_children(the_for->lb_op());
            spill_calls_on_children(the_for->ub_op());
            spill_calls_on_children(the_for->step_op());
            break;
          }
        default:
            break;
      }
  }

static void spill_calls_on_children(operand the_operand)
  {
    if (the_operand.kind() != OPER_INSTR)
        return;

    instruction *the_instr = the_operand.instr();
    the_instr->src_map(&spill_calls_on_operand, NULL);
  }

static boolean spill_calls_on_operand(instruction *, operand *the_operand,
                                      void *)
  {
    if (the_operand->kind() != OPER_INSTR)
        return FALSE;
    spill_calls_on_children(*the_operand);
    instruction *the_instr = the_operand->instr();
    if (the_instr->opcode() != io_cal)
        return FALSE;
    force_dest_not_expr(the_instr);
    return FALSE;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
