/* file "nest.cc" */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to try to make loops nest perfectly for the porky program for
 * SUIF */


#define RCS_BASE_FILE nest_cc

#include "porky.h"

RCS_BASE(
    "$Id: nest.cc,v 1.2 1999/08/25 03:27:32 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

enum for_push_result { FPR_PUSHED, FPR_HANDLED, FPR_STOPPED };

DECLARE_DLIST_CLASS(tree_for_list, tree_for *);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_nest_todo = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void find_fors(suif_object *the_object, so_walker *the_walker);
static void try_nesting(tree_for *inner_for);
static for_push_result push_conditional_out_of_for(tree_for *the_for,
                                                   operand conditional_op);
static void condify_node(tree_node *the_node, operand condition);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_nest(void)
  {
    ANNOTE(k_porky_nest_todo, "porky nest todo", FALSE);
  }

extern void nest_proc(tree_proc *the_proc)
  {
    tree_for_list for_list;
    so_walker the_walker;
    the_walker.set_data(0, &for_list);
    the_walker.set_post_function(&find_fors);
    the_walker.walk(the_proc);
    while (!for_list.is_empty())
      {
        tree_for *this_for = for_list.pop();
        annote *todo_annote =
                this_for->annotes()->get_annote(k_porky_nest_todo);
        if (todo_annote != NULL)
          {
            try_nesting(this_for);
            delete todo_annote;
          }
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void find_fors(suif_object *the_object, so_walker *the_walker)
  {
    if (!the_object->is_tree_obj())
        return;
    tree_node *the_node = (tree_node *)the_object;
    if (!the_node->is_for())
        return;
    tree_for *the_for = (tree_for *)the_node;
    tree_for_list *for_list = (tree_for_list *)(the_walker->get_data(0).ptr);
    assert(for_list != NULL);
    for_list->append(the_for);
    the_for->append_annote(k_porky_nest_todo);
  }

static void try_nesting(tree_for *inner_for)
  {
    operand_list the_guards;

    if (inner_for->peek_annote(k_guarded) == NULL)
        return;
    tree_for *this_for = inner_for;
    while (TRUE)
      {
        annote *todo_annote =
                this_for->annotes()->get_annote(k_porky_nest_todo);
        if (todo_annote != NULL)
            delete todo_annote;
        operand_list_e *guard_e = the_guards.head();
        while (guard_e != NULL)
          {
            operand_list_e *next_e = guard_e->next();
            for_push_result fpr =
                    push_conditional_out_of_for(this_for, guard_e->contents);
            switch (fpr)
              {
                case FPR_PUSHED:
                    break;
                case FPR_HANDLED:
                    the_guards.remove(guard_e);
                    break;
                case FPR_STOPPED:
                  {
                    tree_node_list *holder = new tree_node_list;
                    tree_node *placeholder =
                            new tree_instr(new in_rrr(io_mrk));
                    holder->append(this_for->body());
                    this_for->body()->push(placeholder);
                    while (guard_e != NULL)
                      {
                        next_e = guard_e->next();
                        condify_node(placeholder, guard_e->contents);
                        the_guards.remove(guard_e);
                        guard_e = next_e;
                      }
                    placeholder->parent()->insert_after(holder,
                                                        placeholder->list_e());
                    kill_node(placeholder);
                    delete holder;
                    break;
                  }
                default:
                    assert(FALSE);
              }
            guard_e = next_e;
          }

        tree_node_list *parent_list = this_for->parent();
        assert(parent_list != NULL);
        tree_node *parent_node = parent_list->parent();
        while ((parent_node != NULL) && (parent_node->is_if()))
          {
            tree_if *this_if = (tree_if *)parent_node;
            if ((this_if->then_part() != parent_list) ||
                (!no_effects(this_if->else_part())) ||
                (single_effect(parent_list) != this_for))
              {
                break;
              }
            tree_node *test_effect = single_effect(this_if->header());
            if ((test_effect == NULL) || (!test_effect->is_instr()))
                break;
            tree_instr *test_tree_instr = (tree_instr *)test_effect;
            instruction *test_instr = test_tree_instr->instr();
            if ((test_instr->opcode() != io_btrue) &&
                (test_instr->opcode() != io_bfalse))
              {
                break;
              }
            in_bj *test_bj = (in_bj *)test_instr;
            operand test_op = test_bj->src_op();
            if (!test_op.is_expr())
                break;
            instruction *cond_instr = test_op.instr();
            if ((cond_instr->opcode() != io_sl) &&
                (cond_instr->opcode() != io_sle))
              {
                break;
              }
            in_rrr *the_rrr = (in_rrr *)cond_instr;
            operand op1 = the_rrr->src1_op();
            operand op2 = the_rrr->src2_op();
            op1.remove();
            op2.remove();
            operand new_op;
            if (cond_instr->opcode() == io_sl)
              {
                if (test_instr->opcode() == io_btrue)
                    new_op = op1 - op2;
                else
                    new_op = (op2 - op1) - 1;
              }
            else
              {
                if (test_instr->opcode() == io_btrue)
                    new_op = (op1 - op2) - 1;
                else
                    new_op = op2 - op1;
              }
            the_guards.push(new_op);
            remove_node(this_for);
            parent_list = this_if->parent();
            assert(parent_list != NULL);
            parent_list->insert_after(this_for, this_if->list_e());
            kill_node(this_if);
            parent_node = parent_list->parent();
          }
        if ((parent_node != NULL) && (parent_node->is_for()))
          {
            tree_for *next_for = (tree_for *)parent_node;
            if ((next_for->body() == parent_list) &&
                no_effects(next_for->landing_pad()) &&
                (single_effect(parent_list) == this_for) &&
                (next_for->peek_annote(k_guarded) != NULL))
              {
                this_for = next_for;
                continue;
              }
          }

        while (!the_guards.is_empty())
          {
            operand this_guard = the_guards.pop();
            condify_node(this_for, this_guard);
          }
        return;
      }
  }

/*
 *  Start with:
 *
 *      for (i = lb; i <op> ub; ++step)
 *          if (<conditional_op> >= 0)
 *              <body>
 *
 *  and push the conditional_op out of the for, if possible, assuming
 *  the_for will be guarded.
 */
static for_push_result push_conditional_out_of_for(tree_for *the_for,
                                                   operand conditional_op)
  {
    operand coefficient, remainder;
    boolean is_linear =
            linear_form(conditional_op, the_for->index(), &remainder,
                        &coefficient);
    if (!is_linear)
        return FPR_STOPPED;
    if ((might_modify(remainder, the_for->body())) ||
        (might_modify(coefficient, the_for->body())))
      {
        return FPR_STOPPED;
      }

    /*
     *  If the coefficient of the outer loop index variable isn't
     *  constant, we probably can't tell if it's positive or negative,
     *  so we can't change the loop bounds to nice max/min
     *  expressions.  But it would be difficult to do loop
     *  transformations or anything else that takes advantage of
     *  perfectly nested loops in this case anyway, so we just give
     *  up at that point.
     */
    immed coefficient_immed;
    eval_status status = evaluate_const_expr(coefficient, &coefficient_immed);
    if ((status != EVAL_OK) || (!coefficient_immed.is_int_const()))
        return FPR_STOPPED;
    i_integer coeff_i = immed_to_ii(coefficient_immed);

    if (coeff_i == 0)
        return FPR_PUSHED;

    operand new_bound = remainder/(-coeff_i);
    boolean change_lower;
    boolean eq_comp;
    switch (the_for->test())
      {
        case FOR_SGT:
        case FOR_UGT:
            change_lower = FALSE;
            eq_comp = FALSE;
            break;
        case FOR_SGTE:
        case FOR_UGTE:
            change_lower = FALSE;
            eq_comp = TRUE;
            break;
        case FOR_SLT:
        case FOR_ULT:
            change_lower = TRUE;
            eq_comp = FALSE;
            break;
        case FOR_SLTE:
        case FOR_ULTE:
            change_lower = TRUE;
            eq_comp = TRUE;
            break;
        default:
            error_line(1, the_for, "bad ``for'' test");
      }
    if_ops mm_op;
    if (change_lower)
        mm_op = io_max;
    else
        mm_op = io_min;
    if (coeff_i < 0)
        change_lower = !change_lower;
    if (change_lower)
      {
        operand old_lower = the_for->lb_op();
        old_lower.remove();
        type_node *old_type = old_lower.type();
        the_for->set_lb_op(fold_real_2op_rrr(mm_op, old_type, old_lower,
                                             cast_op(new_bound, old_type)));
      }
    else
      {
        operand old_upper = the_for->ub_op();
        old_upper.remove();
        type_node *old_type = old_upper.type();
        mm_op = ((mm_op == io_max) ? io_min : io_max);
        if (!eq_comp)
            new_bound = new_bound + ((mm_op == io_min) ? 1 : -1);
        the_for->set_ub_op(fold_real_2op_rrr(mm_op, old_type, old_upper,
                                             cast_op(new_bound, old_type)));
      }
    kill_op(conditional_op);
    return FPR_HANDLED;
  }

/*
 *  Replace the_node with:
 *
 *      if (<condition> >= 0)
 *          <the_node>
 *
 */
static void condify_node(tree_node *the_node, operand condition)
  {
    tree_node_list *parent = the_node->parent();
    assert(parent != NULL);
    operand test_op =
            fold_sle(const_op(i_integer(0), condition.type()), condition);
    tree_node_list *then_part = new tree_node_list;
    tree_if *new_if = if_node(the_node->scope(), test_op, then_part);
    parent->insert_after(new_if, the_node->list_e());
    remove_node(the_node);
    then_part->append(the_node);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
