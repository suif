/* file "private.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE private_cc

#include "porky.h"
#include <string.h>

RCS_BASE(
    "$Id: private.cc,v 1.2 1999/08/25 03:27:33 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

typedef struct
  {
    var_sym *old_var;
    var_sym *new_var;
  } two_vars;

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variable Declarations
 *----------------------------------------------------------------------*/

static const char *k_privatized;
static const char *k_needs_finalization;

/*----------------------------------------------------------------------*
    End Private Global Variable Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void privatize_on_list(tree_node_list *the_node_list);
static void privatize_on_node(tree_node *the_node, void *);
static void replace_var_on_node(tree_node *the_node, void *data);
void replace_var_on_instr(instruction *the_instr, void *data);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_privatization(void)
  {
    k_privatized = lexicon->enter("privatized")->sp;
    k_needs_finalization = lexicon->enter("needs finalization")->sp;
  }

extern void do_privatization(tree_node *the_node)
  {
    if (the_node->kind() != TREE_BLOCK)
        return;

    tree_block *the_block = (tree_block *)the_node;
    privatize_on_list(the_block->body());
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void privatize_on_list(tree_node_list *the_node_list)
  {
    assert(the_node_list != NULL);
    the_node_list->map(&privatize_on_node, NULL);
  }

static void privatize_on_node(tree_node *the_node, void *)
  {
    if (!the_node->is_for())
        return;

    tree_for *the_for = (tree_for *)the_node;

    immed_list *privatizable_immeds =
            (immed_list *)(the_for->peek_annote(k_privatized));
    if (privatizable_immeds == NULL)
        return;

    if (privatizable_immeds->is_empty())
        return;

    tree_block *inner_block = NULL;
    tree_node_list *body = the_for->body();
    assert(body != NULL);
    if (!body->is_empty())
      {
        tree_node_list_iter node_iter(body);
        tree_node *first_node = node_iter.step();
        assert(first_node != NULL);
        if (node_iter.is_empty())
          {
            if (first_node->is_block())
                inner_block = (tree_block *)first_node;
          }
      }

    block_symtab *inner_symtab;
    if (inner_block == NULL)
      {
        tree_node_list *new_body = new tree_node_list;
        new_body->append(body);
        base_symtab *scope = the_for->scope();
        assert(scope->is_block());
        block_symtab *old_block_symtab = (block_symtab *)scope;
        inner_symtab = old_block_symtab->new_unique_child();
        inner_block = new tree_block(new_body, inner_symtab);
        body->append(inner_block);
      }
    else
      {
        inner_symtab = inner_block->symtab();
      }

    immed_list *finalizable_immeds =
            (immed_list *)(the_for->peek_annote(k_needs_finalization));
    if (finalizable_immeds == NULL)
        finalizable_immeds = new immed_list;

    immed_list_iter the_iter(privatizable_immeds);
    while (!the_iter.is_empty())
      {
        immed this_immed = the_iter.step();
        if (!this_immed.is_symbol())
            continue;

        sym_node *this_symbol = this_immed.symbol();
        assert(this_symbol != NULL);
        if (!this_symbol->is_var())
            continue;

        var_sym *this_var = (var_sym *)this_symbol;

        if (this_var->is_global())
            error_line(1, the_for, "Unable to privatize global");

        const char *old_name = this_var->name();
        char *new_name = new char[strlen(old_name) + 12];
        strcpy(new_name, "privatized_");
        strcat(new_name, old_name);
        var_sym *new_var =
                inner_symtab->new_unique_var(this_var->type()->unqual(),
                                             new_name);
        delete[] new_name;
        two_vars the_info;
        the_info.old_var = this_var;
        the_info.new_var = new_var;
        inner_block->body()->map(&replace_var_on_node, &the_info);

        if (finalizable_immeds->lookup(immed(this_var)) == NULL)
            continue;

        instruction *new_cmp =
                new in_rrr(io_seq, type_signed, operand(), operand(new_var),
                           the_for->ub_op().clone());
        label_sym *else_label =
            the_for->proc()->block()->proc_syms()->new_unique_label(NULL);
        else_label->reset_userdef();
        instruction *new_branch =
                new in_bj(io_bfalse, else_label, operand(new_cmp));
        tree_instr *branch_tree_instr = new tree_instr(new_branch);
        tree_node_list *header = new tree_node_list;
        header->append(branch_tree_instr);

        instruction *the_copy =
                new in_rrr(io_cpy, new_var->type(), operand(this_var),
                           operand(new_var));
        tree_instr *tree_copy = new tree_instr(the_copy);
        tree_node_list *then_part = new tree_node_list;
        then_part->append(tree_copy);

        tree_if *new_if =
                new tree_if(else_label, header, then_part, new tree_node_list);
        inner_block->body()->append(new_if);
      }
  }

static void replace_var_on_node(tree_node *the_node, void *data)
  {
    assert(the_node != NULL);

    two_vars *replacement_info = (two_vars *)data;

    if (the_node->is_for())
      {
        tree_for *the_for = (tree_for *)the_node;
        if (the_for->index() == replacement_info->old_var)
            the_for->set_index(replacement_info->new_var);
        return;
      }

    if (!the_node->is_instr())
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();
    assert(the_instr != NULL);

    operand destination = the_instr->dst_op();
    if (destination.is_symbol())
      {
        sym_node *the_symbol = destination.symbol();
        assert(the_symbol != NULL);
        if (the_symbol == replacement_info->old_var)
            the_instr->set_dst(operand(replacement_info->new_var));
      }

    the_tree_instr->instr_map(&replace_var_on_instr, data);
  }

void replace_var_on_instr(instruction *the_instr, void *data)
  {
    assert(the_instr != NULL);

    two_vars *replacement_info = (two_vars *)data;

    if (the_instr->opcode() == io_ldc)
      {
        in_ldc *the_ldc = (in_ldc *)the_instr;
        immed value = the_ldc->value();
        if (value.is_symbol())
          {
            if (value.symbol() == replacement_info->old_var)
              {
                the_ldc->set_value(immed(replacement_info->new_var,
                                         value.offset()));
              }
          }
      }

    int num_srcs = the_instr->num_srcs();
    for (int src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_operand = the_instr->src_op(src_num);
        if (!this_operand.is_symbol())
            continue;

        sym_node *this_symbol = this_operand.symbol();
        assert(this_symbol != NULL);
        if (this_symbol != replacement_info->old_var)
            continue;

        the_instr->set_src_op(src_num, operand(replacement_info->new_var));
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
