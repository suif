/* file "propagate.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE propagate_cc

#include "fact.h"
#include "structured_facts.h"
#include "porky.h"

RCS_BASE(
    "$Id: propagate.cc,v 1.2 1999/08/25 03:27:33 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

class propagate_var_fact : public propagated_fact
  {
private:
    var_sym *the_var;
    instruction *the_definition;

public:
    propagate_var_fact(var_sym *new_var, instruction *new_definition)
      {
        the_var = new_var;
        the_definition = new_definition;
      }
    virtual ~propagate_var_fact(void) { }

    propagated_fact *clone(void)
      {
        return new propagate_var_fact(the_var, the_definition);
      }
    boolean killed_by_instr(instruction *the_instr);
    boolean killed_by_for_bounds(tree_for *the_for);
    boolean killed_by_for_step(tree_for *the_for);
    boolean killed_by_unknown_mem_write(void);
    boolean killed_by_arbitrary_function_call(void);
    boolean killed_exiting_scope(base_symtab *scope);
    boolean is_same_fact(propagated_fact *other_fact);
    void merge_reasons(propagated_fact * /* other_fact */) { }

    operand replacement_expression(propagated_fact_list *facts);
    var_sym *var(void) { return the_var; }
  };

class propagate_fact_manager : public fact_manager
  {
private:
    boolean has_substitution(propagated_fact_list *fact_list, var_sym *the_var,
                             operand *result);

public:
    void initial_fact_creator(propagated_fact_list *) { }
    void instr_fact_creator(propagated_fact_list *the_facts,
                            instruction *the_instr);
    void bound_fact_creator(propagated_fact_list *, tree_for *) { }
    void step_fact_creator(propagated_fact_list *, tree_for *) { }
    void index_fact_creator(propagated_fact_list *, tree_for *) { }
    void act_on_facts(instruction *the_instr, propagated_fact_list *fact_list);
    void act_on_for_facts(tree_for *the_for, propagated_fact_list *fact_list);
    boolean use_kill_summary(void) { return FALSE; }
  };

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Variable Declarations
 *----------------------------------------------------------------------*/

static forward_prop_kind prop_kind_flag;

/*----------------------------------------------------------------------*
    End Private Variable Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void substitute_into_expression(instruction *to_substitute,
                                       propagated_fact_list *the_facts,
                                       boolean only_write_proof);
static boolean leaf_call_children_sub(instruction *the_expr,
                                      propagated_fact_list *the_facts);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void forward_propagate(tree_node *the_node,
                              forward_prop_kind which_kind)
  {
    if (the_node->kind() != TREE_BLOCK)
        return;

    prop_kind_flag = which_kind;
    tree_block *the_block = (tree_block *)the_node;
    propagate_fact_manager the_manager;
    apply_through_structured_control(the_block->body(), &the_manager);
  }

extern boolean instr_might_store(instruction *the_instr, var_sym *the_var)
  {
    if ((the_instr->opcode() == io_str) || (the_instr->opcode() == io_memcpy))
      {
        in_rrr *the_rrr = (in_rrr *)the_instr;
        sym_node *the_symbol =
                operand_address_root_symbol(the_rrr->dst_addr_op());
        if ((the_symbol == NULL) || (the_symbol == the_var))
            return TRUE;
      }

    return FALSE;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

operand propagate_var_fact::replacement_expression(propagated_fact_list *facts)
  {
    instruction *old_instr = the_definition;

    while (old_instr->opcode() == io_cpy)
      {
        operand new_op = old_instr->src_op(0);
        if (!new_op.is_instr())
            return new_op.clone();
        old_instr = new_op.instr();
      }

    instruction *new_instr = old_instr->clone();
    new_instr->set_dst(operand());
    substitute_into_expression(new_instr, facts, FALSE);
    return operand(new_instr);
  }

boolean propagate_var_fact::killed_by_instr(instruction *the_instr)
  {
    if (the_instr->dst_op().is_symbol())
      {
        var_sym *dest_var = the_instr->dst_op().symbol();
        if (the_var->overlaps(dest_var) ||
            instr_may_reference_var(the_definition, dest_var))
          {
            return TRUE;
          }
      }

    if ((the_instr->opcode() == io_str) || (the_instr->opcode() == io_memcpy))
      {
        in_rrr *the_rrr = (in_rrr *)the_instr;
        operand dest_addr = the_rrr->dst_addr_op();
        if (instr_may_reference_location(the_definition, dest_addr))
            return TRUE;
        sym_node *modified_sym = operand_address_root_symbol(dest_addr);
        if (modified_sym != NULL)
          {
            if (modified_sym->is_var())
              {
                var_sym *modified_var = (var_sym *)modified_sym;
                if (the_var->overlaps(modified_var))
                    return TRUE;
              }
          }
        else
          {
            if (the_var->is_addr_taken())
                return TRUE;
          }
      }

    if (instr_contains_impure_call(the_instr))
      {
        if (killed_by_arbitrary_function_call())
          {
            return TRUE;
          }
      }

    return FALSE;
  }

boolean propagate_var_fact::killed_by_for_bounds(tree_for *the_for)
  {
    return (the_var->overlaps(the_for->index()) ||
            instr_may_reference_var(the_definition, the_for->index()));
  }

boolean propagate_var_fact::killed_by_for_step(tree_for *the_for)
  {
    return (the_var->overlaps(the_for->index()) ||
            instr_may_reference_var(the_definition, the_for->index()));
  }

boolean propagate_var_fact::killed_by_unknown_mem_write(void)
  {
    return (the_var->is_addr_taken() ||
            instr_reads_addressed_var(the_definition) ||
            instr_contains_load(the_definition));
  }

boolean propagate_var_fact::killed_by_arbitrary_function_call(void)
  {
    return (killed_by_unknown_mem_write() || (!(the_var->is_auto())) ||
            instr_may_read_statically_allocated_var(the_definition));
  }

boolean propagate_var_fact::killed_exiting_scope(base_symtab *scope)
  {
    return ((the_var->parent() == scope) ||
            instr_uses_scope(the_definition, scope));
  }

boolean propagate_var_fact::is_same_fact(propagated_fact *other_fact)
  {
    propagate_var_fact *other_var_fact = (propagate_var_fact *)other_fact;
    return ((the_var == other_var_fact->the_var) &&
            instrs_are_same_expr(the_definition,
                                 other_var_fact->the_definition));
  }


boolean propagate_fact_manager::has_substitution(
                propagated_fact_list *fact_list, var_sym *the_var,
                operand *result)
  {
    propagated_fact_list_iter fact_iter(fact_list);
    while (!fact_iter.is_empty())
      {
        propagated_fact *this_fact = fact_iter.step();
        propagate_var_fact *this_var_fact = (propagate_var_fact *)this_fact;
        if (this_var_fact->var() == the_var)
          {
            *result = this_var_fact->replacement_expression(fact_list);
            return TRUE;
          }
      }
    return FALSE;
  }

void propagate_fact_manager::instr_fact_creator(
             propagated_fact_list *the_facts, instruction *the_instr)
  {
    if (!the_instr->dst_op().is_symbol())
        return;
    var_sym *dest_var = the_instr->dst_op().symbol();

    if (dest_var->type()->is_volatile() || !instr_reevaluation_ok(the_instr) ||
        instr_may_reference_var(the_instr, dest_var))
      {
        return;
      }

    if (prop_kind_flag == FPK_SIMPLE_LOCAL_VARS)
      {
        if (the_instr->opcode() != io_cpy)
            return;
        in_rrr *the_cpy = (in_rrr *)the_instr;
        if (!the_cpy->src_op().is_symbol())
            return;
        var_sym *source_var = the_cpy->src_op().symbol();
        if (source_var->is_global() || dest_var->is_global())
            return;
      }

    the_facts->append(new propagate_var_fact(dest_var, the_instr));
  }

void propagate_fact_manager::act_on_facts(instruction *the_instr,
                                          propagated_fact_list *fact_list)
  {
    call_order the_call_order = call_query(the_instr);

    if (the_call_order == NO_CALLS)
      {
        substitute_into_expression(the_instr, fact_list, FALSE);
      }
    else
      {
        if (the_call_order == CALLS_TOTALLY_ORDERED)
            (void)leaf_call_children_sub(the_instr, fact_list);

        substitute_into_expression(the_instr, fact_list, TRUE);
      }
  }

void propagate_fact_manager::act_on_for_facts(tree_for *the_for,
                                              propagated_fact_list *fact_list)
  {
    if (the_for->lb_op().is_symbol())
      {
        operand result;
        boolean replace =
                has_substitution(fact_list, the_for->lb_op().symbol(),
                                 &result);
        if (replace)
          {
            the_for->set_lb_op(result);
            made_progress = TRUE;
          }
      }
    else if (the_for->lb_op().is_expr())
      {
        act_on_facts(the_for->lb_op().instr(), fact_list);
      }

    if (the_for->ub_op().is_symbol())
      {
        operand result;
        boolean replace =
                has_substitution(fact_list, the_for->ub_op().symbol(),
                                 &result);
        if (replace)
          {
            the_for->set_ub_op(result);
            made_progress = TRUE;
          }
      }
    else if (the_for->ub_op().is_expr())
      {
        act_on_facts(the_for->ub_op().instr(), fact_list);
      }

    if (the_for->step_op().is_symbol())
      {
        operand result;
        boolean replace =
                has_substitution(fact_list, the_for->step_op().symbol(),
                                 &result);
        if (replace)
          {
            the_for->set_step_op(result);
            made_progress = TRUE;
	  }
      }
    else if (the_for->step_op().is_expr())
      {
        act_on_facts(the_for->step_op().instr(), fact_list);
      }
  }


static void substitute_into_expression(instruction *to_substitute,
                                       propagated_fact_list *the_facts,
                                       boolean only_write_proof)
  {
    unsigned num_srcs = to_substitute->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = to_substitute->src_op(src_num);
        if (this_op.is_symbol())
          {
            propagated_fact_list_iter fact_iter(the_facts);
            while (!fact_iter.is_empty())
              {
                propagated_fact *this_fact = fact_iter.step();
                propagate_var_fact *this_var_fact =
                        (propagate_var_fact *)this_fact;
                if (only_write_proof &&
                    this_var_fact->killed_by_unknown_mem_write())
                  {
                    continue;
                  }
                if (this_var_fact->var() == this_op.symbol())
                  {
                    operand new_op =
                            this_var_fact->replacement_expression(the_facts);
                    if (new_op.is_expr() &&
                        instr_may_reference_var(new_op.instr(),
                                                this_op.symbol()))
                      {
                        break;
                      }
                    to_substitute->set_src_op(src_num, new_op);
                    made_progress = TRUE;
                    break;
                  }
              }
          }
        else if (this_op.is_instr())
          {
            substitute_into_expression(this_op.instr(), the_facts,
                                       only_write_proof);
          }
      }
  }

static boolean leaf_call_children_sub(instruction *the_expr,
                                      propagated_fact_list *the_facts)
  {
    unsigned num_srcs = the_expr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_expr->src_op(src_num);
        if (this_op.is_expr())
          {
            boolean done = leaf_call_children_sub(this_op.instr(), the_facts);
            if (done)
                return TRUE;
          }
      }

    if (instr_is_impure_call(the_expr) || (the_expr->opcode() == io_gen))
      {
        substitute_into_expression(the_expr, the_facts, FALSE);
        return TRUE;
      }
    else
      {
        return FALSE;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
