/* file "codegen.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE codegen_cc

#include "porky.h"

RCS_BASE(
    "$Id: codegen.cc,v 1.2 1999/08/25 03:27:25 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static operand operand_dumb_mul(type_node *the_type, operand arg1,
                                operand arg2);
static operand operand_dumb_add(type_node *the_type, operand arg1,
                                operand arg2);
static operand operand_dumb_sub(type_node *the_type, operand arg1,
                                operand arg2);
static operand operand_dumb_lsl(operand arg1, operand arg2);
static boolean try_to_fold_addition(operand original_operand, int constant);
static boolean try_to_fold_multiply(type_node* the_type,
                                    operand *original_operand, int constant);
static boolean will_fold_multiply(operand original_operand, int constant);
static boolean log_base_two(int to_test, int *the_log);
static void dispose_of_operand(operand to_go);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern operand operand_multiply(type_node *the_type, operand arg1,
                                operand arg2)
  {
    assert(!the_type->is_modifier());

    int constant;

    operand new_arg1 = fold_constants(arg1);
    operand new_arg2 = fold_constants(arg2);

    if (int_const_from_operand(new_arg1, &constant))
      {
        dispose_of_operand(new_arg1);
        return operand_mul_by_const(the_type, new_arg2, constant);
      }
    if (int_const_from_operand(new_arg2, &constant))
      {
        dispose_of_operand(new_arg2);
        return operand_mul_by_const(the_type, new_arg1, constant);
      }
    return operand_dumb_mul(the_type, new_arg1, new_arg2);
  }

extern operand operand_add(type_node *the_type, operand arg1, operand arg2)
  {
    assert(!the_type->is_modifier());

    int constant;

    operand new_arg1 = fold_constants(arg1);
    operand new_arg2 = fold_constants(arg2);

    if (int_const_from_operand(new_arg1, &constant))
      {
        dispose_of_operand(new_arg1);
        return operand_add_const(the_type, new_arg2, constant);
      }
    if (int_const_from_operand(new_arg2, &constant))
      {
        dispose_of_operand(new_arg2);
        return operand_add_const(the_type, new_arg1, constant);
      }
    return operand_dumb_add(the_type, new_arg1, new_arg2);
  }

extern operand operand_sub(type_node *the_type, operand arg1, operand arg2)
  {
    assert(!the_type->is_modifier());

    int constant;

    operand new_arg1 = fold_constants(arg1);
    operand new_arg2 = fold_constants(arg2);

    if (int_const_from_operand(new_arg2, &constant))
      {
        dispose_of_operand(new_arg2);
        return operand_sub_const(the_type, new_arg1, constant);
      }
    if (int_const_from_operand(new_arg1, &constant))
      {
        if (constant == 0)
          {
            dispose_of_operand(new_arg1);
            return operand_neg(the_type, new_arg2);
          }
      }
    return operand_dumb_sub(the_type, new_arg1, new_arg2);
  }

extern operand operand_lsl(operand arg1, operand arg2)
  {
    int constant;

    operand new_arg1 = fold_constants(arg1);
    operand new_arg2 = fold_constants(arg2);
    if (int_const_from_operand(new_arg1, &constant))
      {
        if (constant == 0)
          {
            dispose_of_operand(new_arg2);
            return new_arg1;
          }
      }
    if (int_const_from_operand(new_arg2, &constant))
      {
        if (constant == 0)
          {
            dispose_of_operand(new_arg2);
            return new_arg1;
          }
      }
    return operand_dumb_lsl(new_arg1, new_arg2);
  }

extern operand operand_neg(type_node *the_type, operand arg)
  {
    assert(!the_type->is_modifier());

    int constant;

    operand new_arg = fold_constants(arg);

    if (int_const_from_operand(new_arg, &constant))
      {
        int negation = -constant;
        if (-negation == constant)
          {
            dispose_of_operand(new_arg);
            return operand_int(the_type, negation);
          }
      }

    return operand_rrr(the_type, io_neg, new_arg, operand());
  }

extern operand operand_div(type_node *the_type, operand arg1, operand arg2)
  {
    assert(!the_type->is_modifier());

    int constant;

    operand new_arg1 = fold_constants(arg1);
    operand new_arg2 = fold_constants(arg2);

    if (int_const_from_operand(new_arg2, &constant))
      {
        dispose_of_operand(new_arg2);
        return operand_div_by_const(the_type, new_arg1, constant);
      }
    return operand_rrr(the_type, io_div, new_arg1, new_arg2);
  }

extern operand operand_rrr(type_node *the_type, if_ops opcode, operand arg1,
                           operand arg2)
  {
    assert(which_format(opcode) == inf_rrr);
    assert(!the_type->is_modifier());
    in_rrr *new_rrr = new in_rrr(opcode, the_type, operand(), arg1, arg2);
    operand new_operand(new_rrr);
    return new_operand;
  }

/*
 *  If the constant can't be folded but it's a power of two, use reduction of
 *  strength to make it a shift
 */
extern operand operand_mul_by_const(type_node *the_type, operand arg1,
                                    int arg2)
  {
    assert(!the_type->is_modifier());

    operand new_operand = arg1;
    if (try_to_fold_multiply(the_type, &new_operand, arg2))
        return cast_op(new_operand, the_type);
    int the_log;
    type_node *source_type = new_operand.type();
    assert(source_type != NULL);
    if (log_base_two(arg2, &the_log) && (source_type->op() == TYPE_INT) &&
        strength_reduce)
      {
        operand source = new_operand;

        base_type *the_base_type = (base_type *)source_type;
        if (the_base_type->is_signed())
          {
            type_node *new_type =
                    new base_type(TYPE_INT, the_base_type->size(), FALSE);
            new_type = the_base_type->parent()->install_type(new_type);
            source = operand(new in_rrr(io_cvt, new_type, operand(), source));
          }

        operand result =
                operand_lsl(source, operand_int(type_unsigned, the_log));

        if (result.type() != the_type)
            result = operand(new in_rrr(io_cvt, the_type, operand(), result));
        return result;
      }
    else
      {
        return operand_dumb_mul(the_type, new_operand,
                                operand_int(the_type, arg2));
      }
  }

extern operand operand_add_const(type_node *the_type, operand arg1, int arg2)
  {
    assert(!the_type->is_modifier());

    operand new_operand = arg1;
    if (try_to_fold_addition(new_operand, arg2))
        return cast_op(new_operand, the_type);
    type_node *const_type = (the_type->is_ptr() ? type_ptr_diff : the_type);
    return operand_dumb_add(the_type, arg1, operand_int(const_type, arg2));
  }

extern operand operand_sub_const(type_node *the_type, operand arg1, int arg2)
  {
    assert(!the_type->is_modifier());

    int negation = -arg2;
    if (-negation == arg2)
        return operand_add_const(the_type, arg1, negation);
    type_node *const_type =
            (the_type->unqual()->is_ptr()) ? type_ptr_diff : the_type;
    return operand_dumb_sub(the_type, arg1, operand_int(const_type, arg2));
  }

extern operand operand_div_by_const(type_node *the_type, operand arg1,
                                    int arg2)
  {
    assert(!the_type->is_modifier());

    if (arg2 == 1)
        return arg1;
    if (arg2 != 0)
      {
        int constant;
        if (int_const_from_operand(arg1, &constant))
          {
            dispose_of_operand(arg1);
            return operand_int(the_type, constant / arg2);
          }
      }
    return operand_rrr(the_type, io_div, arg1, operand_int(the_type, arg2));
  }

extern operand operand_int(type_node *the_type, int the_int)
  {
    assert(!the_type->is_modifier());

    immed the_immed(the_int);
    instruction *the_instr;
    if ((the_type->op() == TYPE_INT) || (the_type->op() == TYPE_PTR))
      {
        the_instr = new in_ldc(the_type, operand(), the_immed);
      }
    else
      {
        in_ldc *the_ldc = new in_ldc(type_signed, operand(), the_immed);
        operand ldc_operand(the_ldc);
        the_instr = new in_rrr(io_cvt, the_type, operand(), ldc_operand,
                               operand());
      }
    operand new_operand(the_instr);
    return new_operand;
  }

extern tree_if *create_if(operand test, tree_node_list *then_part,
                          tree_node_list *else_part, base_symtab *scope)
  {
    assert(scope->is_block());
    block_symtab *the_block_symtab = (block_symtab *)scope;
    label_sym *else_label = the_block_symtab->new_unique_label(NULL);
    tree_node_list *test_part = new tree_node_list;
    instruction *test_instr = new in_bj(io_bfalse, else_label, test);
    tree_node *test_node = new tree_instr(test_instr);
    append_tree_node_to_list(test_part, test_node);
    return new tree_if(else_label, test_part, then_part, else_part);
  }

extern boolean try_constant_operation(type_node *the_type, if_ops opcode,
                                      int source_1, int source_2, int *result)
  {
    assert(!the_type->is_modifier());

    if (is_real_2op_rrr(opcode))
      {
        immed result_val;
        eval_status return_code =
                calc_real_2op_rrr(opcode, &result_val, the_type,
                                  immed(source_1), immed(source_2));
        if (return_code != EVAL_OK)
            return FALSE;
        if (!result_val.is_integer())
            return FALSE;
        *result = result_val.integer();
        return TRUE;
      }
    else if (is_real_1op_rrr(opcode))
      {
        immed result_val;
        eval_status return_code =
                calc_real_1op_rrr(opcode, &result_val, the_type,
                                  immed(source_1));
        if (return_code != EVAL_OK)
            return FALSE;
        if (!result_val.is_integer())
            return FALSE;
        *result = result_val.integer();
        return TRUE;
      }
    else
      {
        return FALSE;
      }
  }

extern boolean operand_is_zero(operand the_operand)
  {
    int value;
    if (!int_const_from_operand(the_operand, &value))
        return FALSE;
    return (value == 0);
  }


extern boolean operand_is_int_const(operand the_operand)
  {
    int value;
    return int_const_from_operand(the_operand, &value);
  }


extern boolean int_const_from_operand(operand the_operand, int *value)
  {
    if (the_operand.kind() == OPER_NULL)
      {
        *value = 0;
        return TRUE;
      }
    if (the_operand.kind() != OPER_INSTR)
        return FALSE;
    instruction *instr = the_operand.instr();
    if (instr->opcode() != io_ldc)
        return FALSE;
    in_ldc *the_ldc = (in_ldc *)instr;
    immed immed_value = the_ldc->value();
    if (immed_value.kind() != im_int)
        return FALSE;
    *value = immed_value.integer();
    return TRUE;
  }

extern void replace_instruction_with_operand(instruction *old,
                                             operand new_operand)
  {
    operand original_destination = old->dst_op();
    boolean in_tree = (original_destination.kind() == OPER_INSTR);

    if (in_tree)
      {
        instruction *parent_instr = original_destination.instr();
        assert(parent_instr != NULL);

        unsigned num_srcs = parent_instr->num_srcs();
        for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
          {
            if (parent_instr->src_op(src_num) == operand(old))
              {
                old->remove();
                parent_instr->set_src_op(src_num, new_operand);
                return;
              }
          }
        assert(FALSE);
      }
    else
      {
        tree_node *owner = old->owner();
        assert(owner != NULL);
        switch (owner->kind())
          {
            case TREE_INSTR:
              {
                tree_instr *owner_instr_node = (tree_instr *)owner;
                owner_instr_node->remove_instr(old);

                instruction *new_instr;
                if (new_operand.is_expr())
                  {
                    new_instr = new_operand.instr();
                    new_instr->set_dst(original_destination);
                  }
                else
                  {
                    new_instr =
                            new in_rrr(io_cpy, old->result_type(),
                                       original_destination, new_operand);
                  }

                owner_instr_node->set_instr(new_instr);
                break;
              }
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)owner;

                operand lb_op = the_for->lb_op();
                if ((lb_op.kind() == OPER_INSTR) && (lb_op.instr() == old))
                  {
                    old->remove();
                    the_for->set_lb_op(new_operand);
                    return;
                  }

                operand ub_op = the_for->ub_op();
                if ((ub_op.kind() == OPER_INSTR) && (ub_op.instr() == old))
                  {
                    old->remove();
                    the_for->set_ub_op(new_operand);
                    return;
                  }

                operand step_op = the_for->step_op();
                if ((step_op.kind() == OPER_INSTR) && (step_op.instr() == old))
                  {
                    old->remove();
                    the_for->set_step_op(new_operand);
                    return;
                  }

                assert(FALSE);
                break;
              }
            default:
                assert(FALSE);
          }
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static operand operand_dumb_mul(type_node *the_type, operand arg1,
                                operand arg2)
  {
    return operand_rrr(the_type, io_mul, arg1, arg2);
  }

static operand operand_dumb_add(type_node *the_type, operand arg1,
                                operand arg2)
  {
    return operand_rrr(the_type, io_add, arg1, arg2);
  }

static operand operand_dumb_sub(type_node *the_type, operand arg1,
                                operand arg2)
  {
    return operand_rrr(the_type, io_sub, arg1, arg2);
  }

static operand operand_dumb_lsl(operand arg1, operand arg2)
  {
    return operand_rrr(arg1.type()->unqual(), io_lsl, arg1, arg2);
  }

static boolean try_to_fold_addition(operand original_operand, int constant)
  {
    if (constant == 0)
        return TRUE;
    if (original_operand.kind() == OPER_INSTR)
      {
        instruction *the_instr = original_operand.instr();
        switch (the_instr->opcode())
          {
            case io_cpy:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                return try_to_fold_addition(the_rrr->src1_op(), constant);
	      }
            case io_add:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                boolean result =
                        try_to_fold_addition(the_rrr->src1_op(), constant);
                if (!result)
                  {
                    result = try_to_fold_addition(the_rrr->src2_op(),
                                                  constant);
                  }
                return result;
	      }
            case io_sub:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                boolean result =
                        try_to_fold_addition(the_rrr->src1_op(), constant);
                if (!result)
                  {
                    int negation = -constant;
                    if (-negation != constant)
                        return FALSE;
                    result = try_to_fold_addition(the_rrr->src2_op(),
                                                  negation);
                  }
                return result;
	      }
            case io_neg:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                int negation = -constant;
                if (-negation != constant)
                    return FALSE;
                return try_to_fold_addition(the_rrr->src1_op(), negation);
	      }
            case io_ldc:
              {
                in_ldc *the_in_ldc = (in_ldc *)the_instr;
                immed the_immediate = the_in_ldc->value();
                if (the_immediate.kind() != im_int)
                    return FALSE;
                int old_value = the_immediate.integer();
                int new_value;
                if (!try_constant_operation(the_in_ldc->result_type(), io_add,
                                            old_value, constant, &new_value))
                  {
                    return FALSE;
                  }
                immed new_immed(new_value);
                the_in_ldc->set_value(new_immed);
                return TRUE;
              }
            default:
                return FALSE;
          }
      }
    else
      {
        return FALSE;
      }
  }

static boolean try_to_fold_multiply(type_node *the_type,
                                    operand *original_operand, int constant)
  {
    assert(!the_type->is_modifier());

    if (constant == 0)
      {
        if (original_operand->kind() == OPER_INSTR)
          {
            instruction *to_go = original_operand->instr();
            delete to_go;
          }
        *original_operand = operand_int(the_type, 0);
        return TRUE;
      }
    if (constant == 1)
        return TRUE;

    if (original_operand->kind() == OPER_INSTR)
      {
        instruction *the_instr = original_operand->instr();
        switch (the_instr->opcode())
          {
            case io_cpy:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                operand source_1 = the_rrr->src1_op();
                source_1.remove();
                boolean success =
                        try_to_fold_multiply(the_type, &source_1, constant);
                the_rrr->set_src1(source_1);
                return success;
	      }
            case io_add:
            case io_sub:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                operand source_1 = the_rrr->src1_op();
                operand source_2 = the_rrr->src2_op();

                boolean success = (will_fold_multiply(source_1, constant) ||
                                   will_fold_multiply(source_2, constant));
                if (!success)
                    return FALSE;

                source_1.remove();
                source_2.remove();

                type_node *result_type = the_rrr->result_type();

                boolean result =
                        try_to_fold_multiply(the_type, &source_1, constant);
                if (!result)
                  {
                    source_1 = operand_mul_by_const(result_type, source_1,
                                                   constant);
                  }

                result = try_to_fold_multiply(the_type, &source_2, constant);

                if (!result)
                  {
                    source_2 = operand_mul_by_const(result_type, source_2,
                                                   constant);
                  }

                the_rrr->set_src1(source_1);
                the_rrr->set_src2(source_2);
                return TRUE;
              }
            case io_neg:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                operand source_1 = the_rrr->src1_op();
                source_1.remove();
                boolean success =
                        try_to_fold_multiply(the_type, &source_1, constant);
                the_rrr->set_src1(source_1);
                return success;
	      }
            case io_mul:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;

                operand source_1 = the_rrr->src1_op();
                source_1.remove();
                boolean success =
                        try_to_fold_multiply(the_type, &source_1, constant);
                the_rrr->set_src1(source_1);
                if (success)
                    return TRUE;

                operand source_2 = the_rrr->src2_op();
                source_2.remove();
                success = try_to_fold_multiply(the_type, &source_2, constant);
                the_rrr->set_src2(source_2);
                return success;
	      }
            case io_ldc:
              {
                in_ldc *the_in_ldc = (in_ldc *)the_instr;
                immed the_immediate = the_in_ldc->value();
                if (the_immediate.kind() != im_int)
                    return FALSE;
                int old_value = the_immediate.integer();
                int new_value;
                if (!try_constant_operation(the_in_ldc->result_type(), io_mul,
                                            old_value, constant, &new_value))
                  {
                    return FALSE;
                  }
                immed new_immed(new_value);
                the_in_ldc->set_value(new_immed);
                return TRUE;
              }
            default:
                return FALSE;
          }
      }
    else
      {
        return FALSE;
      }
  }

static boolean will_fold_multiply(operand original_operand, int constant)
  {
    if (constant == 0)
        return TRUE;
    if (constant == 1)
        return TRUE;

    if (original_operand.kind() == OPER_INSTR)
      {
        instruction *the_instr = original_operand.instr();
        switch (the_instr->opcode())
          {
            case io_cpy:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                return will_fold_multiply(the_rrr->src1_op(), constant);
	      }
            case io_add:
            case io_sub:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                if (!will_fold_multiply(the_rrr->src1_op(), constant))
                    return FALSE;
                return will_fold_multiply(the_rrr->src2_op(), constant);
              }
            case io_neg:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                return will_fold_multiply(the_rrr->src1_op(), constant);
	      }
            case io_mul:
              {
                in_rrr *the_rrr = (in_rrr *)the_instr;
                if (will_fold_multiply(the_rrr->src1_op(), constant))
                    return TRUE;
                return will_fold_multiply(the_rrr->src2_op(), constant);
	      }
            case io_ldc:
              {
                in_ldc *the_in_ldc = (in_ldc *)the_instr;
                immed the_immediate = the_in_ldc->value();
                if (the_immediate.kind() == im_int)
                  {
                    int old_value = the_immediate.integer();
                    int new_value = old_value * constant;
                    boolean overflow =
                            ((constant != 0) &&
                             ((new_value / constant) != old_value));
                    return !overflow;
                  }
                else
                  {
                    return FALSE;
                  }
              }
            default:
                return FALSE;
          }
      }
    else
      {
        return FALSE;
      }
  }

static boolean log_base_two(int to_test, int *the_log)
  {
    int test = to_test;
    int result = 0;

    if (test == 0)
        return FALSE;
    while (TRUE)
      {
        if (test == 1)
          {
            *the_log = result;
            return TRUE;
          }
        if (test & 1)
            return FALSE;
        test >>= 1;
        ++result;
      }
  }

static void dispose_of_operand(operand to_go)
  {
    if (to_go.kind() == OPER_INSTR)
        delete to_go.instr();
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
