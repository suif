/* file "make_empty_file_set_symbol_table.h" */


/*
       Copyright (c) 1998 Stanford University

       All rights reserved.

       This software is provided under the terms described in
       the "suif_copyright.h" include file.
*/

#include <suif_copyright.h>


#ifndef PORKY_MAKE_EMPTY_FILE_SET_SYMBOL_TABLE_H
#define PORKY_MAKE_EMPTY_FILE_SET_SYMBOL_TABLE_H

#ifndef SUPPRESS_PRAGMA_INTERFACE
#pragma interface
#endif


/*
      This is the interface to the make-empty-file-set-symbol-table
      pass of the porky library.
*/


class make_empty_file_set_symbol_table_pass : public suif_to_suif_pass
  {
public:
    make_empty_file_set_symbol_table_pass(void)  { }
    virtual ~make_empty_file_set_symbol_table_pass(void)  { }

    virtual lstring name(void) const
      { return "make-empty-file-set-symbol-table"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


#endif /* PORKY_MAKE_EMPTY_FILE_SET_SYMBOL_TABLE_H */
