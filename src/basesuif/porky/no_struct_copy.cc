/* file "no_struct_copy.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE no_struct_copy_cc

#include "porky.h"

RCS_BASE(
    "$Id: no_struct_copy.cc,v 1.2 1999/08/25 03:27:32 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

#define LOOP_ITERATION_CUTOFF 5

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void node_no_struct_copy(tree_node *the_node, void *);
static void instr_no_struct_copy(instruction *the_instr, void *);
static tree_instr *new_bit_copy(int size, operand offset, in_rrr *the_memcopy);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void proc_no_struct_copy(tree_proc *the_proc)
  {
    the_proc->map(&node_no_struct_copy, NULL, FALSE);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void node_no_struct_copy(tree_node *the_node, void *)
  {
    if (!the_node->is_instr())
        return;
    tree_instr *the_tree_instr = (tree_instr *)the_node;

    the_tree_instr->instr_map(&instr_no_struct_copy, NULL, FALSE);
  }

static void instr_no_struct_copy(instruction *the_instr, void *)
  {
    in_rrr *the_rrr;
    switch (the_instr->opcode())
      {
        case io_memcpy:
        case io_str:
          {
            the_rrr = (in_rrr *)the_instr;
            type_node *dst_type = the_rrr->dst_addr_op().type()->unqual();
            if (!dst_type->is_ptr())
              {
                error_line(1, the_rrr->owner(),
                           "%s destination address type is not a "
                           "pointer", if_ops_name(the_instr->opcode()));
              }
            ptr_type *dst_ptr = (ptr_type *)dst_type;
            if (!dst_ptr->ref_type()->unqual()->is_struct())
                return;

            if (the_instr->opcode() == io_str)
              {
                if (!the_rrr->src2_op().is_expr() ||
                    (the_rrr->src2_op().instr()->opcode() != io_lod))
                  {
                    if (the_rrr->src2_op().is_expr())
                        force_dest_not_expr(the_rrr->src2_op().instr());
                    assert(the_rrr->src2_op().is_symbol());
                    var_sym *source_sym = the_rrr->src2_op().symbol();
                    type_node *source_ptr_type = source_sym->type()->ptr_to();
                    source_sym->set_addr_taken();
                    in_ldc *new_address =
                            new in_ldc(source_ptr_type, operand(),
                                       immed(source_sym));
                    in_rrr *new_load =
                            new in_rrr(io_lod, source_sym->type()->unqual(),
                                       operand(), operand(new_address));
                    the_rrr->set_src2(new_load);
                  }

                assert(the_rrr->src2_op().is_expr());
                assert(the_rrr->src2_op().instr()->opcode() == io_lod);
                in_rrr *the_load = (in_rrr *)(the_rrr->src2_op().instr());
                operand source_address = the_load->src_addr_op();
                operand destination_address = the_rrr->dst_addr_op();
                source_address.remove();
                destination_address.remove();

                in_rrr *new_memcopy =
                        new in_rrr(io_memcpy, type_void, operand(),
                                   destination_address, source_address);
                replace_instruction(the_rrr, new_memcopy);
                delete the_rrr;
                the_rrr = new_memcopy;
              }
            break;
          }
        case io_cpy:
          {
            in_rrr *the_copy = (in_rrr *)the_instr;
            if (!the_copy->result_type()->unqual()->is_struct())
                return;

            force_sources_not_exprs(the_copy);
            force_dest_not_expr(the_copy);

            assert(the_copy->src_op().is_symbol());
            var_sym *source_sym = the_copy->src_op().symbol();

            assert(the_copy->dst_op().is_symbol());
            var_sym *destination_sym = the_copy->dst_op().symbol();

            type_node *source_ptr_type = source_sym->type()->ptr_to();
            source_sym->set_addr_taken();
            in_ldc *source_address =
                    new in_ldc(source_ptr_type, operand(), immed(source_sym));

            type_node *destination_ptr_type =
                    destination_sym->type()->ptr_to();
            destination_sym->set_addr_taken();
            in_ldc *destination_address =
                    new in_ldc(destination_ptr_type, operand(),
                               immed(destination_sym));

            the_rrr =
                    new in_rrr(io_memcpy, type_void, operand(),
                               operand(destination_address),
                               operand(source_address));
            the_copy->set_dst(operand());
            replace_instruction(the_copy, the_rrr);
            delete the_copy;
            break;
          }
        default:
            return;
      }

    assert(the_rrr->opcode() == io_memcpy);
    assert(the_rrr->dst_addr_op().type()->unqual()->is_ptr());
    ptr_type *dst_ptr_type =
            (ptr_type *)(the_rrr->dst_addr_op().type()->unqual());
    type_node *destination_type = dst_ptr_type->ref_type()->unqual();
    int num_bits = destination_type->size();
    if ((num_bits != target.size[C_char]) &&
        (num_bits != target.size[C_short]) &&
        (num_bits != target.size[C_int]) &&
        (num_bits != target.size[C_long]) &&
        (num_bits != target.size[C_longlong]))
      {
        force_sources_not_exprs(the_rrr);
      }

    tree_node *old_tree_node = the_rrr->owner();

    C_types biggest_chunk = C_longlong;

    while ((target.align[biggest_chunk] > get_alignment(destination_type)) &&
           (biggest_chunk != C_char))
      {
        biggest_chunk = (C_types)(((int)biggest_chunk) - 1);
      }

    int offset = 0;
    if (num_bits >= LOOP_ITERATION_CUTOFF * target.size[biggest_chunk])
      {
        assert(the_rrr->owner()->scope()->is_block());
        block_symtab *scope = (block_symtab *)(the_rrr->owner()->scope());

        int num_iterations = num_bits / target.size[biggest_chunk];
        int bytes_per_unit =
                target.size[biggest_chunk] / target.addressable_size;
        var_sym *loop_index = scope->new_unique_var(type_ptr_diff);
        loop_index->reset_userdef();

        operand offset_op = operand_int(type_ptr_diff, offset);
        offset_op =
                fold_real_2op_rrr(io_add, type_ptr_diff, offset_op,
                                  operand(loop_index));

        tree_instr *new_copy =
                new_bit_copy(target.size[biggest_chunk], offset_op, the_rrr);

        tree_node_list *loop_body = new tree_node_list;
        loop_body->append(new_copy);

        operand lb_op = operand_int(type_ptr_diff, 0);
        operand ub_op =
                operand_int(type_ptr_diff, num_iterations * bytes_per_unit);
        operand step_op = operand_int(type_ptr_diff, bytes_per_unit);

        label_sym *continue_label = scope->new_unique_label("continue");
        continue_label->reset_userdef();

        label_sym *break_label = scope->new_unique_label("break");
        break_label->reset_userdef();

        tree_for *new_for =
                new tree_for(loop_index, FOR_SLT, continue_label, break_label,
                             loop_body, lb_op, ub_op, step_op,
                             new tree_node_list);
        old_tree_node->parent()->insert_before(new_for,
                                               old_tree_node->list_e());
        num_bits -= num_iterations * target.size[biggest_chunk];
        offset += num_iterations * target.size[biggest_chunk];
      }

    C_types c_type = biggest_chunk;
    while (TRUE)
      {
        while (num_bits >= target.size[c_type])
          {
            operand byte_offset_op =
                    operand_int(type_ptr_diff,
                                offset / target.addressable_size);
            tree_instr *new_copy =
                    new_bit_copy(target.size[c_type], byte_offset_op, the_rrr);
            old_tree_node->parent()->insert_before(new_copy,
                                                   old_tree_node->list_e());
            num_bits -= target.size[c_type];
            offset += target.size[c_type];
          }

        if (c_type == C_char)
            break;
        c_type = (C_types)(((int)c_type) - 1);
      }

    if (num_bits != 0)
      {
        error_line(0, the_rrr->owner(),
                   "structure of size %d can't be broken into pieces with",
                   num_bits + offset);
        error_line(1, the_rrr->owner(),
                   "sizes matching target integral types");
      }
    kill_node(old_tree_node);
  }

static tree_instr *new_bit_copy(int size, operand offset, in_rrr *the_memcopy)
  {
    type_node *chunk_type = new base_type(TYPE_INT, size, TRUE);
    chunk_type = fileset->globals()->install_type(chunk_type);
    type_node *chunk_ptr_type = chunk_type->ptr_to();

    operand source_address = the_memcopy->src_addr_op().clone();
    operand destination_address = the_memcopy->dst_addr_op().clone();

    operand source_offset_op = offset.clone();
    operand destination_offset_op = offset;

    source_address =
            fold_real_2op_rrr(io_add, chunk_ptr_type, source_address,
                              source_offset_op);
    destination_address =
            fold_real_2op_rrr(io_add, chunk_ptr_type, destination_address,
                              destination_offset_op);

    in_rrr *new_memcopy =
            new in_rrr(io_memcpy, type_void, operand(), destination_address,
                       source_address);
    return new tree_instr(new_memcopy);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
