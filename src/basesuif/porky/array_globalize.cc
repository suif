/* file "array_globalize.cc" */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to make statically allocated arrays global for the porky
 * program for SUIF */


#define RCS_BASE_FILE array_globalize_cc

#include "porky.h"
#include <string.h>

RCS_BASE(
    "$Id: array_globalize.cc,v 1.2 1999/08/25 03:27:23 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void array_globalize_on_list(tree_node_list *node_list,
                                    int array_glob_size, boolean do_autos);
static void array_globalize_on_node(tree_node *the_node, int array_glob_size,
                                    boolean do_autos);
static void interfile_globalize(sym_node *the_sym);
static sym_node *lookup_any_sym(base_symtab *the_symtab, const char *name);
static void resolve_global_name(sym_node *the_sym);
static file_symtab *nearest_file_symtab(base_symtab *original_symtab);
static void remove_var_heirarchy(var_sym *parent_var);
static void add_var_heirarchy(var_sym *parent_var, base_symtab *target_symtab);
static boolean var_heirarchy_types_interfile(var_sym *the_var);
static boolean var_and_sub_vars_types_interfile(var_sym *the_var);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void array_globalize_on_symtab(base_symtab *the_symtab,
                                      int array_glob_size, boolean do_autos)
  {
    if (the_symtab->kind() == SYMTAB_GLOBAL)
        return;

    sym_node_list original_list;

    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        original_list.append(this_sym);
      }

    while (!original_list.is_empty())
      {
        sym_node *this_sym = original_list.pop();
        if (this_sym->parent() != the_symtab)
            continue;
        if (this_sym->is_var())
          {
            var_sym *this_var = (var_sym *)this_sym;
            if (this_var->type()->unqual()->is_array() &&
                (!this_var->is_param()) &&
                (this_var->type()->size() >= array_glob_size) &&
                (this_var->is_static() ||
                 (do_autos && the_symtab->is_block() &&
                  (((block_symtab *)the_symtab)->block()->proc()->peek_annote(
                           k_no_recursion) != NULL))) &&
                var_heirarchy_types_interfile(this_var) &&
                annotes_scope_ok(this_var, fileset->globals()))
              {
                if (this_var->has_var_def())
                  {
                    var_def *this_def = this_var->definition();
                    assert(this_def != NULL);
                    file_symtab *new_def_symtab =
                            nearest_file_symtab(this_var->parent());
                    if (annotes_scope_ok(this_def, new_def_symtab))
                        interfile_globalize(this_var);
                  }
                else
                  {
                    interfile_globalize(this_var);
                  }
              }
          }
      }
  }

extern void array_globalize_on_proc(tree_proc *the_proc, int array_glob_size,
                                    boolean do_autos)
  {
    array_globalize_on_node(the_proc, array_glob_size, do_autos);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void array_globalize_on_list(tree_node_list *node_list,
                                    int array_glob_size, boolean do_autos)
  {
    tree_node_list_iter node_iter(node_list);
    while (!node_iter.is_empty())
      {
        tree_node *this_node = node_iter.step();
        array_globalize_on_node(this_node, array_glob_size, do_autos);
      }
  }

static void array_globalize_on_node(tree_node *the_node, int array_glob_size,
                                    boolean do_autos)
  {
    if (the_node->is_block())
      {
        tree_block *the_block = (tree_block *)the_node;
        array_globalize_on_symtab(the_block->symtab(), array_glob_size,
                                  do_autos);
      }

    unsigned num_children = the_node->num_child_lists();
    for (unsigned child_num = 0; child_num < num_children; ++child_num)
      {
        array_globalize_on_list(the_node->child_list_num(child_num),
                                array_glob_size, do_autos);
      }
  }

static void interfile_globalize(sym_node *the_sym)
  {
    base_symtab *old_symtab = the_sym->parent();
    assert(old_symtab->kind() != SYMTAB_GLOBAL);

    var_def *the_def = NULL;
    if (the_sym->is_var())
      {
        var_sym *the_var = (var_sym *)the_sym;
        if (the_var->parent_var() != NULL)
          {
            interfile_globalize(the_var->parent_var());
            return;
          }
        if (the_var->has_var_def())
          {
            the_def = the_var->definition();
            assert(the_def != NULL);
          }
      }

    if (the_sym->is_var())
        remove_var_heirarchy((var_sym *)the_sym);
    else
        old_symtab->remove_sym(the_sym);

    resolve_global_name(the_sym);

    if (the_sym->is_var())
        add_var_heirarchy((var_sym *)the_sym, fileset->globals());
    else
        fileset->globals()->add_sym(the_sym);

    if (the_def != NULL)
      {
        base_symtab *old_def_parent = the_def->parent();
        if (!old_def_parent->is_file())
          {
            the_def->parent()->remove_def(the_def);
            nearest_file_symtab(old_symtab)->add_def(the_def);
          }
      }
    else if (the_sym->is_var())
      {
        var_sym *the_var = (var_sym *)the_sym;
        int alignment = get_alignment(the_var->type());
        nearest_file_symtab(old_symtab)->define_var(the_var, alignment);
      }
  }

static sym_node *lookup_any_sym(base_symtab *the_symtab, const char *name)
  {
    sym_node *test_sym = the_symtab->lookup_sym(name, SYM_VAR);
    if (test_sym != NULL)
        return test_sym;
    test_sym = the_symtab->lookup_sym(name, SYM_PROC);
    if (test_sym != NULL)
        return test_sym;
    return the_symtab->lookup_sym(name, SYM_LABEL);
  }

static void resolve_global_name(sym_node *the_sym)
  {
    if (lookup_any_sym(fileset->globals(), the_sym->name()) == NULL)
        return;

    char *test_name =
            new char[strlen(the_sym->name()) + sizeof(long) * 3 + 13];
    strcpy(test_name, "globalized_");
    strcat(test_name, the_sym->name());
    if (lookup_any_sym(fileset->globals(), test_name) == NULL)
      {
        the_sym->set_name(test_name);
        delete[] test_name;
        return;
      }

    char *num_place = &(test_name[strlen(test_name)]);
    i_integer sym_num = 0;
    while (TRUE)
      {
        sym_num.write(num_place);
        if (lookup_any_sym(fileset->globals(), test_name) == NULL)
          {
            the_sym->set_name(test_name);
            delete[] test_name;
            return;
          }
        ++sym_num;
      }
  }

static file_symtab *nearest_file_symtab(base_symtab *original_symtab)
  {
    assert(original_symtab->kind() != SYMTAB_GLOBAL);
    base_symtab *new_symtab = original_symtab;
    while (!new_symtab->is_file())
      {
        new_symtab = new_symtab->parent();
        assert(new_symtab != NULL);
      }
    return (file_symtab *)new_symtab;
  }

static void remove_var_heirarchy(var_sym *parent_var)
  {
    base_symtab *old_symtab = parent_var->parent();
    old_symtab->remove_sym(parent_var);
    unsigned num_children = parent_var->num_children();
    for (unsigned child_num = 0; child_num < num_children; ++child_num)
        remove_var_heirarchy(parent_var->child_var(child_num));
  }

static void add_var_heirarchy(var_sym *parent_var, base_symtab *target_symtab)
  {
    target_symtab->add_sym(parent_var);
    unsigned num_children = parent_var->num_children();
    for (unsigned child_num = 0; child_num < num_children; ++child_num)
        add_var_heirarchy(parent_var->child_var(child_num), target_symtab);
  }

static boolean var_heirarchy_types_interfile(var_sym *the_var)
  {
    return var_and_sub_vars_types_interfile(the_var->root_ancestor());
  }

static boolean var_and_sub_vars_types_interfile(var_sym *the_var)
  {
    if (!fileset->globals()->is_visible(the_var->type()))
        return FALSE;

    unsigned num_children = the_var->num_children();
    for (unsigned child_num = 0; child_num < num_children; ++child_num)
      {
        if (!var_and_sub_vars_types_interfile(the_var->child_var(child_num)))
            return FALSE;
      }
    return TRUE;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
