/* file "bitpack.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* bit-packing for the porky program for SUIF */

#define RCS_BASE_FILE bitpack_cc

#include "porky.h"

RCS_BASE(
    "$Id: bitpack.cc,v 1.2 1999/08/25 03:27:24 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_porky_bit_var;
static const char *k_porky_packed_position;
static const char *k_porky_pack_trigger;
static var_sym *current_var;
static proc_symtab *current_symtab;
static int current_position;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void mark_candidates_on_node(tree_node *the_node, void *);
static void mark_non_bit_on_node(tree_node *the_node, void *);
static void mark_non_bit_on_instr(instruction *the_instr);
static void mark_non_bit_on_operand(operand the_op);
static void mark_var_non_bit(var_sym *the_var);
static void substitute_on_node(tree_node *the_node, void *);
static void substitute_on_instr(instruction *the_instr);
static boolean check_var_ref(var_sym *the_var);
static void add_var(var_sym *the_var);
static operand replacement_operand(var_sym *the_var);
static instruction *bit_assign_instr(var_sym *the_var, boolean is_one);
static instruction *bit_var_to_var_instr(var_sym *dest_var,
                                         var_sym *source_var);
static immed mask_for_position(int position);
static void data_for_packed_var(var_sym *old_var, var_sym **new_var,
                                int *new_position);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_bitpack(void)
  {
    ANNOTE(k_porky_bit_var,         "porky bit var",         FALSE);
    ANNOTE(k_porky_packed_position, "porky packed position", FALSE);
    ANNOTE(k_porky_pack_trigger,    "porky pack trigger",    FALSE);
  }

extern void bit_pack(tree_proc *the_proc)
  {
    mark_candidates_on_node(the_proc, NULL);
    the_proc->map(&mark_candidates_on_node, NULL);
    the_proc->map(&mark_non_bit_on_node, NULL);

    current_var = NULL;
    current_symtab = the_proc->proc_syms();
    the_proc->map(&substitute_on_node, NULL, TRUE);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void mark_candidates_on_node(tree_node *the_node, void *)
  {
    if (!the_node->is_block())
        return;

    tree_block *the_block = (tree_block *)the_node;
    block_symtab *the_symtab = the_block->symtab();
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (!this_sym->is_var())
            continue;
        var_sym *this_var = (var_sym *)this_sym;
        if (this_var->is_addr_taken())
            continue;
        type_ops the_type_op = this_var->type()->unqual()->op();
        if ((the_type_op != TYPE_INT) && (the_type_op != TYPE_ENUM))
            continue;
        if (this_var->has_var_def())
            continue;
        this_var->append_annote(k_porky_bit_var, new immed_list);
      }
  }

static void mark_non_bit_on_node(tree_node *the_node, void *)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            mark_non_bit_on_instr(the_tree_instr->instr());
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            mark_var_non_bit(the_for->index());
            mark_non_bit_on_operand(the_for->lb_op());
            mark_non_bit_on_operand(the_for->ub_op());
            mark_non_bit_on_operand(the_for->step_op());
            break;
          }
        default:
            break;
      }
  }

static void mark_non_bit_on_instr(instruction *the_instr)
  {
    if (the_instr->dst_op().is_symbol())
      {
        var_sym *dest_var = the_instr->dst_op().symbol();

        if (the_instr->opcode() == io_ldc)
          {
            in_ldc *the_ldc = (in_ldc *)the_instr;
            if ((the_ldc->value() == immed(0)) ||
                (the_ldc->value() == immed(1)))
              {
                return;
              }
          }

        if (the_instr->opcode() == io_cpy)
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            operand src = the_rrr->src_op();
            if (src.is_symbol())
              {
                var_sym *src_var = src.symbol();
                if (src_var->annotes()->peek_annote(k_porky_bit_var) != NULL)
                  {
                    annote *trigger_annote =
                            src_var->annotes()->peek_annote(
                                    k_porky_pack_trigger);
                    if (trigger_annote == NULL)
                      {
                        trigger_annote = new annote(k_porky_pack_trigger);
                        src_var->annotes()->append(trigger_annote);
                      }

                    trigger_annote->immeds()->append(immed(dest_var));
                    return;
                  }
              }
          }

        mark_var_non_bit(dest_var);
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
        mark_non_bit_on_operand(the_instr->src_op(src_num));
  }

static void mark_non_bit_on_operand(operand the_op)
  {
    if (the_op.is_expr())
        mark_non_bit_on_instr(the_op.instr());
  }

static void mark_var_non_bit(var_sym *the_var)
  {
    annote *bit_var_annote = the_var->annotes()->get_annote(k_porky_bit_var);
    if (bit_var_annote != NULL)
      {
        delete bit_var_annote;
        annote *trigger_annote =
                the_var->annotes()->get_annote(k_porky_pack_trigger);
        if (trigger_annote == NULL)
            return;
        immed_list *trigger_immeds = trigger_annote->immeds();
        while (!trigger_immeds->is_empty())
          {
            immed this_immed = trigger_immeds->pop();
            assert(this_immed.is_symbol());
            sym_node *this_sym = this_immed.symbol();
            assert(this_sym->is_var());
            var_sym *this_var = (var_sym *)this_sym;
            mark_var_non_bit(this_var);
          }
        delete trigger_annote;
      }
  }

static void substitute_on_node(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        substitute_on_instr(the_tree_instr->instr());
      }
  }

static void substitute_on_instr(instruction *the_instr)
  {
    if (the_instr->dst_op().is_symbol())
      {
        var_sym *dest_var = the_instr->dst_op().symbol();
        boolean replace = check_var_ref(dest_var);
        if (replace)
          {
            instruction *new_instr;
            if (the_instr->opcode() == io_ldc)
              {
                in_ldc *the_ldc = (in_ldc *)the_instr;
                boolean is_one = (the_ldc->value() == immed(1));
                new_instr = bit_assign_instr(dest_var, is_one);
              }
            else if (the_instr->opcode() == io_cpy)
              {
                in_rrr *the_cpy = (in_rrr *)the_instr;
                assert(the_cpy->src_op().is_symbol());
                var_sym *source_var = the_cpy->src_op().symbol();
                new_instr = bit_var_to_var_instr(dest_var, source_var);
              }
            else
              {
                assert(FALSE);
              }

            tree_instr *the_tree_instr = the_instr->parent();
            assert(the_tree_instr->instr() == the_instr);
            the_tree_instr->remove_instr(the_instr);
            the_tree_instr->set_instr(new_instr);
            delete the_instr;
            return;
          }
      }

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_symbol())
          {
            var_sym *this_var = this_op.symbol();
            boolean replace = check_var_ref(this_var);
            if (replace)
                the_instr->set_src_op(src_num, replacement_operand(this_var));
          }
        else if (this_op.is_expr())
          {
            substitute_on_instr(this_op.instr());
          }
        mark_non_bit_on_operand(the_instr->src_op(src_num));
      }
  }

/* return TRUE iff the_var is now packed and needs to be replaced */
static boolean check_var_ref(var_sym *the_var)
  {
    annote *bit_var_annote = the_var->annotes()->get_annote(k_porky_bit_var);
    if (bit_var_annote != NULL)
      {
        delete bit_var_annote;
        add_var(the_var);
        return TRUE;
      }

    return (the_var->annotes()->peek_annote(k_porky_packed_position) != NULL);
  }

static void add_var(var_sym *the_var)
  {
    if (current_var == NULL)
      {
        current_var = current_symtab->new_unique_var(type_unsigned);
        current_position = 0;
      }

    immed_list *new_immeds = new immed_list();
    new_immeds->append(current_var);
    new_immeds->append(current_position);
    the_var->append_annote(k_porky_packed_position, new_immeds);

    ++current_position;
    if (current_position == type_unsigned->size())
        current_var = NULL;
  }

static operand replacement_operand(var_sym *the_var)
  {
    var_sym *packed_var;
    int packed_position;
    data_for_packed_var(the_var, &packed_var, &packed_position);

    immed mask_immed = mask_for_position(packed_position);
    in_ldc *mask_ldc = new in_ldc(type_unsigned, operand(), mask_immed);
    in_rrr *and_instr =
            new in_rrr(io_and, type_unsigned, operand(), operand(mask_ldc),
                       operand(packed_var));

    in_ldc *shift_ldc =
            new in_ldc(type_unsigned, operand(), immed(packed_position));
    in_rrr *lsr_instr =
            new in_rrr(io_lsr, type_unsigned, operand(), operand(and_instr),
                       operand(shift_ldc));
    if (type_unsigned == the_var->type())
        return operand(lsr_instr);
    in_rrr *cvt_instr =
            new in_rrr(io_cvt, the_var->type(), operand(), operand(lsr_instr));
    return operand(cvt_instr);
  }

static instruction *bit_assign_instr(var_sym *the_var, boolean is_one)
  {
    var_sym *packed_var;
    int packed_position;
    data_for_packed_var(the_var, &packed_var, &packed_position);

    immed mask_immed = mask_for_position(packed_position);
    if_ops combine_opcode;
    if (is_one)
      {
        combine_opcode = io_ior;
      }
    else
      {
        combine_opcode = io_and;
        eval_status status =
                calc_real_1op_rrr(io_not, &mask_immed, type_unsigned,
                                  mask_immed);
        assert(status == EVAL_OK);
      }

    in_ldc *mask_ldc = new in_ldc(type_unsigned, operand(), mask_immed);
    in_rrr *combine_rrr =
            new in_rrr(combine_opcode, type_unsigned, operand(packed_var),
                       operand(packed_var), operand(mask_ldc));

    return combine_rrr;
  }

static instruction *bit_var_to_var_instr(var_sym *dest_var,
                                         var_sym *source_var)
  {
    var_sym *packed_dest_var;
    int dest_position;
    data_for_packed_var(dest_var, &packed_dest_var, &dest_position);

    var_sym *packed_source_var;
    int source_position;
    data_for_packed_var(source_var, &packed_source_var, &source_position);

    instruction *dest_cleared_instr = bit_assign_instr(dest_var, FALSE);
    dest_cleared_instr->set_dst(operand());

    immed mask_immed = mask_for_position(source_position);
    in_ldc *mask_ldc = new in_ldc(type_unsigned, operand(), mask_immed);
    in_rrr *and_instr =
            new in_rrr(io_and, type_unsigned, operand(), operand(mask_ldc),
                       operand(packed_source_var));

    int shift_magnitude;
    if_ops shift_opcode;
    if (source_position > dest_position)
      {
        shift_magnitude = source_position - dest_position;
        shift_opcode = io_lsr;
      }
    else if (source_position < dest_position)
      {
        shift_magnitude = dest_position - source_position;
        shift_opcode = io_lsl;
      }
    else
      {
        shift_opcode = io_nop;
      }

    instruction *shifted_instr;
    if (shift_opcode == io_nop)
      {
        shifted_instr = and_instr;
      }
    else
      {
        in_ldc *shift_ldc =
                new in_ldc(type_unsigned, operand(), immed(shift_magnitude));
        shifted_instr =
                new in_rrr(shift_opcode, type_unsigned, operand(),
                           operand(and_instr), operand(shift_ldc));
      }

    in_rrr *or_instr =
            new in_rrr(io_ior, type_unsigned, operand(packed_dest_var),
                       operand(dest_cleared_instr), operand(shifted_instr));
    return or_instr;
  }

/* return an immed for (1 << position) */
static immed mask_for_position(int position)
  {
    immed result;
    eval_status status =
            calc_real_2op_rrr(io_lsl, &result, type_unsigned, immed(1),
                              immed(position));
    assert(status == EVAL_OK);
    return result;
  }

static void data_for_packed_var(var_sym *old_var, var_sym **new_var,
                                int *new_position)
  {
    annote *position_annote =
            old_var->annotes()->peek_annote(k_porky_packed_position);
    assert(position_annote != NULL);
    immed_list *position_immeds = position_annote->immeds();
    assert(position_immeds != NULL);
    assert((*position_immeds)[0].is_symbol());
    assert((*position_immeds)[1].is_integer());

    sym_node *packed_sym = (*position_immeds)[0].symbol();
    assert(packed_sym->is_var());
    *new_var = (var_sym *)packed_sym;
    *new_position = (*position_immeds)[1].integer();
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
