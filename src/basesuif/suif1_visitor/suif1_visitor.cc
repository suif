#include "suif1_visitor.h"

extern void init_suif1_visitor(int &, char **) {}
extern void exit_suif1_visitor(void) {}

extern void enter_suif1_visitor(int *, char **) {}

// Now define the generic visitors
void suif1_visitor::process_fileset(file_set *fs) 
{
  this->pre_process_fileset(fs);
  this->walk_fileset(fs);
  this->post_process_fileset(fs);
}

void suif1_visitor::walk_fileset(file_set *fs)
{
  if (!do_walk_file()) return;
  fs->reset_iter();

  if (do_walk_symtab()) { 
    this->process_symtab(fs->globals());
  }
  file_set_entry *fse;
  while ((fse = fs->next_file())  != NULL)
    this->process_file(fse);
}

void suif1_visitor::process_file(file_set_entry *fse) 
{
  this->pre_process_file(fse);
  if (do_walk_symtab()) { 
    this->process_symtab(fse->symtab());
  }
  this->walk_file(fse);
  this->post_process_file(fse);
}

void suif1_visitor::walk_file(file_set_entry *fse)
{
  if (!do_walk_procedure()) return;
  fse->reset_proc_iter();
  proc_sym *psym;
  while ((psym = fse->next_proc()) != NULL) {
    if (!psym->is_in_memory()) {
      psym->read_proc(TRUE, psym->src_lang() == src_fortran);
    }

    this->process_procedure(psym);
    if (this->do_write_proc())
      psym->write_proc(fse);
    if (this->do_flush_proc()) {
      psym->flush_proc();
    }
  }
}

void suif1_visitor::process_procedure(proc_sym *psym) 
{
  this->pre_process_procedure(psym);
  this->walk_procedure(psym);
  this->post_process_procedure(psym);
}

void suif1_visitor::walk_procedure(proc_sym *psym)
{
  if (!do_walk_block()) return;
  // Only one block in the procedure,
  // but we walk through them as NODES before the type of node
  tree_node *tp = psym->block();
  this->process_block(tp);
}


// Be careful, we will hit EACH tree block 2 times.
// Once as a tree_node and once as a tree_block
// No pre or post-process for this.

void suif1_visitor::process_block(tree_node *tn) 
{
  //  this->pre_process_block(tn);
  this->walk_block(tn);
  //  this->post_process_block(tn);
}
  
void suif1_visitor::walk_block(tree_node *tp)
{
  switch(tp->kind()) {
  case TREE_INSTR:
    this->process_block((tree_instr *)tp);
    break;
  case TREE_LOOP:
    this->process_block((tree_loop *)tp);
    break;
  case TREE_FOR:
    this->process_block((tree_for *)tp);
    break;
  case TREE_IF:
    this->process_block((tree_if *)tp);
    break;
  case TREE_BLOCK:
    if (tp->is_proc()) {
      this->process_block((tree_proc *)tp);
    } else {
      this->process_block((tree_block *)tp);
    }
    break;
  default:
    assert(0);
  }
}

// No pre or post process for this.. Only a walk.
void suif1_visitor::process_block_list(tree_node_list *tnl) 
{
  tree_node_list_iter tnli(tnl);
  while (!tnli.is_empty()) {
    this->process_block(tnli.step());
  }
}
  

void suif1_visitor::process_block(tree_proc *tp) 
{
  this->pre_process_block(tp);
  if (do_walk_symtab()) { 
    this->process_symtab(tp->proc_syms());
  }
  this->walk_block(tp);
  this->post_process_block(tp);
}
  
void suif1_visitor::walk_block(tree_proc *tp)
{
  this->process_block_list(tp->body());
}

void suif1_visitor::process_block(tree_block *tb) 
{
  this->pre_process_block(tb);
  if (do_walk_symtab()) { 
    this->process_symtab(tb->symtab());
  }
  this->walk_block(tb);
  this->post_process_block(tb);
}

void suif1_visitor::walk_block(tree_block *tb)
{
  this->process_block_list(tb->body());
}



void suif1_visitor::process_block(tree_loop *tl) 
{
  this->pre_process_block(tl);
  this->walk_block(tl);
  this->post_process_block(tl);
}


void suif1_visitor::walk_block(tree_loop *tl)
{
  this->process_block_list(tl->body());
  this->process_block_list(tl->test());
}


void suif1_visitor::process_block(tree_for *tf) 
{
  this->pre_process_block(tf);
  this->walk_block(tf);
  this->post_process_block(tf);
}


void suif1_visitor::walk_block(tree_for *tf)
{
  this->process_src_operand(tf->lb_op());
  this->process_src_operand(tf->ub_op());
  this->process_src_operand(tf->step_op());

  this->process_block_list(tf->landing_pad());
  this->process_block_list(tf->body());
}

void suif1_visitor::process_block(tree_instr *ti) 
{
  this->pre_process_block(ti);
  this->walk_block(ti);
  this->post_process_block(ti);
}


void suif1_visitor::walk_block(tree_instr *ti)
{
  if (!do_walk_instruction()) return;
  this->process_instruction(ti->instr());
}

void suif1_visitor::process_block(tree_if *ti) 
{
  this->pre_process_block(ti);
  this->walk_block(ti);
  this->post_process_block(ti);
}


void suif1_visitor::walk_block(tree_if *ti)
{
  this->process_block_list(ti->header());
  this->process_block_list(ti->then_part());
  this->process_block_list(ti->else_part());
}

void suif1_visitor::process_instruction(instruction *i) 
{
  this->pre_process_instruction(i);
  this->walk_instruction(i);
  this->post_process_instruction(i);
}


void suif1_visitor::walk_instruction(instruction *i)
{
  // I believe this works for all instructions
  // Make sure to chack for a detached instruction
  if (i->parent() == NULL ||
      i->parent()->instr() == i) {
    this->process_dst_operand(i->dst_op());
  }

  for (unsigned srcn = 0; srcn< i->num_srcs(); srcn++) {
    this->process_src_operand(i->src_op(srcn));
    if (this->do_replace_operand()) {
      i->set_src_op(srcn, this->post_replace_src_operand(i->src_op(srcn)));
    }
  }
}

void suif1_visitor::process_src_operand(operand op) 
{
  this->pre_process_src_operand(op);
  this->walk_src_operand(op);
  this->post_process_src_operand(op);
}



void suif1_visitor::walk_src_operand(operand op)
{
  if (op.is_instr()) {
    this->process_instruction(op.instr());
  }
}

void suif1_visitor::process_dst_operand(operand op) 
{
  this->pre_process_dst_operand(op);
  this->walk_dst_operand(op);
  this->post_process_dst_operand(op);
}


void suif1_visitor::walk_dst_operand(operand)
{
  
}

// Define the symtab walkers
void suif1_visitor::process_symtab(global_symtab *gst)
{
  this->pre_process_symtab(gst);
  this->walk_symtab(gst);
  this->post_process_symtab(gst);
}

void suif1_visitor::walk_symtab(global_symtab *gst) {}


void suif1_visitor::process_symtab(file_symtab *fst)
{
  this->pre_process_symtab(fst);
  this->walk_symtab(fst);
  this->post_process_symtab(fst);
}
void suif1_visitor::walk_symtab(file_symtab *fst) {}

void suif1_visitor::process_symtab(proc_symtab *pst)
{
  this->pre_process_symtab(pst);
  this->walk_symtab(pst);
  this->post_process_symtab(pst);
}
void suif1_visitor::walk_symtab(proc_symtab *pst) {}

void suif1_visitor::process_symtab(block_symtab *bst)
{
  this->pre_process_symtab(bst);
  this->walk_symtab(bst);
  this->post_process_symtab(bst);
}

void suif1_visitor::walk_symtab(block_symtab *bst) {}

  
