/*  Association List Implementation */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#define _MODULE_ "libsuif.a"

#pragma implementation "alist.h"

#define RCS_BASE_FILE alist_cc

#include "misc.h"
#include "glist.h"
#include "mtflist.h"
#include "alist.h"

RCS_BASE(
    "$Id: alist.cc,v 1.2 1999/08/25 03:29:08 brm Exp $")


/*****************************************************************************/


/*
 *  The search functions in amtflist use the same search function as
 *  mtflist by providing a separate function to compare the keys.
 */

static boolean
amtflist_pred (alist_e *a, void *k)
{
    return a->key == k;
}


alist_e *
amtflist::search (void *k)
{
    return (alist_e *)mtflist::lookup((mtflist_test_f)amtflist_pred, k);
}


const void *
amtflist::lookup (void *k)
{
    alist_e *ap = search(k);
    assert_msg(ap, ("amtflist::lookup - attempt to lookup %x failed", k));
    return ap->info;
}


boolean
amtflist::exists (void *k, const void **i)
{
    alist_e *ap = search(k);
    if (ap) {
	if (i) *i = ap->info;
	return TRUE;
    }
    return FALSE;
}


