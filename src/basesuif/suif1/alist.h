/*  Association List Definitions */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef ALIST_H
#define ALIST_H

#pragma interface

RCS_HEADER(alist_h,
    "$Id: alist.h,v 1.2 1999/08/25 03:29:08 brm Exp $")

/*
 *  Association list elements include both a key and a data pointer.
 *  This allows access to a particular list element specified by a key.
 */

template<typename T>
class ass_list_e : public glist_e {

private:
    /* We make explicit copy constructor and assignment operator and
     * make them private to foil C++'s automatic default versions. */
    ass_list_e(const ass_list_e &) { assert(FALSE); }
    ass_list_e &operator=(const ass_list_e &) { assert(FALSE); return *this; }

public:
    const void *key;
    T info;

    ass_list_e(const void *k, T i) : key(k), info(i) { }
    ass_list_e *next()			{ return (ass_list_e *)glist_e::next(); }
};

typedef ass_list_e<const void *>	alist_e;


/*
 *  The alist class defines several functions to search for a
 *  particular key and return either the list element or the info
 *  pointer from that element.  Several wrapper function also provide
 *  type-correct access to the underlying glist functions.
 */

template<typename T>
class ass_list : public glist {

public:
    ass_list_e<T> *head() const		{ return (ass_list_e<T> *)glist::head(); }
    ass_list_e<T> *tail() const		{ return (ass_list_e<T> *)glist::tail(); }
    ass_list_e<T> *push(ass_list_e<T> *e)		{ return (ass_list_e<T> *)glist::push(e); }
    ass_list_e<T> *pop()			{ return (ass_list_e<T> *)glist::pop(); }
    ass_list_e<T> *remove(ass_list_e<T> *e)		{ return (ass_list_e<T> *)glist::remove(e); }

    ass_list_e<T> *enter(const void *k, T i)	
					{ return push(new ass_list_e<T>(k, i)); }

    /*
     *  The following two functions search through the elements of an
     *  alist to find a match for the specified key.  The return value
     *  varies.
     */
    ass_list_e<T> *search(const void *k) const {
	ass_list_e<T> *ap = (ass_list_e<T> *)head();
	while (ap) {
	    if (ap->key == k)
		return ap;
	    ap = ap->next();
	}
	return NULL;
    }
    const void *lookup(const void *k) const {
	ass_list_e<T> *ap = search(k);
	assert_msg(ap, ("alist::lookup - attempt to lookup %x failed", k));
	return ap->info;
    }

    /*
     *  Check if the list contains the specified key.  If so, optionally
     *  return the associated info pointer through the second argument.
     */
    boolean exists(const void *k, T *i = NULL) const {
	ass_list_e<T> *ap = search(k);
	if (ap) {
	    if (i) *i = ap->info;
	    return TRUE;
	}
	return FALSE;
    }
};

typedef ass_list<const void *>	alist;


/*
 *  The amtflist class combines the features of the association lists
 *  with those of the move-to-front lists.  The interface is equivalent
 *  to the alist interface.
 */

class amtflist : public mtflist {
public:
    alist_e *head()			{ return (alist_e*)mtflist::head(); }
    alist_e *tail()			{ return (alist_e*)mtflist::tail(); }
    alist_e *push(alist_e *e)		{ return (alist_e*)mtflist::push(e); }
    alist_e *pop()			{ return (alist_e*)mtflist::pop(); }
    alist_e *remove(alist_e *e)		{ return (alist_e*)mtflist::remove(e);}

    alist_e *enter(void *k, void *i)	{ return push(new alist_e(k, i)); }
    alist_e *search(void *k);
    const void *lookup(void *k);
    boolean exists(void *k, const void **i = NULL);
};


/*
 *  This iterator works for both alists and amtflists.
 */

template<typename T>
class ass_list_iter : public glist_iter {

private:
    /* We make explicit copy constructor and assignment operator and
     * make them private to foil C++'s automatic default versions. */
    ass_list_iter(const ass_list_iter &) { assert(FALSE); }
    ass_list_iter &operator=(const ass_list_iter &) { assert(FALSE); return *this; }

public:
    ass_list_iter()			{ }
    ass_list_iter(const ass_list<T> *l)		{ reset(l); }
    ass_list_iter(const amtflist *l)	{ reset(l); }

    ass_list_e<T> *step()	{ return (ass_list_e<T> *)glist_iter::step(); }
    ass_list_e<T> *peek()	{ return (ass_list_e<T> *)glist_iter::peek(); }
    ass_list_e<T> *cur_elem()	{ return (ass_list_e<T> *)glist_iter::cur_elem(); }
};

typedef ass_list_iter<const void *>	alist_iter;

#endif /* ALIST_H */
