/*  Include file for internal declarations for the SUIF check library */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

#ifndef CHECK_INTERNAL_H
#define CHECK_INTERNAL_H

#include "check.h"
#include <stdarg.h>

RCS_HEADER(check_internal_h,
     "$Id: check_internal.h,v 1.1.1.1 1998/06/16 15:15:44 brm Exp $")


/*----------------------------------------------------------------------*
    Begin Declarations
 *----------------------------------------------------------------------*/

extern base_symtab *current_scope;
extern tree_node *current_node;

extern void problem(char *fmt, ...);
extern void vproblem(char *fmt, va_list ap);
extern void problem_instr(tree_node *the_node, instruction *the_instr,
                          char *fmt, ...);
extern void vproblem_instr(tree_node *the_node, instruction *the_instr,
                           char *fmt, va_list ap);

/*----------------------------------------------------------------------*
    End Declarations
 *----------------------------------------------------------------------*/

#endif
