/* file "fred_inline.cc" of the fred_inline program for SUIF */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for the fred_inline program
 *  for SUIF.
 */

#define RCS_BASE_FILE fred_inline_cc

#include <suif1.h>
#include <useful.h>

RCS_BASE(
    "$Id: fred_inline.cc,v 1.1.1.1 1998/06/16 15:17:27 brm Exp $")

INCLUDE_SUIF_COPYRIGHT


DECLARE_DLIST_CLASS(call_list, in_cal *);


static call_list pending_calls;


static void usage(void);
static void do_proc(tree_proc *the_proc);
static void inline_fred_on_object(suif_object *the_object);
static void unstatic_proc(tree_proc *the_proc);
static void unstatic_symtab(base_symtab *source, base_symtab *target);
static void remove_var_and_children(base_symtab *the_symtab, var_sym *the_var);
static void add_var_and_children(base_symtab *the_symtab, var_sym *the_var);


extern int main(int argc, char **argv)
  {
    /* @@@ -- handle varargs correctly */
    start_suif(argc, argv);

    if ((argc < 3) || (argc % 2 != 1))
        usage();

    for (int arg_num = 1; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            if (!this_proc_sym->is_in_memory())
              {
                this_proc_sym->read_proc(TRUE, FALSE);
                if (this_proc_sym->peek_annote(k_fred) != NULL)
                    unstatic_proc(this_proc_sym->block());
              }
            do_proc(this_proc_sym->block());
            this_proc_sym->write_proc(fse);
            if (this_proc_sym->peek_annote(k_fred) == NULL)
                this_proc_sym->flush_proc();
          }
      }

    exit_suif();
    return 0;
  }


static void usage(void)
  {
    fprintf(stderr, "usage: %s <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void do_proc(tree_proc *the_proc)
  {
    walk(the_proc, &inline_fred_on_object);
    while (!pending_calls.is_empty())
      {
        in_cal *this_call = pending_calls.pop();
        inline_call(this_call);
      }
  }

static void inline_fred_on_object(suif_object *the_object)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_cal)
        return;
    in_cal *the_call = (in_cal *)the_instr;
    proc_sym *the_proc = proc_for_call(the_call);
    if (the_proc == NULL)
        return;
    if (the_proc->peek_annote(k_fred) == NULL)
        return;
    if (!the_proc->is_in_memory())
      {
        if (!the_proc->is_readable())
            return;
        the_proc->read_proc();
        unstatic_proc(the_proc->block());
      }
    pending_calls.append(the_call);
  }

static void unstatic_proc(tree_proc *the_proc)
  {
    unstatic_symtab(the_proc->proc_syms(), the_proc->scope());
  }

static void unstatic_symtab(base_symtab *source, base_symtab *target)
  {
    while (!source->var_defs()->is_empty())
      {
        var_def *this_def = source->var_defs()->head()->contents;
        var_sym *this_var = this_def->variable();
        source->remove_def(this_def);
        target->add_def(this_def);
        remove_var_and_children(source, this_var);
        add_var_and_children(target, this_var);
      }

    base_symtab_list_iter child_iter(source->children());
    while (!child_iter.is_empty())
      {
        base_symtab *this_child = child_iter.step();
        unstatic_symtab(this_child, target);
      }
  }

static void remove_var_and_children(base_symtab *the_symtab, var_sym *the_var)
  {
    the_symtab->remove_sym(the_var);
    unsigned num_children = the_var->num_children();
    for (unsigned child_num = 0; child_num < num_children; ++child_num)
        remove_var_and_children(the_symtab, the_var->child_var(child_num));
  }

static void add_var_and_children(base_symtab *the_symtab, var_sym *the_var)
  {
    the_symtab->add_sym(the_var);
    unsigned num_children = the_var->num_children();
    for (unsigned child_num = 0; child_num < num_children; ++child_num)
        add_var_and_children(the_symtab, the_var->child_var(child_num));
  }
