/* file "main.cc" of the special_decalpha program for SUIF */

/*  Copyright (c) 1994, 1995, 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for the special_decalpha
 *  program for SUIF.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:18:50 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        The special_decalpha program does special processing to
        interface with the DEC ALPHA back-end compiler.  It does this
        by means of ``#pragma'' lines, both ``#pragma'' lines in the
        input from system header files that give SUIF information
        about the back-end, and ``#pragma'' lines that SUIF puts in
        the output C files to communicate information SUIF has to the
        back-end compiler.  It also does special optimizations that
        are specific to the DEC ALPHA back-end.

        This program should be run twice, once after the front end
        with the ``-in'' option to read in annotations, and once just
        before s2c with the ``-out'' option.  The
        ``-prefetches-to-dec'' and ``-kill-dec-prefetches'' options
        are only used if prefetching instructions need to be passed to
        the DEC Fortran compiler.  If that is the case, this program
        should be run two more times, once with ``-prefetches-to-dec''
        before the Fortranizer, then once with
        ``-kill-dec-prefetches'' after the Fortranizer before the
        remaining code is translated to C.

        These are the specific functions currently performed by this
        program:

            -in option:
              * Read ``#pragma _KAP no side effects'' annotations and
                add "pure function" annotations based on that
                information.

            -out option:
              * Add output ``#pragma _KAP no side effects''
                annotations for functions with "pure function"
                annotations.
              * Add output ``#pragma intrinsic'' annotations for input
                ``#pragma intrinsic'' annotations.
              * Replace floating point io_abs instructions with calls
                to fabs or fabsf because the back-end treats these as
                intrinsics and can produce much better code than it
                could with the abs() macro that would otherwise be
                used for this instructions.
              * Add output ``#pragma ivdep'' annotations just before
                each for loop with the SUIF ``doall'' annotation.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Constants
 *----------------------------------------------------------------------*/

#define DEC_PREFETCH_NAME "dfor$prefetch_"

/*----------------------------------------------------------------------*
    End Private Constants
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_C_pragma;
static const char *k_s2c_pragma;
static const char *k_doall;
static proc_sym *float_abs_sym = NULL;
static proc_sym *double_abs_sym = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void process_in_fse(file_set_entry *the_fse);
static void process_out_fse(file_set_entry *the_fse);
static void handle_possible_intrinsic_annote(file_set_entry *the_fse,
                                             annote *the_annote);
static void add_no_side_effects_annotes(global_symtab *the_symtab,
                                       file_set_entry *the_fse);
static void replace_fp_abs_on_proc(tree_proc *the_proc);
static void replace_fp_abs_on_node(tree_node *the_node, void *);
static void replace_fp_abs_on_instr(instruction *the_instr);
static void ivdep_pragmas_on_proc(tree_proc *the_proc);
static void ivdep_pragmas_on_node(tree_node *the_node, void *);
static proc_sym *get_abs_func(char *name, type_node *arg_type,
                              char *fortran_intrinsic_name);
static void prefetches_to_dec_form_on_proc(tree_proc *the_proc);
static void prefetches_to_dec_form_on_object(suif_object *the_object,
                                             so_walker *the_walker);
static proc_sym *get_dec_prefetch_proc(void);
static void kill_dec_prefetches_on_proc(tree_proc *the_proc);
static void kill_dec_prefetches_on_object(suif_object *the_object,
                                          so_walker *the_walker);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    boolean prefetches_to_dec_flag = FALSE;
    boolean kill_dec_prefetches_flag = FALSE;

    start_suif(argc, argv);

    if ((argc < 4) || (argc % 2 != 0))
        usage();

    boolean is_in;
    if (strcmp(argv[1], "-in") == 0)
        is_in = TRUE;
    else if (strcmp(argv[1], "-out") == 0)
        is_in = FALSE;
    else if (strcmp(argv[1], "-prefetches-to-dec") == 0)
        prefetches_to_dec_flag = TRUE;
    else if (strcmp(argv[1], "-kill-dec-prefetches") == 0)
        kill_dec_prefetches_flag = TRUE;
    else
        usage();

    ANNOTE(k_C_pragma, "C pragma", TRUE);
    ANNOTE(k_s2c_pragma, "s2c pragma", TRUE);
    ANNOTE(k_doall, "doall", TRUE);

    for (int arg_num = 2; arg_num < argc; arg_num += 2)
        fileset->add_file(argv[arg_num], argv[arg_num + 1]);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;

        if ((!prefetches_to_dec_flag) && (!kill_dec_prefetches_flag))
          {
            if (is_in)
                process_in_fse(fse);
            else
                process_out_fse(fse);
          }

        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            if (prefetches_to_dec_flag)
              {
                prefetches_to_dec_form_on_proc(this_proc_sym->block());
              }
            else if (kill_dec_prefetches_flag)
              {
                kill_dec_prefetches_on_proc(this_proc_sym->block());
              }
            else if (!is_in)
              {
                replace_fp_abs_on_proc(this_proc_sym->block());
                ivdep_pragmas_on_proc(this_proc_sym->block());
              }
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }

      }

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr,
            "usage: %s { -in | -out | -prefetches-to-dec |\n"
            "                          -kill-dec-prefetches } <infile> "
            "<outfile>\n"
            "                                               { <infile> "
            "<outfile> }*\n", _suif_prog_base_name);
    exit(1);
  }

static void process_in_fse(file_set_entry *the_fse)
  {
    annote_list_iter annote_iter(the_fse->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();
        if (this_annote->name() == k_C_pragma)
          {
            immed_list *this_data = this_annote->immeds();
            if ((this_data->count() == 7) &&
                ((*this_data)[0] == immed("_KAP")) &&
                ((*this_data)[1] == immed("no")) &&
                ((*this_data)[2] == immed("side")) &&
                ((*this_data)[3] == immed("effects")) &&
                ((*this_data)[4] == immed("(")) &&
                ((*this_data)[6] == immed(")")) &&
                (*this_data)[5].is_symbol())
              {
                sym_node *this_sym = (*this_data)[5].symbol();
                if (this_sym->is_proc())
                  {
                    if (this_sym->annotes()->peek_annote(k_pure_function) ==
                        NULL)
                      {
                        this_sym->append_annote(k_pure_function, NULL);
                      }
                  }
              }
          }
      }
  }

static void process_out_fse(file_set_entry *the_fse)
  {
    annote_list_iter annote_iter(the_fse->annotes());
    while (!annote_iter.is_empty())
      {
        annote *this_annote = annote_iter.step();
        handle_possible_intrinsic_annote(the_fse, this_annote);
      }

    add_no_side_effects_annotes(fileset->globals(), the_fse);
    add_no_side_effects_annotes(the_fse->symtab(), the_fse);
  }

static void handle_possible_intrinsic_annote(file_set_entry *the_fse,
                                             annote *the_annote)
  {
    if (the_annote->name() != k_C_pragma)
        return;
    immed_list *this_data = the_annote->immeds();
    if ((this_data->count() < 4) || ((*this_data)[0] != immed("intrinsic")) ||
        ((*this_data)[1] != immed("(")))
      {
        return;
      }
    sym_node_list the_syms;
    for (unsigned immed_num = 2;
         immed_num < (unsigned)(this_data->count() - 1); ++immed_num)
      {
        immed this_immed = (*this_data)[immed_num];
        if ((!this_immed.is_symbol()) || (this_immed.offset() != 0))
            return;
        sym_node *this_sym = this_immed.symbol();
        if (!this_sym->is_proc())
            return;
        the_syms.append(this_sym);
      }
    if ((*this_data)[this_data->count() - 1] != immed(")"))
        return;

    immed_list *new_immeds = new immed_list;
    new_immeds->append(immed("intrinsic ("));
    sym_node *this_sym = the_syms.pop();
    new_immeds->append(immed(this_sym));
    while (!the_syms.is_empty())
      {
        sym_node *this_sym = the_syms.pop();
        new_immeds->append(immed(","));
        new_immeds->append(immed(this_sym));
      }
    new_immeds->append(immed(")"));
    the_fse->append_annote(k_s2c_pragma, new_immeds);
  }

static void add_no_side_effects_annotes(global_symtab *the_symtab,
                                       file_set_entry *the_fse)
  {
    sym_node_list_iter sym_iter(the_symtab->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (this_sym->is_proc() &&
            (this_sym->annotes()->peek_annote(k_pure_function) != NULL))
          {
            immed_list *new_immeds = new immed_list;
            new_immeds->append(immed("_KAP no side effects ("));
            new_immeds->append(this_sym);
            new_immeds->append(immed(")"));
            the_fse->append_annote(k_s2c_pragma, new_immeds);
          }
      }
  }

static void replace_fp_abs_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&replace_fp_abs_on_node, NULL);
  }

static void replace_fp_abs_on_node(tree_node *the_node, void *)
  {
    if (the_node->is_instr())
      {
        tree_instr *the_tree_instr = (tree_instr *)the_node;
        replace_fp_abs_on_instr(the_tree_instr->instr());
      }
  }

static void replace_fp_abs_on_instr(instruction *the_instr)
  {
    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand this_op = the_instr->src_op(src_num);
        if (this_op.is_expr())
            replace_fp_abs_on_instr(this_op.instr());
      }

    if ((the_instr->opcode() == io_abs) &&
        (the_instr->result_type()->is_same(type_float) ||
         the_instr->result_type()->is_same(type_double)))
      {
        in_rrr *the_rrr = (in_rrr *)the_instr;

        proc_sym *the_proc_sym;
        if (the_rrr->result_type()->is_same(type_float))
          {
            if (float_abs_sym == NULL)
                float_abs_sym = get_abs_func("fabsf", type_float, "ABS");
            the_proc_sym = float_abs_sym;
          }
        else
          {
            if (double_abs_sym == NULL)
                double_abs_sym = get_abs_func("fabs", type_double, "DABS");
            the_proc_sym = double_abs_sym;
          }

        in_ldc *new_proc_ldc =
                new in_ldc(the_proc_sym->type()->ptr_to(), operand(),
                           immed(the_proc_sym));
        in_cal *new_cal =
                new in_cal(the_instr->result_type(), operand(),
                           operand(new_proc_ldc), 1);

        operand src_op = the_rrr->src_op();
        if (src_op.is_expr())
            src_op.remove();

        new_cal->set_argument(0, src_op);
        replace_instruction(the_rrr, new_cal);
        delete the_rrr;
      }
  }

static void ivdep_pragmas_on_proc(tree_proc *the_proc)
  {
    the_proc->map(&ivdep_pragmas_on_node, NULL);
  }

static void ivdep_pragmas_on_node(tree_node *the_node, void *)
  {
    if (the_node->is_for() && (the_node->annotes()->peek_annote(k_doall)))
      {
        immed_list *new_immeds = new immed_list;
        new_immeds->append(immed("ivdep"));
        in_rrr *new_mark = new in_rrr(io_mrk);
        new_mark->append_annote(k_s2c_pragma, new_immeds);
        tree_instr *new_node = new tree_instr(new_mark);
        the_node->parent()->insert_before(new_node, the_node->list_e());
      }
  }

static proc_sym *get_abs_func(char *name, type_node *arg_type,
                              char *fortran_intrinsic_name)
  {
    proc_sym *result = fileset->globals()->lookup_proc(name, FALSE);
    if (result == NULL)
      {
        func_type *the_func_type = new func_type(arg_type, 1);
        the_func_type->set_arg_type(0, arg_type);
        the_func_type =
                (func_type *)(arg_type->parent()->install_type(the_func_type));
        result =
                fileset->globals()->new_proc(the_func_type, src_unknown, name);
      }
    if (result->peek_annote(k_fortran_intrinsic) == NULL)
      {
        result->append_annote(k_fortran_intrinsic,
                              new immed_list(immed(fortran_intrinsic_name)));
      }
    return result;
  }

static void prefetches_to_dec_form_on_proc(tree_proc *the_proc)
  {
    walk(the_proc, &prefetches_to_dec_form_on_object);
  }

static void prefetches_to_dec_form_on_object(suif_object *the_object,
                                             so_walker *the_walker)
  {
    if (!the_object->is_instr_obj())
        return;
    instruction *the_instr = (instruction *)the_object;
    if (the_instr->opcode() != io_gen)
        return;
    in_gen *the_gen = (in_gen *)the_instr;
    if (strcmp(the_gen->name(), "prefetch") != 0)
        return;
    if (the_gen->num_srcs() != 1)
      {
        error_line(1, the_gen,
                   "confused by prefetch instruction with number of source "
                   "operands not equal to one");
      }
    proc_sym *dec_prefetch_proc = get_dec_prefetch_proc();
    operand argument = the_gen->src_op(0);
    argument.remove();
    in_cal *new_call =
            new in_cal(type_void, operand(), addr_op(dec_prefetch_proc),
                       argument);
    the_walker->replace_object(new_call);
    delete the_gen;
  }

static proc_sym *get_dec_prefetch_proc(void)
  {
    sym_node_list_iter sym_iter(fileset->globals()->symbols());
    while (!sym_iter.is_empty())
      {
        sym_node *this_sym = sym_iter.step();
        if (!this_sym->is_proc())
            continue;
        proc_sym *this_proc = (proc_sym *)this_sym;
        if (strcmp(this_proc->name(), DEC_PREFETCH_NAME) != 0)
            continue;
        func_type *this_func_type = this_proc->type();
        if (this_func_type->return_type()->op() != TYPE_VOID)
            continue;
        if (this_func_type->args_known())
            continue;
        return this_proc;
      }
    func_type *prefetch_type = new func_type(type_void);
    prefetch_type =
            (func_type *)(fileset->globals()->install_type(prefetch_type));
    return fileset->globals()->new_proc(prefetch_type, src_unknown,
                                        DEC_PREFETCH_NAME);
  }

static void kill_dec_prefetches_on_proc(tree_proc *the_proc)
  {
    walk(the_proc, &kill_dec_prefetches_on_object);
  }

static void kill_dec_prefetches_on_object(suif_object *the_object,
                                          so_walker *the_walker)
  {
    if (!the_object->is_tree_obj())
        return;
    tree_node *the_tree_node = (tree_node *)the_object;
    if (!the_tree_node->is_instr())
        return;
    tree_instr *the_tree_instr = (tree_instr *)the_tree_node;
    the_walker->set_skip();
    instruction *the_instr = the_tree_instr->instr();
    if (the_instr->opcode() != io_cal)
        return;
    in_cal *the_call = (in_cal *)the_instr;
    proc_sym *callee = proc_for_call(the_call);
    if (callee == NULL)
        return;
    if (strcmp(callee->name(), DEC_PREFETCH_NAME) != 0)
        return;
    if (callee->parent() != fileset->globals())
        return;
    kill_node(the_tree_instr);
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
