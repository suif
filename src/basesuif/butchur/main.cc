/* file "main.cc" of the butchur program for SUIF */

/*  Copyright (c) 1996 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for butchur.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.2 1999/08/25 03:27:13 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char *k_butchur_selected = NULL;
static const char *k_butchur_reachable = NULL;
static file_set_entry *the_fse;
static boolean keep_unused = FALSE;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static char **extract_names(char *name_list, size_t *count);
static void deallocate_names(char **name_list, size_t count);
static void select_only(size_t num_definitions, char **def_names);
static void select_all_but(size_t num_definitions, char **def_names);
static file_set_entry *get_new_fse(suif_object *the_object);
static void do_procedure_bodies(void);
static global_symtab *find_new_symtab(suif_object *the_object);
static void setup_replacements(void);
static void mark_reachable_sym(sym_node *the_sym, so_walker *the_walker);
static void mark_reachable_type(type_node *the_type, so_walker *the_walker);
static void mark_reachable_from_object(suif_object *the_object,
                                       file_set_entry *new_fse);
static void mark_reachable_from_annotes(suif_object *the_object,
                                        file_set_entry *new_fse);
static file_set_entry *unwritten_fse(void);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    static cmd_line_option option_table[] =
      {
        {CLO_NOARG, "-keep-unused", NULL, &keep_unused}
      };

    start_suif(argc, argv);

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    k_butchur_selected = lexicon->enter("butchur selected")->sp;
    k_butchur_reachable = lexicon->enter("butchur reachable")->sp;

    if (argc != 5)
        usage();

    the_fse = fileset->add_file(argv[3], argv[4]);
    size_t count;
    char **name_list = extract_names(argv[2], &count);
    if (strcmp(argv[1], "-only") == 0)
        select_only(count, name_list);
    else if (strcmp(argv[1], "-all-but") == 0)
        select_all_but(count, name_list);
    else
        usage();
    deallocate_names(name_list, count);

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr,
            "usage: %s <options> -only <defname>{,<defname>}* <infile> "
                    "<outfile>\n"
            "       %s <options> -all-but <defname>{,<defname>}* <infile> "
                    "<outfile>\n"
            "  options:\n"
            "       -keep-unused: keep all global symbol table objects, even\n"
            "                     if they are not used\n",
            _suif_prog_base_name, _suif_prog_base_name);
    exit(1);
  }

static char **extract_names(char *name_list, size_t *count)
  {
    size_t num_names = 1;
    char *follow = name_list;
    while (*follow != 0)
      {
        if (*follow == ',')
            ++num_names;
        ++follow;
      }
    *count = num_names;
    char **result = new char *[num_names];
    size_t word_num = 0;
    follow = name_list;
    while (TRUE)
      {
        char *inner = follow;
        while ((*inner != 0) && (*inner != ','))
            ++inner;
        size_t word_len = inner - follow;
        char *this_word = new char[word_len + 1];
        memcpy(this_word, follow, word_len);
        this_word[word_len] = 0;
        result[word_num] = this_word;
        ++word_num;
        if (*inner == 0)
            break;
        follow = inner + 1;
      }
    assert(word_num == num_names);
    return result;
  }

static void deallocate_names(char **name_list, size_t count)
  {
    for (size_t name_num = 0; name_num < count; ++name_num)
        delete[] name_list[name_num];
    delete[] name_list;
  }

static void select_only(size_t num_definitions, char **def_names)
  {
    boolean *used_defs = new boolean[num_definitions];
      {
        for (size_t def_num = 0; def_num < num_definitions; ++def_num)
            used_defs[def_num] = FALSE;
      }

    the_fse->reset_proc_iter();
    while (TRUE)
      {
        proc_sym *the_proc = the_fse->next_proc();
        if (the_proc == NULL)
            break;
	size_t def_num = 0;
        for ( ; def_num < num_definitions; ++def_num)
          {
            if (strcmp(def_names[def_num], the_proc->name()) == 0)
              {
                the_proc->append_annote(k_butchur_selected, the_fse);
                used_defs[def_num] = TRUE;
                break;
              }
          }
        if (def_num >= num_definitions)
            the_proc->append_annote(k_butchur_selected, unwritten_fse());
      }

    var_def_list_iter def_iter(the_fse->symtab()->var_defs());
    while (!def_iter.is_empty())
      {
        var_def *this_def = def_iter.step();
	size_t def_num = 0;
        for ( ; def_num < num_definitions; ++def_num)
          {
            if (strcmp(def_names[def_num], this_def->variable()->name()) == 0)
              {
                this_def->append_annote(k_butchur_selected, the_fse);
                used_defs[def_num] = TRUE;
                break;
              }
          }
        if (def_num >= num_definitions)
            this_def->append_annote(k_butchur_selected, unwritten_fse());
      }

    for (size_t def_num = 0; def_num < num_definitions; ++def_num)
      {
        if (!used_defs[def_num])
          {
            warning_line(the_fse, "definition `%s' not found",
                         def_names[def_num]);
          }
      }
    delete[] used_defs;

    do_procedure_bodies();
  }

static void select_all_but(size_t num_definitions, char **def_names)
  {
    boolean *used_defs = new boolean[num_definitions];
      {
        for (size_t def_num = 0; def_num < num_definitions; ++def_num)
            used_defs[def_num] = FALSE;
      }

    the_fse->reset_proc_iter();
    while (TRUE)
      {
        proc_sym *the_proc = the_fse->next_proc();
        if (the_proc == NULL)
            break;
	size_t def_num = 0;
        for ( ; def_num < num_definitions; ++def_num)
          {
            if (strcmp(def_names[def_num], the_proc->name()) == 0)
              {
                used_defs[def_num] = TRUE;
                break;
              }
          }
        if (def_num >= num_definitions)
            the_proc->append_annote(k_butchur_selected, the_fse);
        else
            the_proc->append_annote(k_butchur_selected, unwritten_fse());
      }

    var_def_list_iter def_iter(the_fse->symtab()->var_defs());
    while (!def_iter.is_empty())
      {
        var_def *this_def = def_iter.step();
	size_t def_num = 0;
        for ( ; def_num < num_definitions; ++def_num)
          {
            if (strcmp(def_names[def_num], this_def->variable()->name()) == 0)
              {
                used_defs[def_num] = TRUE;
                break;
              }
          }
        if (def_num >= num_definitions)
            this_def->append_annote(k_butchur_selected, the_fse);
        else
            this_def->append_annote(k_butchur_selected, unwritten_fse());
      }

    for (size_t def_num = 0; def_num < num_definitions; ++def_num)
      {
        if (!used_defs[def_num])
          {
            warning_line(the_fse, "definition `%s' not found",
                         def_names[def_num]);
          }
      }
    delete[] used_defs;

    do_procedure_bodies();
  }

static file_set_entry *get_new_fse(suif_object *the_object)
  {
    return (file_set_entry *)(the_object->peek_annote(k_butchur_selected));
  }

static void do_procedure_bodies(void)
  {
    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *the_fse = fileset->next_file();
        if (the_fse == NULL)
            break;

        the_fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *the_proc = the_fse->next_proc();
            if (the_proc == NULL)
                break;
            the_proc->read_proc(TRUE, FALSE);
            file_set_entry *new_fse = get_new_fse(the_proc);
            mark_reachable_from_object(the_proc->block(), new_fse);
            the_proc->flush_proc();
          }

        var_def_list_iter def_iter(the_fse->symtab()->var_defs());
        while (!def_iter.is_empty())
          {
            var_def *the_def = def_iter.step();
            file_set_entry *new_fse = get_new_fse(the_def);
            mark_reachable_from_object(the_def, new_fse);
          }

        mark_reachable_from_annotes(the_fse, the_fse);
        mark_reachable_from_annotes(the_fse->symtab(), the_fse);
      }

    setup_replacements();

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *the_fse = fileset->next_file();
        if (the_fse == NULL)
            break;

        the_fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *the_proc = the_fse->next_proc();
            if ((the_proc == NULL) || (!the_proc->is_readable()))
                break;
            the_proc->read_proc(TRUE, FALSE);
            file_set_entry *new_fse = get_new_fse(the_proc);
            tree_proc *the_block = the_proc->block();
            do_replacement(the_block);
            /* NOTE: do_replacement() might have changed the symbol
             * that the_block is attached to, so it's important to use
             * the_block->proc() instead of the old value of
             * the_proc. */
            the_proc = the_block->proc();
            if ((new_fse != NULL) && (new_fse->out_name() != NULL))
                the_proc->write_proc(new_fse);
            the_proc->flush_proc();
          }

        var_def_list_iter def_iter(the_fse->symtab()->var_defs());
        while (!def_iter.is_empty())
          {
            var_def *the_def = def_iter.step();
            file_set_entry *new_fse = get_new_fse(the_def);
            do_replacement(the_def);
            if (new_fse != the_fse)
              {
                the_def->parent()->remove_def(the_def);
                if (new_fse != NULL)
                    new_fse->symtab()->add_def(the_def);
                else
                    delete the_def;
              }
          }

        sym_node_list_iter sym_iter(the_fse->symtab()->symbols());
        while (!sym_iter.is_empty())
          {
            sym_node *this_sym = sym_iter.step();
            if (this_sym->peek_annote(k_replacement) != NULL)
              {
                if (this_sym->is_var())
                  {
                    var_sym *this_var = (var_sym *)this_sym;
                    assert(!this_var->has_var_def());
                    if (this_var->parent_var() != NULL)
                        this_var->parent_var()->remove_child(this_var);
                    while (this_var->num_children() > 0)
                        this_var->remove_child(this_var->child_var(0));
                  }
                the_fse->symtab()->remove_sym(this_sym);
                delete this_sym;
              }
          }

        type_node_list_iter type_iter(the_fse->symtab()->types());
        while (!type_iter.is_empty())
          {
            type_node *this_type = type_iter.step();
            if (this_type->peek_annote(k_replacement) != NULL)
              {
                the_fse->symtab()->remove_type(this_type);
                delete this_type;
              }
          }
      }
  }

static global_symtab *find_new_symtab(suif_object *the_object)
  {
    file_set_entry_list *reachable_list =
            (file_set_entry_list *)(the_object->peek_annote(
                    k_butchur_reachable));
    if (reachable_list == NULL)
        return NULL;
    if (reachable_list->count() > 1)
        return fileset->globals();
    return reachable_list->head()->contents->symtab();
  }

static void setup_replacements(void)
  {
    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *the_fse = fileset->next_file();
        if (the_fse == NULL)
            break;

        sym_node_list_iter sym_iter(the_fse->symtab()->symbols());
        while (!sym_iter.is_empty())
          {
            sym_node *the_sym = sym_iter.step();
            global_symtab *target_symtab = find_new_symtab(the_sym);
            if ((target_symtab != NULL) &&
                (target_symtab != the_fse->symtab()))
              {
                sym_node *new_sym = the_sym->copy();
                new_sym->copy_annotes(the_sym);
                target_symtab->add_sym(new_sym);
                the_sym->append_annote(k_replacement, new immed_list(new_sym));
                do_replacement(new_sym);
                if (!target_symtab->is_file())
                  {
                    const char *old_name = the_sym->name();
                    size_t num_space = 50;
                    size_t old_name_length = strlen(old_name);
                    char *new_name =
                            new char[old_name_length + 13 + num_space];
                    sprintf(new_name, "__static@@__%s", old_name);
                    if (target_symtab->lookup_sym(new_name, the_sym->kind())
                        != NULL)
                      {
                        i_integer suffix = 1;
                        char *num_place = &(new_name[old_name_length + 12]);
                        do
                          {
                            if (suffix.written_length() > num_space)
                              {
                                delete[] new_name;
                                new_name =
                                        new char[old_name_length + 13 +
                                                 (2 * num_space)];
                                sprintf(new_name, "__static@@__%s", old_name);
                                num_place = &(new_name[old_name_length + 12]);
                                num_space *= 2;
                              }
                            suffix.write(num_place);
                            ++suffix;
                          } while (target_symtab->lookup_sym(new_name,
                                                             the_sym->kind())
                                   != NULL);
                      }
                    new_sym->set_name(new_name);
                    delete[] new_name;
                  }
              }
          }

        /* Now fix child variable relationships of the new symbols. */
        sym_node_list_iter fix_sym_iter(the_fse->symtab()->symbols());
        while (!fix_sym_iter.is_empty())
          {
            sym_node *the_sym = fix_sym_iter.step();
            if (!the_sym->is_var())
                continue;
            var_sym *the_var = (var_sym *)the_sym;
            var_sym *parent_var = the_var->parent_var();
            if (parent_var == NULL)
                continue;
            annote *replacement_annote =
                    the_var->annotes()->peek_annote(k_replacement);
            if (replacement_annote == NULL)
                continue;
            annote *parent_replacement_annote =
                    parent_var->annotes()->peek_annote(k_replacement);
            assert(parent_replacement_annote != NULL);
            immed replacement_immed =
                    replacement_annote->immeds()->head()->contents;
            immed parent_replacement_immed =
                    parent_replacement_annote->immeds()->head()->contents;
            assert(replacement_immed.is_symbol() &&
                   parent_replacement_immed.is_symbol());
            sym_node *replacement_sym = replacement_immed.symbol();
            sym_node *parent_replacement_sym =
                    parent_replacement_immed.symbol();
            assert(replacement_sym->is_var() &&
                   parent_replacement_sym->is_var());
            var_sym *replacement_var = (var_sym *)replacement_sym;
            var_sym *parent_replacement_var =
                    (var_sym *)parent_replacement_sym;
            parent_replacement_var->add_child(replacement_var,
                                              the_var->offset());
          }

        type_node_list_iter type_iter(the_fse->symtab()->types());
        while (!type_iter.is_empty())
          {
            type_node *this_type = type_iter.step();
            global_symtab *target_symtab = find_new_symtab(this_type);
            if ((target_symtab != NULL) &&
                (target_symtab != the_fse->symtab()))
              {
                type_node *new_type = this_type->copy();
                new_type->copy_annotes(this_type);
                target_symtab->add_type(new_type);
                this_type->append_annote(k_replacement,
                                         new immed_list(new_type));
                do_replacement(new_type);
              }
          }
      }
  }

static void mark_reachable_sym(sym_node *the_sym, so_walker *the_walker)
  {
    if (!the_sym->parent()->is_file())
        return;
    if (the_sym->peek_annote(k_globalize) != NULL)
        return;
    file_set_entry *new_fse = (file_set_entry *)(the_walker->get_data(0).ptr);
    file_set_entry_list *reachable_list =
            (file_set_entry_list *)(the_sym->peek_annote(k_butchur_reachable));
    if (reachable_list == NULL)
      {
        the_sym->append_annote(k_butchur_reachable,
                               new file_set_entry_list(new_fse));
      }
    else
      {
        file_set_entry_list_iter reachable_iter(reachable_list);
        while (!reachable_iter.is_empty())
          {
            file_set_entry *this_fse = reachable_iter.step();
            if (this_fse == new_fse)
                return;
          }
        reachable_list->append(new_fse);
      }
    mark_reachable_from_object(the_sym, new_fse);
  }

static void mark_reachable_type(type_node *the_type, so_walker *the_walker)
  {
    if (!the_type->parent()->is_file())
        return;
    if (the_type->peek_annote(k_globalize) != NULL)
        return;
    file_set_entry *new_fse = (file_set_entry *)(the_walker->get_data(0).ptr);
    file_set_entry_list *reachable_list =
            (file_set_entry_list *)(the_type->peek_annote(
                    k_butchur_reachable));
    if (reachable_list == NULL)
      {
        the_type->append_annote(k_butchur_reachable,
                                new file_set_entry_list(new_fse));
      }
    else
      {
        file_set_entry_list_iter reachable_iter(reachable_list);
        while (!reachable_iter.is_empty())
          {
            file_set_entry *this_fse = reachable_iter.step();
            if (this_fse == new_fse)
                return;
          }
        reachable_list->append(new_fse);
      }
    mark_reachable_from_object(the_type, new_fse);
  }

static void mark_reachable_from_object(suif_object *the_object,
                                       file_set_entry *new_fse)
  {
    so_walker the_walker;
    the_walker.set_data(0, new_fse);
    the_walker.set_leaf_function(&mark_reachable_sym);
    the_walker.set_leaf_function(&mark_reachable_type);
    the_walker.walk(the_object);
  }

static void mark_reachable_from_annotes(suif_object *the_object,
                                        file_set_entry *new_fse)
  {
    /* We must mark everything reachable from an annotation on
     * the_object, but only the annotations, not everything reachable
     * from objects within the_object.  So we copy the annotations to
     * a dummy instruction and use the so_walker on that. */
    instruction *dummy_instr = new in_rrr(io_mrk);
    dummy_instr->annotes()->grab_from(the_object->annotes());
    mark_reachable_from_object(dummy_instr, new_fse);
    the_object->annotes()->grab_from(dummy_instr->annotes());
    delete dummy_instr;
  }

static file_set_entry *unwritten_fse(void)
  {
    static file_set_entry *result = NULL;

    if (result == NULL)
        result = fileset->add_file(NULL, NULL);
    return result;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
