/* file "clean_bad.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE clean_bad_cc

#include "porky.h"

RCS_BASE(
    "$Id: clean_bad.cc,v 1.2 1999/08/25 03:27:25 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

typedef struct
  {
    label_sym *old_label;
    label_sym *new_label;
  } replace_data;

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Variable Definitions
 *----------------------------------------------------------------------*/

static i_integer gele_split_depth = 0;

/*----------------------------------------------------------------------*
    End Private Variable Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void fix_breaks_on_a_node(tree_node *the_node, void *);
static tree_node *next_on_list(tree_node_list *the_list, tree_node *the_node);
static void replace_label(tree_node *the_node, void *the_data);
static void fix_gele_pre_object(suif_object *the_object);
static void fix_gele_post_object(suif_object *the_object);
static void clean_a_node(tree_node *the_node, void *data);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void clean_bad_nodes(proc_sym *proc, boolean fancy)
  {
    assert(proc != NULL);

    tree_proc *the_block = proc->block();
    assert(the_block != NULL);

    the_block->map(&fix_breaks_on_a_node, NULL, FALSE);

    so_walker the_walker;
    the_walker.set_pre_function(&fix_gele_pre_object);
    the_walker.set_post_function(&fix_gele_post_object);
    the_walker.walk(the_block);

    /*
     *  Make two passes; the second gets LOOPs that were created by dismantling
     *  FORs on the first pass.
     */
    boolean is_fancy = fancy;
    the_block->map(&clean_a_node, &is_fancy, FALSE);
    the_block->map(&clean_a_node, &is_fancy, FALSE);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void fix_breaks_on_a_node(tree_node *the_node, void *)
  {
    assert(the_node != NULL);

    tree_node_list *body;
    label_sym *break_label;

    switch (the_node->kind())
      {
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)the_node;
            body = the_loop->body();
            break_label = the_loop->brklab();
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            body = the_for->body();
            break_label = the_for->brklab();
            break;
          }
        default:
            return;
      }

    tree_node_list *parent = the_node->parent();
    assert(parent != NULL);

    tree_node *next = next_on_list(parent, the_node);

    while (next != NULL)
      {
        if (next->kind() != TREE_INSTR)
            break;
        tree_instr *the_tree_instr = (tree_instr *)next;
        instruction *the_instr = the_tree_instr->instr();
        if (the_instr->opcode() != io_mrk)
            break;
        next = next_on_list(parent, next);
      }

    if (next == NULL)
        return;

    if (next->kind() != TREE_INSTR)
        return;

    tree_instr *the_tree_instr = (tree_instr *)next;
    instruction *the_instr = the_tree_instr->instr();
    if (the_instr->format() != inf_lab)
        return;

    in_lab *the_lab = (in_lab *)the_instr;
    label_sym *old_label = the_lab->label();

    replace_data the_data;
    the_data.old_label = old_label;
    the_data.new_label = break_label;
    body->map(&replace_label, &the_data);
  }

static tree_node *next_on_list(tree_node_list *the_list, tree_node *the_node)
  {
    assert(the_list != NULL);
    assert(the_node != NULL);
    tree_node_list_iter the_iter(the_list);

    tree_node *current = NULL;
    while (!the_iter.is_empty())
      {
        current = the_iter.step();
        if (current == the_node)
            break;
      }

    if (the_iter.is_empty())
        return NULL;
    else
        return the_iter.step();
  }

static void replace_label(tree_node *the_node, void *the_data)
  {
    replace_data *the_replace_data = (replace_data *)the_data;

    if (the_node->kind() != TREE_INSTR)
        return;

    tree_instr *the_tree_instr = (tree_instr *)the_node;
    instruction *the_instr = the_tree_instr->instr();
    if_ops opcode = the_instr->opcode();

    switch (opcode)
      {
        case io_btrue:
        case io_bfalse:
        case io_jmp:
          {
            in_bj *the_bj = (in_bj *)the_instr;
            if (the_bj->target() == the_replace_data->old_label)
                the_bj->set_target(the_replace_data->new_label);
            break;
          }
        case io_mbr:
          {
            in_mbr *the_mbr = (in_mbr *)the_instr;

            if (the_mbr->default_lab() == the_replace_data->old_label)
                the_mbr->set_default_lab(the_replace_data->new_label);

            int num_labels = the_mbr->num_labs();
            for (int index = 0; index < num_labels; ++index)
              {
                if (the_mbr->label(index) == the_replace_data->old_label)
                    the_mbr->set_label(index, the_replace_data->new_label);
              }
            break;
          }
        default:
            break;
      }
  }

static void fix_gele_pre_object(suif_object *the_object)
  {
    if (!the_object->is_tree_obj())
        return;
    tree_node *the_node = (tree_node *)the_object;

    if (!the_node->is_for())
        return;
    tree_for *the_for = (tree_for *)the_node;

    tree_for_test the_test = the_for->test();
    if ((the_test != FOR_SGELE) && (the_test != FOR_UGELE))
        return;

    if ((the_for->proc()->src_lang() == src_fortran) &&
        !node_has_incoming_goto(the_for))
      {
        ++gele_split_depth;
      }
  }

static void fix_gele_post_object(suif_object *the_object)
  {
    if (!the_object->is_tree_obj())
        return;
    tree_node *the_node = (tree_node *)the_object;

    if (!the_node->is_for())
        return;
    tree_for *the_for = (tree_for *)the_node;

    tree_for_test the_test = the_for->test();
    if ((the_test != FOR_SGELE) && (the_test != FOR_UGELE))
        return;

    if ((the_for->proc()->src_lang() != src_fortran) ||
        node_has_incoming_goto(the_for))
      {
        dismantle_for(the_for);
      }
    else
      {
        if (gele_split_depth <= max_gele_split_depth)
            split_fortran_gele(the_for);
        else
            dismantle_for(the_for);
        --gele_split_depth;
      }
  }

static void clean_a_node(tree_node *the_node, void *data)
  {
    boolean fancy = *(boolean *)data;

    if (the_node->kind() == TREE_FOR)
      {
        tree_for *the_for = (tree_for *)the_node;
        if ((the_for->test() == FOR_EQ) || (the_for->test() == FOR_NEQ))
          {
            warning_line(the_for,
                         "for test of == or != automatically dismantled");
            dismantle_for(the_for);
            return;
          }
      }

    boolean yes_dismantle = node_is_bad(the_node);

    if ((!fancy) && (!yes_dismantle))
        yes_dismantle = node_contains_goto(the_node);

    if (!yes_dismantle)
        return;

    switch (the_node->kind())
      {
        case TREE_LOOP:
          {
            dismantle(the_node);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;
            dismantle_for(the_for);
            break;
          }
        case TREE_IF:
          {
            dismantle(the_node);
            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;
            if (!the_block->is_proc())
                dismantle(the_block);
            break;
          }
        default:
            return;
      }

    if (verbosity_level > 0)
      {
        printf("Completed dismantling of a bad node\n");
        fflush(stdout);
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
