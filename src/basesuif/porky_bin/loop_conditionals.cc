/* file "loop_conditionals.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* code to move loop invariant conditionals outside the loop for the
 * porky program for SUIF */


#define RCS_BASE_FILE loop_conditionals_cc

#include "porky.h"

RCS_BASE(
    "$Id: loop_conditionals.cc,v 1.2 1999/08/25 03:27:30 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static boolean conditionals_on_list(tree_node_list *the_list);
static boolean conditionals_from_node(tree_node *starting_node);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void init_loop_conditionals(void)
  {
    init_ucf_opt();
  }

extern void loop_conditionals_on_proc(tree_proc *the_proc)
  {
    boolean found;
    do
      {
        proc_know_bounds(the_proc);
        fold_all_constants(the_proc);
        control_flow_prune(the_proc);
        optimize_ucf_on_proc(the_proc);
        found = conditionals_on_list(the_proc->body());
      } while (found);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static boolean conditionals_on_list(tree_node_list *the_list)
  {
    tree_node_list_iter node_iter(the_list);
    while (!node_iter.is_empty())
      {
        tree_node *current_node = node_iter.step();
        switch (current_node->kind())
          {
            case TREE_INSTR:
                break;
            case TREE_LOOP:
              {
                tree_loop *the_loop = (tree_loop *)current_node;
                boolean done = conditionals_on_list(the_loop->body());
                if (done)
                    return TRUE;
                done = conditionals_on_list(the_loop->test());
                if (done)
                    return TRUE;
                break;
              }
            case TREE_FOR:
              {
                tree_for *the_for = (tree_for *)current_node;
                boolean done = conditionals_on_list(the_for->landing_pad());
                if (done)
                    return TRUE;
                done = conditionals_on_list(the_for->body());
                if (done)
                    return TRUE;
                break;
              }
            case TREE_IF:
              {
                tree_if *the_if = (tree_if *)current_node;
                boolean done = conditionals_on_list(the_if->header());
                if (done)
                    return TRUE;
                done = conditionals_on_list(the_if->then_part());
                if (done)
                    return TRUE;
                done = conditionals_on_list(the_if->else_part());
                if (done)
                    return TRUE;
                break;
              }
            case TREE_BLOCK:
              {
                tree_block *the_block = (tree_block *)current_node;
                boolean done = conditionals_on_list(the_block->body());
                if (done)
                    return TRUE;
                break;
              }
            default:
                assert(FALSE);
          }
      }
    tree_node_list_e *head = the_list->head();
    if (head == NULL)
        return FALSE;
    return conditionals_from_node(head->contents);
  }

static boolean conditionals_from_node(tree_node *starting_node)
  {
    tree_node *current_node = starting_node;
    while (TRUE)
      {
        tree_node_list_e *next_e = current_node->list_e()->next();
        if (current_node->is_instr())
          {
            tree_instr *current_tree_instr = (tree_instr *)current_node;
            instruction *current_instr = current_tree_instr->instr();
            if ((current_instr->opcode() == io_btrue) ||
                (current_instr->opcode() == io_bfalse))
              {
                in_bj *current_bj = (in_bj *)current_instr;
                operand src_op = current_bj->src_op();
                tree_node *outer_loop = NULL;
                tree_node_list *follow_parent = current_node->parent();
                while (follow_parent != NULL)
                  {
                    tree_node *test_node = follow_parent->parent();
                    if (test_node->is_loop())
                      {
                        tree_loop *test_loop = (tree_loop *)test_node;
                        if ((test_loop->body() == follow_parent) &&
                            !might_modify(src_op, follow_parent) &&
                            !might_modify(src_op, test_loop->test()))
                          {
                            outer_loop = test_loop;
                          }
                      }
                    else if (test_node->is_for())
                      {
                        tree_for *test_for = (tree_for *)test_node;
                        if ((test_for->body() == follow_parent) &&
                            !might_modify(src_op, follow_parent) &&
                            (!operand_may_reference_var(src_op,
                                                        test_for->index())))
                          {
                            outer_loop = test_for;
                          }
                      }
                    else if (test_node->is_block())
                      {
                        replacements test_replacements;
                        src_op.find_exposed_refs(test_node->scope(),
                                                 &test_replacements);
                        if ((!test_replacements.oldsyms.is_empty()) ||
                            (!test_replacements.oldtypes.is_empty()))
                          {
                            break;
                          }
                      }
                    follow_parent = test_node->parent();
                  }

                if (outer_loop != NULL)
                  {
                    if (outer_loop->is_for())
                      {
                        tree_for *outer_for = (tree_for *)outer_loop;
                        tree_node_list *landing_pad = outer_for->landing_pad();
                        if (might_modify(src_op, landing_pad))
                          {
                            guard_for(outer_for);
                            tree_node_list *parent = outer_for->parent();
                            landing_pad = outer_for->landing_pad();
                            parent->insert_before(landing_pad,
                                                  outer_for->list_e());
                          }
                        if (might_modify(src_op, outer_for->lb_list()))
                            make_lb_temp(outer_for);
                        if (might_modify(src_op, outer_for->ub_list()))
                            make_ub_temp(outer_for);
                        if (might_modify(src_op, outer_for->step_list()))
                            make_step_temp(outer_for);
                      }

                    in_ldc *false_ldc =
                            new in_ldc(src_op.type(), operand(), immed(0));
                    src_op.remove();
                    current_bj->set_src_op(operand(false_ldc));

                    base_symtab *outer_base_symtab = outer_loop->scope();
                    assert(outer_base_symtab->is_block());
                    block_symtab *outer_symtab =
                            (block_symtab *)outer_base_symtab;

                    label_sym *jumpto = outer_symtab->new_unique_label();
                    in_bj *jump_instr = new in_bj(io_bfalse, jumpto, src_op);
                    tree_instr *jump_node = new tree_instr(jump_instr);

                    tree_node_list *header = new tree_node_list;
                    header->append(jump_node);
                    tree_node_list *then_part = new tree_node_list;
                    tree_node_list *else_part = new tree_node_list;

                    tree_if *new_if =
                            new tree_if(jumpto, header, then_part, else_part);
                    tree_node_list_e *loop_e= outer_loop->list_e();
                    outer_loop->parent()->insert_after(new_if, loop_e);

                    tree_node *loop_clone = outer_loop->clone();

                    else_part->append(loop_clone);
                    outer_loop->parent()->remove(loop_e);
                    then_part->append(loop_e);

                    false_ldc->set_value(immed(1));

                    return TRUE;
                  }
              }
          }
        if (next_e == NULL)
            return FALSE;
        current_node = next_e->contents;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
