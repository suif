/* file "dismantle.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE dismantle_cc

#include "porky.h"

RCS_BASE(
    "$Id: dismantle.cc,v 1.2 1999/08/25 03:27:27 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

typedef struct
  {
    boolean straight_control;
    boolean must_hit_target;
    boolean might_hit_target;
    label_sym *label;
  } might_branch_data;

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void dismantle_rrr(in_rrr *the_rrr);
static void dismantle_memcpy(in_rrr *the_memcopy);
static tree_node *create_jump_node(label_sym *symbol);
static tree_node *create_btrue_node(label_sym *target, operand condition);
static int if_test_result(tree_if *the_if);
static void might_branch_to(tree_node *the_node, void *data);
static void dismantle_eq_neq_for(tree_for *the_for);
static void dismantle_gele_for(tree_for *the_for);
static void dismantle_good_for(tree_for *the_for);
static void move_bound_evals_out(tree_for *the_for);
static operand array_replacement(in_array *the_array);
static operand eq_neq_test_done(tree_for *the_for, operand index_op);
static tree_node_list *gele_branch(tree_for *the_for, operand index_op,
                                   label_sym *branch_to, boolean reverse_test,
                                   base_symtab *scope);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void dismantle_for(tree_for *the_for)
  {
    move_bound_evals_out(the_for);
    dismantle_for_ops_reevaluated(the_for);
  }

extern void dismantle_for_ops_reevaluated(tree_for *the_for)
  {
    tree_node_list_e *preceeding_e = the_for->list_e()->prev();
    tree_node_list_e *following_e = the_for->list_e()->next();
    tree_node_list *parent = the_for->parent();

    tree_for_test the_test = the_for->test();

    if ((the_test == FOR_SGELE) || (the_test == FOR_UGELE))
        dismantle_gele_for(the_for);
    else if ((the_test == FOR_EQ) || (the_test == FOR_NEQ))
        dismantle_eq_neq_for(the_for);
    else
        dismantle_good_for(the_for);

    /* Now we need to call the main processing routine on the things we just
       created in case they need to be dismantled or have some other action
       taken. */
    tree_node_list_e *follow_e;
    if (preceeding_e == NULL)
        follow_e = parent->head();
    else
        follow_e = preceeding_e;
    while (follow_e != following_e)
      {
        assert(follow_e != NULL);
        tree_node *this_node = follow_e->contents;
        follow_e = follow_e->next();
        if (this_node->is_if())
          {
            tree_if *this_if = (tree_if *)this_node;

            tree_node_list_iter then_iter(this_if->then_part());
            while (!then_iter.is_empty())
              {
                tree_node *then_node = then_iter.step();
                process_node(then_node, NULL);
              }

            tree_node_list_iter else_iter(this_if->else_part());
            while (!else_iter.is_empty())
              {
                tree_node *else_node = else_iter.step();
                process_node(else_node, NULL);
              }
          }
        process_node(this_node, NULL);
      }
  }

extern void dismantle_instr(instruction *the_instr)
  {
    assert(the_instr != NULL);

    switch (the_instr->opcode())
      {
        case io_mbr:
          {
            in_mbr *the_mbr = (in_mbr *)the_instr;
            dismantle_mbr(the_mbr);
            break;
          }
        case io_array:
          {
            in_array *the_array = (in_array *)the_instr;
            (void)dismantle_array(the_array);
            break;
          }
        case io_max:
        case io_min:
        case io_abs:
        case io_divfloor:
        case io_divceil:
        case io_mod:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            dismantle_rrr(the_rrr);
            break;
          }
        case io_memcpy:
          {
            in_rrr *the_rrr = (in_rrr *)the_instr;
            dismantle_memcpy(the_rrr);
            break;
          }
        default:
            assert_msg(FALSE,
                       ("it is impossible to dismantle an instruction with "
                        "opcode %s", if_ops_name(the_instr->opcode())));
      }
  }

extern operand dismantle_array(in_array *the_array)
  {
    operand new_operand = array_replacement(the_array);

    replace_instruction_with_operand(the_array, new_operand);
    delete the_array;
    return new_operand;
  }

extern void dismantle_mbr(in_mbr *the_mbr)
  {
    tree_node *owner = the_mbr->owner();

    assert(owner->kind() == TREE_INSTR);
    tree_instr *owner_tree_instr = (tree_instr *)owner;
    assert(owner_tree_instr->instr() == the_mbr);

    int num_labels = the_mbr->num_labs();

    if (num_labels != 1)
        force_sources_not_exprs(the_mbr);

    operand switch_operand = the_mbr->src_op();
    int lower = the_mbr->lower();

    for (int index = 0; index < num_labels; ++index)
      {
        operand case_operand = operand_int(switch_operand.type(), lower + index);
        operand condition = operand_rrr(type_signed, io_seq, switch_operand,
                                        case_operand);
        tree_node *new_node = create_btrue_node(the_mbr->label(index),
                                                condition);
        insert_tree_node_before(new_node, owner);
      }

    tree_node *new_node = create_jump_node(the_mbr->default_lab());
    insert_tree_node_before(new_node, owner);
    kill_node(owner);
  }

extern boolean if_test_must_be_true(tree_if *the_if)
  {
    return (if_test_result(the_if) == 1);
  }

extern boolean if_test_must_be_false(tree_if *the_if)
  {
    return (if_test_result(the_if) == 0);
  }

extern boolean for_must_execute(tree_for *the_for)
  {
    int lb, ub;

    if ((the_for->test() == FOR_UGELE) || (the_for->test() == FOR_SGELE))
        return FALSE;
    if (!(the_for->lb_is_constant(&lb)))
        return FALSE;
    if (!(the_for->ub_is_constant(&ub)))
        return FALSE;
    switch (the_for->test())
      {
        case FOR_EQ:
            return (lb == ub);
        case FOR_NEQ:
            return (lb != ub);
        case FOR_SGELE:
        case FOR_UGELE:
            return FALSE;
        case FOR_SGT:
        case FOR_UGT:
            return (lb > ub);
        case FOR_SGTE:
        case FOR_UGTE:
            return (lb >= ub);
        case FOR_SLT:
        case FOR_ULT:
            return (lb < ub);
        case FOR_SLTE:
        case FOR_ULTE:
            return (lb <= ub);
        default:
            assert(FALSE);
      }
    return FALSE;
  }

extern boolean for_must_not_execute(tree_for *the_for)
  {
    int lower_bound, upper_bound;

    return (the_for->lb_is_constant(&lower_bound) &&
            the_for->ub_is_constant(&upper_bound) &&
            (the_for->test() != FOR_SGELE) && (the_for->test() != FOR_UGELE) &&
            !for_must_execute(the_for));
  }

extern void split_fortran_gele(tree_for *the_for, tree_for **left,
                               tree_for **right)
  {
    base_symtab *for_scope = the_for->scope();

    tree_node_list *then_list = new tree_node_list();
    tree_node_list *else_list = new tree_node_list();

    operand first_step = the_for->step_op().clone();
    operand test_op = first_step < 0;
    tree_if *new_if = create_if(test_op, then_list, else_list, for_scope);

    goto_data *the_goto_data = goto_data_from_node(the_for);
    set_node_goto_data(the_goto_data, new_if);

    insert_tree_node_before(new_if, the_for);
    remove_node(the_for);

    tree_for *other_for = (tree_for *)the_for->clone();

    set_node_goto_data(the_goto_data, other_for);
    free(the_goto_data);

    if (the_for->test() == FOR_SGELE)
      {
        the_for->set_test(FOR_SGTE);
        other_for->set_test(FOR_SLTE);
      }
    else
      {
        the_for->set_test(FOR_UGTE);
        other_for->set_test(FOR_ULTE);
      }

    then_list->append(the_for);
    else_list->append(other_for);

    if (right != NULL)
        *right = the_for;
    if (left != NULL)
        *left = other_for;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void dismantle_rrr(in_rrr *the_rrr)
  {
    base_symtab *scope = the_rrr->parent()->scope();
    assert(scope != NULL);
    type_node *the_type = the_rrr->result_type();

    force_sources_not_exprs(the_rrr);
    force_dest_not_expr(the_rrr);

    operand src1_operand = the_rrr->src1_op();
    operand src2_operand = the_rrr->src2_op();
    operand original_destination = the_rrr->dst_op();
    assert(original_destination.kind() == OPER_SYM);
    var_sym *destination_var = original_destination.symbol();

    operand test_operand;
    operand then_operand;
    operand else_operand;

    tree_node *new_node;

    switch (the_rrr->opcode())
      {
        case io_abs:
          {
            test_operand = (0 <= src1_operand);

            tree_node *then_node = assign(destination_var, src1_operand);
            tree_node *else_node = assign(destination_var, -src1_operand);
            tree_node_list *then_list = new tree_node_list;
            append_tree_node_to_list(then_list, then_node);
            tree_node_list *else_list = new tree_node_list;
            append_tree_node_to_list(else_list, else_node);
            new_node = create_if(test_operand, then_list, else_list, scope);
            break;
          }
        case io_max:
          {
            test_operand = src2_operand <= src1_operand;

            tree_node *then_node = assign(destination_var, src1_operand);
            tree_node *else_node = assign(destination_var, src2_operand);
            tree_node_list *then_list = new tree_node_list;
            append_tree_node_to_list(then_list, then_node);
            tree_node_list *else_list = new tree_node_list;
            append_tree_node_to_list(else_list, else_node);
            new_node = create_if(test_operand, then_list, else_list, scope);
            break;
          }
        case io_min:
          {
            test_operand = src2_operand <= src1_operand;

            tree_node *then_node = assign(destination_var, src2_operand);
            tree_node *else_node = assign(destination_var, src1_operand);
            tree_node_list *then_list = new tree_node_list;
            append_tree_node_to_list(then_list, then_node);
            tree_node_list *else_list = new tree_node_list;
            append_tree_node_to_list(else_list, else_node);
            new_node = create_if(test_operand, then_list, else_list, scope);
            break;
          }
        case io_divfloor:
          {
            operand src1_negative = (src1_operand < 0);
            operand src2_negative = (src2_operand < 0);
            test_operand = fold_seq(src1_negative, src2_negative);
            then_operand = src1_operand / src2_operand;
            operand abs_src1 = fold_abs(src1_operand);
            operand abs_src2 = operand_rrr(the_type, io_abs, src2_operand,
                                           operand());
            operand temp_1 = abs_src2.clone();
            else_operand = -((abs_src1 + abs_src2.clone()) - 1);
            else_operand /= abs_src2;

            tree_node_list *then_list = new tree_node_list;
            then_list->append(assign(destination_var,
                                     src1_operand / src2_operand));

            tree_node_list *else_list = new tree_node_list;
            else_list->append(assign(destination_var, else_operand));

            new_node = create_if(test_operand, then_list, else_list, scope);
            break;
          }
        case io_divceil:
          {
            operand src1_negative = (src1_operand < 0);
            operand src2_negative = (src2_operand < 0);
            test_operand = fold_sne(src1_negative, src2_negative);
            operand abs_src1 = fold_abs(src1_operand);
            operand abs_src2 = fold_abs(src2_operand);
            else_operand = (abs_src1 + abs_src2.clone()) - 1;
            else_operand /= abs_src2;

            tree_node_list *then_list = new tree_node_list;
            then_list->append(assign(destination_var,
                                     src1_operand / src2_operand));

            tree_node_list *else_list = new tree_node_list;
            else_list->append(assign(destination_var, else_operand));

            new_node = create_if(test_operand, then_list, else_list, scope);
            break;
          }
        case io_mod:
          {
            operand remainder = src1_operand % src2_operand;

            tree_node_list *then_list = new tree_node_list;
            then_list->append(assign(destination_var, remainder.clone()));

            tree_node_list *else_list = new tree_node_list;
            else_list->append(assign(destination_var,
                                     remainder.clone() + src2_operand));

            new_node = create_if(0 <= remainder, then_list, else_list, scope);
            break;
          }
        default:
            assert(FALSE);
      }

    insert_tree_node_before(new_node, the_rrr->owner());
    kill_node(the_rrr->owner());

    /* Now we need to call the main processing routine on the things we just
       created in case they need to be dismantled or have some other action
       taken. */
    process_node(new_node, NULL);
  }

static void dismantle_memcpy(in_rrr *the_memcopy)
  {
    operand source_addr = the_memcopy->src_addr_op();
    source_addr.remove();
    type_node *source_addr_type = source_addr.type();
    assert(source_addr_type->is_ptr());
    ptr_type *source_addr_ptr = (ptr_type *)source_addr_type;
    in_rrr *new_load =
            new in_rrr(io_lod, source_addr_ptr->ref_type()->unqual(),
                       operand(), source_addr);
    the_memcopy->set_src2(new_load);
    the_memcopy->set_opcode(io_str);
  }

static tree_node *create_jump_node(label_sym *symbol)
  {
    return new tree_instr(new in_bj(io_jmp, symbol));
  }

static tree_node *create_btrue_node(label_sym *target, operand condition)
  {
    return new tree_instr(new in_bj(io_btrue, target, condition));
  }

/*
 *  Returns: -1 if it can't figure out the test result
 *            0 if the test always evaluates to FALSE
 *            1 if the test always evaluates to TRUE
 */
static int if_test_result(tree_if *the_if)
  {
    might_branch_data the_data;
    the_data.straight_control = TRUE;
    the_data.might_hit_target = FALSE;
    the_data.must_hit_target = FALSE;
    the_data.label = the_if->jumpto();
    the_if->header()->map(&might_branch_to, &the_data, TRUE);
    if (the_data.must_hit_target)
        return 0;
    else if (the_data.might_hit_target)
        return -1;
    else
        return 1;
  }

static void might_branch_to(tree_node *the_node, void *data)
  {
    might_branch_data *branch_data = (might_branch_data *)data;
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            instruction *the_instr = the_tree_instr->instr();
            switch (the_instr->opcode())
              {
                case io_mrk:
                    break;
                case io_btrue:
                case io_bfalse:
                case io_jmp:
                  {
                    in_bj *the_bj = (in_bj *)the_instr;
                    boolean always_taken = FALSE;
                    boolean never_taken = FALSE;

                    if (the_bj->opcode() == io_jmp)
                      {
                        always_taken = TRUE;
                      }
                    else
                      {
                        int the_const;
                        boolean const_test =
                                int_const_from_operand(the_bj->src_op(),
                                                       &the_const);
                        if (const_test)
                          {
                            if (the_bj->opcode() == io_bfalse)
                                the_const = !the_const;
                            if (the_const == 0)
                                never_taken = TRUE;
                            else
                                always_taken = TRUE;
                          }
                      }

                    if (always_taken)
                      {
                        if (the_bj->target() == branch_data->label)
                          {
                            branch_data->might_hit_target = TRUE;
                            if (branch_data->straight_control)
                                branch_data->must_hit_target = TRUE;
                          }
                        branch_data->straight_control = FALSE;
                      }
                    else if (!never_taken)
                      {
                        if (the_bj->target() == branch_data->label)
                            branch_data->might_hit_target = TRUE;
                        branch_data->straight_control = FALSE;
                      }

                    break;
                  }
                case io_lab:
                    branch_data->must_hit_target = FALSE;
                    branch_data->straight_control = FALSE;
                    break;
                case io_mbr:
                  {
                    in_mbr *the_mbr = (in_mbr *)the_instr;
                    int num_labs = the_mbr->num_labs();
                    for (int index = 0; index < num_labs; ++index)
                      {
                        if (the_mbr->label(index) == branch_data->label)
                            branch_data->might_hit_target = TRUE;
                      }
                    if (the_mbr->default_lab() == branch_data->label)
                        branch_data->might_hit_target = TRUE;
                    branch_data->straight_control = FALSE;
                  }
                    break;
                default:
                    break;
              }
            break;
          }
        case TREE_LOOP:
        case TREE_FOR:
        case TREE_IF:
            branch_data->straight_control = FALSE;
            break;
        case TREE_BLOCK:
            break;
        default:
            assert(FALSE);
      }
  }

static void dismantle_eq_neq_for(tree_for *the_for)
  {
    assert(the_for != NULL);

    assert((the_for->test() == FOR_EQ) || (the_for->test() == FOR_NEQ));

    var_sym *the_index = the_for->index();
    base_symtab *scope = the_for->scope();
    operand index_operand(the_index);

    goto_data *the_goto_data = goto_data_from_node(the_for);

    if (for_must_not_execute(the_for))
      {
        kill_node(the_for);
        return;
      }

    boolean generate_if = !(for_must_execute(the_for));

    operand lb_operand = the_for->lb_op();
    lb_operand.remove();

    operand step_operand = the_for->step_op();
    step_operand.remove();

    tree_node_list *the_loop = the_for->landing_pad();
    the_for->set_landing_pad(NULL);

    tree_node_list *the_loop_body = the_for->body();
    the_for->set_body(NULL);

    if (the_loop_body == NULL)
        the_loop_body = new tree_node_list;

    insert_tree_node_before(assign(the_index, lb_operand), the_for);

    operand test_op = !eq_neq_test_done(the_for, index_operand);

    assert(scope->is_block());
    block_symtab *the_block_symtab = (block_symtab *)scope;
    label_sym *top_label = the_block_symtab->new_unique_label(NULL);

    tree_node_list *test_list = new tree_node_list;
    test_list->append(btrue_node(top_label, test_op));

    tree_loop *new_loop_node =
            new tree_loop(the_loop_body, test_list, the_for->contlab(),
                          the_for->brklab(), top_label);

    tree_node_list *test_part = new_loop_node->test();

    test_part->push(assign(the_index, index_operand + step_operand));
    the_loop->append(new_loop_node);

    set_node_goto_data(the_goto_data, new_loop_node);

    if (generate_if)
      {
        tree_node_list *else_list = new tree_node_list;

        operand test_op = !eq_neq_test_done(the_for, index_operand);
        tree_node *new_if_node = if_node(scope, test_op, the_loop, else_list);

        set_node_goto_data(the_goto_data, new_if_node);
        insert_tree_node_before(new_if_node, the_for);
      }
    else
      {
        insert_tree_node_list_before(the_loop, the_for);
      }

    free(the_goto_data);
    kill_node(the_for);
  }

static void dismantle_gele_for(tree_for *the_for)
  {
    assert(the_for != NULL);

    assert((the_for->test() == FOR_SGELE) || (the_for->test() == FOR_UGELE));

    var_sym *the_index = the_for->index();
    base_symtab *scope = the_for->scope();
    operand index_operand(the_index);

    goto_data *the_goto_data = goto_data_from_node(the_for);

    if (for_must_not_execute(the_for))
      {
        kill_node(the_for);
        return;
      }

    boolean generate_if = !(for_must_execute(the_for));

    operand lb_operand = the_for->lb_op();
    lb_operand.remove();

    tree_node_list *the_loop = the_for->landing_pad();
    the_for->set_landing_pad(NULL);

    tree_node_list *the_loop_body = the_for->body();
    the_for->set_body(NULL);

    if (the_loop_body == NULL)
        the_loop_body = new tree_node_list;

    insert_tree_node_before(assign(the_index, lb_operand), the_for);

    assert(scope->is_block());
    block_symtab *the_block_symtab = (block_symtab *)scope;
    label_sym *top_label = the_block_symtab->new_unique_label(NULL);

    tree_node_list *test_part =
            gele_branch(the_for, index_operand, top_label, TRUE, scope);

    tree_loop *new_loop_node =
            new tree_loop(the_loop_body, test_part, the_for->contlab(),
                          the_for->brklab(), top_label);

    test_part->push(assign(the_index,
                           index_operand + the_for->step_op().clone()));
    the_loop->append(new_loop_node);

    set_node_goto_data(the_goto_data, new_loop_node);

    if (generate_if)
      {
        assert(scope->is_block());
        block_symtab *the_block_symtab = (block_symtab *)scope;
        label_sym *else_label = the_block_symtab->new_unique_label(NULL);
        tree_node_list *test_part =
                gele_branch(the_for, index_operand, else_label, FALSE, scope);

        tree_node_list *else_list = new tree_node_list;

        tree_node *new_if_node =
                new tree_if(else_label, test_part, the_loop, else_list);

        set_node_goto_data(the_goto_data, new_if_node);
        insert_tree_node_before(new_if_node, the_for);
      }
    else
      {
        insert_tree_node_list_before(the_loop, the_for);
      }

    free(the_goto_data);
    kill_node(the_for);
  }

static void dismantle_good_for(tree_for *the_for)
  {
    assert(the_for != NULL);

    assert((the_for->test() != FOR_EQ) && (the_for->test() != FOR_NEQ) &&
           (the_for->test() != FOR_SGELE) && (the_for->test() != FOR_UGELE));

    guard_for(the_for);

    var_sym *the_index = the_for->index();

    goto_data *the_goto_data = goto_data_from_node(the_for);

    if (for_must_not_execute(the_for))
      {
        kill_node(the_for);
        return;
      }

    insert_tree_node_list_before(the_for->landing_pad(), the_for);

    tree_node_list *the_loop_body = the_for->body();
    the_for->set_body(new tree_node_list);

    if (no_effects(the_loop_body))
      {
        insert_tree_node_before(assign(the_index, final_index_value(the_for)),
                                the_for);
        delete the_loop_body;
      }
    else
      {
        operand lb_operand = the_for->lb_op();
        lb_operand.remove();
        insert_tree_node_before(assign(the_index, lb_operand), the_for);

        base_symtab *scope = the_for->scope();
        assert(scope->is_block());
        block_symtab *the_block_symtab = (block_symtab *)scope;
        label_sym *top_label = the_block_symtab->new_unique_label(NULL);

        tree_node_list *test_part = new tree_node_list();
        in_bj *branch_instruction =
                new in_bj(io_bfalse, top_label,
                          for_test_done(the_for, the_index));
        test_part->append(new tree_instr(branch_instruction));

        tree_loop *new_loop_node =
                new tree_loop(the_loop_body, test_part, the_for->contlab(),
                              the_for->brklab(), top_label);

        operand step_operand = the_for->step_op();
        step_operand.remove();
        test_part->push(assign(the_index, the_index + step_operand));

        insert_tree_node_before(new_loop_node, the_for);

        set_node_goto_data(the_goto_data, new_loop_node);
      }


    free(the_goto_data);
    kill_node(the_for);
  }

/*
 * If the upper bound and/or the step is not something that can harmlessly be
 * re-evaluated on each iteration, move its calculation outside the TREE_FOR.
 */
static void move_bound_evals_out(tree_for *the_for)
  {
    if (might_modify(the_for->ub_op(), the_for->body()))
        make_ub_temp(the_for);

    if (might_modify(the_for->step_op(), the_for->body()))
        make_step_temp(the_for);
  }

static operand array_replacement(in_array *the_array)
  {
    int dimensions = the_array->dims();

    operand result;

    if (dimensions == 0)
      {
        result = operand_int(type_ptr_diff, 0);
      }
    else
      {
        result = the_array->index(0);
        result.remove();
        result = cast_op(result, type_ptr_diff);

        /*
         *  The first bound doesn't matter, so throw it away.
         */
        operand first_bound = the_array->bound(0);
        first_bound.remove();
        if (first_bound.kind() == OPER_INSTR)
            delete first_bound.instr();

        for (int index_num = 1; index_num < dimensions; ++index_num)
          {
            operand index_operand = the_array->index(index_num);
            index_operand.remove();
            index_operand = cast_op(index_operand, type_ptr_diff);
            operand bound_operand = the_array->bound(index_num);
            bound_operand.remove();
            bound_operand = cast_op(bound_operand, type_ptr_diff);
            result = operand_multiply(type_ptr_diff, result, bound_operand);
            result = operand_add(type_ptr_diff, result, index_operand);
          }
      }

    operand offset_operand = the_array->offset_op();
    offset_operand.remove();

    result = operand_sub(type_ptr_diff, result, offset_operand);

    int element_size = the_array->elem_size();
    assert((element_size % target.addressable_size) == 0);
    element_size /= target.addressable_size;

    result = operand_mul_by_const(type_ptr_diff, result, element_size);

    int offset = the_array->offset();
    if (offset != 0)
      {
        assert((offset % target.addressable_size) == 0);
        offset /= target.addressable_size;

        result = operand_add_const(type_ptr_diff, result, offset);
      }

    operand base_operand = the_array->base_op();
    base_operand.remove();

    if (base_operand.type() != the_array->result_type())
      {
        base_operand = operand(new in_rrr(io_cvt, the_array->result_type(),
                                          operand(), base_operand));
      }

    result = operand_add(base_operand.type()->unqual(), base_operand, result);

    return result;
  }

static operand eq_neq_test_done(tree_for *the_for, operand index_op)
  {
    if (the_for->test() == FOR_EQ)
        return fold_sne(index_op, the_for->ub_op().clone());
    else if (the_for->test() == FOR_NEQ)
        return fold_seq(index_op, the_for->ub_op().clone());
    else
        assert(FALSE);
    return operand();
  }

static tree_node_list *gele_branch(tree_for *the_for, operand index_op,
                                   label_sym *branch_to, boolean reverse_test,
                                   base_symtab *scope)
  {
    assert((the_for->test() == FOR_SGELE) || (the_for->test() == FOR_UGELE));

    operand step_op = the_for->step_op();
    operand step_comp =
            fold_sl(step_op.clone(), operand_int(step_op.type(), 0));

    var_sym *new_var = scope->new_unique_var(type_signed);
    new_var->reset_userdef();

    in_rrr *then_test =
            new in_rrr(io_sle, type_signed, operand(new_var),
                       the_for->ub_op().clone(), index_op);
    in_rrr *else_test =
            new in_rrr(io_sle, type_signed, operand(new_var), index_op,
                       the_for->ub_op().clone());

    tree_node_list *then_part = new tree_node_list;
    tree_node_list *else_part = new tree_node_list;

    then_part->append(new tree_instr(then_test));
    else_part->append(new tree_instr(else_test));

    tree_node_list *test_part = new tree_node_list();
    test_part->append(create_if(step_comp, then_part, else_part, scope));

    if_ops branch_opcode;
    if (reverse_test)
        branch_opcode = io_btrue;
    else
        branch_opcode = io_bfalse;

    in_bj *branch_instruction =
            new in_bj(branch_opcode, branch_to, operand(new_var));
    test_part->append(new tree_instr(branch_instruction));

    return test_part;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
