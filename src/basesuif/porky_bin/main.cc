/* file "main.cc" */

/*  Copyright (c) 1994,95 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 * This is the main program for the porky program for the SUIF system.
 */

#define RCS_BASE_FILE main_cc

#include "porky.h"
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.2 1999/08/25 03:27:31 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Documentation
 *----------------------------------------------------------------------*

                Summary
                -------

        porky <options> <source filespec> <destination filespec>
                       { <source filespec> <destination filespec> } *

        Porky1 makes various transformations to the SUIF code.
        Command line options specify which transformations are
        performed.  These options fall into two broad catagories.  The
        first catagory is transformations to subsets of SUIF.  The
        purpose of each such transformation is to allow subsequent
        passes to make simplifying assumptions, such as the assumption
        that there are no branches into a loop body.  The other
        catagory of transformations is those that try to rearrange the
        code to make it easier for subsequent passes to get
        information, but don't get rid of any particular constructs.
        For example, the ``-forward-prop'' transformation tries to
        move as much computation as possible into the bound
        expressions of each loop but can't guarantee any particular
        nice form for the results.

        This pass expects two or more file specifications on the
        command line, paired into input and then output files.  For
        each input file, there must be a corresponding output file.
        If more than one input/output pair is specified, all the input
        files must have had their global symbol table information
        merged so that they can form a single file set (see the
        documentation for the linksuif pass).  The output files will
        also have merged global symbol table information.

        In addition to the file specifications, there can be any
        number of options given on the command line.  All options
        begin with ``-''.  If no options at all are given, the SUIF
        file will simply be read and written back again without any
        transformations being performed.

        The options are as follows:


            Options that set operating modes
            --------------------------------

        -V <n>
                This sets the the verbosity level to the integer n.
                The default is verbosity level 0.  At higher verbosity
                levels, more comments will be written to standard
                output about what porky is doing.  Verbosity levels
                above three have the same effect as level three.

        -fortran
                This tells porky to look at the suif file in Fortran
                mode to see call-by-reference variables.  For some
                kinds of analysis, such as that required for forward
                propagation (the -forward-prop option), this gives
                porky more information, so it can do a better job.

        -iterate
                This flag says to keep doing all the specified
                optimizations as long as any of them make progress.

        -max-iters <n>
                This sets the maximum number of iterations done by the
                -iterate option to the integer n.  The default is to
                have no limit on the number of iterations.

        -no-glob-merge
                This option only affects the -cse, -loop-invariants,
                and -fred-loop-invariants options.  It says not to
                consider references to global variables as loads for
                the purposes of common sub-expression elimination or
                loop-invariant moving.  The default is to essentially
                consider all references to global variables as loads,
                so multiple dynamic references to the same global
                variable will, if possible, be changed to a read of
                the global variable into a local followed by multiple
                references to the local variable.

        -fast
                When doing data-flow analysis on structured
                control-flow, for example for the ``-dead-code'' or
                ``-cse'' passes, a faster algorithm is used when the
                ``-fast'' flag is given.  Some precision is lost, but
                it seldom matters and the speed-up is often very
                important.  If a porky pass is taking an
                exceptionally long time, try it with ``-fast''.


            Transformations to subsets of SUIF
            ----------------------------------

        -Dfors
                This dismantles all TREE_FORs.

        -Dloops
                This dismantles all TREE_LOOPs.

        -Difs
                This dismantles all TREE_IFs.

        -Dblocks
                This dismantles all TREE_BLOCKs.

        -Darrays
                This dismantles all SUIF array instructions.

        -Dmbrs
                This dismantles all mbr (multi-way branch)
                instructions.

        -Dmins
                This dismantles all min instructions.

        -Dmaxs
                This dismantles all max instructions.

        -Dabss
                This dismantles all abs instructions.

        -Ddivfloors
                This dismantles all divfloor instructions.

        -Ddivceils
                This dismantles all divceil instructions.

        -Dmods
                This dismantles all mod instructions.

        -Dmemcpys
                This dismantles all memcpy instructions.

        -Dimins
                This dismantles all integer min instructions.

        -Dimaxs
                This dismantles all integer max instructions.

        -Diabss
                This dismantles all integer abs instructions.


        -Dfcmmas
                This dismantles all SUIF divfloor, divceil, min, max,
                abs, and mod instructions.  This must be done before
                mexp/mgen as that back end can't handle these
                instructions.  It is equivalent to each of -Ddivfloor,
                -Ddivceil, -Dmin, -Dmax, -Dabss, and -Dmods.

        -Dfcimmas
                This dismantles all SUIF divfloor, and divceil
                instructions, and also all integer min, max, and abs
                instructions.  This must be done before the Iwarp
                software pipeliner because that can handle floating
                point min, max, and abs instructions but not divfloor
                and divceil or integer versions of min, max, and abs.
                It is equivalent to each of -Ddivfloor, -Ddivceil,
                -Dimin, -Dimax, and -Diabss.

        -defaults
                This does the default options to be used right after
                the front end, to turn some non-standard SUIF that the
                front end produces into standard SUIF.  It also does
                some things, like constant folding and removing empty
                symbol tables, to make the code as simple as possible
                without losing information.  It is equivalent to all
                of the options -fixbad, -for-bound, -no-index-mod,
                -no-empty-fors, -no-empty-table, -control-simp, and
                -fold.

        -fixbad
                This fixes ``bad'' nodes.  Note that the single ``-b''
                option is rarely, if ever, used; it is the doubled
                version, ``-bb'', that is used as part of the default
                expansion after the front end.  The single ``-b''
                option is just a less refined version that breaks
                apart so many nodes that it is probably of limited
                usefulness.
                The effects of the single ``-b'' are as follows:
                  * Any jump or branch from inside a FOR or LOOP to a
                    label immediately following the FOR or LOOP (i.e.
                    no intervening instructions that might do
                    anything) are changed to use as its target the
                    break label of that FOR or LOOP.  (This isn't
                    really relevant for the plain ``-b'' option, but
                    this same process is used with the ``-bb''
                    variation, and in that case this process will save
                    nodes from being dismantled in some cases because
                    they will have branches to break labels instead of
                    arbitrary outside labels.)
                  * FOR nodes with GELE comparison are broken into two
                    FORs and an IF to decide between them.
                  * Any FOR, LOOP, or IF node containing a branch or
                    jump, or the target label used by a branch or
                    jump, is entirely dismantled, unless both the
                    branch or jump and all its possible targets are
                    within the same instruction list.  ``Contains'' in
                    this case means at any level down in nested
                    sub-lists.  The only exception is for a branch or
                    jump from the first level instruction list of the
                    test part of a LOOP node that has as target the
                    toplab label of the LOOP.  This exception reflects
                    the fact that LOOP test parts are supposed to have
                    potential jumps to the top of the loop and this
                    shouldn't be considered a ``goto''.  Note,
                    however, that IF nodes customarily have branches
                    in their headers but these are still considered
                    ``gotos''.  Hence any node containing a
                    non-degenerate IF node will be dismantled.
                  * Any FOR with a test of ``equal to'' or ``not equal
                    to'' is dismantled and a warning message is
                    printed.

        -fixbadstrict
                This has the same effect as -fixbadstrict except that
                it uses a stricter definition of ``bad''.  In this
                case, any node containing a jump or branch, or
                possible target of a jump or branch, is dismantled,
                even if they are both within the same instruction
                list.  The exception for loops with a possible branch
                to the top label in the test part remains in effect.

        -max-gele-split-depth <depth>
                This sets the maximum depth of FOR loops with GELE
                comparisons that will be split by -fixbad or
                -fixbadstrict into two FORs and an IF to decide
                between them to <depth>.  Any more deeply nested GELE
                FOR loops will be dismantled.  This limits the maximum
                code explosion to 2**<depth>.  The default <depth>
                limit is 5.

        -for-bound
                This dismantles TREE_FORs unless porky can tell that
                the upper bound and step are both loop constants.

        -no-index-spill
                This dismantles TREE_FORs with spilled index variable.

        -no-index-mod
                This dismantles TREE_FORs for which the index variable
                might be modified by the TREE_FOR body.

        -no-empty-fors
                This dismantles TREE_FORs with empty bodies.

        -no-call-expr
                This takes any calls that are within expression trees
                out of the expression trees and creates new local
                variables for them to write their results into, then
                substitutes a reference to that local variable in the
                expression tree.

        -no-empty-table
                This dismantles all TREE_BLOCKs that have empty symbol
                tables.

        -fix-ldc-types
                This puts the correct types on all ldc (load constant)
                instructions that load symbol addresses.  This is
                needed after the front end because parts of the types
                of symbols may be completed only after a procedure
                that references the symbol is written out.  For
                example, p might be declared ``extern char p[]'', then
                used in various procedures, then later defined
                ``char p[30]''.  The complete type information isn't
                needed at the earlier stage, but in SUIF we must use
                one symbol and use it consistently, so the symbol's
                type must change to the completed type.  At that point
                any ldc instructions already written will have the
                wrong type and must be fixed up by this pass.

        -no-struct-copy
                This gets rid of all structure copies, whether through
                copy instructions, load-store pairs, or memcopy
                instructions.  They are replaced with memcopies of
                integer sized chunks that cover all the bits of the
                structure.  This option is useful before a RISC back
                end, so that it doesn't have to generate code for
                multi-word moves.

        -no-sub-vars
                This removes all sub-variables and replaces uses of
                them with uses their root ancestors, with the
                appropriate offsets.

        -globalize
                This changes all static local variables into global
                variables in the file symbol table.  It will do this
                unconditionally to all static locals, so after this
                pass there will no longer be any static locals.  Both
                the variable and its var_def will be moved.  If any
                annotations on the var_sym or var_def refer to
                anything not visible in the file symbol table (other
                than static locals that will soon be moved to the file
                symbol table), or operands or instructions, such
                annotations will be deleted.  If the type of the
                static local is not visible in the file symbol table,
                its type will be changed to the nearest approximation
                to that type which can be made in the file symbol
                table and all uses of the symbol will be changed to
                include casts to the original type.

        -array-glob <cutoff-size>
                This makes all statically allocated arrays with size
                greater than <cutoff-size> and type visible in the
                inter-file global symbol table into globals with
                inter-file scope (external linkage).  That is, it will
                move static local arrays and arrays with file scope
                that meet the size limit into the inter-file global
                symbol table.  The variables are renamed if necessary
                to avoid conflict with existing global symbols.  Note
                that to be safe this pass should only be run on code
                that has been linksuif'ed with all other source files
                to make sure all global namespace conflicts are
                discovered.  The motivating use for this pass is to
                make all arrays visible to the object-level linker so
                that array alignment specifications given to the
                linker will apply to all possible arrays.  This allows
                cache-line alignment of arrays when alignment
                specifications cannot be given to a back-end C
                compiler.

        -glob-autos
                This changes the behavior of the ``-array-glob'' flag
                to affect automatic local arrays as well as static
                local arrays, provided there is a guarantee of no
                recursion, by way of the ``no recursion'' annotation.

        -guard-fors
                This adds ``if'' nodes around some tree_for nodes to
                insure that whenever any tree_if node is executed, at
                the landing pad and first iteration will always be
                executed.  Any tree_for nodes that already have
                "guarded" annotations will be unaffected because this
                condition is already guaranteed.  All tree_for nodes
                end up with "guarded" annotations after this pass is
                done.  This pass also empties out the landing pads of
                tree_fors -- after they are guarded, it is legal to
                simply move the landing pad in front of the tree_for,
                so this pass does so.

        -no-ldc-sym-offsets
                This breaks all load constant instruction of a symbol
                and non-zero offset into an explicit addition of the
                offset.

        -only-simple-var-ops
                This puts in explicit loads and stores for access to
                all variables that are not local, non-static,
                non-volatile variables without the addr_taken flag
                set.

        -kill-enum
                This replaces all uses of enumerated types with a
                corresponding plain integer type.  This is useful if a
                pass doesn't want to see any enumerated types, just
                the corresponding plain integer types.  It is also
                useful before s2c if the back-end C compiler to run
                after s2c may not handle enumerated types as the SUIF
                code does (for example, a particular enumerated type
                may be treated as an ``unsigned 8-bit integer'' by
                SUIF but the same enumerated type declaration for the
                back-end compiler might be treated as a ``signed
                32-bit integer'').


            Other transformations
            ---------------------

        -fold
                This folds constants wherever possible.

        -reassociate
                This tries to reassociate the result of any arrays
                that are dismantled so that the dependence on the
                index variable of the nearest enclosing FOR is a
                simple linear expression, if possible.  Since arrays
                are dismantled only if the -Darrays option is used,
                there is no effect if -Darrays is not specified.

        -control-simp
                This simplifies TREE_IFs for which this pass can tell
                that one branch or the other always executes, leaving
                only the instructions from the branch that executes
                and any parts of the test section that might have side
                effects.  It also removes entirely any TREE_FORs which
                it can tell will never be executed.

        -forward-prop
                This forward propagates the calculation of local
                variables into uses of those variables when possible.
                The idea is to give more information about loop bounds
                for doing loop transformations, or generally to any
                pass doing analysis.

        -copy-prop
                This does copy propagation, which is the same as
                forward propagation limited to expressions that are
                simple local variables (i.e. if there is a simple copy
                from one local variable into another, uses of the
                source variable will replace the destination variable
                where the copy is live).

        -const-prop
                This does simple constant propagation.

        -ivar
                This does simple induction variable detection.  It
                replaces the uses of the induction variable within the
                loop by expressions of the loop index and moves the
                incrementing of the induction variable outside the
                loop.

        -reduction
                This finds simple instances of reduction.  It moves
                the summation out of the loop.

        -for-mod-ref
                This puts mod/ref annotations on TREE_FORs.  It
                assumes that the addresss of a symbol is never stored
                anywhere, which is valid for Fortran, but usually not
                for C.

        -privatize
                This privatizes all variables listed in the annotation
                "privatizable" on each TREE_FOR.

        -scalarize
                This turns local array variables into collections of
                element variables when all uses of the array are loads
                or stores of known elements.  It will partly scalarize
                multi-dimensional arrays if they can be scalarized in
                some but not all dimensions.

        -know-bounds
                This replaces comparisons of upper and lower bounds of
                a loop inside the loop body with the known result of
                that comparison.  This is particularly useful after
                multi-level induction variables have been replaced.

        -cse
                This does simple common sub-expression elimination.

        -dead-code
                This does simple dead-code elimination.

        -dead-code0
                This does even simpler (flow insensitive) dead-code
                elimination.

        -unused-syms
                This removes symbols taht are never referenced and
                have no external linkage, or that have external
                linkage but are not defined in this file (i.e. no
                procedure body or var_def).  Static procedures that
                are never referenced but have bodies will be removed,
                but only if this pass is re-run, because by the time
                porky figures out that it is safe to delete a
                procedure, it will already have been written.  The
                ``-iter'' option does not help this problem, because
                that iterates within procedures, not across all
                procedures; porky cannot iterate on the entire file
                because it keeps only one procedure in memory at a
                time.

        -unused-types
                This removes types that are never referenced.

        -loop-invariants
                This moves the calculation of loop-invariant
                expressions outside loop bodies.

        -fred-loop-invariants
                This is the same as the -loop-invariants flag except
                that it only considers moving instructions marked with
                the "Fred" annotation.

        -bitpack
                This combines local variables that are used only as
                single bits (i.e. assigned only one, zero, or the
                value of another bit variable, and are never
                addressed), packing them together into variables of
                type ``unsigned int'' and using bitwise operations to
                set and extract the appropriate bits.  This can be
                useful in some cases if it allows register allocation
                of the packed bits, though in other cases the cost
                associated with the bitwise operations will outweigh
                the savings.

        -if-hoist
                This moves certain ``if'' nodes up in the code under
                some cirumstances that can allow the test for the if
                to be eliminated.  The ``if'' nodes that are
                candidates to be hoisted are those that have a
                condition depending on only a single variable.  If
                that is the case, and in the code preceeding the
                ``if'' (on the same tree_node_list) there is another
                ``if'' which assigns a constant to the value of that
                condition variable in either the ``then'' or ``else''
                part, this will duplicate the original ``if'' node and
                put it in both the ``then'' and ``else'' parts of the
                higher ``if'' node, if this is legal.  This is useful
                for code which has ``chains'' of ``if'' nodes; that
                is, the body of one sets a variable that is used as a
                test in a later ``if''.  After hoisting, the constant
                value can often be propagated into the condition in
                one of the branches of the ``if''.  In simple cases
                where the flag is cleared before the higher ``if'' and
                then set only in one of its branches, the test can be
                eliminated in both parts.

        -find-fors
                This builds tree_for nodes out of tree_loop nodes for
                which a suitable index variable and bounds can be
                found.

        -glob-priv
                Do some code transformations to help with
                privatization of global variables across calls.  It
                looks for ``possible global privatizable'' annotations
                on proc_syms.  In each such annotation it expects to
                find a list of global variables.  It changes the code
                so that a new parameter is added to the procedure for
                each symbol in the annotation, and all uses of the
                symbol are replaced by indirect references through the
                new parameter, and at callsites the location of that
                symbol is passed.  If the procedure is a Fortran
                procedure, the new parameter is a call-by-ref
                parameter.  It arranges for this to work through
                arbitrary call graphs of procedures.  The result is
                code that has the same semantics but in which the
                globals listed in each of these annotations are never
                referenced directly, but instead a location to use is
                passed as a parameter.  If the annotations are put on
                the input code properly, this allows privatization of
                global variables to be done as if the globals were
                local.

        -build-arefs
                Add array reference instructions in place of pointer
                arithmetic where possible.  This helps dependence
                analysis of programs that were originally written in
                C, for example.

        -for-norm
                Normalize all ``for'' loops to have lower bound of
                zero, step size of one, and ``less than or equal to''
                test.

        -ucf-opt
                Do simple optimizations on unstructured control flow
                (branches and labels).  The optimizations are done
                simultaneously in such a way that the result cannot
                benefit from any more of these optimizations -- the
                output run through this pass again will not change.
                The following optimizations are performed:
                  * Labels that are not the target of any possible
                    branch are removed.
                  * Uses of labels that are followed by unconditional
                    jumps or other labels without any intervening
                    executable code are changed to uses of the last
                    label that must always be executed before some
                    executable code, and those labels are removed.
                  * Unreachable code is removed.
                  * Branches that would end up in the same place
                    before any code is executed as they would if they
                    were not taken are removed.
                  * Conditional branches followed in the code by
                    unconditional branches without any intervening
                    executable code, followed without any intervening
                    executable code by the label that is the target of
                    the conditional branch, are changed to reverse the
                    condition, change its target to that of the
                    unconditional branch, and remove the conditional
                    branch.  That is,

                        if (<cond>)
                            goto L1;
                        goto L2;
                        L1:

                    is replaced by

                        if (!<cond>)
                            goto L2;
                        L1:

                    (and L1 is removed if it is not a target of some
                    other instruction).

        -uncbr
                Replace call-by-reference scalar variables with
                copy-in, copy-out.  This is useful when a later pass,
                such as a back-end compiler after s2c will not have
                access to call-by-ref form.  Instead of seeing pointer
                references that might alias with anything, this will
                allow the pass to see a local variable.  Note that
                without the ``-fortran'' flag, this pass has no effect
                because without the ``-fortran'' flag, porky won't see
                anything in call-by-ref form to begin with.

        -loop-cond
                Move all loop-invariant conditionals that are inside a
                TREE_LOOP or TREE_FOR outside the outermost loop.

        -child-scalarize
                This turns array references with constant indexes that
                point to array elements that exactly overlap scalar
                variables (through the sub-variable mechanism) into
                uses of those scalar variables.

        -child-scalarize-aggressive
                This is the same as the ``-child-scalarize'' flag
                except that if a sub-variable that exactly overlaps
                doesn't exist but the array is already a descendant of
                a variable with group type, a new sub-variable will be
                added to meet the requirement.  If the array isn't
                already under a group type super-variable, new
                subvariables aren't added because that would tend to
                complicate some kinds of analysis.  If the array is
                already part of a group, the complication of
                sub-variables is already there, so it's assumed to be
                worth it to add another sub-variable.

        -kill-redundant-line-marks
                This removes all mark instructions that contain
                nothing but line information that is followed
                immediately by another line information mark.

        -nest
                This attempts to turn non-perfectly nested loop nests
                into perfectly nested loop nests by pulling
                conditionals as far out as possible.  This is
                particularly useful for pulling out loop guarding
                expressions to restore nests that were originally
                perfectly nested.

        -delinearize
                This attempts to turn 1-dimensional array references
                to multi-dimensional arrays into multi-dimensional
                array references.  It will only do so if it prove that
                the new indices obey all bound restrictions.

        -form-arrays
                This flag causes "form array" annotations to be read
                and arrays to be formed based on them.  See the
                comments for the k_form_array annotation name in the
                ``useful'' library for details.  If any of the
                original variables were themselves arrays, it's best
                to run porky again, this time with the
                ``-chain-arefs'' flag, after ``-form-arrays'' is done.

        -chain-arefs
                This causes porky to attempt to chain together
                multiple array reference instructions in series into a
                single array reference instruction.

        -form-all-arrays
                This causes porky to find and mark all sets of
                compatible variables that can be formed into arrays.
                All such sets are marked with "form array"
                annotations, so if this pass is followed by porky
                with the ``-form-arrays'' flag, the variables will
                actually be transformed into arrays.

        -ub-from-var
                This flag causes porky to attempt to extract upper
                bound information for array reference instructions
                from the variables for the arrays being referenced.

        -extract-consts
                This causes porky to attempt to replace uses of
                variables with "is constant" annotations with
                constants based on the static initialization
                information.

        -extract-array-bounds
                This causes porky to try to improve the array bound
                information by replacing variables used in array
                bounds with constants by looking for constant
                assignments to those variables at the start of the
                scope of each such array type.

        -cse-no-pointers
                This flag only has an effect when used with the
                ``-cse'' flag.  When used that way, it causes common
                sub-expression elimination to be supressed on
                sub-expressions with pointer type.  This is useful for
                avoiding creating temporary variables with pointer
                type, which inhibits conversion back to Fortran as a
                back end.  It also generally helps avoid breaking up
                address arithmetic expressions which are often better
                left intact for the back end.

        -breakup <cutoff-size>
                This flag causes porky to attempt to break up
                expression trees with more than <cutoff-size>
                instructions into smaller expression trees reading and
                writing new temporary variables.  It will not create
                any temporary variables with pointer type.  This is
                useful for back ends that have trouble with really
                large expression trees, but which are better off with
                address computations not broken up.  This is
                particularly useful when converting to Fortran because
                of Fortran's limit of 19 continuation lines; it can
                also be useful for C if the back-end compiler has
                hard-coded limits on line sizes or expression sizes,
                or for machine-code back-ends that can't handle
                expressin trees that are very large.

        -mark-constants
                This flag causes porky to put "is constant"
                annotations on all statically allocated var_syms for
                which porky can prove the annotation applies.

        -fix-addr-taken
                This flag causes porky to set the is_addr_taken flag
                of each variable to TRUE or FALSE depending on whether
                or not its address is actually taken, for each
                variable used only in the given fileset.  Any variable
                which may be used outside the current fileset will
                have its is_addr_taken flag set TRUE if its address is
                taken in the given fileset, and otherwise the
                is_addr_taken flag of that variable will not be
                changed.


 *----------------------------------------------------------------------*
    End Documentation
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static boolean flag_dismantle_fors = FALSE;
static boolean flag_dismantle_loops = FALSE;
static boolean flag_dismantle_ifs = FALSE;
static boolean flag_dismantle_blocks = FALSE;
static boolean flag_dismantle_arrays = FALSE;
static boolean flag_dismantle_mbrs = FALSE;
static boolean flag_dismantle_mins = FALSE;
static boolean flag_dismantle_maxs = FALSE;
static boolean flag_dismantle_abss = FALSE;
static boolean flag_dismantle_divfloors = FALSE;
static boolean flag_dismantle_divceils = FALSE;
static boolean flag_dismantle_mods = FALSE;
static boolean flag_dismantle_memcpys = FALSE;
static boolean flag_dismantle_imins = FALSE;
static boolean flag_dismantle_imaxs = FALSE;
static boolean flag_dismantle_iabss = FALSE;
static boolean flag_dismantle_fcmmas = FALSE;
static boolean flag_dismantle_fcimmas = FALSE;
static boolean flag_defaults = FALSE;
static boolean flag_fix_bad = FALSE;
static boolean flag_fix_bad_strict = FALSE;
static boolean flag_for_bound = FALSE;
static boolean flag_no_index_spill = FALSE;
static boolean flag_no_index_mod = FALSE;
static boolean flag_no_empty_fors = FALSE;
static boolean flag_no_call_expr = FALSE;
static boolean flag_no_empty_table = FALSE;
static boolean flag_fix_ldc_types = FALSE;
static boolean flag_no_struct_copy = FALSE;
static boolean flag_no_sub_vars = FALSE;
static boolean flag_globalize = FALSE;
static int array_glob_size = -1;
static boolean flag_array_glob_autos = FALSE;
static boolean flag_guard_fors = TRUE;
static boolean flag_no_ldc_sym_offsets = TRUE;
static boolean flag_only_simple_var_ops = TRUE;
static boolean flag_kill_enum = TRUE;
static boolean flag_fold_every_constant = FALSE;
static boolean flag_reassociate_arrays = FALSE;
static boolean flag_control_simp = FALSE;
static boolean flag_forward_prop = FALSE;
static boolean flag_copy_prop = FALSE;
static boolean flag_const_prop = FALSE;
static boolean flag_ivar = FALSE;
static boolean flag_reduction = FALSE;
static boolean flag_for_mod_ref = FALSE;
static boolean flag_privatize = FALSE;
static boolean flag_scalarize = FALSE;
static boolean flag_know_bounds = FALSE;
static boolean flag_cse = FALSE;
static boolean flag_dead_code = FALSE;
static boolean flag_dead_code0 = FALSE;
static boolean flag_unused_syms = FALSE;
static boolean flag_unused_types = FALSE;
static boolean flag_loop_invariants = FALSE;
static boolean flag_fred_li = FALSE;
static boolean flag_bitpack = FALSE;
static boolean flag_if_hoist = FALSE;
static boolean flag_find_fors = FALSE;
static boolean flag_glob_priv = FALSE;
static boolean flag_build_arefs = FALSE;
static boolean flag_for_norm = FALSE;
static boolean flag_ucf_opt = FALSE;
static boolean flag_uncbr = FALSE;
static boolean flag_loop_cond = FALSE;
static boolean flag_child_scalarize = FALSE;
static boolean flag_child_scalarize_aggressive = FALSE;
static boolean flag_kill_redundant_line_marks = FALSE;
static boolean flag_nest = FALSE;
static boolean flag_delinearize = FALSE;
static boolean flag_form_arrays = FALSE;
static boolean flag_chain_arefs = FALSE;
static boolean flag_form_all_arrays = FALSE;
static boolean flag_ub_from_var = FALSE;
static boolean flag_extract_consts = FALSE;
static boolean flag_extract_array_bounds = FALSE;
static boolean flag_fortran_mode = FALSE;
static boolean flag_iterate = FALSE;
static boolean flag_no_glob_merge = FALSE;
static boolean flag_fast = FALSE;
static boolean flag_cse_no_pointers = FALSE;
static boolean flag_mark_constants = FALSE;
static boolean flag_fix_addr_taken = FALSE;
static int breakup_size = -1;

int max_iterations = 0;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage_error(void);
static int parse_arguments(int argc, char *argv[], char ***input_filespec,
                           char ***output_filespec);
static void process_file(file_set_entry *fse);
static void process_procedure(proc_sym *the_procedure);
static void process_instr(instruction *the_instr, void *);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    file_set_entry *fse;
    char **input_filespec, **output_filespec;

    start_suif(argc, argv);

    int num_inputs =
            parse_arguments(argc, argv, &input_filespec, &output_filespec);

    if (flag_for_mod_ref)
        initialize_mod_ref();

    if (flag_privatize)
        init_privatization();

    if (flag_scalarize)
        init_scalarization();

    if (flag_dead_code)
	init_dead_code();

    if (flag_unused_syms || flag_unused_types)
        init_unused();

    if (flag_bitpack)
        init_bitpack();

    if (flag_if_hoist)
        init_if_hoist();

    if (flag_globalize)
        init_globalize();

    if (flag_glob_priv)
        init_glob_priv();

    if (flag_ucf_opt)
        init_ucf_opt();

    if (flag_loop_cond)
        init_loop_conditionals();

    if (flag_nest)
        init_nest();

    if (flag_kill_enum)
        init_kill_enum();

    if (flag_mark_constants)
        init_mark_constants();

    if (flag_const_prop)
        init_propagate_constants();

    for (int input_num = 0; input_num < num_inputs; ++input_num)
      {
        fileset->add_file(input_filespec[input_num],
                          output_filespec[input_num]);
      }

    delete[] input_filespec;
    delete[] output_filespec;

    if (flag_unused_syms || flag_unused_types)
      {
        unused_start_symtab(fileset->globals(), flag_unused_syms,
                            flag_unused_types);
      }

    if (flag_dead_code || flag_dead_code0)
        symtab_mark_bound_vars(fileset->globals());

    if (flag_form_arrays)
        form_arrays_on_symtab(fileset->globals());

    if (flag_form_all_arrays)
        form_all_arrays_on_symtab(fileset->globals());

    if (flag_kill_enum)
        kill_enum_on_symtab(fileset->globals());

    if (flag_mark_constants)
        mark_constants_start_symtab(fileset->globals());

    if (flag_fix_addr_taken)
        fix_addr_taken_start_symtab(fileset->globals());


    fileset->reset_iter();
    fse = fileset->next_file();
    while (fse != NULL)
      {
        process_file(fse);
        fse = fileset->next_file();
      }

    if (flag_no_sub_vars)
        symtab_no_sub_vars(fileset->globals());

    if (flag_unused_syms || flag_unused_types)
        unused_end_symtab(fileset->globals());

    exit_suif();
    return 0;
  }

extern void process_node(tree_node *the_node, void *)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            the_tree_instr->instr_map(&process_instr, NULL, FALSE);
            break;
          }
        case TREE_LOOP:
          {
            if (flag_dismantle_loops)
                dismantle(the_node);

            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;

            if (flag_dismantle_fors ||
                (flag_no_index_spill && the_for->index()->is_spilled()) ||
                (flag_no_index_mod &&
                 (the_for->proc()->src_lang() != src_fortran) &&
                 might_modify(the_for->index(), the_for->body())) ||
                (flag_no_empty_fors &&
                 (last_action_node(the_for->body()) == NULL)))
              {
                dismantle_for(the_for);
              }

            break;
          }
        case TREE_IF:
          {
            if (flag_dismantle_ifs)
                dismantle(the_node);

            break;
          }
        case TREE_BLOCK:
          {
            tree_block *the_block = (tree_block *)the_node;

            if ((!the_block->is_proc()) &&
                (flag_dismantle_blocks ||
                 (flag_no_empty_table && block_symtab_empty(the_block))))
              {
                dismantle(the_block);
              }

            break;
          }
        default:
            assert(FALSE);
      }
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage_error(void)
  {
    fprintf(stderr,
            "usage: %s <options> <source filespec> <destination filespec>\n"
            "       %*s         { <source filespec> <destination filespec> }"
            " *\n", _suif_program_name, (int)(strlen(_suif_program_name)),
            " ");
    exit(1);
  }

static int parse_arguments(int argc, char *argv[], char ***input_filespec,
                           char ***output_filespec)
  {
    static cmd_line_option option_table[] =
      {
        {CLO_INT,   "-V",                    "0",  &verbosity_level},
        {CLO_NOARG, "-fortran",              NULL, &flag_fortran_mode},
        {CLO_NOARG, "-iterate",              NULL, &flag_iterate},
        {CLO_INT,   "-max-iters",            "0",  &max_iterations},
        {CLO_NOARG, "-no-glob-merge",        NULL, &flag_no_glob_merge},
        {CLO_NOARG, "-Dfors",                NULL, &flag_dismantle_fors},
        {CLO_NOARG, "-Dloops",               NULL, &flag_dismantle_loops},
        {CLO_NOARG, "-Difs",                 NULL, &flag_dismantle_ifs},
        {CLO_NOARG, "-Dblocks",              NULL, &flag_dismantle_blocks},
        {CLO_NOARG, "-Darrays",              NULL, &flag_dismantle_arrays},
        {CLO_NOARG, "-Dmbrs",                NULL, &flag_dismantle_mbrs},
        {CLO_NOARG, "-Dmins",                NULL, &flag_dismantle_mins},
        {CLO_NOARG, "-Dmaxs",                NULL, &flag_dismantle_maxs},
        {CLO_NOARG, "-Dabss",                NULL, &flag_dismantle_abss},
        {CLO_NOARG, "-Ddivfloors",           NULL, &flag_dismantle_divfloors},
        {CLO_NOARG, "-Ddivceils",            NULL, &flag_dismantle_divceils},
        {CLO_NOARG, "-Dmods",                NULL, &flag_dismantle_mods},
        {CLO_NOARG, "-Dmemcpys",             NULL, &flag_dismantle_memcpys},
        {CLO_NOARG, "-Dimins",               NULL, &flag_dismantle_imins},
        {CLO_NOARG, "-Dimaxs",               NULL, &flag_dismantle_imaxs},
        {CLO_NOARG, "-Diabss",               NULL, &flag_dismantle_iabss},
        {CLO_NOARG, "-Dfcmmas",              NULL, &flag_dismantle_fcmmas},
        {CLO_NOARG, "-Dfcimmas",             NULL, &flag_dismantle_fcimmas},
        {CLO_NOARG, "-defaults",             NULL, &flag_defaults},
        {CLO_NOARG, "-fixbad",               NULL, &flag_fix_bad},
        {CLO_NOARG, "-fixbadstrict",         NULL, &flag_fix_bad_strict},
        {CLO_INT,   "-max-gele-split-depth", "5",  &max_gele_split_depth},
        {CLO_NOARG, "-for-bound",            NULL, &flag_for_bound},
        {CLO_NOARG, "-no-index-spill",       NULL, &flag_no_index_spill},
        {CLO_NOARG, "-no-index-mod",         NULL, &flag_no_index_mod},
        {CLO_NOARG, "-no-empty-fors",        NULL, &flag_no_empty_fors},
        {CLO_NOARG, "-no-call-expr",         NULL, &flag_no_call_expr},
        {CLO_NOARG, "-no-empty-table",       NULL, &flag_no_empty_table},
        {CLO_NOARG, "-fix-ldc-types",        NULL, &flag_fix_ldc_types},
        {CLO_NOARG, "-no-struct-copy",       NULL, &flag_no_struct_copy},
        {CLO_NOARG, "-no-sub-vars",          NULL, &flag_no_sub_vars},
        {CLO_NOARG, "-globalize",            NULL, &flag_globalize},
        {CLO_INT,   "-array-glob",           "-1", &array_glob_size},
        {CLO_NOARG, "-glob-autos",           NULL, &flag_array_glob_autos},
        {CLO_NOARG, "-guard-fors",           NULL, &flag_guard_fors},
        {CLO_NOARG, "-no-ldc-sym-offsets",   NULL, &flag_no_ldc_sym_offsets},
        {CLO_NOARG, "-only-simple-var-ops",  NULL, &flag_only_simple_var_ops},
        {CLO_NOARG, "-kill-enum",            NULL, &flag_kill_enum},
        {CLO_NOARG, "-fold",                 NULL, &flag_fold_every_constant},
        {CLO_NOARG, "-reassociate",          NULL, &flag_reassociate_arrays},
        {CLO_NOARG, "-control-simp",         NULL, &flag_control_simp},
        {CLO_NOARG, "-forward-prop",         NULL, &flag_forward_prop},
        {CLO_NOARG, "-copy-prop",            NULL, &flag_copy_prop},
        {CLO_NOARG, "-const-prop",           NULL, &flag_const_prop},
        {CLO_NOARG, "-ivar",                 NULL, &flag_ivar},
        {CLO_NOARG, "-reduction",            NULL, &flag_reduction},
        {CLO_NOARG, "-for-mod-ref",          NULL, &flag_for_mod_ref},
        {CLO_NOARG, "-privatize",            NULL, &flag_privatize},
        {CLO_NOARG, "-scalarize",            NULL, &flag_scalarize},
        {CLO_NOARG, "-know-bounds",          NULL, &flag_know_bounds},
        {CLO_NOARG, "-cse",                  NULL, &flag_cse},
        {CLO_NOARG, "-dead-code",            NULL, &flag_dead_code},
        {CLO_NOARG, "-dead-code0",           NULL, &flag_dead_code0},
        {CLO_NOARG, "-unused-syms",          NULL, &flag_unused_syms},
        {CLO_NOARG, "-unused-types",         NULL, &flag_unused_types},
        {CLO_NOARG, "-loop-invariants",      NULL, &flag_loop_invariants},
        {CLO_NOARG, "-fred-loop-invariants", NULL, &flag_fred_li},
        {CLO_NOARG, "-bitpack",              NULL, &flag_bitpack},
        {CLO_NOARG, "-if-hoist",             NULL, &flag_if_hoist},
        {CLO_NOARG, "-find-fors",            NULL, &flag_find_fors},
        {CLO_NOARG, "-glob-priv",            NULL, &flag_glob_priv},
        {CLO_NOARG, "-build-arefs",          NULL, &flag_build_arefs},
        {CLO_NOARG, "-for-norm",             NULL, &flag_for_norm},
        {CLO_NOARG, "-ucf-opt",              NULL, &flag_ucf_opt},
        {CLO_NOARG, "-uncbr",                NULL, &flag_uncbr},
        {CLO_NOARG, "-loop-cond",            NULL, &flag_loop_cond},
        {CLO_NOARG, "-child-scalarize",      NULL, &flag_child_scalarize},
        {CLO_NOARG, "-child-scalarize-aggressive", NULL,
                            &flag_child_scalarize_aggressive},
        {CLO_NOARG, "-kill-redundant-line-marks", NULL,
                            &flag_kill_redundant_line_marks},
        {CLO_NOARG, "-nest",                 NULL, &flag_nest},
        {CLO_NOARG, "-fast",                 NULL, &flag_fast},
        {CLO_NOARG, "-delinearize",          NULL, &flag_delinearize},
        {CLO_NOARG, "-form-arrays",          NULL, &flag_form_arrays},
        {CLO_NOARG, "-chain-arefs",          NULL, &flag_chain_arefs},
        {CLO_NOARG, "-form-all-arrays",      NULL, &flag_form_all_arrays},
        {CLO_NOARG, "-ub-from-var",          NULL, &flag_ub_from_var},
        {CLO_NOARG, "-extract-consts",       NULL, &flag_extract_consts},
        {CLO_NOARG, "-extract-array-bounds", NULL, &flag_extract_array_bounds},
        {CLO_NOARG, "-cse-no-pointers",      NULL, &flag_cse_no_pointers},
        {CLO_INT,   "-breakup",              "-1", &breakup_size},
        {CLO_NOARG, "-mark-constants",       NULL, &flag_mark_constants},
        {CLO_NOARG, "-fix-addr-taken",       NULL, &flag_fix_addr_taken}
      };

    parse_cmd_line(argc, argv, option_table,
                   sizeof(option_table) / sizeof(cmd_line_option));

    if ((argc < 3) || ((argc % 2) != 1))
        usage_error();

    int num_inputs = (argc - 1) / 2;

    *input_filespec = new char *[num_inputs];
    *output_filespec = new char *[num_inputs];
    for (int input_num = 0; input_num < num_inputs; ++input_num)
      {
        (*input_filespec)[input_num] = argv[(input_num * 2) + 1];
        (*output_filespec)[input_num] = argv[(input_num * 2) + 2];
      }

    if (flag_defaults)
      {
        /* equivalent to "-fixbad -for-bound -no-index-mod -no-empty-fors
                          -no-empty-table -control-simp -fold" */
        flag_fix_bad = TRUE;
        flag_for_bound = TRUE;
        flag_no_index_mod = TRUE;
        flag_no_empty_fors = TRUE;
        flag_no_empty_table = TRUE;
        flag_control_simp = TRUE;
        flag_fold_every_constant = TRUE;
      }

    if (flag_dismantle_fcmmas)
      {
        /* equivalent to "-Ddivfloor -Ddivceil -Dmin -Dmax -Dabss -Dmods" */
        flag_dismantle_divfloors = TRUE;
        flag_dismantle_divceils = TRUE;
        flag_dismantle_mins = TRUE;
        flag_dismantle_maxs = TRUE;
        flag_dismantle_abss = TRUE;
        flag_dismantle_mods = TRUE;
      }

    if (flag_dismantle_fcimmas)
      {
        /* equivalent to "-Ddivfloor -Ddivceil -Dimin -Dimax -Diabss" */
        flag_dismantle_divfloors = TRUE;
        flag_dismantle_divceils = TRUE;
        flag_dismantle_imins = TRUE;
        flag_dismantle_imaxs = TRUE;
        flag_dismantle_iabss = TRUE;
      }

    if (flag_no_glob_merge)
      {
        if (!(flag_cse || flag_loop_invariants || flag_fred_li))
          {
            error_line(1, NULL,
                       "``-no-glob-merge'' flag may only be used with "
                       "``-cse'', ``-loop-invariants'', or "
                       "``-fred-loop-invariants''");
          }
        merge_globals = FALSE;
      }

    if (max_iterations < 0)
      {
        error_line(1, NULL, "maximum number of iterations cannot be negative");
      }

    if (flag_array_glob_autos && (array_glob_size == -1))
      {
        error_line(1, NULL,
                   "``-glob-autos'' flag used without the ``-array-glob''"
                   " option");
      }
    return num_inputs;
  }

static void process_file(file_set_entry *fse)
  {
    proc_sym *psym;

    if (flag_unused_syms || flag_unused_types)
      {
        unused_start_symtab(fse->symtab(), flag_unused_syms,
                            flag_unused_types);
      }

    if (flag_dead_code || flag_dead_code0)
        symtab_mark_bound_vars(fse->symtab());

    if (array_glob_size != -1)
      {
        array_globalize_on_symtab(fse->symtab(), array_glob_size,
                                  flag_array_glob_autos);
      }

    if (flag_form_arrays)
        form_arrays_on_symtab(fse->symtab());

    if (flag_form_all_arrays)
        form_all_arrays_on_symtab(fse->symtab());

    if (flag_kill_enum)
        kill_enum_on_symtab(fse->symtab());

    if (flag_mark_constants)
        mark_constants_start_symtab(fse->symtab());

    if (flag_fix_addr_taken)
        fix_addr_taken_start_symtab(fileset->globals());

    fse->reset_proc_iter();
    for (psym = fse->next_proc(); psym != NULL; psym = fse->next_proc())
      {
        psym->read_proc(TRUE, flag_fortran_mode);

        if (flag_unused_syms && is_unused_procedure(psym))
          {
            if (verbosity_level >= 1)
                printf("Removing body of procedure `%s'.\n", psym->name());
            psym->flush_proc();
            continue;
          }

        if (verbosity_level >= 1)
          {
            fprintf(stdout, "Processing procedure `%s'.\n",
                    psym->name());
            fflush(stdout);
          }

        process_procedure(psym);
        if (verbosity_level >= 1)
          {
            printf("Writing procedure `%s' ... ", psym->name());
            fflush(stdout);
          }
        psym->write_proc(fse);
        psym->flush_proc();
        if (verbosity_level >= 1)
          {
            printf("done.\n");
            fflush(stdout);
          }
      }

    fse->reset_proc_iter();

    if (flag_no_sub_vars)
      {
        no_annote_sub_vars(fse);
        symtab_no_sub_vars(fse->symtab());
      }

    if (flag_unused_syms || flag_unused_types)
        unused_end_symtab(fse->symtab());
  }

static void process_procedure(proc_sym *the_procedure)
  {
    if (flag_fast)
        fast_structured_facts = TRUE;

    if (flag_cse_no_pointers)
        cse_no_pointers = TRUE;

    boolean fort = (the_procedure->src_lang() == src_fortran);

    calculate_goto_information(the_procedure->block());

    if (flag_fix_ldc_types)
        fix_proc_ldc_types(the_procedure->block());

    if (flag_for_bound && !fort)
        change_semantics(the_procedure->block());

    if (flag_no_struct_copy)
        proc_no_struct_copy(the_procedure->block());

    if (flag_no_sub_vars)
        proc_no_sub_vars(the_procedure->block());

    if (flag_fix_bad || flag_fix_bad_strict)
        clean_bad_nodes(the_procedure, !flag_fix_bad_strict);

    if (flag_const_prop)
        propagate_constants(the_procedure->block());

    if (flag_forward_prop)
        forward_propagate(the_procedure->block(), FPK_ALL);

    if (flag_copy_prop)
        forward_propagate(the_procedure->block(), FPK_SIMPLE_LOCAL_VARS);

    if (flag_fold_every_constant)
        fold_all_constants(the_procedure->block());

    if (flag_control_simp)
        control_flow_prune(the_procedure->block());

    the_procedure->block()->map(&process_node, NULL, FALSE);
    process_node(the_procedure->block(), NULL);

    if (flag_no_call_expr)
        spill_calls(the_procedure->block());

    if (flag_ivar)
        find_ivars(the_procedure->block());

    if (flag_iterate)
      {
        unsigned iteration_num = 0;
        boolean keep_going;
        assert(max_iterations >= 0);
        do
          {
            keep_going = FALSE;

            if (verbosity_level >= 1)
              {
                printf("Beginning `%s' iteration #%u\n",
                       the_procedure->name(), iteration_num);
              }

            if (flag_const_prop)
              {
                made_progress = FALSE;
                propagate_constants(the_procedure->block());
                if (verbosity_level >= 2)
                    printf("  Constant propagation made ");
                if (made_progress)
                  {
                    keep_going = TRUE;
                  }
                else
                  {
                    if (verbosity_level >= 2)
                        printf("no ");
                  }
                if (verbosity_level >= 2)
                    printf("progress.\n");
              }

            if (flag_forward_prop)
              {
                made_progress = FALSE;
                forward_propagate(the_procedure->block(), FPK_ALL);
                if (verbosity_level >= 2)
                    printf("  Forward propagation made ");
                if (made_progress)
                  {
                    keep_going = TRUE;
                  }
                else
                  {
                    if (verbosity_level >= 2)
                        printf("no ");
                  }
                if (verbosity_level >= 2)
                    printf("progress.\n");
              }

            if (flag_fold_every_constant)
              {
                made_progress = FALSE;
                fold_all_constants(the_procedure->block());
                if (verbosity_level >= 2)
                    printf("  Constant folding made ");
                if (made_progress)
                  {
                    keep_going = TRUE;
                  }
                else
                  {
                    if (verbosity_level >= 2)
                        printf("no ");
                  }
                if (verbosity_level >= 2)
                    printf("progress.\n");
              }

            if (flag_control_simp)
              {
                made_progress = FALSE;
                control_flow_prune(the_procedure->block());
                if (verbosity_level >= 2)
                    printf("  Control-flow pruning made ");
                if (made_progress)
                  {
                    keep_going = TRUE;
                  }
                else
                  {
                    if (verbosity_level >= 2)
                        printf("no ");
                  }
                if (verbosity_level >= 2)
                    printf("progress.\n");
              }

            ++iteration_num;

            if (keep_going && (iteration_num == (unsigned)max_iterations))
              {
                if (verbosity_level >= 1)
                    printf("Iteration limit reached with work remaining\n");
                break;
              }

          } while (keep_going);

        if (verbosity_level >= 1)
          {
            printf("Done after %u iteration%s in `%s'\n", iteration_num,
                   ((iteration_num == 1) ? "" : "s"), the_procedure->name());
          }
      }

    if (flag_for_mod_ref)
        mark_loop_mod_ref(the_procedure->block());

    if (flag_reduction)
        find_reductions(the_procedure->block());

    if (flag_privatize)
        do_privatization(the_procedure->block());

    if (flag_scalarize)
        scalarize_proc(the_procedure->block());

    if (flag_know_bounds)
        proc_know_bounds(the_procedure->block());

    if (flag_cse)
        proc_cse(the_procedure->block());

    if (flag_dead_code)
        proc_dead_code(the_procedure->block());

    if (flag_dead_code0)
        proc_dead_code0(the_procedure->block());

    if (flag_loop_invariants)
        proc_loop_invariants(the_procedure->block(), FALSE);

    if (flag_fred_li)
        proc_loop_invariants(the_procedure->block(), TRUE);

    if (flag_bitpack)
        bit_pack(the_procedure->block());

    if (flag_if_hoist)
        if_hoist_proc(the_procedure->block());

    if (flag_find_fors)
        find_fors_on_proc(the_procedure->block());

    if (flag_glob_priv)
        glob_priv_on_proc(the_procedure->block());

    if (flag_build_arefs)
        build_arefs_on_proc(the_procedure->block());

    if (flag_for_norm)
        normalize_fors_on_proc(the_procedure->block());

    if (flag_ucf_opt)
        optimize_ucf_on_proc(the_procedure->block());

    if (flag_uncbr)
        uncbr_on_proc(the_procedure->block());

    if (flag_loop_cond)
        loop_conditionals_on_proc(the_procedure->block());

    if (flag_child_scalarize)
        child_scalarize_proc(the_procedure->block(), FALSE);

    if (flag_child_scalarize_aggressive)
        child_scalarize_proc(the_procedure->block(), TRUE);

    if (flag_kill_redundant_line_marks)
        kill_redundant_line_marks(the_procedure->block());

    if (flag_nest)
        nest_proc(the_procedure->block());

    if (flag_delinearize)
        delinearize_on_proc(the_procedure->block());

    if (flag_form_arrays)
        form_arrays_on_proc(the_procedure->block());

    if (flag_chain_arefs)
        chain_arefs_on_proc(the_procedure->block());

    if (flag_form_all_arrays)
        form_all_arrays_on_proc(the_procedure->block());

    if (flag_ub_from_var)
        ub_from_var_on_proc(the_procedure->block());

    if (flag_extract_consts)
        extract_constants(the_procedure->block());

    if (flag_extract_array_bounds)
        extract_array_bounds(the_procedure->block());

    if (flag_globalize)
        globalize_on_proc(the_procedure->block());

    if (array_glob_size != -1)
      {
        array_globalize_on_proc(the_procedure->block(), array_glob_size,
                                flag_array_glob_autos);
      }

    if (flag_guard_fors)
        guard_fors_on_proc(the_procedure->block());

    if (flag_no_ldc_sym_offsets)
        no_ldc_sym_offsets_on_proc(the_procedure->block());

    if (flag_only_simple_var_ops)
        only_simple_var_ops_on_proc(the_procedure->block());

    if (flag_kill_enum)
        kill_enum_on_proc(the_procedure->block());

    if (flag_unused_syms || flag_unused_types)
      {
        proc_unused_syms(the_procedure->block(), flag_unused_syms,
                         flag_unused_types);
      }

    if (breakup_size != -1)
        breakup_on_proc(the_procedure->block(), breakup_size);

    if (flag_mark_constants)
        mark_constants_on_proc(the_procedure->block());

    if (flag_fix_addr_taken)
        fix_addr_taken_on_proc(the_procedure->block());
  }

static void process_instr(instruction *the_instr, void *)
  {
    switch (the_instr->opcode())
      {
        case io_divfloor:
          {
            if (flag_dismantle_divfloors)
                dismantle_instr(the_instr);
            break;
          }
        case io_divceil:
          {
            if (flag_dismantle_divceils)
                dismantle_instr(the_instr);
            break;
          }
        case io_min:
          {
            if (flag_dismantle_mins ||
                (flag_dismantle_imins &&
                 (the_instr->result_type()->unqual()->op() == TYPE_INT)))
              {
                dismantle_instr(the_instr);
              }
            break;
          }
        case io_max:
          {
            if (flag_dismantle_maxs ||
                (flag_dismantle_imaxs &&
                 (the_instr->result_type()->unqual()->op() == TYPE_INT)))
              {
                dismantle_instr(the_instr);
              }
            break;
          }
        case io_abs:
          {
            if (flag_dismantle_abss ||
                (flag_dismantle_iabss &&
                 (the_instr->result_type()->unqual()->op() == TYPE_INT)))
              {
                dismantle_instr(the_instr);
              }
            break;
          }
        case io_mod:
          {
            if (flag_dismantle_mods)
                dismantle_instr(the_instr);
            break;
          }
        case io_memcpy:
          {
            if (flag_dismantle_memcpys)
                dismantle_instr(the_instr);
            break;
          }
        case io_array:
          {
            if (flag_dismantle_arrays)
              {
                in_array *the_array = (in_array *)the_instr;
                operand new_operand = dismantle_array(the_array);
                if (flag_reassociate_arrays)
                    try_reassociation(new_operand);
              }
            break;
          }
        case io_mbr:
          {
            if (flag_dismantle_mbrs)
              {
                in_mbr *the_mbr = (in_mbr *)the_instr;
                dismantle_mbr(the_mbr);
              }
            break;
          }
        default:
            break;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
