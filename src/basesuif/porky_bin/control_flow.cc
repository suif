/* file "control_flow.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE control_flow_cc

#include "porky.h"

RCS_BASE(
    "$Id: control_flow.cc,v 1.2 1999/08/25 03:27:26 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void prune_a_node(tree_node *the_node, void *);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void control_flow_prune(tree_node *the_node)
  {
    the_node->map(&prune_a_node, NULL, FALSE);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void prune_a_node(tree_node *the_node, void *)
  {
    if (the_node->kind() == TREE_IF)
      {
        tree_if *the_if = (tree_if *)the_node;
        if (if_test_must_be_true(the_if) || if_test_must_be_false(the_if))
          {
            made_progress = TRUE;
            dismantle(the_if);
          }
      }
    else if (the_node->kind() == TREE_FOR)
      {
        tree_for *the_for = (tree_for *)the_node;
        if (for_must_not_execute(the_for))
          {
            made_progress = TRUE;
            dismantle_for(the_for);
          }
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
