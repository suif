/* file "loop_invariants.cc" */

/*  Copyright (c) 1994,95 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* moving of loop-invariant expressions out of loop bodies for the
 * porky program for SUIF */

#define RCS_BASE_FILE loop_invariants_cc

#include "porky.h"

RCS_BASE(
    "$Id: loop_invariants.cc,v 1.2 1999/08/25 03:27:31 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Constant Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Constant Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

DECLARE_LIST_CLASS(for_list, tree_for *);

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static for_list *enclosing_fors = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void li_on_node_and_descendents(tree_node *the_node, void *data);
static void li_on_instr_tree(instruction *the_instr, boolean fred_only);
static boolean must_be_executed_by_body(tree_node *the_node,
                                        tree_node_list *body);
static boolean list_contains_branch(tree_node_list *body);
static boolean li_desired(operand the_op, boolean fred_only);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void proc_loop_invariants(tree_proc *the_proc, boolean fred_only)
  {
    to_copy_form_on_proc(the_proc);

    assert(enclosing_fors == NULL);
    enclosing_fors = new for_list;

    li_on_node_and_descendents(the_proc, &fred_only);

    assert(enclosing_fors->is_empty());
    delete enclosing_fors;
    enclosing_fors = NULL;

    from_copy_form_on_proc(the_proc);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void li_on_node_and_descendents(tree_node *the_node, void *data)
  {
    switch (the_node->kind())
      {
        case TREE_INSTR:
          {
            tree_instr *the_tree_instr = (tree_instr *)the_node;
            li_on_instr_tree(the_tree_instr->instr(), *(boolean *)data);
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)the_node;

            the_for->lb_list()->map(&li_on_node_and_descendents, data, TRUE,
                                    FALSE);
            the_for->ub_list()->map(&li_on_node_and_descendents, data, TRUE,
                                    FALSE);
            the_for->step_list()->map(&li_on_node_and_descendents, data, TRUE,
                                      FALSE);
            the_for->landing_pad()->map(&li_on_node_and_descendents, data,
                                        TRUE, FALSE);

            enclosing_fors->push(the_for);
            the_for->body()->map(&li_on_node_and_descendents, data, TRUE,
                                 FALSE);
            enclosing_fors->pop();
            break;
          }
        default:
            the_node->map(&li_on_node_and_descendents, data, TRUE, FALSE);
            break;
      }
  }

static void li_on_instr_tree(instruction *the_instr, boolean fred_only)
  {
    if (enclosing_fors->is_empty())
        return;

    unsigned num_srcs = the_instr->num_srcs();
    for (unsigned src_num = 0; src_num < num_srcs; ++src_num)
      {
        operand the_op = the_instr->src_op(src_num);
        boolean done = FALSE;
        if (li_desired(the_op, fred_only))
          {
            tree_for *good_for = NULL;
            for_list_iter for_iter(enclosing_fors);
            while (!for_iter.is_empty())
              {
                tree_for *this_for = for_iter.step();
                if (might_modify(the_op, this_for->body()))
                    break;
                if (operand_may_reference_var(the_op, this_for->index()))
                    break;
                if (!must_be_executed_by_body(the_instr->owner(),
                                              this_for->body()))
                  {
                    break;
                  }
                good_for = this_for;
              }

            if (good_for != NULL)
              {
                the_op.remove();
                expand_scope(the_op, good_for->parent());
                var_sym *new_var =
                        good_for->scope()->new_unique_var(the_op.type());
                the_instr->set_src_op(src_num, operand(new_var));
                in_rrr *new_cpy =
                        new in_rrr(io_cpy, the_op.type(), operand(new_var),
                                   operand(the_op));
                tree_instr *new_node = new tree_instr(new_cpy);
                good_for->landing_pad()->append(new_node);
                done = TRUE;
              }
          }

        if ((!done) && the_op.is_expr())
            li_on_instr_tree(the_op.instr(), fred_only);
      }
  }

static boolean must_be_executed_by_body(tree_node *the_node,
                                        tree_node_list *body)
  {
    tree_node_list *parent_list = the_node->parent();
    assert(parent_list != NULL);
    tree_node_list_e *target = the_node->list_e();
    tree_node_list_e *follow = parent_list->head();
    assert(follow != NULL);
    while (follow != NULL)
      {
        if (follow == target)
            break;

        tree_node *this_node = follow->contents;
        if (this_node->is_instr())
          {
            tree_instr *this_tree_instr = (tree_instr *)this_node;
            if (this_tree_instr->instr()->is_branch())
                return FALSE;
          }
        follow = follow->next();
      }

    if (parent_list == body)
        return TRUE;

    tree_node *parent_node = parent_list->parent();
    assert(parent_node != NULL);
    switch (parent_node->kind())
      {
        case TREE_LOOP:
          {
            tree_loop *the_loop = (tree_loop *)parent_node;
            if (parent_list == the_loop->test())
              {
                if (list_contains_branch(the_loop->body()))
                    return FALSE;
              }
            break;
          }
        case TREE_FOR:
          {
            tree_for *the_for = (tree_for *)parent_node;
            if (!for_must_execute(the_for))
                return FALSE;
            break;
          }
        case TREE_IF:
          {
            tree_if *the_if = (tree_if *)parent_node;
            if (parent_list == the_if->header())
              {
                break;
              }
            else if (parent_list == the_if->then_part())
              {
                if (if_test_must_be_true(the_if))
                    break;
              }
            else if (parent_list == the_if->else_part())
              {
                if (if_test_must_be_false(the_if))
                    break;
              }
            else
              {
                assert(FALSE);
              }

            return FALSE;
          }
        case TREE_BLOCK:
            break;
        case TREE_INSTR:
        default:
            assert(FALSE);
      }

    return must_be_executed_by_body(parent_node, body);
  }

static boolean list_contains_branch(tree_node_list *body)
  {
    tree_node_list_e *follow = body->head();
    while (follow != NULL)
      {
        tree_node *this_node = follow->contents;
        if (this_node->is_instr())
          {
            tree_instr *this_tree_instr = (tree_instr *)this_node;
            if (this_tree_instr->instr()->is_branch())
                return TRUE;
          }
        follow = follow->next();
      }
    return FALSE;
  }

static boolean li_desired(operand the_op, boolean fred_only)
  {
    if (!(reuse_desired(the_op) && operand_reevaluation_ok(the_op)))
        return FALSE;
    if (!fred_only)
        return TRUE;
    if (the_op.is_expr())
        return (the_op.instr()->annotes()->peek_annote(k_fred) != NULL);
    else if (the_op.is_symbol())
        return (the_op.symbol()->annotes()->peek_annote(k_fred) != NULL);
    else
        return FALSE;
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
