/* file "for_semantics.cc" */

/*  Copyright (c) 1994 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/* for the porky program for SUIF */

#define RCS_BASE_FILE for_semantics_cc

#include "porky.h"

RCS_BASE(
    "$Id: for_semantics.cc,v 1.2 1999/08/25 03:27:28 brm Exp $")

/*----------------------------------------------------------------------*
    Begin Type Declarations
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Type Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void change_for_semantics_on_a_node(tree_node *the_node, void *);
static void change_semantics_on_a_for(tree_for *the_for);
static boolean for_op_ok(operand the_operand, tree_for *the_for);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern void change_semantics(tree_node *the_node)
  {
    the_node->map(&change_for_semantics_on_a_node, NULL, FALSE);
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void change_for_semantics_on_a_node(tree_node *the_node, void *)
  {
    if (the_node->kind() == TREE_FOR)
      {
        tree_for *the_for = (tree_for *)the_node;
        change_semantics_on_a_for(the_for);
      }
  }

static void change_semantics_on_a_for(tree_for *the_for)
  {
    boolean bad_node = node_is_bad(the_for);
    boolean ub_ok = !bad_node && for_op_ok(the_for->ub_op(), the_for);
    boolean step_ok = !bad_node && for_op_ok(the_for->step_op(), the_for);
    if (!ub_ok || !step_ok)
      {
        if (ub_ok)
          {
            operand old_ub_operand = the_for->ub_op();
            if ((old_ub_operand.kind() == OPER_INSTR) &&
                (old_ub_operand.instr()->format() != inf_ldc))
              {
                make_ub_temp(the_for);
              }
          }

        if (step_ok)
          {
            operand old_step_operand = the_for->step_op();
            if ((old_step_operand.kind() == OPER_INSTR) &&
                (old_step_operand.instr()->format() != inf_ldc))
              {
                make_step_temp(the_for);
              }
          }

        dismantle_for_ops_reevaluated(the_for);
      }
  }

static boolean for_op_ok(operand the_operand, tree_for *the_for)
  {
    return (!might_modify(the_operand, the_for->body()) &&
            !operand_contains_impure_call(the_operand) &&
            !operand_may_reference_var(the_operand, the_for->index()));
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
