/* file "dismantle.h" */


/*
       Copyright (c) 1998 Stanford University

       All rights reserved.

       This software is provided under the terms described in
       the "suif_copyright.h" include file.
*/

#include <suif_copyright.h>


#ifndef PORKY_DISMANTLE_H
#define PORKY_DISMANTLE_H

#ifndef SUPPRESS_PRAGMA_INTERFACE
#pragma interface
#endif


/*
      This is the interface to dismantling passes of the porky
      library.
*/


/*
 *  PREREQUISITES: "Statement List Form"
 *                 "Statements Only In Statement Lists"
 *                 "Statements Only In Procedure Definitions"
 *                 "All Szots Scoped"
 *                 "Standard If Statement Semantics"
 *                 "Standard Statement List Semantics"
 *                 "Standard Branch Statement Semantics"
 *                 "Standard Jump Statement Semantics"
 *                 "Standard Label Location Statement Semantics"
 *                 "Standard If Statement Completeness"
 *  GENERATES: "No If Statements"
 */

class dismantle_if_statements_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<if_statement *> _candidates;

public:
    dismantle_if_statements_pass(void)  { }
    virtual ~dismantle_if_statements_pass(void)  { }

    virtual lstring name(void) const  { return "dismantle-if-statements"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(file_set_block *the_file_set_block)
      { }
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_if_statement(if_statement *the_if);
  };


/*
 *  PREREQUISITES: "Statement List Form"
 *                 "Statements Only In Statement Lists"
 *                 "Statements Only In Procedure Definitions"
 *                 "All Szots Scoped"
 *                 "Standard While Statement Semantics"
 *                 "Standard Statement List Semantics"
 *                 "Standard Branch Statement Semantics"
 *                 "Standard Jump Statement Semantics"
 *                 "Standard Label Location Statement Semantics"
 *                 "Standard While Statement Completeness"
 *  GENERATES: "No While Statements"
 */

class dismantle_while_statements_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<while_statement *> _candidates;

public:
    dismantle_while_statements_pass(void)  { }
    virtual ~dismantle_while_statements_pass(void)  { }

    virtual lstring name(void) const  { return "dismantle-while-statements"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(file_set_block *the_file_set_block)
      { }
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_while_statement(while_statement *the_while);
  };


/*
 *  PREREQUISITES: "Statement List Form"
 *                 "Statements Only In Statement Lists"
 *                 "Statements Only In Procedure Definitions"
 *                 "All Szots Scoped"
 *                 "Standard Do While Statement Semantics"
 *                 "Standard Statement List Semantics"
 *                 "Standard Branch Statement Semantics"
 *                 "Standard Label Location Statement Semantics"
 *                 "Standard Do While Statement Completeness"
 *  GENERATES: "No Do While Statements"
 */

class dismantle_do_while_statements_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<do_while_statement *> _candidates;

public:
    dismantle_do_while_statements_pass(void)  { }
    virtual ~dismantle_do_while_statements_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-do-while-statements"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(file_set_block *the_file_set_block)
      { }
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_do_while_statement(
            do_while_statement *the_do_while);
  };


class dismantle_for_statements_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<for_statement *> _candidates;

public:
    dismantle_for_statements_pass(void)  { }
    virtual ~dismantle_for_statements_pass(void)  { }

    virtual lstring name(void) const  { return "dismantle-for-statements"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(file_set_block *the_file_set_block)
      { }
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_for_statement(for_statement *the_for);
  };


class dismantle_scope_statements_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<scope_statement *> _candidates;

public:
    dismantle_scope_statements_pass(void)  { }
    virtual ~dismantle_scope_statements_pass(void)  { }

    virtual lstring name(void) const  { return "dismantle-scope-statements"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(file_set_block *the_file_set_block)
      { }
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_scope_statement(scope_statement *the_scope);
  };


class dismantle_multi_way_branch_statements_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<multi_way_branch_statement *> _candidates;

public:
    dismantle_multi_way_branch_statements_pass(void)  { }
    virtual ~dismantle_multi_way_branch_statements_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-multi-way-branch-statements"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(file_set_block *the_file_set_block)
      { }
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_multi_way_branch_statement(
            multi_way_branch_statement *the_mwb);
  };


class dismantle_select_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_select_instructions_pass(void)  { }
    virtual ~dismantle_select_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-select-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_array_reference_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_array_reference_instructions_pass(void)  { }
    virtual ~dismantle_array_reference_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-array-reference-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_field_access_instructions_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<field_access_instruction *> _candidates;

public:
    dismantle_field_access_instructions_pass(void)  { }
    virtual ~dismantle_field_access_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-field-access-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block);
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_field_access_instruction(
            field_access_instruction *the_field_access);
  };


class dismantle_extract_fields_instructions_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<extract_fields_instruction *> _candidates;

public:
    dismantle_extract_fields_instructions_pass(void)  { }
    virtual ~dismantle_extract_fields_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-extract-fields-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block);
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_extract_fields_instruction(
            extract_fields_instruction *the_extract_fields);
  };


class dismantle_set_fields_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_set_fields_instructions_pass(void)  { }
    virtual ~dismantle_set_fields_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-set-fields-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_extract_elements_instructions_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<extract_elements_instruction *> _candidates;

public:
    dismantle_extract_elements_instructions_pass(void)  { }
    virtual ~dismantle_extract_elements_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-extract-elements-instruction"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block);
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_extract_elements_instruction(
            extract_elements_instruction *the_extract_elements);
  };


class dismantle_set_elements_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_set_elements_instructions_pass(void)  { }
    virtual ~dismantle_set_elements_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-set-elements-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_bit_size_of_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_bit_size_of_instructions_pass(void)  { }
    virtual ~dismantle_bit_size_of_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-bit-size-of-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_byte_size_of_instructions_pass : public suif_to_suif_pass
  {
private:
    adlist_tos<byte_size_of_instruction *> _candidates;

public:
    dismantle_byte_size_of_instructions_pass(void)  { }
    virtual ~dismantle_byte_size_of_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-byte-size-of-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block);
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition);
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }

    virtual void do_a_zot(zot *the_zot);
    virtual void dismantle_byte_size_of_instruction(
            byte_size_of_instruction *the_byte_size_of);
  };


class dismantle_bit_alignment_of_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_bit_alignment_of_instructions_pass(void)  { }
    virtual ~dismantle_bit_alignment_of_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-bit-alignment-of-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_byte_alignment_of_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_byte_alignment_of_instructions_pass(void)  { }
    virtual ~dismantle_byte_alignment_of_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-byte-alignment-of-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_bit_offset_of_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_bit_offset_of_instructions_pass(void)  { }
    virtual ~dismantle_bit_offset_of_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-bit-offset-of-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_byte_offset_of_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_byte_offset_of_instructions_pass(void)  { }
    virtual ~dismantle_byte_offset_of_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-byte-offset-of-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_sc_and_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_sc_and_instructions_pass(void)  { }
    virtual ~dismantle_sc_and_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-sc-and-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_sc_or_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_sc_or_instructions_pass(void)  { }
    virtual ~dismantle_sc_or_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-sc-or-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_sc_select_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_sc_select_instructions_pass(void)  { }
    virtual ~dismantle_sc_select_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-sc-select-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_load_value_block_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_load_value_block_instructions_pass(void)  { }
    virtual ~dismantle_load_value_block_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-load-value-block-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


class dismantle_multi_way_branch_instructions_pass : public suif_to_suif_pass
  {
public:
    dismantle_multi_way_branch_instructions_pass(void)  { }
    virtual ~dismantle_multi_way_branch_instructions_pass(void)  { }

    virtual lstring name(void) const
      { return "dismantle-multi-way-branch-instructions"; }
    virtual lstring memory_model(void) const
      { return k_mm_one_procedure_definition_at_a_time; }

    virtual void do_file_set_block_preorder(
            file_set_block *the_file_set_block);
    virtual void do_file_set_block_postorder(
            file_set_block *the_file_set_block)  { }
    virtual void do_file_block_preorder(file_block *the_file_block)  { }
    virtual void do_file_block_postorder(file_block *the_file_block)  { }
    virtual void do_procedure_definition_preorder(
            procedure_definition *the_procedure_definition)  { }
    virtual void do_procedure_definition_postorder(
            procedure_definition *the_procedure_definition)  { }
  };


#endif /* PORKY_DISMANTLE_H */
