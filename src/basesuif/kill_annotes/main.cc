/* file "main.cc" of the kill_annotes program for SUIF */

/*  Copyright (c) 1995 Stanford University

    All rights reserved.

    This software is provided under the terms described in
    the "suif_copyright.h" include file. */

#include <suif_copyright.h>

/*
 *  This file contains the main program for kill_annotes.
 */

#define RCS_BASE_FILE main_cc

#include <suif1.h>
#include <useful.h>
#include <string.h>

RCS_BASE(
    "$Id: main.cc,v 1.1.1.1 1998/06/16 15:17:35 brm Exp $")

INCLUDE_SUIF_COPYRIGHT

/*----------------------------------------------------------------------*
    Begin Private Type Definitions
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Private Type Definitions
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Global Variables
 *----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*
    End Public Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Global Variables
 *----------------------------------------------------------------------*/

static const char **kill_annote_names = NULL;

/*----------------------------------------------------------------------*
    End Private Global Variables
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Declarations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[]);

/*----------------------------------------------------------------------*
    End Public Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Declarations
 *----------------------------------------------------------------------*/

static void usage(void);
static void kill_annotes_on_object(suif_object *the_object);

/*----------------------------------------------------------------------*
    End Private Function Declarations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Public Function Implementations
 *----------------------------------------------------------------------*/

extern int main(int argc, char *argv[])
  {
    start_suif(argc, argv);

    int table_size = 20;
    kill_annote_names = new const char *[table_size];
    int table_position = 0;

    char *input_name = NULL;
    boolean found_input = FALSE;
    for (int arg_num = 1; arg_num < argc; ++arg_num)
      {
        if (strcmp(argv[arg_num], "-named") == 0)
          {
            ++arg_num;
            if (arg_num >= argc)
                usage();
            if (table_position + 1 >= table_size)
              {
                const char **new_table = new const char *[table_size * 2];
                memcpy(new_table, kill_annote_names,
                       table_size * sizeof(char *));
                table_size *= 2;
                delete[] kill_annote_names;
                kill_annote_names = new_table;
              }
            kill_annote_names[table_position] =
                    lexicon->enter(argv[arg_num])->sp;
            ++table_position;
          }
        else if (input_name == NULL)
          {
            input_name = argv[arg_num];
          }
        else
          {
            fileset->add_file(input_name, argv[arg_num]);
            input_name = NULL;
            found_input = TRUE;
          }
      }

    if ((!found_input) || (input_name != NULL))
        usage();

    kill_annote_names[table_position] = NULL;

    walk(fileset->globals(), &kill_annotes_on_object);

    fileset->reset_iter();
    while (TRUE)
      {
        file_set_entry *fse = fileset->next_file();
        if (fse == NULL)
            break;
        walk(fse, &kill_annotes_on_object);
        fse->reset_proc_iter();
        while (TRUE)
          {
            proc_sym *this_proc_sym = fse->next_proc();
            if (this_proc_sym == NULL)
                break;
            this_proc_sym->read_proc(TRUE, FALSE);
            walk(this_proc_sym->block(), &kill_annotes_on_object);
            this_proc_sym->write_proc(fse);
            this_proc_sym->flush_proc();
          }
      }

    delete[] kill_annote_names;

    exit_suif();
    return 0;
  }

/*----------------------------------------------------------------------*
    End Public Function Implementations
 *----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*
    Begin Private Function Implementations
 *----------------------------------------------------------------------*/

static void usage(void)
  {
    fprintf(stderr,
            "usage: %s { -named <annote-name> }*\n"
            "           <infile> <outfile> { <infile> <outfile> }*\n",
            _suif_prog_base_name);
    exit(1);
  }

static void kill_annotes_on_object(suif_object *the_object)
  {
    annote_list_e *follow_e = the_object->annotes()->head();
    while (follow_e != NULL)
      {
        annote_list_e *next_e = follow_e->next();
        const char **follow_names = kill_annote_names;
        while (*follow_names != NULL)
          {
            if (follow_e->contents->name() == *follow_names)
              {
                the_object->annotes()->remove(follow_e);
                delete follow_e->contents;
                delete follow_e;
                break;
              }
            ++follow_names;
          }
        follow_e = next_e;
      }
  }

/*----------------------------------------------------------------------*
    End Private Function Implementations
 *----------------------------------------------------------------------*/
