# read the standard variable definitions

include $(SUIFHOME)/Makefile.defs

# comment these out and/or pass them in from the environment to not
# build the documentation on your machine:

# BUILD_INFO =	1
# BUILD_PS =  	1
# BUILD_HTML =	1
# BUILD_MAN =	1

# similar for this one for the shared TCL libraries and scripts:

# BUILD_TCL =	1
# BUILD_SCRIPTS = 1

#
# To save time on some systems, set CATSOURCE to catted.cc and
# set BUILD_CATTED_LIBRARIES in Makefile, to cat all .cc files together
# and compile as one unit.
#

ifdef CATSOURCE
OBJS :=		$(subst .cc,.o,$(CATSOURCE))
OLD_SRCS :=	$(SRCS)
SRCS :=		$(CATSOURCE)
$(CATSOURCE):	$(OLD_SRCS)
		rm -f $(CATSOURCE)
		fgrep '#pragma implementation' $(OLD_SRCS) | \
			sed -e 's/^[^:]*://' > $(CATSOURCE)
		cat $(OLD_SRCS) | fgrep -v '#pragma implementation' \
			>> $(CATSOURCE)
endif
ifdef BUILD_CATTED_LIBRARIES
USE_SUIF_STRIPPED_HEADERS =
else
USE_SUIF_STRIPPED_HEADERS = -DSUIF_STRIPPED_HEADERS
endif

REALSRCS =	$(YSRCS) $(DSRCS) $(HEADERS) $(CSRCS) $(SRCS) \
		$(INFOSRCS) $(MANPAGES) $(GIFSRCS) $(VERDATAFILE) $(TCLSRCS) \
		$(OTHERSRCS)
ALLTMPSRCS =	$(TMP_YSRCS) $(TMP_DSRCS) $(TMP_HEADERS) $(TMP_CSRCS) \
		$(TMP_SRCS) $(TMP_INFOSRCS) $(TMP_MANPAGES) $(TMP_GIFSRCS) \
		$(TMP_VERDATAFILE) $(TMP_TCLSRCS) \
		$(TMP_OTHERSRCS)
ALLSRCS =	$(REALSRCS) $(ALLTMPSRCS)
ALLCSRCS =	$(CSRCS) $(TMP_CSRCS)
ALLCXXSRCS =	$(SRCS) $(TMP_SRCS)
ALLHEADERS =	$(HEADERS) $(AUXILIARY_HEADERS) $(TMP_HEADERS)
ALLGIFSRCS =	$(GIFSRCS) $(TMP_GIFSRCS)
ALLVERDATAFILE = $(VERDATAFILE) $(TMP_VERDATAFILE)
ALLTCLSRCS =	$(TCLSRCS) $(TMP_TCLSRCS)

# note: ALLHEADERS exported so we can suif-install-env it
# it is cleared in suif-recursive-make
unexport REALSRCS ALLTMPSRCS ALLSRCS ALLCSRCS ALLCXXSRCS
unexport ALLGIFSRCS ALLVERDATAFILE ALLTCLSRCS

# Why put $(LIBNAME_SET) under .PHONY?  Because at the top of
# Makefile.rules vpath is set to include the library binary directory,
# but we don't want it to find local targets there and not build them
# locally.  Makefile.rules attempts to solve this problem by only
# adding the vpath constructs if LIBS is defined, so it won't be
# invoked on pure libraries.  But this isn't sufficient for sharlit
# and nsharlit because they define both a program and a library.  So
# we use .PHONY to make sure the local libraries are always rebuilt.
#
.PHONY:		$(TARGET) $(LIBNAME_SET) pure prog_ver.cc lib_ver.cc \
		start_sty.cc register_lib.cc lib prog info install \
		install-keep-src show-install reinstall reinstall-keep-src \
		show-reinstall reinstall-bin show-reinstall-bin \
		real-reinstall real-reinstall-keep-src deinstall \
		install-local-ver install-ver install-prog install-scripts \
		install-lib install-bin-base deinstall-bin \
		install-bin-recursive install-bin-all deinstall-src \
		install-src-dir install-local-src install-recursive-src \
		install-src install-incl install-local-incl \
		deinstall-local-incl install-recursive-incl install-info \
		install-local-info deinstall-local-info \
		install-recursive-info preinstall-check install-verdata \
		deinstall-verdata source clean clean-src clean-rcs-links \
		depend sub-packages recursive build-dir directory-structure

FORCE_RCS=FORCE

unexport FORCE_RCS

include $(SUIFHOME)/Makefile.rules

ifdef BUILD_INFO
STAMP_INFO_TARGET =	stamp-info
endif
ifdef BUILD_PS
STAMP_PS_TARGET =	stamp-ps
endif
ifdef BUILD_HTML
STAMP_HTML_TARGET =	stamp-html
endif
STAMP_DOC_TARGETS =	$(STAMP_INFO_TARGET) $(STAMP_PS_TARGET) \
			$(STAMP_HTML_TARGET)

unexport STAMP_INFO_TARGET STAMP_PS_TARGET STAMP_HTML_TARGET STAMP_DOC_TARGETS

%.o : %.c $(REALSRCS) $(TMP_HEADERS) $(TMP_CSRCS) Makefile.deps FORCE
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

%.o : %.cc $(REALSRCS) $(TMP_HEADERS) $(TMP_SRCS) Makefile.deps FORCE
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

$(TARGET): $(OBJS) Makefile.deps
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

pure: $(OBJS) Makefile.deps
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

$(STATIC_LIBNAME): $(OBJS) Makefile.deps
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

$(SHARED_LIBNAME): $(OBJS) Makefile.deps
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

prog_ver.cc: $(TMP_HEADERS) $(TMP_SRCS) $(TMP_CSRCS) $(OBJS) Makefile.deps
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

lib_ver.cc: $(TMP_HEADERS) $(TMP_SRCS) $(TMP_CSRCS) $(OBJS) Makefile.deps
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

start_sty.cc: $(TMP_HEADERS) $(TMP_SRCS) $(TMP_CSRCS) Makefile.deps Makefile
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

register_lib.cc: $(TMP_HEADERS) $(TMP_SRCS) $(TMP_CSRCS) Makefile.deps Makefile
		@$(MAKE) --no-print-directory -f $(SUIFHOME)/Makefile.sub $@

lib:		$(LIBNAME_SET)

prog:		$(TARGET_EXECUTABLES)

info:		stamp-info

ps:		stamp-ps

html:		stamp-html

all-docs:	stamp-info stamp-ps stamp-html

stamp-info:	$(INFOSRCS)
ifdef INFOS
		for i in $(INFOS) ; do \
			makeinfo $$i.texi ; \
		done
endif
		@touch stamp-info

ifdef INFOS
stamp-ps:	$(INFOSRCS) suif_logo.epsf
		for i in $(INFOS) ; do \
		    if test ! -f $$i.ps || \
			newer $$i.texi $$i.ps ; then \
			tex $$i.texi ; \
			texindex $$i.cp $$i.fn $$i.ky $$i.pg $$i.tp $$i.vr ; \
			tex $$i.texi ; \
			dvips -o $$i.ps $$i.dvi ; \
		    else : ; \
		    fi ; \
		done
else
stamp-ps:	$(INFOSRCS)
endif
		@touch stamp-ps

stamp-html:	$(INFOSRCS)
ifdef INFOS
		for i in $(INFOS) ; do \
			texi2html -split_node -menu $$i.texi ; \
		done
endif
		@touch stamp-html

current:
		$(MAKE) --no-print-directory

show-install:
		@$(MAKE) --no-print-directory preinstall-check
		@$(MAKE) --no-print-directory deinstall-src 1>> $(LOGFILE) 2>&1
		@$(MAKE) --no-print-directory install-src-dir 1>> $(LOGFILE) \
			2>&1
ifdef SUB_PACKAGES
		@SILENT_RECURSIVE_MAKE=yes $(BINDIR)/suif-recursive-make $@ 2> \
			$(LOGFILE) ; exit 0
endif
		@if $(MAKE) --no-print-directory SUB_PACKAGES= \
				install-keep-src 1>> $(LOGFILE) 2>&1; then \
			echo $(PACKAGE_FULL_NAME)" ... OK"; \
		else \
			echo $(PACKAGE_FULL_NAME)" ... FAILED -- see" \
				$(PACKAGE_FULL_NAME)/$(LOGFILE); \
		fi
		@$(MAKE) --no-print-directory install-verdata 1>> $(LOGFILE) \
			2>&1

install:
		@$(MAKE) --no-print-directory install-ver
		@$(MAKE) --no-print-directory reinstall

install-keep-src:
		@$(MAKE) --no-print-directory install-ver
		@$(MAKE) --no-print-directory reinstall-keep-src

show-reinstall:
		@$(MAKE) --no-print-directory deinstall-verdata 1> $(LOGFILE) \
			2>&1
		@$(MAKE) --no-print-directory preinstall-check
		@$(MAKE) --no-print-directory deinstall-src 1>> $(LOGFILE) 2>&1
		@$(MAKE) --no-print-directory install-src-dir 1>> $(LOGFILE) \
			2>&1
ifdef SUB_PACKAGES
		@SILENT_RECURSIVE_MAKE=yes $(BINDIR)/suif-recursive-make $@ 2> \
			$(LOGFILE) ; exit 0
endif
		@if $(MAKE) --no-print-directory SUB_PACKAGES= \
				reinstall-keep-src 1>> $(LOGFILE) 2>&1; then \
			echo $(PACKAGE_FULL_NAME)" ... OK"; \
		else \
			echo $(PACKAGE_FULL_NAME)" ... FAILED -- see" \
				$(PACKAGE_FULL_NAME)/$(LOGFILE); \
		fi;
		@$(MAKE) --no-print-directory install-verdata 1>> $(LOGFILE) \
			2>&1

reinstall:
		$(MAKE) --no-print-directory real-reinstall ; \

reinstall-keep-src:
		$(MAKE) --no-print-directory real-reinstall-keep-src

show-reinstall-keep-src:
ifdef SUB_PACKAGES
		@SILENT_RECURSIVE_MAKE=yes $(BINDIR)/suif-recursive-make $@ 2> \
			$(LOGFILE) ; exit 0
else
		@echo > $(LOGFILE)
endif
		@if $(MAKE) --no-print-directory SUB_PACKAGES= \
				reinstall-keep-src 1>> $(LOGFILE) 2>&1; then \
			echo $(PACKAGE_FULL_NAME)" ... OK"; \
		else \
			echo $(PACKAGE_FULL_NAME)" ... FAILED -- see" \
				$(PACKAGE_FULL_NAME)/$(LOGFILE); \
		fi;

show-reinstall-bin:
ifdef SUB_PACKAGES
		@SILENT_RECURSIVE_MAKE=yes $(BINDIR)/suif-recursive-make $@ 2> \
			$(LOGFILE) ; exit 0
endif
		@if $(MAKE) --no-print-directory SUB_PACKAGES= reinstall-bin \
				1> $(LOGFILE) 2>&1; then \
			echo $(PACKAGE_FULL_NAME)" ... OK"; \
		else \
			echo $(PACKAGE_FULL_NAME)" ... FAILED -- see" \
				$(PACKAGE_FULL_NAME)/$(LOGFILE); \
		fi;

reinstall-bin:
		$(MAKE) --no-print-directory install-bin-all

real-reinstall:
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) deinstall-verdata
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) preinstall-check
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) deinstall-src
		@$(MAKE) --no-print-directory real-reinstall-keep-src
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-verdata

real-reinstall-keep-src:
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-bin
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-src-dir
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-local-src
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-local-incl
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-local-info
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make real-reinstall-keep-src
endif

deinstall:
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) deinstall-verdata
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) deinstall-src
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) deinstall-bin
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) deinstall-local-incl
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) deinstall-local-info
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make deinstall
endif

install-local-ver:

install-ver:	install-local-ver
		@if test ! ""$(SUPER_PACKAGES_PATH) = "" ; then \
			owner_package=`echo $(SUPER_PACKAGES_PATH) | \
				sed 's/.*\///'`; \
			: ; \
		else : ; \
		fi

install-prog:	$(TARGET_EXECUTABLES) install-bin-base
		for target in $(TARGET_EXECUTABLES); do \
			if test -f $$target$(EXEEXT); then \
				$(BINDIR)/install-sh -C -m 775 $$target$(EXEEXT) $(BINDIR) ; \
			else \
				$(BINDIR)/install-sh -C -m 775 $$target $(BINDIR) ; \
			fi; \
		done

install-scripts:	$(SCRIPTS) install-bin-base
ifdef BUILD_SCRIPTS
		for target in $(SCRIPTS); do \
			$(BINDIR)/install-sh -C -m 775 $$target $(SCRIPTDIR) ; \
		done
endif

install-lib:	$(LIBNAME_SET) install-bin-base
ifeq ($(INSTALL_SHARED_LIB),true)
		$(BINDIR)/install-sh -C -m 444 $(SHARED_LIBNAME) $(SODIR)
endif
ifeq ($(INSTALL_STATIC_LIB),true)
		$(BINDIR)/install-sh -C -m 444 $(STATIC_LIBNAME) $(LIBDIR)
ifeq ($(MACHINE),sparc-sun-sunos4)
		ranlib $(LIBDIR)/$(STATIC_LIBNAME)
endif
endif
		rm -f $(AUTOINITDIR)/$(TARGET)
ifdef AUTOINITLIB
		touch $(AUTOINITDIR)/$(TARGET)
endif

install-bin-base: $(ALLTCLSRCS)
ifdef BUILD_TCL
ifneq (,$(strip $(ALLTCLSRCS)))
		for target in $(ALLTCLSRCS); do \
			$(BINDIR)/install-sh -C -m 444 $$target $(TCLDIR) ; \
		done
endif
endif

deinstall-bin:
ifneq (,$(strip $(TARGET_EXECUTABLES)))
		for target in $(TARGET_EXECUTABLES); do \
			rm -f $(BINDIR)/$$target ; \
		done
endif
		rm -f $(LIBDIR)/$(STATIC_LIBNAME)
		rm -f $(SODIR)/$(SHARED_LIBNAME)
ifneq (,$(TARGET))
		rm -f $(AUTOINITDIR)/$(TARGET)
endif

install-bin-recursive:
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make install-bin-all
endif

install-bin-all:
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-bin
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-bin-recursive

install-src:
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) deinstall-src
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-src-dir
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-local-src
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-recursive-src

deinstall-src:

install-src-dir:

install-local-src:	$(REALSRCS)

install-recursive-src:
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make install-src
endif

install-incl:
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-local-incl
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-recursive-incl

install-local-incl:	$(EXPORTS)
ifdef EXPORTS
ifneq (,$(strip $(ALLHEADERS)))
		if test ! -d $(INCLDIR)/$(PACKAGE_NAME) ; then \
			mkdir $(INCLDIR)/$(PACKAGE_NAME) ; \
		fi ; \
		chmod 775 $(INCLDIR)/$(PACKAGE_NAME) ; \
		$(BINDIR)/install-sh -C -m 444 $(ALLHEADERS) $(INCLDIR)/$(PACKAGE_NAME)
else
		rm -rf $(INCLDIR)/$(PACKAGE_NAME)
		mkdir $(INCLDIR)/$(PACKAGE_NAME)
		chmod 775 $(INCLDIR)/$(PACKAGE_NAME)
endif
		for i in $(EXPORTS) ; do \
			$(BINDIR)/install-sh -C -m 444 $$i $(INCLDIR) ; \
		done
endif

deinstall-local-incl:
ifdef EXPORTS
		rm -rf $(INCLDIR)/$(PACKAGE_NAME)
		for i in $(EXPORTS) ; do \
			rm -f $(INCLDIR)/$$i ; \
		done
endif

install-recursive-incl:
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make install-incl
endif

install-info:
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-local-info
		@$(MAKE) --no-print-directory $(MAKEPARFLAGS) install-recursive-info

install-local-info:	$(STAMP_DOC_TARGETS) $(MANPAGES) $(ALLGIFSRCS)
ifdef INFOS
ifdef BUILD_INFO
		for i in $(INFOS) ; do \
		    for j in $$i.info* ; do \
			$(BINDIR)/install-sh -C -m 444 $$j $(INFODIR) ; \
		    done ; \
		done
endif
ifdef BUILD_PS
		for i in $(INFOS) ; do \
			$(BINDIR)/install-sh -C -m 444 $$i.ps $(DOCDIR) ; \
		done
endif
ifdef BUILD_HTML
ifneq (,$(strip $(ALLGIFSRCS)))
		for i in $(ALLGIFSRCS) ; do \
			$(BINDIR)/install-sh -C -m 444 $$i $(HTMLDIR) ; \
		done
endif
		for i in $(INFOS) ; do \
		    for j in $$i*.html ; do \
			$(BINDIR)/install-sh -C -m 444 $$j $(HTMLDIR) ; \
		    done ; \
		done
endif
endif
ifdef MANPAGES
ifdef BUILD_MAN
		for i in $(MANPAGES) ; do \
			MANSUFFIX=`echo $$i | sed 's/.*\.//'` ; \
			MANSUFDIR=$(MANDIR)/man$$MANSUFFIX ; \
			if test ! -d $$MANSUFDIR; then \
				mkdir $$MANSUFDIR; \
				chmod 775 $$MANSUFDIR; \
			else : ; \
			fi ; \
			$(BINDIR)/install-sh -C -m 444 $$i $$MANSUFDIR ; \
		done
endif
ifdef BUILD_HTML
		for i in $(MANPAGES) ; do \
 			if test ! -f $(HTMLDIR)/man_$$i.html || \
				newer $$i $(HTMLDIR)/man_$$i.html; then \
				rm -f $(HTMLDIR)/man_$$i.html ; \
				nroff -man $$i | rman -n $$i -f HTML -r \
				"man_%s.%s.html" -l "Man page for %s" > \
				$(HTMLDIR)/man_$$i.html ; \
			else : ; fi ; \
		done
endif
endif

deinstall-local-info:
ifdef INFOS
		for i in $(INFOS) ; do \
			rm -f $(INFODIR)/$$i.info* ; \
		done
		for i in $(INFOS) ; do \
			rm -f $(DOCDIR)/$$i.ps ; \
		done
		for i in $(ALLGIFSRCS) ; do \
			rm -f $(HTMLDIR)/$$i ; \
		done
		for i in $(INFOS) ; do \
			rm -f $(HTMLDIR)/$$i*.html ; \
		done
endif
ifdef MANPAGES
		for i in $(MANPAGES) ; do \
			rm -f $(MANDIR)/$$i ; \
		done
		for i in $(MANPAGES) ; do \
			rm -f $(HTMLDIR)/man_$$i.html ; \
		done
endif

install-recursive-info:
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make install-info
endif

preinstall-check: $(ALLVERDATAFILE)
ifneq (,$(strip $(ALLVERDATAFILE)))
		@if test -f $(BINDIR)/ververver ; then \
			$(BINDIR)/ververver -check $(PACKAGE_NAME) $(ALLVERDATAFILE) \
				$(VERDATADIR) ; \
		else \
			: ; \
		fi
else
		@echo > /dev/null
endif

install-verdata:
ifneq (,$(strip $(ALLVERDATAFILE)))
		@$(BINDIR)/ververver -install $(PACKAGE_NAME) $(ALLVERDATAFILE) \
			$(VERDATADIR)
endif

deinstall-verdata:
ifneq (,$(strip $(ALLVERDATAFILE)))
		@if test -f $(BINDIR)/ververver ; then \
			$(BINDIR)/ververver -deinstall $(PACKAGE_NAME) \
				$(ALLVERDATAFILE) $(VERDATADIR) ; \
		else \
			: ; \
		fi
endif

source:		$(REALSRCS)

objects:	$(OBJS)

clean:		clean-src
		rm -f core $(OBJS) $(TARGET) $(STATIC_LIBNAME)
		rm -f $(SHARED_LIBNAME) $(ALLTMPSRCS)
		rm -f Makefile.deps
		rm -f versions.ver
		rm -f prog_ver.cc prog_ver.o
		rm -f lib_ver.cc lib_ver.o
		rm -f start_sty.cc start_sty.o
		rm -f register_lib.cc register_lib.o
		rm -f so_locations
		rm -rf ii_files
		rm -rf Templates.DB
		rm -f *.rpo
		rm -f stamp-library-closure
ifneq ($(PACKAGE_NAME),suif1)
		rm -f suif_logo.epsf
endif
ifdef INFOS
		for i in $(INFOS) ; do \
			rm -f $$i.cp $$i.fn $$i.ky $$i.pg $$i.tp $$i.vr ; \
			rm -f $$i.cps $$i.fns $$i.kys $$i.pgs $$i.tps ; \
			rm -f $$i.vrs $$i.toc $$i.info* $$i*.html ; \
			rm -f $$i.dvi $$i.ps $$i.aux $$i.log ; \
		done
endif
		rm -f stamp-info
		rm -f stamp-ps
		rm -f stamp-html
		rm -f stamp-depend
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make clean
endif

#
# NOTE: I had to separate out the components of REALSRCS here because
# otherwise some libraries, such as libsuif, cause lines that are too
# long and the attempted ``make clean'' fails.
#
clean-src:

clean-rcs-links:
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make clean-rcs-links
endif

stamp-depend:	Makefile $(ALLCSRCS) $(ALLCXXSRCS) $(ALLHEADERS)
		@touch stamp-depend

depend:		Makefile.deps

Makefile.deps:	Makefile stamp-depend
		@rm -f Makefile.deps
		@echo > Makefile.deps
ifneq ($(strip $(ALLCXXSRCS)),)
		@echo '# Dependencies for C++ files' >> Makefile.deps
		@echo >> Makefile.deps
		$(CXX) $(DEPSFLAG) $(CXXFLAGS) $(ALLCXXSRCS) >> Makefile.deps
		@echo >> Makefile.deps
endif
ifneq ($(strip $(ALLCSRCS)),)
		@echo '# Dependencies for C files' >> Makefile.deps
		@echo >> Makefile.deps
		$(CC) $(DEPSFLAG) $(CFLAGS) $(ALLCSRCS) >> Makefile.deps
		@echo >> Makefile.deps
endif

sub-packages:
		@echo $(SUB_PACKAGES)

recursive:
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make
endif

ifneq ($(PACKAGE_NAME),suif1)
suif_logo.epsf:
		rm -f suif_logo.epsf
		ln -s $(INCLDIR)/suif_logo.epsf .
endif

build-dir: source
		@REALSRCS='$(REALSRCS)' build_dir_setup_links

directory-structure:
ifdef SUB_PACKAGES
		$(BINDIR)/suif-recursive-make directory-structure
endif


